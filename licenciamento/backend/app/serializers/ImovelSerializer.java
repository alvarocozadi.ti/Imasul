package serializers;

import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import utils.SerializerUtil;
import utils.flexjson.GeometryTransformer;

public class ImovelSerializer {

	public static JSONSerializer getCompleto = SerializerUtil.create(
			"id",
			"nome",
			"codigo",
			"geo",
			"municipio.id",
			"municipio.nome",
			"municipio.estado.nome",
			"municipio.estado.codigo",
			"municipio.aptoLicenciamento",
			"municipio.atividadesNaoAptas",
			"dataCriacao",
			"areaImovel",
			"areaAPP",
			"areaReservaLegal",
			"areaUsoAlternativoSolo")
			.transform(new GeometryTransformer(), Geometry.class)
			.exclude("*");

}


