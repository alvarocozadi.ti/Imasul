package serializers;

import flexjson.JSONSerializer;
import utils.SerializerUtil;

public class AtividadeSerializer {
	
	public static JSONSerializer findAtividade = SerializerUtil.create(
			"id",
			"nome",
			"codigo",
			"tipologia.nome",
			"siglaSetor",
			"dentroEmpreendimento",
			"dentroMunicipio",
			"tiposAtividade.codigo",
			"tiposAtividade.nome",
			"atividadesCnae.id",
			"atividadesCnae.nome",
			"atividadesCnae.codigo",
			"parametros.id",
			"parametros.nome",
			"parametros.codigo",
			"parametros.casasDecimais",
			"parametros.descricao",
			"temLinha",
			"temPonto",
			"temPoligono",
			"licenciamentoMunicipal",
			"limiteParametroMunicipal",
			"limiteInferiorLicenciamentoSimplificado",
			"limiteSuperiorLicenciamentoSimplificado",
			"tiposLicenca.id",
			"grupoDocumento.id",
			"limites.limiteInferior",
			"limites.limiteSuperior",
			"limites.parametroAtividade.codigo",
			"limites.parametroAtividade.id",
			"tipoEstudo"

	);

	public static JSONSerializer findPerguntas = SerializerUtil.create(
			"id",
			"texto",
			"codigo",
			"ordem",
			"tipoPergunta",
			"respostas.id",
			"respostas.texto",
			"respostas.tipoValidacao",
			"respostas.permiteLicenciamento"
	);
}
