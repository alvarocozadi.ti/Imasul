package jobs;

import models.caracterizacao.DaeLicenciamento;
import play.Logger;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import utils.Configuracoes;

import java.util.ArrayList;
import java.util.List;

import static play.db.jpa.JPA.em;

/**
 * Job que verifica o pagamento de DAEs".
 */
//@On("cron.processamentoDaes")
//@OnApplicationStart
public class ProcessamentoDaeLicenciamento extends GenericJob {

	@Override
	public void executar() {

		Logger.info("ProcessamentoDaeLicenciamento:: Iniciando job");

		List<DaeLicenciamento> daesPendente = DaeLicenciamento.findByStatus(DaeLicenciamento.Status.EMITIDO);
		List<DaeLicenciamento> daesVencidoAguardandoPagamento = DaeLicenciamento.findByStatus(DaeLicenciamento.Status.VENCIDO_AGUARDANDO_PAGAMENTO);

		List<DaeLicenciamento> daesParaBuscar = new ArrayList<>();
		daesParaBuscar.addAll(daesPendente);
		daesParaBuscar.addAll(daesVencidoAguardandoPagamento);

		for(DaeLicenciamento daeLicenciamento : daesParaBuscar) {

			processarPagamentoDae(daeLicenciamento);
		}

		Logger.info("ProcessamentoDaeLicenciamento:: Encerrando job");
	}

	private void processarPagamentoDae(DaeLicenciamento daeLicenciamento) {

		try {

			Logger.info("ProcessamentoDaeLicenciamento:: Processando DAE id " + daeLicenciamento.id +
					" - Status DAE: " + daeLicenciamento.status.name() + " - Status caracterização: " +
					daeLicenciamento.caracterizacao.status.nome + " id caracterizacao :" + daeLicenciamento.caracterizacao.id);

			if(Configuracoes.VERIFICAR_PAGAMENTO_GESTAO_PAGAMENTO) {
				Logger.info("=== PAGAMENTO DAE EMITIDAS - VERIFICA SE PAGAMENTO FOI REALIZADO NO GESTAO PAGAMENTOS === ");
				daeLicenciamento.processarPagamento();
			} else {
				Logger.info("=== PAGAMENTO DAE EMITIDAS - SEM VERIFICACAO NO GESTAO PAGAMENTOS === ");
				daeLicenciamento.processaPagamentoSemVerificarGestaoPagamentos();
			}

			commitTransaction();

			Logger.info("ProcessamentoDaeLicenciamento:: Finalizado DAE id " + daeLicenciamento.id + " - Status DAE: " + daeLicenciamento.status.name() + " - Status caracterização: " + daeLicenciamento.caracterizacao.status.nome);

		} catch (Exception e) {

			Logger.error(e, e.getMessage());
			rollbackTransaction();
		}

	}

}
