package jobs;

import models.caracterizacao.Caracterizacao;
import models.caracterizacao.StatusCaracterizacao;
import models.integracaoPagamentos.PagamentosWS;
import play.Logger;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import utils.Configuracoes;

import java.util.List;

/**
 * Job que verifica o status das Guias".
 */
@On("cron.processamentoGuia")
@OnApplicationStart
public class ProcessamentoGuia extends GenericJob{

	public String situacao;

	public void executar() {

		Logger.info("ProcessamentoGuia:: Iniciando job");

		if(Configuracoes.VERIFICAR_PAGAMENTO_SIRIEMA_PAGAMENTO){
			processarComApiPagamentos();
		}else{
			processarSemApiPagamentos();
		}

		Logger.info("ProcessamentoGuia:: Encerrando job");
	}

	public void processarSemApiPagamentos(){

		List<Caracterizacao> caracterizacoes = Caracterizacao.getCaracterizacaoComGuiaEmitida();

		Logger.info("ProcessamentoGuia:: Quantidade de Guias a serem processadas: " + caracterizacoes.size());
		caracterizacoes.forEach(caracterizacao -> {

			caracterizacao.status = StatusCaracterizacao.findById(StatusCaracterizacao.EM_ANALISE);
			Logger.info("ProcessamentoGuia:: Guia : " + caracterizacao.codigoGuia + " mudou seu status para PAGO");
			caracterizacao.save();
		});
	}

	public void processarComApiPagamentos(){

		List<Caracterizacao> caracterizacoes = Caracterizacao.getCaracterizacaoComGuiaEmitida();

		Logger.info("ProcessamentoGuia:: Quantidade de Guias a serem processadas: " + caracterizacoes.size());

		caracterizacoes.forEach(caracterizacao -> {

			situacao = PagamentosWS.situacaoBoleto(caracterizacao.codigoGuia).situacaoBoleto;

			if(!situacao.equals("EMITIDO")) {

				if(situacao.equals("PAGO")) {
					caracterizacao.status = StatusCaracterizacao.findById(StatusCaracterizacao.EM_ANALISE);
					Logger.info("ProcessamentoGuia:: Guia : " + caracterizacao.codigoGuia + " mudou seu status para PAGO");

				} else {
					caracterizacao.status = StatusCaracterizacao.findById(StatusCaracterizacao.GUIA_VENCIDA);
					Logger.info("ProcessamentoGuia:: Guia : " + caracterizacao.codigoGuia + " mudou seu status para VENCIDA");
				}
				caracterizacao.save();
			}
		});

	}

}