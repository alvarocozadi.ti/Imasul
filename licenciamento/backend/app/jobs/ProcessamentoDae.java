package jobs;

import models.caracterizacao.Dae;
import play.Logger;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import utils.Configuracoes;

import java.util.ArrayList;
import java.util.List;

/**
 * Job que verifica o pagamento de DAEs".
 */
//@On("cron.processamentoDaes")
//@OnApplicationStart
public class ProcessamentoDae extends GenericJob {

	@Override
	public void executar() {

		Logger.info("ProcessamentoDae:: Iniciando job");

		List<Dae> daesPendente = Dae.findByStatus(Dae.Status.EMITIDO);
		List<Dae> daesVencidoAguardandoPagamento = Dae.findByStatus(Dae.Status.VENCIDO_AGUARDANDO_PAGAMENTO);

		List<Dae> daesParaBuscar = new ArrayList<>();
		daesParaBuscar.addAll(daesPendente);
		daesParaBuscar.addAll(daesVencidoAguardandoPagamento);

		for(Dae dae : daesParaBuscar) {

			processarPagamentoDae(dae);
		}

		Logger.info("ProcessamentoDae:: Encerrando job");
	}

	private void processarPagamentoDae(Dae dae) {

		try {

			Logger.info("ProcessamentoDae:: Processando DAE id " + dae.id + " - Status DAE: " + dae.status.name() + " - Status caracterização: " + dae.caracterizacao.status.nome);

			if(Configuracoes.VERIFICAR_PAGAMENTO_GESTAO_PAGAMENTO) {
				Logger.info("=== PAGAMENTO DAE EMITIDAS - VERIFICA SE PAGAMENTO FOI REALIZADO NO GESTAO PAGAMENTOS === ");
				dae.processarPagamento();
			} else {
				Logger.info("=== PAGAMENTO DAE EMITIDAS - SEM VERIFICACAO NO GESTAO PAGAMENTOS === ");
				dae.processaPagamentoSemVerificarGestaoPagamentos();
			}

			commitTransaction();

			Logger.info("ProcessamentoDae:: Finalizado DAE id " + dae.id + " - Status DAE: " + dae.status.name() + " - Status caracterização: " + dae.caracterizacao.status.nome);

		} catch (Exception e) {

			Logger.error(e, e.getMessage());
			rollbackTransaction();
		}

	}

}
