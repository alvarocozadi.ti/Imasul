package jobs;

import models.caracterizacao.Uferms;
import models.integracaoPagamentos.PagamentosWS;
import models.integracaoPagamentos.UfermsVO;
import play.Logger;
import play.jobs.On;
import play.jobs.OnApplicationStart;

import java.util.Date;

/**
 * Job que verifica o valor da UFERMS".
 */
//@On("cron.processamentoUferms")
//@OnApplicationStart
public class ProcessamentoUferms extends GenericJob{

    public void executar(){

        Logger.info("ProcessamentoValorUferms:: Iniciando job");

        Date data = new Date();
        UfermsVO ufermsVO = PagamentosWS.gerarUferms(data);

        if(ufermsVO != null) {
            Double valorNoSistema = Uferms.get();


            if (ufermsVO.valor != valorNoSistema) {

                Uferms.set(ufermsVO.valor);
                Logger.info("ProcessamentoValorUferms:: Valor da UFERMS atualizado para: " + ufermsVO.valor);

            } else {
                Logger.info("ProcessamentoValorUferms:: Valor da UFERMS não difere da base do Siriema, valor atual: " + ufermsVO.valor);
            }

            Logger.info("ProcessamentoValorUferms:: Encerrando job");
        }
    }
}