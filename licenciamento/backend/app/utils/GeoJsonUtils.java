package utils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import com.vividsolutions.jts.geom.Polygon;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;

import com.vividsolutions.jts.geom.Geometry;
import org.opengis.feature.Feature;

public class GeoJsonUtils {

	public static final Integer SRID = 4674;

	public static final Integer PRECISION = 15;

	public static Geometry toGeometry(String geoJson) {

		try {

			GeometryJSON gjson = new GeometryJSON();
			Geometry geometry = gjson.read(geoJson);
			geometry.setSRID(SRID);

			return geometry;

		} catch (IOException e) {

			throw new RuntimeException(e);

		}

	}

	public static Geometry toGeometryPolygon(String geoJson) {

		try {

			Reader reader = new StringReader(geoJson);
			GeometryJSON gjson = new GeometryJSON();

			Polygon polygon = gjson.readPolygon(reader);
			polygon.setSRID(SRID);

			return polygon;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public static String toGeoJson(Geometry geometry){

		GeometryJSON gjson = new GeometryJSON(PRECISION);
		return gjson.toString(geometry);

	}

}
