package models;

import play.db.jpa.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "licenciamento", name = "distancia_capital")
public class DistanciaCapital extends GenericModel {

    @Id
    @Column(name="codigo_ibge")
    public String codigoIbge;

    @Column(name="nome_municipio")
    public String nomeMunicipio;

    @Column(name="distancia")
    public Double distancia;

    public static DistanciaCapital findDistanciaByCodigoIbge(Long codigoIbge){

        return find("codigo_ibge = :codigoIbge")
                .setParameter("codigoIbge", codigoIbge)
                .first();

    }

}
