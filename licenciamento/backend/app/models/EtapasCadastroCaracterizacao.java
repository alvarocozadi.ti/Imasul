package models;

public enum EtapasCadastroCaracterizacao {

    ATIVIDADE(1),
    LOCALIZACAO(2),
    CONDICOES(3),
    DOCUMENTACAO(4),
    ENQUADRAMENTO(5);

    public Long id;

    EtapasCadastroCaracterizacao(long id) {

        this.id = id;
    }
}
