package models.tokenSiriema;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import exceptions.WebServiceException;
import play.Play;
import play.libs.WS;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SiriemaWS {

    public static final String SIRIEMA_CLIENT_ID = Play.configuration.getProperty("siriema.client.id");
    public static final String SIRIEMA_CLIENT_SECRET = Play.configuration.getProperty("siriema.client.secret");
    public static final String SIRIEMA_TOKEN_AUTENTICACAO = Play.configuration.getProperty("siriema.url.token.autenticacao");
    public static final String SIRIEMA_GRANT_TYPE = Play.configuration.getProperty("siriema.grant.type");

    private static final GsonBuilder gsonBuilder = new GsonBuilder();

    private static String stagedToken;
    private static Date expirationDate;

    public static String getToken() {

        Date now = new Date();

        if(expirationDate == null || now.after(expirationDate) || stagedToken == null || stagedToken.isEmpty()){

            SiriemaResponseTokenVO token = gerarTokenAcesso();
            long expirationMilliseconds = (Integer.toUnsignedLong(token.expires_in) - 100L) * 1000;

            stagedToken = token.access_token;
            expirationDate = new Date(now.getTime() + expirationMilliseconds);

        }

        return stagedToken;

    }

    private static SiriemaResponseTokenVO gerarTokenAcesso(){

        Map<String, String> paramsAuth = new HashMap<>();

        paramsAuth.put("grant_type", SIRIEMA_GRANT_TYPE);
        paramsAuth.put("client_id", SIRIEMA_CLIENT_ID);
        paramsAuth.put("client_secret", SIRIEMA_CLIENT_SECRET);

        WS.WSRequest request = WS.url(SIRIEMA_TOKEN_AUTENTICACAO);

        request.setHeader("Content-Type","application/x-www-form-urlencoded");
        request.setHeader("Accept","application/json");
        request.setParameters(paramsAuth);

        WS.HttpResponse responseAuth = request.post();

        if (!responseAuth.success())
            throw new WebServiceException(responseAuth);

        Type type = new TypeToken<SiriemaResponseTokenVO>(){}.getType();

        return gsonBuilder.create().fromJson(responseAuth.getJson(), type);

    }

}