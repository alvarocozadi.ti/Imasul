package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Id;

import br.ufla.lemaf.beans.pessoa.Pais;
import br.ufla.lemaf.beans.pessoa.TipoEndereco;
import br.ufla.lemaf.beans.pessoa.ZonaLocalizacao;


import static utils.Configuracoes.*;


public class Endereco extends br.ufla.lemaf.beans.pessoa.Endereco {

	public static Endereco getByCorrespondencia(Collection<Endereco> enderecos, boolean correspondencia) {

		if (enderecos == null)
			return null;

		Integer idTipoEndereco = correspondencia ? ID_TIPO_ENDERECO_CORRESPONDENCIA : ID_TIPO_ENDERECO_PRINCIPAL;

		return enderecos.stream().filter(e -> e.tipo.id.equals(idTipoEndereco)).findFirst().orElse(null);

	}

	public String getDescricao() {

		return getDescricao(Integer.MAX_VALUE, true);
	}

	public String getDescricao(int max) {

		return getDescricao(max, true);
	}

	public String getDescricao(Boolean comCep) {

		return getDescricao(Integer.MAX_VALUE, comCep);
	}

	private String getDescricao(int max, Boolean comCep) {


		return "";
	}
}
