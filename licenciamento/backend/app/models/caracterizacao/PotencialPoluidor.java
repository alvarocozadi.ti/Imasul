package models.caracterizacao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import play.db.jpa.Model;


@Entity
@Table(schema = "licenciamento", name = "potencial_poluidor")

public class PotencialPoluidor extends Model{

	@Column(name = "nome")
	public String nome;

	@Column(name = "valor")
	public Long valorPpd;

	
}
