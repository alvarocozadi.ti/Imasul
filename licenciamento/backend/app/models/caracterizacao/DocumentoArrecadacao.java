package models.caracterizacao;

import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema = "licenciamento", name = "documento_arrecadacao")
public class DocumentoArrecadacao extends GenericModel {

	@Id
	@Required
	@Column(name = "id_documento_arrecadacao")
	public Integer idDocumentoArrecadacao;

	@Required
	@ManyToOne
	@JoinColumn(name = "id_dae", referencedColumnName="id")
	public Dae dae;

}
