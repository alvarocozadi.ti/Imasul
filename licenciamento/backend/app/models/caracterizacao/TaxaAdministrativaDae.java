package models.caracterizacao;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import play.db.jpa.Model;

@Entity
@Table(schema = "licenciamento", name = "taxa_administrativa_dae")
public class TaxaAdministrativaDae extends Model {

	public Integer ano;
	
	public Double valor;
	
	@Column(name="link_taxas_licenciamento")
	public String linkTaxasLicenciamento;
	
	public static TaxaAdministrativaDae getTaxaAtual() {
		
		int ano = Calendar.getInstance().get(Calendar.YEAR);
		
		TaxaAdministrativaDae taxa = find("ano = :ano")
				.setParameter("ano", ano)
				.first();
		
		if (taxa == null) {
			
			throw new RuntimeException("O valor de taxa administrativa do DAE "
					+ "para o ano de " + ano + " não está configurado.");
		}
		
		return taxa;
	}
	
	public static Double findValorAtual() {
		
		TaxaAdministrativaDae taxa = getTaxaAtual();
		
		return taxa.valor;
	}
}
