package models.caracterizacao;

import play.db.jpa.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "licenciamento", name = "variavel_calculo_taxa")
public class VariavelCalculoTaxa extends GenericModel {

    @Id
    @Column(name = "id")
    public Long id;

    @Column(name = "nome")
    public String nome;

    @Column(name = "sigla")
    public String sigla;
}
