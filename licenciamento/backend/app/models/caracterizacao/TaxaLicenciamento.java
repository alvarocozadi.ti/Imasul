package models.caracterizacao;

import beans.VariaveisTaxaVO;
import play.db.jpa.GenericModel;
import javax.persistence.*;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Entity
@Table(schema = "licenciamento", name = "taxa_licenciamento")
public class TaxaLicenciamento extends GenericModel {

	@Id
	@Column(name = "id")
	public Long id;

	@Column(name = "tipo_estudo")
	public String tipoEstudo;

	@ManyToOne
	@JoinColumn(name="id_porte_empreendimento")
	public PorteEmpreendimento porteEmpreendimento;

	@ManyToOne
	@JoinColumn(name="id_variavel_calculo")
	public VariavelCalculoTaxa variavelCalculoTaxa;

	@Column(name = "valor")
	public String valor;

	public static List<TaxaLicenciamento> findVariaveis(AtividadeCaracterizacao atividadeCaracterizacao){
		return TaxaLicenciamento.find("tipo_estudo = :tipoEstudo AND id_porte_empreendimento = :porteEmpreendimento")
				.setParameter("tipoEstudo",atividadeCaracterizacao.atividade.tipoEstudo)
				.setParameter("porteEmpreendimento",atividadeCaracterizacao.porteEmpreendimento.id).fetch();
	}

	public static Double getTaxaLicenca(Caracterizacao caracterizacao) {

		caracterizacao.atividadesCaracterizacao.forEach(atividadeCaracterizacao -> {

			List<TaxaLicenciamento> variaveisCalculoTaxa = TaxaLicenciamento.findVariaveis(atividadeCaracterizacao);

			VariaveisTaxaVO variaveisTaxaVO = new VariaveisTaxaVO(variaveisCalculoTaxa, atividadeCaracterizacao, caracterizacao);

			atividadeCaracterizacao.vistoria = calcularVistoria(variaveisTaxaVO);
			atividadeCaracterizacao.variaveisTaxaVO = variaveisTaxaVO;

		});

		AtividadeCaracterizacao atividade = caracterizacao.atividadesCaracterizacao.stream().max(Comparator.comparing(atividadeCaracterizacao -> atividadeCaracterizacao.vistoria)).get();

		caracterizacao.atividadesCaracterizacao.forEach(atividadeCaracterizacao -> {

			if(!atividadeCaracterizacao.equals(atividade))
				atividadeCaracterizacao.vistoria = 0.0;

			ArrayList tiposLicenca = new ArrayList(Arrays.asList("LI", "LO", "RLI", "RLO"));
			boolean reduzir = false;

			if(atividadeCaracterizacao.atividade.tipoEstudo.equals("EIA-RIMA") && tiposLicenca.contains(caracterizacao.tiposLicencaEmAndamento.get(0).sigla))
				reduzir = true;

			atividadeCaracterizacao.valorTaxaLicenciamento = calcularTaxaLicenciamentoPorAtividade(atividadeCaracterizacao.variaveisTaxaVO, atividadeCaracterizacao.vistoria, atividadeCaracterizacao.atividade.potencialPoluidor.valorPpd, reduzir);
			caracterizacao.valorTotalTaxaLicenciamento += atividadeCaracterizacao.valorTaxaLicenciamento;
		});

		return caracterizacao.valorTotalTaxaLicenciamento + TaxaPublicacao.get();
	}

	public static Double calcular(Caracterizacao caracterizacao, TipoLicenca tipoLicenca) throws ScriptException {

		Double taxaLicenca;

		if(caracterizacao.valorTotalTaxaLicenciamento > 0){
			taxaLicenca = caracterizacao.valorTotalTaxaLicenciamento;
		}else {
			taxaLicenca = getTaxaLicenca(caracterizacao);
		}

//		if(caracterizacao.valorTaxaAdministrativa == 0.00){
//			//caracterizacao.valorTaxaAdministrativa = TaxaAdministrativaDae.findValorAtual();
//		}

		return taxaLicenca; //+ caracterizacao.valorTaxaAdministrativa;

	}

	public static Double calcularVistoria(VariaveisTaxaVO variaveisTaxaVO){

		return  (variaveisTaxaVO.numeroServidores * variaveisTaxaVO.numeroDiasVistoria * variaveisTaxaVO.custosDespesasViagem) +
				(variaveisTaxaVO.numeroVeiculosUsados * variaveisTaxaVO.numeroKmRodados * variaveisTaxaVO.custoKmRodado);

	}

	public static Double calcularTaxaLicenciamentoPorAtividade(VariaveisTaxaVO variaveisTaxaVO, Double vistoria, Long valorPpd, Boolean reduzir){

		Double servicoTecnico = variaveisTaxaVO.numeroServidores * variaveisTaxaVO.numeroHoras * variaveisTaxaVO.custoHoraTecnica;
		Double custoAdministrativoVistoria = 0.1 * (servicoTecnico + vistoria);
		Double custoTotal = (servicoTecnico + vistoria + custoAdministrativoVistoria) * valorPpd;

		if(reduzir)
			custoTotal = custoTotal * 0.22;

		return  custoTotal;

	}

}
