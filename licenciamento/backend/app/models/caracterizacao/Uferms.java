package models.caracterizacao;

import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema = "licenciamento", name = "uferms")
public class Uferms extends GenericModel {

	@Id
	public Integer id;

	@Column(name = "valor")
	public Double valor;

	public static Double get() {

		Uferms uferms = findById(1);
		return uferms.valor;

	}

	public static void set(Double valor) {

		Uferms uferms = findById(1);
		uferms.delete();

		uferms.valor = valor;
		uferms.id = 1;
		uferms.save();
	}
}
