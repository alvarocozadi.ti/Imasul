package models.caracterizacao;


public enum Variaveis {

    NUMERO_TECNICOS("Número de técnicos envolvidos (Jurídico e Técnico)",   "T1"),
    NUMERO_SERVIDORES("Número de servidores (Motoristas e Técnico)",        "T2"),
    NUMERO_HORAS("Número de horas de análise/técnico",                      "H"),
    CUSTO_HORA("Custo hora técnica (2 UFERMS)",                             "CH"),
    NUMERO_DIAS("Número de dias em vistoria",                               "D"),
    CUSTO_DESPESAS("Custo despesas viagem (5 UFERMS)",                      "CD"),
    NUMERO_VEICULOS("Número de veículos na vistoria",                       "V"),
    NUMERO_KM_RODADOS("Número de Km rodados",                               "R"),
    CUSTO_KM("Custo do Km rodado",                                          "CK");


    public String nome;
    public String sigla;

    Variaveis(String nome, String sigla) {

        this.nome = nome;
        this.sigla = sigla;
    }
}
