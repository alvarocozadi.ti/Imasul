package models.caracterizacao;

import play.db.jpa.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "licenciamento", name = "taxa_publicacao")
public class TaxaPublicacao extends GenericModel {

    @Id
    public Integer id;

    @Column(name = "valor")
    public Double valor;

    public static Double get() {

        TaxaPublicacao taxaPublicacao = findById(1);
        return taxaPublicacao.valor;
    }
}
