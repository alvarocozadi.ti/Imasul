package models.caracterizacao;

import models.AtividadeParametroLimites;
import play.db.jpa.GenericModel;
import utils.ListUtil;
import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(schema = "licenciamento", name = "atividade")
public class Atividade extends GenericModel {

	@Id
	public Long id;

	public String nome;

	@ManyToOne
	@JoinColumn(name="id_tipologia")
	public Tipologia tipologia;

	@Column(name = "geo_linha")
	public Boolean temLinha;

	@Column(name = "geo_ponto")
	public Boolean temPonto;

	@Column(name = "geo_poligono")
	public Boolean temPoligono;

	@Column(name = "ativo")
	public Boolean ativo;

	@Column(name = "v1")
	public Boolean v1;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_tipo_licenca_caracterizacao_andamento",
			joinColumns = @JoinColumn(name = "id_caracterizacao"),
			inverseJoinColumns = @JoinColumn(name = "id_tipo_licenca"))
	public List<TipoLicenca> tiposLicencaEmAndamento;

//	@ManyToMany
//	@OrderBy("ordem ASC")
//	@JoinTable(schema = "licenciamento", name = "rel_atividade_pergunta",
//			joinColumns = @JoinColumn(name = "id_atividade"),
//			inverseJoinColumns = @JoinColumn(name = "id_pergunta"))
//	public List<Pergunta> perguntas;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_atividade_tipo_atividade",
			joinColumns = @JoinColumn(name = "id_atividade"),
			inverseJoinColumns = @JoinColumn(name = "id_tipo_atividade"))
	public List<TipoAtividade> tiposAtividade;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_atividade_porte_atividade",
			joinColumns = @JoinColumn(name = "id_atividade"),
			inverseJoinColumns = @JoinColumn(name = "id_porte_atividade"))
	public List<PorteAtividade> portesAtividade;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_atividade_taxa_licenciamento",
			joinColumns = @JoinColumn(name = "id_atividade"),
			inverseJoinColumns = @JoinColumn(name = "id_taxa_licenciamento"))
	public List<TaxaLicenciamento> taxasLicenca;

	public String codigo;

	@Column(name = "licenciamento_municipal")
	public Boolean licenciamentoMunicipal;

	@Column(name = "limite_parametro_municipal")
	public Double limiteParametroMunicipal;

	@Column(name = "limite_inferior_simplificado")
	public Double limiteInferiorLicenciamentoSimplificado;

	@Column(name = "limite_superior_simplificado")
	public Double limiteSuperiorLicenciamentoSimplificado;

	@Column(name = "observacoes")
	public String observacoes;

	@Column(name = "sigla_setor")
	public String siglaSetor;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_atividade_parametro_atividade",
			joinColumns = @JoinColumn(name = "id_atividade"),
			inverseJoinColumns = @JoinColumn(name = "id_parametro_atividade"))
	public List<ParametroAtividade> parametros;

	@ManyToOne
	@JoinColumn(name="id_potencial_poluidor")
	public PotencialPoluidor potencialPoluidor;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_atividade_tipo_licenca",
			joinColumns = @JoinColumn(name = "id_atividade"),
			inverseJoinColumns = @JoinColumn(name = "id_tipo_licenca"))
	public List<TipoLicenca> tiposLicenca;

	@OneToMany(mappedBy = "atividade")
	public List<TipoCaracterizacaoAtividade> tiposCaracterizacaoAtividade;

	@OneToMany(mappedBy = "atividade")
	public List<AtividadeParametroLimites> limites;

	@Column(name = "dentro_empreendimento")
	public Boolean dentroEmpreendimento;

	@Column(name = "dentro_municipio")
	public Boolean dentroMunicipio;

	@Column(name = "tipo_estudo")
	public String tipoEstudo;

	@ElementCollection
	@CollectionTable(
			schema = "licenciamento", name = "rel_atividade_parametro_atividade",
			joinColumns = @JoinColumn(name = "id_atividade")
	)
	@MapKeyJoinColumn(name = "id_parametro_atividade", updatable = false, insertable = false)
	public Map<ParametroAtividade, RelAtividadeParametrosAtividade> parametrosAtividadeComIdEDescricaoMap;

	@ManyToOne
	@JoinColumn(name="id_grupo_documento")
	public GrupoDocumento grupoDocumento;

	public static List<Atividade> findAtividades(TipoCaracterizacaoAtividade.FiltroAtividade filtro) {

		String jpql = "SELECT DISTINCT atividade " +
				"FROM " + Atividade.class.getSimpleName() + " atividade " +
				"   JOIN atividade.tiposCaracterizacaoAtividade tiposCaracterizacaoAtividade " +
				"   JOIN atividade.tiposAtividade tiposAtividade " +
				"	LEFT JOIN atividade.limites " +
				(filtro.licenciamentoSimplificado != null ? " JOIN atividade.tiposLicenca tiposLicenca " : " ") +
				"WHERE ";

		if(filtro.dispensaLicenciamento != null) {
			jpql += " (tiposCaracterizacaoAtividade.dispensaLicenciamento = :dispensaLicenciamento) ";
		}

		else if(filtro.licenciamentoSimplificado != null) {

			jpql += " (tiposCaracterizacaoAtividade.licenciamentoSimplificado = :licenciamentoSimplificado " +
					"OR tiposCaracterizacaoAtividade.licenciamentoDeclaratorio = :licenciamentoDeclaratorio) ";
		}

		if(filtro.idTipologia != null) {
			jpql += " AND (atividade.tipologia.id = :idTipologia) " ;
		}

		if (filtro.tipoEndereco == 0){
			jpql+= " AND (tiposAtividade.codigo = 'URBANA') ";
		}
		else{
			jpql+= " AND (tiposAtividade.codigo = 'RURAL') ";
		}

		jpql+= " AND v1=true ";

		JPAQuery jpaQuery = Atividade.find(jpql);

		if(filtro.dispensaLicenciamento != null) {

			jpaQuery.setParameter("dispensaLicenciamento", filtro.dispensaLicenciamento);
		}

		else if(filtro.licenciamentoSimplificado != null) {

			jpaQuery.setParameter("licenciamentoSimplificado", filtro.licenciamentoSimplificado);
			jpaQuery.setParameter("licenciamentoDeclaratorio", filtro.licenciamentoSimplificado);
		}

		if(filtro.idTipologia != null) {

 			jpaQuery.setParameter("idTipologia", filtro.idTipologia);
		}

		if(filtro.listaCodigosCnaes != null) {

			jpaQuery.setParameter("listaCodigosCnae", filtro.listaCodigosCnaes);
		}

		List<Atividade> atividades = jpaQuery.fetch();

		atividades.forEach(atividade -> {

			atividade.parametros = atividade.parametros.stream().map(p ->
					new ParametroAtividade(p, atividade.parametrosAtividadeComIdEDescricaoMap.get(p).descricaoUnidade)
			).collect(Collectors.toList());

		});

		return atividades;
	}

	public static List<TipoCaracterizacaoAtividade> findByAtividadeCaracterizacao(AtividadeCaracterizacao atividade) {

		return find("atividade.id = :idAtividade")
				.setParameter("idAtividade", atividade.atividade.id)
				.fetch();
	}

	public boolean containsTipoLicenca(Long idTipoLicenca) {

		if(this.tiposLicenca == null) {
			this.tiposLicenca = ((Atividade)findById(this.id)).tiposLicenca;
		}

		return this.tiposLicenca.stream().anyMatch(tl -> tl.id.equals(idTipoLicenca));
	}

	@Embeddable
	public class RelAtividadeParametrosAtividade {
		@Column(name = "descricao_unidade", updatable = false, insertable = false)
		public String descricaoUnidade;
	}

//	public Boolean containsPergunta(Pergunta pergunta){
//		return this.perguntas.contains(pergunta);
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Atividade)) return false;
		if (!super.equals(o)) return false;
		Atividade atividade = (Atividade) o;
		return id.equals(atividade.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), id, nome, siglaSetor);
	}
}
