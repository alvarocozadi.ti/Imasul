package models.caracterizacao;

import beans.DadosSobreposicaoVO;
import beans.VariaveisTaxaVO;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import exceptions.AppException;
import exceptions.ValidacaoException;
import models.*;
import play.db.jpa.GenericModel;
import utils.GeoJsonUtils;
import utils.Mensagem;
import utils.WebServiceEntradaUnica;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(schema = "licenciamento", name = "atividade_caracterizacao")
public class AtividadeCaracterizacao extends GenericModel implements GuardaSobreposicao{

	private static final String SEQ = "licenciamento.atividade_caracterizacao_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ)
	@SequenceGenerator(name = SEQ, sequenceName = SEQ, allocationSize = 1)
	public Long id;

	@ManyToOne
	@JoinColumn(name="id_porte_empreendimento")
	public PorteEmpreendimento porteEmpreendimento;

	@ManyToOne
	@JoinColumn(name="id_porte_empreendimento_para_calculo_do_porte")
	public PorteEmpreendimento porteEmpreendimentoParaCalculoDoPorte;

	@ManyToOne
	@JoinColumn(name="id_atividade")
	public Atividade atividade;


	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<GeometriaAtividade> geometriasAtividade;

	@ManyToOne
	@JoinColumn(name = "id_caracterizacao", referencedColumnName = "id")
	public Caracterizacao caracterizacao;
	
	@OrderBy("id ASC")
	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<AtividadeCaracterizacaoParametros> atividadeCaracterizacaoParametros;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<SobreposicaoCaracterizacaoAtividade> sobreposicaoCaracterizacaoAtividades;

	@Column(name = "is_principal")
	public Boolean isPrincipal;

	@Column(name = "distancia_capital")
	public Double distanciaCapital;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<SolicitacaoGrupoDocumento> solicitacoesGruposDocumentos;

	@Column(name="valor_taxa_licenciamento")
	public Double valorTaxaLicenciamento;

	@Transient
	public VariaveisTaxaVO variaveisTaxaVO;

	@Transient
	public Double vistoria;

	public static TipoCaracterizacao getEnquadramento(List<AtividadeCaracterizacao> atividadesCaracterizacao) {

		List<TipoCaracterizacaoAtividade> tipos = new ArrayList<>();

		atividadesCaracterizacao.stream().map(TipoCaracterizacaoAtividade::findByAtividadeCaracterizacao)
				.forEach(tipos::addAll);

		if (TipoCaracterizacaoAtividade.isSimplificado(tipos)) {
			return TipoCaracterizacao.findById(TipoCaracterizacao.SIMPLIFICADO);
		}

		if (TipoCaracterizacaoAtividade.isDispensaLicenciamento(tipos)) {
			return TipoCaracterizacao.findById(TipoCaracterizacao.DISPENSA);
		}

		throw new AppException(Mensagem.TIPOS_CARACTERIZACAOES_DIFERENTES);
	}

	/*
	 * Valida se a atividade selecionada pertence ao tipo de licença selecionado
	 */
	public void validarTipoLicenca(TipoLicenca tipoLicenca) {

		List<TipoCaracterizacaoAtividade> tipos = TipoCaracterizacaoAtividade.findByAtividadeCaracterizacao(this);

		if(tipoLicenca != null && tipoLicenca.id.equals(TipoLicenca.DISPENSA_INEXIGIBILIDADE)) {

			if(!TipoCaracterizacaoAtividade.isDispensaLicenciamento(tipos)) {

				throw new ValidacaoException(Mensagem.CARACTERIZACAO_ATIVIDADE_NAO_PERTENCE_TIPO_LICENCA);
			}

		} else {

			if (!TipoCaracterizacaoAtividade.isSimplificado(tipos) && !TipoCaracterizacaoAtividade.isDeclaratorio(tipos)) {

				throw new ValidacaoException(Mensagem.CARACTERIZACAO_ATIVIDADE_NAO_PERTENCE_TIPO_LICENCA);
			}
		}

	}

	/**
	 * Gera uma cópia da atividade caracterização, duplicando todas as informações
	 * declaradas pelo cadastrante. Utilizado quando duplicar a caracterização.
	 */
	public AtividadeCaracterizacao gerarCopia() {

		AtividadeCaracterizacao copia = new AtividadeCaracterizacao();
		copia.atividade = this.atividade;
		copia.porteEmpreendimento = this.porteEmpreendimento;
		copia.atividadeCaracterizacaoParametros = this.atividadeCaracterizacaoParametros;
		copia.geometriasAtividade = new ArrayList<>();


		if (this.geometriasAtividade != null) {

			for (GeometriaAtividade geo : this.geometriasAtividade) {

				GeometriaAtividade copiaGeo = geo.gerarCopia();
				copiaGeo.atividadeCaracterizacao = copia;
				copia.geometriasAtividade.add(copiaGeo);
			}
		}

		return copia;
	}

	public Double getValorInvestido() {

		ParametroAtividade vdi = ParametroAtividade.find("codigo = :vdi").setParameter("vdi", "VDI").first();

		if(vdi == null) {
			return null;
		}

		Optional<AtividadeCaracterizacaoParametros> parametroVdi = this.atividadeCaracterizacaoParametros.stream().filter(acp -> acp.parametroAtividade.id.equals(vdi.id)).findFirst();

		if(parametroVdi.isPresent()) {
			return parametroVdi.get().valorParametro;
		}

		return 0d;

	}

	public PorteEmpreendimento getPorteParaCalculoDeTaxa() {

		if(this.porteEmpreendimento == null) {
			return null;
		}

		PorteEmpreendimento porte = null;
		br.ufla.lemaf.beans.Empreendimento empreendimento =  WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(this.caracterizacao.empreendimento.idEmpreendimentoEU);

		if(empreendimento.porte != null && (empreendimento.porte.equals("ME") || empreendimento.porte.equals("EPP"))){
			porte = PorteEmpreendimento.find("codigo = :codigo")
					.setParameter("codigo", "MICRO")
					.first();
		}
		else {
			porte = PorteEmpreendimento.find("codigo = :codigo")
					.setParameter("codigo", this.porteEmpreendimento.codigo)
					.first();
		}
		return porte;
	}

	public String getNomeAtividade(){
		return this.atividade != null ? this.atividade.nome : "";
	}

//	public Boolean containsPergunta(Pergunta pergunta){
//		return this.atividade.containsPergunta(pergunta);
//	}

	public Boolean atividadeDentroEmpreendimento(Atividade at){
		return this.atividade.dentroEmpreendimento.equals(at.dentroEmpreendimento);
	}

	public void zeraParametros(){
		this.atividadeCaracterizacaoParametros.forEach(acp -> acp.id = null);
	}

	@Override
	public List getListaSobreposicao() {
		return this.sobreposicaoCaracterizacaoAtividades;
	}

	@Override
	public void setListaSobreposicao(List sobreposicoes) {
		this.sobreposicaoCaracterizacaoAtividades = sobreposicoes;
	}

	@Override
	public <T extends SobreposicaoDistancia> T
	getObjetoSobreposicao(DadosSobreposicaoVO dadosSobreposicao, GuardaSobreposicao guardaSobreposicao, TipoSobreposicao tipoSobreposicao) {
		return (T) new SobreposicaoCaracterizacaoAtividade(tipoSobreposicao,(AtividadeCaracterizacao) guardaSobreposicao, dadosSobreposicao);
	}

	public static Double calcularDistanciaCapital(AtividadeCaracterizacao atividadeCaracterizacao ,Caracterizacao caracterizacao){

		Double distancia = null;

		if(atividadeCaracterizacao.atividade.dentroEmpreendimento || atividadeCaracterizacao.geometriasAtividade.isEmpty()){

			Empreendimento e = Empreendimento.findById(caracterizacao.empreendimento.id);
			distancia = DistanciaCapital.findDistanciaByCodigoIbge(e.municipio.id).distancia * 2;

		}else{

			Geometry geo = atividadeCaracterizacao.geometriasAtividade.get(0).geometria;
			Municipio municipio = AtividadeCaracterizacao.getMunicipioAtividadeForaEmpreendimento(geo);
			distancia = DistanciaCapital.findDistanciaByCodigoIbge(municipio.id).distancia * 2;

		}

		if(caracterizacao.empreendimento.localizacao == TipoLocalizacao.ZONA_RURAL){
			distancia = distancia + (distancia * 0.2);
		}

		return distancia;
	}

	public static Double calcularDistanciaCapitalComplexo(GeometriaComplexo geo, Caracterizacao caracterizacao){
		Double distancia = null;

		Municipio municipio = AtividadeCaracterizacao.getMunicipioAtividadeForaEmpreendimento(geo.geometria);
		distancia = DistanciaCapital.findDistanciaByCodigoIbge(municipio.id).distancia * 2;

		if(caracterizacao.empreendimento.localizacao == TipoLocalizacao.ZONA_RURAL){
		distancia = distancia + (distancia * 0.2);
	}

		return distancia;
	}

	public static Municipio getMunicipioAtividadeForaEmpreendimento(Geometry geo) {

		//Geometry geo = atividadeCaracterizacao.geometriasAtividade.get(0).geometria;
		Point centroide = geo.getCentroid();
		Municipio municipio =  Municipio.findByCoordenada(centroide.getX(),centroide.getY());

		return municipio;

	}

	public void preparaGrupoDocumentos(TipoLicenca tipoLicenca) {

		if(this.solicitacoesGruposDocumentos == null) {
			this.solicitacoesGruposDocumentos = new ArrayList<>();
		}

		List<TipoLicencaGrupoDocumento> grupoDocumentos = TipoLicencaGrupoDocumento
																.find("grupoDocumento.id = :idGrupoDocumento and tipoLicenca.id = :idTipoLicenca")
																.setParameter("idGrupoDocumento", atividade.grupoDocumento.id)
																.setParameter("idTipoLicenca", tipoLicenca.id).fetch();

		if (grupoDocumentos != null) {

			grupoDocumentos.forEach(tipoLicencaGrupoDocumento -> {

				GrupoDocumento umGrupoDocumento = tipoLicencaGrupoDocumento.grupoDocumento;
				TipoDocumento umTipoDocumento = tipoLicencaGrupoDocumento.tipoDocumento;
				Boolean obrigatorio = tipoLicencaGrupoDocumento.obrigatorio;

				Boolean alreadyExistent = this.solicitacoesGruposDocumentos.stream()
						.anyMatch(
								sgd -> (sgd.grupoDocumento != null && sgd.tipoDocumento != null) &&
								sgd.grupoDocumento.id.equals(tipoLicencaGrupoDocumento.grupoDocumento.id) &&
								sgd.tipoDocumento.id.equals(tipoLicencaGrupoDocumento.tipoDocumento.id)
							);

				if(!alreadyExistent){

					this.solicitacoesGruposDocumentos.add(
							new SolicitacaoGrupoDocumento(
									this,
									tipoLicencaGrupoDocumento.grupoDocumento,
									tipoLicencaGrupoDocumento.tipoDocumento,
									tipoLicencaGrupoDocumento.obrigatorio
							));

				}

			});

		}

	}
}
