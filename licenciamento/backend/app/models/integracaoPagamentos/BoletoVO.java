package models.integracaoPagamentos;

public class BoletoVO {

    public Long codigoBoleto;
    public String contribuinte;
    public String dados;
    public String dataEmissao;
    public String dataVencimento;
    public String empreendimento;
    public String numeroProcesso;
    public String observacao;
    public String situacaoBoleto;
    public String tipoArrecadacao;
    public int valorPago;
    public double valorReal;
    public double valorUferms;

}