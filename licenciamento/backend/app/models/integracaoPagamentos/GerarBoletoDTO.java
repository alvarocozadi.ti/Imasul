package models.integracaoPagamentos;

public class GerarBoletoDTO {

    public String contribuinte;
    public String tipoArrecadacao;
    public double valorUferms;
    public double valorReal;
    public String observacao;
    public String empreendimento;
    public String numeroProcesso;

}