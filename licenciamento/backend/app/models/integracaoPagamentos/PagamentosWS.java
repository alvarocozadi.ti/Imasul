package models.integracaoPagamentos;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import configuracao.deserializers.DateDeserializer;
import configuracao.serializers.DateSerializer;
import configuracao.services.RequestService;
import exceptions.WebServiceException;
import models.tokenSiriema.SiriemaWS;
import play.libs.WS;

import java.io.*;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.GsonBuilder;

import static utils.Configuracoes.API_PAGAMENTOS;

public class PagamentosWS extends RequestService {

    private static final Gson gson = (new GsonBuilder()).serializeSpecialFloatingPointValues().registerTypeAdapter(Date.class, new DateSerializer()).registerTypeAdapter(Date.class, new DateDeserializer()).create();

    private static final String FORMATO_DATA = "dd-MM-yyyy";

    /**
     * Requisição POST - (Retorna os dados e o pdf boleto que é gerado após validar suas informações e salvar o mesmo).
     */
    public static final String GERAR_BOLETO = API_PAGAMENTOS + "/boleto";

    /**
     * Requisição GET - (Retorna os dados e o pdf boleto do código informado).
     */
    public static final String BUSCAR_BOLETO = API_PAGAMENTOS + "/boleto/";

    /**
     * Requisição GET - (Retorna a situação do boleto do código informado).
     */
    public static final String SITUACAO_BOLETO = API_PAGAMENTOS + "/boleto/situacao/";

    /**
     * Requisição GET - (Retorna os dados de UFERMS de acordo com a data informada).
     */
    public static final String BUSCAR_UFERMS = API_PAGAMENTOS + "/uferms/";

    private static GsonBuilder gsonBuilder = new GsonBuilder();

    private static DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);

    private static SiriemaWS webServiceSiriema = new SiriemaWS();

    public static BoletoVO gerarBoleto(GerarBoletoDTO boletoDTO) throws FileNotFoundException {

        String json = gson.toJson(boletoDTO);

        WS.WSRequest request = WS.url(GERAR_BOLETO);

        request.setHeader("Content-Type", "application/json");
        request.setHeader("Authorization", "Bearer "+ webServiceSiriema.getToken());
        request.body(json);

        WS.HttpResponse response = request.post();

        if (!response.success())
            throw new WebServiceException(response);

        Type type = new TypeToken<BoletoVO>(){}.getType();
        BoletoVO retornoBoletoVO = gsonBuilder.create().fromJson(response.getJson(), type);

        return retornoBoletoVO;
    }

    public static BoletoVO getBoleto(Long id){

        WS.WSRequest request = WS.url(BUSCAR_BOLETO + id);
        request.setHeader("Authorization", "Bearer "+ webServiceSiriema.getToken());

        WS.HttpResponse response = request.get();

        if (!response.success())
            throw new WebServiceException(response);

        Type type = new TypeToken<BoletoVO>(){}.getType();
        BoletoVO retornoBoletoVO = gsonBuilder.create().fromJson(response.getJson(), type);

        return retornoBoletoVO;
    }

    public static SituacaoBoletoVO situacaoBoleto(Long id) {

        WS.WSRequest request = WS.url(SITUACAO_BOLETO + id);
        request.setHeader("Authorization", "Bearer " + webServiceSiriema.getToken());

        WS.HttpResponse response = request.get();

        if (!response.success())
            throw new WebServiceException(response);

        Type type = new TypeToken<SituacaoBoletoVO>(){}.getType();
        SituacaoBoletoVO retornoSituacaoBoletoVO = gsonBuilder.create().fromJson(response.getJson(), type);

        return retornoSituacaoBoletoVO;
    }

    public static UfermsVO gerarUferms(Date data) {

        WS.WSRequest request = WS.url(BUSCAR_UFERMS + dateFormat.format(data));
        request.setHeader("Authorization", "Bearer " + webServiceSiriema.getToken());

        WS.HttpResponse response = request.get();

        if (!response.success())
            throw new WebServiceException(response);

        Type type = new TypeToken<UfermsVO>(){}.getType();
        UfermsVO retornoUfermsVO = gsonBuilder.create().fromJson(response.getJson(), type);

        return retornoUfermsVO;
    }

}