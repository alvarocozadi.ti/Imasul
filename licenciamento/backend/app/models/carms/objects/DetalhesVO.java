package models.carms.objects;

import models.Municipio;
import models.sicar.ImovelSicar;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DetalhesVO {

	public String numeroDoCar;
	public String nomePropriedade;
	public String municipioSede;
	public String municipioSedeIBGE;
	public String roteiroAcesso;
	public CoordenadaVO sedeDoCar;
	public List<MunicipioFiscalVO> municipiosFiscais;
	public List<DocumentoPropriedadeVO> documentos;
	public List<ParticipesVO> participes;
	public String dataUltimaAtualizacao;
	public String situacaoCAR;
	public boolean perimetroIgualAreaReserva;
	public boolean conflitoPerimetro;
	public AssentamentoVO assentamento;
	public String areaTotalDocumentos;
	public String areaTotalCalculada;
	public int istuacaoId;

	public ImovelSicar getImovelSicar(String geometry){

		Municipio mncp = Municipio.findById(Long.parseLong(municipioSedeIBGE));

		geometry = Objects.nonNull(geometry) ? geometry : "";

		return new ImovelSicar(
				0L,
				nomePropriedade,
				numeroDoCar,
				geometry,
				mncp,
				new Date(),
				0D,
				0D,
				0D,
				0D
		);
	}

}