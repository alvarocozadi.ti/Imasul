package models.carms.objects;

import java.util.List;

public class DocumentoPropriedadeVO {

	public Double area;
	public String ccir;
	public String dataDocumento;
	public String folhas;
	public String livro;
	public String municipioDoCartorio;
	public String municipioDoCartorioIBGE;
	public String nomeDoCartorio;
	public String numeroIncra;
	public String numeroMatricula;
	public String tipoDeCartorio;
	public String tipoDeDocumento;
	public String tipoRelacao;
	public List<ParticipesVO> pessoasDocumento;

}