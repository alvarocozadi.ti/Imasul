package models.carms.objects;

import models.Municipio;
import models.sicar.ImovelSicar;

import java.util.Date;
import java.util.List;

public class DocumentoVO {

	public String municipioSede;
	public String municipioSedeIBGE;
	public String nomePropriedade;
	public String numeroDoCar;
	public List<ParticipesVO> participes;
	public boolean perimetroIgualAreaReserva;
	public String situacaoCAR;
	public int situacaoId;

	public ImovelSicar getImovelSicar() {

		Municipio m = Municipio.findById(Long.parseLong(municipioSedeIBGE));

		return new ImovelSicar(0L, nomePropriedade, numeroDoCar, "", m, new Date(),
				0D, 0D, 0D, 0D);

	}

}