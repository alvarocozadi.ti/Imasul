package models.carms.objects;

import models.Municipio;
import models.sicar.ImovelSicar;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DetalhesMinimosVO {

	public int idcar;
	public String municipio;
	public String nomeImovel;
	public String numero;
	public List<ParticipeCarVO> participes;
	public String urlMapa;
	public String situacao;
	public int situacaoId;

	public ImovelSicar getImovelSicar(String geometry) {

		Municipio mncp = Municipio.findByNome(this.municipio);
		Long id = Integer.toUnsignedLong(idcar);

		geometry = Objects.nonNull(geometry) ? geometry : "";

		return new ImovelSicar(
				id,
				nomeImovel,
				numero,
				geometry,
				mncp,
				new Date(),
				0D,
				0D,
				0D,
				0D
		);

	}

}