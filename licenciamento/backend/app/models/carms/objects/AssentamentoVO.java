package models.carms.objects;

public class AssentamentoVO {

	public String codigoDoProjetoDoAssentamento;
	public String dataCriacaoDoAssentamento;
	public String nomeDoAssentamento;
	public String orgaoDoAssentamento;
	public int numeroDeLotes;

}