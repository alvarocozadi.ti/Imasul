package models.carms.pinms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.vividsolutions.jts.geom.Geometry;
import configuracao.serializers.DateSerializer;
import exceptions.WebServiceException;
import models.carms.pinms.token.PinmsToken;
import play.libs.WS;
import utils.GeoJsonUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static utils.Configuracoes.PINMS_MAP_SERVER;

public class PinmsWS {

	private static final PinmsToken token = PinmsToken.getInstance();

	private static final String PARAMETRO_NUMERO = "numero = '{numeroCar}'";
	private static final String RETURN_GEOMETRY = "true";
	private static final String FEATURE_ENCODING = "esriDefault";
	private static final String RESPONSE_FORMAT = "geojson";

	/**
	 * Requisição GET
	 *
	 * - Retorna a feição geográfica do Empreendimento através do número do CAR
	 */
	private static final String URL_QUERY_BY_NUMERO_CAR = PINMS_MAP_SERVER + "/query";

	/**
	 * Busca na base do PIN MS a feição geográfica de um Empreendimento
	 * através do número do CAR
	 *
	 * @param numeroCar
	 * @return String
	 */
	public String getGeometriaByNumeroCAR(String numeroCar){

		Map<String, String> params = new HashMap<>();

		params.put("where", PARAMETRO_NUMERO.replace("{numeroCar}", numeroCar));
		params.put("returnGeometry", RETURN_GEOMETRY);
		params.put("featureEncoding", FEATURE_ENCODING);
		params.put("f", RESPONSE_FORMAT);

		WS.WSRequest request = WS.url(URL_QUERY_BY_NUMERO_CAR);

		request.setHeader("Authorization", "Bearer "+ token.get());
		request.setParameters(params);

		WS.HttpResponse response = request.get();

		if (!response.success())
			throw new WebServiceException(response);

		return response.getString();

	}

}
