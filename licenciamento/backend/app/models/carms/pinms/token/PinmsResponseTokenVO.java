package models.carms.pinms.token;

public class PinmsResponseTokenVO {

    public String token;
    public long expires;
    public String ssl;

}