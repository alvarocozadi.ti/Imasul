package models.carms.pinms.token;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import exceptions.WebServiceException;
import play.Play;
import play.libs.WS;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static utils.Configuracoes.PINMS_TOKEN_URL;

/**
 * Web Service de geração do Token de Acesso ao PIN MS
 * utilizado pela classe de controle do Token.
 */
public class PinmsTokenWS {

	public static final String PINMS_USERNAME = Play.configuration.getProperty("pinms.token.username");
	public static final String PINMS_PASSWORD = Play.configuration.getProperty("pinms.token.password");
	public static final String PINMS_REFERER = Play.configuration.getProperty("pinms.token.referer");
	public static final String PINMS_RESPONSE_FORMAT = Play.configuration.getProperty("pinms.token.response.format");

	private static final String FORMATO_DATA = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	private static final DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);

	private static final GsonBuilder gsonBuilder = new GsonBuilder();

	/**
	 * Gera um novo token de autenticação para a base do PIN MS
	 * @return
	 */
	static PinmsResponseTokenVO getToken() {

		Map<String, String> paramsAuth = new HashMap<>();

		paramsAuth.put("username", PINMS_USERNAME);
		paramsAuth.put("password", PINMS_PASSWORD);
		paramsAuth.put("referer", PINMS_REFERER);
		paramsAuth.put("f", PINMS_RESPONSE_FORMAT);

		WS.WSRequest request = WS.url(PINMS_TOKEN_URL);

		request.setHeader("Content-Type","application/x-www-form-urlencoded");
		request.setParameters(paramsAuth);

		WS.HttpResponse responseAuth = request.post();

		if (!responseAuth.success())
			throw new WebServiceException(responseAuth);

		Type type = new TypeToken<PinmsResponseTokenVO>(){}.getType();

		PinmsResponseTokenVO token = gsonBuilder.create().fromJson(responseAuth.getJson(), type);

		return token;

	}

}
