package models.carms.pinms.token;

import java.util.Date;

/**
 * - SINGLETON CLASS
 *
 * Controla a criação, acesso e atualização do token de acesso
 * para a base do PIN MS
 */
public class PinmsToken {

	private static final long MARGIN = 60000L;

	private static PinmsToken instance;

	private static String token;
	private static Date expiration;

	private PinmsToken(){
		refresh();
	}

	/**
	 * Verifica se o token armazenado já expirou
	 * @return
	 */
	private boolean isExpired() {
		return new Date().after(expiration);
	}

	/**
	 * Gera um novo token consumindo o serviço de autenticação PIN MS
	 */
	private static void refresh() {

		PinmsResponseTokenVO generatedToken = PinmsTokenWS.getToken();

		token = generatedToken.token;
		expiration = new Date(generatedToken.expires - MARGIN);

	}

	/**
	 * Retorna a instância da classe de controle
	 * do token de acesso à API do PIN MS
	 *
	 * @return
	 */
	public static PinmsToken getInstance(){

		if(instance == null) {
			instance = new PinmsToken();
		}

		return instance;

	}

	/**
	 * Retorna o token ativo para acesso à API do PIN MS
	 *
	 * @return
	 */
	public String get() {

		if(isExpired()){
			refresh();
		}

		return token;

	}

}