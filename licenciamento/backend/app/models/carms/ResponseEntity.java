package models.carms;

import java.util.Arrays;

public class ResponseEntity<T> {

	public enum Status {

		ERROR("e"),
		SUCCESS("s"),
		ALERTA("a");

		final String value;

		Status(String value) {
			this.value = value;
		}

		static Status getByValue(String s) {
			return Arrays.stream(values()).filter(status -> status.value.equals(s)).findFirst().orElse(null);
		}

	}

	public String status;
	public T data;
	public String error;

	public Status status() {
		return Status.getByValue(this.status);
	}

}