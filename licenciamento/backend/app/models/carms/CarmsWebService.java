package models.carms;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import configuracao.serializers.DateSerializer;
import models.carms.objects.DetalhesMinimosVO;
import models.carms.objects.DetalhesVO;
import models.carms.objects.DocumentoVO;
import models.carms.objects.IdentificacaoVO;
import models.sicar.ImovelSicar;
import models.tokenSiriema.SiriemaWS;
import play.libs.WS;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static utils.Configuracoes.API_CARMS;

public class CarmsWebService {

	private static final String FORMATO_DATA = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
	private DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA);

	private static final Gson gson = (new GsonBuilder()).serializeSpecialFloatingPointValues().registerTypeAdapter(Date.class, new DateSerializer())
			.registerTypeAdapter(Date.class, new configuracao.deserializers.DateDeserializer()).create();

	private static final GsonBuilder gsonBuilder = new GsonBuilder();

	/**
	 * Requisição GET
	 *
	 * - Retorna os Imóveis CAR relacionados com o documento de identificação.
	 *   (CPF, CNPJ, Passaporte ou Numero de Identificação Estrangeira)
	 */
	public static final String URL_CAR_CPF_CNPJ = API_CARMS + "/fazenda/identidade/{cpfCnpj}";

	/**
	 * Requisição GET
	 *
	 * - Retorna os detalhes básicos do Imovel CAR através do número.
	 */
	public static final String URL_DETALHES_MINIMOS = API_CARMS + "/fazenda/{numeroCar}";

	/**
	 * Requisição GET
	 *
	 * - Retorna os detalhes do Imovel CAR através do número.
	 */
	public static final String URL_DETALHES = API_CARMS + "/fazenda/detalhes/{numeroCar}";

	private static final SiriemaWS webServiceSiriema = new SiriemaWS();

	/**
	 * Busca na API CARMS os Imóveis CAR vinculados ao documento.
	 * Aceita: CPF, CNPJ, Passsaporte ou Número de Identificação Estrangeira
	 * @param cpfCnpj
	 * @return
	 */
	public static List<ImovelSicar> getImoveisSimplificadosPorCpfCnpj(String cpfCnpj) {

		String url = URL_CAR_CPF_CNPJ.replace("{cpfCnpj}", cpfCnpj);

		WS.WSRequest request = simpleRequestHeader(url);
		WS.HttpResponse response = request.get();

		ResponseEntity<IdentificacaoVO> responseEntity;

		try {

			Type type = new TypeToken<ResponseEntity<IdentificacaoVO>>(){}.getType();
			responseEntity = gsonBuilder.create().fromJson(response.getJson(), type);

		} catch(Exception e) {
			throw new exceptions.WebServiceException(response);
		}

		if(responseEntity.status() == ResponseEntity.Status.ERROR) {
			return new ArrayList<>();
		}

		List<DocumentoVO> cars = Arrays.stream(responseEntity.data.cars)
				.filter(x -> Objects.nonNull(x.numeroDoCar)).collect(Collectors.toList());

		List<ImovelSicar> imoveis = new ArrayList<>();

		cars.forEach(c -> imoveis.add(c.getImovelSicar()));
		return imoveis;

	}

	/**
	 * Busca na API CARMS os dados completos de um Imóvel.
	 *
	 * @param numeroCar
	 * @return
	 */
	public static DetalhesVO getDetalhes(String numeroCar) {

		String url = URL_DETALHES.replace("{numeroCar}", numeroCar);

		WS.WSRequest request = simpleRequestHeader(url);
		WS.HttpResponse response = request.get();

		ResponseEntity<DetalhesVO> responseEntity;

		try {

			Type type = new TypeToken<ResponseEntity<DetalhesVO>>(){}.getType();
			responseEntity = gson.fromJson(response.getJson(), type);

		} catch(Exception e) {
			throw new exceptions.WebServiceException(response);
		}

		return responseEntity.data;

	}

	/**
	 * Busca na API CARMS os dados básicos de um Imóvel.
	 *
	 * @param numeroCar
	 * @return
	 */
	public static DetalhesMinimosVO getDetalhesMinimos(String numeroCar) {

		String url = URL_DETALHES_MINIMOS.replace("{numeroCar}", numeroCar);

		WS.WSRequest request = simpleRequestHeader(url);
		WS.HttpResponse response = request.get();

		ResponseEntity<DetalhesMinimosVO> responseEntity;

		try {

			Type type = new TypeToken<ResponseEntity<DetalhesMinimosVO>>(){}.getType();
			responseEntity = gsonBuilder.create().fromJson(response.getJson(), type);

		} catch(Exception e) {
			throw new exceptions.WebServiceException(response);
		}

		return responseEntity.data;

	}

	// Default methods

	private CarmsWebService(){}

	private static WS.WSRequest simpleRequestHeader(String url){

		WS.WSRequest request = WS.url(url);

		request.setHeader("Content-Type", "application/json");
		request.setHeader("Authorization", "Bearer "+ webServiceSiriema.getToken());

		return request;

	}

}
