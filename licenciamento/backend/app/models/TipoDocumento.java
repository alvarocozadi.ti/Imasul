package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import models.pdf.PDFTemplate;
import play.db.jpa.Model;

@Entity
@Table(schema = "licenciamento", name = "tipo_documento")
public class TipoDocumento extends Model {

	public static Long DOCUMENTO_REPRESENTACAO = 1l;
	public static Long DISPENSA_LICENCIAMENTO = 2l;
	public static Long DOCUMENTO_ARRECADACAO_ESTADUAL = 3l;
	
	public static Long LICENCA_PREVIA = 101L;
	public static Long LICENCA_INSTALACAO = 102L;
	public static Long LICENCA_OPERACAO = 103L;
	public static Long LICENCA_INSTALACAO_OPERACAO = 104L;
	public static Long AUTORIZACAO_AMBIENTAL = 105l;
	public static Long COMUNICADO_AMBIENTAL = 106L;
	public static Long RENOVACAO_LICENCA_PREVIA = 107L;
	public static Long RENOVACAO_LICENCA_DE_INSTALACAO = 108L;
	public static Long RENOVACAO_LICENCA_DE_OPERACAO = 109L;
	public static Long RENOVACAO_LICENCA_DE_INSTALACAO_OPERACAO = 110L;
	public static Long RENOVACAO_AUTORIZACAO_AMBIENTAL = 111L;


	public static final String COD_LICENCA_PREVIA = "DOC_LP";
	public static final String COD_LICENCA_INSTALACAO = "DOC_LI";
	public static final String COD_LICENCA_OPERACAO = "DOC_LO";
	public static final String COD_LICENCA_INSTALACAO_OPERACAO = "DOC_LIO";
	public static final String COD_AUTORIZACAO_AMBIENTAL = "DOC_AA";
	public static final String COD_COMUNICADO_AMBIENTAL = "DOC_CA";
	public static final String COD_RENOVACAO_LICENCA_PREVIA = "DOC_RLP";
	public static final String COD_RENOVACAO_LICENCA_DE_INSTALACAO = "DOC_RLI";
	public static final String COD_RENOVACAO_LICENCA_DE_OPERACAO = "DOC_RLO";
	public static final String COD_RENOVACAO_LICENCA_DE_INSTALACAO_OPERACAO = "DOC_RLIO";
	public static final String COD_RENOVACAO_AUTORIZACAO_AMBIENTAL = "DOC_RAA";


	public String nome;
	
	@Column(name="caminho_modelo")
	public String caminhoModelo;

	@Column(name="caminho_pasta")
	public String caminhoPasta;
	
	@Column(name="prefixo_nome_arquivo")
	public String prefixoNomeArquivo;
	
	public String codigo;
	
	
	public PDFTemplate getPdfTemplate() {
	
		return PDFTemplate.getByTipoDocumento(this.id);
	}
}
