package models.analise;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema="analise", name="inconsistencia")
public class Inconsistencia extends GenericModel {

	public static final String SEQ = "analise.inconsistencia_id_seq";

	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
	@SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
	public Long id;

	@Column(name="id_caracterizacao")
	public Long idCaracterizacao;

	@Column
	public String categoria;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(schema="analise", name="rel_documento_inconsistencia",
			joinColumns=@JoinColumn(name="id_inconsistencia"),
			inverseJoinColumns=@JoinColumn(name="id_documento"))
	public List<Documento> anexos;

}
