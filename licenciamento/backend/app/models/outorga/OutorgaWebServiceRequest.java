package models.outorga;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import exceptions.WebServiceException;
import models.tokenSiriema.SiriemaWS;
import play.libs.WS;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class OutorgaWebServiceRequest {

	private String requestURL;

	public OutorgaWebServiceRequest setURL(String requestURL) {

		this.requestURL = requestURL;

		return this;

	}

	public List<OutorgaDTO> executeRequestGet() throws WebServiceException {

		WS.WSRequest request = WS.url(requestURL);

		request.setHeader("Authorization", "Bearer ".concat(SiriemaWS.getToken()));
		request.setHeader("Accept","application/json");

		WS.HttpResponse response = request.get();

		if (!response.success()) {
			throw new WebServiceException(response);
		}

		Type type = new TypeToken<List<OutorgaDTO>>(){}.getType();
		GsonBuilder gson = new GsonBuilder();

		try {

			return gson.create().fromJson(response.getJson(), type);

		} catch (RuntimeException e) {

			//  --> A API retorna uma mensagem em txt puro ao invés de um objeto JSON:
			// "Não existe(m) processo(s) de outorga de águas deferido(s) na base de dados para esse número de
			// identificação (CPF/CNPJ)."
			return new LinkedList<>();

		}

	}

}
