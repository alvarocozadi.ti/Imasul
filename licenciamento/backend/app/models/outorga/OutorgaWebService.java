package models.outorga;

import play.Play;
import java.util.List;
import java.util.stream.Collectors;

/**
 * API de Processo Outorga do IMASUL
 *
 * @link http://hom.api.sgi.ms.gov.br/d0479/outorga-service/v1/swagger/swagger-ui.html
 * @version 1.0
 */
public class OutorgaWebService {

	private static final String BASE_URL = Play.configuration.getProperty("outorga.api.url");
	private static final String URL_OUTORGA_CONTROLLER = BASE_URL.concat("/outorga/outorgaDeferidaComPortariaEmitida/");

	public static List<String> getOutorgas(String cpfCnpj) {

		OutorgaWebServiceRequest request = new OutorgaWebServiceRequest();

		List<OutorgaDTO> processosOutorga = request.setURL(URL_OUTORGA_CONTROLLER.concat(cpfCnpj)).executeRequestGet();

		return processosOutorga.stream().map(outorgaDTO -> outorgaDTO.processoOutorga).collect(Collectors.toList());

	}

}
