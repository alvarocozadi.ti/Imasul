package deserializers;

import java.lang.reflect.Type;

import com.google.gson.JsonObject;
import utils.GeoJsonUtils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.vividsolutions.jts.geom.Geometry;

public class GeometryDeserializer implements JsonDeserializer<Geometry> {

	@Override
	public Geometry deserialize(JsonElement json, Type type, JsonDeserializationContext context) {

		if (json.isJsonPrimitive()) {

			return parseGeometry(json.getAsJsonPrimitive().getAsString());

		} else {

			JsonObject jsonObject = json.getAsJsonObject();

			if (jsonObject.get("type").toString().equals("\"Feature\""))
				return parseGeometry(jsonObject.get("geometry").toString());
			else
				return parseGeometry(jsonObject.toString());

		}

	}

	public static Geometry parseGeometry(String geoJson) {
		return GeoJsonUtils.toGeometry(geoJson);
	}
}
