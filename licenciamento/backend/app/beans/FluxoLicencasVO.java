package beans;

import models.caracterizacao.TipoLicenca;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FluxoLicencasVO {

    public Map<String,Map<String,TipoLicencaVO>> fluxos;

    public FluxoLicencasVO(List<TipoLicenca> tipos) {
        this.fluxos = tipos.stream().filter(TipoLicenca::naoSimplificado).collect(Collectors.toMap(
                tp -> tp.sigla, TipoLicenca::hashFilhas
        ));
    }
}
