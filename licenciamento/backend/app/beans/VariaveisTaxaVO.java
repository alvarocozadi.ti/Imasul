package beans;

import models.caracterizacao.*;

import java.util.List;

import static models.caracterizacao.Variaveis.*;

public class VariaveisTaxaVO {

    public Long numeroTecnicos;

    public Long numeroServidores;

    public Long numeroHoras;

    public Double custoHoraTecnica;

    public Long numeroDiasVistoria;

    public Double custosDespesasViagem;

    public Long numeroVeiculosUsados;

    public Double numeroKmRodados;

    public Double custoKmRodado;

    public VariaveisTaxaVO (){};

    public VariaveisTaxaVO (List<TaxaLicenciamento> listaVariaveis, AtividadeCaracterizacao atividadeCaracterizacao, Caracterizacao caracterizacao){

        listaVariaveis.forEach(valorVariaveisTaxa -> {

            if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_TECNICOS.sigla))
                numeroTecnicos = resolverFormulaInteiros(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_SERVIDORES.sigla))
                numeroServidores = resolverFormulaInteiros(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_HORAS.sigla))
                numeroHoras = resolverFormulaInteiros(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(CUSTO_HORA.sigla))
                custoHoraTecnica = resolverFormula(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_DIAS.sigla))
                numeroDiasVistoria = resolverFormulaInteiros(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(CUSTO_DESPESAS.sigla))
                custosDespesasViagem = resolverFormula(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_VEICULOS.sigla))
                numeroVeiculosUsados = resolverFormulaInteiros(valorVariaveisTaxa.valor);

            else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(NUMERO_KM_RODADOS.sigla)) {
                if (caracterizacao.complexo) {
                    numeroKmRodados = AtividadeCaracterizacao.calcularDistanciaCapitalComplexo(caracterizacao.geometriasComplexo.get(0),caracterizacao);
                }else {
                    numeroKmRodados = AtividadeCaracterizacao.calcularDistanciaCapital(atividadeCaracterizacao, caracterizacao);
                }

            }else if(valorVariaveisTaxa.variavelCalculoTaxa.sigla.equals(CUSTO_KM.sigla))
                custoKmRodado = resolverFormula(valorVariaveisTaxa.valor);
        });
    }

    private static Long resolverFormulaInteiros(String formula) {

        return Long.parseLong(formula);
    }

    private static Double resolverFormula(String formula) {

        String uferms = formula.substring(0,6);
        Double numero = null;
        String sinalNumero = null;

        if(uferms.equals("UFERMS")){

             sinalNumero = formula.replace("UFERMS","");

            switch (sinalNumero.charAt(0)){
                case '*':
                    numero = Uferms.get() * Double.parseDouble(sinalNumero.replace("*",""));
                    break;
                case '/':
                    numero = Uferms.get() / Double.parseDouble(sinalNumero.replace("/",""));
                    break;
                case '-':
                    numero = Uferms.get() - Double.parseDouble(sinalNumero.replace("-",""));
                    break;
                case '+':
                    numero = Uferms.get() + Double.parseDouble(sinalNumero.replace("+",""));
                    break;

            }
        }

        return numero;

    }
}
