package beans;

import com.vividsolutions.jts.geom.Geometry;
import models.caracterizacao.TipoSobreposicao;
import org.geotools.feature.FeatureCollection;
import utils.DataUtils;

import java.util.Date;

public class DadosSobreposicaoVO {

    public Geometry geometria;
    public String nomeAreaSobreposicao;
    public String dataAreaSobreposicao;
    public String cpfCnpjAreaSobreposicao;

    public DadosSobreposicaoVO() {}

    public DadosSobreposicaoVO(Geometry geometria, Object nomeAreaSobreposicao, Object dataAreaSobreposicao, Object cpfCnpjAreaSobreposicao){
        this.geometria = geometria;
        this.nomeAreaSobreposicao = (nomeAreaSobreposicao == null ? null : nomeAreaSobreposicao.toString());
        this.dataAreaSobreposicao = (dataAreaSobreposicao == null ? null : formataData(dataAreaSobreposicao));
        this.cpfCnpjAreaSobreposicao = (cpfCnpjAreaSobreposicao == null ? null : cpfCnpjAreaSobreposicao.toString());
    }

    private static String formataData(Object data) {

        if(data instanceof Date) {
            return DataUtils.formataData((Date) data, "dd/MM/yyyy");
        }

        return data.toString();

    }
}
