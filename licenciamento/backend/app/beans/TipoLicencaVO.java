package beans;

import models.caracterizacao.TipoLicenca;

import java.io.Serializable;

public class TipoLicencaVO implements Serializable {

    public Long id;
    public Integer validadeEmAnos;
    public String nome;
    public String sigla;

    public TipoLicencaVO(Long id, Integer validadeEmAnos, String nome, String sigla) {
        this.id = id;
        this.validadeEmAnos = validadeEmAnos;
        this.nome = nome;
        this.sigla = sigla;
    }

    public TipoLicencaVO(TipoLicenca tp) {
        this.id = tp.id;
        this.validadeEmAnos = tp.validadeEmAnos;
        this.nome = tp.nome;
        this.sigla = tp.sigla;
    }


}
