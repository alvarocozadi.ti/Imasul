package beans;

import models.caracterizacao.TipoSobreposicao;
import org.geotools.feature.FeatureCollection;

public class ResultadoSobreposicaoCamadaVO {

    public FeatureCollection featureCollection;
    public TipoSobreposicao tipoSobreposicao;

    public ResultadoSobreposicaoCamadaVO(FeatureCollection featureCollection, TipoSobreposicao tipoSobreposicao){
        this.featureCollection = featureCollection;
        this.tipoSobreposicao = tipoSobreposicao;
    }
}
