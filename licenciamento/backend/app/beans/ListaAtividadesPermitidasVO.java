package beans;

import java.util.List;

/***
 * Classe utilizada para tratar o possível hashmap vindo do Front
 * e evitar o tratamento dos dados em gerais no mesmo
 ***/
public class ListaAtividadesPermitidasVO {

	public List<AtividadePermitidaVO> licencasAtividade;

}
