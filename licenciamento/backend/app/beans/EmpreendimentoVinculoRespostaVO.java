package beans;

import models.Empreendimento;

public class EmpreendimentoVinculoRespostaVO {
    public String mensagem;
    public Boolean existeVinculo;

    public EmpreendimentoVinculoRespostaVO(Boolean existeVinculo, String mensagem) {
        this.existeVinculo = existeVinculo;
        this.mensagem = mensagem;
    }
}
