package beans;

import models.caracterizacao.TipoLicenca;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TipoLicencaBuscaVO implements Serializable {

	public List<TipoLicenca> tipoLicencaSolicitacao;
	public List<TipoLicenca> tipoLicencaRenovacao;
	public List<TipoLicenca> tipoLicencaAtualizacao;
	public List<TipoLicenca> tipoLicencaCadastro;

	public TipoLicencaBuscaVO() {
		this.tipoLicencaSolicitacao = new ArrayList<>();
		this.tipoLicencaRenovacao = new ArrayList<>();
		this.tipoLicencaAtualizacao = new ArrayList<>();
	}

	public TipoLicencaBuscaVO(List<TipoLicenca> tipoLicencaSolicitacao, List<TipoLicenca> tipoLicencaRenovacao, List<TipoLicenca> tipoLicencaAtualizacao, List<TipoLicenca> tipoLicencaCadastro ) {
		this.tipoLicencaSolicitacao = tipoLicencaSolicitacao;
		this.tipoLicencaRenovacao = tipoLicencaRenovacao;
		this.tipoLicencaAtualizacao = tipoLicencaAtualizacao;
		this.tipoLicencaCadastro = tipoLicencaCadastro;
	}

}
