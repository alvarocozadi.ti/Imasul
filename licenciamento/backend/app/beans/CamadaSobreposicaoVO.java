package beans;

import models.caracterizacao.TipoSobreposicao;

public class CamadaSobreposicaoVO {

    public String camada;
    public TipoSobreposicao tipoSobreposicao;

    public CamadaSobreposicaoVO(String camada, TipoSobreposicao tipoSobreposicao){
        this.camada = camada;
        this.tipoSobreposicao = tipoSobreposicao;
    }
}
