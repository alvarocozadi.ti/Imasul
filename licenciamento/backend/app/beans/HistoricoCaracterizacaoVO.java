package beans;

import models.caracterizacao.Caracterizacao;

import java.util.Date;

public class HistoricoCaracterizacaoVO {

    public Long idCaracterizacao;
    public Date data;
    public String nomeResponsavel;

    public HistoricoCaracterizacaoVO() {
    }


    public HistoricoCaracterizacaoVO(Caracterizacao caracterizacao, String nomeResponsavel) {
        this.idCaracterizacao = caracterizacao.id;
        this.data = caracterizacao.dataRetificacao;
        this.nomeResponsavel = nomeResponsavel;
    }



}
