package controllers;

import beans.CadastroEmpreendimentoVO;
import beans.EmpreendimentoVinculoRespostaVO;
import beans.RemoveEmpreendimentoVO;
import beans.VinculoEmpreendimentoCpfCnpjVO;
import exceptions.AppException;
import br.ufla.lemaf.beans.FiltroEmpreendimento;
import exceptions.ValidacaoException;
import models.*;
import security.Acao;
import security.services.Auth;
import serializers.EmpreendimentoSerializer;
import serializers.PessoasSerializer;
import utils.Mensagem;
import utils.WebServiceEntradaUnica;

import javax.xml.bind.ValidationException;
import java.util.List;
import java.util.stream.Collectors;

public class Empreendimentos extends InternalController {

	private static List<Empreendimento> empreendimentosVinculadosPessoaEU;

	public static void verificaUsuario(Empreendimento empreendimento) {
		empreendimento.validarSeUsuarioCadastrante();
	}

	public static void getEmpreendimentoPorId(Long idEmpreendimento) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		Empreendimento empreendimento = Empreendimento.findById(idEmpreendimento);

		empreendimento.empreendimentoEU = WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(empreendimento.idEmpreendimentoEU);

		renderJSON(empreendimento, EmpreendimentoSerializer.getEmpreendimento);

	}

	public static void getEmpreendimentoCompleto(Long id) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		Empreendimento empreendimento = Empreendimento.findById(id);

		renderJSON(Empreendimento.convert(WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(empreendimento.idEmpreendimentoEU)), EmpreendimentoSerializer.getEmpreendimento);

	}

public static void getEmpreendimentoPorCpfCnpj(CadastroEmpreendimentoVO cadastroEmpVO) throws ValidationException {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		List<Empreendimento> empreendimentos = WebServiceEntradaUnica.findEmpreendimentosByCpfCnpj(cadastroEmpVO.cpfCnpjEmpreendedor);

		Empreendimento empreendimento = new Empreendimento();

		if(!empreendimentos.isEmpty()) {

			if(cadastroEmpVO.mesmoEmpreendedor == false){
				empreendimento = Empreendimento.getEmpreendimentoVinculo(cadastroEmpVO.cpfCnpjEmpreendimento, empreendimentos);
			}else{
				empreendimento = Empreendimento.find("cpf_cnpj = :cpfCnpj AND is_principal = :isPrincipal")
						.setParameter("cpfCnpj",cadastroEmpVO.cpfCnpjEmpreendimento)
						.setParameter("isPrincipal",true)
						.first();

				if(empreendimento == null){
					empreendimento = Empreendimento.convert(empreendimentos.get(0).empreendimentoEU, cadastroEmpVO.cpfCnpjEmpreendedor);
				}
			}
		}else{
			empreendimento = Empreendimento.convert(null, cadastroEmpVO.cpfCnpjEmpreendimento);
		}

		if(Empreendimento.verificaExistenciaDeEmpreendimentoLicencimento(cadastroEmpVO.cpfCnpjEmpreendimento)){
			empreendimento.empreendimentoJaCadastrado = true;
		}

		Boolean isJunta = empreendimento.empreendimentoEU.pessoa.isJunta;

//		if(isJunta != null && isJunta == false){
//			throw new ValidationException("Empreendimento não faz parte da junta");
//		}

		renderJSON(empreendimento, EmpreendimentoSerializer.getEmpreendimento);
	}

	public static void getEmpreendimentosParaOutroEmpreendedor(Long id) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		Empreendimento empreendimento = Empreendimento.findById(id);

		renderJSON(Empreendimento.convert(WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(empreendimento.idEmpreendimentoEU)), EmpreendimentoSerializer.getEmpreendimento);

	}

	public static void list(String pesquisa, Integer numeroPagina, Integer qtdItensPorPagina) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		List<Empreendimento> empreendimentosLicenciamento = Empreendimento.find("byCpfCnpjCadastrante",getUsuarioSessao().login).fetch();

		if (empreendimentosLicenciamento == null || empreendimentosLicenciamento.isEmpty()) {
			renderJSON(new Pagination<>(0L, empreendimentosLicenciamento), EmpreendimentoSerializer.listGrid);
		}
//		int totalEmpreendimentosLicenciamento = empreendimentosLicenciamento.size();

		List<String> cpfcnpjs = empreendimentosLicenciamento.stream().map(empreendimento -> empreendimento.cpfCnpj).collect(Collectors.toList());

		FiltroEmpreendimento filtroEmpreendimento = new FiltroEmpreendimento();

		filtroEmpreendimento.ordenacao = "DENOMINACAO_ASC";

		filtroEmpreendimento.cpfsCnpjs = cpfcnpjs;

		filtroEmpreendimento.busca = pesquisa;

		filtroEmpreendimento.tamanhoPagina = qtdItensPorPagina;
		filtroEmpreendimento.numeroPagina = numeroPagina;

		Pagination<Empreendimento> pagination = WebServiceEntradaUnica.listEmpreendimento(filtroEmpreendimento);

		pagination.getPageItems().forEach(empreendimento ->
				empreendimento.possuiCaracterizacoes = empreendimento.temCaracterizacoes()
		);

		renderJSON(pagination, EmpreendimentoSerializer.listGrid);

	}

	public static void listEmpreendimentosEmpreendedor(String cpfCnpj, Boolean listagem, String pesquisa, Integer numeroPagina, Integer qtdItensPorPagina) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		FiltroEmpreendimento filtroEmpreendimento = new FiltroEmpreendimento();

		filtroEmpreendimento.ordenacao = "DENOMINACAO_ASC";

		filtroEmpreendimento.cpfCnpjEmpreendedor = cpfCnpj;

		filtroEmpreendimento.busca = pesquisa;

		filtroEmpreendimento.tamanhoPagina = qtdItensPorPagina;
		filtroEmpreendimento.numeroPagina = numeroPagina;
		filtroEmpreendimento.cpfCnpj = getUsuarioSessao().login;

		Pagination<Empreendimento> pagination = WebServiceEntradaUnica.listEmpreendimento(filtroEmpreendimento);

		pagination.getPageItems().forEach(empreendimento ->
				empreendimento.possuiCaracterizacoes = empreendimento.temCaracterizacoes()
		);

		renderJSON(pagination, EmpreendimentoSerializer.listGrid);

	}

	public static void getPessoaPorCpfCnpj(String cpfCnpj) {

		verificarPermissao(Acao.CADASTRAR_EMPREENDIMENTO);

		Pessoa pessoa = (Pessoa) WebServiceEntradaUnica.findPessoaByCpfCnpj(cpfCnpj);

		renderJSON(pessoa, PessoasSerializer.findByCpfCnpj);
	}

	public static void create(Empreendimento empreendimento) {

		verificarPermissao(Acao.CADASTRAR_EMPREENDIMENTO);

		empreendimento.cadastrante = Auth.getUsuarioSessao().findPessoa();

		empreendimento.save();

		renderMensagem(Mensagem.EMPREENDIMENTO_CADASTRADO_SUCESSO);
	}


	public static void update(Long id, Empreendimento empreendimentoAtualizado) {

		verificarPermissao(Acao.ALTERAR_EMPREENDIMENTO);

		Empreendimento empreendimento = Empreendimento.findById(id);

		empreendimento.update(empreendimentoAtualizado);

		renderMensagem(Mensagem.EMPREENDIMENTO_ATUALIZADO_SUCESSO);
	}

	public static void findByIdResumido(Long id) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		Empreendimento empreendimentoLocal = Empreendimento.findById(id);

		if (empreendimentoLocal == null) {
			throw new AppException(Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO);
		}

		Empreendimento empreendimento = Empreendimento.convert(WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(empreendimentoLocal.idEmpreendimentoEU));

		if (empreendimento == null) {
			throw new AppException(Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO);
		}

		empreendimento.validarSeUsuarioCadastrante();

		renderJSON(empreendimento, EmpreendimentoSerializer.findByIdResumido);

	}

	public static void findToUpdate(Long id) {

		verificarPermissao(Acao.ALTERAR_EMPREENDIMENTO);

		Empreendimento empreendimentoLocal = Empreendimento.findById(id);

		if (empreendimentoLocal == null) {
			throw new AppException(Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO);
		}
		Empreendimento empreendimento = Empreendimento.convert(WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(empreendimentoLocal.idEmpreendimentoEU));

		if (empreendimento == null) {
			throw new AppException(Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO);
		}

		renderJSON(empreendimento, EmpreendimentoSerializer.findToUpdate);
	}

	public static void delete(Long id) {

		verificarPermissao(Acao.EXCLUIR_EMPREENDIMENTO);

		Empreendimento empreendimento = Empreendimento.findById(id);

		if(empreendimento != null){
			empreendimento.delete();
		}else{
			throw new AppException(Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO);
		}

		renderMensagem(Mensagem.EMPREENDIMENTO_EXCLUIDO_SUCESSO);

	}

	public static void deleteEmpreendimentos(RemoveEmpreendimentoVO removeVO){

		verificarPermissao(Acao.EXCLUIR_EMPREENDIMENTO);

		try {
			if(removeVO.ids != null) {
				removeVO.ids.forEach(id -> {

					Empreendimento empreendimento = Empreendimento.findById(id);
					empreendimento.delete();
				});
			}else{

				throw new ValidationException("Ocorreu um erro, não existem empreendimentos para serem excluidos");
			}
		}catch (Exception e){

			throw new ValidacaoException(Mensagem.EMPREENDEDOR_POSSUI_EMPREENDIMENTO_COM_CARACTERIZACAO);
		}

		renderMensagem(Mensagem.EMPREENDIMENTO_EXCLUIDO_SUCESSO);

	}

	public static void getVinculos(VinculoEmpreendimentoCpfCnpjVO cpfCnpjVO){

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		EmpreendimentoVinculoRespostaVO respostaVO = new EmpreendimentoVinculoRespostaVO(false,"");

		List<Empreendimento> empreendimentos = WebServiceEntradaUnica.findEmpreendimentosByCpfCnpj(cpfCnpjVO.cpfCnpjEmpreendedor);

		if(!empreendimentos.isEmpty()){

			Boolean possuiVinculo = Empreendimento.possuiVinculoCpfCnpjEmpreendedor(empreendimentos, cpfCnpjVO.cpfCnpjEmpreendimento);

			respostaVO.mensagem = possuiVinculo ? "" : Mensagem.CPF_CNPJ_NAO_VINCULADO_EMPREENDEDOR.getTexto();

			respostaVO.existeVinculo = possuiVinculo;
		}else {
			respostaVO.mensagem = Mensagem.EMPREENDIMENTO_NAO_ENCONTRADO.getTexto();
		}

		renderJSON(respostaVO, EmpreendimentoSerializer.getEmpreendimentoVinculo);

	}

	public void getCadastrante(String cpfCnpj) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		renderJSON(WebServiceEntradaUnica.findPessoaByCpfCnpjEU(cpfCnpj), PessoasSerializer.findByCpfCnpj);

	}

}
