package controllers;

import br.ufla.lemaf.beans.pessoa.Endereco;
import br.ufla.lemaf.enums.TipoEndereco;
import models.AtividadeParametroLimites;
import models.Empreendimento;
import models.caracterizacao.Atividade;
import models.caracterizacao.TipoCaracterizacaoAtividade.FiltroAtividade;
import security.Acao;
import serializers.AtividadeSerializer;
import utils.WebServiceEntradaUnica;

import java.util.List;

public class Atividades extends InternalController {

	public static void findAtividade() {

		verificarPermissao(Acao.CADASTRAR_CARACTERIZACAO);

		FiltroAtividade filtro = new FiltroAtividade();
		filtro.licenciamentoSimplificado = getParamAsBoolean("licenciamentoSimplificado");
		filtro.idTipologia = getParamAsLong("idTipologia");
		filtro.dispensaLicenciamento = getParamAsBoolean("dispensaLicenciamento");
		Long idEmpreendimento = getParamAsLong("idEmpreendimento");

		Empreendimento emp = Empreendimento.findById(idEmpreendimento);
		Empreendimento empreendimento = Empreendimento.convert(WebServiceEntradaUnica.oAuthClient.buscarEmpreendimentoComId(emp.idEmpreendimentoEU));
		returnIfNull(empreendimento, "Empreendimento");

		Endereco endereco = empreendimento.empreendimentoEU.enderecos.stream().filter(end -> end.tipo.id == TipoEndereco.ID_PRINCIPAL).findFirst().get();

		filtro.tipoEndereco = endereco.zonaLocalizacao.codigo;

		//if(empreendimento.cpfCnpj.length() > 11){
			//filtro.listaCodigosCnaes = empreendimento.getCodigosCnaes();
		//}

		List<Atividade> atividades = Atividade.findAtividades(filtro);

		renderJSON(atividades, AtividadeSerializer.findAtividade);
	}

	public static void findPerguntasDLA(Long idAtividade) {

		verificarPermissao(Acao.CADASTRAR_CARACTERIZACAO);

		Atividade atividade = Atividade.findById(idAtividade);

		renderJSON(atividade, AtividadeSerializer.findPerguntas);
	}

}
