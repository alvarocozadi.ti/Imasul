package controllers;

import models.outorga.OutorgaWebService;

public class ProcessosOutorga extends InternalController {

	public static void getOutorga(String cpfCnpj) {

		renderJSON(OutorgaWebService.getOutorgas(cpfCnpj));

	}

}
