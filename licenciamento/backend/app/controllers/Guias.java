package controllers;

import exceptions.AppException;
import models.caracterizacao.Caracterizacao;
import models.caracterizacao.StatusCaracterizacao;
import models.caracterizacao.Uferms;
import models.integracaoPagamentos.BoletoVO;
import models.integracaoPagamentos.GerarBoletoDTO;
import models.integracaoPagamentos.PagamentosWS;
import security.Acao;
import sun.misc.BASE64Decoder;
import utils.Mensagem;
import play.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static utils.Configuracoes.ARQUIVOS_DOCUMENTOS_PATH;

public class Guias extends InternalController {

    public static String OBSERVACAO = "Guia referente à solicitação de Licenciamento Ambiental. Após o pagamento a solicitação tramitará para a análise do setor responsável.";

    public void downloadGuia(Long id) throws FileNotFoundException {

        verificarPermissao(Acao.CADASTRAR_CARACTERIZACAO);

        Caracterizacao caracterizacao = Caracterizacao.findById(id);

        if(caracterizacao.valorTotalTaxaLicenciamento == 0.00) {
            throw new AppException(Mensagem.CARACTERIZACAO_TAXALICENCIAMENTO_ZERO);

        }else {
            caracterizacao.empreendimento.validarSeUsuarioCadastrante();

            GerarBoletoDTO boleto = convertToBoleto(caracterizacao);

            BoletoVO boletoVO = PagamentosWS.gerarBoleto(boleto);

            try {
                caracterizacao.status = StatusCaracterizacao.findById(StatusCaracterizacao.AGUARDANDO_PAGAMENTO_DA_GUIA);
                caracterizacao.codigoGuia = boletoVO.codigoBoleto;
                caracterizacao.save();

                renderBinaryFile(byteArrayToFile(boletoVO.dados, boletoVO.codigoBoleto));

            } catch (Exception e) {

                Logger.error(e.getMessage());

                renderMensagem(Mensagem.CARACTERIZACAO_ERRO_EMISSAO_DAE);

            }
        }
    }

    public void getGuia(Long id) throws FileNotFoundException {

        verificarPermissao(Acao.CADASTRAR_CARACTERIZACAO);

        Caracterizacao caracterizacao = Caracterizacao.findById(id);
        caracterizacao.empreendimento.validarSeUsuarioCadastrante();

        BoletoVO boletoVO = PagamentosWS.getBoleto(caracterizacao.codigoGuia);

        try{
            renderBinaryFile(byteArrayToFile(boletoVO.dados, boletoVO.codigoBoleto));

        }catch (Exception e){

            Logger.error(e.getMessage());

            renderMensagem(Mensagem.CARACTERIZACAO_ERRO_EMISSAO_DAE);

        }
    }

    public GerarBoletoDTO  convertToBoleto(Caracterizacao caracterizacao){

        GerarBoletoDTO boleto = new GerarBoletoDTO();
        boleto.contribuinte = verificaCpfCnpj(caracterizacao.empreendimento.cpfCnpj);
        boleto.observacao = OBSERVACAO;
        boleto.tipoArrecadacao = caracterizacao.tipoLicenca.tipoArrecadaocao;
        boleto.valorReal = caracterizacao.valorTotalTaxaLicenciamento;
        boleto.valorUferms = Uferms.get();
        boleto.empreendimento = caracterizacao.empreendimento.denominacao.length() > 80 ? caracterizacao.empreendimento.denominacao.substring(0,79) : caracterizacao.empreendimento.denominacao;
        boleto.numeroProcesso = caracterizacao.processo.numero;



        return boleto;
    }

    public String formataCpf(String cpf) {

        StringBuilder sBuilder = new StringBuilder(cpf)
                .insert(3, ".")
                .insert(7, ".")
                .insert(11, "-");

        return sBuilder.toString();
    }

    public String formataCnpj(String cnpj){

        StringBuilder sBuilder = new StringBuilder(cnpj)
                .insert(2, ".")
                .insert(6, ".")
                .insert(10, "/")
                .insert(15, "-");

        return sBuilder.toString();
    }

    public String verificaCpfCnpj(String cpfCnpj){

        String cpfOUcnpj;

        if(cpfCnpj.length() > 11){
            cpfOUcnpj = formataCnpj(cpfCnpj);
        }else{
            cpfOUcnpj = formataCpf(cpfCnpj);
        }

        return cpfOUcnpj;
    }

    public File byteArrayToFile(String bString, Long codigo) throws FileNotFoundException {

        File file = new File(ARQUIVOS_DOCUMENTOS_PATH + "/dae/Guia_" + codigo + ".pdf");
        FileOutputStream fop = new FileOutputStream(file);
        try {

            BASE64Decoder decoder = new BASE64Decoder();
            byte[] decodedBytes = decoder.decodeBuffer(bString);

            fop.write(decodedBytes);
            fop.flush();
            fop.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }

        return file;
    }

}