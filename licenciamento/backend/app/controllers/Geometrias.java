package controllers;

import com.vividsolutions.jts.geom.Geometry;
import utils.GeoCalc;

public class Geometrias extends InternalController {

    public void calcularArea(Geometry geometria) {

        double area = GeoCalc.area(geometria) / 1e4;

        renderJSON(area);

    }

}
