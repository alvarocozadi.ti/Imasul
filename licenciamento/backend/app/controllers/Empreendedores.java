package controllers;

import br.ufla.lemaf.beans.*;

import models.Empreendimento;
import models.Pagination;
import security.Acao;
import serializers.EmpreendedorSerializer;
import serializers.EmpreendimentoSerializer;
import utils.Mensagem;
import utils.WebServiceEntradaUnica;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Empreendedores extends InternalController {

	public static void findByCpfCnpj(String cpfCnpj) {
		
		verificarPermissao(Acao.CADASTRAR_EMPREENDIMENTO);

		Empreendedor empreendedor = new Empreendedor();

		empreendedor.pessoa = WebServiceEntradaUnica.findPessoaByCpfCnpjEU(cpfCnpj) ;

		if (empreendedor.pessoa != null) {

			empreendedor.pessoa.enderecos.forEach(endereco -> {
				if(endereco.zonaLocalizacao.descricao.equals("Urbana") && endereco.numero == null){
					endereco.semNumero = true;
				}
			});
		}

		renderJSON(empreendedor, EmpreendedorSerializer.findByCpfCnpj);
		
	}

	public static void list(String pesquisa, Integer numeroPagina, Integer qtdItensPorPagina) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		FiltroEmpreendedor filtroEmpreendedor = new FiltroEmpreendedor();

		EmpreendedorFiltroResult empreendedores = WebServiceEntradaUnica.oAuthClient.getEmpreendedoresCadastrante(getUsuarioSessao().login);

		List<String> stringList = new ArrayList<>();
		empreendedores.pageItems.stream().filter(Objects::nonNull).forEach(empreendedor -> {
			stringList.add(empreendedor.pessoa.cpf != null ? empreendedor.pessoa.cpf : empreendedor.pessoa.cnpj);
		});
		filtroEmpreendedor.ordenacao = "NOME_DESC";

		if(stringList.size() == 0){
			filtroEmpreendedor.cpfsCnpjs = null;
			filtroEmpreendedor.cpfCnpj = getUsuarioSessao().login;
		}else{
			filtroEmpreendedor.cpfsCnpjs = stringList;
		}

		filtroEmpreendedor.busca = pesquisa;
		filtroEmpreendedor.tamanhoPagina = qtdItensPorPagina;
		filtroEmpreendedor.numeroPagina = numeroPagina;

		Pagination<Empreendedor> pagination = WebServiceEntradaUnica.listEmpreendedor(filtroEmpreendedor);

		renderJSON(pagination, EmpreendedorSerializer.listGrid);

	}
	public static void getEmpreendimentos(String cpfCnpj){

		List<Empreendimento> empreendimentosLicenciamento = Empreendimento.find("byCpfCnpj", cpfCnpj).fetch();

		FiltroEmpreendimento filtroEmpreendimento = new FiltroEmpreendimento();

		filtroEmpreendimento.ordenacao = "DENOMINACAO_ASC";

		filtroEmpreendimento.cpfCnpjEmpreendedor = cpfCnpj;

		filtroEmpreendimento.busca = cpfCnpj;
		filtroEmpreendimento.cpfCnpj = getUsuarioSessao().login;

		List<Empreendimento> lista = WebServiceEntradaUnica.getEmpreendimento(filtroEmpreendimento);

		lista.forEach(empreendimento ->
				empreendimento.possuiCaracterizacoes = empreendimento.temCaracterizacoes()
		);

		renderJSON(lista, EmpreendimentoSerializer.getEmpreendimento);
	}

	public static void getEmpreendedorCompleto(String cpfCnpj) {

		verificarPermissao(Acao.LISTAR_EMPREENDIMENTO);

		List<Empreendimento> empreendimentos = WebServiceEntradaUnica.findEmpreendimentosByCpfCnpj(cpfCnpj)
				.stream().filter(emp -> emp.id != null).collect(Collectors.toList());

		Empreendimento empreendimento = null;

		if (!empreendimentos.isEmpty()) {
			empreendimento = empreendimentos.get(0);
		}

		renderJSON(empreendimento, EmpreendimentoSerializer.getEmpreendedor);

	}

	public static void deleteEmpreendedor(Long idEmpreendedor) {

		WebServiceEntradaUnica.inativaEmpreendedor(idEmpreendedor);

		renderMensagem(Mensagem.EMPREENDEDOR_EXCLUIDO_SUCESSO);

	}

	public static void forcarAtualizacaoDadosRedesim(String cnpj) {

		verificarPermissao(Acao.CADASTRAR_EMPREENDIMENTO);

		Boolean aBoolean = WebServiceEntradaUnica.forcarAtualizacaoDadosRedesim(cnpj);

		renderJSON(aBoolean);

	}
	
}
