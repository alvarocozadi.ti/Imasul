# --!Ups


CREATE TABLE licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna (
    id serial primary key ,
    requisicao TEXT,
    licenca TEXT,
    numero INT,
    descricao TEXT,
    tipo_requisicao TEXT
);
INSERT INTO licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna (requisicao, licenca, numero, descricao, tipo_requisicao) VALUES
    ('REQ-822','LIO',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-822','LIO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-822','RLIO ',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-822','RLIO ',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-822','RLIO ',1,'Cronograma de instalação ou de operação conforme o caso','TRUE'),
    ('REQ-822','LO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-822','LO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-822','RLO',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-822','RLO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-823','LP',2,'RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS) ','TRUE'),
    ('REQ-823','LP',2,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-823','RLP',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-823','RLP',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-823','RLP',2,'Cronograma de instalação ','TRUE'),
    ('REQ-823','LO',2,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-823','RLO',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-823','RLO',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-824','LP',3,'ESTUDO AMBIENTAL PRELIMINAR (EAP)','TRUE'),
    ('REQ-824','RLP',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-824','RLP',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-824','LI',3,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-824','LI',3,'PLANO BÁSICO AMBIENTAL (PBA)','TRUE'),
    ('REQ-824','RLI',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-824','RLI',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-824','RLI',3,'Cronograma de instalação ','TRUE'),
    ('REQ-824','LO',3,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-824','RLO',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-824','RLO',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-831','LP',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-831','LP',1,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-831','LP',1,'PLANO BÁSICO AMBIENTAL (PBA)','TRUE'),
    ('REQ-831','RLP',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-831','RLP',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-831','RLP',1,'Cronograma de instalação ','TRUE'),
    ('REQ-831','LO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-831','RLO',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-831','RLO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-832','LP',2,'RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS) ','TRUE'),
    ('REQ-832','RLP',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-832','RLP',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-832','LI',2,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-832','LI',2,'PLANO BÁSICO AMBIENTAL (PBA)','TRUE'),
    ('REQ-832','RLI',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-832','RLI',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-832','RLI',2,'Cronograma de instalação ','TRUE'),
    ('REQ-832','LO',2,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-832','RLO',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-832','RLO',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-841','LP',2,'RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS) ','TRUE'),
    ('REQ-841','RLP',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-841','RLP',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-841','LI',2,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-841','LI',2,'PLANO BÁSICO AMBIENTAL (PBA)','TRUE'),
    ('REQ-841','RLI',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-841','RLI',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-841','RLI',2,'Cronograma de instalação ','TRUE'),
    ('REQ-841','LO',2,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-841','RLO',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-841','RLO',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-842','LP',3,'RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS) ','TRUE'),
    ('REQ-842','RLP',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-842','RLP',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-842','LI',3,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-842','LI',3,'PLANO BÁSICO AMBIENTAL (PBA)','TRUE'),
    ('REQ-842','RLI',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-842','RLI',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-842','RLI',3,'Cronograma de instalação ','TRUE'),
    ('REQ-842','LO',3,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-842','RLO',3,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-842','RLO',3,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-851','LIO',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-851','LIO',1,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-851','LIO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-851','RLIO ',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-851','RLIO ',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-851','RLIO ',1,'Cronograma de instalação ou de operação conforme o caso','TRUE'),
    ('REQ-851','LO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-851','LO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-851','RLO',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-851','RLO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-861','LIO',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-861','LIO',1,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-861','LIO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-861','RLIO ',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-861','RLIO ',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-861','RLIO ',1,'Cronograma de instalação ou de operação conforme o caso','TRUE'),
    ('REQ-861','LO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-861','LO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-861','RLO',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-861','RLO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-862','LP',2,'RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS) ','TRUE'),
    ('REQ-862','LP',2,'PROJETO EXECUTIVO (PE)','TRUE'),
    ('REQ-862','RLP',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-862','RLP',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-862','RLP',2,'Cronograma de instalação ','TRUE'),
    ('REQ-862','LO',2,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-862','RLO',2,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-862','RLO',2,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-871','AA',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-871','RAA',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-871','RAA',1,'CRONOGRAMA DAS CAMPANHAS','TRUE'),
    ('REQ-881','AA',1,'FORMULÁRIO DE TRANSPORTE DE FAUNA','TRUE'),
    ('REQ-891','AA',1,'FORMULÁRIO PARA CADASTRO DE ÁREA DE SOLTURA DE ANIMAIS SILVESTRES - ASAS','TRUE'),
    ('REQ-891','AA',1,'FORMULÁRIO DE SOLICITAÇÃO DE VISTORIA DE ÁREA DE SOLTURA DE ANIMAIS SILVESTRES - ASAS','TRUE'),
    ('REQ-891','RAA',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE'),
    ('REQ-8101','LIO',1,'PROPOSTA TÉCNICA AMBIENTAL (PTA)','TRUE'),
    ('REQ-8101','LIO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-8101','LO',1,'RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC) ','TRUE'),
    ('REQ-8101','LO',1,'Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)','TRUE'),
    ('REQ-8101','RLO',1,'RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA','TRUE');

INSERT INTO licenciamento.grupo_documento(codigo, descricao)
(SELECT DISTINCT trim(requisicao), trim(requisicao) FROM licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna ORDER BY 1);

INSERT INTO licenciamento.tipo_documento(nome, caminho_pasta, prefixo_nome_arquivo)
(SELECT DISTINCT trim(descricao), '', '' FROM licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna );

INSERT INTO licenciamento.rel_tipo_licenca_grupo_documento (id_grupo_documento, id_tipo_documento, id_tipo_licenca, obrigatorio )
   SELECT DISTINCT gd.id, td.id, tl.id,
    CASE
        WHEN trim(r1.tipo_requisicao) = 'TRUE' THEN TRUE
        WHEN trim(r1.tipo_requisicao) = 'FALSE' THEN FALSE
    END
  FROM licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna r1
  INNER JOIN licenciamento.grupo_documento gd ON trim(gd.codigo) = trim(r1.requisicao)
  INNER JOIN licenciamento.tipo_documento td ON trim(td.nome) = trim(r1.descricao)
  INNER JOIN licenciamento.tipo_licenca tl ON trim(tl.sigla) = trim(r1.licenca) ORDER BY 1, 2;

CREATE TABLE licenciamento.codigo_ambiental_versus_tipologia_fauna (
    id serial primary key ,
    Codigo_da_Resolucao TEXT,
    Codigo_da_Tipologia INT,
    Tipologia_da_Lei TEXT,
    Atividade TEXT,
    Gerencia TEXT,
    Urbana_ou_Rural TEXT,
    Tipo_de_estudo TEXT,
    PPD INT,
    CA_LIO TEXT,
    AA TEXT,
    LP TEXT,
    LI TEXT,
    LO TEXT,
    LIO TEXT,
    REVISAO TEXT,
    Porte_na_Lei TEXT,
    Limite_inferior TEXT,
    Parametros TEXT,
    Limite_superior TEXT,
    parametro_sigla TEXT,
    Requerimento_padrao TEXT,
    Geometria_da_propriedade TEXT,
    Feicao_geografica TEXT,
    Categoria_de_Impacto INT,
    Regras_Valores_Licenca TEXT,
    REQUISITO TEXT,
    Versao_do_Sistema TEXT,
    Observacoes INT
);
INSERT INTO licenciamento.codigo_ambiental_versus_tipologia_fauna(Codigo_da_Resolucao, Codigo_da_Tipologia, Tipologia_da_Lei, Atividade, Gerencia, Urbana_ou_Rural, Tipo_de_estudo, PPD, CA_LIO, AA, LP, LI, LO, LIO,  REVISAO, Porte_na_Lei, Limite_inferior, Parametros, Limite_superior, parametro_sigla,  Requerimento_padrao, Geometria_da_propriedade, Feicao_geografica, Categoria_de_Impacto, Regras_Valores_Licenca, REQUISITO, Versao_do_Sistema, Observacoes) VALUES
    ('822a',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE PEQUENO) - CUTIA (Dasyprocta agouti) e PACA (Cuniculus paca) -  até 100 animais','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','Quantidade de Animais (Un) < 100','100','QA','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-822','v1',NULL),
    ('822b',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE PEQUENO) - QUEIXADA (Tayassu peceari), CATETO (Pecari tacaju) e CAPIVARA (Hidrochoerus hidrochoaeris) - até 250 animais','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','Quantidade de Animais (Un) < 250','250','QA','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-822','v1',NULL),
    ('822c',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE PEQUENO) - EMA(Rhea americana) - até 500 animais','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','Quantidade de Animais (Un) < 500','500','QA','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-822','v1',NULL),
    ('822d',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE PEQUENO) - JACARÉ DO PANTANAL(Caiman yacare) e JACARÉ DO PAPO AMARELO (Caiman latirostris) - até 750 animais','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','Quantidade de Animais (Un) < 500','750','QA','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-822','v1',NULL),
    ('823a',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE MÉDIO) - CUTIA(Dasyprocta agouti) e PACA (Cuniculus paca) - Acima de 100 e até 500 animais','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','-','S','-','LP/LO','VI/UFERMS','100','100 < Quantidade de animais (Un) <= 500','500','QA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-823','v1',NULL),
    ('823b',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE MÉDIO) - QUEIXADA (Tayassu peceari), CATETO (Pecari tacaju) e CAPIVARA (Hidrochoerus hidrochoaeris) - Acima de 250 e até 500 animais','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','-','S','-','LP/LO','VI/UFERMS','250','250 < Quantidade de animais (Un) <= 500','500','QA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-823','v1',NULL),
    ('823c',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE MÉDIO) - EMA (Rhea americana) - Acima de 500 e até 1.500 animais','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','-','S','-','LP/LO','VI/UFERMS','500','500 < Quantidade de animais (Un) <= 1500','1500','QA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-823','v1',NULL),
    ('823d',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE MÉDIO) - JACARÉ DO PANTANAL( Caiman yacare) e JACARÉ DO PAPO AMARELO (Caiman latirostris) - Acima de 750 até 5.000 animais','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','-','S','-','LP/LO','VI/UFERMS','750','750 < Quantidade de animais (Un) <= 5000','5000','QA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-823','v1',NULL),
    ('823e',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE MÉDIO) - Serpentes peçonhentas para coleta de veneno','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,NULL,NULL,'S',NULL,'S',NULL,'LP/LO','VI/UFERMS','-','-','-','QA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-823','v1',NULL),
    ('824a',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE GRANDE) - CUTIA(Dasyprocta agouti) e PACA (Cuniculus paca) - Acima de 500 animais','GPF/UNIFAUNA','Urbana ou Rural','EAP',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','500','Quantidade de animais (Un) > 500','-','QA','formulario principal','Dentro','POLIGONO',3,'Tab-Taxas','REQ-824','v1',NULL),
    ('824b',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE GRANDE) - QUEIXADA (Tayassu peceari), CATETO (Pecari tacaju) e CAPIVARA (Hidrochoerus hidrochoaeris) - Acima de  500 animais','GPF/UNIFAUNA','Urbana ou Rural','EAP',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','500','Quantidade de animais (Un) > 500','-','QA','formulario principal','Dentro','POLIGONO',3,'Tab-Taxas','REQ-824','v1',NULL),
    ('824c',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE GRANDE) - EMA (Rhea americana) -Acima de  1.500 animais','GPF/UNIFAUNA','Urbana ou Rural','EAP/PBA',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','1500','Quantidade de animais (Un) > 1500','-','QA','formulario principal','Dentro','POLIGONO',3,'Tab-Taxas','REQ-824','v1',NULL),
    ('824d',8,'FAUNA','CRIADOURO COMERCIAL Fauna Silvestre (PORTE GRANDE) - JACARÉ DO PANTANAL( Caiman yacare) e JACARÉ DO PAPO AMARELO (Caiman latirostris) - Acima de 5.000 animais','GPF/UNIFAUNA','Urbana ou Rural','EAP',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','5000','Quantidade de animais (Un) > 5000','-','QA','formulario principal','Dentro','POLIGONO',3,'Tab-Taxas','REQ-824','v1',NULL),
    ('831',8,'FAUNA','Centro de Triagem e Reabilitação de Animais Silvestres (CETRAS) - capacidade de recebimento até 800 animais/ano','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','S','S','S','-','LP/LO','VI/UFERMS','-','Capacidade de recebimento de animais (Un/Ano) <= 800','800','CRA','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-831','v1',NULL),
    ('832',8,'FAUNA','Centro de Triagem e Reabilitação de Animais Silvestres (CETRAS) - capacidade de recebimento acima de 800 animais/ano','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','800','Capacidade de recebimento de animais (Un/Ano) > 800','-','CRA','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-832','v1',NULL),
    ('841',8,'FAUNA','JARDIM ZOOLÓGICO/AQUÁRIO – área construída até a 10.000,00 m², ou capacidade para visitação até 2.000 pessoas/dia','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','-','Área Construída (m²) <= 10000
Capacidade de visitação (Un/Dia) <= 2000','10000, 2000','AC, CV','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-841','v1',NULL),
    ('842',8,'FAUNA','JARDIM ZOOLÓGICO/AQUÁRIO - Somatório da área construída superior a 10.000,00m² ou capacidade para visitação superior a 2.000 pessoa/dia','GPF/UNIFAUNA','Urbana ou Rural','EAP',3,'-','-','S','S','S','-','LP/LI/LO','VI/UFERMS','10000, 2000','Área Construída (m²) > 10000
Capacidade de visitação (Un/Dia) > 2000','-','AC, CV','formulario principal','Dentro','POLIGONO',3,'Tab-Taxas','REQ-842','v1',NULL),
    ('851',8,'FAUNA','MANTENEDOR DE FAUNA','GPF/UNIFAUNA','Urbana ou Rural','PTA',0,'-','-','-','-','-','S','LIO','VI/UFERMS','-','-','-','-','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-851','v1',NULL),
    ('861',8,'FAUNA','CRIADOURO CIENTÍFICO e/ou CRIADOURO CONSERVACIONISTA - Área útil até 10.000m²','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','Área útil <= 10000','10000','AU','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-861','v1',NULL),
    ('862',8,'FAUNA','CRIADOURO CIENTÍFICO e/ou  CRIADOURO CONSERVACIONISTA - Área  útil acima de 10.000 m²','GPF/UNIFAUNA','Urbana ou Rural','RAS',3,'-','-','S','-','S','-','LP/LO','VI/UFERMS','10000','Área útil > 10000','-','AU','formulario principal','Dentro','POLIGONO',2,'Tab-Taxas','REQ-862','v1',NULL),
    ('871',8,'FAUNA','MANEJO DE FAUNA IN SITU ','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','PONTO',1,'Tab-Taxas','REQ-871','v1',NULL),
    ('881',8,'FAUNA','TRANSPORTE DE FAUNA SILVESTRE','GPF/UNIFAUNA','Urbana ou Rural','FORMULÁRIO',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','PONTO',1,'Tab-Taxas','REQ-881','v1',NULL),
    ('891',8,'FAUNA','Áreas de Soltura de Animais Silvestres (ASAS)','GPF/UNIFAUNA','Urbana ou Rural','FORMULÁRIOS',0,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','POLIGONO',1,'Tab-Taxas','REQ-891','v1',NULL),
    ('8101',8,'FAUNA','Empreendimento comercial de animais vivos da fauna silvestre nativa e/ou fauna exótica – Revenda','GPF/UNIFAUNA','Urbana ou Rural','PTA',3,'-','-','-','-','-','S','LIO','VI/UFERMS','-','-','-','-','formulario principal','Dentro','PONTO',1,'Tab-Taxas','REQ-8101','v1',NULL);

INSERT INTO licenciamento.tipologia (id,nome, ativo) VALUES
(7, 'FAUNA', true);

CREATE OR REPLACE FUNCTION f_Converte_Str_Int(columnConvert character varying)
RETURNS integer AS
$BODY$
SELECT CASE WHEN trim($1) SIMILAR TO '[0-9]+'
      THEN CAST(trim($1) AS integer)
  ELSE NULL END;
$BODY$
LANGUAGE 'sql' IMMUTABLE STRICT;

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_fauna
    ALTER COLUMN ppd TYPE integer USING f_Converte_Str_Int(ppd::character varying);

INSERT INTO licenciamento.atividade (v1, ativo, id_potencial_poluidor, nome, id_tipologia, geo_linha, geo_ponto, geo_poligono, codigo, licenciamento_municipal,
limite_parametro_municipal, limite_inferior_simplificado, limite_superior_simplificado, observacoes,
sigla_setor, 
dentro_empreendimento, tipo_estudo)
SELECT
  true as v1,
  true as ativo,
  pp.id as id_potencial_poluidor,
  trim(atividade) as nome,
  t.id as id_tipologia,
  unaccent(trim(feicao_geografica)) ilike '%linha%' as geo_linha,
  unaccent(trim(feicao_geografica)) ilike '%ponto%' as geo_ponto,
  unaccent(trim(feicao_geografica)) ilike '%poligono%' as geo_poligono,  
  codigo_da_resolucao as codigo,
  null as licenciamento_municipal, 
  null as limite_parametro_municipal,
  null as limite_inferior_simplificado, 
  null as limite_superior_simplificado,
  null as observacoes,
  gerencia as sigla_setor,
  false as dentro_empreendimento,
  trim(tipo_de_estudo)
FROM licenciamento.codigo_ambiental_versus_tipologia_fauna f
INNER JOIN licenciamento.tipologia t ON unaccent(trim(f.tipologia_da_lei)) = unaccent(trim(t.nome))
INNER JOIN licenciamento.potencial_poluidor pp ON f.ppd = pp.valor
ORDER BY codigo_da_resolucao;

UPDATE licenciamento.atividade a
SET dentro_empreendimento = true
WHERE trim(a.nome) IN
      (SELECT trim(atividade) FROM licenciamento.codigo_ambiental_versus_tipologia_fauna
        WHERE geometria_da_propriedade = 'Dentro') and a.id_tipologia in
        (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

ALTER TABLE licenciamento.atividade ADD COLUMN requisito_temp text;

UPDATE licenciamento.atividade a
SET requisito_temp = (SELECT f.requisito
                      FROM  licenciamento.codigo_ambiental_versus_tipologia_fauna f
                      WHERE replace(trim(unaccent(f.atividade)), '  ', ' ') = replace(trim(unaccent(a.nome)), '  ', ' ')
                      AND replace(trim(unaccent(a.codigo)), '  ', ' ') = replace(trim(unaccent(f.codigo_da_resolucao)), '  ', ' '))
WHERE a.id_tipologia in
        (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');


UPDATE licenciamento.atividade a
SET id_grupo_documento = (SELECT gd.id
                          FROM licenciamento.grupo_documento gd
                          WHERE trim(unaccent(a.requisito_temp)) = trim(unaccent(gd.codigo))
                          AND trim(unaccent(a.requisito_temp)) = trim(unaccent(gd.descricao)))
WHERE a.id_tipologia in
        (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

ALTER TABLE licenciamento.atividade DROP COLUMN requisito_temp;

INSERT INTO licenciamento.rel_atividade_tipo_atividade (id_atividade, id_tipo_atividade)
SELECT a.id, ta.id
FROM licenciamento.atividade a
FULL OUTER JOIN licenciamento.tipo_atividade ta ON ta.id IS NOT NULL
WHERE a.ativo AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

INSERT INTO licenciamento.rel_atividade_tipo_licenca (id_atividade, id_tipo_licenca)
WITH atl AS (
    SELECT f.id as id, regexp_split_to_table(revisao, '\/') as sigla, trim(f.codigo_da_resolucao) as codigo_atividade,
           trim(atividade) as nome
    FROM licenciamento.codigo_ambiental_versus_tipologia_fauna f
)
SELECT DISTINCT a.id as id_atividade, tp.id as id_tipo_licenca FROM licenciamento.tipo_licenca tp
INNER JOIN atl ON tp.sigla  = trim(atl.sigla)
INNER JOIN licenciamento.atividade a ON a.codigo = atl.codigo_atividade AND a.nome = atl.nome;

INSERT INTO licenciamento.rel_atividade_tipo_licenca (id_atividade, id_tipo_licenca)
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RLP') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'LP' AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA')
UNION ALL
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RLI') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'LI' AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA')
UNION ALL
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RLO') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'LO' AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA')
UNION ALL
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RLIO') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'LIO' AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA')
UNION ALL
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RAA') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'AA' AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

INSERT INTO licenciamento.rel_atividade_parametro_atividade (id_atividade, id_parametro_atividade)
WITH ps AS (
    SELECT trim(regexp_split_to_table(f.parametro_sigla, ',')) as parametro_sigla, trim(codigo_da_resolucao) as codigo, trim(atividade) as nome
    FROM licenciamento.codigo_ambiental_versus_tipologia_fauna f
)
SELECT DISTINCT a.id as id_atividade, pa.id as id_parametro_atividade FROM ps ps
INNER JOIN licenciamento.parametro_atividade pa ON ps.parametro_sigla = pa.codigo
INNER JOIN licenciamento.atividade a ON a.codigo = ps.codigo AND unaccent(trim(ps.nome)) = unaccent(trim(a.nome));

INSERT INTO licenciamento.rel_atividade_parametro_atividade
  SELECT DISTINCT
    a.id,
    (SELECT pa.id FROM licenciamento.parametro_atividade pa WHERE codigo = 'VDI'),
    'Valor de Investimento (R$)'
  FROM licenciamento.atividade a
  WHERE a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

INSERT INTO licenciamento.rel_atividade_porte_atividade
  SELECT a.id, pa.id
  FROM licenciamento.porte_atividade pa, licenciamento.atividade a
  WHERE a.id IN (
    SELECT id_atividade
    FROM licenciamento.rel_atividade_parametro_atividade rapa
    INNER JOIN licenciamento.atividade a ON rapa.id_atividade = a.id
    WHERE rapa.id_parametro_atividade = (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'VDI')
      AND a.id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA')
  );

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_fauna
    ALTER COLUMN limite_inferior TYPE integer USING f_Converte_Str_Int(limite_inferior::character varying);

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_fauna
    ALTER COLUMN limite_superior TYPE integer USING f_Converte_Str_Int(limite_superior::character varying);

INSERT INTO licenciamento.atividade_parametro_limites (id_atividade, limite_inferior, limite_superior, id_parametro_atividade)
SELECT
  a.id as id_atividade,
  f.limite_inferior,
  f.limite_superior,
  pa.id as id_parametro_atividade
FROM licenciamento.codigo_ambiental_versus_tipologia_fauna f 
INNER JOIN licenciamento.atividade a ON unaccent(trim(f.atividade)) = unaccent(trim(a.nome))
INNER JOIN licenciamento.parametro_atividade pa ON unaccent(trim(f.parametro_sigla)) = unaccent(trim(pa.codigo))
WHERE codigo_da_resolucao <> '841' and codigo_da_resolucao <> '842';

INSERT INTO licenciamento.atividade_parametro_limites (id_atividade, limite_inferior, limite_superior, id_parametro_atividade) VALUES
((SELECT id FROM licenciamento.atividade WHERE codigo = '841'), null, 10000, (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'AC')),
((SELECT id FROM licenciamento.atividade WHERE codigo = '841'), null, 2000, (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'CV')),
((SELECT id FROM licenciamento.atividade WHERE codigo = '842'), 10000, null, (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'AC')),
((SELECT id FROM licenciamento.atividade WHERE codigo = '842'), 2000, null, (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'CV'));

DROP TABLE licenciamento.codigo_ambiental_versus_tipologia_fauna;
DROP TABLE licenciamento.grupo_de_requisitos_documentos_tecnicos_v2_6_fauna ;

--atividades licenca AA serao V2
UPDATE licenciamento.atividade SET v1 = false where codigo in ('871','881','891'); 

# ---!Downs

UPDATE licenciamento.atividade SET v1 = true where codigo in ('871','881','891'); 

DELETE FROM licenciamento.rel_atividade_tipo_licenca
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE id_tipologia IN
	(SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA'));

DELETE FROM licenciamento.rel_atividade_tipo_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE id_tipologia IN
	(SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA'));

DELETE FROM licenciamento.rel_atividade_parametro_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE id_tipologia IN
	(SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA'));

DELETE FROM licenciamento.rel_atividade_porte_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE id_tipologia IN
	(SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA'));

DELETE FROM licenciamento.atividade
WHERE id_tipologia IN (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');

DELETE FROM licenciamento.rel_tipo_licenca_grupo_documento 
WHERE id_grupo_documento IN (SELECT id FROM licenciamento.grupo_documento 
							 WHERE codigo IN ('REQ-8101','REQ-822','REQ-823','REQ-824','REQ-831','REQ-832',
							 'REQ-841','REQ-842','REQ-851','REQ-861','REQ-862','REQ-871','REQ-881','REQ-891') 
							);

UPDATE licenciamento.atividade 
SET id_grupo_documento = NULL 
WHERE id_grupo_documento IN (SELECT id FROM licenciamento.grupo_documento 
							 WHERE codigo IN ('REQ-8101','REQ-822','REQ-823','REQ-824','REQ-831','REQ-832',
							 'REQ-841','REQ-842','REQ-851','REQ-861','REQ-862','REQ-871','REQ-881','REQ-891') 
							);

DELETE FROM licenciamento.grupo_documento 
WHERE codigo IN ('REQ-8101','REQ-822','REQ-823','REQ-824','REQ-831','REQ-832','REQ-841','REQ-842','REQ-851',
				 'REQ-861','REQ-862','REQ-871','REQ-881','REQ-891');

DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('FORMULÁRIO DE SOLICITAÇÃO DE VISTORIA DE ÁREA DE SOLTURA DE ANIMAIS SILVESTRES - ASAS'));

DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('PROJETO EXECUTIVO (PE)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('PROPOSTA TÉCNICA AMBIENTAL (PTA)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('ESTUDO AMBIENTAL PRELIMINAR (EAP)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('FORMULÁRIO DE TRANSPORTE DE FAUNA'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('RELATÓRIO TÉCNICO DE CONCLUSÃO (RTC)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('PLANO BÁSICO AMBIENTAL (PBA)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('RELATÓRIO DE ATENDIMENTO DE CONDICIONANTES DA LICENÇA A SER RENOVADA'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('Cronograma de instalação ou de operação conforme o caso'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('Cronograma de instalação'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('Acompanhado de levantamento fotográfico da área diretamente afetada e das estruturas pertinentes a atividade (quando existirem)'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('FORMULÁRIO PARA CADASTRO DE ÁREA DE SOLTURA DE ANIMAIS SILVESTRES - ASAS'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('CRONOGRAMA DAS CAMPANHAS'));
				
DELETE FROM licenciamento.tipo_documento 
WHERE id IN (SELECT max(id) FROM licenciamento.tipo_documento WHERE nome IN ('RELATÓRIO AMBIENTAL SIMPLIFICADO (RAS)'));
