# --!Ups

UPDATE licenciamento.atividade SET v1 = true WHERE codigo IN ('871', '881', '891');

CREATE TABLE licenciamento.codigo_ambiental_versus_tipologia_licenca_AA (
    id serial primary key ,
    Codigo_da_Resolucao TEXT,
    Codigo_da_Tipologia INT,
    Tipologia_da_Lei TEXT,
    Atividade TEXT,
    Gerencia TEXT,
    Urbana_ou_Rural TEXT,
    Tipo_de_estudo TEXT,
    PPD INT,
    CA_LIO TEXT,
    AA TEXT,
    LP TEXT,
    LI TEXT,
    LO TEXT,
    LIO TEXT,
    REVISAO TEXT,
    Porte_na_Lei TEXT,
    Limite_inferior TEXT,
    Parametros TEXT,
    Limite_superior TEXT,
    parametro_sigla TEXT,
    Requerimento_padrao TEXT,
    Geometria_da_propriedade TEXT,
    Feicao_geografica TEXT,
    Categoria_de_Impacto INT,
    Regras_Valores_Licenca TEXT,
    REQUISITO TEXT,
    Versao_do_Sistema TEXT
);
INSERT INTO licenciamento.codigo_ambiental_versus_tipologia_licenca_AA (Codigo_da_Resolucao, Codigo_da_Tipologia, Tipologia_da_Lei, Atividade, Gerencia, Urbana_ou_Rural, Tipo_de_estudo, PPD, CA_LIO, AA, LP, LI, LO, LIO,  REVISAO, Porte_na_Lei, Limite_inferior, Parametros, Limite_superior, parametro_sigla,  Requerimento_padrao, Geometria_da_propriedade, Feicao_geografica, Categoria_de_Impacto, Regras_Valores_Licenca, REQUISITO, Versao_do_Sistema) VALUES 
    (2561,2,'INFRAESTRUTURA','DRAGAGEM para manutenção de reservatórios em barragens','GLA/AGROINFRA','Urbana ou Rural','PTA',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','POLIGONO',1,'Tab-Taxas','REQ-67','v2'),
    (2562,2,'INFRAESTRUTURA','DRAGAGEM de curso d''água','GLA/AGROINFRA','Urbana ou Rural','EAP',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','POLIGONO',3,'Tab-Taxas','REQ-68','v2'),
    (4121,4,'MINERAÇÃO','RECUPERAÇÃO DE ÁREA DEGRADADA POR MINERAÇÃO','GLA/MINEROTUR','Urbana ou Rural','PTA',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal + formulario mineração','Dentro e fora','POLIGONO',1,'Tab-Taxas','REQ-164','v2'),
    (7311,7,'SANEAMENTO/RESÍDUOS SÓLIDOS','RECUPERAÇÃO DE ÁREA DEGRADADA POR DISPOSIÇÃO INADEQUADA DE RESÍDUOS SÓLIDOS, ENCERRAMENTO DE ATIVIDADE DE ATERRO SANITÁRIO, OU POR CONTAMINAÇÃO DO SOLO E/OU ÁGUA SUBTERRÂNEA (Situações de passivo ambiental em decorrência de produtos ou resíduos perigosos contaminantes de solo e água)','GLA/UNISAN','Urbana ou Rural','PTA',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro e fora','POLIGONO',1,'Tab-Taxas','REQ-403','v2'),
    (61151,6,' INDUSTRIAL','DESATIVAÇÃO DE COMERCIO DE COMBUSÍVEL COM SASC;  E/OU RETIRADA DO SASC  ','GLA/UNIIND','Urbana ou Rural','PTA',3,'-','S','-','-','-','-','AA','VI/UFERMS','-','-','-','-','formulario principal','Dentro','POLIGONO',1,'Tab-Taxas','REQ-372','v2');

CREATE OR REPLACE FUNCTION f_Converte_Str_Int(columnConvert character varying)
RETURNS integer AS
$BODY$
SELECT CASE WHEN trim($1) SIMILAR TO '[0-9]+'
      THEN CAST(trim($1) AS integer)
  ELSE NULL END;
$BODY$
LANGUAGE 'sql' IMMUTABLE STRICT;

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_licenca_AA
    ALTER COLUMN ppd TYPE integer USING f_Converte_Str_Int(ppd::character varying);

INSERT INTO licenciamento.atividade (v1, ativo, id_potencial_poluidor, nome, id_tipologia, geo_linha, geo_ponto, geo_poligono, codigo, licenciamento_municipal,
limite_parametro_municipal, limite_inferior_simplificado, limite_superior_simplificado, observacoes,
sigla_setor, 
dentro_empreendimento, tipo_estudo)
SELECT
  true as v1,
  true as ativo,
  pp.id as id_potencial_poluidor,
  trim(atividade) as nome,
  t.id as id_tipologia,
  unaccent(trim(feicao_geografica)) ilike '%linha%' as geo_linha,
  unaccent(trim(feicao_geografica)) ilike '%ponto%' as geo_ponto,
  unaccent(trim(feicao_geografica)) ilike '%poligono%' as geo_poligono,  
  codigo_da_resolucao as codigo,
  null as licenciamento_municipal, 
  null as limite_parametro_municipal,
  null as limite_inferior_simplificado, 
  null as limite_superior_simplificado,
  null as observacoes,
  gerencia as sigla_setor,
  false as dentro_empreendimento,
  trim(tipo_de_estudo)
FROM licenciamento.codigo_ambiental_versus_tipologia_licenca_AA f
INNER JOIN licenciamento.tipologia t ON unaccent(trim(f.tipologia_da_lei)) = unaccent(trim(t.nome))
INNER JOIN licenciamento.potencial_poluidor pp ON f.ppd = pp.valor
ORDER BY codigo_da_resolucao;

UPDATE licenciamento.atividade a
SET dentro_empreendimento = true
WHERE trim(a.nome) IN
      (SELECT trim(atividade) FROM licenciamento.codigo_ambiental_versus_tipologia_licenca_AA
        WHERE geometria_da_propriedade = 'Dentro')
      AND a.codigo IN ('2561','2562','4121','7311','61151');

ALTER TABLE licenciamento.atividade ADD COLUMN requisito_temp text;

UPDATE licenciamento.atividade a
SET requisito_temp = (SELECT f.requisito
                      FROM  licenciamento.codigo_ambiental_versus_tipologia_licenca_AA f
                      WHERE replace(trim(unaccent(f.atividade)), '  ', ' ') = replace(trim(unaccent(a.nome)), '  ', ' ')
                      AND replace(trim(unaccent(a.codigo)), '  ', ' ') = replace(trim(unaccent(f.codigo_da_resolucao)), '  ', ' '))
WHERE a.codigo IN ('2561','2562','4121','7311','61151');

UPDATE licenciamento.atividade a
SET id_grupo_documento = (SELECT gd.id
                          FROM licenciamento.grupo_documento gd
                          WHERE trim(unaccent(a.requisito_temp)) = trim(unaccent(gd.codigo))
                          AND trim(unaccent(a.requisito_temp)) = trim(unaccent(gd.descricao)))
WHERE a.codigo IN ('2561','2562','4121','7311','61151');

ALTER TABLE licenciamento.atividade DROP COLUMN requisito_temp;

INSERT INTO licenciamento.rel_atividade_tipo_atividade (id_atividade, id_tipo_atividade)
SELECT a.id, ta.id
FROM licenciamento.atividade a
FULL OUTER JOIN licenciamento.tipo_atividade ta ON ta.id IS NOT NULL
WHERE a.ativo AND a.codigo IN ('2561','2562','4121','7311','61151');

INSERT INTO licenciamento.rel_atividade_tipo_licenca (id_atividade, id_tipo_licenca)
WITH atl AS (
    SELECT f.id as id, regexp_split_to_table(revisao, '\/') as sigla, trim(f.codigo_da_resolucao) as codigo_atividade,
           trim(atividade) as nome
    FROM licenciamento.codigo_ambiental_versus_tipologia_licenca_AA f
)
SELECT DISTINCT a.id as id_atividade, tp.id as id_tipo_licenca FROM licenciamento.tipo_licenca tp
INNER JOIN atl ON tp.sigla  = trim(atl.sigla)
INNER JOIN licenciamento.atividade a ON a.codigo = atl.codigo_atividade AND a.nome = atl.nome;

INSERT INTO licenciamento.rel_atividade_tipo_licenca (id_atividade, id_tipo_licenca)
SELECT ratl.id_atividade, (SELECT id FROM licenciamento.tipo_licenca tl2 WHERE tl2.sigla = 'RAA') FROM licenciamento.rel_atividade_tipo_licenca ratl
INNER JOIN licenciamento.atividade a ON ratl.id_atividade = a.id
INNER JOIN licenciamento.tipo_licenca tl ON ratl.id_tipo_licenca = tl.id
WHERE tl.sigla = 'AA' AND a.codigo IN ('2561','2562','4121','7311','61151');

INSERT INTO licenciamento.rel_atividade_parametro_atividade
  SELECT DISTINCT
    a.id,
    (SELECT pa.id FROM licenciamento.parametro_atividade pa WHERE codigo = 'VDI'),
    'Valor de Investimento (R$)'
  FROM licenciamento.atividade a
  WHERE a.codigo IN ('2561','2562','4121','7311','61151');

INSERT INTO licenciamento.rel_atividade_porte_atividade
  SELECT a.id, pa.id
  FROM licenciamento.porte_atividade pa, licenciamento.atividade a
  WHERE a.id IN (
    SELECT id_atividade
    FROM licenciamento.rel_atividade_parametro_atividade rapa
    INNER JOIN licenciamento.atividade a ON rapa.id_atividade = a.id
    WHERE rapa.id_parametro_atividade = (SELECT id FROM licenciamento.parametro_atividade WHERE codigo = 'VDI')
      AND a.codigo IN ('2561','2562','4121','7311','61151')
  );

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_licenca_AA
    ALTER COLUMN limite_inferior TYPE integer USING f_Converte_Str_Int(limite_inferior::character varying);

ALTER TABLE licenciamento.codigo_ambiental_versus_tipologia_licenca_AA
    ALTER COLUMN limite_superior TYPE integer USING f_Converte_Str_Int(limite_superior::character varying);

DROP TABLE licenciamento.codigo_ambiental_versus_tipologia_licenca_AA;


# ---!Downs

DELETE FROM licenciamento.rel_atividade_tipo_licenca
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151'));

DELETE FROM licenciamento.rel_atividade_tipo_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151'));

DELETE FROM licenciamento.rel_atividade_parametro_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151'));

DELETE FROM licenciamento.rel_atividade_porte_atividade
WHERE id_atividade IN (SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151'));

DELETE FROM licenciamento.atividade
WHERE id_tipologia IN (SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151');

DELETE FROM licenciamento.rel_tipo_licenca_grupo_documento 
WHERE id_grupo_documento IN (SELECT id FROM licenciamento.grupo_documento 
							 WHERE codigo IN ('REQ-67','REQ-68', 'REQ-164', 'REQ-403', 'REQ-372') 
							);

UPDATE licenciamento.atividade 
SET id_grupo_documento = NULL 
WHERE id_grupo_documento IN (SELECT id FROM licenciamento.grupo_documento 
							 WHERE codigo IN ('REQ-67','REQ-68', 'REQ-164', 'REQ-403', 'REQ-372') 
							);

DELETE FROM licenciamento.grupo_documento 
WHERE codigo IN ('REQ-67','REQ-68', 'REQ-164', 'REQ-403', 'REQ-372');

UPDATE licenciamento.atividade SET v1 = false where codigo in ('871','881','891'); 
