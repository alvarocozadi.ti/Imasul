# --- !Ups

ALTER TABLE licenciamento.tipo_licenca ADD COLUMN ativo boolean DEFAULT TRUE;
COMMENT ON COLUMN licenciamento.tipo_licenca.ativo is 'Indica se a licença está ativa.';

UPDATE licenciamento.tipo_licenca SET ativo = FALSE WHERE sigla in ('AA', 'CA', 'RAA');

DELETE FROM licenciamento.rel_tipo_licenca_grupo_documento WHERE id_tipo_documento IN
(SELECT max(id) FROM licenciamento.tipo_documento WHERE nome = 'PROPOSTA TÉCNICA AMBIENTAL (PTA)');

DELETE FROM licenciamento.tipo_documento WHERE id =
(SELECT max(id) FROM licenciamento.tipo_documento WHERE nome = 'PROPOSTA TÉCNICA AMBIENTAL (PTA)');


# --- !Downs

-- Fazer backup da tabela licenciamento.tipo_documento e licenciamento.rel_tipo_licenca_grupo_documento 
SELECT * FROM licenciamento.rel_tipo_licenca_grupo_documento WHERE id_tipo_documento IN
(SELECT max(id) FROM licenciamento.tipo_documento WHERE nome = 'PROPOSTA TÉCNICA AMBIENTAL (PTA)');

SELECT * FROM licenciamento.tipo_documento WHERE id =
(SELECT max(id) FROM licenciamento.tipo_documento WHERE nome = 'PROPOSTA TÉCNICA AMBIENTAL (PTA)');

ALTER TABLE licenciamento.tipo_licenca DROP COLUMN ativo;


