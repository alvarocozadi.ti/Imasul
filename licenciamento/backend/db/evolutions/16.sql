# --!Ups

UPDATE licenciamento.atividade SET tipo_estudo = 'PTA' WHERE codigo IN ('881', '891');

INSERT INTO licenciamento.tipo_documento(nome, caminho_pasta, prefixo_nome_arquivo) VALUES
('Carta de aceite da instituição receptora dos espécimes e/ou material biológico (quando couber)', '', '');

INSERT INTO licenciamento.tipo_documento_tipo_licenca (id_tipo_documento, id_tipo_licenca, obrigatorio ) VALUES
   ((SELECT id FROM licenciamento.tipo_documento WHERE nome = 'Carta de aceite da instituição receptora dos espécimes e/ou material biológico (quando couber)'),
    (SELECT id FROM licenciamento.tipo_licenca WHERE sigla = 'AA'),
    FALSE );


# ---!Downs

DELETE FROM	licenciamento.tipo_documento WHERE nome = 'Carta de aceite da instituição receptora dos espécimes e/ou material biológico (quando couber)'; 

DELETE FROM licenciamento.tipo_documento_tipo_licenca WHERE 
id_tipo_documento = (SELECT id FROM licenciamento.tipo_documento WHERE nome = 'Carta de aceite da instituição receptora dos espécimes e/ou material biológico (quando couber)') AND 
id_tipo_licenca = (SELECT id FROM licenciamento.tipo_licenca WHERE sigla = 'AA');

UPDATE licenciamento.atividade SET tipo_estudo = 'FORMULÁRIO' WHERE codigo = '881';

UPDATE licenciamento.atividade SET tipo_estudo = 'FORMULÁRIOS' WHERE codigo = '881', '891');
