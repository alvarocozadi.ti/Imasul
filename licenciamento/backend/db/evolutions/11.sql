# --!Ups

--IMPORTANTE!!!
--Pegar o resultado desse select e rodar no ambiente em que o mesmo foi executado para gerar os inserts
-- copiar o resultado do select e rodar o insert manualmente
SELECT
'INSERT INTO licenciamento.tipo_caracterizacao_atividade (id_atividade, dispensa_licenciamento, licenciamento_simplificado, licenciamento_declaratorio) VALUES ('||id||', FALSE, TRUE, FALSE);'
FROM licenciamento.atividade WHERE id_tipologia = (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA');


# ---!Downs

DELETE FROM licenciamento.tipo_caracterizacao_atividade WHERE id_atividade IN 
(SELECT id FROM licenciamento.atividade WHERE id_tipologia IN
 (SELECT id FROM licenciamento.tipologia WHERE nome = 'FAUNA'));