# --- !Ups
CREATE DATABASE licenciamento_ms
  WITH TEMPLATE template1;

-- executar daqui em diante de dentro do banco licenciamento_ms
CREATE ROLE licenciamento_ms LOGIN
  ENCRYPTED PASSWORD 'licenciamento_ms'
  SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;


CREATE SCHEMA licenciamento;
CREATE SCHEMA correios;

ALTER SCHEMA licenciamento OWNER TO postgres;
ALTER SCHEMA correios OWNER TO postgres;

GRANT USAGE ON SCHEMA public TO licenciamento_ms;
GRANT USAGE ON SCHEMA licenciamento TO licenciamento_ms;
GRANT USAGE ON SCHEMA correios TO licenciamento_ms;

ALTER DEFAULT PRIVILEGES FOR USER licenciamento_ms IN SCHEMA licenciamento, correios
    GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO licenciamento_ms;

ALTER DEFAULT PRIVILEGES FOR USER licenciamento_ms IN SCHEMA licenciamento, correios
    GRANT SELECT, USAGE ON SEQUENCES TO licenciamento_ms;


# --- !Downs

DROP SCHEMA licenciamento CASCADE;
DROP SCHEMA correios CASCADE;

DROP ROLE licenciamento_ms;

DROP DATABASE licenciamento_ms;
  