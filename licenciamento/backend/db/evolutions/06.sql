# --- !Ups

-- atualizando status de notificação
UPDATE licenciamento.status_caracterizacao SET nome = 'Notificação de pendência' WHERE codigo = 'NOTIFICADO';


# --- !Downs

UPDATE licenciamento.status_caracterizacao SET nome = 'Notificado' WHERE codigo = 'NOTIFICADO';
