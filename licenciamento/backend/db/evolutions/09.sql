# --!Ups

DELETE FROM licenciamento.rel_tipo_sobreposicao_orgao;
DELETE FROM licenciamento.tipo_sobreposicao;

INSERT INTO licenciamento.tipo_sobreposicao(id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) values
(1, 'AREA_INFLUENCIA_UC', 'AI de Unidade de Conservação', 'tiponimia', NULL, NULL),
(2, 'AREA_USO_RESTRITO_PANTANAL', 'Área de uso restrito do pantanal', 'area', NULL, NULL),
(3, 'AREAS_PRIORITARIAS', 'Áreas prioritárias', 'nome', NULL, NULL),
(4, 'BACIA_FORMOSO_E_PRATA', 'Bacia Formoso e Prata', 'nome', NULL, NULL),
(5, 'BACIA_PARAGUAIA', 'Bacia Paraguaia', 'nome', NULL, NULL),
(6, 'BACIA_PARANA', 'Bacia Paraná', 'nome', NULL, NULL),
(7, 'BIOMAMATAATLANTICA', 'Bioma Mata Atlântica', 'cd_legenda', NULL, NULL),
(8, 'BIOMAS', 'Biomas', 'cd_legenda', NULL, NULL),
(9, 'CORREDORES_ECOLOGICOS', 'Corredores ecológicos', 'corredor', NULL, NULL),
(10, 'ENTORNO_DO_TAQUARI', 'Entorno do Taquari', 'id', NULL, NULL),
(11, 'GEOLOGIA_2006', 'Geologia 2006', 'nome_unida', NULL, NULL),
(12, 'GRADE_CBERS_CCD', 'Grade CBERS CCD', 'orbita_pon', NULL, NULL),
(13, 'GRADE_DAS_CARTAS_1_PARA_100_MIL', 'Grade Carta 1 para 100mil', 'nome', NULL, NULL),
(14, 'GRADE_DAS_CARTAS_1_PARA_250_MIL', 'Grade Carta 1 para 250mil', 'nome', NULL, NULL),
(15, 'MACROZONEAMENTO', 'Macrozoneamento', 'dominio', NULL, NULL),
(16, 'MACROZONEAMENTO_SOLOS', 'Macrozoneamento Solos', 'classe', NULL, NULL),
(17, 'MACROZONEAMENTO_SOLOS_AMOSTRAS', 'Macrozoneamento Solos Amostras', 'pt_coleta', NULL, NULL),
(18, 'RIOS_DO_DOMINIO_DO_ESTADO_DE_MS_ANA', 'Rios do MS', 'noriocomp', 'dtversao', NULL),
(19, 'SITIOS_ARQUEOLOGICOS', 'Sítios Arqueológicos', 'nome', NULL, NULL),
(20, 'TERRAS_INDIGENAS_EM_ESTUDO', 'Terras indígenas em estudo', 'nome_ti', 'datadoc', NULL),
(21, 'TERRAS_INDIGENAS_FUNAI_2015', 'Terras indíginas FUNAI', 'terrai_nom', NULL, NULL),
(22, 'UCS_MS_MOSAICO', 'Unidades de Conservação do MS', 'nome_uc', NULL, NULL),
(23, 'UNIDADE_DE_PLANEJAMENTO_E_GERENCIAMENTO', 'Unidade de Planejamento e Gerenciamento', 'sourcethm', NULL, NULL),
(24, 'UNIDADES_HIDROGELOLOGICAS', 'Unidades Hidrogeológicas', 'nome', NULL, NULL),
(25, 'ZEE_MS', 'ZEE', 'zonas', NULL, NULL),
(26, 'ZONA_AMORT_UC_MS', 'Zona de Amortecimento UC MS', 'nome_uc', NULL, NULL),
(27, 'ZONA_AMORT_UC_MS_CONAMA_2KM', 'Zona de Amortecimento UC MS - CONAMA 2KM', 'nome', NULL, NULL),
(28, 'ZONA_AMORT_UC_MS_CONAMA_3KM', 'Zona de Amortecimento UC MS - CONAMA 3KM', 'nome', NULL, NULL);


# ---!Downs

DELETE FROM licenciamento.tipo_sobreposicao;

INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(1, 'TERRA_INDIGENA', 'Terra Indígena', 'terrai_nom', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(2, 'UC_FEDERAL', 'Unidade de conservação federal', 'nome', 'anocriacao', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(3, 'UC_ESTADUAL', 'Unidade de conservação estadual', 'nome', 'criaÃ§Ã£o_', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(4, 'UC_MUNICIPAL', 'UC Municipal', 'uc', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(5, 'TERRA_INDIGENA_ZA', 'Entorno de TI', 'terrai_nom', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(6, 'TERRA_INDIGENA_ESTUDO', 'Terra Indígena Estudo', 'terrai_nom', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(7, 'UC_FEDERAL_APA_DENTRO', 'UC Federal APA', 'nome', 'anocriacao', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(8, 'UC_FEDERAL_APA_FORA', 'UC Federal PI e Uso Sustentável', 'nome', 'anocriacao', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(9, 'UC_FEDERAL_ZA', 'Unidade de conservação federal ZA', 'nome', 'anocriacao', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(10, 'UC_ESTADUAL_PI_DENTRO', 'UC Estadual Proteção Integral', 'nome', 'criaÃ§Ã£o_', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(11, 'UC_ESTADUAL_PI_FORA', 'UC Estadual Uso Sustentável e APA', 'nome', 'criaÃ§Ã£o_', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(12, 'UC_ESTADUAL_ZA', 'UC Estadual ZA', 'nome', 'criaÃ§Ã£o_', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(13, 'TOMBAMENTO_ENCONTRO_AGUAS', 'Tombamento Encontro Águas', 'interessad', 'created_da', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(14, 'TOMBAMENTO_ENCONTRO_AGUAS_ZA', 'Tombamento Encontro Águas ZA', 'interessad', 'created_da', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(15, 'AREAS_EMBARGADAS_IBAMA', 'Áreas Embargadas IBAMA', 'nom_pessoa', 'data_cadas', 'cpf_cnpj_s');
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(16, 'AUTO_DE_INFRACAO_IBAMA', 'Auto de Infração IBAMA', 'null', 'dat_hora_a', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(17, 'SAUIM_DE_COLEIRA', 'Sauim de Coleira', 'nome', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(18, 'SITIOS_ARQUEOLOGICOS', 'Sítios Arqueológicos', 'name', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(19, 'UC_ESTADUAL_ZA_PI_FORA', 'Unidade de conservação estadual ZA fora PI', 'nome', 'criaÃ§Ã£o_', NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(20, 'BENS_ACAUTELADOS_IPHAN_PT', 'Bens Acautelados IPHAN Ponto', 'name', NULL, NULL);
INSERT INTO licenciamento.tipo_sobreposicao (id, codigo, nome, coluna_nome, coluna_data, coluna_cpf_cnpj) VALUES(21, 'BENS_ACAUTELADOS_IPHAN_POL', 'Bens Acautelados IPHAN Polígono', 'name', NULL, NULL);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'TERRA_INDIGENA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'FUNAI')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'TERRA_INDIGENA_ZA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'FUNAI')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'TERRA_INDIGENA_ESTUDO' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'FUNAI')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_ESTADUAL' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_MUNICIPAL' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_ESTADUAL_PI_DENTRO' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_ESTADUAL_PI_FORA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_ESTADUAL_ZA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_ESTADUAL_ZA_PI_FORA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'DEMUC')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_FEDERAL' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'ICMBIO')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_FEDERAL_APA_DENTRO' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'ICMBIO')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_FEDERAL_APA_FORA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'ICMBIO')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'UC_FEDERAL_ZA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'ICMBIO')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'TOMBAMENTO_ENCONTRO_AGUAS' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'TOMBAMENTO_ENCONTRO_AGUAS_ZA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'SAUIM_DE_COLEIRA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'SITIOS_ARQUEOLOGICOS' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'BENS_ACAUTELADOS_IPHAN_PT' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'BENS_ACAUTELADOS_IPHAN_POL' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IPHAN')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'AREAS_EMBARGADAS_IBAMA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IBAMA')
);

INSERT INTO licenciamento.rel_tipo_sobreposicao_orgao VALUES (
	( SELECT ts.id FROM licenciamento.tipo_sobreposicao ts WHERE ts.codigo = 'AUTO_DE_INFRACAO_IBAMA' ),
	( SELECT o.id FROM licenciamento.orgao o WHERE o.sigla = 'IBAMA')
);
