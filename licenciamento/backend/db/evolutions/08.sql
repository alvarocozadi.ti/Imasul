# --- !Ups

ALTER TABLE licenciamento.questionario_3 ADD ef_ind_destino_final_vala_infiltracao bool NULL;
COMMENT ON COLUMN licenciamento.questionario_3.ef_ind_destino_final_vala_infiltracao IS 'Flag que indica destino final - vala de infiltração (efluentes)';

ALTER TABLE licenciamento.questionario_3 ADD ef_ind_destino_final_lancamento_corpo_hidrico bool NULL;
COMMENT ON COLUMN licenciamento.questionario_3.ef_ind_destino_final_lancamento_corpo_hidrico IS 'Flag que indica destino final - lançamento de corpo hidrico (efluentes)';

ALTER TABLE licenciamento.questionario_3 ADD ef_ind_tipo_tratamento_vala_infiltracao bool NULL;
COMMENT ON COLUMN licenciamento.questionario_3.ef_ind_tipo_tratamento_vala_infiltracao IS 'Flag que indica tipo de tratamento - vala de infiltraçã (efluentes)';

ALTER TABLE licenciamento.questionario_3 ADD ef_dom_destino_final_ete bool NULL;
COMMENT ON COLUMN licenciamento.questionario_3.ef_dom_destino_final_ete IS 'Flag que indica tipo de destino final - ete (efluentes)';


# --- !Downs

ALTER TABLE licenciamento.questionario_3 DROP COLUMN ef_ind_destino_final_vala_infiltracao;
ALTER TABLE licenciamento.questionario_3 DROP COLUMN ef_ind_destino_final_lancamento_corpo_hidrico;
ALTER TABLE licenciamento.questionario_3 DROP COLUMN ef_ind_tipo_tratamento_vala_infiltracao;
ALTER TABLE licenciamento.questionario_3 DROP COLUMN ef_dom_destino_final_ete;