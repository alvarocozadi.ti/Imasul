# --- !Ups

CREATE SEQUENCE licenciamento.numero_licenca_seq ;

ALTER SEQUENCE licenciamento.numero_licenca_seq OWNER TO postgres;

CREATE OR REPLACE FUNCTION licenciamento.f_gerar_numero_licenca()
 RETURNS VARCHAR
 LANGUAGE plpgsql
AS $function$
DECLARE
    v_ultimo_ano INTEGER;

BEGIN
    SELECT extract(year from (data_cadastro))::INTEGER INTO v_ultimo_ano
    FROM licenciamento.licenca
    ORDER BY id DESC
    LIMIT 1;

    IF v_ultimo_ano <> (SELECT extract(year from (current_date))::INTEGER) THEN

        PERFORM setval('licenciamento.numero_licenca_seq', 1, false);

    END IF;

    RETURN CONCAT( lpad(cast(nextval('licenciamento.numero_licenca_seq'::REGCLASS) AS VARCHAR), 6, '0'),'/', extract(YEAR FROM (current_date)));

END; $function$
;

ALTER FUNCTION licenciamento.f_gerar_numero_licenca() OWNER TO postgres;


# --- !Downs

DROP FUNCTION licenciamento.f_gerar_numero_licenca();
DROP SEQUENCE licenciamento.numero_licenca_seq;


