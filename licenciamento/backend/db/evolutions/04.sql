# --- !Ups

ALTER TABLE licenciamento.caracterizacao ADD COLUMN processo_outorga varchar(25);
COMMENT ON COLUMN licenciamento.caracterizacao.processo_outorga IS 'Campo para armazenar o número do processo do sistema de outorga do SIRIEMA';

# --- !Downs

ALTER TABLE licenciamento.caracterizacao DROP COLUMN processo_outorga;