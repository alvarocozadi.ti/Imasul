# --!Ups

UPDATE licenciamento.atividade SET v1 = false WHERE codigo in ('2301');

COMMENT ON COLUMN licenciamento.taxa_licenciamento.tipo_estudo IS 'Código do tipo de estudo da atividade que impacta no valor da licença';
COMMENT ON COLUMN licenciamento.taxa_licenciamento.id_variavel_calculo IS 'Identificador que faz o relacionamento com a tabela licenciamento.variavel_calculo_taxa';

COMMENT ON COLUMN licenciamento.licenca.numero IS 'Identificador da licença.';

COMMENT ON COLUMN licenciamento.potencial_poluidor.valor IS 'Valor identificador do potencial poluidor';

COMMENT ON COLUMN licenciamento.tipologia.ativo IS 'Campo que indica se a tipologia está ativa';
COMMENT ON COLUMN licenciamento.tipologia.codigo IS 'Código único identificador da tipologia';
COMMENT ON COLUMN licenciamento.tipologia.id_diretoria IS 'Identificador que indica a diretoria responsável, faz o relacionamento com a tabela licenciamento.diretoria';

COMMENT ON TABLE licenciamento.diretoria IS 'Entidade responsável por armazenar as diretorias.';
COMMENT ON COLUMN licenciamento.diretoria.id IS 'Identificador único da entidade.';
COMMENT ON COLUMN licenciamento.diretoria.nome IS 'Nome da diretoria.';
COMMENT ON COLUMN licenciamento.diretoria.sigla IS 'Sigla da diretoria.';

ALTER TABLE licenciamento.tipologia DROP CONSTRAINT fk_t_diretoria;
ALTER TABLE licenciamento.tipologia
	ADD CONSTRAINT fk_t_diretoria FOREIGN KEY (id_diretoria)
	REFERENCES licenciamento.diretoria (id);


# ---!Downs

ALTER TABLE licenciamento.tipologia DROP CONSTRAINT fk_t_diretoria;
ALTER TABLE licenciamento.tipologia
	ADD CONSTRAINT fk_t_diretoria FOREIGN KEY (id_diretoria)
	REFERENCES licenciamento.tipologia (id);

UPDATE licenciamento.atividade SET v1 = true WHERE codigo in ('2301');
