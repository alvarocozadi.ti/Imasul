# --- !Ups

create sequence licenciamento.numero_processo_seq;

alter sequence licenciamento.numero_processo_seq owner to postgres;

grant select, update, usage on sequence licenciamento.numero_processo_seq to licenciamento_ms;

create table licenciamento.estado
(
    cod_estado varchar(2) not null
        constraint pk_estado
            primary key,
    nome varchar(60) not null,
    the_geom geometry
        constraint enforce_geotype_geo_localizacao
            check ((geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))
        constraint enforce_dims_geo_localizacao
            check (st_ndims(the_geom) = 2)
        constraint enforce_srid_geo_localizacao
            check (st_srid(the_geom) = 4674),
    cod_estado_ibge integer not null
);
comment on table licenciamento.estado is 'Entidade responsável por armazenar os estados.';
comment on column licenciamento.estado.cod_estado is 'Identificador único da entidade.';
comment on column licenciamento.estado.nome is 'Nome do estado.';
comment on column licenciamento.estado.the_geom is 'Coluna georreferenciada.';
comment on column licenciamento.estado.cod_estado_ibge is 'Código IBGE para o estado.';

alter table licenciamento.estado owner to postgres;

create table licenciamento.municipio
(
    id_municipio integer not null
        constraint pk_municipio
            primary key,
    nome varchar(60) not null,
    cod_estado varchar(2) not null
        constraint fk_municipio_estado
            references licenciamento.estado,
    the_geom geometry
        constraint enforce_geotype_geo_localizacao
            check ((geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))
        constraint enforce_dims_geo_localizacao
            check (st_ndims(the_geom) = 2)
        constraint enforce_srid_geo_localizacao
            check (st_srid(the_geom) = 4674),
    apto_licenciamento boolean default false not null,
    cod_tse integer
);
comment on table licenciamento.municipio is 'Entidade responsavel por armazenar as informações do municipio.';
comment on column licenciamento.municipio.id_municipio is 'Identificado único da entidade.';
comment on column licenciamento.municipio.nome is 'Nome do municipio.';
comment on column licenciamento.municipio.cod_estado is 'Identificador da entidade cod_estado que realizará o relacionamento entre as entidades estado e municipio.';
comment on column licenciamento.municipio.the_geom is 'Coluna georreferenciada.';
comment on column licenciamento.municipio.apto_licenciamento is 'Informa se o município pode realizar licenciamento.';
comment on column licenciamento.municipio.cod_tse is 'Código do municipio segundo TSE. utilizado na emissão de DAEs.';

alter table licenciamento.municipio owner to postgres;

create table licenciamento.tipo_documento
(
    id serial not null
        constraint pk_tipo_documento
            primary key,
    nome varchar(500) not null,
    caminho_modelo text,
    caminho_pasta text not null,
    prefixo_nome_arquivo text not null,
    codigo varchar(50),
    tipo_analise integer
);
comment on table licenciamento.tipo_documento is 'Entidade responsavel por armazenar os possíveis tipos de documentos exigidos pelo licenciamento.';
comment on column licenciamento.tipo_documento.id is 'Identificado único da entidade.';
comment on column licenciamento.tipo_documento.nome is 'Nome do tipo de documento.';
comment on column licenciamento.tipo_documento.caminho_modelo is 'URL do modelo do documento para o usuário preencher.';
comment on column licenciamento.tipo_documento.caminho_pasta is 'Caminho da pasta onde os documentos deste tipo serão armazenados, relativo a pasta raiz de armazenamento dos documentos.';
comment on column licenciamento.tipo_documento.prefixo_nome_arquivo is 'Prefixo adicionado no nome do arquivo ao ser armazenado no disco para documentos deste tipo.';
comment on column licenciamento.tipo_documento.codigo is 'Código para identificar o tipo_documento.';
comment on column licenciamento.tipo_documento.tipo_analise is 'Indica se o documento será avaliado na análise técnica ou análise jurídica (0 - Jurídica; 1 - Técnica).';

alter table licenciamento.tipo_documento owner to postgres;

grant select, usage on sequence licenciamento.tipo_documento_id_seq to licenciamento_ms;

create table licenciamento.documento
(
    id serial not null
        constraint pk_documento
            primary key,
    id_tipo_documento integer not null
        constraint fk_d_tipo_documento
            references licenciamento.tipo_documento,
    caminho text not null,
    data_cadastro date default now() not null
);
comment on table licenciamento.documento is 'Entidade responsavel por armazenar aos documentos.';
comment on column licenciamento.documento.id is 'Identificado único da entidade.';
comment on column licenciamento.documento.id_tipo_documento is 'Identificador único da entidade tipo_documento que realizará o relacionamento entre documento e tipo_documento';
comment on column licenciamento.documento.caminho is 'Path do arquivo.';
comment on column licenciamento.documento.data_cadastro is 'Data em que o documento foi cadastrado na tabela.';

alter table licenciamento.documento owner to postgres;


create table licenciamento.estado_civil
(
    id integer not null
        constraint pk_estado_civil
            primary key,
    nome varchar(200) not null,
    codigo integer
);
comment on table licenciamento.estado_civil is 'Entidade responsavel por armazenar os possíveis tipos de estado civil.';
comment on column licenciamento.estado_civil.id is 'Identificado único da entidade.';
comment on column licenciamento.estado_civil.nome is 'Nome do estado civil, sendo: Amaziado(a), Casado(a), Divorciado(a), Separado(a) judicialmente, Solteiro(a) e Viúvo(a)';
comment on column licenciamento.estado_civil.codigo is 'Identificador do estado civil';

alter table licenciamento.estado_civil owner to postgres;


create table licenciamento.orgao_classe
(
    id integer not null
        constraint pk_orgao_classe
            primary key,
    nome varchar(200) not null
);
comment on table licenciamento.orgao_classe is 'Entidade responsavel por armazenar os possíveis valores do órgão de classe.';
comment on column licenciamento.orgao_classe.id is 'Identificado único da entidade.';
comment on column licenciamento.orgao_classe.nome is 'Nome do órgão de classe.';

alter table licenciamento.orgao_classe owner to postgres;


create table licenciamento.empreendimento
(
    id serial not null
        constraint pk_empreendimento
            primary key,
    tipo_localizacao integer not null,
    data_cadastro timestamp not null,
    ativo boolean not null,
    id_municipio integer not null
        constraint fk_e_id_municipio
            references licenciamento.municipio,
    tipo_esfera integer not null,
    possui_shape boolean,
    cpf_cnpj text not null,
    cpf_cnpj_cadastrante text not null,
    denominacao varchar(200),
    is_principal boolean,
    id_empreendimento_pai integer,
    id_empreendimento_eu integer
);
comment on table licenciamento.empreendimento is 'Entidade responsavel por armazenar as informações referentes ao empreendimento.';
comment on column licenciamento.empreendimento.id is 'Identificado único da entidade.';
comment on column licenciamento.empreendimento.tipo_localizacao is 'Indica se a localização é zona rual ou zona urbana (0 - zona urbana e 1 - zona rural)';
comment on column licenciamento.empreendimento.data_cadastro is 'Data de cadastro do empreendimento.';
comment on column licenciamento.empreendimento.id_municipio is 'Identificador da tabela município.';
comment on column licenciamento.empreendimento.tipo_esfera is 'Tipo de Esfera, podendo ser 0 - Municipal, 1 - Estadual e 2 - Federal.';
comment on column licenciamento.empreendimento.possui_shape is 'Boooleano que indica se o empreendimento posui ou não upload de shapes, se é nulo o empreendimento nunca cadastrou shapes';
comment on column licenciamento.empreendimento.cpf_cnpj is 'CPF ou CNPJ da pessoa que representa o empreendimento';
comment on column licenciamento.empreendimento.cpf_cnpj_cadastrante is 'CPF ou CNPJ da pessoa cadastrante da caracterização referenciado no Entrada Única';
comment on column licenciamento.empreendimento.denominacao is 'Nome do empreendimento.';
comment on column licenciamento.empreendimento.is_principal is 'Flag responsável por verificar se o empreendimento é o principal, ou seja, se foi o primeiro com esse cpfCnpj a ser cadastrado.';
comment on column licenciamento.empreendimento.id_empreendimento_pai is 'Coluna responsável por armazenar o id do empreendimento cadastrado pela primeira vez, com esse mesmo cpfCnpj.';
comment on column licenciamento.empreendimento.id_empreendimento_eu is 'Coluna responsável por armazenar o id desse empreendimento no Entrada Única';

alter table licenciamento.empreendimento owner to postgres;


create table licenciamento.imovel_empreendimento
(
    id serial not null
        constraint pk_imovel_empreendimento
            primary key,
    codigo_imovel varchar(100) not null,
    id_imovel_car integer not null,
    nome varchar(200) not null,
    id_municipio integer not null,
    id_empreendimento integer not null
        constraint fk_ie_empreendimento
            references licenciamento.empreendimento,
    the_geom geometry not null,
    data_cadastro date,
    area_total double precision,
    area_reserva_legal double precision,
    area_app double precision,
    area_uso_alternativo_solo double precision
);
comment on table licenciamento.imovel_empreendimento is 'Entidade responsavel por armazenar as informações referentes aos imóveis cadastrados no Sicar no caso do empreendimento estar localizado na zona rural. Neste caso é necessário apresentar informações do CAR referente a esse empreendimento.';
comment on column licenciamento.imovel_empreendimento.id is 'Identificado único da entidade.';
comment on column licenciamento.imovel_empreendimento.codigo_imovel is 'Código do imóvel no Sicar.';
comment on column licenciamento.imovel_empreendimento.id_imovel_car is 'Identificador unico do imóvel no Sicar.';
comment on column licenciamento.imovel_empreendimento.nome is 'Nome do imóvel no Sicar.';
comment on column licenciamento.imovel_empreendimento.id_municipio is 'Identificador unico do município do imóvel no Sicar.';
comment on column licenciamento.imovel_empreendimento.id_empreendimento is 'Identificador único da entidade empreendimento que realizará o relacionamento empreendimento e imovel_empreendimento.';
comment on column licenciamento.imovel_empreendimento.the_geom is 'Campo georeferenciado da área vetorizada do imóvel no Sicar.';
comment on column licenciamento.imovel_empreendimento.data_cadastro is 'Data em que o imovel foi cadastrado no SiCAR.';
comment on column licenciamento.imovel_empreendimento.area_total is 'Área total do imóvel em Ha.';
comment on column licenciamento.imovel_empreendimento.area_reserva_legal is 'Área total da reserva legal do imóvel em Ha.';
comment on column licenciamento.imovel_empreendimento.area_app is ' Área total da preservação permanente do imóvel em Ha.';
comment on column licenciamento.imovel_empreendimento.area_uso_alternativo_solo is 'Área de Uso Alternativo do solo, composta pela Área Liquida do Imóvel - (Área de Preservação Permanente + Hidrografia + Área de Reserva Legal + Área de Uso Restrito).';

alter table licenciamento.imovel_empreendimento owner to postgres;


create table licenciamento.tipo_licenca
(
    id integer not null
        constraint pk_tipo_licenca
            primary key,
    nome text not null,
    validade_em_anos integer,
    sigla varchar(15) not null,
    finalidade varchar(50),
    ordem integer default 0 not null,
    cod_arrecadacao varchar(3)
);
comment on table licenciamento.tipo_licenca is 'Entidade responsável por armazenar as configurações dos tipos da licenca(DLA,simplificado,declaratório e ordinário).';
comment on column licenciamento.tipo_licenca.id is 'Identificador único da entidade.';
comment on column licenciamento.tipo_licenca.nome is 'Nome dos tipos de licença.';
comment on column licenciamento.tipo_licenca.validade_em_anos is 'Indica o prazo de validade da Licença, em anos.';
comment on column licenciamento.tipo_licenca.sigla is 'Sigla dos tipos de licença.';
comment on column licenciamento.tipo_licenca.finalidade is 'Coluna que contém a descrição da finalidade do tipo da licença';
comment on column licenciamento.tipo_licenca.ordem is 'Ordem das licenças conforme fases.';
comment on column licenciamento.tipo_licenca.cod_arrecadacao is 'Código de arrecadação do Siriema para auxiliar na geração de boleto na API de pagamentos';

alter table licenciamento.tipo_licenca owner to postgres;


create table licenciamento.status_caracterizacao
(
    id integer not null
        constraint pk_status_caracterizacao
            primary key,
    nome text not null,
    codigo varchar(200)
);
comment on table licenciamento.status_caracterizacao is 'Entidade responsável por armazenar os status da caracterização.';
comment on column licenciamento.status_caracterizacao.id is 'Identificador único da entidade.';
comment on column licenciamento.status_caracterizacao.nome is 'Nome dos status.';
comment on column licenciamento.status_caracterizacao.codigo is 'Código do status da caracterização.';

alter table licenciamento.status_caracterizacao owner to postgres;


create table licenciamento.tipologia
(
    id integer not null
        constraint pk_tipologia
            primary key,
    nome text not null,
    id_diretoria integer
        constraint fk_t_diretoria
            references licenciamento.tipologia,
    ativo boolean default true not null,
    codigo text
);
comment on table licenciamento.tipologia is 'Entidade responsável por armazenar a tipologia das atividades.';
comment on column licenciamento.tipologia.id is 'Identificador único da entidade.';
comment on column licenciamento.tipologia.nome is 'Nome da tipologia.';

alter table licenciamento.tipologia owner to postgres;


create table licenciamento.parametro_atividade
(
    id serial not null
        constraint pk_parametro_atividade
            primary key,
    nome text not null,
    codigo text not null,
    casas_decimais integer
);
comment on table licenciamento.parametro_atividade is 'Entidade responsável por mapear todos os parâmetros existentes.';
comment on column licenciamento.parametro_atividade.id is 'Identificador único da entidade.';
comment on column licenciamento.parametro_atividade.nome is 'Nome do parâmetro.';
comment on column licenciamento.parametro_atividade.codigo is 'Código exclusivo de cada parâmetro.';
comment on column licenciamento.parametro_atividade.casas_decimais is 'Quantidade de casas decimais que o usuário utilizará ao informar o valor do parâmetro.';

alter table licenciamento.parametro_atividade owner to postgres;


create table licenciamento.potencial_poluidor
(
    id integer not null
        constraint pk_potencial_poluidor
            primary key,
    nome text not null,
    valor integer
);
comment on table licenciamento.potencial_poluidor is 'Entidade responsável por guardar registros de potêncial poluidor.';
comment on column licenciamento.potencial_poluidor.id is 'Identificador único da entidade.';
comment on column licenciamento.potencial_poluidor.nome is 'Nome do potêncial poluidor (PEQUENO, MÉDIO, GRANDE).';

alter table licenciamento.potencial_poluidor owner to postgres;


create table licenciamento.porte_empreendimento
(
    id integer not null
        constraint pk_porte_empreendimento
            primary key,
    nome text not null,
    codigo text not null
);
comment on table licenciamento.porte_empreendimento is 'Entidade responsável por guardar os tipos de porte de empreendimento.';
comment on column licenciamento.porte_empreendimento.id is 'Identificador único da entidade.';
comment on column licenciamento.porte_empreendimento.nome is 'Nome do porte do empreendimento (MICRO, PEQUENO, MÉDIO, GRANDE, EXCEPCIONAL, MACRO).';
comment on column licenciamento.porte_empreendimento.codigo is 'Código de identificação definido para cada porte.';

alter table licenciamento.porte_empreendimento owner to postgres;

create table licenciamento.tipo_caracterizacao
(
    id integer not null
        constraint pk_tipo_caracterizacao
            primary key,
    nome varchar(100) not null
);
comment on table licenciamento.tipo_caracterizacao is 'O tipo de caracterização representa qual o fluxo pelo qual o empreendimento passará para obter a licença.';
comment on column licenciamento.tipo_caracterizacao.id is 'Identificador único da entidade.';
comment on column licenciamento.tipo_caracterizacao.nome is 'Nome do tipo de caracterização.';

alter table licenciamento.tipo_caracterizacao owner to postgres;


create table licenciamento.taxa_administrativa_dae
(
    id serial not null
        constraint pk_taxa_administrativa_dae
            primary key,
    ano integer not null,
    valor numeric(13,2) not null,
    link_taxas_licenciamento text not null
);
comment on table licenciamento.taxa_administrativa_dae is 'Entidade responsavel por armazenar a taxa administrativa cobrada pela Secretaria da Fazenda para a emissão de DAE.';
comment on column licenciamento.taxa_administrativa_dae.id is 'Identificado único da entidade.';
comment on column licenciamento.taxa_administrativa_dae.ano is 'Ano de vigência da taxa administrativa.';
comment on column licenciamento.taxa_administrativa_dae.valor is 'Valor da taxa administrativa a ser cobrado.';
comment on column licenciamento.taxa_administrativa_dae.link_taxas_licenciamento is 'link para a tabela anual com valores das taxas de licenciamento da SEMAS-PA.';

alter table licenciamento.taxa_administrativa_dae owner to postgres;


create table licenciamento.diretoria
(
    id integer not null
        constraint pk_diretoria
            primary key,
    nome varchar(200) not null,
    sigla varchar(20) not null
);
comment on table licenciamento.diretoria is 'Entidade responsável por armazenar as diretorias da SEMAS.';
comment on column licenciamento.diretoria.id is 'Identificador único da entidade.';
comment on column licenciamento.diretoria.nome is 'Nome da diretoria da SEMAS.';
comment on column licenciamento.diretoria.sigla is 'Sigla da diretoria da SEMAS.';

alter table licenciamento.diretoria owner to postgres;


create table licenciamento.reenvio_email
(
    id serial not null
        constraint pk_reenvio_email
            primary key,
    id_itens_email integer not null,
    log text,
    emails_destinatario text not null
);
comment on table licenciamento.reenvio_email is 'Entidade responsável por armazenar o controle do reenvio de email. Guarda os emails que não foram enviados para serem enviados na próxima interação do job de reenvio de email.';
comment on column licenciamento.reenvio_email.id is 'Identificador primário da entidade.';
comment on column licenciamento.reenvio_email.id_itens_email is 'Identificador generico das entididades que podem ser utilizadas nos emails (não é uma FK).';
comment on column licenciamento.reenvio_email.log is 'Motivo da tentativa de envio de email.';
comment on column licenciamento.reenvio_email.emails_destinatario is 'Lista de destinatários (separado por ;) que receberão os emails enviados.';

alter table licenciamento.reenvio_email owner to postgres;


create table licenciamento.tipo_atividade
(
    id integer not null
        constraint pk_tipo_atividade
            primary key,
    nome varchar(200),
    codigo varchar(200)
);
comment on table licenciamento.tipo_atividade is 'Entidade responsável por armazenar os tipos de atividades.';
comment on column licenciamento.tipo_atividade.id is 'Identificador único da entidade.';
comment on column licenciamento.tipo_atividade.nome is 'Nome do tipo de atividade.';
comment on column licenciamento.tipo_atividade.codigo is 'Código do tipo de atividade.';

alter table licenciamento.tipo_atividade owner to postgres;


grant insert, select, update, delete on licenciamento.tipo_atividade to licenciamento_ms;

create table licenciamento.usuario_licenciamento
(
    id serial not null
        constraint pk_usuario_licenciamento
            primary key,
    login varchar(50)
);
comment on table licenciamento.usuario_licenciamento is 'Entidade responsável por armazenar os usuários que possui permissão para acessar o módulo de análise do licenciamento';
comment on column licenciamento.usuario_licenciamento.id is 'Identificador único da entidade usuario.';
comment on column licenciamento.usuario_licenciamento.login is 'Login do usuário que pode ser cpf ou cnpj';

alter table licenciamento.usuario_licenciamento owner to postgres;

grant select, usage on sequence licenciamento.usuario_licenciamento_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.usuario_licenciamento to licenciamento_ms;

create table licenciamento.tipo_sobreposicao
(
    id integer not null
        constraint pk_tipo_sobreposicao
            primary key,
    codigo varchar(50) not null,
    nome varchar(50) not null,
    coluna_nome varchar(50),
    coluna_data varchar(50),
    coluna_cpf_cnpj varchar(50)
);
comment on table licenciamento.tipo_sobreposicao is 'Tabela para armazenamento dos tipos de sobreposição do licenciamento';
comment on column licenciamento.tipo_sobreposicao.id is 'Indentificador único da entidade';
comment on column licenciamento.tipo_sobreposicao.codigo is 'Código de identificação da entidade';
comment on column licenciamento.tipo_sobreposicao.nome is 'Nome que descreve o tipo de sobreposição';
comment on column licenciamento.tipo_sobreposicao.coluna_nome is 'Coluna que contém o nome da área em sua respectiva tabela';
comment on column licenciamento.tipo_sobreposicao.coluna_data is 'Coluna que contém a data de criação da área em sua respectiva tabela';
comment on column licenciamento.tipo_sobreposicao.coluna_cpf_cnpj is 'Coluna que contém o CPF/CNPJ do responsável pela área em sua respectiva tabela';

alter table licenciamento.tipo_sobreposicao owner to postgres;

grant insert, select, update, delete on licenciamento.tipo_sobreposicao to licenciamento_ms;


create table licenciamento.porte_atividade
(
    id serial not null
        constraint pk_porte_atividade
            primary key,
    id_porte_empreendimento integer not null
        constraint fk_pa_porte_empreendimento
            references licenciamento.porte_empreendimento,
    limite_inferior_um numeric(13,2),
    limite_superior_um numeric(13,2),
    limite_inferior_dois numeric(13,2),
    limite_superior_dois numeric(13,2),
    id_parametro_um integer
        constraint fk_pa_parametro_atividade_um
            references licenciamento.parametro_atividade,
    id_parametro_dois integer
        constraint fk_pa_parametro_atividade_dois
            references licenciamento.parametro_atividade,
    limite_inferior_um_incluso boolean,
    limite_inferior_dois_incluso boolean,
    limite_superior_um_incluso boolean,
    limite_superior_dois_incluso boolean,
    codigo integer not null
);
comment on table licenciamento.porte_atividade is 'Entidade responsável pelos portes referentes a cada atividade.';
comment on column licenciamento.porte_atividade.id is 'Identificador único da entidade.';
comment on column licenciamento.porte_atividade.id_porte_empreendimento is 'Identificador da entidade porte_empreendimento que faz o relacionamento entre as entidades porte_atividade e porte_empreendimento.';
comment on column licenciamento.porte_atividade.limite_inferior_um is 'Primeoiro limite inferior para definir qual o porte da atividade.';
comment on column licenciamento.porte_atividade.limite_superior_um is 'Primeiro limite superior para definir qual o porte da atividade.';
comment on column licenciamento.porte_atividade.limite_inferior_dois is 'Primeoiro limite inferior para definir qual o porte da atividade.';
comment on column licenciamento.porte_atividade.limite_superior_dois is 'Primeiro limite superior para definir qual o porte da atividade.';
comment on column licenciamento.porte_atividade.id_parametro_um is 'Referência para a entidade parametro_atividade que representa o primeiro parâmetro do porte_atividade.';
comment on column licenciamento.porte_atividade.id_parametro_dois is 'Referência para a entidade parametro_atividade que representa o segundo parâmetro do porte_atividade.';
comment on column licenciamento.porte_atividade.limite_inferior_um_incluso is 'Define se o primeiro limite inferior é <= (TRUE) ou < (FALSE)';
comment on column licenciamento.porte_atividade.limite_inferior_dois_incluso is 'Define se o segundo limite inferior é <= (TRUE) ou < (FALSE)';
comment on column licenciamento.porte_atividade.limite_superior_um_incluso is 'Define se o primeiro limite superior é >= (TRUE) ou > (FALSE)';
comment on column licenciamento.porte_atividade.limite_superior_dois_incluso is 'Define se o segundo limite superior é >= (TRUE) ou > (FALSE)';
comment on column licenciamento.porte_atividade.codigo is 'Código identificador do grupo de cálculo de porte.';

alter table licenciamento.porte_atividade owner to postgres;

grant select, usage on sequence licenciamento.porte_atividade_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.porte_atividade to licenciamento_ms;

create table licenciamento.orgao
(
    id serial not null
        constraint pk_orgao
            primary key,
    nome text not null,
    sigla text not null,
    email text not null
);
comment on table licenciamento.orgao is 'Entidade responsavel por armazenar os possíveis valores do órgão de classe.';
comment on column licenciamento.orgao.id is 'Identificador único da entidade.';
comment on column licenciamento.orgao.nome is 'Nome do órgão.';
comment on column licenciamento.orgao.sigla is 'Sigla do órgão.';
comment on column licenciamento.orgao.email is 'Email do órgão.';

alter table licenciamento.orgao owner to postgres;

grant select, usage on sequence licenciamento.orgao_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.orgao to licenciamento_ms;


create table licenciamento.rel_tipo_sobreposicao_orgao
(
    id_tipo_sobreposicao integer not null
        constraint fk_rtso_tipo_sobreposicao
            references licenciamento.tipo_sobreposicao,
    id_orgao integer not null
        constraint fk_rtso_orgao
            references licenciamento.orgao,
    constraint pk_rel_tipo_sobreposicao_orgao
        primary key (id_tipo_sobreposicao, id_orgao)
);
comment on table licenciamento.rel_tipo_sobreposicao_orgao is 'Entidade responsavel por fazer o relacionamento da tabela de tipo sobreposição(restrição) com a tabela que armazena os órgãos responsáveis por determinado tipo de restrição.';
comment on column licenciamento.rel_tipo_sobreposicao_orgao.id_tipo_sobreposicao is 'Identificador que faz a ligação com a tabela tipo_sobreposicao.';
comment on column licenciamento.rel_tipo_sobreposicao_orgao.id_orgao is 'Identificador que faz a ligação com a tabela orgao.';

alter table licenciamento.rel_tipo_sobreposicao_orgao owner to postgres;

grant insert, select, update, delete on licenciamento.rel_tipo_sobreposicao_orgao to licenciamento_ms;


create table licenciamento.tipo_documento_tipo_licenca
(
    id serial not null
        constraint pk_tipo_documento_tipo_licenca
            primary key,
    id_tipo_documento integer not null
        constraint fk_tdtl_tipo_documento
            references licenciamento.tipo_documento,
    id_tipo_licenca integer not null
        constraint fk_tdtl_tipo_licenca
            references licenciamento.tipo_licenca,
    obrigatorio boolean not null
);
comment on table licenciamento.tipo_documento_tipo_licenca is 'Entidade responsável pelos tipos de documentos necessários para cada atividade.';
comment on column licenciamento.tipo_documento_tipo_licenca.id is 'Identificador único da entidade.';
comment on column licenciamento.tipo_documento_tipo_licenca.id_tipo_documento is 'Identificador que faz o relacionamento com a entidade tipo_documento.';
comment on column licenciamento.tipo_documento_tipo_licenca.id_tipo_licenca is 'Identificador que faz o relacionamento com a entidade tipo_licenca.';
comment on column licenciamento.tipo_documento_tipo_licenca.obrigatorio is 'Define se o tipo de documento é obrigatório para cada atividade.';

alter table licenciamento.tipo_documento_tipo_licenca owner to licenciamento_ms;


create table licenciamento.rel_tipo_licenca_permitida
(
    id_tipo_licenca_pai integer not null,
    id_tipo_licenca_filho integer not null
        constraint fk_rtlp_tipo_licenca
            references licenciamento.tipo_licenca,
    constraint pk_rel_tipo_licenca_permitida
        primary key (id_tipo_licenca_pai, id_tipo_licenca_filho)
);
comment on table licenciamento.rel_tipo_licenca_permitida is 'Entidade responsável por armazenar as ligações entr os tipos de licença';
comment on column licenciamento.rel_tipo_licenca_permitida.id_tipo_licenca_pai is 'Identificador do tipo_licenca atual';
comment on column licenciamento.rel_tipo_licenca_permitida.id_tipo_licenca_filho is 'Identificador da que referência o tipo_licenca que o id_tipo_licenca pai pode referenciar';

alter table licenciamento.rel_tipo_licenca_permitida owner to postgres;


grant insert, select, update, delete on licenciamento.rel_tipo_licenca_permitida to licenciamento_ms;

create table licenciamento.grupo_documento
(
    id serial not null
        constraint pk_grupo_documento
            primary key,
    codigo text not null
        constraint uq_codigo_grupo_documento
            unique,
    descricao text
);
comment on table licenciamento.grupo_documento is 'Entidade responsavel por armazenar um grupo de documentos que serão solicitados pela caracterização';
comment on column licenciamento.grupo_documento.id is 'Identificador único da entidade';
comment on column licenciamento.grupo_documento.codigo is 'Código de identificação da entidade';
comment on column licenciamento.grupo_documento.descricao is 'Descrição do grupo de documentos';

alter table licenciamento.grupo_documento owner to postgres;

grant select, usage on sequence licenciamento.grupo_documento_id_seq to licenciamento_ms;

create table licenciamento.atividade
(
    nome text not null,
    id_tipologia integer not null
        constraint fk_a_tipologia
            references licenciamento.tipologia,
    geo_linha boolean not null,
    geo_ponto boolean not null,
    geo_poligono boolean not null,
    codigo text,
    licenciamento_municipal boolean,
    limite_parametro_municipal numeric(13,2),
    limite_inferior_simplificado numeric(13,2),
    limite_superior_simplificado numeric(13,2),
    id_potencial_poluidor integer
        constraint fk_a_potencial_poluidor
            references licenciamento.potencial_poluidor,
    id serial not null
        constraint pk_atividade
            primary key,
    observacoes text,
    sigla_setor varchar(200),
    ativo boolean default true not null,
    dentro_empreendimento boolean default true not null,
    id_grupo_documento integer
        constraint fk_a_grupo_documento_solicitacao
            references licenciamento.grupo_documento,
    dentro_municipio boolean default false not null,
    v1 boolean,
    tipo_estudo text
);
comment on table licenciamento.atividade is 'Entidade responsável por armazenar as atividades dos regulamentos do estado.';
comment on column licenciamento.atividade.nome is 'Nome da atividade.';
comment on column licenciamento.atividade.id_tipologia is 'Identificador da entidade tipologia que realizará o relacionamento entre as entidades tipologia e atividade.';
comment on column licenciamento.atividade.geo_linha is 'Indica quando a atividade permite o georreferenciamento por polígono.';
comment on column licenciamento.atividade.codigo is 'Código da atividade.';
comment on column licenciamento.atividade.licenciamento_municipal is 'Define se a atividade pode ser licenciada por municípios.';
comment on column licenciamento.atividade.limite_parametro_municipal is 'Limite máximo para que para que uma atividade seja licenciada por um município.';
comment on column licenciamento.atividade.limite_inferior_simplificado is 'Limite inferior para que uma atividade seja licenciada pelo estado.';
comment on column licenciamento.atividade.limite_superior_simplificado is 'Limite superior que define que o licenciamento é simplificado.';
comment on column licenciamento.atividade.id_potencial_poluidor is 'Identificador da entidade potencial_poluidor que faz o relacionamento entre as entidades atividade e potencial_poluidor.';
comment on column licenciamento.atividade.id is 'Identificador único da entidade';
comment on column licenciamento.atividade.observacoes is 'Observação para ser exibida no PDF';
comment on column licenciamento.atividade.dentro_empreendimento is 'Campo que identifica se a atividade deve ser georeferenciada dentro do empreendimento.';
comment on column licenciamento.atividade.id_grupo_documento is 'Referência para o grupo de documentos que a atividade pertence';
comment on column licenciamento.atividade.dentro_municipio is 'Campo que identifica se a atividade deve ser georeferenciada dentro do municipio, implementado apenas para DI';
comment on column licenciamento.atividade.tipo_estudo is 'Código do tipo de estudo da atividade que impacta no valor da licença';

alter table licenciamento.atividade owner to postgres;


create table licenciamento.rel_atividade_tipo_licenca
(
    id_atividade integer not null
        constraint fk_ratl_atividade
            references licenciamento.atividade,
    id_tipo_licenca integer not null
        constraint fk_ratl_tipo_licenca
            references licenciamento.tipo_licenca,
    constraint pk_rel_atividade_tipo_licenca
        primary key (id_atividade, id_tipo_licenca)
);
comment on table licenciamento.rel_atividade_tipo_licenca is 'Entidade responsável pelo armazenamento da relação entre atividade e tipo_licenca';
comment on column licenciamento.rel_atividade_tipo_licenca.id_atividade is 'Identificador único da tabela atividade.';
comment on column licenciamento.rel_atividade_tipo_licenca.id_tipo_licenca is 'Identificador único da tabela tipo_licenca';

alter table licenciamento.rel_atividade_tipo_licenca owner to postgres;


create table licenciamento.rel_municipio_atividade_nao_apta
(
    id_municipio integer not null
        constraint fk_rmana_municipio
            references licenciamento.municipio,
    id_atividade integer not null
        constraint fk_rmana_atividade
            references licenciamento.atividade,
    constraint pk_rel_municipio_atividade_nao_apta
        primary key (id_municipio, id_atividade)
);
comment on table licenciamento.rel_municipio_atividade_nao_apta is 'Entidade que realiza o relacionamento entre as entidades municipio e atividade.';
comment on column licenciamento.rel_municipio_atividade_nao_apta.id_municipio is 'Identificador da entidade município.';
comment on column licenciamento.rel_municipio_atividade_nao_apta.id_atividade is 'Identificador da entidade atividade.';

alter table licenciamento.rel_municipio_atividade_nao_apta owner to postgres;


create table licenciamento.rel_atividade_tipo_atividade
(
    id_atividade integer not null
        constraint fk_rata_atividade
            references licenciamento.atividade,
    id_tipo_atividade integer not null
        constraint fk_rata_tipo_atividade
            references licenciamento.tipo_atividade,
    constraint pk_rel_atividade_tipo_atividade
        primary key (id_atividade, id_tipo_atividade)
);
comment on table licenciamento.rel_atividade_tipo_atividade is 'Entidade responsável por armazenar o o relacionamento entre as atividades e os tipos de atividades.';
comment on column licenciamento.rel_atividade_tipo_atividade.id_atividade is 'Identificador único da entidade atividade.';
comment on column licenciamento.rel_atividade_tipo_atividade.id_tipo_atividade is 'Identificador único da entidade tipo_atividade  ';

alter table licenciamento.rel_atividade_tipo_atividade owner to postgres;

grant insert, select, update, delete on licenciamento.rel_atividade_tipo_atividade to licenciamento_ms;


create table licenciamento.rel_atividade_parametro_atividade
(
    id_atividade integer not null
        constraint fk_rapa_atividade
            references licenciamento.atividade,
    id_parametro_atividade integer not null
        constraint fk_rapa_parametro_atividade
            references licenciamento.parametro_atividade,
    descricao_unidade text default ''::text not null,
    constraint pk_rel_atividade_parametro_atividade
        primary key (id_atividade, id_parametro_atividade)
);
comment on table licenciamento.rel_atividade_parametro_atividade is 'Entidade de representa o relacionamento relacionamento das entidades atividade e parametro_atividade';
comment on column licenciamento.rel_atividade_parametro_atividade.id_atividade is 'Referência para a entidade atividade';
comment on column licenciamento.rel_atividade_parametro_atividade.id_parametro_atividade is 'Referência para a entidade parametro_atividade';
comment on column licenciamento.rel_atividade_parametro_atividade.descricao_unidade is 'Descrição da unidade de medida';

alter table licenciamento.rel_atividade_parametro_atividade owner to postgres;

grant insert, select, update, delete on licenciamento.rel_atividade_parametro_atividade to licenciamento_ms;


create table licenciamento.rel_atividade_porte_atividade
(
    id_atividade integer not null
        constraint fk_rapa_atividade
            references licenciamento.atividade,
    id_porte_atividade integer not null
        constraint fk_rapa_porte_atividade
            references licenciamento.porte_atividade,
    constraint pk_rel_atividade_porte_atividade
        primary key (id_atividade, id_porte_atividade)
);
comment on table licenciamento.rel_atividade_porte_atividade is 'Entidade responsável por armazenar o relacionamento entre as atividades e os cálculos de porte de atividades.';
comment on column licenciamento.rel_atividade_porte_atividade.id_atividade is 'Identificador único da entidade atividade.';
comment on column licenciamento.rel_atividade_porte_atividade.id_porte_atividade is 'Identificador único da entidade porte_atividade.';

alter table licenciamento.rel_atividade_porte_atividade owner to postgres;

grant insert, select, update, delete on licenciamento.rel_atividade_porte_atividade to licenciamento_ms;


grant insert, select, update, delete on licenciamento.grupo_documento to licenciamento_ms;


create table licenciamento.processo
(
    id serial not null
        constraint pk_processo
            primary key,
    numero char(12)
        constraint uq_numero_processo
            unique
);
comment on table licenciamento.processo is 'Entidade de processo responsável por agrupar caracterizações';
comment on column licenciamento.processo.id is 'Identificador primário da entidade';
comment on column licenciamento.processo.numero is 'Numero único gerado para cada processo';

alter table licenciamento.processo owner to postgres;

grant select, usage on sequence licenciamento.processo_id_seq to licenciamento_ms;

create table licenciamento.caracterizacao
(
    id serial not null
        constraint pk_caracterizacao
            primary key,
    numero text,
    data_cadastro timestamp not null,
    data_finalizacao timestamp,
    id_status integer not null
        constraint fk_c_status_caracterizacao
            references licenciamento.status_caracterizacao,
    id_tipo_licenca integer
        constraint fk_c_tipo_licenca
            references licenciamento.tipo_licenca,
    id_empreendimento integer not null
        constraint fk_c_empreendimento
            references licenciamento.empreendimento,
    declaracao_veracidade_informacoes boolean,
    id_tipo integer not null
        constraint fk_c_tipo_caracterizacao
            references licenciamento.tipo_caracterizacao,
    analise boolean default false,
    numero_processo_automatico varchar(50),
    renovacao boolean default false,
    descricao_atividade text,
    origem_sobreposicao varchar(50),
    id_origem integer
        constraint fk_c_origem_caracterizacao
            references licenciamento.caracterizacao,
    bloqueada boolean default false,
    id_processo integer
        constraint fk_c_processo
            references licenciamento.processo,
    ativo boolean default true,
    vigencia_solicitada integer,
    data_retificacao timestamp,
    retificacao boolean default false not null,
    complexo boolean default false not null,
    valor_taxa_licenciamento numeric(13,2) default 0.00 not null,
    valor_taxa_administrativa numeric(13,2) default 0.00 not null,
    ocultar_listagem boolean default false not null,
    cpf_cnpj_cadastrante text not null,
    etapa_rascunho integer,
    cod_guia integer
);
comment on table licenciamento.caracterizacao is 'Entidade reponsável por armazenar a caracterização da atividade no empreendimento.';
comment on column licenciamento.caracterizacao.id is 'Identificador único da entidade.';
comment on column licenciamento.caracterizacao.numero is 'Numero do processo vinculado ao SIMLAM, gerado por integração com o sistema, referencia um título.';
comment on column licenciamento.caracterizacao.data_cadastro is 'Data de início da caracterização.';
comment on column licenciamento.caracterizacao.data_finalizacao is 'Data da finalização da carcterização.';
comment on column licenciamento.caracterizacao.id_status is 'Identificador da entidade status_caracterizacao que realizará o relacionamento entre as entidades caracterizacao e status_caracterizacao.';
comment on column licenciamento.caracterizacao.id_tipo_licenca is 'Identificador da entidade tipo_licenca que realizará o relacionamento entre as entidades caracterizacao e tipo_licenca.';
comment on column licenciamento.caracterizacao.id_empreendimento is 'Identificador da entidade empreendimento que realizará o relacionamento entre as entidades caracterizacao e empreendimento.';
comment on column licenciamento.caracterizacao.declaracao_veracidade_informacoes is 'Flag que indica se o usuário declarou a veracidade das informações e dos documentos enviados.';
comment on column licenciamento.caracterizacao.id_tipo is 'Identificador da tabela tipo_caracterizacao.';
comment on column licenciamento.caracterizacao.analise is 'Indica se a caracterização foi importada para o módulo de análise.';
comment on column licenciamento.caracterizacao.numero_processo_automatico is 'Número do processo gerado automaticamente.';
comment on column licenciamento.caracterizacao.renovacao is 'Flag que indica se uma caracterização está em processo de renovação.';
comment on column licenciamento.caracterizacao.descricao_atividade is 'Campo que descreve a atividade da caracterizacao';
comment on column licenciamento.caracterizacao.origem_sobreposicao is 'Coluna que contém a descrição da área que originou a sobreposição (EMPREENDIMENTO, ATIVIDADE, COMPLEXO, SEM_SOBREPOSICAO)';
comment on column licenciamento.caracterizacao.id_origem is 'Referência para a caracterização de origem, quando a caracterização é um atualização ou uma renovação';
comment on column licenciamento.caracterizacao.bloqueada is 'Flag que controla se uma caracterização está bloqueada,uma caracterização está bloqueada quando ela é renovada, evoluída ou atualizada para ALP';
comment on column licenciamento.caracterizacao.id_processo is 'Referecia para a entidade processo a qual a caracterizacao pertence';
comment on column licenciamento.caracterizacao.ativo is 'Campo que indica se a caracterização está ativa';
comment on column licenciamento.caracterizacao.vigencia_solicitada is 'Tempo de vigência da licença';
comment on column licenciamento.caracterizacao.data_retificacao is 'Campo responsável por guardar data em que o registro foi retificado.';
comment on column licenciamento.caracterizacao.retificacao is 'Coluna da entidade que informa se o registro é uma retificação de um registro anteriaor';
comment on column licenciamento.caracterizacao.complexo is 'Coluna que identifica se o registro da entidade possui um complexo de atividades';
comment on column licenciamento.caracterizacao.valor_taxa_licenciamento is 'Campo que armazena o valor da taxa de licenciamento';
comment on column licenciamento.caracterizacao.valor_taxa_administrativa is 'Coluna que armazena o valor da taxa administrativa';
comment on column licenciamento.caracterizacao.ocultar_listagem is 'Flag que controla se será exibida a licença ';
comment on column licenciamento.caracterizacao.cpf_cnpj_cadastrante is 'CPF ou CNPJ da pessoa cadastrante da caracterização referenciado no Entrada Única';
comment on column licenciamento.caracterizacao.cod_guia is 'Identificador para manter a relação da caracterização e o boleto gerado pela API de pagamentos';

alter table licenciamento.caracterizacao owner to postgres;


create table licenciamento.dispensa_licenciamento
(
    id serial not null
        constraint pk_dispensa_licenciamento
            primary key,
    id_caracterizacao integer not null
        constraint fk_dl_caracterizacao
            references licenciamento.caracterizacao,
    data_cadastro timestamp not null,
    informacao_adicional text,
    id_documento integer
        constraint fk_dl_documento
            references licenciamento.documento,
    numero varchar(30),
    ativo boolean default true not null
);
comment on table licenciamento.dispensa_licenciamento is 'Entidade responsável por armazenar as informações geradas, quando o tipo de licenciamento é uma DLA(Dispensa de licenciamento Ambiental).';
comment on column licenciamento.dispensa_licenciamento.id is 'Identificador único da entidade.';
comment on column licenciamento.dispensa_licenciamento.id_caracterizacao is 'Identificador da entidade caracterizacao que realizará o relacionamento entre as entidades dispensa_licenciamento e caracterizacao.';
comment on column licenciamento.dispensa_licenciamento.data_cadastro is 'Data da dispensa.';
comment on column licenciamento.dispensa_licenciamento.informacao_adicional is 'Informações inseridas pelo usuário no momento da geração do documento de dispensa.';
comment on column licenciamento.dispensa_licenciamento.id_documento is 'Identificador da entidade documento que realizará o relacionamento entre as entidades dispensa_licenciamento e documento.';
comment on column licenciamento.dispensa_licenciamento.numero is 'Registra o número da dispensa.';
comment on column licenciamento.dispensa_licenciamento.ativo is 'Indica se a dispensa está ativa (TRUE - Ativa; FALSE - Inativa).';

alter table licenciamento.dispensa_licenciamento owner to postgres;


create table licenciamento.atividade_caracterizacao
(
    id serial not null
        constraint pk_atividade_caracterizacao
            primary key,
    id_caracterizacao integer not null
        constraint fk_ac_caracterizacao
            references licenciamento.caracterizacao,
    id_atividade integer not null
        constraint fk_ac_atividade
            references licenciamento.atividade,
    id_porte_empreendimento integer
        constraint fk_ac_porte_empreendimento
            references licenciamento.porte_empreendimento,
    id_porte_empreendimento_para_calculo_do_porte integer
        constraint id_porte_empreendimento_para_calculo_do_porte
            references licenciamento.porte_empreendimento,
    is_principal boolean default false,
    distancia_capital double precision,
    valor_taxa_licenciamento numeric(13,2)
);
comment on table licenciamento.atividade_caracterizacao is 'Entidade responsável por armazenar as atividades relacionadas a caracterização.';
comment on column licenciamento.atividade_caracterizacao.id is 'Identificador único da entidade.';
comment on column licenciamento.atividade_caracterizacao.id_caracterizacao is 'Identificador da entidade caracterizacao que realizará o relacionamento entre as entidades caracterizacao e atividade_caracterizacao.';
comment on column licenciamento.atividade_caracterizacao.id_atividade is 'Identificador da entidade atividade que realizará o relacionamento entre as entidades atividade e atividade_caracterizacao.';
comment on column licenciamento.atividade_caracterizacao.id_porte_empreendimento is 'Identificador da entidade porte_empreendimento que faz o relacionamento entre as entidades atividade_caracterizacao e porte_empreendimento.';
comment on column licenciamento.atividade_caracterizacao.id_porte_empreendimento_para_calculo_do_porte is 'É o id que referecia o porte_empreendimento que é utilizado para o cálculo da taxa do porte';
comment on column licenciamento.atividade_caracterizacao.is_principal is 'Campo para verificar se a atividade caracterização é a principal';
comment on column licenciamento.atividade_caracterizacao.distancia_capital is 'Distância(km) do município da atividade até a capital.';
comment on column licenciamento.atividade_caracterizacao.valor_taxa_licenciamento is 'Valor da taxa de licenciamento da atividade licenciada.';

alter table licenciamento.atividade_caracterizacao owner to postgres;


create table licenciamento.geometria_atividade
(
    id serial not null
        constraint pk_geometria_atividade
            primary key,
    id_atividade_caracterizacao integer not null
        constraint fk_ga_atividade_caracterizacao
            references licenciamento.atividade_caracterizacao,
    area double precision,
    the_geom geometry
        constraint enforce_dims_the_geom
            check (st_ndims(the_geom) = 2)
        constraint enforce_srid_the_geom
            check (st_srid(the_geom) = 4326)
);
comment on table licenciamento.geometria_atividade is 'Entidade responsável por armazenar os georreferenciamentos das atividades de caracterização.';
comment on column licenciamento.geometria_atividade.id is 'Identificador único da entidade.';
comment on column licenciamento.geometria_atividade.id_atividade_caracterizacao is 'Identificador da entidade atividade_caracterizacao que realizará o relacionamento entre as entidades atividade_caracterizacao e geometria_atividade.';
comment on column licenciamento.geometria_atividade.area is 'Preenchido no caso do tipo da geometria ser polígono, armazenado em hectares.';
comment on column licenciamento.geometria_atividade.the_geom is 'Geometria da atividade, limitando-se ao tipo da geometria aceita na atividade.';

alter table licenciamento.geometria_atividade owner to postgres;


create table licenciamento.licenca
(
    id serial not null
        constraint pk_licenca
            primary key,
    id_documento integer
        constraint fk_l_documento
            references licenciamento.documento,
    id_caracterizacao integer not null
        constraint fk_l_caracterizacao
            references licenciamento.caracterizacao,
    data_cadastro timestamp not null,
    data_validade timestamp,
    numero varchar(30),
    id_licenca_anterior integer
        constraint fk_licenca_anterior
            references licenciamento.licenca,
    ativo boolean default true not null,
    data_validade_prorrogada timestamp,
    prorrogacao boolean default false,
    id_licenca_analise integer
);
comment on table licenciamento.licenca is 'Entidade responsável pelos registros referentes as licenças geradas.';
comment on column licenciamento.licenca.id is 'Identificador único da entidade.';
comment on column licenciamento.licenca.id_documento is 'Identificador da entidade documento que faz o relacionamento entre as entidades licenca e documento.';
comment on column licenciamento.licenca.id_caracterizacao is 'Identificador da entidade caracterizacao que faz o relacionamento entre as entidades licenca e caracterizacao.';
comment on column licenciamento.licenca.data_cadastro is 'Data em que a licença foi cadastrada na tabela.';
comment on column licenciamento.licenca.data_validade is 'Número de registro da Licença gerada.';
comment on column licenciamento.licenca.id_licenca_anterior is 'Armazena a licença antiga se ela for suspensa e depois uma nova licença for emitida.';
comment on column licenciamento.licenca.ativo is 'Flag para identificar se a licença está ou não ativa.';
comment on column licenciamento.licenca.data_validade_prorrogada is 'Nova data de prorrogação.';
comment on column licenciamento.licenca.prorrogacao is 'Flag que indica se a licença está em período de prorrogação.';
comment on column licenciamento.licenca.id_licenca_analise is 'Identificador da entidade analise.licenca_analise que faz o relacionamento entre as entidades licenca e analise.licenca_analise.';

alter table licenciamento.licenca owner to postgres;


create table licenciamento.rel_tipo_licenca_caracterizacao_andamento
(
    id_tipo_licenca integer not null
        constraint fk_rtlca_tipo_licenca
            references licenciamento.tipo_licenca,
    id_caracterizacao integer not null
        constraint fk_rtlca_caracterizacao
            references licenciamento.caracterizacao,
    constraint pk_rel_tipo_licenca_caracterizacao_andamento
        primary key (id_tipo_licenca, id_caracterizacao)
);
comment on table licenciamento.rel_tipo_licenca_caracterizacao_andamento is 'Entidade responsável pelos registros dos tipos de licença solicitadas em caracterizações que ainda estão em andamento.';
comment on column licenciamento.rel_tipo_licenca_caracterizacao_andamento.id_tipo_licenca is 'Identificador da entidade tipo_licenca.';
comment on column licenciamento.rel_tipo_licenca_caracterizacao_andamento.id_caracterizacao is 'Identificador da entidade caracterizacao.';

alter table licenciamento.rel_tipo_licenca_caracterizacao_andamento owner to postgres;


create table licenciamento.solicitacao_documento_caracterizacao
(
    id serial not null
        constraint pk_solicitacao_documento_caracterizacao
            primary key,
    id_caracterizacao integer not null
        constraint fk_sdc_caracterizacao
            references licenciamento.caracterizacao,
    id_tipo_documento integer not null
        constraint fk_sdc_tipo_documento
            references licenciamento.tipo_documento,
    obrigatorio boolean not null,
    id_documento integer
        constraint fk_sdc_documento
            references licenciamento.documento
);
comment on table licenciamento.solicitacao_documento_caracterizacao is 'Entidade responsável pelos documentos enviados referentes a caracterização.';
comment on column licenciamento.solicitacao_documento_caracterizacao.id is 'Identificador único da entidade.';
comment on column licenciamento.solicitacao_documento_caracterizacao.id_caracterizacao is 'Identificador da entidade caracterizacao que faz o relacionamento entre as entidades solicitacao_documento_caracterizacao e caracterizacao.';
comment on column licenciamento.solicitacao_documento_caracterizacao.id_tipo_documento is 'Identificador da entidade tipo_documento que faz o relacionamento entre as entidades solicitacao_documento_caracterizacao e tipo_documento.';
comment on column licenciamento.solicitacao_documento_caracterizacao.obrigatorio is 'Define se o tipo de documento é obrigatório para cada caracaterização.';
comment on column licenciamento.solicitacao_documento_caracterizacao.id_documento is 'Identificador da entidade documento que faz o relacionamento entre as entidades solicitacao_documento_caracterizacao e documento.';

alter table licenciamento.solicitacao_documento_caracterizacao owner to postgres;


create table licenciamento.dae
(
    id serial not null
        constraint pk_dae
            primary key,
    id_documento integer
        constraint fk_d_documento
            references licenciamento.documento,
    valor numeric(13,2) not null,
    id_caracterizacao integer not null
        constraint fk_d_caracterizacao
            references licenciamento.caracterizacao,
    numero varchar(30),
    competencia timestamp,
    data_cadastro timestamp not null,
    data_emissao timestamp,
    data_vencimento timestamp,
    cpf_cnpj_contribuinte varchar(14) not null,
    status integer not null,
    erro_emissao text,
    data_pagamento timestamp
);
comment on table licenciamento.dae is 'Entidade responsável pelo DAE gerado para cada caracterizacao.';
comment on column licenciamento.dae.id is 'Identificador único da entidade.';
comment on column licenciamento.dae.id_documento is 'Identificador da entidade documento que faz o relacionamento entre as entidades dae e documento.';
comment on column licenciamento.dae.valor is 'Valor do DAE.';
comment on column licenciamento.dae.id_caracterizacao is 'Identificador da entidade caracterizacao que faz o relacionamento entre as entidades dae e caracterizacao.';
comment on column licenciamento.dae.numero is 'Número do DAE gerado após a emissão do mesmo no webservice da Secretaria da Fazenda';
comment on column licenciamento.dae.competencia is 'Competência / período de referência do DAE';
comment on column licenciamento.dae.data_cadastro is 'Data em que o DAE foi cadastrada';
comment on column licenciamento.dae.data_emissao is 'Data em que o DAE foi emitido através do webservice da Secretaria da Fazenda';
comment on column licenciamento.dae.data_vencimento is 'Data de vencimento do DAE';
comment on column licenciamento.dae.cpf_cnpj_contribuinte is 'CPF/CNPJ do contribuinte';
comment on column licenciamento.dae.status is 'Status do DAE: 0 - Não emitido, 1 - Emitido, 2 - Erro ao emitir, 3 - Pago, 4 - Vencido';
comment on column licenciamento.dae.erro_emissao is 'Retorno do webservice da Secretaria da Fazenda quando ocorrer erro na emissão';
comment on column licenciamento.dae.data_pagamento is 'Data em que o DAE foi pago.';

alter table licenciamento.dae owner to postgres;


create table licenciamento.sobreposicao_caracterizacao_atividade
(
    id serial not null
        constraint pk_id_sobreposicao_caracterizacao_atividade
            primary key,
    id_tipo_sobreposicao integer not null
        constraint fk_sca_tipo_sobreposicao
            references licenciamento.tipo_sobreposicao,
    id_atividade_caracterizacao integer not null
        constraint fk_sca_atividade_caracterizacao
            references licenciamento.atividade_caracterizacao,
    geometria geometry
        constraint enforce_dims_geometria
            check (st_ndims(geometria) = 2)
        constraint enforce_srid_geometria
            check (st_srid(geometria) = 4326),
    distancia numeric,
    nome_area_sobreposicao varchar(200),
    data_area_sobreposicao varchar(20),
    cpf_cnpj_area_sobreposicao varchar(20)
);
comment on table licenciamento.sobreposicao_caracterizacao_atividade is 'Tabela que relaciona a sobreposicao de uma caracterização com uma atividade';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.id is 'Indentificador único da entidade';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.id_tipo_sobreposicao is 'Referência para o tipo de sobreposição';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.id_atividade_caracterizacao is 'Referência para a para a junção de atividade com caracterização';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.geometria is 'Campo que comtém os dados geométricos da entidade';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.distancia is 'Coluna que armazena a distância a geometria da sobreposição para á area de restrição.';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.nome_area_sobreposicao is 'Nome da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.data_area_sobreposicao is 'Data de criação da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_caracterizacao_atividade.cpf_cnpj_area_sobreposicao is 'CPF/CNPJ do responsável pela área que está sendo sobreposta';

alter table licenciamento.sobreposicao_caracterizacao_atividade owner to postgres;

grant select, usage on sequence licenciamento.sobreposicao_caracterizacao_atividade_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.sobreposicao_caracterizacao_atividade to licenciamento_ms;


create table licenciamento.documento_arrecadacao
(
    id_documento_arrecadacao integer not null
        constraint pk_documento_arrecadacao
            primary key,
    id_dae integer not null
        constraint fk_da_dae
            references licenciamento.dae
);
comment on table licenciamento.documento_arrecadacao is 'Entidade responsável por armazenar documentos de arrecadações gerados para pagamento';
comment on column licenciamento.documento_arrecadacao.id_documento_arrecadacao is 'Compo que guarda a referência para o boleto informado pelo gestão de pagamentos';
comment on column licenciamento.documento_arrecadacao.id_dae is 'Campo que guarda a referência para a dae';

alter table licenciamento.documento_arrecadacao owner to postgres;

grant insert, select, update, delete on licenciamento.documento_arrecadacao to licenciamento_ms;


create table licenciamento.atividade_caracterizacao_parametros
(
    id serial not null
        constraint pk_atividade_caracterizacao_parametros
            primary key,
    id_atividade_caracterizacao integer not null
        constraint fk_acp_atividade_caracterizacao
            references licenciamento.atividade_caracterizacao,
    id_parametro_atividade integer not null
        constraint fk_acp_parametro_atividade
            references licenciamento.parametro_atividade,
    valor_parametro numeric(13,4) not null
);
comment on table licenciamento.atividade_caracterizacao_parametros is 'Entidade de representa o relacionamento relacionamento das entidades atividade_caracterizacao e parametro_atividade';
comment on column licenciamento.atividade_caracterizacao_parametros.id is 'Identificador único da entidade';
comment on column licenciamento.atividade_caracterizacao_parametros.id_atividade_caracterizacao is 'Referência para a entidade atividade_caracterizacao';
comment on column licenciamento.atividade_caracterizacao_parametros.id_parametro_atividade is 'Referência para a entidade parametro_atividade';
comment on column licenciamento.atividade_caracterizacao_parametros.valor_parametro is 'Parâmetro enviado pelo usuário';

alter table licenciamento.atividade_caracterizacao_parametros owner to postgres;

grant select, usage on sequence licenciamento.atividade_caracterizacao_parametros_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.atividade_caracterizacao_parametros to licenciamento_ms;

create table licenciamento.comunicacao_dashboard
(
    id serial not null
        constraint pk_comunicacao_dashboard
            primary key,
    id_caracterizacao integer not null
        constraint fk_cd_caracterizacao
            references licenciamento.caracterizacao,
    tentativas integer not null,
    data_cadastro date default now() not null
);
comment on table licenciamento.comunicacao_dashboard is 'Entidade responsável por armazenar os dados de cominicacao com o dashboard alertas';
comment on column licenciamento.comunicacao_dashboard.id is 'Cheve primária da entidade';
comment on column licenciamento.comunicacao_dashboard.id_caracterizacao is 'Referência para a entidade caracterizacao que faz parte da comunicação';
comment on column licenciamento.comunicacao_dashboard.tentativas is 'Numero de tentativas de comunicação com o sashboard alertas';
comment on column licenciamento.comunicacao_dashboard.data_cadastro is 'Data de cadastro do registro na entidade';

alter table licenciamento.comunicacao_dashboard owner to postgres;

grant select, usage on sequence licenciamento.comunicacao_dashboard_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.comunicacao_dashboard to licenciamento_ms;


create table licenciamento.dispensa_licencamento_cancelada
(
    id serial not null
        constraint pk_dispensa_licencamento_cancelada
            primary key,
    id_dispensa_licencamento integer not null
        constraint uq_dispensa_licencamento_cancelada_id_dispensa_licencamento
            unique
        constraint fk_dlc_dispensa_licenciamento
            references licenciamento.dispensa_licenciamento,
    justificativa text,
    id_usuario_executor integer not null
        constraint fk_dlc_usuario_licenciamento_executor
            references licenciamento.usuario_licenciamento,
    data_cancelamento timestamp not null
);
comment on table licenciamento.dispensa_licencamento_cancelada is 'Entidade responsável por armazenar as Dispensas de licenciamento Ambiental canceladas.';
comment on column licenciamento.dispensa_licencamento_cancelada.id is 'Identificador único da entidade.';
comment on column licenciamento.dispensa_licencamento_cancelada.id_dispensa_licencamento is 'Identificador da entidade licenciamento.dispensa_licenciamento que realiza o relacionamento entre as entidades dispensa_licencamento_cancelada e licenciamento.dispensa_licenciamento.';
comment on column licenciamento.dispensa_licencamento_cancelada.justificativa is 'Justificativa do cancelamento da Dispensa de licenciamento Ambiental.';
comment on column licenciamento.dispensa_licencamento_cancelada.id_usuario_executor is 'Identificador da entidade licenciamento.usuario_licenciamento que realiza o relacionamento entre as entidades dispensa_licencamento_cancelada e licenciamento.usuario_licenciamento e identifica o usuário executor da ação.';
comment on column licenciamento.dispensa_licencamento_cancelada.data_cancelamento is 'Data do cancelamento da Dispensa de licenciamento Ambiental.';

alter table licenciamento.dispensa_licencamento_cancelada owner to postgres;

grant select, usage on sequence licenciamento.dispensa_licencamento_cancelada_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.dispensa_licencamento_cancelada to licenciamento_ms;


create table licenciamento.questionario_3
(
    id serial not null
        constraint pk_questionario_3
            primary key,
    id_caracterizacao integer not null
        constraint fk_q3_caracterizacao
            references licenciamento.caracterizacao,
    data_cadastro timestamp not null,
    ca boolean not null,
    ca_asub_consumo_medio numeric(13,2),
    ca_asub_uso_domestico boolean,
    ca_asub_uso_industrial boolean,
    ca_asup_consumo_medio numeric(13,2),
    ca_asup_uso_domestico boolean,
    ca_asup_uso_industrial boolean,
    ca_rp_consumo_medio numeric(13,2),
    ca_rp_uso_domestico boolean,
    ca_rp_uso_industrial boolean,
    ef boolean not null,
    ef_dom_destino_final_fossa_filtro boolean,
    ef_dom_destino_final_fossa_septica boolean,
    ef_dom_destino_final_outros boolean,
    ef_dom_destino_final_outros_espec text,
    ef_dom_destino_final_remocao_oleo_graxa boolean,
    ef_dom_regime_carga text,
    ef_dom_tipo_tratamento_ete boolean,
    ef_dom_tipo_tratamento_fossa_filtro boolean,
    ef_dom_tipo_tratamento_fossa_septica boolean,
    ef_dom_tipo_tratamento_outros boolean,
    ef_dom_tipo_tratamento_outros_espec text,
    ef_dom_tipo_tratamento_remocao_oleo_graxa boolean,
    ef_dom_vazao_media numeric(13,2),
    ef_ind_destino_final_fossa_filtro boolean,
    ef_ind_destino_final_fossa_septica boolean,
    ef_ind_destino_final_outros boolean,
    ef_ind_destino_final_outros_espec text,
    ef_ind_destino_final_remocao_oleo_graxa boolean,
    ef_ind_regime_carga text,
    ef_ind_tipo_tratamento_ete boolean,
    ef_ind_tipo_tratamento_fossa_filtro boolean,
    ef_ind_tipo_tratamento_fossa_septica boolean,
    ef_ind_tipo_tratamento_outros boolean,
    ef_ind_tipo_tratamento_outros_espec text,
    ef_ind_tipo_tratamento_remocao_oleo_graxa boolean,
    ef_ind_vazao_media numeric(13,2),
    rs boolean not null,
    rs_dom_quantidade_media numeric(13,2),
    rs_dom_tipo_coleta_propria boolean,
    rs_dom_tipo_coleta_publica boolean,
    rs_dom_tipo_coleta_terceiros boolean,
    rs_dom_tipo_coleta_terceiros_espec text,
    rs_dom_tratamento_disposicao_aca boolean,
    rs_dom_tratamento_disposicao_as boolean,
    rs_dom_tratamento_disposicao_compostagem boolean,
    rs_dom_tratamento_disposicao_inc boolean,
    rs_dom_tratamento_disposicao_outros boolean,
    rs_dom_tratamento_disposicao_outros_espec text,
    rs_dom_tratamento_disposicao_reciclagem boolean,
    rs_esc_quantidade_media numeric(13,2),
    rs_esc_tipo_coleta_propria boolean,
    rs_esc_tipo_coleta_publica boolean,
    rs_esc_tipo_coleta_terceiros boolean,
    rs_esc_tipo_coleta_terceiros_espec text,
    rs_esc_tratamento_disposicao_aca boolean,
    rs_esc_tratamento_disposicao_as boolean,
    rs_esc_tratamento_disposicao_inc boolean,
    rs_esc_tratamento_disposicao_outros boolean,
    rs_esc_tratamento_disposicao_outros_espec text,
    rs_esc_tratamento_disposicao_reciclagem boolean,
    rs_indperig_quantidade_media numeric(13,2),
    rs_indperig_tipo_coleta_propria boolean,
    rs_indperig_tipo_coleta_publica boolean,
    rs_indperig_tipo_coleta_terceiros boolean,
    rs_indperig_tipo_coleta_terceiros_espec text,
    rs_indperig_tratamento_disposicao_aca boolean,
    rs_indperig_tratamento_disposicao_aterro_industrial boolean,
    rs_indperig_tratamento_disposicao_as boolean,
    rs_indperig_tratamento_disposicao_inc boolean,
    rs_indperig_tratamento_disposicao_outros boolean,
    rs_indperig_tratamento_disposicao_outros_espec text,
    rs_indperig_tratamento_disposicao_reciclagem boolean,
    rs_ind_quantidade_media numeric(13,2),
    rs_ind_tipo_coleta_propria boolean,
    rs_ind_tipo_coleta_publica boolean,
    rs_ind_tipo_coleta_terceiros boolean,
    rs_ind_tipo_coleta_terceiros_espec text,
    rs_ind_tratamento_disposicao_aca boolean,
    rs_ind_tratamento_disposicao_aterro_industrial boolean,
    rs_ind_tratamento_disposicao_as boolean,
    rs_ind_tratamento_disposicao_inc boolean,
    rs_ind_tratamento_disposicao_outros boolean,
    rs_ind_tratamento_disposicao_outros_espec text,
    rs_ind_tratamento_disposicao_reaproveitamento_processo boolean,
    rs_ind_tratamento_disposicao_reciclagem boolean,
    rs_outros_quantidade_media numeric(13,2),
    rs_outros_origem text,
    rs_outros_tipo_coleta_propria boolean,
    rs_outros_tipo_coleta_publica boolean,
    rs_outros_tipo_coleta_terceiros boolean,
    rs_outros_tipo_coleta_terceiros_espec text,
    rs_outros_tratamento_disposicao_aca boolean,
    rs_outros_tratamento_disposicao_as boolean,
    rs_outros_tratamento_disposicao_inc boolean,
    rs_outros_tratamento_disposicao_outros boolean,
    rs_outros_tratamento_disposicao_outros_espec text,
    ca_asub boolean,
    ca_asup boolean,
    ca_rp boolean,
    ef_dom boolean,
    ef_ind boolean,
    rs_dom boolean,
    rs_esc boolean,
    rs_indperig boolean,
    rs_ind boolean,
    rs_outros boolean
);
comment on table licenciamento.questionario_3 is 'Entidade responsável por armazenar os as respostas do questionário 3';
comment on column licenciamento.questionario_3.id is 'Chave primária da entidade';
comment on column licenciamento.questionario_3.id_caracterizacao is 'Referência para a entidade caracterizacao';
comment on column licenciamento.questionario_3.data_cadastro is 'Data de cadastro do registro na entidade';
comment on column licenciamento.questionario_3.ca is 'Flag que indica consumo de água';
comment on column licenciamento.questionario_3.ca_asub_consumo_medio is 'Valor informado do consumo médio de água subterrânea (consumo de água)';
comment on column licenciamento.questionario_3.ca_asub_uso_domestico is 'Flag que indica uso doméstico de água subterrânea (consumo de água)';
comment on column licenciamento.questionario_3.ca_asub_uso_industrial is 'Flag que indica uso industrial de água subterrânea (consumo de água)';
comment on column licenciamento.questionario_3.ca_asup_consumo_medio is 'Valor informado do consumo médio de água superficial (consumo de água)';
comment on column licenciamento.questionario_3.ca_asup_uso_domestico is 'Flag que indica uso doméstico de água superficial (consumo de água)';
comment on column licenciamento.questionario_3.ca_asup_uso_industrial is 'Flag que indica uso industrial de água superficial (consumo de água)';
comment on column licenciamento.questionario_3.ca_rp_consumo_medio is 'Valor informado do consumo médio de rede pública (consumo de água)';
comment on column licenciamento.questionario_3.ca_rp_uso_domestico is 'Flag que indica uso doméstico de rede pública (consumo de água)';
comment on column licenciamento.questionario_3.ca_rp_uso_industrial is 'Flag que indica uso industrial de rede pública (consumo de água)';
comment on column licenciamento.questionario_3.ef is 'Flag que indica efluentes';
comment on column licenciamento.questionario_3.ef_dom_destino_final_fossa_filtro is 'Flag que indica destino final - fossa filtro de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_destino_final_fossa_septica is 'Flag que indica destino final - fossa septica de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_destino_final_outros is 'Flag que indica destino final - outros de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_destino_final_outros_espec is 'Valor informado do especificar de destino final - outros de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_destino_final_remocao_oleo_graxa is 'Flag que indica destino final - remoção de óleo e graxa de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_regime_carga is 'Código que indica regime de carga de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_ete is 'Flag que indica tipo de tratamento - ETE de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_fossa_filtro is 'Flag que indica tipo de tratamento - fossa filtro de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_fossa_septica is 'Flag que indica tipo de tratamento - fossa septica de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_outros is 'Flag que indica tipo de tratamento - outros de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_outros_espec is 'Valor informado do especificar de tipo de tratamento - outros de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_tipo_tratamento_remocao_oleo_graxa is 'Flag que indica tipo de tratamento - remoção de óleo e graxa de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_dom_vazao_media is 'Valor informado da vazão média de doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_destino_final_fossa_filtro is 'Flag que indica destino final - fossa filtro de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_destino_final_fossa_septica is 'Flag que indica destino final - fossa septica de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_destino_final_outros is 'Flag que indica destino final - outros de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_destino_final_outros_espec is 'Valor informado do especificar de destino final - outros de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_destino_final_remocao_oleo_graxa is 'Flag que indica destino final - remoção de óleo e graxa de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_regime_carga is 'Código que indica regime de carga de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_ete is 'Flag que indica tipo de tratamento - ETE de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_fossa_filtro is 'Flag que indica tipo de tratamento - fossa filtro de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_fossa_septica is 'Flag que indica tipo de tratamento - fossa septica de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_outros is 'Flag que indica tipo de tratamento - outros de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_outros_espec is 'Valor informado do especificar de tipo de tratamento - outros de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_tipo_tratamento_remocao_oleo_graxa is 'Flag que indica tipo de tratamento - remoção de óleo e graxa de indústria (efluentes)';
comment on column licenciamento.questionario_3.ef_ind_vazao_media is 'Valor informado da vazão média de indústria (efluentes)';
comment on column licenciamento.questionario_3.rs is 'Flag que indica resíduos sólidos';
comment on column licenciamento.questionario_3.rs_dom_quantidade_media is 'Valor informado da quantidade média de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tipo_coleta_propria is 'Flag que indica tipo de coleta - propria de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tipo_coleta_publica is 'Flag que indica tipo de coleta - publica de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tipo_coleta_terceiros is 'Flag que indica tipo de coleta - terceiros de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tipo_coleta_terceiros_espec is 'Valor informado do especificar tipo de coleta - terceiros de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_aca is 'Flag que indica tratamento/disposição - aterro a céu aberto de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_as is 'Flag que indica tratamento/disposição - aterro sanitário de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_compostagem is 'Flag que indica tratamento/disposição - compostagem de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_inc is 'Flag que indica tratamento/disposição - incineração de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_outros is 'Flag que indica tratamento/disposição - outros de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_outros_espec is 'Valor informado do especificar tratamento/disposição - outros de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_dom_tratamento_disposicao_reciclagem is 'Flag que indica tratamento/disposição - reciclagem de doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_quantidade_media is 'Valor informado da quantidade média de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tipo_coleta_propria is 'Flag que indica tipo de coleta - propria de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tipo_coleta_publica is 'Flag que indica tipo de coleta - publica de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tipo_coleta_terceiros is 'Flag que indica tipo de coleta - terceiros de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tipo_coleta_terceiros_espec is 'Valor informado do especificar tipo de coleta - terceiros de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_aca is 'Flag que indica tratamento/disposição - aterro a céu aberto de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_as is 'Flag que indica tratamento/disposição - aterro sanitário de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_inc is 'Flag que indica tratamento/disposição - incineração de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_outros is 'Flag que indica tratamento/disposição - outros de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_outros_espec is 'Valor informado do especificar tratamento/disposição - outros de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc_tratamento_disposicao_reciclagem is 'Flag que indica tratamento/disposição - reciclagem de escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_quantidade_media is 'Valor informado da quantidade média de indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tipo_coleta_propria is 'Flag que indica tipo de coleta - propria de indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tipo_coleta_publica is 'Flag que indica tipo de coleta - publica de indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tipo_coleta_terceiros is 'Flag que indica tipo de coleta - terceiros de indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tipo_coleta_terceiros_espec is 'Valor informado do especificar tipo de coleta - terceiros de indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_aca is 'Flag que indica tratamento/disposição - aterro a céu aberto de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_aterro_industrial is 'Flag que indica tratamento/disposição - aterro industrial de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_as is 'Flag que indica tratamento/disposição - aterro sanitário de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_inc is 'Flag que indica tratamento/disposição - incineração de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_outros is 'Flag que indica tratamento/disposição - outros de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_outros_espec is 'Valor informado do especificar tratamento/disposição - outros de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig_tratamento_disposicao_reciclagem is 'Flag que indica tratamento/disposição - reciclagem de industrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_quantidade_media is 'Valor informado da quantidade média de indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tipo_coleta_propria is 'Flag que indica tipo de coleta - propria de indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tipo_coleta_publica is 'Flag que indica tipo de coleta - publica de indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tipo_coleta_terceiros is 'Flag que indica tipo de coleta - terceiros de indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tipo_coleta_terceiros_espec is 'Valor informado do especificar tipo de coleta - terceiros de indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_aca is 'Flag que indica tratamento/disposição - aterro a céu aberto de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_aterro_industrial is 'Flag que indica tratamento/disposição - aterro industrial de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_as is 'Flag que indica tratamento/disposição - aterro sanitário de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_inc is 'Flag que indica tratamento/disposição - incineração de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_outros is 'Flag que indica tratamento/disposição - outros de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_outros_espec is 'Valor informado do especificar tratamento/disposição - outros de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_reaproveitamento_processo is 'Flag que indica tratamento/disposição - reaproveitamento no processo de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind_tratamento_disposicao_reciclagem is 'Flag que indica tratamento/disposição - reciclagem de industrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_quantidade_media is 'Valor informado da quantidade média de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_origem is 'Texto informado da origem outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tipo_coleta_propria is 'Flag que indica tipo de coleta - propria de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tipo_coleta_publica is 'Flag que indica tipo de coleta - publica de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tipo_coleta_terceiros is 'Flag que indica tipo de coleta - terceiros de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tipo_coleta_terceiros_espec is 'Valor informado do especificar tipo de coleta - terceiros de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tratamento_disposicao_aca is 'Flag que indica tratamento/disposição - aterro a céu aberto de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tratamento_disposicao_as is 'Flag que indica tratamento/disposição - aterro sanitário de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tratamento_disposicao_inc is 'Flag que indica tratamento/disposição - incineração de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tratamento_disposicao_outros is 'Flag que indica tratamento/disposição - outros de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros_tratamento_disposicao_outros_espec is 'Valor informado do especificar tratamento/disposição - outros de outros (resíduos sólidos)';
comment on column licenciamento.questionario_3.ca_asub is 'Flag que indica água subterrânea (consumo de água)';
comment on column licenciamento.questionario_3.ca_asup is 'Flag que indica água superficial (consumo de água)';
comment on column licenciamento.questionario_3.ca_rp is 'Flag que indica rede pública (consumo de água)';
comment on column licenciamento.questionario_3.ef_dom is 'Flag que indica doméstico (efluentes)';
comment on column licenciamento.questionario_3.ef_ind is 'Flag que indica indústria (efluentes)';
comment on column licenciamento.questionario_3.rs_dom is 'Flag que indica doméstico (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_esc is 'Flag que indica escritório (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_indperig is 'Flag que indica indústrial perigosa (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_ind is 'Flag que indica indústrial (resíduos sólidos)';
comment on column licenciamento.questionario_3.rs_outros is 'Flag que indica outros (resíduos sólidos)';

alter table licenciamento.questionario_3 owner to postgres;

grant select, usage on sequence licenciamento.questionario_3_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.questionario_3 to licenciamento_ms;


create table licenciamento.comunicacao_rede_simples
(
    id serial not null
        constraint pk_comunicacao_rede_simples
            primary key,
    id_caracterizacao integer not null
        constraint fk_crs_caracterizacao
            references licenciamento.caracterizacao,
    tentativas integer not null,
    data_cadastro date default now() not null
);
comment on table licenciamento.comunicacao_rede_simples is 'Entidade responsável por armazenar os dados de cominicacao com a Rede Simples';
comment on column licenciamento.comunicacao_rede_simples.id is 'Cheve primária da entidade';
comment on column licenciamento.comunicacao_rede_simples.id_caracterizacao is 'Referência para a entidade caracterizacao que faz parte da comunicação';
comment on column licenciamento.comunicacao_rede_simples.tentativas is 'Numero de tentativas de comunicação com a Rede Simples';
comment on column licenciamento.comunicacao_rede_simples.data_cadastro is 'Data de cadastro do registro na entidade';

alter table licenciamento.comunicacao_rede_simples owner to postgres;

grant select, usage on sequence licenciamento.comunicacao_rede_simples_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.comunicacao_rede_simples to licenciamento_ms;

create table licenciamento.dae_licenciamento
(
    id serial not null
        constraint pk_dae_licenciamento
            primary key,
    id_documento integer
        constraint fk_dl_documento
            references licenciamento.documento,
    valor numeric(13,2) not null,
    id_caracterizacao integer not null
        constraint fk_dl_caracterizacao
            references licenciamento.caracterizacao,
    numero varchar(30),
    competencia timestamp,
    data_cadastro timestamp not null,
    data_emissao timestamp,
    data_vencimento timestamp,
    cpf_cnpj_contribuinte varchar(14) not null,
    status integer not null,
    erro_emissao text,
    data_pagamento timestamp
);
comment on table licenciamento.dae_licenciamento is 'Entidade responsável pelo DAE da taxa de licenciamento gerado para cada caracterizacao.';
comment on column licenciamento.dae_licenciamento.id is 'Identificador único da entidade.';
comment on column licenciamento.dae_licenciamento.id_documento is 'Identificador da entidade documento que faz o relacionamento entre as entidades dae_licenciamento e documento.';
comment on column licenciamento.dae_licenciamento.valor is 'Valor do DAE da taxa de licenciamento.';
comment on column licenciamento.dae_licenciamento.id_caracterizacao is 'Identificador da entidade caracterizacao que faz o relacionamento entre as entidades dae_licenciamento e caracterizacao.';
comment on column licenciamento.dae_licenciamento.numero is 'Número do DAE da taxa de licenciamento gerado após a emissão do mesmo no webservice da Secretaria da Fazenda';
comment on column licenciamento.dae_licenciamento.competencia is 'Competência / período de referência do DAE da taxa de licenciamento';
comment on column licenciamento.dae_licenciamento.data_cadastro is 'Data em que o DAE da taxa de licenciamento foi cadastrada';
comment on column licenciamento.dae_licenciamento.data_emissao is 'Data em que o DAE da taxa de licenciamento foi emitido através do webservice da Secretaria da Fazenda';
comment on column licenciamento.dae_licenciamento.data_vencimento is 'Data de vencimento do DAE da taxa de licenciamento';
comment on column licenciamento.dae_licenciamento.cpf_cnpj_contribuinte is 'CPF/CNPJ do contribuinte';
comment on column licenciamento.dae_licenciamento.status is 'Status do DAE da taxa de licenciamento: 0 - Não emitido, 1 - Emitido, 2 - Erro ao emitir, 3 - Pago, 4 - Vencido';
comment on column licenciamento.dae_licenciamento.erro_emissao is 'Retorno do webservice da Secretaria da Fazenda quando ocorrer erro na emissão';
comment on column licenciamento.dae_licenciamento.data_pagamento is 'Data em que o DAE da taxa de licenciamento foi pago.';

alter table licenciamento.dae_licenciamento owner to postgres;

grant select, usage on sequence licenciamento.dae_licenciamento_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.dae_licenciamento to licenciamento_ms;

create table licenciamento.documento_arrecadacao_licenciamento
(
    id_documento_arrecadacao_licenciamento integer not null
        constraint pk_documento_arrecadacao_licenciamento
            primary key,
    id_dae_licenciamento integer not null
        constraint fk_dal_dae_licenciamento
            references licenciamento.dae_licenciamento
);
comment on table licenciamento.documento_arrecadacao_licenciamento is 'Entidade responsável por armazenar documentos de arrecadações gerados para pagamento';
comment on column licenciamento.documento_arrecadacao_licenciamento.id_documento_arrecadacao_licenciamento is 'Compo que guarda a referência para o boleto informado pelo gestão de pagamentos';
comment on column licenciamento.documento_arrecadacao_licenciamento.id_dae_licenciamento is 'Campo que guarda a referência para a dae_licenciamento';

alter table licenciamento.documento_arrecadacao_licenciamento owner to postgres;

grant insert, select, update, delete on licenciamento.documento_arrecadacao_licenciamento to licenciamento_ms;

create table licenciamento.geometria_complexo
(
    id serial not null
        constraint pk_geometria_complexo
            primary key,
    id_caracterizacao integer not null
        constraint fk_gc_caracterizacao
            references licenciamento.caracterizacao,
    area double precision,
    geometria geometry
        constraint enforce_dims_geometria
            check (st_ndims(geometria) = 2)
        constraint enforce_srid_geometria
            check (st_srid(geometria) = 4326)
);
comment on table licenciamento.geometria_complexo is 'Entidade responsável por armazenar os georreferenciamentos do comlexo da caracterização.';
comment on column licenciamento.geometria_complexo.id is 'Identificador único da entidade.';
comment on column licenciamento.geometria_complexo.id_caracterizacao is 'Identificador da entidade caracterizacao que realizará o relacionamento entre as entidades caracterizacao e geometria_complexo.';
comment on column licenciamento.geometria_complexo.area is 'Preenchido no caso do tipo da geometria ser polígono, armazenado em hectares.';
comment on column licenciamento.geometria_complexo.geometria is 'Geometria do complexo, limitando-se ao tipo da geometria aceita no complexo.';

alter table licenciamento.geometria_complexo owner to postgres;

grant select, usage on sequence licenciamento.geometria_complexo_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.geometria_complexo to licenciamento_ms;

create table licenciamento.sobreposicao_complexo
(
    id serial not null
        constraint pk_sobreposicao_complexo
            primary key,
    id_tipo_sobreposicao integer not null
        constraint fk_sc_tipo_sobreposicao
            references licenciamento.tipo_sobreposicao,
    id_caracterizacao integer not null
        constraint fk_sc_caracterizacao
            references licenciamento.caracterizacao,
    geometria geometry
        constraint enforce_dims_geometria
            check (st_ndims(geometria) = 2)
        constraint enforce_srid_geometria
            check (st_srid(geometria) = 4326),
    distancia numeric,
    nome_area_sobreposicao varchar(200),
    data_area_sobreposicao varchar(20),
    cpf_cnpj_area_sobreposicao varchar(20)
);
comment on table licenciamento.sobreposicao_complexo is 'Tabela que relaciona a sobreposicao de uma caracterização com um complexo';
comment on column licenciamento.sobreposicao_complexo.id is 'Indentificador único da entidade';
comment on column licenciamento.sobreposicao_complexo.id_tipo_sobreposicao is 'Referência para o tipo de sobreposição';
comment on column licenciamento.sobreposicao_complexo.id_caracterizacao is 'Referência para a para a junção da sobreposição do complexo com caracterização';
comment on column licenciamento.sobreposicao_complexo.geometria is 'Campo que comtém os dados geométricos da entidade';
comment on column licenciamento.sobreposicao_complexo.distancia is 'Coluna que armazena a distância a geometria da sobreposição para á area de restrição.';
comment on column licenciamento.sobreposicao_complexo.nome_area_sobreposicao is 'Nome da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_complexo.data_area_sobreposicao is 'Data de criação da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_complexo.cpf_cnpj_area_sobreposicao is 'CPF/CNPJ do responsável pela área que está sendo sobreposta';

alter table licenciamento.sobreposicao_complexo owner to postgres;

grant select, usage on sequence licenciamento.sobreposicao_complexo_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.sobreposicao_complexo to licenciamento_ms;


create table licenciamento.sobreposicao_caracterizacao_empreendimento
(
    id serial not null
        constraint pk_sobreposicao_caracterizacao_empreendimento
            primary key,
    id_tipo_sobreposicao integer not null
        constraint fk_sce_tipo_sobreposicao
            references licenciamento.tipo_sobreposicao,
    id_caracterizacao integer not null
        constraint fk_sce_caracterizacao
            references licenciamento.caracterizacao,
    geometria geometry
        constraint enforce_dims_geometria
            check (st_ndims(geometria) = 2)
        constraint enforce_srid_geometria
            check (st_srid(geometria) = 4326),
    distancia numeric,
    nome_area_sobreposicao varchar(200),
    data_area_sobreposicao varchar(20),
    cpf_cnpj_area_sobreposicao varchar(20)
);
comment on table licenciamento.sobreposicao_caracterizacao_empreendimento is 'Tabela que relaciona a sobreposicao de uma caracterização com o empreendimento';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.id is 'Indentificador único da entidade';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.id_tipo_sobreposicao is 'Referência para o tipo de sobreposição';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.id_caracterizacao is 'Referência para a para a junção da sobreposição do empreendimento com caracterização';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.geometria is 'Campo que comtém os dados geométricos da sobreposiçao';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.distancia is 'Coluna que armazena a distância a geometria da sobreposição para á area de restrição.';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.nome_area_sobreposicao is 'Nome da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.data_area_sobreposicao is 'Data de criação da área que está sendo sobreposta';
comment on column licenciamento.sobreposicao_caracterizacao_empreendimento.cpf_cnpj_area_sobreposicao is 'CPF/CNPJ do responsável pela área que está sendo sobreposta';

alter table licenciamento.sobreposicao_caracterizacao_empreendimento owner to postgres;

grant select, usage on sequence licenciamento.sobreposicao_caracterizacao_empreendimento_id_seq to licenciamento_ms;

grant insert, select, update, delete on licenciamento.sobreposicao_caracterizacao_empreendimento to licenciamento_ms;

create table licenciamento.solicitacao_grupo_documento
(
    id serial not null
        constraint pk_solicitacao_grupo_documento
            primary key,
    id_atividade_caracterizacao integer not null
        constraint fk_sgd_atividade_caracterizacao
            references licenciamento.atividade_caracterizacao,
    id_grupo_documento integer not null
        constraint fk_sgd_grupo_documento_solicitacao
            references licenciamento.grupo_documento,
    id_tipo_documento integer not null
        constraint fk_sgd_tipo_documento
            references licenciamento.tipo_documento,
    obrigatorio boolean not null,
    id_documento integer
        constraint fk_sgd_documento
            references licenciamento.documento
);
comment on table licenciamento.solicitacao_grupo_documento is 'Entidade responsável por armazenar os documentos enviados referentes para cada caracterização por grupo de atividade.';
comment on column licenciamento.solicitacao_grupo_documento.id is 'Identificador único da entidade.';
comment on column licenciamento.solicitacao_grupo_documento.id_atividade_caracterizacao is 'Referência  para a caracterização qual a solicitação ele pertence.';
comment on column licenciamento.solicitacao_grupo_documento.id_grupo_documento is 'Referência  para o grupo de documentos o qual a solicitação ele pertence.';
comment on column licenciamento.solicitacao_grupo_documento.id_tipo_documento is 'Referência para o tipo_documento que está sendo solicitado.';
comment on column licenciamento.solicitacao_grupo_documento.obrigatorio is 'Define se o tipo de documento é obrigatório para cada grupo de atividade.';
comment on column licenciamento.solicitacao_grupo_documento.id_documento is 'Refrência para o documento do tipo de documento do grupo.';

alter table licenciamento.solicitacao_grupo_documento owner to postgres;
grant select, usage on sequence licenciamento.solicitacao_grupo_documento_id_seq to licenciamento_ms;
grant insert, select, update, delete on licenciamento.solicitacao_grupo_documento to licenciamento_ms;

create table licenciamento.envio_informativo_email
(
    id serial not null
        constraint pk_envio_informativo_email
            primary key,
    cpf_cnpj varchar(24) not null
);
comment on table licenciamento.envio_informativo_email is 'Entidade responsável por armazenar solicitação de envio de email de empreendimento';
comment on column licenciamento.envio_informativo_email.id is 'Identificador único da entidade.';
comment on column licenciamento.envio_informativo_email.cpf_cnpj is 'Identificador do representante do empreendimento que teve seu email enviado';

alter table licenciamento.envio_informativo_email owner to postgres;

grant insert, select, update, delete on licenciamento.envio_informativo_email to licenciamento_ms;


create table licenciamento.codigos_taxa_licenciamento
(
    id integer not null
        constraint pk_codigos_taxa_licenciamento
            primary key,
    descricao varchar(40)
);
comment on table licenciamento.codigos_taxa_licenciamento is 'Entidade responsável por armazenar informações sobre as tabelas de taxa licenciamento';
comment on column licenciamento.codigos_taxa_licenciamento.id is 'Identificador único da entidade';
comment on column licenciamento.codigos_taxa_licenciamento.descricao is 'Descrição do que se trata o código da tabela taxa_licenciamento';

alter table licenciamento.codigos_taxa_licenciamento owner to postgres;


create table licenciamento.rel_tipo_licenca_grupo_documento
(
    id_grupo_documento integer not null
        constraint fk_tlgd_grupo_documento
            references licenciamento.grupo_documento,
    id_tipo_licenca integer not null
        constraint fk_tlgd_tipo_licenca
            references licenciamento.tipo_licenca,
    id_tipo_documento integer not null
        constraint fk_tlgd_tipo_documento
            references licenciamento.tipo_documento,
    obrigatorio boolean default false not null,
    ordem integer,
    constraint pk_rel_tipo_licenca_grupo_documento
        primary key (id_grupo_documento, id_tipo_licenca, id_tipo_documento)
);
comment on table licenciamento.rel_tipo_licenca_grupo_documento is 'Entidade responsável por fazer o relacionamento da licença, com o grupo documento, com o tipo documento';
comment on column licenciamento.rel_tipo_licenca_grupo_documento.id_grupo_documento is 'Referência para o grupo de documentos';
comment on column licenciamento.rel_tipo_licenca_grupo_documento.id_tipo_licenca is 'Referência para o tipo de licenças';
comment on column licenciamento.rel_tipo_licenca_grupo_documento.id_tipo_documento is 'Referência para o tipo de documento';
comment on column licenciamento.rel_tipo_licenca_grupo_documento.ordem is 'Ordem dos documentos.';

alter table licenciamento.rel_tipo_licenca_grupo_documento owner to postgres;

grant insert, select, update, delete on licenciamento.rel_tipo_licenca_grupo_documento to licenciamento_ms;


create table licenciamento.atividade_parametro_limites
(
    id serial not null
        constraint pk_apl_atividade_parametro_limites
            primary key,
    id_atividade integer not null
        constraint fk_ac_atividade
            references licenciamento.atividade,
    limite_inferior numeric(20,4),
    limite_superior numeric(20,4),
    id_parametro_atividade integer not null
        constraint fk_rapa_parametro_atividade
            references licenciamento.parametro_atividade
);
comment on table licenciamento.atividade_parametro_limites is 'Entidade responsável por armazenar os limites máximos e mínimos dos parâmetros das atividades para fins de controle e validação.';
comment on column licenciamento.atividade_parametro_limites.id is 'Identificador único da entidade atividade_parametro_limites.';
comment on column licenciamento.atividade_parametro_limites.id_atividade is 'Identificador único que vincula a tabela atividade_parametro_limites a tabela atividade.';
comment on column licenciamento.atividade_parametro_limites.limite_inferior is 'Valor numérico que indica o menor valor que pode ser inserido pelo usuário como parâmetro no sistema.';
comment on column licenciamento.atividade_parametro_limites.limite_superior is 'Valor numérico que indica o maior valor que pode ser inserido pelo usuário como parâmetro no sistema.';
comment on column licenciamento.atividade_parametro_limites.id_parametro_atividade is 'Identificador único que vincula a tabela atividade_parametro_limites a tabela parametro_atividade.';

alter table licenciamento.atividade_parametro_limites owner to licenciamento_ms;


grant insert, select, update, delete, truncate, references, trigger on licenciamento.atividade_parametro_limites to postgres;

create table licenciamento.uferms
(
    id serial not null,
    valor numeric
);
comment on table licenciamento.uferms is 'Tabela para armazenar o valor atual da UFERMS';
comment on column licenciamento.uferms.id is 'Coluna que armazena o id da taxa';
comment on column licenciamento.uferms.valor is 'Coluna que armazena o valor da taxa.';

alter table licenciamento.uferms owner to postgres;

grant select on licenciamento.uferms to licenciamento_ms;

create table licenciamento.distancia_capital
(
    codigo_ibge integer not null
        constraint pk_distancia_capital
            primary key,
    nome_municipio varchar(200) not null,
    distancia double precision not null
);
comment on table licenciamento.distancia_capital is 'Entidade responsavel por armazenar a distância de cada município até a capital do Estado do Mato Grosso do Sul - Campo Grande.';
comment on column licenciamento.distancia_capital.codigo_ibge is 'Codigo de IBGE do município.';
comment on column licenciamento.distancia_capital.nome_municipio is 'Nome do município.';
comment on column licenciamento.distancia_capital.distancia is 'Distância(km) desse município até a capital.';

alter table licenciamento.distancia_capital owner to postgres;

create table licenciamento.variavel_calculo_taxa
(
    id serial not null
        constraint pk_variavel_calculo_taxa
            primary key,
    nome varchar(200) not null,
    sigla varchar(5) not null
);
comment on table licenciamento.variavel_calculo_taxa is 'Entidade responsável por armazenar as variaveis usadas no calculo da taxa de licenciamento.';
comment on column licenciamento.variavel_calculo_taxa.id is 'Identificador único da entidade.';
comment on column licenciamento.variavel_calculo_taxa.nome is 'Armazena o nome da variavel do calculo do porte';
comment on column licenciamento.variavel_calculo_taxa.sigla is 'Armazena a sigla da variavel do calculo do porte.';

alter table licenciamento.variavel_calculo_taxa owner to postgres;

create table licenciamento.taxa_licenciamento
(
    id serial not null
        constraint pk_taxa_licenciamento
            primary key,
    id_porte_empreendimento integer not null
        constraint fk_tl_porte_empreendimento
            references licenciamento.porte_empreendimento,
    valor varchar(30) not null,
    id_variavel_calculo integer not null
        constraint fk_tl_variavel_calculo
            references licenciamento.variavel_calculo_taxa,
    tipo_estudo varchar(10) not null
);
comment on table licenciamento.taxa_licenciamento is 'Entidade responsável pela taxa calculada para geração do DAE.';
comment on column licenciamento.taxa_licenciamento.id is 'Identificador único da entidade.';
comment on column licenciamento.taxa_licenciamento.id_porte_empreendimento is 'Identificador da entidade porte_empreendimento que faz o relacionamento entre as entidades taxa_licenciamento e porte_empreendimento.';
comment on column licenciamento.taxa_licenciamento.valor is 'Valor da taxa definida pelo governo';

alter table licenciamento.taxa_licenciamento owner to postgres;


create table licenciamento.taxa_publicacao
(
    id serial not null,
    valor numeric
);
comment on table licenciamento.taxa_publicacao is 'Tabela para armazenar o valor atual da Taxa de publicação';
comment on column licenciamento.taxa_publicacao.id is 'Coluna que armazena o id da taxa';
comment on column licenciamento.taxa_publicacao.valor is 'Coluna que armazena o valor da taxa.';

alter table licenciamento.taxa_publicacao owner to postgres;

grant select on licenciamento.taxa_publicacao to licenciamento_ms;

create table licenciamento.tipo_caracterizacao_atividade
(
    id serial not null
        constraint pk_tipo_caracterizacao_atividade
            primary key,
    id_atividade integer
        constraint fk_tca_atividade
            references licenciamento.atividade,
    dispensa_licenciamento boolean not null default false,
    licenciamento_simplificado boolean not null default true,
    licenciamento_declaratorio boolean not null default false
);
comment on table licenciamento.tipo_caracterizacao_atividade is 'Entidade responsável por relacionar atividades do Licenciamento com atividades do CNAE e configurar os tipos de licenciamentos que essas combinações permitem.';
comment on column licenciamento.tipo_caracterizacao_atividade.id is 'Identificador único da entidade.';
comment on column licenciamento.tipo_caracterizacao_atividade.id_atividade is 'Identificador da entidade atividade.';
comment on column licenciamento.tipo_caracterizacao_atividade.dispensa_licenciamento is 'Indica se a combinação atividade e atividade CNAE permite caracterização do tipo Dispensa.';
comment on column licenciamento.tipo_caracterizacao_atividade.licenciamento_simplificado is 'Indica se a combinação atividade e atividade CNAE permite caracterização do tipo Simplificado.';
comment on column licenciamento.tipo_caracterizacao_atividade.licenciamento_declaratorio is 'Indica se a combinação atividade e atividade CNAE permite caracterização do tipo Declaratório.';
alter table licenciamento.tipo_caracterizacao_atividade owner to postgres;



create or replace view licenciamento.licenca_emitida(id_sequencial, id, id_documento, id_caracterizacao, data_cadastro, informacao_adicional, data_validade, numero, tipo_dispensa, ativo) as
    WITH licencas_emitidas AS (
    SELECT dispensa_licenciamento.id,
           dispensa_licenciamento.id_documento,
           dispensa_licenciamento.id_caracterizacao,
           dispensa_licenciamento.data_cadastro,
           dispensa_licenciamento.informacao_adicional,
           NULL::timestamp without time zone AS data_validade,
           dispensa_licenciamento.numero,
           0                                 AS tipo_dispensa,
           dispensa_licenciamento.ativo
    FROM licenciamento.dispensa_licenciamento
    UNION
    SELECT licenca.id,
           licenca.id_documento,
           licenca.id_caracterizacao,
           licenca.data_cadastro,
           NULL::text AS informacao_adicional,
           licenca.data_validade,
           licenca.numero,
           1          AS tipo_dispensa,
           licenca.ativo
    FROM licenciamento.licenca
)
SELECT row_number() OVER () AS id_sequencial,
       licencas_emitidas.id,
       licencas_emitidas.id_documento,
       licencas_emitidas.id_caracterizacao,
       licencas_emitidas.data_cadastro,
       licencas_emitidas.informacao_adicional,
       licencas_emitidas.data_validade,
       licencas_emitidas.numero,
       licencas_emitidas.tipo_dispensa,
       licencas_emitidas.ativo
FROM licencas_emitidas;
comment on view licenciamento.licenca_emitida is 'View responsável por fazer a união das tabelas "licenciamento.dispensa_licenciamento" e "licenciamento.licenca" para que possa ser utilizada na consulta de licenças emitidas.';
comment on column licenciamento.licenca_emitida.tipo_dispensa is 'Indica o tipo de licenca emitida (0 - Dispensa; 1-Simplificado);';

alter table licenciamento.licenca_emitida owner to postgres;

grant select on licenciamento.licenca_emitida to licenciamento_ms;

create or replace function f_gerar_numero_processo() returns trigger
    language plpgsql
as $$
DECLARE
    v_ultimo_ano INTEGER;

BEGIN
    SELECT extract(YEAR FROM (data_cadastro))::INTEGER INTO v_ultimo_ano
    FROM licenciamento.caracterizacao ORDER BY 1 DESC LIMIT 1;

    IF v_ultimo_ano <> (SELECT extract(YEAR FROM (current_date))::INTEGER) THEN

        PERFORM setval('licenciamento.numero_processo_seq', 1, FALSE);

    END IF;

    IF NEW.numero IS NULL OR NEW.numero = '' THEN

        NEW.numero_processo_automatico =
                CONCAT(lpad(cast(nextval('licenciamento.numero_processo_seq'::REGCLASS) AS VARCHAR), 7, '0'), '/' , extract(YEAR FROM (current_date)));

    END IF;

    RETURN NEW;

END;
$$;

alter function f_gerar_numero_processo() owner to postgres;

create trigger t_gerar_numero_processo
    before insert
    on licenciamento.caracterizacao
    for each row
    execute procedure f_gerar_numero_processo();

grant execute on function f_gerar_numero_processo() to licenciamento_ms;

-- criando tabelas do schema correios

create table correios.localidade
(
    id              integer not null,
    cod_ibge        integer,
    tipo_localidade char    not null,
    cep             numeric(8),
    id_pai          integer,
    nome            varchar(50),
    constraint pk_localidade
        primary key (id),
    constraint fk_l_localidade
        foreign key (id_pai) references correios.localidade,
    constraint fk_l_municipio
        foreign key (cod_ibge) references licenciamento.municipio
);

comment on table correios.localidade is 'Entidade responsável por armazenar a relação completa dos nomes das localidades brasileiras.';

comment on column correios.localidade.id is 'Identificador único da entidade localidade.';

comment on column correios.localidade.cod_ibge is 'Código IBGE do município. Obs: caso o cod_ibge for null, siginifica que município pode ser um distrito ou Povoado.';

comment on column correios.localidade.tipo_localidade is 'Tipo da localidade. (d - distrito, m - município, p - povoado, r - região e a - administrativa).';

comment on column correios.localidade.cep is 'CEP da localidade. O valor do CEP será vazio caso a localidade possua CEP por logradouros. Caso seja null, a localidade possui mais de um CEP.';

comment on column correios.localidade.id_pai is 'Identificador que faz o auto relacionamento da entidade localidade. Se caso o município for Distrito ou Povoado o campo será preenchido.';

comment on column correios.localidade.nome is 'Nome da localidade.';

alter table correios.localidade
    owner to postgres;

create table correios.bairro
(
    id            integer      not null,
    nome          varchar(200) not null,
    id_localidade integer      not null,
    constraint pk_bairro
        primary key (id),
    constraint fk_b_localidade
        foreign key (id_localidade) references correios.localidade
);

comment on table correios.bairro is 'Entidade responsável por armazenar a relação completa dos nomes dos Bairros de todas as Localidades de cada Unidade da Federação.';

comment on column correios.bairro.id is 'Identificador único da entidade bairro.';

comment on column correios.bairro.nome is 'Nome do bairro.';

comment on column correios.bairro.id_localidade is 'Identificador da entidade Localidade que realizará o relacionamento entre as entidades Localidade e Bairro.';

alter table correios.bairro
    owner to postgres;

create table correios.bairro_faixa
(
    id         integer    not null,
    cep_inicio numeric(8) not null,
    cep_fim    numeric(8) not null,
    constraint pk_bairro_faixa
        primary key (id, cep_inicio),
    constraint fk_bf_bairro
        foreign key (id) references correios.bairro
);

comment on table correios.bairro_faixa is 'Entidade responsável por armazenar os dados relativos às faixas de cep dos bairros das localidades codificadas por logradouros.';

comment on column correios.bairro_faixa.id is 'Identificador único da entidade bairro_faixa. Sendo também identificador único da entidade bairro.';

comment on column correios.bairro_faixa.cep_inicio is 'Identificador único da entidade bairro e cep inicial da faixa do bairro.';

comment on column correios.bairro_faixa.cep_fim is 'Cep final da faixa do bairro.';

alter table correios.bairro_faixa
    owner to postgres;

create table correios.localidade_faixa
(
    id         integer    not null,
    cep_inicio numeric(8) not null,
    cep_fim    numeric(8) not null,
    constraint pk_localidade_faixa
        primary key (id, cep_inicio),
    constraint fk_fl_localidade
        foreign key (id) references correios.localidade
);

comment on table correios.localidade_faixa is 'Entidade responsável por armazenar dados relativos às faixas de CEP das Localidades codificadas por logradouros.  Obs: Contém apenas as cidades que possuem um único CEP e os CEP''''S armazenados não são encontrados em outras entidades pertencentes a esse grupo.';

comment on column correios.localidade_faixa.id is 'Identificador único das entidades localidade_faixa. Sendo também identificado único da entidade localidade.';

comment on column correios.localidade_faixa.cep_inicio is 'Identificador único da entidade localidade_faixa e CEP Inicial da Faixa da Localidade Codificada.';

comment on column correios.localidade_faixa.cep_fim is 'CEP Final da Faixa da Localidade Codificada.';

alter table correios.localidade_faixa
    owner to postgres;

create table correios.logradouro
(
    id            integer      not null,
    nome          varchar(500) not null,
    cep           numeric(8)   not null,
    id_bairro     integer      not null,
    id_localidade integer      not null,
    constraint pk_logradouro
        primary key (id),
    constraint fk_l_bairro
        foreign key (id_bairro) references correios.bairro,
    constraint fk_l_localidade
        foreign key (id_localidade) references correios.localidade
);

comment on table correios.logradouro is 'Entidade responsável por armazenar a relação dos nomes dos Logradouros de cada Unidade da Federação e seus respectivos códigos postais (CEPs).';

comment on column correios.logradouro.id is 'Identificador único da entidade logradouro.';

comment on column correios.logradouro.nome is 'Nome do logradouro.';

comment on column correios.logradouro.cep is 'CEP do logradouro.';

comment on column correios.logradouro.id_bairro is 'Identificador da entidade Bairro que realizará o relacionamento entre as entidades Bairro e Logradouro.';

comment on column correios.logradouro.id_localidade is 'Identificador da entidade Localidade que realizará o relacionamento entre as entidades  Localidade e Logradouro.';

alter table correios.logradouro
    owner to postgres;

create table correios.grande_usuario
(
    id            integer    not null,
    cep           numeric(8) not null,
    id_logradouro integer,
    constraint pk_grande_usuario
        primary key (id),
    constraint fk_gu_logradouro
        foreign key (id_logradouro) references correios.logradouro
);

comment on table correios.grande_usuario is 'Entidade responsável por armazenar a relação de Nomes Oficiais de Grandes Usuários dos Correios, seus endereços e respectivos CEPs.';

comment on column correios.grande_usuario.id is 'Identificador único da entidade grande_usuário.';

comment on column correios.grande_usuario.cep is 'Cep do grande usuário.';

comment on column correios.grande_usuario.id_logradouro is 'Identificador da entidade Logradouro que realizará o relacionamento entre as entidades Logradouro e Grande_Usuario.';

alter table correios.grande_usuario
    owner to postgres;

create table correios.uf_faixa
(
    id         integer    not null,
    cod_estado varchar(2) not null,
    cep_inicio numeric(8) not null,
    cep_fim    numeric(8) not null,
    constraint pk_uf_faixa
        primary key (id, cep_inicio),
    constraint fk_fuf_uf
        foreign key (cod_estado) references licenciamento.estado
);

comment on table correios.uf_faixa is 'Entidade responsável por armazenar os dados relativos às faixas de CEP destinadas a cada Unidade da Federação.';

comment on column correios.uf_faixa.id is 'Identificador único das entidades faixa_uf. Sendo também identificador único da entidade uf.';

comment on column correios.uf_faixa.cod_estado is 'identificador único do estado e fk para a entidade uf.';

comment on column correios.uf_faixa.cep_inicio is 'Identificador único da entidade uf_faixa e CEP inicial da faixa UF.';

comment on column correios.uf_faixa.cep_fim is 'CEP final da faixa UF.';

alter table correios.uf_faixa
    owner to postgres;

create table correios.unidade_operacional
(
    id            integer    not null,
    cep           numeric(8) not null,
    id_logradouro integer,
    constraint pk_unidade_operacional
        primary key (id),
    constraint fk_uo_logradouro
        foreign key (id_logradouro) references correios.logradouro
);

comment on table correios.unidade_operacional is 'entidade responsável por armazenar a relação dos nomes das unidades operacionais dos correios, seus endereços e respectivos códigos de endereçamento postais (ceps).';

comment on column correios.unidade_operacional.id is 'identificador único da entidade unidade_operacional.';

comment on column correios.unidade_operacional.cep is 'cep da unidade operacional.';

comment on column correios.unidade_operacional.id_logradouro is 'fk para a entidade logradouro.';

alter table correios.unidade_operacional
    owner to postgres;


# --- !Downs

DROP SCHEMA licenciamento CASCADE;
