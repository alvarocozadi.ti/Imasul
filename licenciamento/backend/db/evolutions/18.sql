# --- !Ups

-- AdaptANDo atividades rurais para rurais/urbano
INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '2381'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '2491'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '2501'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '3271'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '5171'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '5182'), 2);

INSERT INTO licenciamento.rel_atividade_tipo_atividade VALUES 
((SELECT id FROM licenciamento.atividade WHERE codigo = '5183'), 2);


# --- !Downs

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '2381') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '2491') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '2501') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '3271') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '5171') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '5182') AND id_tipo_atividade = 2;

DELETE FROM licenciamento.rel_atividade_tipo_atividade WHERE 
id_atividade = (SELECT id FROM licenciamento.atividade WHERE codigo = '5183') AND id_tipo_atividade = 2;