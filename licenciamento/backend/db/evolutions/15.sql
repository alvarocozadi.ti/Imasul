# --!Ups

--IMPORTANTE!!!
--Pegar o resultado desse select e rodar no ambiente em que o mesmo foi executado para gerar os inserts
-- copiar o resultado do select e rodar o insert manualmente
SELECT
'INSERT INTO licenciamento.tipo_caracterizacao_atividade (id_atividade, dispensa_licenciamento, licenciamento_simplificado, licenciamento_declaratorio) VALUES ('||id||', FALSE, TRUE, FALSE);'
FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151');

UPDATE licenciamento.tipo_licenca SET ativo = true WHERE sigla IN ('AA', 'RAA');

UPDATE licenciamento.tipo_licenca SET nome = 'RAA - Renovação de Autorização Ambiental' WHERE sigla = 'RAA';


# ---!Downs

UPDATE licenciamento.tipo_licenca SET nome = 'RAA - Renovação de Licença de Instalação e Operação' WHERE sigla = 'RAA';

UPDATE licenciamento.tipo_licenca SET ativo = false WHERE sigla IN ('AA', 'RAA');

DELETE FROM licenciamento.tipo_caracterizacao_atividade WHERE id_atividade IN 
(SELECT id FROM licenciamento.atividade WHERE codigo IN ('2561','2562','4121','7311','61151'));
