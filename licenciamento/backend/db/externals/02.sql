# --- !Ups

ALTER TABLE licenciamento.licenca
	ADD CONSTRAINT fk_l_licenca_analise FOREIGN KEY (id_licenca_analise) 
	REFERENCES analise.licenca_analise(id);

# --- !Downs

ALTER TABLE licenciamento.licenca
	DROP CONSTRAINT fk_l_licenca_analis; 
