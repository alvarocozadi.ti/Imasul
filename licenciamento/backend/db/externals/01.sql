# --- !Ups

INSERT INTO portal_seguranca.permissao(codigo, data_cadastro, nome, id_modulo)
VALUES ('ALTERAR_EMPREENDIMENTO', now(),'Alterar Empreendimento', 2);

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
VALUES (2,(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ALTERAR_EMPREENDIMENTO'));

# --- !Downs

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil=2 AND id_permissao=(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ALTERAR_EMPREENDIMENTO');

DELETE FROM portal_seguranca.permissao WHERE codigo='ALTERAR_EMPREENDIMENTO';
