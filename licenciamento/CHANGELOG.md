## [v1.0]
### [v1.0]
_________________________________________________________
#  SPRINT 3: (27/04/2020 - 01/09/2020)                    #
#  SQUAD: Lucas, Felipe, Gil, Gustavo e Rodrigo           #
# _______________________________________________________ #

## OSxx

#  OS  |  US   | Descrição da história de usuário                                                                                      | Pontos   | Dev                 #
* OSxx | US5   | [CadastroUnificado-MS] Alterar URL e credencias da sincronia com a Rede Simples                                       |  3pts    | Lucas               #
* OSxx | US6   | [Licenciamento-MS] Criar tela inicial do sistema (Empreendedor)                                                       |  8pts    | Lucas               #
* OSxx | US7   | [Licenciamento-MS] Adaptar a visualização para o empreendedor                                                         |  5pts    | Lucas               #
* OSxx | US8   | [Licenciamento-MS] Criar ação de exclusão de empreendedor                                                             |  8pts    | Felipe              #
* OSxx | US9   | [Licenciamento-MS] Criar ação listar empreendimentos no grid de empreendedores                                        |  8pts    | Lucas               #
* OSxx | US10  | [Licenciamento-MS] Adaptar botão de novo emprrendedor/empreendimento                                                  |  3pts    | Lucas               #
* OSxx | US11  | [Licenciamento-MS] Adaptar filtro de listagem de empreendimentos                                                      |  3pts    | Lucas               #
* OSxx | US36  | [Licenciamento-MS] Remover seleção de DI da solicitação															   |  2pts	  | Rodrigo				#
* OSxx | US44  | [Licenciamento-MS] Remover campo de vigência da licença na solicitação												   |  3pts	  | Rodrigo				#
* OSxx | US35  | [Licenciamento-MS] Alterar regra de visualização dos empreendedores/empreendimentos cadastrados		               |  8pts    | Felipe	         	#
* OSxx | US86  | [Licenciamento-MS] Implementação do deploy automático em teste                                                        |  -pts    |	Kellyson            #
* OSxx | US68  | [Licenciamento-MS] Permitir o cadastro apenas de empreendimentos vindo da junta comercial				  			   |  -pts	  | Rodrigo				#
* OSxx | US83  | [Licenciamento-MS] Adaptar tela de login do Licenciamento 															   |  -pts    | Rodrigo				#
* OSxx | US78  | [Licenciamento-MS] Refatorar código para implementar regra de mais de um empreendimento com o mesmo CPF/CNPJ	       |  X       | Lucas	         	#
* OSxx | US81  | [Licenciamento-MS] Alterar regra para permitir a seleção de várias atividades na solicitação			               |  X       | Felipe		        #
* OSxx | US80  | [Licenciamento-MS] Implementar salvamento por etapa no cadastro de solicitação                                        |  X       | Lucas	         	#
* OSxx | US122 | [Licenciamento-MS] Implementar cálculo da taxa da licença para antes do Análise									   |  X		  | Rodrigo				#
* OSxx | US125 | [Licenciamento-MS] Incluir o tipo de estudo no cálculo das taxas													   |  X		  | Rodrigo				#