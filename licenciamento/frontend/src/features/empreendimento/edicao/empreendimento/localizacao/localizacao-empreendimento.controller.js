var LocalizacaoEmpreendimentoControllerEdicao = function($scope, mensagem, imovelService, $anchorScroll, municipioService, $injector) {

	// Criando extensão da controller LocalizacaoEmpreendimentoController
	$injector.invoke(exports.controllers.LocalizacaoEmpreendimentoController, this,
		{
			$scope: $scope,
			mensagem: mensagem,
			imovelService: imovelService,
			$anchorScroll: $anchorScroll,
			municipioService: municipioService
		}
	);

	var localizacaoEmpreendimento = this;

	localizacaoEmpreendimento.getImoveis = getImoveis;
	localizacaoEmpreendimento.getDadosImovel = getDadosImovel;

	function getImoveis(getImoveis) {

		var cpfCnpj = $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf ? $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf : $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cnpj;
		var idMunicipio = $scope.cadastro.empreendimento.municipio.id;
		$scope.cadastro.empreendimento.enderecos[0].tipo = 'ZONA_RURAL';
		$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo = localizacaoEmpreendimento.localizacaoEmpreendimento.RURAL;
		
		imovelService.getImoveisSimplificadosPorCpfCnpj(cpfCnpj, idMunicipio)
			.then(function(response){

				$scope.imoveisPesquisados = true;

				localizacaoEmpreendimento.imoveis = (response.data && response.data.length) ? response.data : null;

				if(getImoveis){
					getImoveis();
				}

			})
			.catch(function(){

				mensagem.warning("Não foi possível obter os dados dos imóveis no CAR.");

			});

	}

	function getDadosImovel(codigoImovel) {

		if(codigoImovel === undefined) {
			return;
		}

		imovelService.getImoveisCompletoByCodigo(codigoImovel)
			.then(function(response){

				localizacaoEmpreendimento.imovelSelecionado = response.data;

			})
			.catch(function(err){

				mensagem.error("Não foi possível obter os dados do imóvel no CAR.");

			});

	}

	function initLocalizacao() {

		if ($scope.cadastro.empreendimento.localizacao === 'ZONA_URBANA') {

			localizacaoEmpreendimento.getGeometriaMunicipio();

		} else if ($scope.cadastro.empreendimento.localizacao === 'ZONA_RURAL') {

			localizacaoEmpreendimento.getImoveis(function(){

				if (!$scope.cadastro.empreendimento.imovel){

					return;
				}

				if($scope.cadastro.empreendimento && $scope.formLocalizacaoEmpreendimento) {

					let enderecos = $scope.cadastro.empreendimento.enderecos;

					_.forEach(enderecos, (endereco) => {

						if(endereco.descricaoAcesso) {
							setDescricaoAcesso(endereco.descricaoAcesso);
						}

					});

				}

				_.forEach(localizacaoEmpreendimento.imoveis, function(imovel){

					if (imovel.codigo === $scope.cadastro.empreendimento.imovel.codigo) {

						$scope.cadastro.empreendimento.imovel = imovel;

						localizacaoEmpreendimento.getDadosImovel($scope.cadastro.empreendimento.imovel.codigo);
						return false;

					}
				});
			});
		}
	}


	function setDescricaoAcesso(descricao){

		if(!$scope.formLocalizacaoEmpreendimento){
			return;
		}

		$scope.formLocalizacaoEmpreendimento.roteiro.$viewValue = descricao;
		$scope.formLocalizacaoEmpreendimento.roteiro.$render();

	}

	initLocalizacao();
};

exports.controllers.LocalizacaoEmpreendimentoControllerEdicao = LocalizacaoEmpreendimentoControllerEdicao;
