var LocalizacaoEmpreendimentoController = function($scope, $rootScope, mensagem, imovelService, municipioService) {

	var localizacaoEmpreendimento = this;

	localizacaoEmpreendimento.abaValida = abaValida;
	localizacaoEmpreendimento.anterior = anterior;
	localizacaoEmpreendimento.proximo = proximo;
	localizacaoEmpreendimento.getImoveis = getImoveis;
	localizacaoEmpreendimento.getDadosImovel = getDadosImovel;
	localizacaoEmpreendimento.getGeometriaMunicipio = getGeometriaMunicipio;
	localizacaoEmpreendimento.formatarDados = formatarDados;
	localizacaoEmpreendimento.getDescricaoCAR = getDescricaoCAR;
	localizacaoEmpreendimento.updateDescricaoAcesso = updateDescricaoAcesso;
	localizacaoEmpreendimento.imoveis = [];
	localizacaoEmpreendimento.localizacaoEmpreendimento = app.LOCALIZACOES_EMPREENDIMENTO;
	localizacaoEmpreendimento.tipoGeometria = {
		polygon: true,
		circle: false,
		rectangle: false,
		polyline: false,
		marker: false
	};

	localizacaoEmpreendimento.errors= {
		roteiroAcesso: false
	};

	$scope.imoveisPesquisados = false;
	$scope.cadastro.empreendimento.imovel = {};

	if(!$scope.cadastro.etapas.EMPREENDIMENTO.abas.LOCALIZACAO.abaValida){
		$scope.cadastro.etapas.EMPREENDIMENTO.abas.LOCALIZACAO.abaValida = abaValida;
	}

	function abaValida() {

		if ($scope.cadastro.enderecoEmpreendimento.principal !== undefined) {

			if ($scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao && 
				$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo === localizacaoEmpreendimento.localizacaoEmpreendimento.RURAL) {

				return $scope.formLocalizacaoEmpreendimento.$valid &&
					$scope.cadastro.empreendimento.coordenadas && $scope.formLocalizacaoEmpreendimento.roteiro.$viewValue;
	
			} else {
	
				return $scope.formLocalizacaoEmpreendimento && $scope.formLocalizacaoEmpreendimento.$valid && $scope.cadastro.empreendimento.coordenadas;
			}
		}

	}

	function anterior() {

		$scope.cadastro.etapas.EMPREENDIMENTO.tabIndex--;
	}

	function formatarDados() {

		if ($scope.cadastro.empreendimento.proprietarios === null || $scope.cadastro.empreendimento.proprietarios === undefined ){
			var listaProprietarios = [];

			_.forEach( $scope.cadastro.empreendimento.empreendimentoEU.proprietarios, function(proprietario){

				var proprietarioPessoa = {
					pessoa:null
				};

				proprietarioPessoa.pessoa = proprietario;
				listaProprietarios.push(proprietarioPessoa);

			});
			$scope.cadastro.empreendimento.proprietarios = listaProprietarios;
		}

		if ( $scope.cadastro.empreendimento.representantesLegais === null || $scope.cadastro.empreendimento.representantesLegais === undefined ){
			var listaRepresentantesLegais = [];

			_.forEach( $scope.cadastro.empreendimento.empreendimentoEU.representantesLegais, function(representanteLegal){

				var representanteLegalPessoa = {
					pessoa:null,
					dataVinculacao:null,
				};

				representanteLegalPessoa.pessoa = representanteLegal;
				representanteLegalPessoa.dataVinculacao = representanteLegal.dataCadastro;
				listaRepresentantesLegais.push(representanteLegalPessoa);

			});
			$scope.cadastro.empreendimento.representantesLegais = listaRepresentantesLegais;
		}

		if ($scope.cadastro.empreendimento.responsaveis === null || $scope.cadastro.empreendimento.responsaveis === undefined ){

			$scope.cadastro.empreendimento.responsaveis = [];

			_.forEach( $scope.cadastro.empreendimento.empreendimentoEU.responsaveisTecnicos, function(responsavelTecnico){

				var responsavelTecnicoPessoa = {
					pessoa:null,
					tipo: 'TECNICO'
				};

				responsavelTecnicoPessoa.pessoa = responsavelTecnico;
				$scope.cadastro.empreendimento.responsaveis.push(responsavelTecnicoPessoa);

			});

			_.forEach( $scope.cadastro.empreendimento.empreendimentoEU.responsaveisLegais, function(responsavelLegal){

				var responsavelLegalPessoa = {
					pessoa:null,
					tipo:'LEGAL',
				};

				responsavelLegalPessoa.pessoa = responsavelLegal;
				$scope.cadastro.empreendimento.responsaveis.push(responsavelLegalPessoa);

			});
		}
	}

	function proximo() {

		$scope.formLocalizacaoEmpreendimento.$setSubmitted();

		//$scope.cadastro.empreendimento.localizacao = 'ZONA_URBANA';

		var cpfCnpj = $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf ? $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf : $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cnpj;
		var denominacao = $scope.cadastro.empreendimento.empreendimentoEU.denominacao;

		$scope.cadastro.enderecoEmpreendimento.principal.descricaoAcesso = $scope.formLocalizacaoEmpreendimento.roteiro.$viewValue;

		localizacaoEmpreendimento.errors.roteiroAcesso = $scope.formLocalizacaoEmpreendimento.roteiro.$viewValue === undefined ? true : false;

		if (localizacaoEmpreendimento.abaValida()) {

			localizacaoEmpreendimento.formatarDados();

			$scope.cadastro.proximo();

		} else {

			if(!$scope.cadastro.empreendimento.coordenadas && $scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo === localizacaoEmpreendimento.localizacaoEmpreendimento.URBANA ) {

				mensagem.warning('É preciso adicionar o perímetro do empreendimento dentro dos limites mostrados no mapa');

			} else if($scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo === localizacaoEmpreendimento.localizacaoEmpreendimento.RURAL && $scope.imoveisPesquisados && !localizacaoEmpreendimento.imoveis) {
				
				$scope.imoveisPesquisados = true;
				
				imovelService.enviarEmail($scope.cadastro.empreendimento.id, denominacao)

					.then(function(response){
						$scope.cadastro.emailEnviado = response.data.emailEnviado;
						mensagem.warning('Providencie o Cadastro Ambiental Rural para prosseguir com o cadastro do Empreendimento. Foi comunicado ao setor responsável no IMASUL para análise do cadastro vinculado ao Empreendimento.', {ttl: 15000});
					});

			}else{

				mensagem.warning('Verifique os campos destacados em vermelho para prosseguir com o cadastro.');
			}

		}

	}

	function getImoveis() {

		resetLocalizacao();

		var cpfCnpj = $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf ? $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cpf : $scope.cadastro.empreendimento.empreendimentoEU.pessoa.cnpj;
		var idMunicipio = $scope.cadastro.empreendimento.municipio.id;

		let rural = 'ZONA_RURAL';

		$scope.cadastro.empreendimento.localizacao = rural;
		$scope.cadastro.empreendimento.enderecos[0].tipo = rural;

		if (!$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao) {

			$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao = { codigo: null };

		}

		$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo = localizacaoEmpreendimento.localizacaoEmpreendimento.RURAL;

		localizacaoEmpreendimento.estiloGeo = app.Geo.overlays.AREA_IMOVEL.style;

		imovelService.getImoveisSimplificadosPorCpfCnpj(cpfCnpj, idMunicipio)
			.then(function(response){

				$scope.imoveisPesquisados = true;

				localizacaoEmpreendimento.imoveis = (response.data && response.data.length) ? response.data : null;

				localizacaoEmpreendimento.controleAtualizacao = false;

			})
			.catch(function(){

				mensagem.warning("Não foi possível obter os dados dos imóveis no CAR.");

			});
	}

	function getDadosImovel(codigoImovel) {

		if(!codigoImovel) {
			return;
		}

		imovelService.getImoveisCompletoByCodigo(codigoImovel)
			.then(function(response){

				localizacaoEmpreendimento.imovelSelecionado = response.data;
				$scope.cadastro.empreendimento.imovel = response.data;

			})
			.catch(function(err){

				mensagem.error("Não foi possível obter os dados do imóvel no CAR.");

			});
	}

	function getGeometriaMunicipio(idMunicipio) {

		if(idMunicipio){
			resetLocalizacao();
		}

		let urbana = 'ZONA_URBANA';

		$scope.cadastro.empreendimento.localizacao = urbana;
		$scope.cadastro.empreendimento.enderecos[0].tipo = urbana;

		if (!$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao) {
			
			$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao = { codigo: null };

		}

		$scope.cadastro.enderecoEmpreendimento.principal.zonaLocalizacao.codigo = localizacaoEmpreendimento.localizacaoEmpreendimento.URBANA;
		
		localizacaoEmpreendimento.estiloGeo = app.Geo.overlays.AREA_MUNICIPIO.style;

		var municipio = idMunicipio !== null && idMunicipio !== undefined ? idMunicipio : $scope.cadastro.empreendimento.municipio.id;

		municipioService.getMunicipioGeometryById(municipio)
			.then(function(response) {

				localizacaoEmpreendimento.municipio = response.data;

				localizacaoEmpreendimento.controleAtualizacao = false;

			})
			.catch(function() {
				mensagem.error("Não foi possível obter o limite do município.");
			});

	}

	function resetLocalizacao() {

		$scope.imoveisPesquisados = false;
		localizacaoEmpreendimento.imoveis = null;
		localizacaoEmpreendimento.imovelSelecionado = null;
		$scope.cadastro.empreendimento.coordenadas = undefined;

		setDescricaoAcesso(null);

	}

	function initLocalizacao() {

		let localizacao = $scope.cadastro.empreendimento.localizacao;
		let rural = localizacao === 'ZONA_RURAL' || localizacao === app.LOCALIZACOES_EMPREENDIMENTO.RURAL;

		if(rural) {
			getImoveis();
		} else {
			getGeometriaMunicipio();
		}

	}

	function setDescricaoAcesso(descricao){

		if(!$scope.formLocalizacaoEmpreendimento){
			return;
		}

		$scope.formLocalizacaoEmpreendimento.roteiro.$viewValue = descricao;
		$scope.formLocalizacaoEmpreendimento.roteiro.$render();
		updateDescricaoAcesso();

	}

	function getDescricaoCAR(imovel) {

		return `${imovel.codigo} - ${imovel.nome} - ${imovel.municipio.nome} / ${imovel.municipio.estado.codigo}`;

	}

	function updateDescricaoAcesso() {

		$rootScope.descricaoAcesso = $scope.formLocalizacaoEmpreendimento.roteiro.$viewValue;

	}

	$scope.$watch('cadastro.etapas.EMPREENDIMENTO.tabIndex', function(tabIndex){

		if($scope.$parent.alterouMunicipioEmpreendimento && tabIndex && 
			tabIndex === $scope.cadastro.etapas.EMPREENDIMENTO.abas.LOCALIZACAO.indice){
			
			$scope.cadastro.empreendimento.coordenadas = null;

			initLocalizacao();

			$scope.$parent.alterouMunicipioEmpreendimento = false;
		}

	});

	$scope.$on('resetLocationStep', function (event, data) {

		resetLocalizacao();

		$scope.cadastro.etapas.EMPREENDIMENTO.tabIndex = $scope.cadastro.etapas.EMPREENDIMENTO.abas.DADOS.indice;

	});

};

exports.controllers.LocalizacaoEmpreendimentoController = LocalizacaoEmpreendimentoController;
