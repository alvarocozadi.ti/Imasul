var ListagemEmpreendedores = function($scope, $rootScope, mensagem, $location, breadcrumb, modalSimplesService, empreendedorService, empreendimentoService, config) {

	var listagem = this;

	listagem.pesquisar = pesquisar;
	listagem.onPaginaAlterada = onPaginaAlterada;
	listagem.buscarEmpreendedor = buscarEmpreendedor;
	listagem.limparPesquisa = limparPesquisa;
	listagem.removerEmpreendedor = removerEmpreendedor;
	listagem.confirmarRemoverEmpreendedor = confirmarRemoverEmpreendedor;
	listagem.empreendimentos = [];
	listagem.campoPesquisa = "";
	listagem.empreendedorVisualizar = {};
	listagem.listarEmpreendimentos = listarEmpreendimentos;
	listagem.novoEmpreendedorEmpreendimento = novoEmpreendedorEmpreendimento;
	listagem.tipoEndereco = app.TIPO_ENDERECO;

	listagem.paginacao = new app.utils.Paginacao(config.QTDE_ITENS_POR_PAGINA);

	function onPaginaAlterada() {

		pesquisar(null);
	}

	function atualizarLista(response) {

		listagem.empreendedores = response.data.pageItems;
		listagem.paginacao.update(response.data.totalResults, listagem.paginacao.paginaAtual);
	}

	function pesquisar(pagina, textoMensagem) {

		var pesquisa = listagem.campoPesquisa;

		if(pagina) {
			listagem.paginacao.paginaAtual = pagina;
		}

		if (pesquisa.isPartOfCpfCnpj()) {

			pesquisa = pesquisa.deixarSomenteNumeros();
		}

		empreendedorService.list(
			pesquisa,
			listagem.paginacao.paginaAtual,
			listagem.paginacao.itensPorPagina
		).then(function(response) {
			atualizarLista(response);
			if(textoMensagem) {
				mensagem.success(textoMensagem);
			}
		})
		.catch(function(){
			mensagem.error("Ocorreu um erro ao buscar a lista de empreendimentos.");
		});
	}

	function limparPesquisa() {

		listagem.campoPesquisa = '';

		empreendimentoService.list(
			'',
			1,
			listagem.paginacao.itensPorPagina
		).then(atualizarLista)
		.catch(function(){
			mensagem.error("Ocorreu um erro ao buscar a lista de empreendimentos.");
		});

	}

	// listagem.caracterizacoes = function (idEmpreendimento) {
	// 	$location.path('/empreendimento/'+ idEmpreendimento +'/caracterizacoes');
	// };

	// function alterarEmpreendimento(id) {

	// 	$location.path("/empreendimentos/editar/"+id);
	// }

	function removerEmpreendedor(cpfCnpj, idEmpreendedor) {

		empreendedorService.buscarEmpreendimento(cpfCnpj).then(function(response){
			var empreendimentos = response.data;
			var podeSerExcluido = true;
			var listaIds = [];

			empreendimentos.forEach(empreendimento => {

				listaIds = listaIds.concat(empreendimento.id);

				if(empreendimento.possuiCaracterizacoes === true){
					podeSerExcluido = false;
				}
			});
			if (podeSerExcluido) {

				var configModal = {
					titulo: 'Confirmar exclusão do empreendedor',
					conteudo: 'Tem certeza que deseja excluir esse empreendedor? Ao selecionar a opção "Sim", todos os empreendimentos vinculados também serão excluídos.'
				};
	
				var instanciaModal = modalSimplesService.abrirModal(configModal);
	
				instanciaModal.result
					.then(function(){
						listagem.confirmarRemoverEmpreendedor(listaIds, idEmpreendedor);
					});
			}else{

				mensagem.error("Não foi possível excluir o empreendedor pois o mesmo possui empreendimentos com solicitações vinculadas");
			}
		});
	}

	function confirmarRemoverEmpreendedor(listaIds, idEmpreendedor) {

		if(listaIds.length > 0) {
			empreendimentoService.deleteEmpreendimentos(listaIds)
				.then(function(response){
				
					empreendedorService.delete(idEmpreendedor)
						.then(function(response){

							listagem.pesquisar(null, response.data.texto);
						})
						.catch(function(response){

							mensagem.error(response.data.texto);
						});

					listagem.pesquisar(null, response.data.texto);
				})
				.catch(function(response){

					mensagem.error(response.data.texto);
				});
		}else {
			
			empreendedorService.delete(idEmpreendedor)
				.then(function(response){

					listagem.pesquisar(null, response.data.texto);
				})
				.catch(function(response){
					
					mensagem.error(response.data.texto);
				});
		}
	}

	function buscarEmpreendedor(cpfCnpj) {

		empreendedorService.buscarEmpreendedorCompleto(cpfCnpj)
			.then(function(response) {

				listagem.empreendedorVisualizar = response.data;
				listagem.empreendedorVisualizar.exibir = true;
				listagem.empreendedorVisualizar.isEmpreendedor = true;
				$('#modalVisualizar').modal("show");

				$('#modalVisualizar').one('hide.bs.modal', function (e) {

					listagem.empreendedorVisualizar = {};
					$scope.$broadcast('destroyMap');

				});

			})
			.catch(function() {

				mensagem.error('Ocorreu um erro ao buscar as informações do o empreendedor.');

			});

	}

	function novoEmpreendedorEmpreendimento() {

		$rootScope.hasEmpreendedor = false;

		$location.path("/empreendimentos/cadastrar?origem=outro");
	}

	function listarEmpreendimentos(cpfCnpj) {

		$rootScope.hasEmpreendedor = true;

		$rootScope.cpfCnpjEmpreendedor = cpfCnpj;

		$location.path("/empreendimentos/" + cpfCnpj + "/listagem/");
	}

	this.pesquisar(null);

	breadcrumb.set([{title:'Empreendedor', href:''}]);

};

exports.controllers.ListagemEmpreendedores = ListagemEmpreendedores;
