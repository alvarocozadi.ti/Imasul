var CaracterizacaoLSACondicoesController = function($scope, 
													$rootScope, 
													mensagem, 
													caracterizacaoService, 
													outorgaService,
													$q) {

	var etapaCondicoes = this;

	etapaCondicoes.passoValido = passoValido;
	etapaCondicoes.proximo = proximo;
	etapaCondicoes.validarPerguntasForaQuestionario = validarPerguntasForaQuestionario;
	etapaCondicoes.respostas = {};
	etapaCondicoes.questionario = $scope.cadastro.caracterizacao.questionario3 ? $scope.cadastro.caracterizacao.questionario3 : {};
	tratarDadosQuestionario(etapaCondicoes.questionario);
	etapaCondicoes.etapasCadastroCaracterizacao = app.ETAPA_CADASTRO_CARACTERIZACAO;
	$scope.cadastro.etapas.CONDICOES.limparEtapa = limparCondicoes;
	etapaCondicoes.processoOutorga = "";
	etapaCondicoes.listaProcessosOutorga = [];

	etapaCondicoes.restricoesMZEE = null;
	etapaCondicoes.restricoesCAR = null;

	etapaCondicoes.nomesAreasSobrepostas = {
		TERRA_INDIGENA: "Terra indígena",
		UNIDADE_CONSERVACAO: "Unidade de conservação",
		AREA_MILITAR: "Área militar",
		ZONA_CONSOLIDACAO: "Zona de consolidação",
		AREA_QUILOMBOLA: "Área quilombola",
		AREA_CONSOLIDADA: "Área consolidada",
		ARL_PROPOSTA: "Reserva legal proposta",
		ARL_AVERBADA: "Reserva legal averbada",
		ARL_APROVADA_NAO_AVERBADA: "Reserva legal aprovada e não averbada"
	};

	$scope.cadastro.etapas.CONDICOES.passoValido = passoValido;
	$scope.cadastro.etapas.CONDICOES.beforeEscolherEtapa = beforeEscolherEtapa;
	etapaCondicoes.validadorPerguntas = new app.ValidadorPerguntasQuestionario3();

	$scope.hideVoltar = false;

	$scope.$on('atividadeAlterada', function(event, args) {
		etapaCondicoes.respostas = {};
		etapaCondicoes.questionario = {};
	});

	etapaCondicoes.$onInit = function() {
		
		etapaCondicoes.processoOutorga = $scope.cadastro.caracterizacao.processoOutorga;
		buscarProcessosOutorga();

	};

	function passoValido() {

		// if($scope.cadastro.continuacao) {
		// 	return true;
		// }

		return todasAsPerguntasForamRespondidas();

	}

	function limparCondicoes(){
		etapaCondicoes.resposta = {};
		etapaCondicoes.questionario = {};
	}

	function beforeEscolherEtapa() {

		var deferred = $q.defer();

		if(etapaCondicoes.passoValido()){

			
			$scope.cadastro.caracterizacao.questionario3 = etapaCondicoes.questionario;

			$scope.cadastro.caracterizacao.respostas = montaObjRespostas();

			salvarCaracterizacao();

			return true;

		}else{
			$scope.formCondicaoQuestionario.$setSubmitted();

			if(!todasAsPerguntasForamRespondidas()) {

				mensagem.warning('Todas as perguntas devem ser respondidas ou suas respostas não permitem prosseguir com a solicitação.');
				deferred.reject();
				return deferred.promise;
			}

			if(etapaCondicoes.tituloOutorgaValido !== undefined && etapaCondicoes.tituloOutorgaValido === false) {

				mensagem.error('O titulo de outorga não foi preenchido ou não foi encontrado ou está vencido. Procure o IMASUL para regularizar a situação da outorga.');
				deferred.reject();
				return deferred.promise;
			}
		}
	}

	function montaObjRespostas() {

		var idRespostas = [];

		_.each(etapaCondicoes.respostas, function(resposta){

			idRespostas.push({
				id: resposta.id
			});
		});

		return idRespostas;

	}

	function proximo() {

		//Validações serão feitas no método beforeEscolherEtapa
		$scope.cadastro.proximo();
	}

	function tratarDadosQuestionario(questionario) {
		if(questionario){			
			questionario.consumoAgua = typeof(questionario.consumoAgua) === "boolean" ? questionario.consumoAgua ? "true" : "false" : null;
			questionario.efluentes = typeof (questionario.efluentes) === "boolean" ? questionario.efluentes ? "true" : "false" : null;
			questionario.residuosSolidos = typeof (questionario.residuosSolidos) === "boolean" ? questionario.residuosSolidos ? "true" : "false" : null;			
		}
	}

	function getValorParametros() {

		var atividadesCaracterizacao = $scope.cadastro.caracterizacao.atividadesCaracterizacao;

		_.forEach(atividadesCaracterizacao, function(ac) {
			_.forEach(ac.atividade.parametros, function(parametro, index) {
				parametro.valorParametro = ac.atividadeCaracterizacaoParametros[index].valorParametro;
			});
		});
	}

	function salvarCaracterizacao() {

		$rootScope.visualizarDocumentacao = true;
		$scope.cadastro.caracterizacao.etapasConcluidas = [];
		$scope.cadastro.caracterizacao.etapasConcluidas.push(etapaCondicoes.etapasCadastroCaracterizacao.ATIVIDADE);
		$scope.cadastro.caracterizacao.etapasConcluidas.push(etapaCondicoes.etapasCadastroCaracterizacao.LOCALIZACAO);
		$scope.cadastro.caracterizacao.etapaRascunho = etapaCondicoes.etapasCadastroCaracterizacao.CONDICOES + 1 ;
		$scope.cadastro.caracterizacao.processoOutorga = etapaCondicoes.processoOutorga;

		caracterizacaoService.saveSimplificado($scope.cadastro.caracterizacao)
			.then(function(response){

				_.extend($scope.cadastro.caracterizacao, response.data);
				// $location.path('/empreendimento/' + $routeParams.idEmpreendimento + '/caracterizacao/' + $scope.cadastro.caracterizacao.id + '/edit');
				mensagem.success('Rascunho salvo com sucesso!' , {ttl: 15000});
				getValorParametros();
				return true;

			})
			.catch(function(response){

				if(response && response.data && response.data.texto) {
					mensagem.error(response.data.texto, {ttl: 15000});
				}
				else {
					mensagem.error('Falha ao salvar rascunho da solicitação.' , {ttl: 15000});
				}
				return false;

			});
	}

	function updateCaracterizacao() {

		// unirGeometriasAtividade();

		// var caracterizacao = JSON.parse(JSON.stringify($scope.cadastro.caracterizacao));

		// caracterizacao.respostas = montaObjRespostas();
		// caracterizacao.porteEmpreendimento = null;

		// _.forEach(caracterizacao.atividadesCaracterizacao, function(atividade) {

		// 	_.forEach(atividade.geometriasAtividade, function(geometria) {

		// 		geometria.area = null;
		// 	});
		// });

		// if($scope.cadastro.renovacao) {
		// 	caracterizacao.tipoLicenca = $scope.licencasEmAndamento;
		// }
		
		caracterizacaoService.updateSimplificado(caracterizacao)
			.then(function(response){
				var caracterizacaoSalva = response.data;
				// Adiciona atributos retornados, como o id, ao objeto 'caracterizacao' atual
				_.extend($scope.cadastro.caracterizacao, caracterizacaoSalva);
				$scope.cadastro.irParaEtapa($scope.cadastro.etapas.DOCUMENTACAO);
				return true;
			})
			.catch(function(response){
				mensagem.error(response.data.texto, {ttl: 15000});
				return false;
			});
	}

	var configModal = {
		titulo: 'Solicitação de licença',
		conteudo: '<span>Os requisitos informados NÃO atendem a Resolução COEMA nº 127 de 16 de Novembro de 2016.</span>' +
				'<br><strong>Seu licenciamento foi classificado como licenciamento ordinário.</strong>',
		conteudoDestaque: 'Dirija-se ao IMASUL para abertura do processo.',
		labelBotaoConfirmar: 'Cancelar solicitação de licença',
		labelBotaoCancelar: 'Fechar'
	};

	function todasAsPerguntasForamRespondidas() {

		return validarPerguntasForaQuestionario() && etapaCondicoes.validadorPerguntas.validar(etapaCondicoes.questionario);
	}

	function validarPerguntasForaQuestionario() {

		var valido = true;

		_($scope.cadastro.caracterizacao.atividadesCaracterizacao).each(function (atividadeCaracterizacao) {
			_(atividadeCaracterizacao.atividade.perguntas).each(function (pergunta) {
				if (valido) {
					if(etapaCondicoes.respostas[pergunta.id]){
						valido = etapaCondicoes.respostas[pergunta.id].permiteLicenciamento;
					} else {
						valido = false;
					}
				}
			});
		});

		return valido;
	}

	function buscarProcessosOutorga() {

		let pessoaEmpreendedora = $scope.empreendimento.empreendimentoEU.empreendedor.pessoa;
		let cpfCnpj = pessoaEmpreendedora.cpf || pessoaEmpreendedora.cnpj;

		outorgaService.getOutorga(cpfCnpj).then(response => {

			etapaCondicoes.listaProcessosOutorga = response.data;

		}).catch(() => {

			mensagem.error('Não foi possível buscar a lista de processos em outorga do empreendedor.');

		});

	}

};

exports.controllers.CaracterizacaoLSACondicoesController = CaracterizacaoLSACondicoesController;
