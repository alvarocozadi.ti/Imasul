var CaracterizacaoLSAGeoController = function($scope, $rootScope, mensagem, caracterizacaoService,empreendimentoService, municipioService,estadoService, imovelService, config, $q) {

	var etapaGeo = this;

	//area para armazenar cálculo de sobreposicao com APP
	var areaAPP = "";

	etapaGeo.passoValido = passoValido;
	etapaGeo.proximo = proximo;
	etapaGeo.initGeo = initGeo;

	$scope.cadastro.etapas.GEO.passoValido = passoValido;
	$scope.cadastro.etapas.GEO.beforeEscolherEtapa = beforeEscolherEtapa;
	etapaGeo.localizacaoEmpreendimento = getGeoLocalizacao();
	etapaGeo.etapasCadastroCaracterizacao = app.ETAPA_CADASTRO_CARACTERIZACAO;
	$scope.cadastro.etapas.GEO.limparEtapa = limparGeo;

	etapaGeo.editarGeo = function editarGeo() {
		if ($scope.cadastro.caracterizacao.notificacao){
			return $scope.cadastro.caracterizacao.notificacao.retificacaoSolicitacaoComGeo;
		}
		return true;
	};

	function limparGeo(){
		_.each($scope.cadastro.caracterizacao.atividadesCaracterizacao, function(atividade) {
			atividade.geometria = null;
			atividade.geometrias = null;
			atividade.geometriasAtividade = null;
		});
	}

	etapaGeo.temAtividadeRural = function (){

		return $rootScope.empreendimento.localizacao === "ZONA_RURAL";
	};

	var areasProibidas = ['APP_TOTAL', 'NASCENTE_OLHO_DAGUA','ARL_TOTAL','AREA_USO_RESTRITO_DECLIVIDADE_25_A_45','AREA_USO_RESTRITO_PANTANEIRA'];
	var geometriasAreasProibidas = {};
	var passoValidoMSG;
	var isAreaVerificada = false;
	var isIntersectionsVerificado = false;
	var hasIntersection = false;
	var imovelValidoParaLicenciamento = false;

	$scope.$on('carregarLocalizacaoEmpreendimento', function() {
		etapaGeo.localizacaoEmpreendimento = getGeoLocalizacao();
	});

	$scope.$on('atividadeAlterada', function(event, args) {
		negarVerificacoes();
	});

	$scope.$on('geometriesUpdated', function(event, args) {
		negarVerificacoes();
	});

	function negarVerificacoes() {
		isAreaVerificada = false;
		isIntersectionsVerificado = false;
		hasIntersection = false;
		imovelValidoParaLicenciamento = false;
	}

	function isEmpreendimentoRural() {
		return $rootScope.empreendimento.localizacao === app.LOCALIZACOES_EMPREENDIMENTO.ZONA_RURAL;
	}

	function getMenssagemSobreposicao(nomeCamada) {
		return 'Atenção! A área desenhada possui sobreposição com área de ' + nomeCamada +
			' do imóvel e por isso a solicitação não pode ser concluída. Por favor, apresente nova área da atividade.';
	}

	function passoValido() {

		if(todasAtividadesTemGeometria() && !isIntersectionsVerificado && isEmpreendimentoRural()){

			let collectionUnnion = $scope.cadastro.caracterizacao.complexo ? getUniaoComplexo() : getUniaoAtividades();

			if(verificaIntercao(collectionUnnion,geometriasAreasProibidas.AREA_USO_RESTRITO_DECLIVIDADE_25_A_45)) {
				passoValidoMSG = getMenssagemSobreposicao('Uso Restrito com declide de 25° até 45°');
				isIntersectionsVerificado = true;
				hasIntersection = true;
				return false;
			}

			if(verificaIntercao(collectionUnnion,geometriasAreasProibidas.AREA_USO_RESTRITO_PANTANEIRA)) {
				passoValidoMSG = getMenssagemSobreposicao('Uso Restrito Pantaneira');
				isIntersectionsVerificado = true;
				hasIntersection = true;
				return false;
			}

			if(verificaIntercao(collectionUnnion,geometriasAreasProibidas.ARL_TOTAL)) {
				passoValidoMSG = getMenssagemSobreposicao('Reserva Legal ');
				isIntersectionsVerificado = true;
				hasIntersection = true;
				return false;
			}

			if(verificaIntercao(collectionUnnion,geometriasAreasProibidas.APP_TOTAL)) {
				passoValidoMSG = getMenssagemSobreposicao('Área de preservação permamente');
				isIntersectionsVerificado = true;
				hasIntersection = true;
				return false;
			}

			if(verificaIntercao(collectionUnnion,geometriasAreasProibidas.NASCENTE_OLHO_DAGUA)) {
				passoValidoMSG = getMenssagemSobreposicao('Nascente ou olho d\'água ');
				isIntersectionsVerificado = true;
				hasIntersection = true;
				return false;
			}

			isIntersectionsVerificado = true;
		}

		return verificaAreaInformadaComDesenho() && !hasIntersection;

	}

	function getUniaoComplexo() {

		reducer = (accumulator, currentValue) => turf.union(accumulator, currentValue);

		return $scope.cadastro.caracterizacao.geometriasComplexo ?
			$scope.cadastro.caracterizacao.geometriasComplexo.features.reduce(reducer) :
			null;
	}


	function getUniaoAtividades() {

		reducer = (accumulator, currentValue) => turf.union(accumulator, currentValue);

		atvReducer = (acc, curr) =>
			turf.union(
			acc.geometriasAtividade.features.reduce(reducer), curr.geometriasAtividade.features.reduce(reducer)
		);

		return $scope.cadastro.caracterizacao.atividadesCaracterizacao ?
			$scope.cadastro.caracterizacao.atividadesCaracterizacao.length === 1 ?
			$scope.cadastro.caracterizacao.atividadesCaracterizacao[0].geometriasAtividade.features.reduce(reducer) :
			$scope.cadastro.caracterizacao.atividadesCaracterizacao.reduce(atvReducer) :
			null;
	}

	function verificaIntercao(collectionUnion, feature){
		return collectionUnion && feature ? turf.intersect(collectionUnion, feature) : false;
	}


	function todasAtividadesTemGeometria() {

		if ($scope.cadastro.retificacao && !$rootScope.dadosCaracterizacaoProntos) {
			return false;
		}

		if ($scope.cadastro.caracterizacao.complexo) {

			return $scope.cadastro.caracterizacao.geometriasComplexo &&
				$scope.cadastro.caracterizacao.geometriasComplexo.features &&
				$scope.cadastro.caracterizacao.geometriasComplexo.features.length > 0;

		} else {

			let contGeometriasValidas = 0;
			let atividadesCaracterizacao = $scope.cadastro.caracterizacao.atividadesCaracterizacao;

			if (!_.isEmpty(atividadesCaracterizacao)) {

				for (let ac of atividadesCaracterizacao) {

					let listaGeometriasErradas = ac.geometriasAtividade ? 
						ac.geometriasAtividade.filter(geo => geo.geometria === null || typeof geo.geometria === 'string') : [];

					if (!_.isEmpty(listaGeometriasErradas)) {

						if (listaGeometriasErradas[0].geometria === null && !!$rootScope.atividadesCaracterizacao) {
							// Em algum lugar está apagando os dados... (workaround!)
							atividadesCaracterizacao = $scope.cadastro.caracterizacao.atividadesCaracterizacao = $rootScope.atividadesCaracterizacao;
							break;
						}
					
						ac.geometriasAtividade = listaGeometriasErradas.map(geo => {
							
							return { geometria: geo.geometria ? JSON.parse(geo.geometria) : geo };

						});

					}

					if (!_.isEmpty(ac.geometriasAtividade)) {

						for (let geometriaAtividade of ac.geometriasAtividade) {

							let geo = geometriaAtividade.geometria || geometriaAtividade;

							if ((geo.type && geo.type == 'FeatureCollection'  && !_.isEmpty(geo.features)) ||
								(            geo.type == 'GeometryCollection' && !_.isEmpty(geo.geometries))) {

								contGeometriasValidas++;
								break;
		
							} 
							
						}

					}

				}

			}

			$rootScope.atividadesCaracterizacao = atividadesCaracterizacao;
			
			return contGeometriasValidas == $scope.cadastro.caracterizacao.atividadesCaracterizacao.length;

		}

	}

	function verificaAreaInformadaComDesenho() {

		if(isAreaVerificada){
			return true;
		}

		if($scope.cadastro.caracterizacao.complexo){
			if((_.isEmpty($scope.cadastro.caracterizacao.geometriasComplexo))){
				passoValidoMSG = "Adicione uma geometria para o complexo de atividades.";
				return false;
			}
			return true;
		}
		else if (todasAtividadesTemGeometria()) {
			return true;
		}
		else {
			passoValidoMSG = "Adicione uma geometria no mapa para cada atividade.";
			return false;
		}

	}

    function allTrue(obj) {
		for(var o in obj)
			if(!obj[o]) return false;
		return true;
    }

	function beforeEscolherEtapa() {

		if(etapaGeo.passoValido()){

			salvarCaracterizacao();

			return true;

		} else {
			passoValidoMSG = "Adicione uma geometria no mapa para cada atividade.";
			return false;
		}

	}

	var controleLayerAdicionada = {};

	function addOverlay(nomeLayer, leafletLayer) {

		if(!etapaGeo.layerGroup) {
			etapaGeo.layerGroup = {};
		}

		if(!etapaGeo.layerGroup.overlays) {
			etapaGeo.layerGroup.overlays = {};
		}

		if(controleLayerAdicionada[nomeLayer]){
			controleLayerAdicionada[nomeLayer].addLayer(leafletLayer);
		} else {
			etapaGeo.layerGroup.overlays[nomeLayer] = leafletLayer;
			controleLayerAdicionada[nomeLayer] = leafletLayer;
		}

	}

	function proximo() {

		//Validações serão feitas no método beforeEscolherEtapa
		isAreaVerificada = false;
		isIntersectionsVerificado = false;
		hasIntersection = false;
		imovelValidoParaLicenciamento = false;

		if(etapaGeo.passoValido()){
			$scope.cadastro.proximo();
		} else {
			mensagem.warning(passoValidoMSG, {ttl: 10000});
		}
	}

	function getGeometriaMunicipio(id) {

		var deffered = $q.defer();

		municipioService.getMunicipioGeometryById(id)
			.then(function(response) {

				etapaGeo.limite = response.data.limite;
				deffered.resolve();
			})
			.catch(function() {

				mensagem.error("Não foi possível obter o limite do município.");
				deffered.reject();
			});

			return deffered.promise;

	}

	function getGeometriaEstado(codigo) {

		var deffered = $q.defer();

		estadoService.getEstadoGeometryByCodigo(codigo)
			.then(function(response) {

				etapaGeo.limite = response.data.limite;
				deffered.resolve();

			})
			.catch(function() {

				mensagem.error("Não foi possível obter o limite do estado.");
				deffered.reject();

			});

			return deffered.promise;

	}

	function getDadosImovel(codigoImovel) {

		var deffered = $q.defer();

		imovelService.getImoveisCompletoByCodigo(codigoImovel)
			.then(function(response){

				if($scope.cadastro.caracterizacao.atividadesCaracterizacao[0].atividade.dentroEmpreendimento){

					etapaGeo.limite = response.data.geo;
				}

				imovelValidoParaLicenciamento = true;
				deffered.resolve();

				/*
				imovelService.verificarLicenciamento(codigoImovel)
					.then((response) => {
						imovelValidoParaLicenciamento = true;
						deffered.resolve();
					})
					.catch((err) => {
						imovelValidoParaLicenciamento = false;
						mensagem.error("A condição do CAR não permite a solicitação de Licenciamento. Favor entrar em contato com o IMASUL para verificação.");
						deffered.reject();
					});
				*/

			})
			.catch(function(err){

				imovelValidoParaLicenciamento = false;
				mensagem.error("Não foi possível obter os dados do imóvel no CAR.");
				deffered.reject();

			});

		return deffered.promise;

	}

	/*
	function getTemasImovel() {

		var deffered = $q.defer();

		imovelService
			.getTemasByImovel($scope.cadastro.empreendimento.imovel.codigo, areasProibidas)
			.then(function(response){

				_(response.data).each(function(geoJson){

					var geo = JSON.parse(geoJson.geo);
					let feature = {type: 'Feature', geometry: geo};
					let codigo = geoJson.tema.codigo;

					if(areasProibidas.indexOf(codigo) !== -1) {

						if(!geometriasAreasProibidas[codigo]) {

							geometriasAreasProibidas[codigo] = feature;

						} else {

							geometriasAreasProibidas[codigo] = turf.union(geometriasAreasProibidas[codigo], feature);
						}

						if(codigo === 'APP_TOTAL') {
							areaAPP = geometriasAreasProibidas.APP_TOTAL;
						}
					}

					addOverlay(geoJson.tema.codigo, L.geoJson(geo, {
						style: function(){
							return app.Geo.overlays[geoJson.tema.codigo].style;
						}
					}));

				});
				deffered.resolve();
			});

		return deffered.promise;

	}
	*/

	var init = function() {

		if (!$scope.cadastro.dadosGEOCarregados){

			delete $scope.cadastro.geometry;

		} else {
			etapaGeo.controleAtualizacao = true;
		}

		$scope.cadastro.dadosGEOCarregados = true;

		// if($scope.cadastro.continuacao){
		// 	getGeometriaEstado($scope.cadastro.caracterizacao.empreendimento.municipio.estado.codigo);
		// }
	};

	$rootScope.$on('iniciacializaGeoComEmpreendimento', function(event){
		initGeo();
	});

	function initGeo() {
		// $scope.cadastro.verificacoesEditar.isEditar = false;
		// $scope.cadastro.verificacoesEditar.modalJaApareceu = false;

		if($scope.cadastro.empreendimento){

			var estado = $scope.cadastro.empreendimento.municipio.estado;
			var sigla = estado.sigla || estado.codigo;

			if($scope.cadastro.empreendimento.imovel instanceof Object){

				if(!$scope.cadastro.caracterizacao.atividadesCaracterizacao[0].atividade.dentroEmpreendimento){

					$q.all([getDadosImovel($scope.cadastro.empreendimento.imovel.codigo), getGeometriaEstado(sigla), /*getTemasImovel()*/])
						.then(init);

				}else{

					$q.all([getDadosImovel($scope.cadastro.empreendimento.imovel.codigo), getGeometriaMunicipio($scope.cadastro.empreendimento.municipio.id), /*getTemasImovel()*/])
						.then(init);
				}

			}else if (!$scope.cadastro.caracterizacao.atividadesCaracterizacao[0].atividade.dentroEmpreendimento){
				
				$q.all(getGeometriaEstado(sigla))
					.then(init);

			}else{

				$q.all([getGeometriaMunicipio($scope.cadastro.empreendimento.municipio.id)])
					.then(init);
			}
		}

		$scope.$broadcast('carregarLocalizacaoEmpreendimento');
	}

	function getGeoLocalizacao() {
		// return $rootScope.empreendimento.localizacao === "ZONA_RURAL" ?
		// 	$rootScope.empreendimento.imovel.limite : $rootScope.empreendimento.empreendimentoEU.localizacao.geometria;
		return $rootScope.empreendimento.empreendimentoEU.localizacao.geometria;
	}

	function verificarSobreposicoesNaoPermitidasSimplificado(caracterizacao) {

		let geometrias = [];

		let geometriasAnalizar = caracterizacao.complexo ? caracterizacao.geometriasComplexo : caracterizacao.atividadesCaracterizacao;

		if(Array.isArray(geometriasAnalizar)) {

			geometriasAnalizar.forEach(function(g) {

				let iteravel = [];

				iteravel = g.geometriasAtividade;

				iteravel.forEach(function(elemento) {

					let features = elemento.features;

					if(features) {
						features.forEach(function(f) {
							geometrias.push(JSON.stringify(f.geometry));
						});
					}

				});

			});

		} else {

			let features = geometriasAnalizar.features;
	
			if(features) {
				features.forEach(function(f) {
					geometrias.push(JSON.stringify(f.geometry));
				});
			}

		}

		var deffered = $q.defer();

		caracterizacaoService.sobreposicoesNaoPermitidasSimplificado(geometrias)
			.then(function(response) {
				deffered.resolve(response.data);
			})
			.catch(function() {
				deffered.reject('"Não foi possível obter as sobreposições."');
			});

		return deffered.promise;

	}

	function unirGeometriasAtividade() {

		$scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach((ac) => {

			let features = ac.geometriasAtividade.features || ac.geometriasAtividade;

			if(!(_.isEmpty(features))) {

				ac.geometriasAtividade = features.map((f) => {
					return {geometria: unionGeometries(f)};
				});
			}

		});

	}

	function unirGeometriasComplexo() {

		if(!(_.isEmpty($scope.cadastro.caracterizacao.geometriasComplexo.features))) {

			$scope.cadastro.caracterizacao.geometriasComplexo = $scope.cadastro.caracterizacao.geometriasComplexo
			.features.map((f) => {
				return {geometria: unionGeometries(f)};
			});
		}
	}

	unionGeometries = (geometries) => {

		if(geometries.type === 'FeatureCollection') {
		
			return {
				type: "Feature",
				geometry: {
					type: "GeometryCollection",
					geometries: geometries.features.map(feature => feature.geometry),
				}
			};

		}

		return geometries.geometry;

	};

	function adequarGeometrias() {

		if($scope.cadastro.caracterizacao.complexo) {

			unirGeometriasComplexo();

			var geometriaComplexo = $scope.cadastro.caracterizacao.geometriasComplexo;
			
			if(!(_.isEmpty(geometriaComplexo.features))) {
			
				var novasGeometrias = [];
			
				geometriaComplexo.features.forEach(function(f) {
					var geometryCollection = {type: "GeometryCollection", geometries: []};
					geometryCollection.geometries.push(f.geometry);
					novasGeometrias.push({geometria: geometryCollection});
				});
			
				$scope.cadastro.caracterizacao.geometriasComplexo = novasGeometrias;
			}

			_.forEach($scope.cadastro.caracterizacao.atividadesCaracterizacao , function(ac){

				ac.geometriasAtividade = [];
			});

		} else {

			unirGeometriasAtividade();

			$scope.cadastro.caracterizacao.geometriasComplexo = [];

		}
	}

	function salvarCaracterizacao() {

		$q.all([verificarSobreposicoesNaoPermitidasSimplificado($scope.cadastro.caracterizacao)])
			.then(function(data){

				var sobreposicaoNaoPermitida = data[0];

				if(!sobreposicaoNaoPermitida) {

					$scope.cadastro.caracterizacao.etapasConcluidas = [];
					$scope.cadastro.caracterizacao.etapasConcluidas.push(etapaGeo.etapasCadastroCaracterizacao.ATIVIDADE);
					$scope.cadastro.caracterizacao.etapaRascunho = etapaGeo.etapasCadastroCaracterizacao.LOCALIZACAO + 1;

					adequarGeometrias();

					$scope.cadastro.podeQuestionario = true;

					if(!$scope.cadastro.verificacoesEditar.isEditar){

						caracterizacaoService.saveSimplificado($scope.cadastro.caracterizacao)
							.then(function(response){

								mensagem.success('Rascunho salvo com sucesso!' , {ttl: 15000});
								// $scope.cadastro.caracterizacao.id = response.data.id;
								_.extend($scope.cadastro.caracterizacao, response.data);
								getValorParametros();
								return true;

							})
							.catch(function(response){

								if(response && response.data && response.data.texto) {
									mensagem.error(response.data.texto, {ttl: 15000});
								}
								else {
									mensagem.error('Falha ao salvar rascunho da solicitação.' , {ttl: 15000});
								}
								return false;

							});

					}else{

						$scope.cadastro.verificacoesEditar.isEditar = false;
						$scope.cadastro.verificacoesEditar.modalJaApareceu = false;

						caracterizacaoService.updateEtapaGeo($scope.cadastro.caracterizacao)
							.then(function(response){
								mensagem.success('Rascunho editado com sucesso!' , {ttl: 15000});
								// $scope.cadastro.caracterizacao.id = response.data.id;
								_.extend($scope.cadastro.caracterizacao, response.data);
								getValorParametros();

								$scope.cadastro.verificacoesEditar.isEditar = false;
								$scope.cadastro.verificacoesEditar.modalJaApareceu = false;
								
								return true;
							})
							.catch(function(response){
								if(response && response.data && response.data.texto) {
									mensagem.error(response.data.texto, {ttl: 15000});
								}
								else {
									mensagem.error('Falha ao editar rascunho da solicitação.' , {ttl: 15000});
								}
								return false;
							});
					}

				} else {

					$scope.cadastro.etapas.GEO.limparEtapa();
					$scope.cadastro.escolherEtapa($scope.cadastro.etapas.GEO);
					mensagem.error("Não foi possível salvar as atividades pois ao menos uma se encontra em área restrita ou de conservação.", {ttl: 15000});

				}

			});

	}
	
	init();
	
	function getValorParametros() {

		var atividadesCaracterizacao = $scope.cadastro.caracterizacao.atividadesCaracterizacao;

		_.forEach(atividadesCaracterizacao, function(ac) {
			_.forEach(ac.atividade.parametros, function(parametro, index) {
				parametro.valorParametro = ac.atividadeCaracterizacaoParametros[index].valorParametro;
			});
		});
	}

};

exports.controllers.CaracterizacaoLSAGeoController = CaracterizacaoLSAGeoController;
