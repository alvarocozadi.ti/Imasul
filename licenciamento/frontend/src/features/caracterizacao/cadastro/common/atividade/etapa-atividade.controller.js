var CaracterizacaoDLAAtividadeController = function ($scope, $location, $rootScope, mensagem, caracterizacaoService, imovelService,$q,
                                                     modalSimplesService, $routeParams, ID_TIPOLOGIA_AGROSSILVIPASTORIL, empreendimentoService, tiposLicencaService) {

    var etapaAtividade = this;

    etapaAtividade.passoValido = passoValido;
    etapaAtividade.proximo = proximo;
    etapaAtividade.anterior = anterior;
    etapaAtividade.removerAtividade = removerAtividade;
    etapaAtividade.getAtividadesPorTipologia = getAtividadesPorTipologia;
    etapaAtividade.alterarTipologia = alterarTipologia;
    etapaAtividade.permitidoAlterarTipologia = true;
    etapaAtividade.verificaTipologiaSelecionada = verificaTipologiaSelecionada;
    etapaAtividade.verificaIsEditar = verificaIsEditar;
    etapaAtividade.verificaIsEditarTipoSolicitacao = verificaIsEditarTipoSolicitacao;
    // etapaAtividade.abrirModalSelecionarCnae = abrirModalSelecionarCnae;
    etapaAtividade.trocarTipoCaracterizacao = trocarTipoCaracterizacao;
    etapaAtividade.filtrarAtividadesDaCnae = filtrarAtividadesDaCnae;
    etapaAtividade.limparDados = limparDados;
    etapaAtividade.cadastroSelecionadoDI = cadastroSelecionadoDI;
    etapaAtividade.cadastroSelecionadoSimplificado = cadastroSelecionadoSimplificado;
    etapaAtividade.configCampoVigenciaRequerida = configCampoVigenciaRequerida;
    // Funções para validação para trocar de passo
    etapaAtividade.validaTiposLicencaEscolhidas = validaTiposLicencaEscolhidas;
    etapaAtividade.validarLicencaEscolhidaNaEtapaCorreta = validarLicencaEscolhidaNaEtapaCorreta;
    // Funções utilizadas na lógica de tiposLicença
    etapaAtividade.listaLicencasAbreModal = listaLicencasAbreModal;
    etapaAtividade.listaTiposLicenca = listaTiposLicenca;
    etapaAtividade.selecionaItem = selecionaItem;
    etapaAtividade.quantidadeCasasDecimais = quantidadeCasasDecimais;
    etapaAtividade.listaLicencasAtualizacaoSelecionadas = [];
    etapaAtividade.hideVoltar = true;
    etapaAtividade.tipologiasCodigo = app.TIPOLOGIAS;
    etapaAtividade.visualizaDescricao = false;
    etapaAtividade.zonaLocalizacao = app.LOCALIZACOES_EMPREENDIMENTO;
    etapaAtividade.atividadePrincipalSelecionada = false;
    etapaAtividade.etapasCadastroCaracterizacao = app.ETAPA_CADASTRO_CARACTERIZACAO;

    $scope.cadastro.etapas.ATIVIDADE.passoValido = passoValido;
    $scope.cadastro.etapas.ATIVIDADE.limparEtapa = limparAtividades;
    $scope.cadastro.etapas.ATIVIDADE.beforeEscolherEtapa = beforeEscolherEtapa;
    $scope.selecionaComplexo = selecionaComplexo;
    $scope.limitesValidos = null;
    $scope.complexoNaoSelecionado = false;

    var imovelValidoParaLicenciamento = false;
    $scope.cadastro.caracterizacao.dispensa = {};
    $scope.cadastro.caracterizacao.atividadesCaracterizacao = [];
    $scope.cadastro.caracterizacao.descricaoAtividade = null;
    $scope.cadastro.caracterizacao.vigenciaSolicitada = null;
    $scope.cadastro.atividadesCnaesEmpreendimento = [];
    $scope.cadastro.caracterizacao.atividadeSelecionada = {};
    etapaAtividade.tipologiaAnterior = undefined;
    etapaAtividade.tipoCaracterizacaoAnterior = undefined;

    function getCaracterizacao(){

        $rootScope.dadosCaracterizacaoProntos = false;

        if($scope.cadastro.idCaracterizacao){
            caracterizacaoService.dadosCaracterizacao($scope.cadastro.idCaracterizacao)
            .then(function(response) {
        
                $scope.cadastro.caracterizacao = response.data;
                $rootScope.dadosCaracterizacaoProntos = true;
        
                getAtividadesPorTipologia();
                getValorParametros();
                selecionaItem($scope.cadastro.caracterizacao.atividadesCaracterizacao[0], false);

                if($scope.cadastro.renovacao || $scope.cadastro.retificacao) {
                    $scope.cadastro.caracterizacao.tipoLicenca = $scope.cadastro.listaSelecionada ?
                        $scope.cadastro.listaSelecionada[0] : $scope.cadastro.caracterizacao.tipoLicenca;
                }else {
                    $scope.cadastro.listaSelecionada = $scope.cadastro.caracterizacao.tiposLicencaEmAndamento;

                }
            });
        }
    }

    function getEmpreendimento(){
        empreendimentoService.getEmpreendimentoPorId($scope.cadastro.idEmpreendimento)
        .then(function (response) {

            etapaAtividade.empreendimento = response.data;
            $rootScope.empreendimento = etapaAtividade.empreendimento;
            $rootScope.estado = etapaAtividade.empreendimento.municipio.estado;
            verificaImovelRuralValidoLicenciamento();
            getTipologias();
            
        });
    }

    function init() {

        $q.all([ getEmpreendimento() ,getCaracterizacao()]);

        $scope.cadastro.dadosGEOCarregados = false;

    }

    init();

    function getValorParametros() {

		var atividadesCaracterizacao = $scope.cadastro.caracterizacao.atividadesCaracterizacao;

		_.forEach(atividadesCaracterizacao, function(ac) {
			_.forEach(ac.atividade.parametros, function(parametro, index) {
				parametro.valorParametro = ac.atividadeCaracterizacaoParametros[index].valorParametro;
			});
		});
	}

    function resetCaracterizacao() {
        etapaAtividade.caracterizacao = {
            empreendimento: {
                id: etapaAtividade.idEmpreendimento
            },
            atividadesCaracterizacao: []
        };

        for (var index in etapaAtividade.etapas) {
            etapaAtividade.etapas[index].beforeEscolherEtapa = undefined;
        }

    }

    function limparDados() {

        etapaAtividade.finalidadeItemAtivo = null;

        if (etapaAtividade.tipoCaracterizacao === 'dispensa') {
            etapaAtividade.tipologia = null;
            etapaAtividade.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
                ac.atividade = null;
            });
            etapaAtividade.caracterizacao.descricaoAtividade = null;
            etapaAtividade.cadastro.caracterizacao.vigenciaSolicitada = null;

            etapaAtividade.limparDados();
        } else {
            resetCaracterizacao();
            $scope.cadastro.caracterizacao.atividadeSelecionada = {};
            etapaAtividade.tipologia = null;
        }
    }

    function verificaIsEditarTipoSolicitacao(item, alterar){
        if($scope.cadastro.verificacoesEditar.isEditar){
            if(!$scope.cadastro.verificacoesEditar.modalJaApareceu){
                $scope.cadastro.verificacoesEditar.houveAlteracao = true;
                $scope.cadastro.verificacoesEditar.modalJaApareceu = true;

                item = $scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0];
                selecionaItem(item,alterar);

                if($scope.cadastro.confirmarEdicao($scope.cadastro.etapas.ATIVIDADE.indice, true)){
                    selecionaItem(item,alterar);
                
                }else{
                    item = $scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0];
                    selecionaItem(item,alterar);

                }
            }else{
                selecionaItem(item,alterar);
                $scope.cadastro.limparProximasEtapas(1);
            }
        }else{
            selecionaItem(item,alterar);
        }
        
    }

    function verificaIsEditar(valorParametro){
        if($scope.cadastro.verificacoesEditar.isEditar && valorParametro != null){
            $scope.cadastro.verificacoesEditar.houveAlteracao = true;

            if(!$scope.cadastro.verificacoesEditar.modalJaApareceu){
                $scope.cadastro.verificacoesEditar.modalJaApareceu = true;
                $scope.cadastro.confirmarEdicao($scope.cadastro.etapas.ATIVIDADE.indice, false);
            }else{
                $scope.cadastro.limparProximasEtapas(1);
            }
        }
    }

    function removerAtividade(atividadeCaracterizacao) {
        
        var configModal = {};
        var instanciaModal = null;

        if(atividadeCaracterizacao.isPrincipal) {
            configModal = {
                titulo: 'Confirmar remoção da atividade',
                conteudo: 'Tem certeza que deseja remover a atividade principal? Após confirmado todas as atividades complementares também serão removidas, e as próximas etapas deverão ser novamente preenchidas.'
            };
    
            instanciaModal = modalSimplesService.abrirModal(configModal);
    
            instanciaModal.result.then(() => {
                if($scope.cadastro.verificacoesEditar.isEditar){
                    $scope.cadastro.verificacoesEditar.houveAlteracao = true;
                    $scope.cadastro.verificacoesEditar.modalJaApareceu = true;
                }
                limparAtividades();
                $scope.cadastro.limparProximasEtapas(1);
                etapaAtividade.listaSolicitacoes = [];
                etapaAtividade.listaRenovacoes = [];
                etapaAtividade.listaCadastro = [];
    
            }, () => {
            });
        } else {
            configModal = {
                titulo: 'Confirmar remoção da atividade',
                conteudo: 'Tem certeza que deseja remover a atividade? Talvez você tenha que preencher alguns dados novamente nas próximas etapas.'
            };
    
            instanciaModal = modalSimplesService.abrirModal(configModal);
    
            instanciaModal.result.then(() => {
                var indexAC = $scope.cadastro.caracterizacao.atividadesCaracterizacao.findIndex(ac => ac === atividadeCaracterizacao);
                $scope.cadastro.caracterizacao.atividadesCaracterizacao[indexAC].atividade.parametros.forEach(function (p) {
                    p.valorParametro = null;
                });
    
                $scope.cadastro.caracterizacao.atividadesCaracterizacao.splice(indexAC, 1);
                etapaAtividade.atividades = etapaAtividade.atividadesBckp;
    
            }, () => {
            });
        }

    }

    // function podeEmitirDla() {

    //     if ($scope.cadastro.tipologia && $scope.cadastro.tipologia.id === ID_TIPOLOGIA_AGROSSILVIPASTORIL) {
    //         return $scope.cadastro.empreendimento.isPessoaFisica;
    //     }
    //     return true;
    // }

    function passoValido() {

        if ($scope.cadastro.dispensa) {
            return true;
        }

        $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
            ac.atividadeCaracterizacaoParametros = [];
            if (ac.atividade) {
                if(ac.atividade.parametros !== undefined){
                    ac.atividade.parametros.forEach(function (p) {
                        ac.atividadeCaracterizacaoParametros.push({
                            parametroAtividade: p,
                            valorParametro: p.valorParametro
                        });
                    });
                }
            }
        });

        return !!(validarCamposFormulario() &&
           /* podeEmitirDla() && */tipoAtividadePermitido() &&
            possuiDescricaoValida() &&
            /*possuiVigenciaValida() &&*/
            validarLicencaEscolhidaNaEtapaCorreta() &&
            validarParametrosPreenchidos() &&
            validarComplexo() &&
            imovelValidoParaLicenciamento);
    }

    // function todasAtividadesComCnaesSelecionados() {
    //     var todosOk = true;
    //     $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
    //         if (_.isEmpty(ac.atividadesCnae)) {
    //             todosOk = false;
    //         }
    //     });
    //     return todosOk;
    // }

    function tipoAtividadePermitido() {

        return $scope.cadastro.empreendimento;
    }

    function validaLimiteParametro(){

        let valido = true;
        $scope.limitesValidos = {};
    
        $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
        
            $scope.limitesValidos[ac.atividade.codigo] = [];
    
            ac.atividadeCaracterizacaoParametros.forEach(function (acp, index) {
    
                let limites = _.find(ac.atividade.limites, limite => limite.parametroAtividade.codigo == acp.parametroAtividade.codigo);

                if (limites) {

                    let valorParametro = acp.valorParametro;
                    let limiteInferior = limites.limiteInferior;
                    let limiteSuperior = limites.limiteSuperior;
                    let valorParametroIsNumber = _.isNumber(valorParametro);
                    let limiteInferiorIsNumber = _.isNumber(limiteInferior);
                    let limiteSuperiorIsNumber = _.isNumber(limiteSuperior);

                    // até X            ---> ]-oo,   X]
                    // acima de X até Y ---> ]  X,   Y]
                    // acima de Y       ---> ]  Y, +oo[

                    if ( !valorParametroIsNumber || 
                        (  limiteInferiorIsNumber &&  limiteSuperiorIsNumber && (valorParametro <= limiteInferior || valorParametro > limiteSuperior) ) || 
                        (  limiteInferiorIsNumber && !limiteSuperiorIsNumber && valorParametro <= limiteInferior ) || 
                        ( !limiteInferiorIsNumber &&  limiteSuperiorIsNumber && valorParametro > limiteSuperior ) ) {

                        $scope.limitesValidos[ac.atividade.codigo][index] = false;
                        valido = false;

                    } 

                }
    
            });
    
        });
        
        return valido;
    
    }

    function validarCamposFormulario() {

        let camposValidos = $scope.formDadosAtividade && $scope.formDadosAtividade.tipoCaracterizacaoRadioSimplificadoDLa && $scope.formDadosAtividade.tipoCaracterizacaoRadioSimplificadoDLa.$valid;

        if ($scope.cadastro.retificacao || $scope.cadastro.renovacao){
            if ($scope.formDadosAtividade.tipoCaracterizacaoRadio) {
                camposValidos = $scope.formDadosAtividade && $scope.formDadosAtividade.tipoCaracterizacaoRadio && $scope.formDadosAtividade.tipoCaracterizacaoRadio.$valid;
            }else{
                camposValidos = $scope.formDadosAtividade;
            }
        }else if (etapaAtividade.empreendimento && etapaAtividade.empreendimento.isPessoaFisica) {
            camposValidos = camposValidos && !!$scope.formDadosAtividade.selectTipologia && $scope.formDadosAtividade.selectTipologia.$valid;
        }

        if ($scope.formDadosAtividade.tipoCaracterizacaoRadio){
            camposValidos = camposValidos && $scope.formDadosAtividade.tipoCaracterizacaoRadio && $scope.formDadosAtividade.tipoCaracterizacaoRadio.$valid;
        }

        return camposValidos;
    }

    function redirecionarListagemCaracterizacoes() {
        $location.path('empreendimento/' + $routeParams.idEmpreendimento + '/caracterizacoes');
    }

    function validaTiposLicencaEscolhidas() {

        let validacaoLicencaRenovacao = false;
        let validaListaAtualizacao = false;
        let validaListaSolicitacoes = false;
        let validaListaCadastro = false;

        $scope.cadastro.listaSelecionada = etapaAtividade.licencaSelecionada;

        if ($scope.cadastro.renovacao) {
            $scope.cadastro.listaSelecionada =
                [$scope.cadastro.listaFluxoLicencas[$scope.cadastro.caracterizacao.tipoLicenca.sigla][$scope.cadastro.acao]];
            return $scope.cadastro.listaSelecionada.length > 0;
        }else if ($scope.cadastro.retificacao) {
            $scope.cadastro.listaSelecionada = [$scope.cadastro.caracterizacao.tipoLicenca];
            return $scope.cadastro.listaSelecionada.length > 0;
        }else {
            // Percorre a lista de Renovações (radiobuttons)
            validacaoLicencaRenovacao = etapaAtividade.listaRenovacoes.some(function (element) {
                return element.selecionado;
            });

            //Percorre a lista de Solicitações
            validaListaSolicitacoes = etapaAtividade.listaSolicitacoes.some(function (element) {
                return element.selecionado;
            });

            //Percorre a lista com os cadastros
            if(etapaAtividade.listaCadastro) {
                validaListaCadastro = etapaAtividade.listaCadastro.some(function (element) {
                    return element.selecionado;
                });
            }

            // Caso já tenha encontrado um selecionado
            if (validacaoLicencaRenovacao) {
                $scope.cadastro.listaSelecionada = etapaAtividade.listaRenovacoes;
                return validacaoLicencaRenovacao;
            }else if (validaListaCadastro){
                $scope.cadastro.listaSelecionada = etapaAtividade.listaCadastro;
            }else {
                // Percorre a lista de Solicitações (checkboxes)
                validaListaSolicitacoes = etapaAtividade.listaSolicitacoes.some(function (element) {
                    $scope.cadastro.listaSelecionada = etapaAtividade.listaSolicitacoes;
                    return element.selecionado;
                });

                if(etapaAtividade.listaCadastro) {
                    validaListaCadastro = etapaAtividade.listaCadastro.some(function (element) {
                        return element.selecionado;
                    });
                }

                if (validacaoLicencaRenovacao) {
                    $scope.cadastro.listaSelecionada = etapaAtividade.listaRenovacoes;
                    return validacaoLicencaRenovacao;
                }
            }
        }

        if(etapaAtividade.licencaSelecionada === undefined && $scope.cadastro.caracterizacao.tiposLicencaEmAndamento){
            if($scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0].finalidade === 'SOLICITACAO'){
                validaListaSolicitacoes = true;
            }
            if($scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0].finalidade === 'RENOVACAO'){
                validacaoLicencaRenovacao = true;
            }
            if($scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0].finalidade === 'CADASTRO'){
                validaListaCadastro = true;
            }
            if($scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0].finalidade === 'ATUALIZACAO'){
                validaListaAtualizacao = true;
            }
        }
        
        return validacaoLicencaRenovacao || validaListaSolicitacoes || validaListaAtualizacao || validaListaCadastro;
    }

    function possuiDescricaoValida() {
        if ($scope.cadastro.renovacao) {
            return true;
        }
        return $scope.formDadosAtividade.descricaoAtividade && $scope.formDadosAtividade.descricaoAtividade.$valid;
    }

    function possuiVigenciaValida() {
        const vigenciaSolicitada = $scope.formDadosAtividade.vigenciaSolicitada;

        if ($scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.DI || $scope.cadastro.finalidadeLicenca === 'CADASTRO') {
            return true;
        } else if ( vigenciaSolicitada.$viewValue >= 1 && vigenciaSolicitada.$viewValue <= etapaAtividade.maxValidadeEmAnos) {
            return true;
        }
        return vigenciaSolicitada && vigenciaSolicitada.$valid;
    }

    function validarLicencaEscolhidaNaEtapaCorreta() {
        return !($scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.SIMPLIFICADO &&
            !validaTiposLicencaEscolhidas());
    }

    function validarParametrosPreenchidos() {
        var parametrosValidos = true;
        $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
            ac.atividadeCaracterizacaoParametros.forEach(function (acp) {
                if ((typeof acp.valorParametro) !== "number") {
                    parametrosValidos = false;
                }
            });
        });
        return parametrosValidos;
    }

    function validarComplexo() {
        if ($scope.cadastro.tiposCaracterizacao.SIMPLIFICADO) {
            if ($scope.cadastro.caracterizacao.atividadesCaracterizacao.length < 2)
                return true;            

            if ($scope.cadastro.caracterizacao.complexo !== undefined)
                return true;
        }
        return false;
    }

    function selecionaComplexo() {
        $scope.complexoNaoSelecionado = false;
    }

    function verificaImovelRuralValidoLicenciamento() {

        if ($rootScope.empreendimento.localizacao === 'ZONA_RURAL')
            verificaImovelValidoLicenciamento();
        else
            imovelValidoParaLicenciamento = true;

    }

    function verificaImovelValidoLicenciamento() {

        getDadosImovel($rootScope.empreendimento.imovel.codigo);

    }

    function getDadosImovel(codigoImovel) {

        imovelService.getImoveisCompletoByCodigo(codigoImovel)
            .then(function (response) {

                imovelValidoParaLicenciamento = true;

                /* 
                imovelService.verificarLicenciamento(codigoImovel)
                    .then((response) => {
                        imovelValidoParaLicenciamento = true;

                    })
                    .catch((err) => {
                        imovelValidoParaLicenciamento = false;

                    }); 
                */

            })
            .catch(function () {

                imovelValidoParaLicenciamento = false;
                mensagem.error("Não foi possível obter os dados do imóvel no CAR.");

            });

    }

    function proximo() {

        verificaImovelRuralValidoLicenciamento();

        // Força validação de erros no formulário
        $scope.formDadosAtividade.$setSubmitted();

        if($scope.cadastro.tipoCaracterizacao ==='dispensa'){
            $scope.cadastro.proximo();
        }

        let limiteParametrosValidos = validaLimiteParametro();
        let parametrosPreenchidosValidos = validarParametrosPreenchidos();

        if (passoValido() && limiteParametrosValidos) {

            if (($scope.cadastro.renovacao || $scope.cadastro.retificacao) && parametrosPreenchidosValidos) {
                $scope.cadastro.continuacao = true;
                $scope.cadastro.proximo();
            }

            $scope.cadastro.proximo();

        } else {

            let nenhumaCondicaoAtivada = true;

            if (!$scope.cadastro.tipologia && etapaAtividade.empreendimento.isPessoaFisica) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning('Selecione uma tipologia para seguir com o cadastro.');
            } 

            if (!validarLicencaEscolhidaNaEtapaCorreta()) {
                nenhumaCondicaoAtivada = false;
                $scope.hasError = true;
                mensagem.warning('Selecione ao menos um tipo de solicitação para prosseguir com o cadastro.');
            } 
            
            if (!possuiDescricaoValida()) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning('Escreva uma descrição da solicitação para prosseguir com o cadastro.');
            }
            
            /* if (!possuiVigenciaValida()) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning('Insira um período de vigência da solicitação dentro dos limites estabelecidos para prosseguir com o cadastro.');
            }*/ 
            
            if (!parametrosPreenchidosValidos) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning('Preencha todos os parâmetros para prosseguir com o cadastro.');
            }
            
            if (!limiteParametrosValidos) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning('Preencha todos os parâmetros dentro dos limites estabelecidos para prosseguir com o cadastro.',{ttl: 10000});
            } 
            
            if (!validarComplexo()) {
                nenhumaCondicaoAtivada = false;
                $scope.complexoNaoSelecionado = true;
                mensagem.warning('Selecione se as atividade serão desenvolvidas em um complexo ou não.');
            } 
            
            if (!imovelValidoParaLicenciamento) {
                nenhumaCondicaoAtivada = false;
                mensagem.warning("A condição do CAR não permite a solicitação de Licenciamento. Favor entrar em contato com o IMASUL para verificação.");
            } 
            
            if (nenhumaCondicaoAtivada) {

                var tipoEndereco = $scope.cadastro.empreendimento.localizacao;

                var possuiAtividadeSemUrbada = $scope.cadastro.caracterizacao.atividadesCaracterizacao.find((ac) =>
                    ac.atividade.tiposAtividade.filter((t) => t.codigo !== 'URBANA').length > 0
                );

                if ((tipoEndereco === 'ZONA_URBANA' && possuiAtividadeSemUrbada) || tipoEndereco === 'ZONA_RURAL') {
                    mensagem.warning('No momento não é possível continuar com a solicitação de declaração de inexigibilidade e licenças ambientais para imóveis e/ou atividades rurais. Favor procurar o IMASUL para prosseguir com o processo de licenciamento.');
                } else {
                    mensagem.warning('Verifique os campos destacados em vermelho para prosseguir com o cadastro.');
                }
                
            }
        }

        if ($scope.cadastro.renovacao) {
            $scope.cadastro.continuacao = true;
        }

        $scope.cadastro.licencaSelecionadaDentroDoPeriodo = false;

    }

    function beforeEscolherEtapa() {

        salvarCaracterizacao();
        return passoValido();
    }

    function anterior() {
        redirecionarListagemCaracterizacoes();
    }

    function trocarTipoCaracterizacao() {

        if (!etapaAtividade.tipologiaAnterior) {
            executarAlteracaoTipoCaracterizacao();
        } else {
            var configModal = {
                titulo: 'Confirmar alteração do tipo de solicitação',
                conteudo: 'Tem certeza que deseja alterar o tipo da solicitação selecionada? Ao alterar perderá todo o cadastro já preenchido até o momento.'
            };

            var instanciaModal = modalSimplesService.abrirModal(configModal);

            instanciaModal.result.then(function () {
                executarAlteracaoTipoCaracterizacao();
            }, function () {
                $scope.cadastro.tipoCaracterizacao = etapaAtividade.tipoCaracterizacaoAnterior;
            });
        }
    }

    function executarAlteracaoTipoCaracterizacao() {

        limparDados();
        limparAtividades();
        getTipologias();
        getAtividadesPorTipologia();

        if (cadastroSelecionadoDI()) {

            if ($scope.cadastro.etapas.DOCUMENTACAO) {
                $scope.cadastro.listaEtapas.splice($scope.cadastro.etapas.DOCUMENTACAO.indice, 1);
            }

            $scope.cadastro.etapas.ENQUADRAMENTO.indice -= 1;

            delete $scope.cadastro.etapas.DOCUMENTACAO;

            var atividadesCnaesDI = $scope.cadastro.atividadesCnaesEmpreendimento;

            if (!atividadesCnaesDI) {
                etapaAtividade.atividadesCnaes = [];
            } else {
                etapaAtividade.atividadesCnaes = atividadesCnaesDI.filter(function (atividadeCnae) {
                    return atividadeCnae.dispensaLicenciamento === true;
                });
            }

        } else if (cadastroSelecionadoSimplificado()) {

            $scope.cadastro.etapas.DOCUMENTACAO = {
                nome: 'Documentação',
                indice: 3,
                passoValido: undefined,
                class: 'documentacao',
                disabled: false
            };
            $scope.cadastro.etapas.ENQUADRAMENTO.indice += 1;

            $scope.cadastro.listaEtapas.splice($scope.cadastro.etapas.DOCUMENTACAO.indice, 0, $scope.cadastro.etapas.DOCUMENTACAO);

            var atividadesCnaes = $scope.cadastro.atividadesCnaesEmpreendimento;

            if (!atividadesCnaes) {
                etapaAtividade.atividadesCnaes = [];
            } else {
                etapaAtividade.atividadesCnaes = atividadesCnaes.filter(function (atividadeCnae) {
                    return atividadeCnae.licenciamentoSimplificado === true || atividadeCnae.licenciamentoDeclaratorio === true;
                });
            }
        }

        etapaAtividade.tipologiaAnterior = undefined;
        $scope.cadastro.tipologia = undefined;
        etapaAtividade.tipoCaracterizacaoAnterior = $scope.cadastro.tipoCaracterizacao;
    }

    function cadastroSelecionadoDI() {
        return $scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.DI;
    }

    function cadastroSelecionadoSimplificado() {
        return $scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.SIMPLIFICADO;
    }

    function getTipologias() {
        let idEmpreendimento = etapaAtividade.empreendimento.id;
        var params;

        if ($scope.cadastro.tipoCaracterizacao === 'dispensa') {
            params = {
                dispensaLicenciamento: true
            };
        } else {
            params = {
                licenciamentoSimplificado: true
            };
        }

        params.idEmpreendimento = idEmpreendimento;

        caracterizacaoService.getTipologias(params)
            .then(function (response) {

                etapaAtividade.tipologias = response.data;

                if (etapaAtividade.tipologias === 0) {
                    mensagem.warning("Os CNAE's cadastrados para o empreendimento não permitem esse tipo de solicitação.");
                }
            });

    }

    function getAtividadesPorTipologia() {

        let idEmpreendimento = etapaAtividade.empreendimento === undefined ? $scope.cadastro.caracterizacao.empreendimento.id : etapaAtividade.empreendimento.id;
        let params;

        if($scope.cadastro.tipologia === undefined){
            $scope.cadastro.tipologia = $scope.cadastro.caracterizacao.tipologia;
        }
        if($scope.cadastro.tipologia){
            verificaTipologiaSelecionada($scope.cadastro.tipologia);
        }

        if (!$scope.cadastro.tipologia) {
            etapaAtividade.atividades = [];
            etapaAtividade.atividadesBckp = [];
        } else {

            if ($scope.cadastro.tipoCaracterizacao !== 'dispensa') {

                params = {
                    licenciamentoSimplificado: true,
                    idTipologia: $scope.cadastro.tipologia.id === undefined ?$scope.cadastro.caracterizaca.tipologia.id : $scope.cadastro.tipologia.id
                };
            } else {

                params = {
                    dispensaLicenciamento: true,
                    idTipologia: $scope.cadastro.tipologia.id
                };
            }

            params.idEmpreendimento = idEmpreendimento;

            return caracterizacaoService.getAtividadesPorTipologia(params)
                .then(function (response) {

                    etapaAtividade.atividades = response.data;
                    etapaAtividade.atividadesBckp = etapaAtividade.atividades;

                    if (!$scope.cadastro.retificacao && !$scope.cadastro.continuacao) {
                        listaLicencasAbreModal($scope.cadastro.caracterizacao.atividadesCaracterizacao[0]);
                    }

                    return response.data;
                });
        }
    }

    function verificaTipologiaSelecionada (tipologiaSelecionada){
        
        if(tipologiaSelecionada.codigo !== undefined && tipologiaSelecionada.codigo !== null ){
            
            if(tipologiaSelecionada.codigo === etapaAtividade.tipologiasCodigo.INDUSTRIA_BORRACHA || 
                tipologiaSelecionada.codigo === etapaAtividade.tipologiasCodigo.INDUSTRIA_COURO_PELES || 
                tipologiaSelecionada.codigo === etapaAtividade.tipologiasCodigo.INDUSTRIA_TEXTIL || 
                tipologiaSelecionada.codigo === etapaAtividade.tipologiasCodigo.INDUSTRIA_PRODUTOS_ALIMENTARES ||
                tipologiaSelecionada.codigo === etapaAtividade.tipologiasCodigo.INDUSTRIA_BEBIDAS_ALCOOL){
                    etapaAtividade.visualizaDescricao = true;
            }else{
                etapaAtividade.visualizaDescricao = false;
            }
        }else{
            etapaAtividade.visualizaDescricao = false;
        }

    }

    function limparAtividades() {

        etapaAtividade.atividadePrincipalSelecionada = false;
        //$scope.cadastro.caracterizacao.atividadesCaracterizacao = [{}];
        $scope.cadastro.caracterizacao.perguntasCarregadas = false;

        $scope.cadastro.caracterizacao.atividadesCaracterizacao = [];

        $scope.cadastro.caracterizacao.atividadeSelecionada = {};

        $scope.cadastro.caracterizacao.descricaoAtividade = null;

        $scope.cadastro.caracterizacao.vigenciaSolicitada = null;

    }

    function alterarTipologia() {

        if ($scope.cadastro.etapa.indice === 0 && !etapaAtividade.permitidoAlterarTipologia) {
            etapaAtividade.permitidoAlterarTipologia = true;
        }

        if (etapaAtividade.permitidoAlterarTipologia) {
            if (!etapaAtividade.atividadePrincipalSelecionada) {
                executarAlteracaoTipologia();
            } else {
                getAtividadesPorTipologia();
            }

           etapaAtividade.permitidoAlterarTipologia = false;
        }

    }

    function executarAlteracaoTipologia() {

        etapaAtividade.tipologiaAnterior = $scope.cadastro.tipologia;
        limparAtividades();
        getAtividadesPorTipologia();
    }

    function filtrarAtividadesDaCnae(atividadeCnae) {
        etapaAtividade.atividades = atividadeCnae.atividades;
        etapaAtividade.atividades.forEach(function (atividade) {
            atividade.atividadesCnae = [];
            atividade.atividadesCnae.push(atividadeCnae);
        });

        $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
            ac.atividade = undefined;
            ac.atividadesCnae = undefined;
        });
    }

    function listaLicencasAbreModal(atividade) {

        let rascunho = false;
        var todasLicencasPermitidas = [];

        if($scope.cadastro.caracterizacao.atividadeSelecionada == undefined ){
            rascunho = true;
            $scope.cadastro.caracterizacao.atividadeSelecionada = atividade;
        }

        todasLicencasPermitidas = todasLicencasPermitidas.concat($scope.cadastro.caracterizacao.atividadeSelecionada.atividade.tiposLicenca);
        $scope.cadastro.caracterizacao.atividadesCaracterizacao.forEach(function (ac) {
            todasLicencasPermitidas = todasLicencasPermitidas.concat(ac.atividade.tiposLicenca);
        });

        // Remove duplicados
        var licencasPermitidas = [];
        todasLicencasPermitidas.forEach(function (tlp) {
            if (licencasPermitidas.filter(function (lp) {
                return lp.id === tlp.id;
            }).length === 0) {
                licencasPermitidas.push(tlp);
            }
        });
        if( etapaAtividade.atividades == undefined){
            etapaAtividade.atividades = atividade.atividade;
        }
        etapaAtividade.atividadesBckp = etapaAtividade.atividades;
        getAtividadesPorTipologia();

        etapaAtividade.atividades = etapaAtividade.atividades.filter(a => {
            return a.dentroEmpreendimento === $scope.cadastro.caracterizacao.atividadeSelecionada.atividade.dentroEmpreendimento;
        });

        etapaAtividade.atividades = etapaAtividade.atividades.filter(a => {
            if ($scope.cadastro.caracterizacao.atividadeSelecionada.atividade.grupoDocumento) {
                return a.grupoDocumento && a.grupoDocumento.id === $scope.cadastro.caracterizacao.atividadeSelecionada.atividade.grupoDocumento.id;
            } else {
                return !a.grupoDocumento;
            }
        });

        // Busca os valores no backend dos tipos de Licenca
        etapaAtividade.listaTiposLicenca(licencasPermitidas);

        var novaAtividade = {};
        novaAtividade.atividadesCnae = [];
        novaAtividade.atividade = atividade;

        if (etapaAtividade.atividadePrincipalSelecionada == false) {
            novaAtividade.isPrincipal = true;
            etapaAtividade.atividadePrincipalSelecionada = true;
        } else {
            novaAtividade.isPrincipal = false;
        }

        if (!$scope.cadastro.caracterizacao.atividadesCaracterizacao ||
            $scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.DI) {
            $scope.cadastro.caracterizacao.atividadesCaracterizacao = [];
        }

        if(!$scope.cadastro.retificacao && rascunho === false){

            $scope.cadastro.caracterizacao.atividadesCaracterizacao.push(novaAtividade);
            $scope.cadastro.caracterizacao.atividadeSelecionada = {};

        }

        // abrirModalSelecionarCnae(atividade, $scope.cadastro.empreendimento);
    }

    // function abrirModalSelecionarCnae(atividade, empreendimento) {

    //     if (atividade.atividadesCnae && atividade.atividadesCnae.length > 1) {

    //         var modalInstance = $uibModal.open({
    //             controller: 'modalCaracterizacaoAtividadeController',
    //             controllerAs: 'modalCtrl',
    //             backdrop: 'static',
    //             keyboard: false,
    //             templateUrl: './features/caracterizacao/cadastro/common/modal-atividade.html',
    //             resolve: {
    //                 atividade: function () {
    //                     return atividade;
    //                 },
    //                 addMaisAtividades: function () {
    //                     return true;
    //                 },
    //                 empreendimento: function () {
    //                     return empreendimento;
    //                 }
    //             }
    //         });

    //         modalInstance.result.then(function (atividades) {

                

    //         }, function () {

    //             $scope.cadastro.caracterizacao.atividadeSelecionada = {};

    //             $rootScope.$broadcast('atividadeAlterada');

    //         });

    //         $scope.cadastro.dadosGEOCarregados = false;

    //         return modalInstance;

    //     } else {

    //         var novaAtividade = {};
    //         novaAtividade.atividade = atividade;
    //         novaAtividade.atividadesCnae = atividade.atividadesCnae;

    //         if (!$scope.cadastro.caracterizacao.atividadesCaracterizacao ||
    //             $scope.cadastro.tipoCaracterizacao === $scope.cadastro.tiposCaracterizacao.DI) {
    //             $scope.cadastro.caracterizacao.atividadesCaracterizacao = [];
    //         }

    //         $scope.cadastro.caracterizacao.atividadesCaracterizacao.push(novaAtividade);

    //         $scope.cadastro.dadosGEOCarregados = false;

    //         $scope.cadastro.caracterizacao.atividadeSelecionada = {};

    //         $rootScope.$broadcast('atividadeAlterada');
    //     }
    // }

    function carregarMunicipios() {

        enderecoService.listMunicipios('MS').then(
            function (response) {
                etapaAtividade.municipios = response.data;
            })
            .catch(function () {
                mensagem.warning('Não foi possível obter a lista de municípios.');
            });
    }

    // Variáveis para controle
    etapaAtividade.listaSolicitacoes = [];
    etapaAtividade.listaRenovacoes = [];
    etapaAtividade.finalidadeItemAtivo = "";
    etapaAtividade.selecionouLicenca = true;

    function listaTiposLicenca(licencasAtividade, item) {

        tiposLicencaService.listaTiposLicenca(licencasAtividade).then(function (response) {
                etapaAtividade.listaSolicitacoes = etapaAtividade.listaSolicitacoes && etapaAtividade.listaSolicitacoes.length > 0 ? etapaAtividade.listaSolicitacoes : response.data.tipoLicencaSolicitacao;
                etapaAtividade.listaRenovacoes = etapaAtividade.listaRenovacoes && etapaAtividade.listaRenovacoes.length > 0? etapaAtividade.listaRenovacoes : response.data.tipoLicencaRenovacao;
                etapaAtividade.listaAtualizacao = etapaAtividade.listaAtualizacao && etapaAtividade.listaAtualizacao.length > 0 ? etapaAtividade.listaAtualizacao : response.data.tipoLicencaAtualizacao;
                etapaAtividade.listaCadastro = etapaAtividade.listaCadastro && etapaAtividade.listaCadastro.length > 0 ? etapaAtividade.listaCadastro : response.data.tipoLicencaCadastro;
                if(item){

                    if(item.finalidade === "SOLICITACAO"){
                        etapaAtividade.listaSolicitacoes.forEach(function (element) {
                            if (element.sigla === item.sigla) {
                                document.getElementById(element.sigla).checked = true;
                                element.selecionado = true;                          
                            }
                        });

                        item.selecionado = !item.selecionado;
                        // Caso o usuário clique no texto, ao invés do botão
                        if (alterarValor) {
                            document.getElementById(item.sigla).checked = item.selecionado;
                        }

                        etapaAtividade.finalidadeItemAtivo = item.finalidade;

                        etapaAtividade.listaRenovacoes.forEach(function (element) {
                            if (element.sigla !== item.sigla) {
                                document.getElementById(element.sigla).checked = false;
                                element.selecionado = false;
                            }
                        });
                    }

                    if (item.finalidade === "RENOVACAO"){

                        etapaAtividade.listaRenovacoes.forEach(function (element) {
                            if (element.sigla === item.sigla) {
                                document.getElementById(element.sigla).checked = true;
                                element.selecionado = true;
                            }
                        });

                        $scope.cadastro.caracterizacao.tipoLicenca = item;
                        item.selecionado = !item.selecionado;
                        // Caso o usuário clique no texto, ao invés do botão
                        if (alterarValor) {
                            document.getElementById(item.sigla).checked = item.selecionado;
                        }

                        etapaAtividade.finalidadeItemAtivo = item.finalidade;

                        etapaAtividade.listaSolicitacoes.forEach(function (element) {
                            if (element.sigla !== item.sigla) {
                                document.getElementById(element.sigla).checked = false;
                                element.selecionado = false;
                            }
                        });

                    }

                    if (item.finalidade === "ATUALIZACAO"){

                        item.selecionado = !item.selecionado;
                        // Caso o usuário clique no texto, ao invés do botão
                        if (alterarValor) {
                            document.getElementById(item.sigla).checked = item.selecionado;
                        }
        
                        etapaAtividade.finalidadeItemAtivo = item.finalidade;
                    }

                    if(item.finalidade === 'CADASTRO'){
                        item.selecionado = !item.selecionado;
                        if (alterarValor) {
                            document.getElementById(item.sigla).checked = item.selecionado;
                        }
                        etapaAtividade.finalidadeItemAtivo = item.finalidade;
                        $scope.cadastro.caracterizacao.vigenciaSolicitada = 999;
                        $scope.cadastro.finalidadeLicenca = 'CADASTRO';
                    }
                }
            }).catch(function (error) {
            });

    }

    function setMaxValidadeEmAnos(validadeEmAnos){
        etapaAtividade.maxValidadeEmAnos = validadeEmAnos;
    }

    function configCampoVigenciaRequerida(){
        if (!!$scope.cadastro.tipoLicencaEvoluir.validadeEmAnos){
            setMaxValidadeEmAnos($scope.cadastro.tipoLicencaEvoluir.validadeEmAnos);
            etapaAtividade.selecionouLicenca = false;
        }
        return $scope.cadastro.retificacao || etapaAtividade.selecionouLicenca;
    }

    function setCampoVigenciaRequerida(validadeEmAnos) {
        const campoVigenciaRequerida = document.getElementById('campo-vigencia-requerida');
        campoVigenciaRequerida.value= "";
        campoVigenciaRequerida.placeholder = "Insira um valor entre 1 e " + validadeEmAnos + " anos";
        setMaxValidadeEmAnos(validadeEmAnos);
    }

    // Função para selecionar e limpar a seleção da lista contrária
    function selecionaItem(item, alterarValor) {

        if($scope.cadastro.caracterizacao.vigenciaSolicitada === 999 || $scope.cadastro.finalidadeLicenca === 'CADASTRO'){
            $scope.cadastro.caracterizacao.vigenciaSolicitada = undefined;
            $scope.cadastro.finalidadeLicenca = undefined;
        }

        if($scope.cadastro.caracterizacao.tiposLicencaEmAndamento && !$scope.cadastro.verificacoesEditar.isEditar){

            // Com problemas ao editar etapa ATIVIDADE
            item = $scope.cadastro.caracterizacao.tiposLicencaEmAndamento[0];
            etapaAtividade.listaTiposLicenca($scope.cadastro.caracterizacao.tiposLicencaEmAndamento);

        }else{
            etapaAtividade.licencaSelecionada = undefined;
        }

        $scope.listaLicencaAtualizacao = $scope.licencasEmAndamento !== undefined ? [$scope.licencasEmAndamento] : $scope.cadastro.caracterizacao.tiposLicencaEmAndamento;
        $scope.hasError = false;

        //Não terá o campo selecionado se for renovação,pois é transient e não armazena em banco de dados usado só para validações.
        if (item && item.selecionado === undefined) {

            item.selecionado = false;
            etapaAtividade.licencaSelecionada = item;

        }
        if (item && item.finalidade === undefined) {
            item.finalidade = $scope.cadastro.finalidadeLicenca;
            etapaAtividade.licencaSelecionada = item;
        }

        if (etapaAtividade.selecionouLicenca){
            etapaAtividade.selecionouLicenca = false;
        }

        var atividadeTiposLicenca = $scope.cadastro.caracterizacao.atividadesCaracterizacao[0].atividade.tiposLicenca;

        tiposLicencaService.listaTiposLicenca(atividadeTiposLicenca)
            .then(function (response) {

                etapaAtividade.listaSolicitacoes = response.data.tipoLicencaSolicitacao;
                etapaAtividade.listaRenovacoes = response.data.tipoLicencaRenovacao;
                etapaAtividade.listaAtualizacao = response.data.tipoLicencaAtualizacao;
                etapaAtividade.listaCadastro = response.data.tipoLicencaCadastro;

                if (item && item.finalidade === "SOLICITACAO") {
                    
                    if (!item.selecionado)
                        etapaAtividade.listaTiposLicenca(atividadeTiposLicenca, item);
                    
                    if (etapaAtividade.listaSolicitacoes.length === 0 && $scope.licencasEmAndamento.length > 0)
                        etapaAtividade.listaSolicitacoes = $scope.licencasEmAndamento;
                        
                } else if (item && item.finalidade === "RENOVACAO") {
        
                    if (item && !item.selecionado)
                        etapaAtividade.listaTiposLicenca(atividadeTiposLicenca, item);
                    
                    if (etapaAtividade.listaRenovacoes.length === 0 && $scope.licencasEmAndamento.length > 0) 
                        etapaAtividade.listaRenovacoes = $scope.licencasEmAndamento;
                    
                } else if (item && item.finalidade === "ATUALIZACAO") {
        
                    if (!item.selecionado)
                        etapaAtividade.listaTiposLicenca(atividadeTiposLicenca, item);

                }
                else if(item.finalidade === 'CADASTRO') {
        
                    if (!item.selecionado)
                        etapaAtividade.listaTiposLicenca(atividadeTiposLicenca, item);
                    
                }
                if (item.selecionado && !etapaAtividade.licencaSelecionada) {
                    etapaAtividade.licencaSelecionada = [];
                    etapaAtividade.licencaSelecionada.push(item);
                    $scope.itemEscolhido = item;
                }
                if(!$scope.cadastro.caracterizacao.tiposLicencaEmAndamento){
                    if(etapaAtividade.licencaSelecionada && etapaAtividade.licencaSelecionada[0].finalidade === 'CADASTRO'){
                        $scope.cadastro.caracterizacao.vigenciaSolicitada = 999;
                        $scope.cadastro.finalidadeLicenca = 'CADASTRO';
                    }
                }

        });

       // setCampoVigenciaRequerida(item.validadeEmAnos);
    }

    function quantidadeCasasDecimais(casasDecimais) {
        return ((typeof casasDecimais) === 'number' ? casasDecimais : '4');
    }

    function salvarCaracterizacao() {

        $rootScope.$broadcast('iniciacializaGeoComEmpreendimento');

        $scope.cadastro.caracterizacao.etapasConcluidas = [];
        if($scope.cadastro.caracterizacao.etapaRascunho === etapaAtividade.etapasCadastroCaracterizacao.ATIVIDADE){
            $scope.cadastro.caracterizacao.etapasConcluidas.push(etapaAtividade.etapasCadastroCaracterizacao.ATIVIDADE);
        }
        $scope.cadastro.caracterizacao.etapaRascunho = etapaAtividade.etapasCadastroCaracterizacao.ATIVIDADE + 1;

        if(cadastroSelecionadoSimplificado()){
            let tipo = {
                id: 3,
                nome: 'Simplificado'
            };
            $scope.cadastro.caracterizacao.tipo = tipo;

        }else if(cadastroSelecionadoDI()){
            let tipo = {
                id: 1,
                nome: 'Dispensa'
            };
            $scope.cadastro.caracterizacao.tipo = tipo;
        }

        if($scope.cadastro.renovacao || $scope.cadastro.retificacao) {
            $scope.cadastro.caracterizacao.tipoLicenca = $scope.cadastro.listaSelecionada ?
                $scope.cadastro.listaSelecionada[0] : $scope.cadastro.caracterizacao.tipoLicenca;
        }else {
            $scope.cadastro.caracterizacao.tiposLicencaEmAndamento =
                $scope.cadastro.listaSelecionada.filter((element) => element.selecionado);
        }

        if ($scope.cadastro.retificacao) {
            // Não é necessário salvar etapaAtividade na retificação pois os campos estão bloqueados para edição
            return true;
        }
        else if($scope.cadastro.verificacoesEditar.isEditar && $scope.cadastro.verificacoesEditar.houveAlteracao){

            caracterizacaoService.updateEtapaAtividade($scope.cadastro.caracterizacao)
            .then(function(response){

                $scope.cadastro.caracterizacao.atividadesCaracterizacao = response.data.atividadesCaracterizacao;
                $scope.cadastro.verificacoesEditar.isEditar = false;
                $scope.cadastro.verificacoesEditar.modalJaApareceu = false;

                mensagem.success('Rascunho editado com sucesso!' , {ttl: 15000});
                _.extend($scope.cadastro.caracterizacao, response.data);
                getValorParametros();
                $rootScope.$broadcast('changeListaAtividadesCaracterizacao',$scope.cadastro.caracterizacao.atividadesCaracterizacao);
                $rootScope.$broadcast('iniciacializaGeoComEmpreendimento');
                $scope.cadastro.verificacoesEditar.houveAlteracao = false;

                return true;

            })
            .catch(function(response){
                $scope.cadastro.verificacoesEditar.houveAlteracao = true;
                if(response && response.data && response.data.texto) {
                    mensagem.error(response.data.texto, {ttl: 15000});
                }
                else {
                    mensagem.error('Falha ao salvar edição rascunho da solicitação.' , {ttl: 15000});
                }
                return false;
            });

        }
        else if(!$rootScope.saveSimplificadoJaExecutado && !$scope.cadastro.verificacoesEditar.isEditar && !$scope.cadastro.verificacoesEditar.houveAlteracao){

            $rootScope.saveSimplificadoJaExecutado = true;

            caracterizacaoService.saveSimplificado($scope.cadastro.caracterizacao)
                .then(function(response){
                    $scope.cadastro.verificacoesEditar.isEditar = false;
                    $scope.cadastro.verificacoesEditar.modalJaApareceu = false;

                    mensagem.success('Rascunho salvo com sucesso!' , {ttl: 15000});
                    _.extend($scope.cadastro.caracterizacao, response.data);
                    getValorParametros();
                    $rootScope.$broadcast('changeListaAtividadesCaracterizacao',$scope.cadastro.caracterizacao.atividadesCaracterizacao);
                    $rootScope.$broadcast('iniciacializaGeoComEmpreendimento');

                    return true;
                })
                .catch(function(response){
                    $scope.cadastro.verificacoesEditar.houveAlteracao = true;
                    if(response && response.data && response.data.texto) {
                        mensagem.error(response.data.texto, {ttl: 15000});
                    }
                    else {
                        mensagem.error('Falha ao salvar rascunho da solicitação.' , {ttl: 15000});
                    }
                    return false;
                })
                .finally(function() {
                    $rootScope.saveSimplificadoJaExecutado = false;
                });
            }

    }

};

exports.controllers.CaracterizacaoDLAAtividadeController = CaracterizacaoDLAAtividadeController;
