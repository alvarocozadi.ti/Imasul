var OutorgaService = function(request, config) {

    this.getOutorga = function(cpfCnpj) {

        return request.get(config.BASE_URL() + `outorga/get/${cpfCnpj}`);

    };

};

exports.services.OutorgaService = OutorgaService;