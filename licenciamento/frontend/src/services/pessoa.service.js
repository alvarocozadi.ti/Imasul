var PessoaService = function(request, config){

	this.byCpfCnpj = function(cpfCnpj) {

		return request.get(config.BASE_URL() + 'pessoas/cpfCnpj/' + cpfCnpj);

	};

	this.byCpf = function(cpf) {

		return request.get(config.BASE_URL() + 'pessoas/cpf/' + cpf);

	};	

	this.verificaCadastroSiriema = function(cpfCnpj){

		return request.get(config.BASE_URL() + 'pessoa/verificaCadastroSiriema/' + cpfCnpj);
	}; 

};

exports.services.PessoaService = PessoaService;