var EmpreendedorService = function(request, config){

	this.getRepresentantes = function(idPessoa) {

		return request.get(config.BASE_URL() + 'empreendedores/pessoas/' + idPessoa + '/representantes');

	};

	this.findByCpfCnpj = function(cpfCnpj) {

		return request.get(config.BASE_URL() + 'empreendedores/pessoas/cpfCnpj/' + cpfCnpj);

	};

	this.list = function(campoPesquisa, paginaAtual, itensPorPagina) {

		return request
			.get(config.BASE_URL() + "empreendedores", {
				pesquisa: campoPesquisa,
				numeroPagina: paginaAtual,
				qtdItensPorPagina: itensPorPagina
			});
	};

	this.buscarEmpreendedorCompleto= function(cpfCnpj) {

		return request.get(config.BASE_URL() + "empreendedor/completo/" + cpfCnpj);
	};

	this.delete = function(listaIds) {

		return request.delete(config.BASE_URL() + "empreendedor/" + listaIds);
	};

	this.buscarEmpreendimento = function(cpfCnpj) {

		return request.get(config.BASE_URL() + "empreendedor/empreendimentos/" + cpfCnpj);
	};

	this.forcarAtualizacaoDadosRedesim = function(cnpj) {

		return request.get(config.BASE_URL() + `empreeendedor/redesim/atualizado/${cnpj}`);
	};

};

exports.services.EmpreendedorService = EmpreendedorService;