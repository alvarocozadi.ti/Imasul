var EmpreendimentoService = function(request, config) {

	this.getEmpreendimentoPorId = function(idEmpreendimento) {

		return request.get(config.BASE_URL() + "empreendimento/"+idEmpreendimento);
	};

	this.list = function(campoPesquisa, paginaAtual, itensPorPagina) {

		return request
			.get(config.BASE_URL() + "empreendimentos", {
				pesquisa: campoPesquisa,
				numeroPagina: paginaAtual,
				qtdItensPorPagina: itensPorPagina
			});
	};
	
	this.listEmpreendimentosEmpreendedor = function(isListagem, cpfCnpjEmpreendedor, paginaAtual, itensPorPagina, pesquisa) {
		
		return request
			.get(config.BASE_URL() + "empreendimentos/listagem/" + cpfCnpjEmpreendedor, {
				listagem: isListagem,
				pesquisa: pesquisa,
				numeroPagina: paginaAtual,
				qtdItensPorPagina: itensPorPagina
			});
	};

	this.getPessoaPorCpfCnpj = function(cpfCnpj) {

		return request.get(config.BASE_URL() + "empreendimentos/pessoas/cpfCnpj/"+cpfCnpj);
	};

	this.save = function(empreendimento) {

		return request.post(config.BASE_URL() + "empreendimentos", empreendimento);

	};

	this.update = function(id, empreendimento) {

		return request.post(config.BASE_URL() + "empreendimentos/" + id + "/update", empreendimento);

	};

	this.buscarPessoa = function(id) {
		return request.get(config.BASE_URL() + "empreendimentos/pessoa/" + id);
	};


	this.buscarEmpreendimentoCompleto= function(id) {

		return request.get(config.BASE_URL() + "empreendimento/completo/" + id);
	};

	this.getEmpreendimentoToUpdate = function(id) {

		return request.get(config.BASE_URL() + "empreendimentos/" + id + "/findToUpdate");
	};

	this.buscarEmpreendimentoPorCpfCnpj = function(cpfsCnpjs) {

		return request.post(config.BASE_URL() + "empreendimento/cpfCnpj", cpfsCnpjs);
	};

	this.buscarEmpreendimentoOutroEmpreendedor = function(id){

		return request.get(config.BASE_URL() + "empreendimento/cpfCnpj/outro/" + id);

	};

	this.validaVinculoEmpreendedorPessoaDoEmpreendimento = function(cpfCnpj,cpfCnpjEmpdor){

		let cpfCnpjs = {
			cpfCnpjEmpreendimento: cpfCnpj,
			cpfCnpjEmpreendedor: cpfCnpjEmpdor
		};

		return request.post(config.BASE_URL() + "empreendimento/cpfCnpj/vinculos", cpfCnpjs);
	};

	this.delete = function(id) {

		return request.delete(config.BASE_URL() + "empreendimento/" + id);
	};

	this.deleteEmpreendimentos = function(listaIds){
		var data =  {
			ids : listaIds
		};
		return request.post(config.BASE_URL() + "empreendimentos/delete" , data);
	};

	this.getCadastrante = function(cpfCnpj) {

		return request.get(config.BASE_URL() + "empreendimentos/pessoa/cadastrante/cpfCnpj/"+cpfCnpj);
	};


};

exports.services.EmpreendimentoService = EmpreendimentoService;
