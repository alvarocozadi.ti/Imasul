var GeometriaService = function(request, config) {

    this.calcularArea = function(geometria) {

        return request.post(config.BASE_URL() + 'geometrias/area', geometria);

    };
    
};

exports.services.GeometriaService = GeometriaService;
