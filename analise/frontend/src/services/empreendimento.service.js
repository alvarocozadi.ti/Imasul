var EmpreendimentoService =  function (request, config) {

	this.getDadosGeoEmpreendimento = function(idEmpreendimento){

		return request.post(config.BASE_URL() + 'empreendimento/buscaDadosGeo/' + idEmpreendimento);

	};

};

exports.services.EmpreendimentoService = EmpreendimentoService;