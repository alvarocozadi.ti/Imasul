var ParecerChefeUnidadeService = function(request, config) {

	this.findParecerByIdHistoricoTramitacao = function(id) {

		return request
			.get(config.BASE_URL() + 'parecer/chefeUnidade/findParecerByIdHistoricoTramitacao/' + id);

	};

};

exports.services.ParecerChefeUnidadeService = ParecerChefeUnidadeService;
