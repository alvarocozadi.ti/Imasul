var ValidacaoAnaliseChefeUnidadeService = function(request,config) {

	this.concluirParecerTecnico = function(params){

		return request
			.post(config.BASE_URL() + 'parecer/chefeUnidade/concluirParecerTecnicoChefeUnidade', params);
	};

};

exports.services.ValidacaoAnaliseChefeUnidadeService = ValidacaoAnaliseChefeUnidadeService;