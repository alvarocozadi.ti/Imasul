var ParecerPresidenteService = function(request, config) {

    this.concluirParecerPresidente = function(params){

		return request
			.post(config.BASE_URL() + 'parecer/presidente/concluirParecerPresidente', params);
	};

	this.findParecerByIdHistoricoTramitacao = function(id) {

		return request
			.get(config.BASE_URL() + 'parecer/presidente/findParecerByIdHistoricoTramitacao/' + id);

	};			


	this.baixarLicenca = function(idCaracterizacao) {

		window.location.href = config.BASE_URL() + "downloadLicenca?idCaracterizacao=" + idCaracterizacao;

	};

	this.assinar = function(caracterizacao, key) {
		
		return request.post(config.BASE_URL() + 'parecer/presidente/assinar/' + key, caracterizacao);
	};

	this.emitirLicenca = function(idCaracterizacao) {
		
		return request.get(config.BASE_URL() + 'parecer/presidente/emitirLicenca/' + idCaracterizacao);
	};

};

exports.services.ParecerPresidenteService = ParecerPresidenteService;
