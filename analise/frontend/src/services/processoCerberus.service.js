var ProcessoCerberusService = function(request, config) {

	this.getProcessos = function(filtro) {

		return request
			.post(config.BASE_URL() + "processosCerberus", filtro);
	};

	this.getProcessosCount = function(filtro) {

		return request
			.post(config.BASE_URL() + "processosCerberus/count", filtro);
	};

	this.findProcessoById = function(idProcesso) {
		return request
			.get(config.BASE_URL() + "processosCerberus/findProcessoById/" + idProcesso);
	};

};

exports.services.ProcessoCerberusService = ProcessoCerberusService;
