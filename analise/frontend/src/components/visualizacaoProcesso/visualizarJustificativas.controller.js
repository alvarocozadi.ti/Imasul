var VisualizarJustificativasController = function ($uibModalInstance, parecer) {

    var visualizarJustificativasCtlr = this;

    visualizarJustificativasCtlr.parecer = parecer;
    visualizarJustificativasCtlr.labelParecer = '';
    visualizarJustificativasCtlr.resultadoAnalise = app.utils.TiposResultadoAnalise;

    var estadosDespacho = [

        //Deferido
        visualizarJustificativasCtlr.resultadoAnalise.DEFERIDO,
        visualizarJustificativasCtlr.resultadoAnalise.ANALISE_APROVADA,
        visualizarJustificativasCtlr.resultadoAnalise.PARECER_VALIDADO,

        //Indeferido
        visualizarJustificativasCtlr.resultadoAnalise.INDEFERIDO,
        visualizarJustificativasCtlr.resultadoAnalise.ANALISE_NAO_APROVADA,
        visualizarJustificativasCtlr.resultadoAnalise.PARECER_NAO_VALIDADO

    ];

    if (!visualizarJustificativasCtlr.parecer.tipoResultadoAnalise ||
        visualizarJustificativasCtlr.parecer.tipoResultadoAnalise === undefined ||
        estadosDespacho.includes(visualizarJustificativasCtlr.parecer.tipoResultadoAnalise.id)){

        visualizarJustificativasCtlr.labelParecer = 'Despacho';

    } else if (visualizarJustificativasCtlr.parecer.tipoResultadoAnalise.id === visualizarJustificativasCtlr.resultadoAnalise.EMITIR_NOTIFICACAO){

        visualizarJustificativasCtlr.labelParecer = 'Descrição da solicitação';

    } else if (visualizarJustificativasCtlr.parecer.tipoResultadoAnalise.id === visualizarJustificativasCtlr.resultadoAnalise.SOLICITAR_AJUSTES){

        visualizarJustificativasCtlr.labelParecer = 'Observações';

    } 

    visualizarJustificativasCtlr.fechar = function () {
        $uibModalInstance.dismiss('cancel');
    };

};

exports.controllers.VisualizarJustificativasController = VisualizarJustificativasController;
