var FiltroProcessosCerberus = {

	bindings: {
		paginacao: '=',
		atualizarLista: '=',
		atualizarPaginacao: '=',
		pesquisarAoInicializar: '<',
		onAfterUpdate: '=',
	},

	controller: function(mensagem, licencaEmitidaService, municipioService, atividadeService,
		$scope, $rootScope, processoCerberusService) {

		var ctrl = this;

		ctrl.openedAccordionPesquisaRapida = false;
		ctrl.openedAccordionPesquisaAvancada = false;

		ctrl.maxDataInicio = new Date();

		this.pesquisaAvancada = function(pagina){

			licencaEmitidaService.getLicencasEmitidasPesquisaAvancada(ctrl.filtro)
				.then(function(response){

					ctrl.atualizarLista(response.data);

					if (_.isFunction(ctrl.onAfterUpdate))
						ctrl.onAfterUpdate(ctrl.filtro);

				})
				.catch(function(response){
					if(!!response.data.texto)
						mensagem.warning(response.data.texto);
					else
						mensagem.error("Ocorreu um erro ao buscar a lista de licenças emitidas.");
				});

				licencaEmitidaService.getLicencasEmitidasPesquisaAvancadaCount(ctrl.filtro)
				.then(function(response){
					 ctrl.atualizarPaginacao(response.data, ctrl.filtro.paginaAtual);
				})
				.catch(function(response){
					if(!!response.data.texto)
						mensagem.warning(response.data.texto);
					else
						mensagem.error("Ocorreu um erro ao buscar a quantidade de processos.");
				});

			$rootScope.$broadcast('atualizarContagemProcessos');
		};

		this.pesquisar = function(pagina){

			if (ctrl.filtro.periodoInicial && ctrl.filtro.periodoInicial) {

				if(moment(ctrl.filtro.periodoInicial, 'DD/MM/YYYY').isAfter(moment())) {

					mensagem.warning("Data de início do período não pode ser posterior a data atual.");
					return;
				}

				var diff = moment(ctrl.filtro.periodoFinal, 'DD/MM/yyyy')
					.diff(moment(ctrl.filtro.periodoInicial, 'DD/MM/yyyy'), 'days');

				if (diff < 0) {
					mensagem.warning("O período inicial não pode ser maior que o período final.");
					return;
				}
			}

			ctrl.filtro.paginaAtual = pagina || ctrl.paginacao.paginaAtual;
			ctrl.filtro.itensPorPagina = ctrl.paginacao.itensPorPagina;

			var filtro = angular.copy(ctrl.filtro);
			processoCerberusService.getProcessos(filtro)
				.then(function(response){

					ctrl.atualizarLista(response.data);

					if (_.isFunction(ctrl.onAfterUpdate))
						ctrl.onAfterUpdate(filtro);

				})
				.catch(function(response){
					if(!!response.data)
						mensagem.warning(response.data);
					else
						mensagem.error("Ocorreu um erro ao buscar a lista de protocolos.");
				});

				processoCerberusService.getProcessosCount(filtro)
				.then(function(response){
					 ctrl.atualizarPaginacao(response.data, filtro.paginaAtual);
				})
				.catch(function(response){
					if(!!response.data.texto)
						mensagem.warning(response.data.texto);
					else
						mensagem.error("Ocorreu um erro ao buscar a quantidade de protocolos.");
				});

			$rootScope.$broadcast('atualizarContagemProcessos');
		};

		function setFiltrosPadrao(){

			ctrl.filtro = {};
			
		}

		$scope.$on('pesquisarProcessosCerberus', function(event){

			ctrl.pesquisar();
		});

		this.limparFiltros = function(){

			setFiltrosPadrao();

			this.pesquisar(1);
		};

		this.$postLink = function(){

			setFiltrosPadrao();

			municipioService.getMunicipiosByUf('MS').then(
				function(response){

					ctrl.municipios = response.data;
				})
				.catch(function(){
					mensagem.warning('Não foi possível obter a lista de municípios.');
				});

			atividadeService.getAtividades().then(
				function(response){

					ctrl.atividades = response.data;
				})
				.catch(function(){
					mensagem.warning('Não foi possível obter a lista de atividades.');
				});


			if (ctrl.pesquisarAoInicializar)
				ctrl.pesquisar(1);
			
		};

		$scope.$on('pesquisarLicencas', function(event){

			ctrl.pesquisar();
		});
	},

	templateUrl: 'components/filtroProcessosCerberus/filtroProcessosCerberus.html'

};

exports.directives.FiltroProcessosCerberus = FiltroProcessosCerberus;