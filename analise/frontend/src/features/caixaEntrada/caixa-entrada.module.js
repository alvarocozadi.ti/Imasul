var caixasEntrada = angular.module("caixasEntrada", ["ngRoute"]);

var utils = app.utils,
    controllers = app.controllers,
    directives = app.directives;

caixasEntrada.config(["$routeProvider", function($routeProvider) {
	
	$routeProvider
		.when("/caixa-entrada", {
			templateUrl: "features/caixaEntrada/caixa-entrada.html",
			controller: controllers.CaixaEntradaController,
			controllerAs: 'caixaEntrada'
		})
		.when("/shape-upload/:idProcesso", {
			templateUrl: "features/caixaEntrada/analistaTecnico/shape-upload.html",
			controller: controllers.CaixaEntradaController,
			controllerAs: 'caixaEntrada',

			resolve: {
				
				idProcesso: function($route) {
					return $route.current.params.idProcesso;
				}
			}
		})
		.otherwise({
			redirectTo: "/"
		});
}]);

caixasEntrada
	.controller('cxEntChefeUnidadeController', controllers.CxEntChefeUnidadeController)
	.controller('modalVincularConsultorController', controllers.ModalVincularConsultorController)
	.controller('cxEntAnalistaTecnicoController', controllers.CxEntAnalistaTecnicoController)
	.controller('cxEntGerenteTecnicoController', controllers.CxEntGerenteController)
	.controller('cxEntDiretorController', controllers.CxEntDiretorController)
	.controller('cxEntPresidenteController', controllers.CxEntPresidenteController);

caixasEntrada
	.component('filtroProcessos', directives.FiltroProcessos);
