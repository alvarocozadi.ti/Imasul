var DistribuirProtocoloController = function ($uibModalInstance, idProcesso, $rootScope,
                                            $location, mensagem, analistaService, processoService) {

    var distribuirProtocoloController = this;
    
    distribuirProtocoloController.analistasTecnicos = null;


    analistaService.getAnalistasTecnicoBySetor()
    .then(function(response){
        distribuirProtocoloController.analistasTecnicos = response.data;
    });


	distribuirProtocoloController.fechar = function () {
		$uibModalInstance.dismiss('cancel');
	};

	
	distribuirProtocoloController.cancelar= function() {
		$location.path('/caixa-entrada');
    };

    distribuirProtocoloController.concluir = function() {

        var params={
            usuarioAnalise: {id: distribuirProtocoloController.analistaTecnico.id},
            processo: {id: idProcesso},
            siglaSetor: $rootScope.usuarioSessao.usuarioEntradaUnica.setorSelecionado.sigla
        };       

        processoService.distribuirProcessos(params)
            .then(function(response){

                $rootScope.$broadcast('rootPesquisarProcessos');
                $rootScope.$broadcast('atualizarContagemProcessos');
                mensagem.success(response.data);
                $location.path('/caixa-entrada');
                $uibModalInstance.close();
                
        }).catch(function(response){
            mensagem.error(response.data.texto, {referenceId: 5});
        });

	};
	
};

exports.controllers.DistribuirProtocoloController = DistribuirProtocoloController;