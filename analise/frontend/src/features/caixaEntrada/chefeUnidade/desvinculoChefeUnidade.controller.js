var DesvinculoChefeUnidadeController = function ($uibModalInstance, processo, $location ,$scope,$rootScope, mensagem, desvinculoService,analistaService) {

	var desvinculoChefeUnidadeController = this;

	desvinculoChefeUnidadeController.processo = processo;
	desvinculoChefeUnidadeController.condicaoTramitacao = app.utils.CondicaoTramitacao;
	desvinculoChefeUnidadeController.pessoa = null;
	desvinculoChefeUnidadeController.desvinculo = null;
	desvinculoChefeUnidadeController.analistas = null;
	desvinculoChefeUnidadeController.analistaDestino = {};
	desvinculoChefeUnidadeController.tipoDesvinculo = null;

	function onInit() {

		if(desvinculoChefeUnidadeController.processo.idCondicaoTramitacao === desvinculoChefeUnidadeController.condicaoTramitacao.SOLICITACAO_DESVINCULO_PENDENTE_ANALISE_TECNICA){

			desvinculoChefeUnidadeController.buscarDesvinculoPeloProcessoTecnico();
			desvinculoChefeUnidadeController.buscarAnalistasTecnicoByIdProcesso();
			desvinculoChefeUnidadeController.tipoDesvinculo = false;
			
		}
	}

	desvinculoChefeUnidadeController.fechar = function () {
		$uibModalInstance.dismiss('cancel');
	};

	desvinculoChefeUnidadeController.cancelar= function() {
		$location.path('/caixa-entrada');
	};

	desvinculoChefeUnidadeController.concluir = function() {

		var params = desvinculoChefeUnidadeController.desvinculo;

		params.aprovada = desvinculoChefeUnidadeController.desvinculoAceito;
		params.respostaGerente = desvinculoChefeUnidadeController.respostaGerente;


		if (desvinculoChefeUnidadeController.tipoDesvinculo === false){

			if(desvinculoChefeUnidadeController.desvinculoAceito) {
				params.analistaTecnicoDestino = {};
				params.analistaTecnicoDestino.id = desvinculoChefeUnidadeController.analistaDestino.id;
			}

			desvinculoService.responderSolicitacaoDesvinculoAnaliseTecnica(params)
			.then(function(response){

				$rootScope.$broadcast('rootPesquisarProcessos');
				$rootScope.$broadcast('atualizarContagemProcessos');
				mensagem.success(response.data);
				$location.path('/caixa-entrada');
				$uibModalInstance.close();

			}).catch(function(response){
				mensagem.error(response.data.texto, {referenceId: 5});
			});
		}

	};

	desvinculoChefeUnidadeController.buscarDesvinculoPeloProcessoGeo = function() {
		desvinculoService.buscarDesvinculoPeloProcessoGeo(processo.idProcesso)
			.then(function(response) {
				desvinculoChefeUnidadeController.desvinculo = response.data;
			});
	};

	desvinculoChefeUnidadeController.buscarAnalistasGeoByIdProcesso = function() {
		analistaService.buscarAnalistasGeoByIdProcesso(processo.idProcesso)
			.then(function(response) {
				desvinculoChefeUnidadeController.analistas = response.data;
			});
	};

	desvinculoChefeUnidadeController.buscarDesvinculoPeloProcessoTecnico = function() {
		desvinculoService.buscarDesvinculoPeloProcessoTecnico(processo.idProcesso)
			.then(function(response) {
				desvinculoChefeUnidadeController.desvinculo = response.data;
			});
	};

	desvinculoChefeUnidadeController.buscarAnalistasTecnicoByIdProcesso = function() {
		analistaService.buscarAnalistasTecnicoByIdProcesso(processo.idProcesso)
			.then(function(response) {
				desvinculoChefeUnidadeController.analistas = response.data;
			});
	};

	desvinculoChefeUnidadeController.validarModalParaConcluir = function() {
		var valido = false;

		if(desvinculoChefeUnidadeController.desvinculoAceito){
			valido = desvinculoChefeUnidadeController.analistaDestino.id &&
					 desvinculoChefeUnidadeController.analistaDestino.id != "" &&
					 desvinculoChefeUnidadeController.respostaGerente;
		} else {
			valido = desvinculoChefeUnidadeController.desvinculoAceito != undefined &&
					desvinculoChefeUnidadeController.respostaGerente;
			
					desvinculoChefeUnidadeController.analistaDestino.id = undefined;
		}

		return valido;
	};

	onInit();
};

exports.controllers.DesvinculoChefeUnidadeController = DesvinculoChefeUnidadeController;