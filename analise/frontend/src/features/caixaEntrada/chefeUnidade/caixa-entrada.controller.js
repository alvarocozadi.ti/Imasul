var CxEntChefeUnidadeController = function($scope, config, $rootScope, $uibModal ,processoService, analiseTecnicaService, $location) {

	$rootScope.tituloPagina = app.TITULOS_PAGINA.CAIXA_ENTRADA_CHEFE_UNIDADE;

	var cxEntChefeUnidade = this;

	cxEntChefeUnidade.atualizarListaProcessos = atualizarListaProcessos;
	cxEntChefeUnidade.atualizarPaginacao = atualizarPaginacao;
	cxEntChefeUnidade.selecionarTodosProcessos = selecionarTodosProcessos;
	cxEntChefeUnidade.onPaginaAlterada = onPaginaAlterada;
	cxEntChefeUnidade.visualizarProcesso = visualizarProcesso;
	cxEntChefeUnidade.legendas = app.utils.CondicaoTramitacao;
	cxEntChefeUnidade.processos = [];
	cxEntChefeUnidade.paginacao = new app.utils.Paginacao(config.QTDE_ITENS_POR_PAGINA);
	cxEntChefeUnidade.PrazoMinimoAvisoAnalise = app.utils.PrazoMinimoAvisoAnalise;
	cxEntChefeUnidade.PrazoAnalise = app.utils.PrazoAnalise;
	cxEntChefeUnidade.dateUtil = app.utils.DateUtil;
	cxEntChefeUnidade.disabledFields = _.concat($scope.caixaEntrada.disabledFields, app.DISABLED_FILTER_FIELDS.GERENCIA, app.DISABLED_FILTER_FIELDS.ANALISTA_GEO);
	cxEntChefeUnidade.condicaoTramitacao = app.utils.CondicaoTramitacao;

	cxEntChefeUnidade.distribuirProtocolo = distribuirProtocolo;

	function atualizarListaProcessos(processos) {

		cxEntChefeUnidade.processos = processos;
	}

	function atualizarPaginacao(totalItens, paginaAtual) {

		cxEntChefeUnidade.paginacao.update(totalItens, paginaAtual);
	}

	function onPaginaAlterada(){

		$scope.$broadcast('pesquisarProcessos');
	}

	function selecionarTodosProcessos() {

		_.each(cxEntChefeUnidade.processos, function(processo){
			processo.selecionado = cxEntChefeUnidade.todosProcessosSelecionados;
		});
	}

	cxEntChefeUnidade.verificarSolicitacaoDesvinculo = function(processo) {
		
		return processo.idCondicaoTramitacao === app.utils.CondicaoTramitacao.SOLICITACAO_DESVINCULO_PENDENTE_ANALISE_TECNICA;
	
	};

	cxEntChefeUnidade.atenderSolicitacaoDesvinculo =  function(processo){

		var modalInstance = $uibModal.open({
			controller: 'desvinculoChefeUnidadeController',
			controllerAs: 'desvinculoChefeUnidadeCtrl',
			templateUrl: 'features/caixaEntrada/chefeUnidade/modalDesvinculo.html',
			backdrop: 'static',
			size: 'lg',
			resolve: {

				processo: function(){
					return processo;
				}
			}
			
		});
	};

	function visualizarProcesso(processo) {
		return processoService.visualizarProcesso(processo);
	}

	cxEntChefeUnidade.verificarStatusTecnico = function(processo) {
		
		return processo.idCondicaoTramitacao === app.utils.CondicaoTramitacao.SOLICITACAO_DESVINCULO_PENDENTE_ANALISE_TECNICA || 
			processo.idCondicaoTramitacao !== cxEntChefeUnidade.legendas.AGUARDANDO_VALIDACAO_TECNICA_PELO_GERENTE; 
	
	};

	cxEntChefeUnidade.verificarAguardandoDistribuicao = function(processo) {

		return processo.idCondicaoTramitacao === cxEntChefeUnidade.legendas.AGUARDANDO_DISTRIBUICAO_CHEFE_UNIDADE;

	};

	cxEntChefeUnidade.iniciarAnaliseGerente = function(idAnalise, idAnaliseTecnica) {

		analiseTecnicaService.iniciarAnaliseTecnicaChefeUnidade({ id : idAnaliseTecnica })
			.then(function(response){

				$rootScope.tituloPagina = app.TITULOS_PAGINA.EM_VALIDACAO_PELO_CHEFE_UNIDADE;
				$location.path('/analise-tecnica-chefe-unidade/' + idAnalise.toString());
				$rootScope.$broadcast('atualizarContagemProcessos');
			
			}, function(error){
				mensagem.error(error.data.texto);
		});
	};

	function distribuirProtocolo(processo) {

		$uibModal.open({
			controller: 'distribuirProtocoloController',
			controllerAs: 'distribuirProtocoloCtrl',
			backdrop: 'static',
			templateUrl: 'features/caixaEntrada/chefeUnidade/modalDistribuirProtocolo.html',
			size: 'md',
			resolve: {

				idProcesso: function(){
					return processo.idProcesso;
				}
			}

		});
	}


};

exports.controllers.CxEntChefeUnidadeController = CxEntChefeUnidadeController;
