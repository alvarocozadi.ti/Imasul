var ConsultarProcessoCerberusController = function($scope, $location,
										config,
										$rootScope) {

	$rootScope.tituloPagina = app.TITULOS_PAGINA.CONSULTAR_PROCESSO_PROTOCOLO_CERBERUS;

	var consultarProcessoCerberus = this;

	consultarProcessoCerberus.usuarioLogadoCodigoPerfil = $rootScope.usuarioSessao.usuarioEntradaUnica.perfilSelecionado.codigo;
	consultarProcessoCerberus.perfis = app.utils.Perfis;
	consultarProcessoCerberus.atualizarListaProcessosCerberus = atualizarListaProcessosCerberus;
	consultarProcessoCerberus.atualizarPaginacao = atualizarPaginacao;
	consultarProcessoCerberus.selecionarTodosProcessos = selecionarTodosProcessos;
	consultarProcessoCerberus.onPaginaAlterada = onPaginaAlterada;
	consultarProcessoCerberus.visualizarProcesso = visualizarProcesso;

	consultarProcessoCerberus.condicaoTramitacao = app.utils.CondicaoTramitacao;
	consultarProcessoCerberus.processos = [];
	consultarProcessoCerberus.paginacao = new app.utils.Paginacao(config.QTDE_ITENS_POR_PAGINA);
	consultarProcessoCerberus.PrazoMinimoAvisoAnalise = app.utils.PrazoMinimoAvisoAnalise;
	consultarProcessoCerberus.dateUtil = app.utils.DateUtil;
	consultarProcessoCerberus.getDiasRestantes = getDiasRestantes;
	
	consultarProcessoCerberus.statusCaracterizacao = app.utils.StatusCaracterizacao;

	function atualizarListaProcessosCerberus(processos) {

		consultarProcessoCerberus.processos = processos;

	}

	function atualizarPaginacao(totalItens, paginaAtual) {

		consultarProcessoCerberus.paginacao.update(totalItens, paginaAtual);
	}

	function onPaginaAlterada(){

		$scope.$broadcast('pesquisarProcessosCerberus');
	}

	function selecionarTodosProcessos() {

		_.each(consultarProcessoCerberus.processos, function(processo){

			processo.selecionado = consultarProcessoCerberus.todosProcessosSelecionados;
		});
	}

	function visualizarProcesso(idProcesso) {

		$rootScope.tituloPagina = app.TITULOS_PAGINA.VISUALIZAR_PROCESSO_CERBERUS;
		$location.path('/visualizar-processo-cerberus/' + idProcesso.toString());
		
	}

	function getDiasRestantes(processo, dataVencimento, dataConclusao) {

		if(processo[dataConclusao])			
			return 'Concluída em ' + processo[dataConclusao].split(' ')[0];	

		else if(processo[dataVencimento])
			return consultarProcessoCerberus.dateUtil.getDiasRestantes(processo[dataVencimento]);
		
		else
			return '-';
	}

};

exports.controllers.ConsultarProcessoCerberusController = ConsultarProcessoCerberusController;