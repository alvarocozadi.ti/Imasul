var visualizarProcessoCerberus = angular.module('visualizarProcessoCerberus',['ngRoute']);

var utils = app.utils,
    controllers = app.controllers,
    directives = app.directives;

    visualizarProcessoCerberus.config(['$routeProvider', function($routeProvider){

	$routeProvider

		.when('/visualizar-processo-cerberus/:idProcesso', {
			templateUrl: 'features/visualizarProcessoCerberus/visualizar/visualizar-processo-cerberus.html',
			controller: controllers.VisualizarProcessoCerberusController,
			controllerAs: 'visualizarProcessoCerberus'
		})
		.otherwise({
			redirectTo: '/'
		});    

}]);

visualizarProcessoCerberus
	.controller('visualizarProcessoCerberusController', controllers.VisualizarProcessoCerberusController);
