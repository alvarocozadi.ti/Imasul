var VisualizarProcessoCerberusController = function(processoCerberusService, $routeParams,
                                                 $location) {

    var visualizarProcessoCerberus = this;

    visualizarProcessoCerberus.init = init;
    visualizarProcessoCerberus.titulo = app.TITULOS_PAGINA.VISUALIZAR_PROCESSO_CERBERUS;  
    visualizarProcessoCerberus.tiposResultadoAnalise = app.utils.TiposResultadoAnalise;
    visualizarProcessoCerberus.tipoDocumento =  app.utils.TiposDocumentosAnalise;
    visualizarProcessoCerberus.dadosProcesso = null;
    visualizarProcessoCerberus.dadosCliente = null;
    visualizarProcessoCerberus.dateUtil = app.utils.DateUtil;
    visualizarProcessoCerberus.latitude = '';
    visualizarProcessoCerberus.longitude = '';


    function init() {
        
		processoCerberusService.findProcessoById(parseInt($routeParams.idProcesso))
		.then(function(response){

            visualizarProcessoCerberus.dadosProcesso = response.data.processoCerberus;
            visualizarProcessoCerberus.dadosCliente = response.data.cliente;

            if ( visualizarProcessoCerberus.dadosProcesso.latG !== '' &&  visualizarProcessoCerberus.dadosProcesso.latG !== null &&
            visualizarProcessoCerberus.dadosProcesso.latM !== '' &&  visualizarProcessoCerberus.dadosProcesso.latM !== null &&
            visualizarProcessoCerberus.dadosProcesso.latS !== '' &&  visualizarProcessoCerberus.dadosProcesso.latS !== null )
                visualizarProcessoCerberus.latitude = visualizarProcessoCerberus.dadosProcesso.latG + "º"+
                visualizarProcessoCerberus.dadosProcesso.latM + "'" + 
                visualizarProcessoCerberus.dadosProcesso.latS + "''";

            else
                visualizarProcessoCerberus.latitude = null;
            

            if ( visualizarProcessoCerberus.dadosProcesso.longG !== '' &&  visualizarProcessoCerberus.dadosProcesso.longG !== null &&
            visualizarProcessoCerberus.dadosProcesso.longM !== '' &&  visualizarProcessoCerberus.dadosProcesso.longM !== null &&
            visualizarProcessoCerberus.dadosProcesso.longS !== '' &&  visualizarProcessoCerberus.dadosProcesso.longS !== null )
                visualizarProcessoCerberus.longitude = visualizarProcessoCerberus.dadosProcesso.longG + "º"+
                visualizarProcessoCerberus.dadosProcesso.longM + "'" + 
                visualizarProcessoCerberus.dadosProcesso.longS + "''";

            else
                visualizarProcessoCerberus.longitude = null;
            




		}, function(error){
			mensagem.error(error.data.texto);
		});
	}

    visualizarProcessoCerberus.voltar = function() {

        $location.path("/consultar-processo-cerberus");
    };

};

exports.controllers.VisualizarProcessoCerberusController = VisualizarProcessoCerberusController;
