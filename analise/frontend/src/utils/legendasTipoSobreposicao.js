var LegendasTipoSobreposicao = {

	AREA_INFLUENCIA_UC: 'influencia_uc',
	AREA_USO_RESTRITO_PANTANAL: 'uso_restrito_pantanal',
	AREAS_PRIORITARIAS: 'areas_prioritarias_ma',
	BACIA_FORMOSO_E_PRATA: 'bacia_formoso_prata',
	BACIA_PARAGUAIA: 'bacia_paraguaia',
	BACIA_PARANA: 'bacia_parana',
	BIOMAMATAATLANTICA: 'mata_atlantica',
	BIOMAS: 'biomas',
	CORREDORES_ECOLOGICOS: 'corredor_ecologico',
	ENTORNO_DO_TAQUARI: 'entorno_taquari',
	GEOLOGIA_2006: 'geologia_2006',
	GRADE_CBERS_CCD: 'grade_cbers',
	GRADE_DAS_CARTAS_1_PARA_100_MIL: 'grade_cbers',
	GRADE_DAS_CARTAS_1_PARA_250_MIL: 'grade_cbers',
	MACROZONEAMENTO: 'macrozoneamento',
	MACROZONEAMENTO_SOLOS: 'macrozoneamento_solos',
	MACROZONEAMENTO_SOLOS_AMOSTRAS: 'macrozoneamento_amostra',
	RIOS_DO_DOMINIO_DO_ESTADO_DE_MS_ANA: 'rio_dominio_ana',
	SITIOS_ARQUEOLOGICOS: 'sitios_arqueologicos',
	TERRAS_INDIGENAS_EM_ESTUDO: 'ti_estudo',
	TERRAS_INDIGENAS_FUNAI_2015: 'terra_indigena',
	UCS_MS_MOSAICO: 'ucs_ms',
	UNIDADE_DE_PLANEJAMENTO_E_GERENCIAMENTO: 'unidade_planejamento_gerenciamento',
	UNIDADES_HIDROGELOLOGICAS: 'unidade_hidrogeologica',
	ZEE_MS: 'zee_ms',
	ZONA_AMORT_UC_MS: 'zaa',
	ZONA_AMORT_UC_MS_CONAMA_2KM: 'zaa_2km',
	ZONA_AMORT_UC_MS_CONAMA_3KM: 'zaa_3km'

};

exports.utils.LegendasTipoSobreposicao = LegendasTipoSobreposicao;
