var InconsistenciaTecnica = {

    TIPO_LICENCA: 'TIPO_LICENCA',
	ATIVIDADE: 'ATIVIDADE',
    PARAMETRO: 'PARAMETRO',
    QUESTIONARIO: 'QUESTIONARIO',
    DOCUMENTO_ADMINISTRATIVO: 'DOCUMENTO_ADMINISTRATIVO',
    OUTORGA: 'OUTORGA',
    DOCUMENTO_TECNICO_AMBIENTAL: 'DOCUMENTO_TECNICO_AMBIENTAL'
    
};

exports.utils.InconsistenciaTecnica = InconsistenciaTecnica;