var CamadaSobreposicao = {

    'AREA_INFLUENCIA_UC' : 'base_referencia_ms:area_influencia_uc',
    'AREA_USO_RESTRITO_PANTANAL' : 'base_referencia_ms:area_uso_restrito_pantanal',
    'AREAS_PRIORITARIAS' : 'base_referencia_ms:areas_prioritarias',
    'BACIA_FORMOSO_E_PRATA' : 'base_referencia_ms:bacia_formoso_e_prata',
    'BACIA_PARAGUAIA' : 'base_referencia_ms:bacia_paraguaia',
    'BACIA_PARANA' : 'base_referencia_ms:bacia_parana',
    'BIOMAMATAATLANTICA' : 'base_referencia_ms:biomamataatlantica',
    'BIOMAS' : 'base_referencia_ms:biomas',
    'CORREDORES_ECOLOGICOS' : 'base_referencia_ms:corredores_ecologicos',
    'ENTORNO_DO_TAQUARI' : 'base_referencia_ms:entorno_do_taquari',
    'GEOLOGIA_2006' : 'base_referencia_ms:geologia_2006',
    'GRADE_CBERS_CCD' : 'base_referencia_ms:grade_cbers_ccd',
    'GRADE_DAS_CARTAS_1_PARA_100_MIL' : 'base_referencia_ms:grade_das_cartas_1_para_100_mil',
    'GRADE_DAS_CARTAS_1_PARA_250_MIL' : 'base_referencia_ms:grade_das_cartas_1_para_250_mil',
    'MACROZONEAMENTO' : 'base_referencia_ms:macrozoneamento',
    'MACROZONEAMENTO_SOLOS' : 'base_referencia_ms:macrozoneamento_solos',
    'MACROZONEAMENTO_SOLOS_AMOSTRAS' : 'base_referencia_ms:macrozoneamento_solos_amostras',
    'RIOS_DO_DOMINIO_DO_ESTADO_DE_MS_ANA' : 'base_referencia_ms:rios_do_dominio_do_estado_de_ms_ana',
    'SITIOS_ARQUEOLOGICOS' : 'base_referencia_ms:sitios_arqueologicos',
    'TERRAS_INDIGENAS_EM_ESTUDO' : 'base_referencia_ms:terras_indigenas_em_estudo',
    'TERRAS_INDIGENAS_FUNAI_2015' : 'base_referencia_ms:terras_indigenas_funai_2015',
    'UCS_MS_MOSAICO' : 'base_referencia_ms:ucs_ms_mosaico',
    'UNIDADE_DE_PLANEJAMENTO_E_GERENCIAMENTO' : 'base_referencia_ms:unidade_de_planejamento_e_gerenciamento',
    'UNIDADES_HIDROGELOLOGICAS' : 'base_referencia_ms:unidades_hidrogelologicas',
    'ZEE_MS' : 'base_referencia_ms:zee_ms',
    'ZONA_AMORT_UC_MS' : 'base_referencia_ms:zona_amort_uc_ms',
    'ZONA_AMORT_UC_MS_CONAMA_2KM' : 'base_referencia_ms:zona_amort_uc_ms_conama_2km',
    'ZONA_AMORT_UC_MS_CONAMA_3KM' : 'base_referencia_ms:zona_amort_uc_ms_conama_3km',


    
};

exports.utils.CamadaSobreposicao = CamadaSobreposicao;