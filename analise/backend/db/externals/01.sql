# --- !Ups
-- imasul
--Aumento do tamanho permitido para sigla
ALTER TABLE portal_seguranca.setor ALTER COLUMN sigla TYPE varchar(25);

-- Inserção dos novos setores do MS
INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES ('GLA/AGROINFRA', 'GLA/AGROINFRA', 3,'01-12-2020', false, true);
INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES ('GLA/MINEROTUR', 'GLA/MINEROTUR', 3,'01-12-2020', false, true);
INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES ('GLA/UNIIND', 'GLA/UNIIND', 3,'01-12-2020', false, true);
INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES ('GLA/UNISAN', 'GLA/UNISAN', 3,'01-12-2020', false, true);
INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES ('GPF/UNIFAUNA', 'GPF/UNIFAUNA', 3,'01-12-2020', false, true);

-- Inserção do setor GLA/AGROINFRA para todos setores do Análise
INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA'));

-- Inserção do setor GLA/MINEROTUR para todos setores do Análise
INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR'));

-- Inserção do setor GLA/UNIIND para todos setores do Análise
INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND'));

-- Inserção do setor GLA/UNISAN para todos setores do Análise
INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN'));

-- Inserção do setor GLA/UNIFAUNA para todos setores do Análise
INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

-- Externals que vieram do estado AM
--54
-- Inserir permissão de visualizar notificações
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
VALUES ('ANL_VISUALIZAR_NOTIFICACAO', now(), 'Visualizar Notificação', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

--58
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) VALUES 
('ANL_VISUALIZAR_PROCESSO', now(), 'Visualizar processo', (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =
(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_PROCESSO'));

INSERT INTO portal_seguranca.permissao_perfil VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =
(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_PROCESSO'));

--60
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_SOLICITAR_DESVINCULO_GEO', now(), 'Solicitar desvínculo análise GEO',(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_SOLICITAR_DESVINCULO_TECNICO', now(), 'Solicitar desvínculo análise Técnica',(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_SOLICITAR_DESVINCULO_TECNICO'));

INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('GER_RESPONDER_SOLICITACAO_DESVINCULO', now(), 'Responder solicitação de desvínculo',(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));                                                     

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'GER_RESPONDER_SOLICITACAO_DESVINCULO'));

--65
-- Salvar inconsistencia técnica
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_SALVAR_INCONSISTENCIA_TECNICA', now(), 'Salvar inconsistência técnica', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

-- Salvar inconsistencia GEO
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_SALVAR_INCONSISTENCIA_GEO', now(), 'Salvar inconsistência GEO', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
    (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

-- Excluir inconsistencia técnica
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_EXCLUIR_INCONSISTENCIA_TECNICA', now(), 'Excluir inconsistência técnica', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

-- Excluir inconsistencia GEO
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
VALUES ('ANL_EXCLUIR_INCONSISTENCIA_GEO', now(), 'Excluir inconsistencia Geo', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
    (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--66
-- Buscar inconsistencia GEO
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_BUSCAR_INCONSISTENCIA_GEO', now(), 'Buscar inconsistência GEO', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
    (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));


-- Buscar inconsistencia técnica
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_BUSCAR_INCONSISTENCIA_TECNICA', now(), 'Buscar inconsistência técnica', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));


INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--67
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_NOTIFICACAO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));


--68
-- Visualizar questionário
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_VISUALIZAR_QUESTIONARIO', now(), 'Visualizar Questionário', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_BAIXAR_DOCUMENTO', now(), 'Baixar Documentos', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_QUESTIONARIO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Diretor' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--76
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
    ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' AND id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
     (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_NOTIFICACAO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));


--79
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA', now(), 'Baixar Documento Relatório Técnico Vistoria',(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA'));

--80
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo)	VALUES 
	('VISUALIZAR_PROTOCOLO', now(), 'Visualizar protocolo', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));
	

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
	((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
	((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));


--82
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_BAIXAR_DOCUMENTO_MINUTA', now(), 'Baixar Documento Minuta',(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA'));


--86
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
 ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
 (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA' AND p.id_modulo =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--87
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) 
	VALUES ('ANL_INICIAR_PARECER_DIRETOR', now(), 'Iniciar parecer pelo diretor', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) 
	VALUES ((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Diretor' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_INICIAR_PARECER_DIRETOR' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--88
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
 ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'DIRETOR' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
  (SELECT p.id  FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
 ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'DIRETOR' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
 (SELECT p.id  FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));


--90
INSERT INTO portal_seguranca.permissao (codigo, data_cadastro, nome, id_modulo) VALUES 
	('ANL_INICIAR_PARECER_PRESIDENTE', now(), 'Iniciar parecer do presidente', (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));


INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
	((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')), 
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_INICIAR_PARECER_PRESIDENTE' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));


--91
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
	((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Diretor' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));
	
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES 
	((SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
	(SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--93
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'PRESIDENTE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
        (SELECT p.id  FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));


--95
UPDATE portal_seguranca.perfil SET nome = 'Gerente TÉCNICO' WHERE codigo = 'GERENTE' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');
UPDATE portal_seguranca.perfil SET nome = 'Diretor TÉCNICO' WHERE codigo = 'DIRETOR' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');
UPDATE portal_seguranca.perfil SET nome = 'Diretor PRESIDENTE' WHERE codigo = 'PRESIDENTE' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');


INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'GER_RESPONDER_SOLICITACAO_DESVINCULO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));


# --- !Downs

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'GER_RESPONDER_SOLICITACAO_DESVINCULO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));


--95
UPDATE portal_seguranca.perfil SET nome = 'Gerente' WHERE codigo = 'GERENTE' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');
UPDATE portal_seguranca.perfil SET nome = 'Diretor' WHERE codigo = 'DIRETOR' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');
UPDATE portal_seguranca.perfil SET nome = 'Presidente' WHERE codigo = 'PRESIDENTE' AND id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL');

--93
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'PRESIDENTE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id  FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

--91
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Diretor' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) 
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));


DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

--90
DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_INICIAR_PARECER_PRESIDENTE' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));
 
DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_INICIAR_PARECER_PRESIDENTE' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL');

--88
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'DIRETOR' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA');


DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'DIRETOR' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA');

--87

DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.nome = 'Diretor' AND 
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_INICIAR_PARECER_DIRETOR');

DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_INICIAR_PARECER_DIRETOR';

--86
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA');


DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA');

--82
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA');

DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND nome = 'Baixar Documento Minuta' AND id_modulo =  (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'); 

--80
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND 
id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND 
id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));


DELETE FROM portal_seguranca.permissao WHERE codigo = 'VISUALIZAR_PROTOCOLO';

--79
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND
id_modulo_pertencente = (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND 
id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA');

DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA' AND nome = 'Baixar Documento Relatório Técnico Vistoria' AND id_modulo =  (SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL'); 

--76
DELETE FROM portal_seguranca.permissao_perfil WHERE 
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' AND 
id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND 
id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_NOTIFICACAO' AND 
id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));


--68
DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT id FROM portal_seguranca.perfil WHERE nome = 'Diretor' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND id_permissao =( SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO');

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Presidente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' );

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' );

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BAIXAR_DOCUMENTO' );

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_QUESTIONARIO' );


DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_BAIXAR_DOCUMENTO' AND nome='Baixar Documentos' ;

DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_VISUALIZAR_QUESTIONARIO' AND nome='Visualizar Questionário' ;

--67
DELETE FROM portal_seguranca.permissao_perfil 
WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) 
AND id_permissao in (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_VISUALIZAR_NOTIFICACAO');

--66
DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')) 
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Gerente' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA';
DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_BUSCAR_INCONSISTENCIA_GEO';


--65

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_TECNICA' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE nome = 'Analista TÉCNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
AND id_permissao = (SELECT id FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_GEO' AND id_modulo = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'));

DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_GEO';
DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_EXCLUIR_INCONSISTENCIA_TECNICA';
DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_GEO';
DELETE FROM portal_seguranca.permissao WHERE codigo = 'ANL_SALVAR_INCONSISTENCIA_TECNICA';


--60
DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND id_permissao=(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'GER_RESPONDER_SOLICITACAO_DESVINCULO');

DELETE FROM portal_seguranca.permissao WHERE codigo='GER_RESPONDER_SOLICITACAO_DESVINCULO' AND nome='Responder solicitação de desvínculo' ;

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND id_permissao=(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_SOLICITAR_DESVINCULO_TECNICO');

DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_SOLICITAR_DESVINCULO_TECNICO' AND nome='Solicitar desvínculo análise Técnica';

DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_SOLICITAR_DESVINCULO_GEO' AND nome='Solicitar desvínculo análise GEO';


--58
DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'ANALISTA_TECNICO' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND id_permissao=(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_PROCESSO');

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil =(SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'GERENTE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')) AND id_permissao=(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_PROCESSO');

DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_VISUALIZAR_PROCESSO';

--54
-- Deleta permissão de visualizar notificações
DELETE FROM portal_seguranca.permissao WHERE codigo='ANL_VISUALIZAR_NOTIFICACAO';

--
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA');

DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR');

DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND');

DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN');

DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'CHEFE_UNIDADE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'ANALISTA_TECNICO' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'GERENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'DIRETOR' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA');
DELETE FROM portal_seguranca.perfil_setor WHERE id_perfil = 
(SELECT id FROM portal_seguranca.perfil where codigo = 'PRESIDENTE' and id_modulo_pertencente = (SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL'))
and id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA');


DELETE FROM portal_seguranca.usuario_setor WHERE id_setor IN (
	SELECT id FROM portal_seguranca.setor WHERE 
	sigla IN ('GLA/AGROINFRA','GLA/MINEROTUR','GLA/UNIIND','GLA/UNISAN','GPF/UNIFAUNA')
);

DELETE FROM portal_seguranca.setor WHERE nome = 'GLA/AGROINFRA' and sigla = 'GLA/AGROINFRA' and tipo_setor = 3 and data_cadastro = '01-12-2020' and removido = false and ativo = true;
DELETE FROM portal_seguranca.setor WHERE nome = 'GLA/MINEROTUR' and sigla = 'GLA/MINEROTUR' and tipo_setor = 3 and data_cadastro = '01-12-2020' and removido = false and ativo = true;
DELETE FROM portal_seguranca.setor WHERE nome = 'GLA/UNIIND'  	and sigla = 'GLA/UNIIND'   	and tipo_setor = 3 and data_cadastro = '01-12-2020' and removido = false and ativo = true;
DELETE FROM portal_seguranca.setor WHERE nome = 'GLA/UNISAN'  	and sigla = 'GLA/UNISAN'   	and tipo_setor = 3 and data_cadastro = '01-12-2020' and removido = false and ativo = true;
DELETE FROM portal_seguranca.setor WHERE nome = 'GPF/UNIFAUNA' 	and sigla = 'GPF/UNIFAUNA'  and tipo_setor = 3 and data_cadastro = '01-12-2020' and removido = false and ativo = true;



ALTER TABLE portal_seguranca.setor ALTER COLUMN sigla TYPE varchar(10);