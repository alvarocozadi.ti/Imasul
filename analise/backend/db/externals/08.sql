# --- !Ups

-- permissao para o chefe de unidade ver a notificacao
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_NOTIFICACAO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

-- permissao para o chefe de unidade buscar inconsistencias
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

-- permissao para o chefe de unidade baixar documentos
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
	VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
		(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

--- permissao para o chefe de unidade baixar rtv
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES
 ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
 (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA' AND p.id_modulo =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

--- permissao para o chefe de unidade baixar minuta
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES
((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')),
(SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT id FROM portal_seguranca.modulo WHERE sigla = 'MAL')));

# --- !Downs

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VISUALIZAR_NOTIFICACAO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BUSCAR_INCONSISTENCIA_TECNICA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_RELATORIO_TECNICO_VISTORIA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_BAIXAR_DOCUMENTO_MINUTA' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));
