# --- !Ups

-- inicializar validacao presidente
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise do presidente'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente'));

-- validar com o presidente
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Aprovar solicitação de licença'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada'));

-- invalidar com o presidente
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Negar solicitação de licença'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença negada'));


# --- !Downs

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Negar solicitação de licença') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença negada');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Aprovar solicitação de licença') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise do presidente') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo presidente');


