# --- !Ups

INSERT INTO tramitacao.acao(id_acao, tx_descricao, fl_ativo, fl_tramitavel) VALUES
(82, 'Iniciar análise técnica gerente tecnico', 1, 1);

INSERT INTO tramitacao.acao(id_acao,tx_descricao,fl_ativo,fl_tramitavel) VALUES
(83,'Validar parecer técnico gerente técnico', 1, 1);

INSERT INTO tramitacao.acao(id_acao,tx_descricao,fl_ativo,fl_tramitavel) VALUES
(84,'Invalidar parecer técnico gerente técnico', 1, 1);

-- transicao para o gerente iniciar validacao
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica gerente tecnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo gerente técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico'));

-- transicao para o gerente validar a analise
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Validar parecer técnico gerente técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria'));

-- transicao para o gerente invalidar a analise
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Invalidar parecer técnico gerente técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria'));

# --- !Downs

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Invalidar parecer técnico gerente técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Validar parecer técnico gerente técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica gerente tecnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo gerente técnico') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo gerente técnico');

DELETE FROM tramitacao.acao where tx_descricao = 'Invalidar parecer técnico gerente técnico';
DELETE FROM tramitacao.acao where tx_descricao = 'Validar parecer técnico gerente técnico';
eDELETE FROM tramitacao.acao where tx_descricao = 'Iniciar análise técnica gerente tecnico';
