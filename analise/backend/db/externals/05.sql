# --- !Ups

-- Atualizando status para o chefe de unidade
UPDATE tramitacao.acao SET tx_descricao = 'Iniciar análise técnica chefe de unidade' WHERE tx_descricao = 'Iniciar análise técnica gerente técnico';

-- Acao apra iniciar validacao da analise tecnica pelo chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade'));


# --- !Downs

UPDATE tramitacao.acao SET tx_descricao = 'Iniciar análise técnica gerente técnico' WHERE tx_descricao = 'Iniciar análise técnica chefe de unidade';

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica chefe de unidade') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade');
