# --- !Ups

-- iniciar uma validacao com o diretor
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise pelo diretor técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico'));

-- transicao para o diretor validar
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Validar análise pelo diretor técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente'));

-- transicao para o diretor invalidar
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Invalidar análise pelo diretor técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente'));


# --- !Downs

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise pelo diretor técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação pela diretoria') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Validar análise pelo diretor técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Invalidar análise pelo diretor técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo diretor técnico') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando assinatura do presidente');


