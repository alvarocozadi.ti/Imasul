# --- !Ups

--- transicao para o presidente cancelar uma licenca
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Cancelar protocolo'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Cancelado'));
    
--- transicao para o presidente suspender uma licenca
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Suspender protocolo'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Suspenso'));


# --- !Downs

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Suspender protocolo') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Suspenso');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Cancelar protocolo') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação da licença aprovada') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Cancelado');

