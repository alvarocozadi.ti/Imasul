# --- !Ups

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
        (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_INICIAR_PARECER_TECNICO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao)
    VALUES ((SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')),
        (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VALIDAR_PARECERES' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

# --- !Downs

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_INICIAR_PARECER_TECNICO' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));

DELETE FROM portal_seguranca.permissao_perfil WHERE
id_perfil = (SELECT p.id FROM portal_seguranca.perfil p WHERE p.codigo = 'CHEFE_UNIDADE' AND p.id_modulo_pertencente =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')
AND id_permissao = (SELECT p.id FROM portal_seguranca.permissao p WHERE p.codigo = 'ANL_VALIDAR_PARECERES' AND p.id_modulo =(SELECT m.id FROM portal_seguranca.modulo m WHERE m.sigla = 'MAL')));
