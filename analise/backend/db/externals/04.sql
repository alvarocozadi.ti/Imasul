# --- !Ups

DELETE FROM tramitacao.transicao;
UPDATE tramitacao.condicao SET id_etapa = null;
DELETE FROM tramitacao.historico_objeto_tramitavel;
DELETE FROM tramitacao.objeto_tramitavel;
DELETE FROM tramitacao.etapa;
DELETE FROM tramitacao.fluxo;
DELETE FROM tramitacao.condicao;

INSERT INTO tramitacao.fluxo (id_fluxo, id_condicao_inicial, tx_descricao, dt_prazo) VALUES (1, null, 'Processo de Análise do Licenciamento Ambiental', null);

INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (1, 1, 'Análise pelo Chefe de unidade', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (2, 1, 'Análise técnica', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (3, 1, 'Análise diretor', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (4, 1, 'Análise presidente', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (5, 1, 'Análise gerente', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (6, 1, 'Análise finalizada', null);
INSERT INTO tramitacao.etapa (id_etapa, id_fluxo, tx_etapa, dt_prazo) VALUES (7, 1, 'Liberação da licença', null);

INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (0, null, 'Estado inicial', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (1, 7, 'Suspenso', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (2, 7, 'Cancelado', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (3, 7, 'Licença emitida', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (4, 6, 'Análise finalizada', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (5, 1, 'Aguardando distribuição pelo Chefe de unidade', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (6, 5, 'Aguardando vinculação técnica pelo gerente técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (7, 2, 'Aguardando análise técnica', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (8, 5, 'Aguardando validação técnica pelo gerente técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (9, 4, 'Aguardando assinatura do presidente', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (10, 3, 'Aguardando validação pela diretoria', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (11, 2, 'Aguardando resposta comunicado', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (12, 2, 'Aguardando resposta jurídica', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (13, 5, 'Em análise técnica pelo gerente técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (14, 3, 'Em análise pelo diretor técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (15, 4, 'Em análise pelo presidente', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (16, 5, 'Em análise pelo gerente técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (17, 1, 'Em análise pelo chefe de unidade', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (18, 5, 'Solicitação de desvínculo pendente análise técnica', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (19, 2, 'Notificado pelo Analista técnico', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (20, 7, 'Solicitação da licença aprovada', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (21, 7, 'Solicitação da licença negada', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (22, 7, 'Arquivado', 1);
INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (23, 7, 'Em análise técnica', 1);

INSERT INTO tramitacao.acao (id_acao, tx_descricao, fl_ativo, fl_tramitavel) VALUES (80, 'Distribuir processo para analista técnico', 1, 1);

INSERT INTO tramitacao.transicao (id_transicao, id_acao, id_condicao_inicial, id_condicao_final, dt_prazo, fl_retornar_fluxo_anterior) VALUES (1, 18, 0, 5, NULL, NULL);
INSERT INTO tramitacao.transicao (id_transicao, id_acao, id_condicao_inicial, id_condicao_final, dt_prazo, fl_retornar_fluxo_anterior) VALUES (2, 80, 5, 7, NULL, NULL);

UPDATE tramitacao.fluxo SET id_condicao_inicial = 5;


-- 5
INSERT INTO tramitacao.acao (id_acao, tx_descricao, fl_ativo, fl_tramitavel) VALUES
    (81, 'Usuário excluído/inativado no entrada única', 1, 1);

INSERT INTO tramitacao.transicao(id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Usuário excluído/inativado no entrada única'),
     (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade'),
     (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade'));

INSERT INTO tramitacao.transicao(id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Usuário excluído/inativado no entrada única'),
     (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'),
     (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade'));

--6
-- Solicitar desvínculo análise técnica
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Solicitar desvínculo análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica'));

-- Iniciar análise técnica
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica'));

-- Corrigir condição
UPDATE tramitacao.condicao
    SET id_etapa = (SELECT id_etapa from tramitacao.etapa where tx_etapa = 'Análise técnica')
    WHERE nm_condicao = 'Em análise técnica';


-- 7
-- Aprovar solicitação de desvínculo
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Aprovar solicitação de desvínculo do Analista técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'));

--8
--Transicao para o chefe de unidade negar o desvinculo
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Negar solicitação de desvínculo do Analista técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'));

--10
--- Atualizando acoes e condicoes para o perfil chefe de unidade
UPDATE tramitacao.condicao SET nm_condicao = 'Aguardando validação técnica pelo chefe de unidade' WHERE nm_condicao = 'Aguardando validação técnica pelo gerente técnico';
UPDATE tramitacao.acao SET tx_descricao = 'Deferir análise técnica via chefe de unidade' WHERE tx_descricao = 'Deferir análise técnica via gerente técnico';
UPDATE tramitacao.acao SET tx_descricao = 'Indeferir análise técnica via chefe de unidade' WHERE tx_descricao = 'Indeferir análise técnica via gerente técnico';


--- Deferir analise tecnica para o chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Deferir análise técnica via chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade'));

--- Indeferir analise tecnica para o chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Indeferir análise técnica via chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade'));

--- Notificar pelo analista tecnico
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Notificar pelo Analista técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Notificado pelo Analista técnico'));

--11
-- Arquivar processo resposta notificação
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Arquivar protocolo'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Notificado pelo Analista técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Análise finalizada'));

-- Iniciar análise técnica pela notificação
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Iniciar analise Técnica por volta de notificação'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'));

# --- !Downs

--5
DELETE FROM tramitacao.transicao WHERE 
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Usuário excluído/inativado no entrada única') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade');

DELETE FROM tramitacao.transicao WHERE 
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Usuário excluído/inativado no entrada única') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade');

DELETE FROM tramitacao.acao WHERE tx_descricao = 'Usuário excluído/inativado no entrada única';

--6
UPDATE tramitacao.condicao
    SET id_etapa = (SELECT id_etapa from tramitacao.etapa where tx_etapa = 'Liberação da licença')
    WHERE nm_condicao = 'Em análise técnica';

DELETE FROM tramitacao.transicao WHERE
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar análise técnica') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica');

DELETE FROM tramitacao.transicao WHERE 
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Solicitar desvínculo análise técnica') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica');

--7
DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Aprovar solicitação de desvínculo do Analista técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica');


-- 8
DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Negar solicitação de desvínculo do Analista técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Solicitação de desvínculo pendente análise técnica') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica');

--10
DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Deferir análise técnica via chefe de unidade') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Indeferir análise técnica via chefe de unidade') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo chefe de unidade');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Notificar pelo Analista técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Notificado pelo Analista técnico');

UPDATE tramitacao.condicao SET nm_condicao = 'Aguardando validação técnica pelo gerente técnico' WHERE nm_condicao = 'Aguardando validação técnica pelo chefe de unidade';
UPDATE tramitacao.acao SET tx_descricao = 'Deferir análise técnica via gerente técnico' WHERE tx_descricao = 'Deferir análise técnica via chefe de unidade';
UPDATE tramitacao.acao SET tx_descricao = 'Indeferir análise técnica via gerente técnico' WHERE tx_descricao = 'Indeferir análise técnica via chefe de unidade';

--11
-- Iniciar análise técnica pela notificação
DELETE FROM tramitacao.transicao WHERE 
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Iniciar analise Técnica por volta de notificação') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando distribuição pelo Chefe de unidade') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica');

-- Arquivar processo resposta notificação
DELETE FROM tramitacao.transicao WHERE
id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Arquivar protocolo') AND
id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Notificado pelo Analista técnico') AND
id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Análise finalizada');

