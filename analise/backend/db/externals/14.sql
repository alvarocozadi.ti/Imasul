# --- !Ups

INSERT INTO portal_seguranca.setor (nome, sigla, tipo_setor, data_cadastro, removido, ativo) VALUES 
('DIPRE', 'DIPRE', 1, now(), false, true);

UPDATE portal_seguranca.perfil SET nome = 'Diretor Presidente' WHERE codigo = 'PRESIDENTE';
UPDATE portal_seguranca.perfil SET nome = 'Diretor' WHERE codigo = 'DIRETOR';
UPDATE portal_seguranca.perfil SET nome = 'Gerente' WHERE codigo = 'GERENTE';
UPDATE portal_seguranca.perfil SET nome = 'Analista Técnico' WHERE codigo = 'ANALISTA_TECNICO';

DELETE FROM portal_seguranca.perfil_setor
WHERE id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE');

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES 
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'),
 (SELECT id FROM portal_seguranca.setor WHERE sigla = 'DIPRE'));


# --- !Downs

DELETE FROM portal_seguranca.perfil_setor WHERE
id_perfil = (SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE') AND
id_setor = (SELECT id FROM portal_seguranca.setor WHERE sigla = 'DIPRE');

INSERT INTO portal_seguranca.perfil_setor (id_perfil, id_setor) VALUES
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'), (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/AGROINFRA')),
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'), (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/MINEROTUR')),
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'), (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNIIND')),
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'), (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GLA/UNISAN')),
((SELECT id FROM portal_seguranca.perfil WHERE codigo = 'PRESIDENTE'), (SELECT id FROM portal_seguranca.setor WHERE sigla = 'GPF/UNIFAUNA'));

UPDATE portal_seguranca.perfil SET nome = 'Diretor PRESIDENTE' WHERE codigo = 'PRESIDENTE';
UPDATE portal_seguranca.perfil SET nome = 'Diretor TÉCNICO' WHERE codigo = 'DIRETOR';
UPDATE portal_seguranca.perfil SET nome = 'Gerente TÉCNICO' WHERE codigo = 'GERENTE';
UPDATE portal_seguranca.perfil SET nome = 'Analista TÉCNICO' WHERE codigo = 'ANALISTA_TECNICO';

DELETE FROM portal_seguranca.setor WHERE nome = 'DIPRE';


