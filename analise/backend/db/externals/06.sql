# --- !Ups

UPDATE tramitacao.acao SET tx_descricao = 'Validar parecer técnico chefe de unidade' WHERE tx_descricao = 'Validar parecer técnico gerente técnico';
UPDATE tramitacao.acao SET tx_descricao = 'Solicitar ajustes parecer técnico pelo chefe de unidade' WHERE tx_descricao = 'Solicitar ajustes parecer técnico pelo gerente técnico';

INSERT INTO tramitacao.condicao (id_condicao, id_etapa, nm_condicao, fl_ativo) VALUES (24, 5, 'Aguardando validação técnica pelo gerente técnico', 1);

-- deferir analise tecnica pelo chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Validar parecer técnico chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo gerente técnico'));

-- solicitar ajuste analise tecnica pelo chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Solicitar ajustes parecer técnico pelo chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica'));

-- indeferir analise tecnica pelo chefe de unidade
INSERT INTO tramitacao.transicao (id_acao, id_condicao_inicial, id_condicao_final) VALUES
    ((select id_acao from tramitacao.acao where tx_descricao = 'Invalidar parecer técnico encaminhando para outro técnico'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade'),
    (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica'));

# --- !Downs

UPDATE tramitacao.acao SET tx_descricao = 'Validar parecer técnico gerente técnico' WHERE tx_descricao = 'Validar parecer técnico chefe de unidade';
UPDATE tramitacao.acao SET tx_descricao = 'Solicitar ajustes parecer técnico pelo gerente técnico' WHERE tx_descricao = 'Solicitar ajustes parecer técnico pelo chefe de unidade';
DELETE from tramitacao.condicao  WHERE nm_condicao = 'Aguardando validação técnica pelo gerente técnico';


DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Validar parecer técnico chefe de unidade') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando validação técnica pelo gerente técnico');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Solicitar ajustes parecer técnico pelo chefe de unidade') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise técnica');

DELETE FROM tramitacao.transicao WHERE
    id_acao = (select id_acao from tramitacao.acao where tx_descricao = 'Invalidar parecer técnico encaminhando para outro técnico') AND
    id_condicao_inicial = (select id_condicao from tramitacao.condicao where nm_condicao = 'Em análise pelo chefe de unidade') AND
    id_condicao_final = (select id_condicao from tramitacao.condicao where nm_condicao = 'Aguardando análise técnica');