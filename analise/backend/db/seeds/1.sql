# --- !Ups

-- Executar no ambiente de testes
UPDATE analise.geoserver
SET url_getcapabilities='http://ap.puma.ti.lemaf.ufla.br/geoserver/wfs?REQUEST=GetCapabilities&version=1.0.0'
WHERE id = 1;


-- Executar no ambiente de homologacao
UPDATE analise.geoserver
SET url_getcapabilities='http://homologacao.licenciamento.imasul.ms.gov.br/geoserver/wfs?REQUEST=GetCapabilities&version=1.0.0'
WHERE id = 1;

-- Executar no ambiente de produção
UPDATE analise.geoserver
SET url_getcapabilities='http://licenciamento.imasul.ms.gov.br/geoserver/wfs?REQUEST=GetCapabilities&version=1.0.0'
WHERE id = 1;



# --- !Downs

UPDATE analise.geoserver
SET url_getcapabilities='http://car.semas.pa.gov.br/geoserver/wfs?REQUEST=GetCapabilities&version=1.1.0'
WHERE id = 1;

