# --- !Ups

ALTER TABLE analise.condicionante ALTER COLUMN prazo DROP NOT NULL;


# --- !Downs

ALTER TABLE analise.condicionante ALTER COLUMN prazo SET NOT NULL;
