# --- !Ups

ALTER TABLE analise.usuario_analise ADD COLUMN ativo BOOL NOT NULL DEFAULT TRUE;
COMMENT ON COLUMN analise.usuario_analise.ativo IS 'Flag que identifica quando o usuário está ativo';

# --- !Downs

ALTER TABLE analise.usuario_analise DROP COLUMN ativo;