# --- !Ups

CREATE TABLE analise.inconsistencia_tecnica_outorga (
	id serial NOT NULL,
	id_inconsistencia_tecnica int4 NOT NULL,
	id_caracterizacao int4 NOT NULL,
	CONSTRAINT pk_inconsistencia_tecnica_outorga PRIMARY KEY (id),
	CONSTRAINT fk_ict_inconsistencia_tecnica FOREIGN KEY(id_inconsistencia_tecnica)
    REFERENCES analise.inconsistencia_tecnica 
);
COMMENT ON TABLE analise.inconsistencia_tecnica_outorga IS 'Entidade responsável por armazenar a inconsistêências técnicas de outorga.';
COMMENT ON COLUMN analise.inconsistencia_tecnica_outorga.id IS 'Identificador único da entidade.';
COMMENT ON COLUMN analise.inconsistencia_tecnica_outorga.id_inconsistencia_tecnica IS 'Identificador que realizará o relacionamento entre analise.inconsistencia_tecnica_outorga e analise.inconsistencia_tecnica.';
COMMENT ON COLUMN analise.inconsistencia_tecnica_outorga.id_caracterizacao IS 'Identificador que realizará o relacionamento entre analise.inconsistencia_tecnica_outorga e licenciamento.caracterizacao.';
ALTER TABLE analise.inconsistencia_tecnica owner TO postgres;
GRANT SELECT, USAGE ON SEQUENCE analise.inconsistencia_tecnica_id_seq TO licenciamento_ms;
GRANT INSERT, SELECT, UPDATE, DELETE ON table analise.inconsistencia_tecnica TO licenciamento_ms;


# --- !Downs

DROP TABLE analise.inconsistencia_tecnica_outorga;
