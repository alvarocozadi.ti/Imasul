# --- !Ups

DELETE FROM analise.configuracao_layer WHERE nome_layer = 'areas_restritas_am:unidade_conservacao';

UPDATE analise.configuracao_layer SET nome_layer='base_referencia_ms:terras_indigenas_funai_2015' WHERE nome_layer='areas_restritas_am:terra_indigena' ;

UPDATE analise.configuracao_layer SET id = 1 WHERE id=2 ;

SELECT setval('analise.configuracao_layer_id_seq', coalesce(max(id), 1)) FROM analise.configuracao_layer;


# --- !Downs

UPDATE analise.configuracao_layer SET id = 2 WHERE id=1 ;

UPDATE analise.configuracao_layer SET nome_layer='areas_restritas_am:terra_indigena' WHERE nome_layer='base_referencia_ms:terras_indigenas_funai_2015' ;

INSERT INTO analise.configuracao_layer (id,atributo_descricao, buffer, descricao, id_geoserver, nome_layer) 
VALUES (1, 'nome_1', 10000, 'Unidade de conservação', 1, 'areas_restritas_am:unidade_conservacao');

SELECT setval('analise.configuracao_layer_id_seq', 2) FROM analise.configuracao_layer;
