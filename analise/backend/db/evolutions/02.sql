# --- !Ups

CREATE TABLE analise.chefe_unidade (
    id serial NOT NULL,
    id_analise integer NOT NULL,
    id_usuario integer NOT NULL,
    data_vinculacao timestamp,
    CONSTRAINT pk_chefe_unidade PRIMARY KEY (id),
    CONSTRAINT fk_a_analise FOREIGN KEY(id_analise)
	REFERENCES analise.analise(id),
	CONSTRAINT fk_a_usuario_analise FOREIGN KEY(id_usuario)
	REFERENCES analise.usuario_analise(id)
);
COMMENT ON TABLE analise.chefe_unidade IS 'Entidade responsável por armazenar o chefe de unidade da analise.';
COMMENT ON COLUMN analise.chefe_unidade.id IS 'Identificador único da entidade.';
COMMENT ON COLUMN analise.chefe_unidade.id_analise IS 'Identificador único da entidade analise.analise que realizará o relacionamento entre analise.chefe_unidade e analise.analise.';
COMMENT ON COLUMN analise.chefe_unidade.id_usuario IS 'Identificador único da entidade analise.usuario_analise que realizará o relacionamento entre analise.chefe_unidade e analise.usuario_analise.';
COMMENT ON COLUMN analise.chefe_unidade.data_vinculacao IS 'Data em que o usuario foi vinculado.';
ALTER TABLE analise.chefe_unidade owner TO postgres;
GRANT SELECT, USAGE ON SEQUENCE analise.chefe_unidade_id_seq TO licenciamento_ms;
GRANT INSERT, SELECT, UPDATE, DELETE ON table analise.chefe_unidade TO licenciamento_ms;

# --- !Downs

DROP TABLE analise.chefe_unidade;