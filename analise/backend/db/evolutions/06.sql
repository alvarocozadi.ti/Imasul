# --- !Ups

ALTER TABLE analise.parecer_analista_tecnico ADD id_carta_imagem int4 NULL;
COMMENT ON COLUMN analise.parecer_analista_tecnico.id_carta_imagem IS 'Identificador único do documento carta_imagem';
ALTER TABLE analise.parecer_analista_tecnico ADD 
	CONSTRAINT parecer_analista_tecnico_fk FOREIGN KEY (id_carta_imagem) 
	REFERENCES analise.documento(id);

# --- !Downs

ALTER TABLE analise.parecer_analista_tecnico DROP COLUMN id_carta_imagem;
