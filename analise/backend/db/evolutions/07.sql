# --- !Ups

CREATE TABLE analise.parecer_chefe_unidade_analise_tecnica
(
  id serial NOT NULL,
  id_tipo_resultado_analise integer NOT NULL,
  parecer text NOT NULL,
  data_parecer timestamp without time zone DEFAULT now(),
  id_usuario_chefe_unidade integer NOT NULL,
  id_analise_tecnica integer NOT NULL,
  id_historico_tramitacao integer NOT NULL,
  CONSTRAINT pk_parecer_chefe_unidade_analise_tecnica PRIMARY KEY (id),
  CONSTRAINT fk_pcuat_analise_tecnica FOREIGN KEY (id_analise_tecnica)
      REFERENCES analise.analise_tecnica (id) ,
  CONSTRAINT fk_pcuat_tipo_resultado_analise FOREIGN KEY (id_tipo_resultado_analise)
      REFERENCES analise.tipo_resultado_analise (id),
  CONSTRAINT fk_pcuat_usuario_analise FOREIGN KEY (id_usuario_chefe_unidade)
      REFERENCES analise.usuario_analise (id)
);

ALTER TABLE analise.parecer_chefe_unidade_analise_tecnica OWNER TO postgres;
ALTER TABLE analise.parecer_chefe_unidade_analise_tecnica OWNER TO licenciamento_ms;
GRANT SELECT,INSERT,UPDATE,DELETE ON TABLE analise.parecer_chefe_unidade_analise_tecnica TO licenciamento_ms;
GRANT ALL ON TABLE analise.parecer_chefe_unidade_analise_tecnica TO postgres;
COMMENT ON TABLE analise.parecer_chefe_unidade_analise_tecnica IS 'Entidade responsável por armazenar informações sobre o parecer do chefe de unidade em uma análise técnica';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.id IS 'Identificador do parecer do chefe de unidade';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.id_tipo_resultado_analise IS 'Tipo do resultado da análise';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.parecer IS 'Descrição do parecer feito pelo chefe de unidade';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.data_parecer IS 'Data do parecer';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.id_usuario_chefe_unidade IS 'Identificador do chefe de unidade responsável pelo parecer';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.id_analise_tecnica IS 'Identificador da análise tecnica que teve parecer';
COMMENT ON COLUMN analise.parecer_chefe_unidade_analise_tecnica.id_historico_tramitacao IS 'Identificador do histórico da tramitação';

# --- !Downs

DROP TABLE analise.parecer_chefe_unidade_analise_tecnica;