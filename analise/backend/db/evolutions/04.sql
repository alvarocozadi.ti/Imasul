# --- !Ups

ALTER TABLE analise.analise_tecnica ADD COLUMN id_analise_geo integer NOT NULL;
COMMENT ON COLUMN analise.analise_tecnica.id_analise_geo IS 'Identificador da tabela analise_geo, responsável pelo relacionamento entre as duas tabelas.';

ALTER TABLE analise.analise_tecnica ADD CONSTRAINT fk_at_analise_geo 
	FOREIGN KEY (id_analise_geo) REFERENCES analise.analise_geo(id);


# --- !Downs

ALTER TABLE analise.analise_tecnica DROP CONSTRAINT fk_at_analise_geo;
ALTER TABLE analise.analise_tecnica DROP COLUMN id_analise_geo;
