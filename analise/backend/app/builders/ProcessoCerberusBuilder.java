package builders;

import models.migracaoCerberus.ProcessoCliente;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ProcessoCerberusBuilder extends CriteriaBuilder<ProcessoCliente> {

    private static final String CLIENTE_ALIAS = "cln";
    private static final String PROCESSO_ALIAS = "pcs";

    public ProcessoCerberusBuilder addClienteAlias() {

        addAlias("cliente", CLIENTE_ALIAS);

        return this;
    }

    public ProcessoCerberusBuilder addProcessoAlias() {

        addAlias("processoCerberus", PROCESSO_ALIAS);

        return this;
    }

    public ProcessoCerberusBuilder groupByCliente(){

        addClienteAlias();
        addProjection(Projections.groupProperty("cliente").as("cliente"));

        return this;
    }

    public ProcessoCerberusBuilder groupByProcesso(){

        addProcessoAlias();
        addProjection(Projections.groupProperty("processoCerberus").as("processoCerberus"));

        return this;
    }

    public ProcessoCerberusBuilder count() {

        addProjection(Projections.countDistinct("id").as("total"));

        return this;
    }

    public ProcessoCerberusBuilder filtrarPorCpfCnpjCliente(String cpfCnpj) {

        if (StringUtils.isNotEmpty(cpfCnpj)) {

            addClienteAlias();

            addRestriction(Restrictions.or(Restrictions.ilike(CLIENTE_ALIAS + ".cpf", cpfCnpj, MatchMode.START),
                                            Restrictions.ilike(CLIENTE_ALIAS + ".cnpj", cpfCnpj, MatchMode.START)));
        }

        return this;
    }

    public ProcessoCerberusBuilder filtrarPorPeriodoProcesso(Date periodoInicial, Date periodoFinal) {

        if (periodoInicial != null)

            addRestriction(Restrictions.ge(PROCESSO_ALIAS + ".dataProcesso", periodoInicial));

        if (periodoFinal != null) {

            //Somando um dia a mais no periodo final para resolver o problema da data com hora
            periodoFinal = new Date(periodoFinal.getTime() + TimeUnit.DAYS.toMillis(1));

            addRestriction(Restrictions.le(PROCESSO_ALIAS + ".dataProcesso", periodoFinal));

        }

        return this;
    }

    public ProcessoCerberusBuilder filtrarPorIdMunicipio(Long idMunicipio) {

        if (idMunicipio != null)
            addRestriction(Restrictions.eq("idMunicipio", idMunicipio));

        return this;
    }

    public ProcessoCerberusBuilder filtrarPorNumeroProcesso(String numeroProcesso) {

        if (StringUtils.isNotEmpty(numeroProcesso)) {
            addProcessoAlias();
            addRestriction(Restrictions.ilike( PROCESSO_ALIAS + ".numero", numeroProcesso, MatchMode.ANYWHERE));
        }

        return this;
    }


    public ProcessoCerberusBuilder filtrarPorIdAtividade(Long idAtividade) {

        if (idAtividade != null) {

//            addAtividadeAlias();
//            addRestriction(Restrictions.eq(ATIVIDADE_ALIAS+".id", idAtividade));
        }

        return this;
    }

    public static class FiltroProcessoCerberus {

        public String cpfCnpjCliente;
        public String numeroProcesso;
        public Long idMunicipio;
        public Long idAtividade;
        public Boolean filtrarPorUsuario = false;
        public Date periodoInicial;
        public Date periodoFinal;
        public Long paginaAtual;
        public Long itensPorPagina;
        public String siglaSetorGerencia;

        public Long idUsuarioLogado;
        public Boolean isConsultarProcessos = false;

        public FiltroProcessoCerberus() {

        }
    }
}
