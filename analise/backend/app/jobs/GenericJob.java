package jobs;

import org.hibernate.Session;
import play.db.jpa.JPA;
import play.jobs.Job;
import utils.Configuracoes;

public abstract class GenericJob extends Job {

	@Override
	public void doJob() throws Exception {

		((Session) JPA.em().getDelegate()).enableFilter("usuarioAtivo").setParameter("ativo", true);
		
		if (!Configuracoes.JOBS_ENABLED)
			return;
		
		executar();
	}
	
	public abstract void executar() throws Exception;
	
	protected void commitTransaction() {
		
		if (JPA.isInsideTransaction()) {
			
			JPA.em().getTransaction().commit();
			JPA.em().getTransaction().begin();
		}
	}
	
	protected void rollbackTransaction() {
		
		if (JPA.isInsideTransaction()) {
			
			JPA.em().getTransaction().rollback();
			JPA.em().getTransaction().begin();
		}
	}
}
