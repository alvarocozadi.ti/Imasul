package jobs;

import models.Analise;
import models.Notificacao;
import models.Processo;
import models.licenciamento.StatusCaracterizacaoEnum;
import models.tramitacao.AcaoTramitacao;
import models.tramitacao.HistoricoTramitacao;
import play.Logger;
import play.jobs.On;
import utils.Helper;
import java.util.Date;
import java.util.List;

//***removido produção 17/02
//@On("cron.verificarPrazoAtendimentoNotificacao")
public class VerificarPrazoAtendimentoNotificacao extends GenericJob {

    @Override
    public void executar() throws Exception {

        Logger.info("[INICIO-JOB] ::VerificarPrazoAtendimentoNotificacao:: [INICIO-JOB]");

        List<Notificacao> notificacoes = Notificacao
                .find("ativo = true AND dataConclusao IS NULL AND dataFinalNotificacao IS NOT NULL")
                .fetch();

        notificacoes.forEach(notificacao -> {

            try {

                verificarPrazoAtendimento(notificacao);
                commitTransaction();

            } catch (Exception e) {

                Logger.error(e.getMessage());
                rollbackTransaction();

            }

        });

        Logger.info("[FIM-JOB] ::VerificarPrazoAtendimentoNotificacao:: [FIM-JOB]");

    }

    public void verificarPrazoAtendimento(Notificacao notificacao) {

        Long diasRestantes = Helper.getDiferencaDias(notificacao.dataFinalNotificacao, new Date());

        if (diasRestantes < 0) {

            Processo processo = notificacao.parecerAnalistaTecnico.analiseTecnica.analise.processo;
            Analise.alterarStatusLicenca(StatusCaracterizacaoEnum.ARQUIVADO.codigo, processo.numero);
            processo.tramitacao.tramitar(processo, AcaoTramitacao.ARQUIVAR_PROTOCOLO);
            HistoricoTramitacao.setSetor(HistoricoTramitacao.getUltimaTramitacao(processo.idObjetoTramitavel), processo.caracterizacao.atividadesCaracterizacao.get(0).atividade.siglaSetor);

            notificacao.ativo = false;
            notificacao.save();

        }

    }
}
