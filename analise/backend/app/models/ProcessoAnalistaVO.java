package models;

public class ProcessoAnalistaVO {

    public UsuarioAnalise usuarioAnalise;
    public Processo processo;
    public String siglaSetor;

    public ProcessoAnalistaVO(UsuarioAnalise usuarioAnalise, Processo processo, String siglaSetor){
        this.usuarioAnalise = usuarioAnalise;
        this.processo = processo;
        this.siglaSetor = siglaSetor;
    }
}
