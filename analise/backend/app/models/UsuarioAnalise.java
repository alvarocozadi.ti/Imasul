package models;

import br.ufla.lemaf.beans.pessoa.Perfil;
import models.EntradaUnica.Usuario;
import org.hibernate.Session;
import org.hibernate.annotations.*;
import play.Play;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import security.cadastrounificado.CadastroUnificadoWS;
import services.IntegracaoEntradaUnicaService;
import utils.Configuracoes;
import utils.Mensagem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.stream.Collectors;

@Entity
@Table(schema = "analise", name = "usuario_analise")
@FilterDefs(value = {
		@FilterDef( name = "usuarioAtivo", parameters = @ParamDef(name = "ativo", type = "boolean"), defaultCondition = "ativo = :ativo" )
})
@Filter(name = "usuarioAtivo")
public class UsuarioAnalise extends GenericModel  {

	public static final String SEQ = "analise.usuario_analise_id_seq";

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator=SEQ)
	@SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
	public Long id;

	@Column(name="login")
	@Required
	@MaxSize(value = 14)
	public String login;

	@OneToOne
	@JoinColumn(name = "id_pessoa")
	public Pessoa pessoa;

	@Column(name = "ativo")
	public Boolean ativo;

	@OneToMany(mappedBy="usuarioAnalise", fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List <SetorUsuarioAnalise> setores;

	@OneToMany(mappedBy="usuarioAnalise", fetch=FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List <PerfilUsuarioAnalise> perfis;

	public static transient ExecutorService executorService = new ScheduledThreadPoolExecutor(Integer.valueOf(Play.configuration.getProperty("usuario.threads", "3")));

	@Transient
	private List<Integer> permittedActionsIds;

	@Transient
	public Usuario usuarioEntradaUnica;

	@Transient
	public String nome;

	@Transient
	public int qtdProcessos;

	public static UsuarioAnalise getUsuarioAnaliseById(Long id) {

		return UsuarioAnalise.findById(id);

	}

	public static UsuarioAnalise getUsuarioEntradaUnicaByLogin(String login) {

		IntegracaoEntradaUnicaService integracaoEntradaUnica= new IntegracaoEntradaUnicaService();

		return integracaoEntradaUnica.findUsuarioByLogin(login);
	}

	public static List<UsuarioAnalise> getUsuariosByPerfil(String codigoPerfil) {

		IntegracaoEntradaUnicaService integracaoEntradaUnica= new IntegracaoEntradaUnicaService();

		return integracaoEntradaUnica.findUsuariosByPerfil(codigoPerfil);
	}
	
	public static List<UsuarioAnalise> getUsuariosEntradaUnica(String codigoPerfil, String siglaSetor) {

		IntegracaoEntradaUnicaService integracaoEntradaUnica = new IntegracaoEntradaUnicaService();

		return integracaoEntradaUnica.findUsuariosByPerfilAndSetor(codigoPerfil, siglaSetor);
	}

	public static List<UsuarioAnalise> getUsuariosByPerfilSetores(String codigoPerfil, List<String> siglasSetores) {

		IntegracaoEntradaUnicaService integracaoEntradaUnica = new IntegracaoEntradaUnicaService();

		return integracaoEntradaUnica.findUsuariosByPerfilAndSetores(codigoPerfil, siglasSetores);
	}
	
	public boolean hasPerfil(String codigoPerfil){

		for (Perfil perfil : this.usuarioEntradaUnica.perfis) {

			if (perfil.codigo.equals(codigoPerfil)) {

				return true;
			}
		}

		return false;
	}

	public static UsuarioAnalise findByDiretor(Diretor diretor) {

		return UsuarioAnalise.find("id = :id_diretor")
				.setParameter("id_diretor", diretor.usuario.id).first();
	}

	public static UsuarioAnalise findByPresidente(Presidente presidente) {

		return UsuarioAnalise.find("id = :id_presidente")
				.setParameter("id_presidente", presidente.usuario.id).first();
	}

	public static UsuarioAnalise findByGerente(Gerente gerente) {

		return UsuarioAnalise.find("id = :id_gerente")
				.setParameter("id_gerente", gerente.usuario.id).first();
	}

	public static UsuarioAnalise findByChefeUnidade(ChefeUnidade chefeUnidade) {

		return UsuarioAnalise.find("id = :id_chefe_unidade")
				.setParameter("id_chefe_unidade", chefeUnidade.usuario.id).first();
	}

	public static UsuarioAnalise findByAnalistaTecnico(AnalistaTecnico analistaTecnico) {

		return UsuarioAnalise.find("id = :id_analista_tecnico")
			.setParameter("id_analista_tecnico", analistaTecnico.usuario.id).first();
	}

	public static UsuarioAnalise findByAnalistaGeo(AnalistaGeo analistaGeo) {

		return UsuarioAnalise.find("id = :id_analista_geo")
				.setParameter("id_analista_geo", analistaGeo.usuario.id).first();
	}

	public static List<UsuarioAnalise> findUsuariosByPerfilAndSetor(String codigoPerfil, String siglaSetor) {

		((Session) JPA.em().getDelegate()).disableFilter("usuarioAtivo");

		JPAQuery query = UsuarioAnalise.find("SELECT DISTINCT u FROM UsuarioAnalise u " +
				"INNER JOIN PerfilUsuarioAnalise p ON p.usuarioAnalise.id = u.id " +
				"INNER JOIN SetorUsuarioAnalise s ON s.usuarioAnalise.id = u.id " +
				"WHERE p.codigoPerfil = :codigoPerfil AND s.siglaSetor = :siglaSetor AND u.ativo = true");

		return query
				.setParameter("codigoPerfil", codigoPerfil)
				.setParameter("siglaSetor", siglaSetor)
				.fetch();

	}

	public static List<UsuarioAnalise> findUsuariosByPerfil(String codigoPerfil) {

		((Session) JPA.em().getDelegate()).disableFilter("usuarioAtivo");

		return UsuarioAnalise.find("SELECT DISTINCT u FROM UsuarioAnalise u " +
				"LEFT JOIN PerfilUsuarioAnalise p ON p.usuarioAnalise.id = u.id " +
				"WHERE p.codigoPerfil = :codigoPerfil AND u.ativo = true")
				.setParameter("codigoPerfil", codigoPerfil)
				.fetch();

	}

	public static void atualizaUsuariosAnalise() {

		((Session) JPA.em().getDelegate()).disableFilter("usuarioAtivo");

		List<UsuarioAnalise> usuariosAnalise = UsuarioAnalise.findAll();

		br.ufla.lemaf.beans.pessoa.Usuario[] usuariosPorModulo = CadastroUnificadoWS.ws.findUsuariosBySiglaModulo(Configuracoes.SIGLA_MODULO);
		List<br.ufla.lemaf.beans.pessoa.Usuario> usuarioList = Arrays.asList(usuariosPorModulo);

		List<Usuario> usuariosFiltrados = usuarioList.stream().map(Usuario::new).collect(Collectors.toList());

			usuariosAnalise.forEach(usuarioAnalise -> {

			Usuario usuario = usuariosFiltrados.stream().filter(usuarioEU -> usuarioEU.login.equals(usuarioAnalise.login)).findAny().orElse(null);//.orElseThrow(PortalSegurancaException::new);

			if(usuario != null && !usuario.perfis.isEmpty()) {

				usuarioAnalise.ativo = true;
				usuarioAnalise.perfis = usuarioAnalise.salvarPerfis(usuario);
				usuarioAnalise.setores = usuarioAnalise.salvarSetores(usuario);
				usuarioAnalise._save();

			} else {

				if(usuarioAnalise.ativo){

					usuarioAnalise.inativar();
					usuarioAnalise._save();

				}

			}

		});

	}

	public void inativar() {

		boolean desvinculado = true;

		for( PerfilUsuarioAnalise p : this.perfis){

			switch (p.codigoPerfil){

				case "CHEFE_UNIDADE":
					desvinculado = desvinculado && ChefeUnidade.inativar(this);
					break;

				case "ANALISTA_TECNICO":
					desvinculado = desvinculado && AnalistaTecnico.inativar(this);
					break;

				case "GERENTE":
					//todo implementar desvínculo Gerente
					break;

				case "DIRETOR":
					//todo implementar desvínculo Diretor
					break;

				case "PRESIDENTE":
					//todo implementar desvínculo Presidente
					break;

				default:
					break;

			}

		}

		this.ativo = !desvinculado;

	}

	private List<PerfilUsuarioAnalise> salvarPerfis(Usuario usuario) {

		if(this.perfis == null) {

			this.perfis = new ArrayList<>();

		}

		this.perfis.forEach(PerfilUsuarioAnalise::_delete);
		this.perfis.clear();
		this._save();

		usuario.perfis.forEach(perfil -> {

			PerfilUsuarioAnalise perfilUsuarioAnalise = new PerfilUsuarioAnalise(perfil, this);
			this.perfis.add(perfilUsuarioAnalise.save());

		});

		return this.perfis;

	}

	private List<SetorUsuarioAnalise> salvarSetores(Usuario usuario) {

		if(this.setores == null) {

			this.setores = new ArrayList<>();

		}

		this.setores.forEach(SetorUsuarioAnalise::_delete);
		this.setores.clear();
		this._save();

		usuario.setores.forEach(setor -> {

			SetorUsuarioAnalise setorUsuarioAnalise = new SetorUsuarioAnalise(setor, this);
			this.setores.add(setorUsuarioAnalise.save());

		});

		return this.setores;

	}

}
