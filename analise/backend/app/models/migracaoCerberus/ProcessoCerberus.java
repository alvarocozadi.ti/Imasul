package models.migracaoCerberus;

import models.*;
import models.licenciamento.*;
import models.tramitacao.*;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import utils.*;

import javax.persistence.*;
import java.util.*;

import static models.licenciamento.Caracterizacao.OrigemSobreposicao.*;

@Entity
@Table(schema="migracao", name="processo")
public class ProcessoCerberus extends GenericModel{

    private static final String SEQ = "migracao.processo_id_seq";

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
    @SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
    @Column(name="processo_id")
    public Integer id;

    @Column(name="responsavel_id")
    public Integer responsavel;

    @Column(name="contato_id")
    public Integer contatoId;

    @Required
    @ManyToOne
    @JoinColumn(name="tipo_processo_id", referencedColumnName="tipo_processo_id")
    public TipoProcessoCerberus tipoProcessoCerberus;

    @Required
    @Column(name="familia_processo_id")
    public String familiaProcesso;

    @Required
    @Column(name="grupo_tipo_processo_id")
    public String grupoTipoProcesso;

    @Required
    @Column(name="numero")
    public String numero;

    @Required
    @Column(name="descricao")
    public String descricao;

    @Column(name="devolucao")
    public String devolucao;

    @Required
    @Column(name="excluido")
    public String excluido;

    @Column(name="codigo_antigo")
    public String codigoAntigo;

    @Column(name="numero_spi")
    public String numeroSpi;

    @Column(name="codmunicipioms")
    public Integer codMunicipio;

    @Column(name="xcod")
    public Integer xCod;

    @Column(name="x2015antes2014")
    public String x2015antes2014;

    @Column(name="dataproc")
    public String dataProcesso;

    @Column(name="latitude")
    public String latitude;

    @Column(name="longitude")
    public String longitude;

    @Column(name="codatividade")
    public String codAtividade;

    @Column(name="mun_id")
    public String idMunicipio;

    @Column(name="id_lic_emitida")
    public String idLicencaEmitida;

    @Column(name="longg")
    public String longG;

    @Column(name="longm")
    public String longM;

    @Column(name="longs")
    public String longS;

    @Column(name="lats")
    public String latS;

    @Column(name="latm")
    public String latM;

    @Column(name="latg")
    public String latG;

    @Column(name="data_protocolo")
    public String dataProtocolo;

    @Column(name="prazo")
    public String prazo;

    @Column(name="status_novo_atual")
    public String statusNovoAtual;

    @Column(name="docvigente")
    public String documentoVigente;

    @Column(name="ultimo_movproc")
    public String ultimoMovProcesso;

    @Column(name="lic_emitida")
    public String licencaEmitida;

    @Column(name="tipo_fim_cat")
    public String tipoFimCat;

    @Column(name="data_fim_cat")
    @Temporal(TemporalType.TIMESTAMP)
    public Date dataFimCat;

    @Column(name="id_ultima_lic_emitida")
    public String idUltimaLicEmitida;

    @Column(name="proc_id_da_lic_emitida")
    public String procIdLicEmitida;

    @Column(name="pendencia_aberta")
    public String pendenciaAberta;

    @Column(name="para_fiscalizar")
    public String paraFiscalizar;

    @Column(name="ca_dilic")
    public String caDilic;

    @Transient
    public LicencaCerberus licenca;


}

