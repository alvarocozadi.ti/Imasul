package models.migracaoCerberus;

import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema="migracao", name="licenca")
public class LicencaCerberus extends GenericModel {

    private static final String SEQ = "migracao.licenca_cerberus_id_seq";

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
    @SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
    @Column(name="lic_id")
    public Integer id;

    @Column(name="lic_atividade")
    public String atividade;

    @Column(name="lic_celularcontato")
    public String celularContato;

    @Column(name="lic_cliente")
    public String cliente;

    @Column(name="lic_contato")
    public String contato;

    @Column(name="lic_cpf_cnpj")
    public String cpfCnpj;

    @Column(name="lic_dataemissao")
    public String dataEmissao;

    @Column(name="lic_datavalidade")
    public String dataValidade;

    @Column(name="lic_emailcontato")
    public String emailContato;

    @Column(name="lic_endereco")
    public String endereco;

    @Column(name="lic_fonecontato")
    public String foneContato;

    @Column(name="lic_numerotext")
    public String numeroText;

    @Column(name="lic_numeroprocesso")
    public String numeroProcesso;

    @Column(name="lic_numerospi")
    public String numeroSpi;

    @Column(name="setor_descricao")
    public String setorDescricao;

    @Column(name="sub_descricao")
    public String subDescricao;

    @Required
    @ManyToOne
    @JoinColumn(name="tipo_processo", referencedColumnName="tipo_processo_id")
    public TipoProcessoCerberus tipoProcesso;

    @Column(name="tipo_processo_id")
    public Integer idTipoProcesso;

    @Column(name="setor_id")
    public Integer setor;

    @Column(name="mun_id")
    public Integer municipio;

    @Column(name="sub_id")
    public Integer sub;

    @Column(name="processo_id")
    public Integer processo;

    public static LicencaCerberus findLicencaByIdProcesso(Integer idProcesso){

        LicencaCerberus licenca =  LicencaCerberus.find("processo_id = :idProcesso")
                .setParameter("idProcesso", idProcesso)
                .first();

        return licenca;

    }

}
