package models.migracaoCerberus;

import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema="migracao", name="cliente")
public class Cliente extends GenericModel {

    private static final String SEQ = "migracao.cliente_id_seq";

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
    @SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
    @Column(name="cliente_id")
    public Integer id;

    @Column(name="cliente")
    public String cliente;

    @Column(name="rg")
    public String rg;

    @Column(name="cpf")
    public String cpf;

    @Column(name="cnpj")
    public String cnpj;

    @Column(name="complemento")
    public String complemento;

    @Column(name="cep_id")
    public String cepId;

    @Column(name="codigo_antigo")
    public String codigoAntigo;

    @Column(name="telefone_cliente")
    public String telefoneCliente;

    @Column(name="celular_cliente")
    public String celularCliente;

    @Column(name="email_cliente")
    public String emailCliente;

    @Column(name="status")
    public String status;

    @Column(name="data")
    public String data;

    @Column(name="usrcad")
    public String usrcad;

}
