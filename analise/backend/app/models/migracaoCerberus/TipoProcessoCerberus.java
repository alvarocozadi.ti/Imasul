package models.migracaoCerberus;

import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.BitSet;

@Entity
@Table(schema="migracao", name="tipo_processo")
public class TipoProcessoCerberus extends GenericModel {

    private static final String SEQ = "migracao.tipo_processo_id_seq";

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
    @SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
    @Column(name="tipo_processo_id")
    public Integer id;

    @Required
    @Column(name="familia_processo_id")
    public String familiaProcesso;

    @Required
    @Column(name="grupo_tipo_processo_id")
    public String grupoTipoProcesso;

    @Required
    @Column(name="sigla_tipo")
    public String siglaTipo;

    @Required
    @Column(name="tipo_processo")
    public String tipoProcesso;

    @Column(name="excluido")
    public String excluido;

    @Column(name="requer_documentacao")
    public String requerDocumentacao;

    @Column(name="requer_planilha")
    public String requerPlanilha;

    @Column(name="contador")
    public Integer contador;

    @Column(name="emite_licenca")
    public String emiteLicenca;

    @Column(name="pontuacao")
    public String pontuacao;

}
