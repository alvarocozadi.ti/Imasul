package models.migracaoCerberus;

import builders.ProcessoCerberusBuilder;
import models.UsuarioAnalise;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(schema="migracao", name="processo_cliente")
public class ProcessoCliente extends GenericModel {

    private static final String SEQ = "migracao.processo_cliente_id_seq";

    @Id
    @Required
    @ManyToOne
    @JoinColumn(name="cliente_id", referencedColumnName="cliente_id")
    public Cliente cliente;

    @Id
    @Required
    @ManyToOne
    @JoinColumn(name="processo_id", referencedColumnName="processo_id")
    public ProcessoCerberus processoCerberus;

    @Required
    public Boolean gerador_acao;

    private static ProcessoCerberusBuilder commonFilterProcesso(ProcessoCerberusBuilder.FiltroProcessoCerberus filtro, UsuarioAnalise usuarioSessao) {

        ProcessoCerberusBuilder processoCerberusBuilder = new ProcessoCerberusBuilder()
                .filtrarPorNumeroProcesso(filtro.numeroProcesso)
                .filtrarPorCpfCnpjCliente(filtro.cpfCnpjCliente)
                .filtrarPorIdAtividade(filtro.idAtividade)
                .filtrarPorPeriodoProcesso(filtro.periodoInicial,filtro.periodoFinal)
                .filtrarPorIdMunicipio(filtro.idMunicipio);

        return processoCerberusBuilder;

    }
    public static List listWithFilter(ProcessoCerberusBuilder.FiltroProcessoCerberus filtro, UsuarioAnalise usuarioSessao) {

        ProcessoCerberusBuilder processoCerberusBuilder = commonFilterProcesso(filtro, usuarioSessao)
                .groupByCliente()
                .groupByProcesso();

        listWithFilterConsultaProcessos(processoCerberusBuilder, filtro);

        return processoCerberusBuilder
                .fetch(filtro.paginaAtual.intValue(), filtro.itensPorPagina.intValue())
                .list();

    }
    private static void listWithFilterConsultaProcessos(ProcessoCerberusBuilder processoCerberusBuilder, ProcessoCerberusBuilder.FiltroProcessoCerberus filtro) {

        if (!filtro.isConsultarProcessos)
            return;


    }
    public static Long countWithFilter(ProcessoCerberusBuilder.FiltroProcessoCerberus filtro, UsuarioAnalise usuarioSessao) {

        ProcessoCerberusBuilder processoCerberusBuilder = commonFilterProcesso(filtro, usuarioSessao)
                .count();

        Object qtdeTotalItens = processoCerberusBuilder.unique();

        return ((Map<String, Long>) qtdeTotalItens).get("total");
    }

}
