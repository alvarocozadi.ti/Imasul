package models;

public enum StatusLicenca {

	ATIVA,
	CANCELADA,
	RENOVADA,
	SUSPENSA
}
