package models;

import models.licenciamento.Caracterizacao;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema="analise", name="inconsistencia_tecnica_outorga")
public class InconsistenciaTecnicaOutorga extends GenericModel{

	public static final String SEQ = "analise.inconsistencia_tecnica_outorga_id_seq";

	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator=SEQ)
	@SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
	public Long id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_inconsistencia_tecnica")
	public InconsistenciaTecnica inconsistenciaTecnica;

	@OneToOne
	@JoinColumn(name="id_caracterizacao")
	public Caracterizacao caracterizacao;

}
