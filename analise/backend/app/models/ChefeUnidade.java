package models;

import controllers.Condicoes;
import exceptions.ValidacaoException;
import models.EntradaUnica.CodigoPerfil;
import models.EntradaUnica.Usuario;
import models.licenciamento.AtividadeCaracterizacao;
import models.licenciamento.Caracterizacao;
import models.tramitacao.AcaoTramitacao;
import models.tramitacao.Condicao;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import security.Auth;
import utils.Mensagem;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(schema="analise", name="chefe_unidade")
public class ChefeUnidade extends GenericModel {

    public static final String SEQ = "analise.chefe_unidade_id_seq";

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator=SEQ)
    @SequenceGenerator(name=SEQ, sequenceName=SEQ, allocationSize=1)
    public Long id;

    @Required
    @ManyToOne
    @JoinColumn(name="id_analise")
    public Analise analise;

    @Required
    @ManyToOne
    @JoinColumn(name="id_usuario")
    public UsuarioAnalise usuario;

    @Required
    @Column(name="data_vinculacao")
    @Temporal(TemporalType.TIMESTAMP)
    public Date dataVinculacao;

    public ChefeUnidade() {}

    public ChefeUnidade(Analise analise, UsuarioAnalise usuario) {

        super();
        this.analise = analise;
        this.usuario = usuario;
        this.dataVinculacao = new Date();

    }

    public static boolean inativar(UsuarioAnalise usuarioAnalise) {

        Long etapaChefeUnidade = 1L;

        String jpql = "SELECT c FROM ChefeUnidade c " +
                "LEFT JOIN Analise a ON a.id = c.analise.id " +
                "LEFT JOIN Processo p ON p.id = a.processo.id " +
                "LEFT JOIN ObjetoTramitavel ot ON ot.id = p.objetoTramitavel.id " +
                "WHERE c.usuario = :usuario AND a.ativo = :ativo AND ot.condicao.idEtapa = :etapaChefeUnidade";

        List<ChefeUnidade> chefesUnidade = find(jpql)
                .setParameter("usuario", usuarioAnalise)
                .setParameter("ativo", true)
                .setParameter("etapaChefeUnidade", etapaChefeUnidade)
                .fetch();

        for(ChefeUnidade chefeUnidade : chefesUnidade) {

            Analise analise = chefeUnidade.analise;

            Caracterizacao caracterizacao = Caracterizacao.findById(analise.processo.caracterizacao.id);
            String siglaSetor = AtividadeCaracterizacao.getSiglaAtividadePrincipal(caracterizacao.atividadesCaracterizacao);

            ChefeUnidade novoChefe;

            try{

                novoChefe = redistribuirProcesso(siglaSetor, analise, usuarioAnalise);

            } catch (Exception e) {

                throw new RuntimeException(Mensagem.ERRO_INATIVAR_USUARIO.getTexto());

            }

            chefeUnidade.usuario = novoChefe.usuario;
            chefeUnidade.dataVinculacao = new Date();

            chefeUnidade._save();

            Processo processo = analise.processo;
            processo.tramitacao.tramitar(processo, AcaoTramitacao.INATIVAR_USUARIO);

        }

        return true;

    }

    public ChefeUnidade gerarCopia() {

        ChefeUnidade copia = new ChefeUnidade();

        copia.usuario = this.usuario;
        copia.dataVinculacao = this.dataVinculacao;

        return copia;
    }

    public static ChefeUnidade redistribuirProcesso(String setorAtividade, Analise analise, UsuarioAnalise usuarioAnalise) {

        List<UsuarioAnalise> usuariosAnalise = UsuarioAnalise.findUsuariosByPerfilAndSetor(CodigoPerfil.CHEFE_UNIDADE, setorAtividade)
                .stream().filter(ua -> !ua.id.equals(usuarioAnalise.id)).collect(Collectors.toList());

        return distribuicaoProcesso(setorAtividade, analise, usuariosAnalise);

    }

    public static ChefeUnidade distribuicaoProcesso(String setorAtividade, Analise analise) throws ValidacaoException {

        List<UsuarioAnalise> usuariosAnalise = UsuarioAnalise.findUsuariosByPerfilAndSetor(CodigoPerfil.CHEFE_UNIDADE, setorAtividade);
        return distribuicaoProcesso(setorAtividade, analise, usuariosAnalise);

    }

    public static ChefeUnidade distribuicaoProcesso(String setorAtividade, Analise analise, List<UsuarioAnalise> usuariosAnalise) throws ValidacaoException {

        if (usuariosAnalise.isEmpty())
            throw new RuntimeException(Mensagem.NENHUM_CHEFE_UNIDADE_ENCONTRADO.getTexto(analise.processo.numero, setorAtividade));

        List<Long> idsChefesUnidade = usuariosAnalise.stream()
                .map(cfu -> cfu.id)
                .collect(Collectors.toList());

        String parameter = "ARRAY[" + getParameterLongAsStringDBArray(idsChefesUnidade) + "]";

        String sql = "WITH t1 AS (SELECT 0 as count, id_usuario, now() as dt_vinculacao FROM unnest(" + parameter + ") as id_usuario ORDER BY id_usuario), " +
                "     t2 AS (SELECT * FROM t1 WHERE t1.id_usuario NOT IN (SELECT id_usuario FROM analise.chefe_unidade cu) LIMIT 1), " +
                "     t3 AS (SELECT count(id), id_usuario, min(data_vinculacao) as dt_vinculacao FROM analise.chefe_unidade " +
                "        WHERE id_usuario in (" + getParameterLongAsStringDBArray(idsChefesUnidade) + ") " +
                "        GROUP BY id_usuario " +
                "        ORDER BY 1, dt_vinculacao OFFSET 0 LIMIT 1) " +
                "SELECT * FROM (SELECT * FROM t2 UNION ALL SELECT * FROM t3) AS t ORDER BY t.count LIMIT 1;";


        Query consulta = JPA.em().createNativeQuery(sql, DistribuicaoProcessoVO.class);

        DistribuicaoProcessoVO distribuicaoProcessoVO = (DistribuicaoProcessoVO) consulta.getSingleResult();

        return new ChefeUnidade(analise, UsuarioAnalise.findById(distribuicaoProcessoVO.id));

    }

    public static ChefeUnidade findByAnalise(Long idAnalise) {

        return ChefeUnidade.find("id_analise = :analise")
                .setParameter("analise", idAnalise).first();
    }

    private static String getParameterLongAsStringDBArray(List<Long> lista) {

        String retorno = "";

        for (Long id : lista) {
            retorno = retorno + "" + id + ", ";
        }
        retorno = retorno.substring(0, retorno.length() -2) ;

        return retorno;
    }

}
