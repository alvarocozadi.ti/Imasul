package models.tramitacao;

import play.db.jpa.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

// View que possui os dados das condicao
@Entity
@Table(schema = "tramitacao", name = "VW_CONDICAO")
public class Condicao extends GenericModel {
	
	public static final Long SUSPENSO = 1l;
	public static final Long CANCELADO = 2l;
	public static final Long LICENCA_EMITIDA = 3l;
	public static final Long ANALISE_FINALIZADA = 4l;
	public static final Long AGUARDANDO_DISTRIBUICAO_CHEFE_UNIDADE = 5l;
	public static final Long AGUARDANDO_VINCULACAO_TECNICA_PELO_GERENTE = 6l;
	public static final Long AGUARDANDO_ANALISE_TECNICA = 7l;
	public static final Long AGUARDANDO_VALIDACAO_TECNICA_PELO_GERENTE = 8l;
	public static final Long AGUARDANDO_ASSINATURA_PRESIDENE = 9l;
	public static final Long AGUARDANDO_VALIDACAO_DIRETORIA = 10l;
	public static final Long AGUARDANDO_RESPOSTA_COMUNICADO = 11l;
	public static final Long AGUARDANDO_RESPOSTA_JURIDICA = 12l;
	public static final Long EM_ANALISE_TECNICA_GERENTE = 13l;
	public static final Long EM_ANALISE_DIRETOR = 14l;
	public static final Long EM_ANALISE_PRESIDENTE = 15l;
	public static final Long EM_ANALISE_GERENTE = 16l;
	public static final Long EM_ANALISE_CHEFE_UNIDADE = 17l;
	public static final Long SOLICITACAO_DESVINCULO_PENDENTE_ANALISE_TECNICA = 18l;
	public static final Long NOTIFICADO_PELO_ANALISTA_TECNICO = 19l;
	public static final Long SOLICITACAO_LICENCA_APROVADA = 20l;
	public static final Long SOLICITACAO_LICENCA_NEGADA = 21l;
	public static final Long ARQUIVADO = 22l;
	public static final Long EM_ANALISE_TECNICA = 23l;

	public static final Long NOTIFICADO_PELO_ANALISTA_GEO = 40l;
	public static final Long AGUARDANDO_VINCULACAO_TECNICA_PELO_COORDENADOR = 41l;
	public static final Long AGUARDANDO_VALIDACAO_TECNICA_PELO_COORDENADOR = 42l;
	public static final Long AGUARDANDO_VINCULACAO_GEO_PELO_GERENTE = 24l;
	public static final Long AGUARDANDO_ANALISE_GEO = 25l;
	public static final Long EM_ANALISE_GEO = 26l;
	public static final Long AGUARDANDO_VALIDACAO_GEO_PELO_GERENTE = 27l;
	public static final Long AGUARDANDO_VALIDACAO_GERENTE = 28l;
	public static final Long SOLICITACAO_DESVINCULO_PENDENTE_ANALISE_GEO = 30l;

	@Id
	@Column(name = "ID_CONDICAO")
	public Long idCondicao;

	@Column(name = "ID_FLUXO")
	public Long idFluxo;

	@Column(name = "ID_ETAPA")
	public Long idEtapa;

	@Column(name = "TX_ETAPA")
	public String nomeEtapa;

	@Column(name = "NM_CONDICAO")
	public String nomeCondicao;

	public String getNome() {

		String nome = this.nomeCondicao.replace("Manejo digital ", "");
		return nome.substring(0, 1).toUpperCase() + nome.substring(1);
	}
}
