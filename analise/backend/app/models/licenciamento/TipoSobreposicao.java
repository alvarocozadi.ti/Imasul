package models.licenciamento;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "licenciamento", name = "tipo_sobreposicao")
public class TipoSobreposicao extends GenericModel {


	public static final long AREA_INFLUENCIA_UC = 1L;
	public static final long AREA_USO_RESTRITO_PANTANAL = 2L;
	public static final long AREAS_PRIORITARIAS = 3L;
	public static final long BACIA_FORMOSO_E_PRATA = 4L;
	public static final long BACIA_PARAGUAIA = 5L;
	public static final long BACIA_PARANA = 6L;
	public static final long BIOMAMATAATLANTICA = 7L;
	public static final long BIOMAS = 8L;
	public static final long CORREDORES_ECOLOGICOS = 9L;
	public static final long ENTORNO_DO_TAQUARI = 10L;
	public static final long GEOLOGIA_2006 = 11L;
	public static final long GRADE_CBERS_CCD = 12L;
	public static final long GRADE_DAS_CARTAS_1_PARA_100_MIL = 13L;
	public static final long GRADE_DAS_CARTAS_1_PARA_250_MIL = 14L;
	public static final long MACROZONEAMENTO = 15L;
	public static final long MACROZONEAMENTO_SOLOS = 16L;
	public static final long MACROZONEAMENTO_SOLOS_AMOSTRAS = 17L;
	public static final long RIOS_DO_DOMINIO_DO_ESTADO_DE_MS_ANA = 18L;
	public static final long SITIOS_ARQUEOLOGICOS = 19L;
	public static final long TERRAS_INDIGENAS_EM_ESTUDO = 20L;
	public static final long TERRAS_INDIGENAS_FUNAI_2015 = 21L;
	public static final long UCS_MS_MOSAICO = 22L;
	public static final long UNIDADE_DE_PLANEJAMENTO_E_GERENCIAMENTO = 23L;
	public static final long UNIDADES_HIDROGELOLOGICAS = 24L;
	public static final long ZEE_MS = 25L;
	public static final long ZONA_AMORT_UC_MS = 26L;
	public static final long ZONA_AMORT_UC_MS_CONAMA_2KM = 27L;
	public static final long ZONA_AMORT_UC_MS_CONAMA_3KM = 28L;

	@Id
	public Long id;

	@Column
	public String codigo;

	@Column
	public String nome;

	@ManyToMany
	@JoinTable(schema = "licenciamento", name = "rel_tipo_sobreposicao_orgao",
			joinColumns = @JoinColumn(name = "id_tipo_sobreposicao"),
			inverseJoinColumns = @JoinColumn(name = "id_orgao"))
	public List<Orgao> orgaosResponsaveis;

}