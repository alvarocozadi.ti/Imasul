package models.licenciamento;

import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(schema = "licenciamento", name = "potencial_poluidor")
public class PotencialPoluidor extends Model implements Comparable<PotencialPoluidor>{

	public String nome;

	@Column(name = "valor")
	public Long valorPpd;


	@Override
	public int compareTo(PotencialPoluidor potencialPoluidor) {

		if(this.equals(potencialPoluidor)) {
			return 0;
		}

		if(this.valorPpd == 1) {

			return -1;

		} else if(this.valorPpd == 2) {

			if(potencialPoluidor.valorPpd == 1) {

				return 1;

			} else if(potencialPoluidor.valorPpd == 3) {

				return -1;

			}

		} else if(this.valorPpd == 3) {

			return 1;

		}

		return 0;
	}

}
