package models.licenciamento;

import models.Documento;
import org.apache.commons.io.FileUtils;
import play.db.jpa.GenericModel;
import play.libs.Crypto;
import utils.Configuracoes;
import utils.FileManager;
import utils.Identificavel;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Entity
@Table(schema = "licenciamento", name = "documento")
public class DocumentoLicenciamento extends GenericModel implements Identificavel {

	private static final String SEQ = "licenciamento.documento_id_seq";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ)
	@SequenceGenerator(name = SEQ, sequenceName = SEQ, allocationSize = 1)
	public Long id;
	
	public String caminho;
	
	@ManyToOne
	@JoinColumn(name="id_tipo_documento", referencedColumnName="id")
	public TipoDocumentoLicenciamento tipo;
	
	@Transient
	public String key;
	
	@Transient
	public String base64;
	
	@Transient
	public File arquivo;
	
	@Transient
	public String extensao;
	
	@Column(name="data_cadastro")
	public Date dataCadastro;
	
	@Override
	public Long getId() {
		
		return this.id;
	}

	@Override
	public DocumentoLicenciamento save() {

		if (this.id != null) // evitando sobrescrever algum documento.
			throw new IllegalStateException("Documento já salvo");

		this.caminho = "temp";

		this.dataCadastro = new Date();

		super.save();

		if (this.tipo == null)
			throw new IllegalStateException("Tipo do documento não preenchido.");

		criarPasta();

		if (this.key != null) {

			saveArquivoTemp();

		}else {

			throw new RuntimeException("Não é possível identificar o arquivo a ser salvo para o documento.");
		}

		super.save();

		return this;
	}

	private void saveArquivoTemp() {

		if (this.key == null)
			throw new IllegalStateException("Key do documento não preenchido.");

		saveArquivo(FileManager.getInstance().getFile(this.key));
	}


	private void saveArquivo(File file) {

		if (file == null || !file.exists())
			throw new IllegalStateException("Arquivo não existente.");

		try {
			FileManager fm = FileManager.getInstance();
			this.key = null;
			this.arquivo = null;
			this.extensao = fm.getFileExtention(file.getName());

			configurarCaminho();

			FileUtils.copyFile(file, new File(this.getCaminhoCompleto()));

		} catch (IOException e) {

			throw new RuntimeException(e);
		}
	}


	private void configurarCaminho() {

		this.caminho = File.separator + tipo.caminhoPasta
				+ File.separator + tipo.prefixoNomeArquivo + "_"
				+ this.id;

		if (this.extensao != null)
			this.caminho += "." + this.extensao;
	}


	private void criarPasta() {

		String caminho = Configuracoes.ARQUIVOS_DOCUMENTOS_PATH + File.separator + tipo.caminhoPasta;

		File pasta = new File(caminho);

		if (!pasta.exists())
			pasta.mkdirs();
	}

	private String getCaminhoCompleto() {
		
		return Configuracoes.ARQUIVOS_DOCUMENTOS_LICENCIAMENTO_PATH + File.separator + this.caminho;
	}
	
	public File getFile() {
		
		return new File(getCaminhoCompleto());
	}	
	
	public String getNome() {
		
		if (this.caminho.isEmpty())
			return "";
		
		return this.caminho.substring(this.caminho.lastIndexOf("/") + 1);
	}

}
