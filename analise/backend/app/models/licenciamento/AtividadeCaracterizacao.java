package models.licenciamento;

import com.vividsolutions.jts.geom.Geometry;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Entity
@Table(schema = "licenciamento", name = "atividade_caracterizacao")
public class AtividadeCaracterizacao extends GenericModel {

	private static final String SEQ = "licenciamento.atividade_caracterizacao_id_seq";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ)
	@SequenceGenerator(name = SEQ, sequenceName = SEQ, allocationSize = 1)
	public Long id;

	@ManyToOne
	@JoinColumn(name="id_atividade")
	public Atividade atividade;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<GeometriaAtividade> geometriasAtividade;

	@ManyToOne
	@JoinColumn(name = "id_caracterizacao", referencedColumnName = "id")
	public Caracterizacao caracterizacao;

	@ManyToOne
	@JoinColumn(name="id_porte_empreendimento")
	public PorteEmpreendimento porteEmpreendimento;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<AtividadeCaracterizacaoParametros> atividadeCaracterizacaoParametros;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<SobreposicaoCaracterizacaoAtividade> sobreposicoesCaracterizacaoAtividade;

	@OneToMany(mappedBy = "atividadeCaracterizacao", cascade = CascadeType.ALL)
	public List<SolicitacaoGrupoDocumento> documentosSolicitacaoGrupo;

	@Column(name = "is_principal")
	public Boolean isPrincipal;

	@Column(name = "distancia_capital")
	public Double distanciaCapital;

	@Column(name="valor_taxa_licenciamento")
	public Double valorTaxaLicenciamento;

	public String getNomeAtividade(){
		return this.atividade != null ? this.atividade.nome : "-";
	}

	public Boolean isAtividadeDentroEmpreendimento() {
		return this.atividade.dentroEmpreendimento;
	}

	public Stream<Geometry> getGeoms(){

		return this.geometriasAtividade.stream().map(ga -> ga.geometria);

	}

	public String getAreaDeclaradaInteressado() {

		AtividadeCaracterizacaoParametros atividadeCaracterizacaoParametro =  this.atividadeCaracterizacaoParametros.stream().filter(valor -> valor.parametroAtividade.codigo.equals("AU")).findAny().orElse(null);

		return (atividadeCaracterizacaoParametro != null) ? atividadeCaracterizacaoParametro.valorParametro.toString() : "-";

	}

	public static String getSiglaAtividadePrincipal(List<AtividadeCaracterizacao> atividadesCaracterizacao){

		List<String> siglaSetor = new ArrayList<>();
		atividadesCaracterizacao.forEach(atividadeCaracterizacao -> {

			if(atividadeCaracterizacao.isPrincipal)
				siglaSetor.add(atividadeCaracterizacao.atividade.siglaSetor);

		});
		return siglaSetor.get(0);
	}

}
