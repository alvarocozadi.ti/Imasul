package models.EntradaUnica;

public class CodigoPerfil {

    public static final String ANALISTA_CAR= "ANALISTA_CAR";
    public static final String ANALISTA_GEO = "ANALISTA_GEO";
    public static final String ANALISTA_TECNICO = "ANALISTA_TECNICO";
    public static final String DIRETOR = "DIRETOR";
    public static final String GERENTE = "GERENTE";
    public static final String PRESIDENTE = "PRESIDENTE";
    public static final String CHEFE_UNIDADE = "CHEFE_UNIDADE";

    //TODO SQUAD2 - Remover estes códigos de perfil após finalização do novo fluxo
    public static final String CONSULTOR_JURIDICO = "CONSULTOR_JURIDICO";
    public static final String COORDENADOR_TECNICO = "COORDENADOR_TECNICO";
    public static final String APROVADOR = "APROVADOR";

}
