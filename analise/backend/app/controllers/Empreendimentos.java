package controllers;

import br.ufla.lemaf.beans.historico.EmpreendimentoSobreposicao;
import models.licenciamento.Empreendimento;
import security.Acao;
import serializers.EmpreendimentoSerializer;
import services.IntegracaoEntradaUnicaService;

public class Empreendimentos extends InternalController {

	public static void buscaDadosGeoEmpreendimento(long idEmpreendimento) {

		returnIfNull(idEmpreendimento, "long");

//		TODO: quando criar as permissões do gerente, adicionar aqui e descomentar.
//		verificarPermissao(Acao.INICIAR_PARECER_GEO);

		renderJSON(Empreendimento.buscaDadosGeoEmpreendimento(idEmpreendimento), EmpreendimentoSerializer.getDadosGeoEmpreendimento);

	}

	public static void sobreposicoes(long idEmpreendimento){

		returnIfNull(idEmpreendimento, "long");

		renderJSON(Empreendimento.buscaSobreposicoesEmpreendimento(idEmpreendimento));

	}
}
