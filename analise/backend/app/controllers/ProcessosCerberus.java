package controllers;

import builders.ProcessoCerberusBuilder;

import models.migracaoCerberus.LicencaCerberus;
import models.migracaoCerberus.ProcessoCliente;
import security.Acao;
import security.Auth;
import serializers.ProcessoClienteCerberusSerializer;

import java.util.List;

public class ProcessosCerberus extends InternalController{

    public static void listWithFilter(ProcessoCerberusBuilder.FiltroProcessoCerberus filtro){

        verificarPermissao(Acao.LISTAR_PROCESSO);

        List processosList = ProcessoCliente.listWithFilter(filtro, Auth.getUsuarioSessao());

        renderJSON(processosList);
    }


    public static void  countWithFilter(ProcessoCerberusBuilder.FiltroProcessoCerberus filtro){

        verificarPermissao(Acao.LISTAR_PROCESSO);

        renderJSON(ProcessoCliente.countWithFilter(filtro, Auth.getUsuarioSessao()));
    }

    public static void findProcessoById (Integer idProcesso){

        ProcessoCliente processoCliente = ProcessoCliente.find("processo_id = :idProcesso")
                .setParameter("idProcesso",idProcesso)
                .first();

//        processoCliente.processoCerberus.licenca = LicencaCerberus.findLicencaByIdProcesso(idProcesso);

        renderJSON(processoCliente, ProcessoClienteCerberusSerializer.getInfo);

    }

}
