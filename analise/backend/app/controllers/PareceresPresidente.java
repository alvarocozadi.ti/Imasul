package controllers;

import exceptions.AppException;
import models.*;
import models.licenciamento.Caracterizacao;
import models.licenciamento.DocumentoLicenciamento;
import models.licenciamento.Licenca;
import play.db.jpa.JPA;
import serializers.ParecerDiretorSerializer;
import serializers.ParecerPresidenteSerializer;
import utils.Mensagem;

import javax.persistence.EntityTransaction;
import java.io.File;

public class PareceresPresidente extends InternalController {

	public static void concluirParecerPresidente(ParecerPresidente parecerPresidente) throws Exception {

		returnIfNull(parecerPresidente, "ParecerPresidente");

		Analise analise = Analise.findById(parecerPresidente.analise.id);

		UsuarioAnalise presidente = getUsuarioSessao();

		parecerPresidente.finalizar(analise, presidente);

		renderMensagem(Mensagem.ANALISE_CONCLUIDA_SUCESSO);

	}

	public static void findParecerByIdHistoricoTramitacao(Long idHistoricoTramitacao) {

		ParecerPresidente parecerPresidente = ParecerPresidente.find("idHistoricoTramitacao", idHistoricoTramitacao).first();

		renderJSON(parecerPresidente, ParecerPresidenteSerializer.findByIdHistoricoTramitacao);

	}

	public static void emitirLicenca(Long idCaracterizacao) {

		Licenca licenca = Licenca.find("byCaracterizacao", Caracterizacao.findById(idCaracterizacao)).first();

		if(idCaracterizacao != null && licenca.documento == null)
			Licenca.emitirLicenca(idCaracterizacao);

		renderMensagem(Mensagem.ANALISE_CONCLUIDA_SUCESSO);
	}

	public static void downloadLicenca(Long idCaracterizacao) throws Exception {

		Licenca licenca = Licenca.find("byCaracterizacao", Caracterizacao.findById(idCaracterizacao)).first();

		if(licenca.documento == null) {

			Licenca.emitirLicenca(idCaracterizacao);
			licenca = Licenca.find("byCaracterizacao", Caracterizacao.findById(idCaracterizacao)).first();
		}

		File file = null;
		file = ((DocumentoLicenciamento) DocumentoLicenciamento.findById(licenca.documento.id)).getFile();

		renderBinary(file, file.getName());

	}

	public static void assinar(Caracterizacao caracterizacao, String key){

		Caracterizacao caracterizacao1 = Caracterizacao.findById(caracterizacao.id);

		caracterizacao1.licenca.documento.key = key;
		caracterizacao1.save();

	}
}
