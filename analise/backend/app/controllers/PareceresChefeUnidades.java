package controllers;

import models.AnaliseTecnica;
import models.ParecerChefeUnidadeAnaliseTecnica;
import models.ParecerGerenteAnaliseTecnica;
import models.UsuarioAnalise;
import serializers.ParecerGerenteSerializer;
import utils.Mensagem;

import static controllers.GenericController.renderMensagem;
import static controllers.GenericController.returnIfNull;
import static controllers.InternalController.getUsuarioSessao;

public class PareceresChefeUnidades extends InternalController {

    public static void concluirParecerTecnicoChefeUnidade(ParecerChefeUnidadeAnaliseTecnica parecerChefeUnidadeAnaliseTecnica) {

        AnaliseTecnica analiseTecnica = AnaliseTecnica.findById(parecerChefeUnidadeAnaliseTecnica.analiseTecnica.id);

        UsuarioAnalise chefeUnidade = getUsuarioSessao();

        parecerChefeUnidadeAnaliseTecnica.finalizar(analiseTecnica, chefeUnidade);

        renderMensagem(Mensagem.ANALISE_CONCLUIDA_SUCESSO);

    }

    public static void findParecerByIdHistoricoTramitacao(Long idHistoricoTramitacao) {

        ParecerChefeUnidadeAnaliseTecnica parecerChefeAnaliseTecnica = ParecerChefeUnidadeAnaliseTecnica.find("idHistoricoTramitacao", idHistoricoTramitacao).first();

        renderJSON(parecerChefeAnaliseTecnica, ParecerGerenteSerializer.findByIdHistoricoTramitacao);

    }

}
