package models;

public class UsuarioAutenticacaoVO {

    public String username;
    public String password;

    public UsuarioAutenticacaoVO() {}

    public UsuarioAutenticacaoVO(String username, String password){

        this.username = username;
        this.password = password;

    }
}
