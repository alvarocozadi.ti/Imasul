package models.integracaoSiriema;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import deserializers.StringDeserializer;
import exceptions.WebServiceException;
import models.UsuarioAutenticacaoVO;
import play.Play;
import play.libs.WS;
import play.Logger;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static utils.Config.SIRIEMA_URL;

public class SiriemaWS {

    private static boolean atualizarUsuarios = Boolean.parseBoolean(
            Play.configuration.getProperty("integracaoSiriema.atualizarUsuarios"));

    /**
     * Requisição POST - (Retorna dados do usuário cadastrados no sistema a partir do login e senha).
     */
    public static final String USUARIOS_LOGIN_URL = SIRIEMA_URL + "/usuarios/login";

    /**
     * Requisição GET - (Envia e-mail com link para alteração de senha de usuário cadastrado no sistema SIRIEMA).
     */
    public static final String USUARIOS_RECUPERAR_SENHA_URL = SIRIEMA_URL + "/usuarios/recuperar-senha";

    public static final String SIRIEMA_CLIENT_ID = Play.configuration.getProperty("siriema.client.id");
    public static final String SIRIEMA_CLIENT_SECRET = Play.configuration.getProperty("siriema.client.secret");
    public static final String SIRIEMA_TOKEN_AUTENTICACAO = Play.configuration.getProperty("siriema.url.token.autenticacao");
    public static final String SIRIEMA_GRANT_TYPE = Play.configuration.getProperty("siriema.grant.type");

    private static final String FORMATO_DATA_COMPLETO = "dd/MM/yyyy HH:mm:ss";

    private SiriemaResponseTokenVO tokenSiriema;

    private GsonBuilder gsonBuilder = new GsonBuilder();
    private DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_COMPLETO);


    public SiriemaWS(){
        this.tokenSiriema = this.buscarTokenAcesso();
    }

    public SiriemaResponseTokenVO buscarTokenAcesso (){

        Map<String, String> paramsAuth = new HashMap<String, String>();

        paramsAuth.put("grant_type", SIRIEMA_GRANT_TYPE);
        paramsAuth.put("client_id", SIRIEMA_CLIENT_ID);
        paramsAuth.put("client_secret", SIRIEMA_CLIENT_SECRET);

        WS.WSRequest request = WS.url(SIRIEMA_TOKEN_AUTENTICACAO);

        request.setHeader("Content-Type","application/x-www-form-urlencoded");
        request.setHeader("Accept","application/json");
        request.setParameters(paramsAuth);

        WS.HttpResponse responseAuth = request.post();

        if (!responseAuth.success())
            throw new WebServiceException(responseAuth);

        Type type = new TypeToken<SiriemaResponseTokenVO>(){}.getType();
        SiriemaResponseTokenVO retornoVO = gsonBuilder.create().fromJson(responseAuth.getJson(), type);

        return retornoVO;
    }

    public PessoaSiriemaVO login(UsuarioAutenticacaoVO usuarioAutenticacaoVO) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("senha", usuarioAutenticacaoVO.password);
        params.put("login", usuarioAutenticacaoVO.username);

        Logger.info("Login: " + usuarioAutenticacaoVO.username);
        Logger.info("Senha: " + usuarioAutenticacaoVO.password);
        Logger.info("Token: " + this.tokenSiriema.access_token);

        WS.WSRequest request = WS.url(USUARIOS_LOGIN_URL);

        request.setHeader( "Authorization", "Bearer " + this.tokenSiriema.access_token);
        request.setHeader("Content-Type","application/json");
        request.setHeader("Accept","application/json");

        Logger.info("URL: " + USUARIOS_LOGIN_URL);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(String.class, new StringDeserializer())
                .create();

        if(params != null) {
            String jsonData = gson.toJson(params);
            Logger.info("JSON: " + jsonData);
            request.body(jsonData);
        }

        WS.HttpResponse response = request.post();

        if (!response.success())
            throw new WebServiceException(response);

        Type type = new TypeToken<PessoaSiriemaVO>(){}.getType();
        PessoaSiriemaVO pessoaSiriemaVO = gsonBuilder.create().fromJson(response.getJson(), type);

        return pessoaSiriemaVO;
    }


}
