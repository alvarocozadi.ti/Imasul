package models.integracaoSiriema;

public class EnderecoPessoaSiriemaVO {

    public String cep;

    public String bairro;

    public String cidade;

    public String numero;

    public String logradouro;

    public String uf;

    public String cidadeIBGE;
}
