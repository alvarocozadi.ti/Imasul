package models.integracaoSiriema;

import java.util.List;

public class PessoaSiriemaVO {

    public String tipo;

    public ContatoPessoaSiriemaVO contato;

    public EnderecoPessoaSiriemaVO endereco;

    public String dataUltimaAlteracao;

    public String nome;

    public String razaoSocial;

    public String cpf;

    public String cnpj;

    public String nomeMae;

    public String sexo;

    public String dataNascimento;

    public String dataInicioAtividade;

    public String estadoCivil;

    public String nacionalidade;

    public String naturalidade;

    public String profissao;

    public List<RepresentantesLegaisSiriemaVO> representantesLegais;
}
