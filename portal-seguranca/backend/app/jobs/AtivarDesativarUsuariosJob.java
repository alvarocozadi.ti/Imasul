package jobs;

import models.AgendaDesativacao;
import play.jobs.Job;
import play.jobs.On;

import java.util.List;

// Dispara todos os dias 0:01:00
@On("cron.ativarDesativarUsuarios")
public class AtivarDesativarUsuariosJob extends Job {

	@Override
	public void doJob() {

		deactivateUsuarios();

		activateUsuarios();

	}

	private void deactivateUsuarios() {

		List<AgendaDesativacao> desativacoes = AgendaDesativacao.find("dataInicio = CURRENT_DATE()").fetch();

		for(AgendaDesativacao agendaDesativacao : desativacoes) {

			agendaDesativacao.usuario.deactivate();

		}

	}

	private void activateUsuarios() {

		List<AgendaDesativacao> ativacoes = AgendaDesativacao.find("dataFim = CURRENT_DATE()").fetch();

		for(AgendaDesativacao agendaDesativacao : ativacoes) {

			agendaDesativacao.usuario.activate();

		}

	}

}
