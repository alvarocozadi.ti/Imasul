package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import exceptions.WebServiceException;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

import java.util.Map;

public class WebService {
    private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();

    public HttpResponse get(String url) {

        return WS.url(url).get();
    }

    public HttpResponse get(String url, String authenticate) {

        WSRequest request = WS.url(url);
        request.setHeader("www-authenticate", authenticate);

        return request.get();
    }

    public HttpResponse get(String url, Map<String, Object> params) {

        WSRequest request = WS.url(url);

        if (params != null && !params.isEmpty()) {
            request.params(params);
        }

        return request.get();
    }

    public HttpResponse post(String url, Map<String, Object> params) {

        WSRequest request = WS.url(url);

        if (params != null && !params.isEmpty()) {
            request.params(params);
        }

        return request.post();
    }

    public HttpResponse post(String url, String data, String contentType) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type",contentType);

        if (data != null)
            request.body(data);

        return request.post();
    }

    public HttpResponse post(String url, String data, String contentType, String authenticate) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type",contentType);
        request.setHeader("www-authenticate", authenticate);

        if (data != null)
            request.body(data);

        return request.post();
    }

    public <T> T postJSON(String url, Object data, Class<T> responseType) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type","application/json");

        if (data != null) {
            String jsonData = gson.toJson(data);
            request.body(jsonData);
        }

        HttpResponse response = request.post();

        if (!response.success())
            throw new WebServiceException(response);

        JsonElement responseJson = response.getJson();

        if (responseJson.isJsonObject()) {

            JsonObject json = (JsonObject) responseJson;
            return gson.fromJson(json, responseType);

        } else if (responseJson.isJsonArray()) {

            return gson.fromJson(responseJson.getAsJsonArray(), responseType);
        }

        return null;
    }

    public <T> T postJSON(String url, String jsonData, Class<T> responseType) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type","application/json");

        if(jsonData == null)
            throw new IllegalArgumentException("Json está null ou não foi informado!");

        request.body(jsonData);

        HttpResponse response = request.post();

        if (!response.success())
            throw new WebServiceException(response);

        JsonElement responseJson = response.getJson();

        if (responseJson.isJsonObject()) {

            JsonObject json = (JsonObject) responseJson;
            return gson.fromJson(json, responseType);

        } else if (responseJson.isJsonArray()) {

            return gson.fromJson(responseJson.getAsJsonArray(), responseType);
        }

        return null;
    }

    public <T> T postJsonAuthenticate(String url, String jsonData, Class<T> responseType, String authenticate) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type","application/json");
        request.setHeader("www-authenticate", authenticate);

        if(jsonData == null)
            throw new IllegalArgumentException("Json está null ou não foi informado!");

        request.body(jsonData);

        HttpResponse response = request.post();

        if (!response.success())
            throw new WebServiceException(response);

        JsonElement responseJson = response.getJson();

        if (responseJson.isJsonObject()) {

            JsonObject json = (JsonObject) responseJson;
            return gson.fromJson(json, responseType);

        } else if (responseJson.isJsonArray()) {

            return gson.fromJson(responseJson.getAsJsonArray(), responseType);
        }

        return null;
    }


    public HttpResponse postJson(String url, Object data) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type","application/json");

        if (data != null) {
            String jsonData = gson.toJson(data);
            request.body(jsonData);
        }

        return request.post();
    }

    public HttpResponse postSiriema(String url, Object data) {

        WSRequest request = WS.url(url);
        request.setHeader("Content-Type","application/x-www-form-urlencoded");
        request.setHeader("Accept","application/json");

        if (data != null)
            request.body(data);

        return request.post();
    }

}
