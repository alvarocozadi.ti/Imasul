package services;

import com.google.gson.JsonObject;
import models.Usuario;
import play.libs.WS;

public class ExternalPessoaService extends BaseExternalService {

	protected static String HAS_PESSOA_WITH_LOGIN_PATH = "usuario/pessoaEstaBloqueado/";
	protected static String FIND_PESSOA_BY_LOGIN = "usuario/pessoa/";

	public static Boolean isPessoaBloqued(String login) {

		WS.WSRequest wsRequest = WS
				.url(cadastroUnificadoUrl + "/public/" + HAS_PESSOA_WITH_LOGIN_PATH + login);

		WS.HttpResponse httpResponse = wsRequest.get();

		verifyResponse(httpResponse);

		return Boolean.valueOf(httpResponse.getString());

	}

	public static Usuario findPessoaByLoginForHeader(String login) {

		WS.WSRequest wsRequest = WS
				.url(cadastroUnificadoUrl + "/public/" + FIND_PESSOA_BY_LOGIN + login);

		WS.HttpResponse httpResponse = wsRequest.get();

		verifyResponse(httpResponse);

		Usuario usuario = new Usuario();
		JsonObject pessoa = httpResponse.getJson().getAsJsonObject().get("pessoa").getAsJsonObject();
		usuario.pessoaId = Integer.parseInt(pessoa.get("id").getAsString());

		usuario.getNomeByObject(pessoa);

		usuario.getRazaoSocialByObject(pessoa);

		return usuario;

	}

}
