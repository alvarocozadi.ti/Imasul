package exceptions;

public class IntegracaoSiriemaException extends Exception {

    public IntegracaoSiriemaException (String msg) {
        super(msg);
    }
}

