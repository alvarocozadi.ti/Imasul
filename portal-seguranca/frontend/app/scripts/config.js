(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/',
		LOGIN_REDIRECT_URL: '/',
		CADASTRO_UNIFICADO: 'http://localhost:9901/',
		COOKIE_DOMAIN: 'localhost',
		COOKIE_PATH: '/'
	});

})();