(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/portal-seguranca/',
		LOGIN_REDIRECT_URL: '/portal-seguranca/',
		CADASTRO_UNIFICADO: 'http://sistemas.ipaam.am.gov.br/cadastro-unificado/',
		COOKIE_DOMAIN: 'ipaam.am.gov.br',
		COOKIE_PATH: '/portal-seguranca/'
	});

})();