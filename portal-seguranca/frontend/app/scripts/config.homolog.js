(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL:'/portal-seguranca/',
		LOGIN_REDIRECT_URL:'/portal-seguranca/',
		CADASTRO_UNIFICADO:'http://homologacao.licenciamento.imasul.ms.gov.br/cadastro-unificado/',
		COOKIE_DOMAIN:'imasul.ms.gov.br',
		COOKIE_PATH: '/portal-seguranca/'
	});

})();