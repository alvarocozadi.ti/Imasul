(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/portal-seguranca/',
		LOGIN_REDIRECT_URL: '/portal-seguranca/',
		CADASTRO_UNIFICADO: 'http://imasul.ti.lemaf.ufla.br/cadastro-unificado/',
		COOKIE_DOMAIN: '.ti.lemaf.ufla.br',
		COOKIE_PATH: '/portal-seguranca/'
	});

})();