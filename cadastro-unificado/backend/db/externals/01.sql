# --- !Ups
-- necessário ter rodado as evolutions do portal_seguranca 
INSERT INTO portal_seguranca.cliente_oauth VALUES (1, '8da81783e735480b29881f1a7b1a9f4499a95c8b3d054b6d7f28674eff03d97cc90815a48ce5ccd09a46ac2e540b8f0d538dede20dc0284108026cb3d870f22c', '2e22a26f02d24a0c18b52cd984e62d2c2f476024b89aaa6512d26ecc41536fa2d0caa123fc796f8f2dc2c09fac97002ac09c3d13f665085e911690ee51b0a5b3', 0, null, null, null, now());
SELECT setval('portal_seguranca.sq_cliente_oauth', 1, TRUE);

INSERT INTO portal_seguranca.modulo(id, data_cadastro, descricao, nome, sigla, ativo, url, ips, id_cliente_oauth) VALUES (2, now(), 'Cadastro unificado de dados a serem utilizados por vários módulos.', 'Cadastro Unificado', 'MCU', true, 'http://localhost:9901/#/', '127.0.0.1,0:0:0:0:0:0:0:1', 1);
SELECT setval('portal_seguranca.sq_modulo', 2, TRUE);

-- AÇÕES DO SISTEMA
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (101, 'Cadastro de Pessoa Fisica', 'cadastrarPessoaFisica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (102, 'Listar Paises', 'listarPaises');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (103, 'Listar Estados', 'listarEstados');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (104, 'Listar Municipios', 'listarMunicipios');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (105, 'Listar Tipos de Contato', 'listarTiposContato');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (106, 'Filtro de Pessoa Fisica', 'filtrarPessoaFisica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (107, 'Visualiza pessoa física', 'visualizarPessoaFisica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (108, 'Editar pessoa física', 'editarPessoaFisica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (109, 'Remover Pessoa Física', 'removerPessoaFisica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (110, 'Cadastrar pessoa jurídica', 'cadastrarPessoaJuridica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (111, 'Listar pessoa jurídica', 'listarPessoasJuridicas');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (112, 'Visualizar pessoa jurídica', 'visualizarPessoaJuridica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (113, 'Remover pessoa jurídica', 'removerPessoaJuridica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (114, 'Editar pessoa jurídica', 'editarPessoaJuridica');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (115, 'Cadastrar empreendimento', 'cadastrarEmpreendimento');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (116, 'Listar empreendimentos', 'listarEmpreendimentos');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (117, 'Visualizar empreendimento', 'visualizarEmpreendimento');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (118, 'Remover empreendimento', 'removerEmpreendimento');
INSERT INTO portal_seguranca.acao_sistema(id, descricao, permissao) VALUES (119, 'Editar empreendimento', 'editarEmpreendimento');

-- PERMISSÕES
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (101, 'cadastrarPessoaFisica', now(), 'Cadastrar Pessoa Física', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (102, 'listarPaises', now(), 'Listar Paises', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (103, 'listarEstados', now(), 'Listar Estados', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (104, 'listarMunicipios', now(), 'Listar Municipios', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (105, 'listarTiposContato', now(), 'Listar Tipos de Contato', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (106, 'listarPessoasFisicas', now(), 'Listar Pessoa Física', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (107, 'visualizarPessoaFisica', now(), 'Visualizar pessoa física', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (108, 'editarPessoaFisica', now(), 'Editar pessoa física', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (109, 'removerPessoaFisica', now(), 'Remover Pessoa Física', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (110, 'cadastrarPessoaJuridica', now(), 'Cadastrar pessoa jurídica', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (111, 'listarPessoasJuridicas', now(), 'Listar pessoa jurídica', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (112, 'visualizarPessoaJuridica', now(), 'Visualizar pessoa jurídica', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (113, 'removerPessoaJuridica', now(), 'Remover pessoa jurídica', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (114, 'editarPessoaJuridica', now(), 'Editar pessoa jurídica', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (115, 'cadastrarEmpreendimento', now(), 'Cadastrar empreendimento', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (116, 'listarEmpreendimentos', now(), 'Listar empreendimentos', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (117, 'visualizarEmpreendimento', now(), 'Visualizar empreendimento', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (118, 'removerEmpreendimento', now(), 'Remover empreendimento', 2);
INSERT INTO portal_seguranca.permissao(id, codigo, data_cadastro, nome, id_modulo) VALUES (119, 'editarEmpreendimento', now(), 'Editar empreendimento', 2);
SELECT setval('portal_seguranca.sq_permissao', 119, true);

-- PERFIL-PERMISSÃO
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 101);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 102);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 103);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 104);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 105);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 106);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 107);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 108);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 109);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 110);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 111);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 112);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 113);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 114);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 115);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 116);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 117);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 118);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Administrador'), 119);

GRANT USAGE ON SCHEMA portal_seguranca TO cadastro_unificado;
GRANT SELECT on portal_seguranca.usuario to cadastro_unificado;
GRANT SELECT on portal_seguranca.perfil to cadastro_unificado;
GRANT SELECT on portal_seguranca.permissao to cadastro_unificado;
GRANT SELECT on portal_seguranca.permissao_perfil to cadastro_unificado;
GRANT SELECT on portal_seguranca.perfil_usuario to cadastro_unificado;
GRANT SELECT on portal_seguranca.agenda_desativacao to cadastro_unificado;
GRANT SELECT on portal_seguranca.motivo to cadastro_unificado;

-- alterar avatar de perfil Gestor
UPDATE portal_seguranca.perfil SET avatar = NULL WHERE nome = 'Administrador' and id_modulo_pertencente = 1;

-- alterar avatar de perfil Externo
UPDATE portal_seguranca.perfil SET avatar = NULL WHERE nome = 'Externo' and id_modulo_pertencente = 1;

-- Inserir perfil Gestor
INSERT INTO portal_seguranca.perfil(id, avatar, data_cadastro, nome, id_modulo_pertencente) 
VALUES (nextval('portal_seguranca."sq_perfil"'), NULL, now(), 'Gestor', 2);

-- Gestão de pessoa física
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 101);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 106);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 107);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 108);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 109);

-- Gestão de localização
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 102);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 103);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 104);

-- Gestão de contatos
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 105);

-- Gestão de pessoa jurídica
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 110);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 112);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 113);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 114);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 111);

-- Gestão de empreendimento
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 117);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 118);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 119);

-- Gestão de usuário
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 4);

-- Empreendimento cadastro e listagem

INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 115);
INSERT INTO portal_seguranca.permissao_perfil(id_perfil, id_permissao) VALUES ((select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2), 116);

GRANT SELECT on portal_seguranca.modulo to cadastro_unificado;

-- Alteração de Nomes dos perfis padrão do sistema

UPDATE portal_seguranca.perfil SET nome = 'Administrador - EU' WHERE nome = 'Administrador' and id = 1 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET nome = 'Público' WHERE nome = 'Externo' and id = 2 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET nome = 'Gestor - EU' WHERE nome = 'Gestor' and id_modulo_pertencente = 2;
UPDATE portal_seguranca.perfil SET codigo = 'administradorEu' WHERE nome = 'Administrador - EU' and id = 1 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET codigo = 'publico' WHERE nome = 'Público' and id = 2 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET codigo = 'gestorEu' WHERE nome = 'Gestor - EU' and id_modulo_pertencente = 2;

# --- !Downs

UPDATE portal_seguranca.perfil SET nome = 'Administrador' WHERE nome = 'Administrador - EU' and id = 1 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET nome = 'Externo' WHERE nome = 'Público' and id = 2 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET nome = 'Gestor' WHERE nome = 'Gestor - EU' and id_modulo_pertencente = 2;
UPDATE portal_seguranca.perfil SET codigo = NULL WHERE nome = 'Administrador - EU' and id = 1 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET codigo = NULL WHERE nome = 'Público' and id = 2 and id_modulo_pertencente = 1;
UPDATE portal_seguranca.perfil SET codigo = NULL WHERE nome = 'Gestor - EU' and id_modulo_pertencente = 2;

REVOKE SELECT on portal_seguranca.modulo FROM cadastro_unificado;

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2) 
AND id_permissao IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 4);

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (select id from portal_seguranca.perfil where nome = 'Gestor' and id_modulo_pertencente = 2) 
AND id_permissao IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119);

DELETE FROM portal_seguranca.perfil WHERE nome = 'Gestor' AND id_modulo_pertencente = 2;

-- alterar avatar de perfil Gestor
UPDATE portal_seguranca.perfil SET avatar = '1.png' WHERE nome = 'Administrador' and id_modulo_pertencente = 1;

-- alterar avatar de perfil Externo
UPDATE portal_seguranca.perfil SET avatar = '1.png' WHERE nome = 'Externo' and id_modulo_pertencente = 1;

REVOKE USAGE ON SCHEMA portal_seguranca FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.usuario FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.perfil FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.permissao FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.permissao_perfil FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.perfil_usuario FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.agenda_desativacao FROM cadastro_unificado;
REVOKE SELECT on portal_seguranca.motivo FROM cadastro_unificado;

DELETE FROM portal_seguranca.permissao_perfil WHERE id_perfil = (select id from portal_seguranca.perfil where nome = 'Administrador') 
AND id_permissao IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119);

DELETE FROM portal_seguranca.permissao WHERE id IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119);

SELECT * FROM portal_seguranca.permissao WHERE id IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119);

SELECT setval('portal_seguranca.sq_permissao', 100, true);

DELETE FROM portal_seguranca.acao_sistema WHERE id IN (101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113,
114, 115, 116, 117, 118, 119);

DELETE FROM portal_seguranca.modulo WHERE id = 2;

SELECT setval('portal_seguranca.sq_modulo', 1, TRUE);

DELETE FROM portal_seguranca.cliente_oauth WHERE id = 1;

SELECT setval('portal_seguranca.sq_cliente_oauth', 1, TRUE);