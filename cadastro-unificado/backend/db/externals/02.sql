# --- !Ups

-- Rodar em ambientes de desenvolvimento
UPDATE portal_seguranca.modulo SET url = 'http://localhost:9901/login' WHERE sigla = 'MCU';

--  Rodar em ambientes de teste
UPDATE portal_seguranca.modulo SET url = 'http://gt4.ti.lemaf.ufla.br/cadastroUnificado/login' WHERE sigla = 'MCU';

--  Rodar em ambientes de homolog
UPDATE portal_seguranca.modulo SET url = 'http://homologacao.licenciamento.imasul.ms.gov.br/cadastro-unificado/login' WHERE sigla = 'MCU';

--  Rodar em ambientes de prod
UPDATE portal_seguranca.modulo SET url = 'http://licenciamento.imasul.ms.gov.br/cadastro-unificado/login' WHERE sigla = 'MCU';


# --- !Downs

UPDATE portal_seguranca.modulo SET url = null WHERE sigla = 'MCU';