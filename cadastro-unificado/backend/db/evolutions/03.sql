# --- !Ups

ALTER TABLE cadastro_unificado.empreendimento ADD COLUMN cpf_cnpj_cadastrante TEXT;
COMMENT ON COLUMN cadastro_unificado.empreendimento.cpf_cnpj_cadastrante IS 'CPF ou CNPJ da pessoa cadastrante do empreendimento';

ALTER TABLE cadastro_unificado.pessoa_juridica ADD COLUMN is_junta BOOLEAN DEFAULT FALSE;
COMMENT ON COLUMN cadastro_unificado.pessoa_juridica.is_junta IS 'Campo para verificar se o CNPJ vem da junta comercial';

create table cadastro_unificado.integracao_siriema (
    id_integracao_siriema serial not null,
    dt_integracao         timestamp not null,
    dt_inicial            timestamp not null,
    dt_final              timestamp not null,
    cod_ultima_pagina     integer,
    tx_descricao          text,
    tx_erro               text,
    tp_status             integer not null,
    constraint pk_integracao_siriema
        primary key (id_integracao_siriema)
);
comment on table cadastro_unificado.integracao_siriema is 'Entidade que armazena as integrações com o Siriema. Cada uma das integrações executadas é responsável por buscar usuários/pessoas no Siriema de um intervalo de tempo e trazê-los para o Cadastro Unificado, sendo que cada uma dessas execuções corresponde a um registro nesta tabela.';
comment on column cadastro_unificado.integracao_siriema.id_integracao_siriema is 'Identificador único da entidade.';
comment on column cadastro_unificado.integracao_siriema.dt_integracao is 'Data de execução da integração com o Siriema.';
comment on column cadastro_unificado.integracao_siriema.dt_inicial is 'Data inicial utilizada para filtrar os dados que serão integrados a partir da data informada.';
comment on column cadastro_unificado.integracao_siriema.dt_final is 'Data final utilizada para filtrar os dados que serão integrados até a data informada.';
comment on column cadastro_unificado.integracao_siriema.cod_ultima_pagina is 'Armazena a página em que a integração parou.';
comment on column cadastro_unificado.integracao_siriema.tx_descricao is 'Descrição da integração executado.';
comment on column cadastro_unificado.integracao_siriema.tx_erro is 'Armazena o erro ocorrido durante a execução da integração.';
comment on column cadastro_unificado.integracao_siriema.tp_status is 'Indica o status da integração: Pendente (1), Finalizado (2) ou Erro (3).';

create sequence cadastro_unificado.integracao_siriema_id_seq;
alter sequence cadastro_unificado.integracao_siriema_id_seq owner to postgres;
grant select, usage on sequence cadastro_unificado.integracao_siriema_id_seq to cadastro_unificado;


# --- !Downs

DROP TABLE cadastro_unificado.integracao_siriema;

ALTER TABLE cadastro_unificado.pessoa_juridica DROP COLUMN is_junta;

ALTER TABLE cadastro_unificado.empreendimento DROP COLUMN cpf_cnpj_cadastrante;


