# --- !Ups

CREATE DATABASE utilitarios_ms
WITH TEMPLATE template1;

CREATE EXTENSION postgis;

CREATE ROLE cadastro_unificado LOGIN
ENCRYPTED PASSWORD 'cadastro_unificado'
SUPERUSER INHERIT NOCREATEDB NOCREATEROLE;

# --- !Downs

DROP ROLE cadastro_unificado;

DROP DATABASE utilitarios_ms;