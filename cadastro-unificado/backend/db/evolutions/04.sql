# --- !Ups

CREATE TABLE cadastro_unificado.update_redesim_controle (
    id SERIAL NOT NULL,
    data_inicio TIMESTAMP NOT NULL,
    data_fim TIMESTAMP NOT NULL,
    qnt_registros_processados INTEGER NOT NULL,
    id_ultima_pj_processada INTEGER NOT NULL,
    CONSTRAINT pk_update_redesim_controle 
        PRIMARY KEY (id),
    CONSTRAINT fk_urc_pessoa_juridica FOREIGN KEY (id)
        REFERENCES cadastro_unificado.pessoa_juridica 
);
COMMENT ON TABLE cadastro_unificado.update_redesim_controle is 'Entidade para controle de integração com o serviço WSE031 da Redesim.';
COMMENT ON COLUMN cadastro_unificado.update_redesim_controle.id is 'Identificador único da entidade.';
COMMENT ON COLUMN cadastro_unificado.update_redesim_controle.data_inicio is 'Data inicial que o job entrou em execução.';
COMMENT ON COLUMN cadastro_unificado.update_redesim_controle.data_fim is 'Data final que o job parou a execução.';
COMMENT ON COLUMN cadastro_unificado.update_redesim_controle.qnt_registros_processados is 'Quantidade de pessoas jurídicas.';
COMMENT ON COLUMN cadastro_unificado.update_redesim_controle.id_ultima_pj_processada is 'Referência para a entidade pessoa juridica, identificador da última pessoa juridica executada.';
ALTER TABLE cadastro_unificado.update_redesim_controle OWNER TO postgres;
GRANT SELECT,USAGE ON SEQUENCE cadastro_unificado.update_redesim_controle_id_seq TO cadastro_unificado;


# --- !Downs

DROP TABLE update_redesim_controle;

