# --- !Ups

CREATE TABLE cadastro_unificado.empreendedor (
    id serial NOT NULL,
    id_pessoa integer NOT NULL,
    ativo boolean DEFAULT true,
    data_cadastro timestamp DEFAULT now(),
    CONSTRAINT pk_empreendedor PRIMARY KEY (id),
    CONSTRAINT fk_e_pessoa FOREIGN KEY (id_pessoa)
        REFERENCES cadastro_unificado.pessoa
);
COMMENT ON TABLE cadastro_unificado.empreendedor IS 'Entidade responsavel por armazenar as informações referentes as pessoas que são empreendedores.';
COMMENT ON COLUMN cadastro_unificado.empreendedor.id IS 'Identificador único da entidade.';
COMMENT ON COLUMN cadastro_unificado.empreendedor.id_pessoa IS 'Referência para a entidade pessoa que realizará o relacionamento entre pessoa e empreendedor.';
COMMENT ON COLUMN cadastro_unificado.empreendedor.ativo IS 'Indica se o empreendedor está ativo (TRUE - Ativo; FALSE - Inativo).';
COMMENT ON COLUMN cadastro_unificado.empreendedor.data_cadastro IS 'Data de cadastro do empreendedor.';
ALTER TABLE cadastro_unificado.empreendedor OWNER TO postgres;
GRANT SELECT,USAGE ON SEQUENCE cadastro_unificado.empreendedor_id_seq TO cadastro_unificado;


ALTER TABLE cadastro_unificado.empreendimento
    ADD COLUMN id_empreendedor INT,
    ADD CONSTRAINT fk_empreendimento_empreendedor FOREIGN KEY (id_empreendedor)
        REFERENCES cadastro_unificado.empreendedor;
COMMENT ON COLUMN cadastro_unificado.empreendimento.id_empreendedor IS 'Referência para a entidade empreendedor que realizará o relacionamento entre empreendimento e empreendedor';


# --- !Downs

ALTER TABLE cadastro_unificado.empreendimento DROP CONSTRAINT fk_empreendimento_empreendedor;

ALTER TABLE cadastro_unificado.empreendimento DROP COLUMN id_empreendedor;

DROP TABLE cadastro_unificado.empreendedor;