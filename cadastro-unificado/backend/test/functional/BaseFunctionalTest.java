package functional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vividsolutions.jts.geom.Geometry;
import common.models.Pagination;
import deserializers.DateDeserializer;
import deserializers.GeometryDeserializer;
import deserializers.PessoaDeserializer;
import deserializers.TipoEnumDeserializer;
import flexjson.JSONSerializer;
import models.Pessoa;
import models.Tipo;
import org.junit.AfterClass;
import play.Play;
import play.cache.Cache;
import play.db.jpa.JPA;
import play.mvc.Http.Response;
import play.mvc.Http.StatusCode;
import play.test.FunctionalTest;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;
import utils.AssertionUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public abstract class BaseFunctionalTest extends FunctionalTest {

	protected static Gson gson;
	public static String BASE_URL = "http://localhost:" + Play.configuration.getProperty("http.port");

	static {

		GsonBuilder builder = new GsonBuilder();

		// Register here the adapters
		builder.registerTypeAdapter(Date.class, new DateDeserializer());
		builder.registerTypeAdapter(Pessoa.class, new PessoaDeserializer());
		builder.registerTypeHierarchyAdapter(Tipo.class, new TipoEnumDeserializer());
		builder.registerTypeAdapter(Geometry.class, new GeometryDeserializer());
		builder.setDateFormat("dd/MM/yyyy HH:mm:ss");

		gson = builder.create();

	}

	protected static void beginTransaction() {

		if (!JPA.isInsideTransaction())
			JPA.em().getTransaction().begin();

	}

	@AfterClass
	public static void close() {

		if (JPA.isInsideTransaction())
			JPA.em().getTransaction().commit();

	}

	protected static void commitTransaction() {

		if (JPA.isInsideTransaction()) {

			JPA.em().getTransaction().commit();
			JPA.em().getTransaction().begin();

		}

	}

	protected static Response login(String login, String profile) {

		Cache.clear();
		Map<String, String> map = new HashMap<>();
		map.put("username", login);
		map.put("profile", profile);

		return POST("/login", map);

	}

	public static void logout() {

		GET("/logout");

	}

	protected static <T> T responseToObject(Response response, Class<T> clazz) {

		assertIsOk(response);
		assertContentType("application/json", response);

		String content = getContent(response);

		return gson.fromJson(content, clazz);

	}

	protected static <T> List<T> responseToListOf(Response response, Class<T> clazz) {

		assertIsOk(response);
		assertContentType("application/json", response);

		String content = getContent(response);

		Type type = ParameterizedTypeImpl.make(List.class, new Type[] { clazz }, null);

		return gson.fromJson(content, type);

	}

	protected static <T> Pagination<T> responseToPaginationOf(Response response, Class<T> clazz) {

		assertIsOk(response);
		assertContentType("application/json", response);

		String content = getContent(response);

		Type type = ParameterizedTypeImpl.make(Pagination.class, new Type[] { clazz }, null);

		return gson.fromJson(content, type);

	}

	public static <T> Response POSTJson(String url, T data, JSONSerializer serializer) {

		String json;

		if (serializer != null)
			json = serializer.serialize(data);
		else
			json = gson.toJson(data);

		return POST(url, "application/json", json);

	}

	public static <T> Response POSTJson(String url, T data) {
		return POSTJson(url, data, null);
	}

	public static <T> Response PUTJson(String url, T data, JSONSerializer serializer) {

		String json;

		if (serializer != null)
			json = serializer.serialize(data);
		else
			json = gson.toJson(data);

		return PUT(url, "application/json", json);

	}

	public static <T> Response PUTJson(String url, T data) {
		return PUTJson(url, data, null);
	}

	protected void assertFileEquals(File expects, File actuals) throws FileNotFoundException, IOException {
		AssertionUtils.assertFileEquals(expects, actuals);
	}

	protected void assertIsForbidden(Response response) {
		assertStatus(StatusCode.FORBIDDEN, response);
	}

	protected void assertIsUnauthorized(Response response) {
		assertStatus(StatusCode.UNAUTHORIZED, response);
	}

	protected void assertIsValidationError(Response response) {
		assertStatus(422, response);
	}

	protected void assertIsError(Response response, String message) {

//		assertStatus(StatusCode.INTERNAL_ERROR, response);
		assertStatus(504, response);

		String content = getContent(response);
		assertEquals(message, content.trim());

	}

	protected void assertIsValidationError(Response response, String message) {

//		assertStatus(StatusCode.INTERNAL_ERROR, response);
		assertStatus(422, response);

		String content = getContent(response);
		assertEquals(message, content.trim());

	}

	protected void assertNotEmpty(Collection<?> collection) {

		assertNotNull(collection);
		assertFalse(collection.isEmpty());

	}

}
