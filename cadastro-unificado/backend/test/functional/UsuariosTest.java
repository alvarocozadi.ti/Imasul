package functional;

import org.junit.Test;
import play.mvc.Http.Response;
import play.mvc.Router;

import java.util.List;

public class UsuariosTest extends BaseFunctionalTest {

	@Test
	public void deveRetornarAcoesPermitidas() {

//		login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

		String url = Router.reverse("Usuarios.getAcoesPermitidas").url;

		assertEquals("/usuario/acoesPermitidas", url);

		Response response = GET(url);

		List<Integer> idsAcoesPermitidas = responseToListOf(response, Integer.class);

		assertNotNull(idsAcoesPermitidas);

	}

	@Test
	public void naoDeveRetornarAcoesUsuarioNaoAutenticado() {

		logout();

		String url = Router.reverse("Usuarios.getAcoesPermitidas").url;

		assertEquals("/usuario/acoesPermitidas", url);

		Response response = GET(url);

		assertIsUnauthorized(response);

	}

}

