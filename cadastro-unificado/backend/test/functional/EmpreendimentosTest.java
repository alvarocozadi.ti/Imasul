package functional;

import builders.EmpreendimentoBuilder;
import common.models.Message;
import common.models.Pagination;
import functional.serializers.EmpreendimentosSerializer;
import models.*;
import org.junit.BeforeClass;
import org.junit.Test;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Http.Response;
import play.mvc.Router;
import utils.DatabaseCleaner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmpreendimentosTest  {

	public static class Cadastro extends BaseFunctionalTest {

		private static String url;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			url = Router.reverse("Empreendimentos.create").url;
			assertEquals("/empreendimento", url);

		}

		@Test
		public void deveCadastrar() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = POSTJson(url, empreendimento, EmpreendimentosSerializer.create);

			assertIsOk(response);

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("empreendimento.cadastrar.sucesso"), message.getText());

		}

		@Test
		public void naoDeveCadastrarSemPermissao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = POSTJson(url, empreendimento, EmpreendimentosSerializer.create);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveCadastrarSemAutenticacao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build();

			Response response = POSTJson(url, empreendimento, EmpreendimentosSerializer.create);

			assertIsUnauthorized(response);

		}

		@Test
		public void naoDeveCadastrarSemPermissaoParaCadastrarPessoa() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO_CADASTRO);

			Response response = POSTJson(url, empreendimento, EmpreendimentosSerializer.create);

			assertIsForbidden(response);

		}

	}

	public static class Listagem extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveFiltrar() {

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaFisicaSalva()
						.save();

			}

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaJuridicaSalva()
						.save();

			}

			commitTransaction();

			FiltroEmpreendimento filtro = new FiltroEmpreendimento();
			filtro.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;
			filtro.busca = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			String url = Router.reverse("Empreendimentos.findByFilter").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimentos", url);

			Response response = POSTJson(url, filtro, EmpreendimentosSerializer.findByFilter);

			assertIsOk(response);

			Pagination paginacao = responseToPaginationOf(response, Empreendimento.class);

			assertNotNull(paginacao.getTotalItems());
			assertNotNull(paginacao.getPageItems());

			assertEquals(20, paginacao.getPageItems().size());

			List<Empreendimento> empreendimentos = paginacao.getPageItems();
			List<Endereco> enderecos = new ArrayList<>();
			enderecos.addAll(empreendimentos.get(0).enderecos);

			assertNotNull(empreendimentos.get(0));
			assertNotNull(empreendimentos.get(0).id);
			assertNotNull(empreendimentos.get(0).denominacao);
			assertNotNull(empreendimentos.get(0).pessoa);
			assertNotNull(empreendimentos.get(0).enderecos);
			assertNotNull(enderecos.get(0).municipio.estado.sigla);
			assertNotNull(enderecos.get(0).municipio.nome);

		}

		@Test
		public void naoDeveFiltrarSemPermissao() {

			new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			FiltroEmpreendimento filtro = new FiltroEmpreendimento();
			filtro.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;
			filtro.busca = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			String url = Router.reverse("Empreendimentos.findByFilter").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			assertEquals("/empreendimentos", url);

			Response response = POSTJson(url, filtro, EmpreendimentosSerializer.findByFilter);

			assertIsForbidden(response);

		}

	}

	public static class Visualizar extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveVisualizarPf() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", empreendimento.id);

			String url = Router.reverse("Empreendimentos.find", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimento/" + empreendimento.id, url);

			Response response = GET(url);

			Empreendimento resultado = responseToObject(response, Empreendimento.class);

			assertNotNull(resultado);
			assertNotNull(resultado.id);
			assertNotNull(resultado.denominacao);

			PessoaFisica resultadoPf = (PessoaFisica) resultado.pessoa;

			assertNotNull(resultadoPf.id);
			assertNotNull(resultadoPf.nome);
			assertNotNull(resultadoPf.cpf);
			assertNotNull(resultadoPf.dataNascimento);
			assertNotNull(resultadoPf.nomeMae);
			assertNotNull(resultadoPf.estrangeiro);
			assertNotNull(resultadoPf.naturalidade);
			assertNotNull(resultadoPf.sexo);
			assertNotNull(resultadoPf.estadoCivil);
			assertNotNull(resultadoPf.rg);

		}

		@Test
		public void deveVisualizarPj() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridicaSalva()
					.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", empreendimento.id);

			String url = Router.reverse("Empreendimentos.find", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimento/" + empreendimento.id, url);

			Response response = GET(url);

			Empreendimento resultado = responseToObject(response, Empreendimento.class);

			assertNotNull(resultado);
			assertNotNull(resultado.id);
			assertNotNull(resultado.denominacao);

			PessoaJuridica resultadoPj = (PessoaJuridica) resultado.pessoa;

			assertNotNull(resultadoPj.id);
			assertNotNull(resultadoPj.razaoSocial);
			assertNotNull(resultadoPj.cnpj);
			assertNotNull(resultadoPj.dataConstituicao);

		}

		@Test
		public void naoDeveVisualizarSemPermissao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", empreendimento.id);

			String url = Router.reverse("Empreendimentos.find", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			assertEquals("/empreendimento/" + empreendimento.id, url);

			Response response = GET(url);

			assertIsForbidden(response);

		}

	}

	public static class Remocao extends BaseFunctionalTest {

		private static Empreendimento empreendimentoNaoRemovido = null;
		private static String urlParam = "id";

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveRemover() {

			empreendimentoNaoRemovido = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.semFlagRemovido()
					.save();

			commitTransaction();

			Map<String, Object> map = new HashMap<>();
			map.put(urlParam, empreendimentoNaoRemovido.id);

			String url = Router.reverse("Empreendimentos.remove", map).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimento/" + map.get(urlParam), url);

			Response response = DELETE(url);

			assertEquals(Http.StatusCode.OK, response.status.longValue());

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("empreendimento.removido.sucesso"), message.getText());

		}

		@Test
		public void naoDeveRemoverSemPermissao() {

			empreendimentoNaoRemovido = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.semFlagRemovido()
					.save();

			commitTransaction();

			Map<String, Object> map = new HashMap<>();
			map.put(urlParam, empreendimentoNaoRemovido.id);

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			String url = Router.reverse("Empreendimentos.remove", map).url;

			assertEquals("/empreendimento/" + map.get(urlParam), url);

			Response response = DELETE(url);

			assertIsForbidden(response);

		}

	}

	public static class Edicao extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveEditar() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(empreendimento.pessoa)
					.comDenominacao("Outra denominação")
					.build();

			this.commitTransaction();

			String url = Router.reverse("Empreendimentos.update").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimento", url);

			Response response = PUTJson(url, empreendimentoEditar, EmpreendimentosSerializer.update);

			assertEquals(Http.StatusCode.OK, response.status.longValue());

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("empreendimento.editar.sucesso"), message.getText());

		}

		@Test
		public void naoDeveEditarSemPermissao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(empreendimento.pessoa)
					.comDenominacao("Outra denominação")
					.build();

			this.commitTransaction();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = PUTJson(Router.reverse("Empreendimentos.update").url, empreendimentoEditar, EmpreendimentosSerializer.update);

			assertEquals(Http.StatusCode.FORBIDDEN, response.status.longValue());

		}

		@Test
		public void naoDeveEditarSeNaoEncontrarNoBd() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(999999999)
					.comPessoa(empreendimento.pessoa)
					.comDenominacao("Outra denominação")
					.build();

			this.commitTransaction();

			String url = Router.reverse("Empreendimentos.update").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/empreendimento", url);

			Response response = PUTJson(url, empreendimentoEditar, EmpreendimentosSerializer.update);

			assertEquals(Http.StatusCode.NOT_FOUND, response.status.longValue());

		}

	}

}
