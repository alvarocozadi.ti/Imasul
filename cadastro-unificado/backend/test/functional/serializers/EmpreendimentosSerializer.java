package functional.serializers;


import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.GeometryTransformer;
import transformers.TipoEnumTransformer;

public class EmpreendimentosSerializer {

	public static JSONSerializer create;
	public static JSONSerializer findByFilter;
	public static JSONSerializer update;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		create = new JSONSerializer()
				.include(
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.bairro",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"enderecos.descricaoAcesso",
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal",
						"pessoa.tipo",
						"pessoa.contatos.tipo.id",
						"pessoa.contatos.valor",
						"pessoa.enderecos.tipo.id",
						"pessoa.enderecos.zonaLocalizacao",
						"pessoa.enderecos.logradouro",
						"pessoa.enderecos.numero",
						"pessoa.enderecos.bairro",
						"pessoa.enderecos.cep",
						"pessoa.enderecos.municipio.id",
						"pessoa.enderecos.pais.id",
						"pessoa.enderecos.descricaoAcesso",
						"pessoa.nome",
						"pessoa.cpf",
						"pessoa.dataNascimento",
						"pessoa.nomeMae",
						"pessoa.estrangeiro",
						"pessoa.naturalidade",
						"pessoa.sexo",
						"pessoa.estadoCivil",
						"pessoa.rg.numero",
						"pessoa.cnpj",
						"pessoa.razaoSocial",
						"pessoa.nomeFantasia",
						"pessoa.dataConstituicao",
						"pessoa.inscricaoEstadual",
						"denominacao",
						"localizacao.geometria"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

		findByFilter = new JSONSerializer()
				.include(
						"ordenacao",
						"filtroSimplificado",
						"tamanhoPagina",
						"numeroPagina"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

		update = new JSONSerializer()
				.include(
						"id",
						"denominacao",
						"localizacao.geometria",
						"localizacao.id",
						"contatos.id",
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.bairro",
						"enderecos.descricaoAcesso",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"pessoa.id",
						"pessoa.nome",
						"pessoa.tipo",
						"pessoa.tituloEleitoral.numero",
						"pessoa.tituloEleitoral.secao",
						"pessoa.tituloEleitoral.zona",
						"pessoa.cpf",
						"pessoa.dataNascimento",
						"pessoa.nomeMae",
						"pessoa.estrangeiro",
						"pessoa.naturalidade",
						"pessoa.sexo",
						"pessoa.estadoCivil",
						"pessoa.rg.numero",
						"pessoa.rg.orgaoExpedidor",
						"pessoa.contatos.tipo.id",
						"pessoa.contatos.valor",
						"pessoa.enderecos.tipo.id",
						"pessoa.enderecos.zonaLocalizacao",
						"pessoa.enderecos.logradouro",
						"pessoa.enderecos.numero",
						"pessoa.enderecos.complemento",
						"pessoa.enderecos.bairro",
						"pessoa.enderecos.cep",
						"pessoa.enderecos.municipio.id",
						"pessoa.enderecos.pais.id",
						"pessoa.enderecos.descricaoAcesso",
						"pessoa.cnpj",
						"pessoa.razaoSocial",
						"pessoa.nomeFantasia",
						"pessoa.dataConstituicao",
						"pessoa.inscricaoEstadual"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

	}

}
