package functional.serializers;

import java.util.Date;

import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import serializers.DateSerializer;
import transformers.TipoEnumTransformer;

public class PessoasJuridicasSerializer {

	public static JSONSerializer deveCadastrar;
	public static JSONSerializer findByFilter;
	public static JSONSerializer deveEditar;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		deveCadastrar = new JSONSerializer()
				.include(
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.complemento",
						"enderecos.bairro",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"enderecos.descricaoAcesso",
						"cnpj",
						"razaoSocial",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

		findByFilter = new JSONSerializer()
				.include(
						"ordenacao",
						"busca",
						"tamanhoPagina",
						"numeroPagina"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

		deveEditar = new JSONSerializer()
				.include(
						"id",
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.bairro",
						"enderecos.descricaoAcesso",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"cnpj",
						"razaoSocial",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

	}

}
