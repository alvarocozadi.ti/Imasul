package functional.serializers;

import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.TipoEnumTransformer;

public class PessoasFisicasSerializer {

	public static JSONSerializer create;
	public static JSONSerializer findByFilter;
	public static JSONSerializer update;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		create = new JSONSerializer()
				.include(
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.bairro",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"enderecos.descricaoAcesso",
						"nome",
						"cpf",
						"dataNascimento",
						"nomeMae",
						"estrangeiro",
						"naturalidade",
						"sexo",
						"estadoCivil",
						"rg.numero",
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

		findByFilter = new JSONSerializer()
				.include(
						"ordenacao",
						"filtroSimplificado",
						"tamanhoPagina",
						"numeroPagina"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

		update = new JSONSerializer()
				.include(
						"id",
						"contatos.id",
						"contatos.tipo.id",
						"contatos.valor",
						"contatos.principal",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.bairro",
						"enderecos.descricaoAcesso",
						"enderecos.cep",
						"enderecos.municipio.id",
						"enderecos.pais.id",
						"nome",
						"tituloEleitoral.numero",
						"tituloEleitoral.secao",
						"tituloEleitoral.zona",
						"cpf",
						"dataNascimento",
						"nomeMae",
						"estrangeiro",
						"naturalidade",
						"sexo",
						"estadoCivil",
						"rg.numero"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

	}

}