package functional;

import builders.PessoaFisicaBuilder;
import common.models.Message;
import common.models.Pagination;
import functional.serializers.PessoasFisicasSerializer;
import models.Endereco;
import models.FiltroPessoaFisica;
import models.Pessoa;
import models.PessoaFisica;
import org.junit.BeforeClass;
import org.junit.Test;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Http.Response;
import play.mvc.Router;
import utils.DatabaseCleaner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PessoasFisicasTest {

	public static class Cadastro extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveCadastrar() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(false).build();

			String url = Router.reverse("PessoasFisicas.create").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaFisica", url);

			Response response = POSTJson(url, pessoa, PessoasFisicasSerializer.create);

			assertEquals(Http.StatusCode.OK, response.status.longValue());

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoa.cadastrar.sucesso"), message.getText());

		}

		@Test
		public void naoDeveCadastrarSemPermissao() {

			Pessoa pessoa = new PessoaFisicaBuilder(false)
					.semTituloEleitoral()
					.build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = POSTJson(Router.reverse("PessoasFisicas.create").url, pessoa, PessoasFisicasSerializer.create);

			assertEquals(Http.StatusCode.FORBIDDEN, response.status.longValue());

		}

	}

	public static class Remocao extends BaseFunctionalTest {

		private static PessoaFisica pessoaNaoRemovida;
		private static String urlParam = "id";

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveRemover() {

			pessoaNaoRemovida = new PessoaFisicaBuilder()
					.semFlagRemovido()
					.save();

			commitTransaction();

			Map<String, Object> map = new HashMap<>();
			map.put(urlParam, pessoaNaoRemovida.id);

			String url = Router.reverse("PessoasFisicas.remove",map).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaFisica/" + map.get(urlParam), url);

			Response response = DELETE(url);

			assertEquals(Http.StatusCode.OK, response.status.longValue());

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoaFisica.removido.sucesso"), message.getText());

		}

		@Test
		public void naoDeveRemoverSemPermissao() {

			pessoaNaoRemovida = new PessoaFisicaBuilder()
					.semFlagRemovido()
					.save();

			commitTransaction();

			Map<String, Object> map = new HashMap<>();
			map.put(urlParam, pessoaNaoRemovida.id);

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			String url = Router.reverse("PessoasFisicas.remove",map).url;

			assertEquals("/pessoaFisica/" + map.get(urlParam), url);

			Response response = DELETE(url);

			assertIsForbidden(response);

		}

	}

	public static class Listagem extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveFiltrar() {

			for(int i = 0; i < 29; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			commitTransaction();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

			String url = Router.reverse("PessoasFisicas.findByFilter").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoasFisicas", url);

			Response response = POSTJson(url, filtro, PessoasFisicasSerializer.findByFilter);

			assertIsOk(response);

			Pagination paginacao = responseToPaginationOf(response, PessoaFisica.class);

			assertNotNull(paginacao.getTotalItems());
			assertNotNull(paginacao.getPageItems());

			assertEquals(10, paginacao.getPageItems().size());

			List<PessoaFisica> pessoas = paginacao.getPageItems();
			List<Endereco> enderecos = new ArrayList<>();
			enderecos.addAll(pessoas.get(0).enderecos);

			assertNotNull(pessoas.get(0));
			assertNotNull(pessoas.get(0).id);
			assertNotNull(pessoas.get(0).nome);
			assertNotNull(pessoas.get(0).cpf);
			assertNotNull(pessoas.get(0).rg.numero);
			assertNotNull(enderecos.get(0).municipio.estado.sigla);
			assertNotNull(enderecos.get(0).municipio.nome);

		}

		@Test
		public void naoDeveFiltrarSemPermissao() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = pessoa.cpf;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			String url = Router.reverse("PessoasFisicas.findByFilter").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			assertEquals("/pessoasFisicas", url);

			Response response = POSTJson(url, filtro, PessoasFisicasSerializer.findByFilter);

			assertIsForbidden(response);

		}

	}

	public static class Visualizar extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveVisualizarComPermissao() {

			Pessoa pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", pessoa.id);

			String url = Router.reverse("PessoasFisicas.find", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaFisica/" + pessoa.id, url);

			Response response = GET(url);

			PessoaFisica resultado = responseToObject(response, PessoaFisica.class);

			assertNotNull(resultado);
			assertNotNull(resultado.id);
			assertNotNull(resultado.nome);
			assertNotNull(resultado.cpf);
			assertNotNull(resultado.enderecos);
			assertNotNull(resultado.dataNascimento);
			assertNotNull(resultado.dataAtualizacao);
			assertNotNull(resultado.contatos);
			assertNotNull(resultado.nomeMae);
			assertNotNull(resultado.estrangeiro);
			assertNotNull(resultado.naturalidade);
			assertNotNull(resultado.sexo);
			assertNotNull(resultado.estadoCivil);
			assertNotNull(resultado.rg);

		}

		@Test
		public void naoDeveVisualizarSemPermissao() {

			Pessoa pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", pessoa.id);

			String url = Router.reverse("PessoasFisicas.find", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			assertEquals("/pessoaFisica/" + pessoa.id, url);

			Response response = GET(url);

			assertIsForbidden(response);

		}

	}

	public static class Edicao extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveEditar() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.save();

			PessoaFisica pessoaEditar = new PessoaFisicaBuilder()
					.comId(pessoa.id)
					.comCpf(pessoa.cpf)
					.comRg(pessoa.rg)
					.build();

			this.commitTransaction();

			String url = Router.reverse("PessoasFisicas.update").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaFisica", url);

			Response response = PUTJson(url, pessoaEditar, PessoasFisicasSerializer.update);

			assertEquals(Http.StatusCode.OK, response.status.longValue());

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoa.editar.sucesso"), message.getText());

		}

		@Test
		public void naoDeveEditarSemPermissao() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.save();

			PessoaFisica pessoaEditar = new PessoaFisicaBuilder()
					.comId(pessoa.id)
					.comCpf(pessoa.cpf)
					.comRg(pessoa.rg)
					.build();

			this.commitTransaction();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = PUTJson(Router.reverse("PessoasFisicas.update").url, pessoaEditar, PessoasFisicasSerializer.update);

			assertEquals(Http.StatusCode.FORBIDDEN, response.status.longValue());

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarSeNaoEncontrarNoBd() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.save();

			PessoaFisica pessoaEditar = new PessoaFisicaBuilder()
					.comId(999999999)
					.comCpf(pessoa.cpf)
					.comRg(pessoa.rg)
					.build();

			this.commitTransaction();

			String url = Router.reverse("PessoasFisicas.update").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaFisica", url);

			Response response = PUTJson(url, pessoaEditar, PessoasFisicasSerializer.update);

			assertEquals(Http.StatusCode.NOT_FOUND, response.status.longValue());

			pessoa._delete();

		}

	}

}
