package functional;

import models.Municipio;
import org.junit.Test;
import play.mvc.Http;
import play.mvc.Router;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EstadosTest {

	public static class Listagem extends BaseFunctionalTest {

		@Test
		public void deveListar() {

			Map<String,Object> args = new HashMap<>();
			args.put("id", 1);

			String url = Router.reverse("Estados.findMunicipios", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/estado/1/municipios", url);

			Http.Response response = GET(url);

			List<Municipio> municipios = responseToListOf(response, Municipio.class);

			assertNotEmpty(municipios);

			assertNotNull(municipios.get(0).id);
			assertNotNull(municipios.get(0).nome);
			assertNotNull(municipios.get(0).codigoIbge);

		}

		@Test public void naoDeveListarSemPermissao() {

			Map<String,Object> args = new HashMap<>();
			args.put("id", 1);

			String url = Router.reverse("Estados.findMunicipios", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Http.Response response = GET(url);

			assertIsForbidden(response);

		}

	}

}
