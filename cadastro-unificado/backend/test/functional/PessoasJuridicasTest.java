package functional;

import builders.EnderecoBuilder;
import builders.PessoaJuridicaBuilder;
import common.models.Message;
import common.models.Pagination;
import functional.serializers.PessoasJuridicasSerializer;
import models.*;
import org.junit.BeforeClass;
import org.junit.Test;
import play.i18n.Messages;
import play.mvc.Http.Response;
import play.mvc.Router;
import utils.DatabaseCleaner;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PessoasJuridicasTest {

	public static class Cadastro extends BaseFunctionalTest {

		private static String url;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			url = Router.reverse("PessoasJuridicas.create").url;
			assertEquals("/pessoaJuridica", url);

		}

		@Test
		public void deveCadastrar() {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaRural()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.comComplemento()
					.comCaixaPostal()
					.build();

			PessoaJuridica pessoa = (PessoaJuridica) new PessoaJuridicaBuilder()
					.comInscricaoEstadual()
					.comNomeFantasia()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.comContatos()
					.build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = POSTJson(url, pessoa, PessoasJuridicasSerializer.deveCadastrar);

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoa.cadastrar.sucesso"), message.getText());

		}

		@Test
		public void naoDeveCadastrarSeInvalido() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder()
					.semRazaoSocial()
					.build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = POSTJson(url, pessoa, PessoasJuridicasSerializer.deveCadastrar);

			assertIsValidationError(response, Messages.get("pessoaJuridica.razaoSocial.obrigatoria"));

		}

		@Test
		public void naoDeveCadastrarSemPermissao() {

			Pessoa pessoa = new PessoaJuridicaBuilder().build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = POSTJson(url, pessoa, PessoasJuridicasSerializer.deveCadastrar);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveCadastrarNaoAutenticado() {

			Pessoa pessoa = new PessoaJuridicaBuilder().build();

			Response response = POSTJson(url, pessoa, PessoasJuridicasSerializer.deveCadastrar);

			assertIsUnauthorized(response);

		}

	}

	public static class Listagem extends BaseFunctionalTest {

		private static String url;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			for(int i = 0; i < 30; i++) {

				new PessoaJuridicaBuilder()
					.comRazaoSocial("Empresa " + i + " SA")
					.comNomeFantasia()
					.save();

			}

			commitTransaction();

			url = Router.reverse("PessoasJuridicas.findByFilter").url;
			assertEquals("/pessoasJuridicas", url);

		}

		@Test
		public void deveListar() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_DESC;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = POSTJson(url, filtro, PessoasJuridicasSerializer.findByFilter);

			Pagination paginacao = responseToPaginationOf(response, PessoaJuridica.class);

			assertNotNull(paginacao.getTotalItems());
			assertNotNull(paginacao.getPageItems());

			assertEquals(10, paginacao.getPageItems().size());
			assertEquals(30, paginacao.getTotalItems().longValue());

			List<PessoaJuridica> pessoas = paginacao.getPageItems();
			assertNotNull(pessoas);

			PessoaJuridica pessoa = pessoas.get(0);
			assertNotNull(pessoa);
			assertNotNull(pessoa.id);
			assertNotNull(pessoa.razaoSocial);
			assertNotNull(pessoa.cnpj);
			assertNotNull(pessoa.nomeFantasia);

			Endereco endereco = pessoa.enderecos.iterator().next();

			assertNotNull(endereco.municipio.estado.sigla);
			assertNotNull(endereco.municipio.nome);

		}

		@Test
		public void naoDeveListarSemPermissao() {

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = POSTJson(url, filtro, PessoasJuridicasSerializer.findByFilter);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveListarNaoAutenticado() {

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();

			Response response = POSTJson(url, filtro, PessoasJuridicasSerializer.findByFilter);

			assertIsUnauthorized(response);

		}

	}

	public static class Edicao extends BaseFunctionalTest {

		private static String url;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			url = Router.reverse("PessoasJuridicas.update").url;

			assertEquals("/pessoaJuridica", url);

		}

		@Test
		public void deveEditar() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.build();

			commitTransaction();

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = PUTJson(url, pessoaEditar, PessoasJuridicasSerializer.deveEditar);

			assertIsOk(response);

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoa.editar.sucesso"), message.getText());



		}

		@Test
		public void naoDeveEditarSemPermissao() {

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder().build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = PUTJson(url, pessoaEditar, PessoasJuridicasSerializer.deveEditar);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveEditarSeNaoEncontrarNoBanco() {

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(999999)
				.build();

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = PUTJson(url, pessoaEditar, PessoasJuridicasSerializer.deveEditar);

			assertIsNotFound(response);

		}

		@Test
		public void naoDeveEditarNaoAuteticado() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().build();

			Response response = POSTJson(url, pessoa, PessoasJuridicasSerializer.deveEditar);

			assertIsUnauthorized(response);

		}

	}

	public static class Visualizacao extends BaseFunctionalTest {

		private static String url;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaRural()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.comComplemento()
					.comCaixaPostal()
					.build();

			PessoaJuridica pessoa = (PessoaJuridica) new PessoaJuridicaBuilder()
					.comInscricaoEstadual()
					.comNomeFantasia()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.comContatos()
					.save();

			commitTransaction();

			Map<String, Object> args = new HashMap<>();
			args.put("id", pessoa.id);

			url = Router.reverse("PessoasJuridicas.find", args).url;
			assertEquals("/pessoaJuridica/" + pessoa.id, url);

		}

		private void verificaEndereco(Endereco endereco) {

			assertNotNull(endereco.tipo);
			assertNotNull(endereco.tipo.id);
			assertNotNull(endereco.tipo.descricao);
			assertNotNull(endereco.zonaLocalizacao);
			assertNotNull(endereco.pais);
			assertNotNull(endereco.municipio);
			assertNotNull(endereco.municipio.id);
			assertNotNull(endereco.municipio.nome);
			assertNotNull(endereco.municipio.estado);
			assertNotNull(endereco.municipio.estado.id);
			assertNotNull(endereco.municipio.estado.nome);
			assertNotNull(endereco.municipio.estado.sigla);
			assertNotNull(endereco.pais.id);
			assertNotNull(endereco.pais.nome);
			assertNotNull(endereco.pais.sigla);

			if(endereco.zonaLocalizacao.equals(ZonaLocalizacao.URBANA)) {

				assertNotNull(endereco.logradouro);
				assertNotNull(endereco.numero);
				assertNotNull(endereco.complemento);
				assertNotNull(endereco.bairro);
				assertNotNull(endereco.cep);
				assertNotNull(endereco.caixaPostal);
				assertNotNull(endereco.semNumero);

			} else {

				assertNotNull(endereco.descricaoAcesso);

			}

		}

		private void verificaContato(Contato contato) {

			assertNotNull(contato.tipo);
			assertNotNull(contato.valor);

		}

		@Test
		public void deveBuscar() {

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			Response response = GET(url);

			PessoaJuridica pessoaJuridica = responseToObject(response, PessoaJuridica.class);

			assertNotNull(pessoaJuridica);
			assertNotNull(pessoaJuridica.cnpj);
			assertNotNull(pessoaJuridica.razaoSocial);
			assertNotNull(pessoaJuridica.nomeFantasia);
			assertNotNull(pessoaJuridica.dataConstituicao);
			assertNotNull(pessoaJuridica.inscricaoEstadual);

			assertNotEmpty(pessoaJuridica.enderecos);

			Iterator<Endereco> enderecosIterator = pessoaJuridica.enderecos.iterator();
			while (enderecosIterator.hasNext()) {
				verificaEndereco(enderecosIterator.next());
			}

			assertNotEmpty(pessoaJuridica.contatos);

			Iterator<Contato> contatosIterator = pessoaJuridica.contatos.iterator();
			while (contatosIterator.hasNext()) {
				verificaContato(contatosIterator.next());
			}

		}

		@Test
		public void naoDeveBuscarSemPermissao() {

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = GET(url);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveBuscarNaoAutenticado() {

			Response response = GET(url);

			assertIsUnauthorized(response);

		}

	}

	public static class Remocao extends BaseFunctionalTest {

		@BeforeClass
		public static void beforeClass() {
			DatabaseCleaner.cleanAll();
		}

		@Test
		public void deveRemover() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.semFlagRemovido()
				.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", pessoaJuridica.id);

			String url = Router.reverse("PessoasJuridicas.remove", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaJuridica/" + args.get("id"), url);

			Response response = DELETE(url);

			assertIsOk(response);

			Message message = responseToObject(response, Message.class);

			assertEquals(Messages.get("pessoaJuridica.removido.sucesso"), message.getText());

		}

		@Test
		public void naoDeveRemoverSemPermissao() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.semFlagRemovido()
				.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", pessoaJuridica.id);

			String url = Router.reverse("PessoasJuridicas.remove", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			assertEquals("/pessoaJuridica/" + args.get("id"), url);

			Response response = DELETE(url);

			assertIsForbidden(response);

		}

		@Test
		public void naoDeveRemoverSemAutenticacao() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.semFlagRemovido()
				.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", pessoaJuridica.id);

			String url = Router.reverse("PessoasJuridicas.remove", args).url;

			assertEquals("/pessoaJuridica/" + args.get("id"), url);

			Response response = DELETE(url);

			assertIsUnauthorized(response);

		}

		@Test
		public void naoDeveRemoverCasoIdNaoEncontrado() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.semFlagRemovido()
				.save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("id", 999999999);

			String url = Router.reverse("PessoasJuridicas.remove", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pessoaJuridica/" + args.get("id"), url);

			Response response = DELETE(url);

			assertIsNotFound(response);

		}

	}

}
