package functional;

import models.TipoContato;
import org.junit.Test;
import play.mvc.Http;
import play.mvc.Router;

import java.util.List;

public class TiposContatoTest {

	public static class Listagem extends BaseFunctionalTest {

		@Test
		public void deveListar() {

			String url = Router.reverse("TiposContato.find").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/tiposContato", url);

			Http.Response response = GET(url);

			List<TipoContato> tiposContato = responseToListOf(response, TipoContato.class);

			assertNotEmpty(tiposContato);

			assertNotNull(tiposContato.get(0).id);
			assertNotNull(tiposContato.get(0).descricao);

		}

		@Test
		public void naoDeveListarSemPermissao() {

			String url = Router.reverse("TiposContato.find").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Http.Response response = GET(url);

			assertIsForbidden(response);

		}

	}

}