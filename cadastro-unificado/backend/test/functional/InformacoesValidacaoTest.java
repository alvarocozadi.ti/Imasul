package functional;

import builders.PessoaFisicaBuilder;
import builders.PessoaJuridicaBuilder;
import models.InformacaoValidacao;
import models.NomePessoa;
import models.PessoaFisica;
import models.PessoaJuridica;
import org.junit.Test;
import play.mvc.Http;
import play.mvc.Router;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InformacoesValidacaoTest {

	public static class Listagem extends BaseFunctionalTest {

		private Boolean hasConteudo(List list, Object object) {

			return list.contains(object);

		}

		@Test
		public void getInformacoesByCpf() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(false).save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("cpf", pessoaFisica.cpf);

			String url = Router.reverse("InformacoesValidacao.getInformacoesByCpf", args).url;

			Http.Response response = GET(url);

			InformacaoValidacao informacao = responseToObject(response, InformacaoValidacao.class);

			assertNotEmpty(informacao.nomesMaes);
			assertNotEmpty(informacao.nomesPessoasFisicas);

			assertEquals(informacao.nomesPessoasFisicas.size(), NomePessoa.nroNomesRandomicos + 1);
			assertEquals(informacao.nomesMaes.size(), NomePessoa.nroNomesRandomicos + 1);

			assertTrue(hasConteudo(informacao.nomesPessoasFisicas, pessoaFisica.nome));
			assertTrue(hasConteudo(informacao.nomesMaes, pessoaFisica.nomeMae.split(" ")[0]));

			pessoaFisica.delete();

		}

		@Test
		public void deveListarNomesPessoasJuridicas() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().save();

			commitTransaction();

			Map<String,Object> args = new HashMap<>();
			args.put("cnpj", pessoaJuridica.cnpj);

			String url = Router.reverse("InformacoesValidacao.getInformacoesByCnpj", args).url;

			Http.Response response = GET(url);

			InformacaoValidacao informacao = responseToObject(response, InformacaoValidacao.class);

			assertNotEmpty(informacao.nomesPessoasJuridicas);
			assertNotNull(informacao.nomesMunicipios);

			assertEquals(informacao.nomesPessoasJuridicas.size(), NomePessoa.nroNomesRandomicos + 1);
			assertEquals(informacao.nomesMunicipios.size(), NomePessoa.nroNomesRandomicos + 1);

			assertTrue(hasConteudo(informacao.nomesPessoasJuridicas, pessoaJuridica.razaoSocial));
			assertTrue(hasConteudo(informacao.nomesMunicipios, pessoaJuridica.enderecos.iterator().next().municipio.nome));

			pessoaJuridica.delete();

		}

	}

}
