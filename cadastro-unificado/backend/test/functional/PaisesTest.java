package functional;

import models.Estado;
import models.Pais;
import org.junit.Test;
import play.mvc.Http;
import play.mvc.Http.Response;
import play.mvc.Router;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaisesTest {

	public static class Listagem extends BaseFunctionalTest {

		@Test
		public void deveListar() {

			String url = Router.reverse("Paises.find").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/paises", url);

			Response response = GET(url);

			List<Pais> paises = responseToListOf(response, Pais.class);

			assertNotEmpty(paises);

			assertNotNull(paises.get(0).id);
			assertNotNull(paises.get(0).nome);
			assertNotNull(paises.get(0).sigla);

		}

		@Test
		public void naoDeveListarSemPermissao() {

			String url = Router.reverse("Paises.find").url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Response response = GET(url);

			assertIsForbidden(response);

		}

	}

	public static class ListagemEstados extends BaseFunctionalTest {

		@Test
		public void deveListar() {

			Map<String, Object> args = new HashMap<>();
			args.put("id", 29);

			String url = Router.reverse("Paises.findEstados", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_ADMINISTRADOR);

			assertEquals("/pais/29/estados", url);

			Http.Response response = GET(url);

			List<Estado> estados = responseToListOf(response, Estado.class);

			assertNotEmpty(estados);

			assertNotNull(estados.get(0).id);
			assertNotNull(estados.get(0).nome);
			assertNotNull(estados.get(0).sigla);

		}

		@Test
		public void naoDeveListarSemPermissao() {

			Map<String, Object> args = new HashMap<>();
			args.put("id", 29);

			String url = Router.reverse("Paises.findEstados", args).url;

//			login("46278149761", AuthenticationServiceFake.PERFIL_SEM_PERMISSAO);

			Http.Response response = GET(url);

			assertIsForbidden(response);

		}

	}

}
