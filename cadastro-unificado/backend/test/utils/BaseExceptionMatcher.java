package utils;

import exceptions.BaseException;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.rules.ExpectedException;

public class BaseExceptionMatcher extends TypeSafeMatcher<BaseException> {

	private String expectedMessage, actualMessage;
	
	public static BaseExceptionMatcher hasUserMessage(String item) {
		return new BaseExceptionMatcher(item);
	}
	
	private BaseExceptionMatcher(String expectedMessage) {
		this.expectedMessage = expectedMessage;
	}
	
	@Override
	public void describeTo(Description description) {
		
		description.appendValue(actualMessage)
			.appendText(" was not found instead of ")
			.appendValue(expectedMessage);

	}

	@Override
	protected boolean matchesSafely(BaseException baseException) {

		ExpectedException.none().expect(BaseException.class);
		
		actualMessage = baseException.getUserMessage();

		return actualMessage.indexOf(expectedMessage) >= 0;

	}

}