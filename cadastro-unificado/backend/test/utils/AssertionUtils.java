package utils;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AssertionUtils {

	public static void assertFileEquals(File expects, File actuals) throws FileNotFoundException, IOException {
		
		MessageDigest md;
		
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Algoritmo inválido");
		}
		
		md.update(IOUtils.toByteArray(new FileInputStream(expects)));
		String firstHash = new String(md.digest());

		md.update(IOUtils.toByteArray(new FileInputStream(actuals)));
		String secondHash = new String(md.digest());

		Assert.assertEquals(firstHash, secondHash);
		
	}
	
}
