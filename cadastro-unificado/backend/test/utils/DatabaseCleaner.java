package utils;

import models.*;
import play.test.Fixtures;

public class DatabaseCleaner {

	public static void cleanAll() {


		Fixtures.delete(
				Empreendimento.class,
				Pessoa.class,
				PessoaJuridica.class,
				PessoaFisica.class,
				Contato.class,
				Endereco.class,
				BloqueioPessoa.class
		);

	}

	public static void clean(Class... classes) {
		Fixtures.delete(classes);
	}

}