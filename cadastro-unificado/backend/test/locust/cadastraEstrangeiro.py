# coding: utf-8
from locust import TaskSet, HttpLocust, task #importa biblioteca TaskSet, HttpLocust e task
from random import randint

class CadastraPessoaFisica(TaskSet):

    def on_start(self):
        self.login() 

    def login(self):
        self.client.post('/login', data ={'username': 'Bruno', 'profile' : 'Administrador-EU'})

    @task
    def cadastraEstrangeiro(self):
        null = None;
        true = True;
        false = False;
        passaporte = randint(1, 1000000);
        with self.client.post('/pessoaFisica', json ={"tituloEleitoral":null,"estrangeiro":true,"nome":"Bruno Mazzi","passaporte":passaporte,"dataNascimento":"1992-09-02T03:00:00.000Z","sexo":{"nome":"MASCULINO","codigo":0,"descricao":"Masculino"},"nomeMae":"Elza Mazzi","estadoCivil":{"nome":"SOLTEIRO","codigo":0,"descricao":"Solteiro(a)"},"enderecos":[{"tipo":{"id":1},"zonaLocalizacao":{"nome":"URBANA","codigo":0,"descricao":"Urbana"},"pais":{"id":29},"semNumero":false,"logradouro":"Rua Angelo Padovani","numero":"178","bairro":"Dona Wanda","complemento":"Casa A","cep":"37200000","uf":{"id":13,"nome":"Minas Gerais","sigla":"MG"},"municipio":{"codigoIbge":3138203,"estado":{"id":13},"id":4028,"nome":"Lavras"}},{"tipo":{"id":2},"zonaLocalizacao":{"nome":"URBANA","codigo":0,"descricao":"Urbana"},"pais":{"id":29},"semNumero":false,"logradouro":"Rua Angelo Padovani","numero":"178","bairro":"Dona Wanda","complemento":"Casa A","cep":"37200000","uf":{"id":13,"nome":"Minas Gerais","sigla":"MG"},"municipio":{"codigoIbge":3138203,"estado":{"id":13},"id":4028,"nome":"Lavras"},"usarPrincipal":true}],"cpf":null,"naturalidade":null,"rg":null}) as response:
            if response.status_code != 200:
                print response.content;

class ApiUser(HttpLocust): #classe ApiUser herda de HttpLocust
    host = 'http://localhost:9901'
    min_wait = 1000 #1 segundo (tempo mínimo entre uma tarefa e outra)
    max_wait = 3000 #3 segundos (tempo máximo entre uma tarefa e outra)
    task_set = CadastraPessoaFisica
