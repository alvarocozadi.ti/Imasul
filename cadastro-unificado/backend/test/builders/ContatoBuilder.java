package builders;

import models.Contato;
import models.TipoContato;

public class ContatoBuilder extends BaseModelBuilder<Contato> {

	public ContatoBuilder() {

		super(new Contato());

		padrao();

	}

	public ContatoBuilder(Contato model) {
		super(model);
	}

	@Override
	protected ContatoBuilder padrao() {

		model = new Contato();

		model.tipo = TipoContato.findById(1);
		model.valor = "joao@live.com";
		model.principal = true;
		model.removido = false;

		return this;

	}

	public ContatoBuilder semValor() {

		model.valor = null;

		return this;

	}

	public ContatoBuilder semTipo() {

		model.tipo = null;

		return this;

	}

	public ContatoBuilder comTipo(Integer id) {

		model.tipo = TipoContato.findById(id);

		return this;

	}

	public ContatoBuilder comValor(String valor) {

		model.valor = valor;

		return this;

	}

	public ContatoBuilder tagFalsePrincipal() {

		model.principal = false;

		return this;

	}

}
