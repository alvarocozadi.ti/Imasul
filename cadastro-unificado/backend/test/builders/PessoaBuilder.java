package builders;

import java.util.Arrays;
import java.util.HashSet;

import models.Contato;
import models.Endereco;
import models.Pessoa;
import models.TipoEndereco;

public abstract class PessoaBuilder<T extends Pessoa> extends BaseModelBuilder<T> {

	public PessoaBuilder(T model) {

		super(model);

	}

	@Override
	protected PessoaBuilder padrao() {

		Endereco enderecoPrincipal = new EnderecoBuilder()
				.zonaRural()
				.comTipo(TipoEndereco.ID_PRINCIPAL)
				.build();

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
				.zonaUrbana()
				.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
				.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoPrincipal);
		this.model.enderecos.add(enderecoCorrespondencia);

		return this;

	}

	public PessoaBuilder comContatos() {

		this.model.contatos = new HashSet<>();
		this.model.contatos.add(new ContatoBuilder().build());

		return this;

	}

	public PessoaBuilder comContatos(Boolean append, Contato... contatos) {

		if(!append || this.model.contatos == null)
			this.model.contatos = new HashSet<>();

		this.model.contatos.addAll(Arrays.asList(contatos));

		return this;

	}

	public PessoaBuilder semEnderecos() {

		this.model.enderecos = null;

		return this;

	}

	public PessoaBuilder comEnderecos(Boolean append, Endereco... enderecos) {

		if(!append || this.model.enderecos == null)
			this.model.enderecos = new HashSet<>();

		this.model.enderecos.addAll(Arrays.asList(enderecos));

		return this;

	}

}
