package builders;

import models.*;

public class EnderecoBuilder extends BaseModelBuilder<Endereco> {

	public EnderecoBuilder() {

		super(new Endereco());

		padrao();

	}

	public EnderecoBuilder(Endereco model) {

		super(model);

	}

	@Override
	protected EnderecoBuilder padrao() {

		this.model.pais = Pais.findById(29);
		this.model.municipio = Municipio.findById(1);

		return this;

	}

	public EnderecoBuilder comTipo(Integer id) {

		this.model.tipo = TipoEndereco.findById(id);

		return this;

	}

	public EnderecoBuilder comCaixaPostal() {

		this.model.caixaPostal = "4154415";

		return this;

	}

	public EnderecoBuilder zonaUrbana() {

		this.model.zonaLocalizacao = ZonaLocalizacao.URBANA;
		this.model.logradouro = "Everaldo Silva";
		this.model.bairro = "Jardim Gloria";
		this.model.numero = 31;
		this.model.cep = "37200000";

		return this;

	}

	public EnderecoBuilder zonaRural() {

		this.model.zonaLocalizacao = ZonaLocalizacao.RURAL;
		this.model.descricaoAcesso = "vai ali e vira ali";

		return this;

	}

	public EnderecoBuilder semNumeroTrue() {

		this.model.semNumero = true;

		return this;

	}

	public EnderecoBuilder comMunicipioValido() {

		this.model.municipio = Municipio.findById(1);

		return this;

	}

	public EnderecoBuilder comMunicipio(Municipio municipio) {

		this.model.municipio = municipio;

		return this;

	}

	public EnderecoBuilder comPaisValido() {

		this.model.pais = Pais.findById(29);

		return this;

	}

	public EnderecoBuilder comPais(Pais pais) {

		this.model.pais = pais;

		return this;

	}

	public EnderecoBuilder semMunicipio() {

		this.model.municipio = null;

		return this;

	}

	public EnderecoBuilder semBairro() {

		this.model.bairro = null;

		return this;

	}

	public EnderecoBuilder semCaixaPostal() {

		this.model.caixaPostal = null;

		return this;

	}

	public EnderecoBuilder semCep() {

		this.model.cep = null;

		return this;

	}

	public EnderecoBuilder comComplemento() {

		this.model.complemento = "APT 123";

		return this;

	}

	public EnderecoBuilder comDescricaoAcesso() {

		this.model.descricaoAcesso = "vira ali, vira aqui e sobe ali";

		return this;

	}

	public EnderecoBuilder semZonaLocalizacao() {

		this.model.zonaLocalizacao = null;

		return this;

	}

	public EnderecoBuilder comZonaLocalizacao(ZonaLocalizacao zonaLocalizacao) {

		this.model.zonaLocalizacao = zonaLocalizacao;

		return this;

	}

	public EnderecoBuilder semLogradouro() {

		this.model.logradouro = null;

		return this;

	}

	public EnderecoBuilder semPais() {

		this.model.pais = null;

		return this;

	}

	public EnderecoBuilder semTipo() {

		this.model.tipo = null;

		return this;

	}

	public EnderecoBuilder tagSemNumeroTrue() {

		this.model.semNumero = true;

		return this;

	}

	public EnderecoBuilder comTipoId(Integer id) {

		this.model.tipo = TipoEndereco.findById(id);

		return this;

	}

	public EnderecoBuilder semNumero() {

		this.model.numero = null;

		return this;

	}

	public EnderecoBuilder semDescricaoAcesso() {

		this.model.descricaoAcesso = null;

		return this;

	}

}
