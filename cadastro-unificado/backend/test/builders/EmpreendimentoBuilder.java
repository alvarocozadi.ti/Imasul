package builders;

import models.*;

import java.util.HashSet;
import java.util.Set;

public class EmpreendimentoBuilder extends BaseModelBuilder<Empreendimento> {

	public EmpreendimentoBuilder() {

		super(new Empreendimento());

		padrao();

	}

	@Override
	protected BaseModelBuilder<Empreendimento> padrao() {

		this.model.denominacao = "Denominacao";
		this.model.pessoa = new PessoaFisicaBuilder().build();

		Endereco enderecoPrincipal = new EnderecoBuilder()
			.zonaRural()
			.comTipo(TipoEndereco.ID_PRINCIPAL)
			.build();

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
			.zonaUrbana()
			.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
			.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoPrincipal);
		this.model.enderecos.add(enderecoCorrespondencia);

		this.model.localizacao = new LocalizacaoBuilder().build();

		return this;

	}

	public EmpreendimentoBuilder comPessoaJuridica() {

		this.model.pessoa = new PessoaJuridicaBuilder().build();

		return this;

	}

	public EmpreendimentoBuilder comContatos() {

		this.model.contatos = new HashSet<>();
		this.model.contatos.add(new ContatoBuilder().build());

		return this;

	}

	public EmpreendimentoBuilder semLocalizacao() {

		this.model.localizacao = null;

		return this;

	}

	public EmpreendimentoBuilder comLocalizacaoSemGeometria() {

		this.model.localizacao =  new LocalizacaoBuilder().semGeometria().build();

		return this;

	}

	public EmpreendimentoBuilder comLocalizacaoComMaisDeUmPoligono() {

		this.model.localizacao =  new LocalizacaoBuilder().comMaisDeUmPoligono().build();

		return this;

	}

	public EmpreendimentoBuilder comProprietarios() {

		this.model.proprietarios = new HashSet<>();
		this.model.proprietarios.add(new PessoaJuridicaBuilder().build().save());

		return this;

	}

	public EmpreendimentoBuilder comResponsaveisTecnicos() {

		this.model.responsaveisTecnicos = new HashSet<>();
		this.model.responsaveisTecnicos.add(new PessoaJuridicaBuilder().build().save());

		return this;

	}

	public EmpreendimentoBuilder comRepresentantesLegais() {

		this.model.representantesLegais = new HashSet<>();
		this.model.representantesLegais.add(new PessoaFisicaBuilder().build().save());

		return this;

	}

	public EmpreendimentoBuilder comResponsaveisLegais() {

		this.model.responsaveisLegais = new HashSet<>();
		this.model.responsaveisLegais.add(new PessoaFisicaBuilder().build().save());

		return this;

	}

	public EmpreendimentoBuilder comPessoa(Pessoa pessoa) {

		this.model.pessoa = pessoa;

		return this;

	}

	public EmpreendimentoBuilder semDenominacao() {

		this.model.denominacao = null;

		return this;

	}

	public EmpreendimentoBuilder semPessoa() {

		this.model.pessoa = null;

		return this;

	}

	public EmpreendimentoBuilder comContatosInvalidos() {

		this.model.contatos = new HashSet<>();
		this.model.contatos.add(new ContatoBuilder().semTipo().build());

		return this;

	}

	public EmpreendimentoBuilder semEndereco() {

		this.model.enderecos = null;

		return this;

	}

	public EmpreendimentoBuilder comEnderecoInvalido() {

		Endereco enderecoPrincipal = new EnderecoBuilder()
				.zonaRural()
				.comTipo(TipoEndereco.ID_PRINCIPAL)
				.build();

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
				.zonaUrbana()
				.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
				.semBairro()
				.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoPrincipal);
		this.model.enderecos.add(enderecoCorrespondencia);

		return this;

	}

	public EmpreendimentoBuilder comPessoaIdInvalido() {

		this.model.pessoa = new PessoaJuridicaBuilder().build();

		this.model.pessoa.id = 999999999;

		return this;

	}

	public EmpreendimentoBuilder semEnderecoPrincipal() {

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
				.zonaUrbana()
				.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
				.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoCorrespondencia);

		return this;

	}

	public EmpreendimentoBuilder semEnderecoCorrespondencia() {

		Endereco enderecoPrincipal = new EnderecoBuilder()
				.zonaRural()
				.comTipo(TipoEndereco.ID_PRINCIPAL)
				.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoPrincipal);

		return this;

	}

	public EmpreendimentoBuilder comEnderecoCorrespondenciaRural() {

		Endereco enderecoPrincipal = new EnderecoBuilder()
				.zonaRural()
				.comTipo(TipoEndereco.ID_PRINCIPAL)
				.build();

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
				.zonaRural()
				.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
				.build();

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.add(enderecoPrincipal);
		this.model.enderecos.add(enderecoCorrespondencia);

		return this;

	}

	public EmpreendimentoBuilder comPessoaFisicaSalva() {

		this.model.pessoa = new PessoaFisicaBuilder().save();

		return this;

	}

	public EmpreendimentoBuilder comPessoaJuridicaSalva() {

		this.model.pessoa = new PessoaJuridicaBuilder().save();

		return this;

	}

	public EmpreendimentoBuilder comDenominacao(String denominacao) {

		this.model.denominacao = denominacao;

		return this;

	}

	public EmpreendimentoBuilder semFlagRemovido() {

		this.model.removido = false;

		return this;

	}

	public EmpreendimentoBuilder comFlagRemovido() {

		this.model.removido = true;

		return this;

	}

	public EmpreendimentoBuilder comId(Integer id) {

		this.model.id = id;

		return this;

	}

	public EmpreendimentoBuilder comEnderecos(Set<Endereco> enderecos) {

		this.model.enderecos = new HashSet<>();
		this.model.enderecos.addAll(enderecos);

		return this;

	}

	public EmpreendimentoBuilder comPessoaEstrangeira() {

		this.model.pessoa = new PessoaFisicaBuilder(true).build();

		return this;

	}

	public EmpreendimentoBuilder comProprietarioEstrangeiro() {

		this.model.proprietarios = new HashSet<>();
		this.model.proprietarios.add(new PessoaJuridicaBuilder().build().save());
		this.model.proprietarios.add(new PessoaFisicaBuilder(true).build().save());

		return this;

	}

	public EmpreendimentoBuilder comRepresentanteLegalEstrangeiro() {

		this.model.representantesLegais = new HashSet<>();
		this.model.representantesLegais.add(new PessoaFisicaBuilder().build().save());
		this.model.representantesLegais.add(new PessoaFisicaBuilder(true).build().save());

		return this;

	}

	public EmpreendimentoBuilder comResponsavelLegalEstrangeiro() {

		this.model.responsaveisLegais = new HashSet<>();
		this.model.responsaveisLegais.add(new PessoaFisicaBuilder().build().save());
		this.model.responsaveisLegais.add(new PessoaFisicaBuilder(true).build().save());

		return this;

	}

	public EmpreendimentoBuilder comResponsavelTecnicoEstrangeiro() {

		this.model.responsaveisTecnicos = new HashSet<>();
		this.model.responsaveisTecnicos.add(new PessoaFisicaBuilder().build().save());
		this.model.responsaveisTecnicos.add(new PessoaFisicaBuilder(true).build().save());

		return this;

	}

}