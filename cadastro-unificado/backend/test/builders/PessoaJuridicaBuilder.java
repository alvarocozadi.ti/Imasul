package builders;

import models.PessoaJuridica;
import utils.CNPJDigitCalculator;

import java.util.Date;
import java.util.Random;

public class PessoaJuridicaBuilder extends PessoaBuilder<PessoaJuridica> {

	private static final Random RANDOM = new Random();

	public PessoaJuridicaBuilder() {

		super(new PessoaJuridica());

		padrao();

	}

	public PessoaJuridicaBuilder(PessoaJuridica model) {
		super(model);
	}

	@Override
	protected PessoaJuridicaBuilder padrao() {

		super.padrao();

		this.model.cnpj = geraCnpjRandomicoValido();
		this.model.razaoSocial = "Empresa SA";
		this.model.dataConstituicao = new Date();

		return this;

	}

	public PessoaJuridicaBuilder semRazaoSocial() {

		this.model.razaoSocial = null;

		return this;

	}

	private String geraCnpjRandomicoValido() {

		final StringBuilder digitos = new StringBuilder();

		for (int i = 0; i < 12; i++) {
			digitos.append(RANDOM.nextInt(10));
		}

		final String cnpjSemDigitos = digitos.toString();
		final String cnpjComDigitos = cnpjSemDigitos + CNPJDigitCalculator.calculate(cnpjSemDigitos);

		return cnpjComDigitos;

	}

	public PessoaJuridicaBuilder comCnpjValido() {

		this.model.cnpj = geraCnpjRandomicoValido();

		return this;

	}

	public PessoaJuridicaBuilder semCnpj() {

		this.model.cnpj = null;

		return this;

	}

	public PessoaJuridicaBuilder comCnpj(String cnpj) {

		this.model.cnpj = cnpj;

		return this;

	}

	public PessoaJuridicaBuilder comCnpjInvalido() {

		this.model.cnpj = "11211111111111";

		return this;

	}

	public PessoaJuridicaBuilder semDataConstituicao() {

		this.model.dataConstituicao = null;

		return this;

	}

	public PessoaJuridicaBuilder comNomeFantasia() {

		this.model.nomeFantasia = "Nome fantasia";

		return this;

	}

	public PessoaJuridicaBuilder semNomeFantasia() {

		this.model.nomeFantasia = null;

		return this;

	}

	public PessoaJuridicaBuilder comInscricaoEstadual() {

		this.model.inscricaoEstadual = "1234567891234";

		return this;

	}

	public PessoaJuridicaBuilder comRazaoSocial(String razaoSocial) {

		this.model.razaoSocial = razaoSocial;

		return this;

	}

	public PessoaJuridicaBuilder semFlagRemovido() {

		this.model.removido = false;

		return this;

	}

	public PessoaJuridicaBuilder comFlagRemovido() {

		this.model.removido = true;

		return this;

	}

	public PessoaJuridicaBuilder comInscricaoEstadual(String inscricaoEstadual) {

		this.model.inscricaoEstadual = inscricaoEstadual;

		return this;

	}

	public PessoaJuridicaBuilder comId(Integer id) {

		this.model.id = id;

		return this;

	}

	public PessoaJuridicaBuilder comNomeFantasia(String nomeFantasia) {

		this.model.nomeFantasia = nomeFantasia;

		return this;

	}

	public PessoaJuridicaBuilder tagBloqueado(Boolean bloqueado) {

		this.model.bloqueado = bloqueado;

		return this;

	}

}
