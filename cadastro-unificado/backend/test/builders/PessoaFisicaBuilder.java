package builders;

import models.*;
import utils.CPFDigitCalculator;

import java.util.Date;
import java.util.HashSet;
import java.util.Random;

public class PessoaFisicaBuilder extends PessoaBuilder<PessoaFisica>{

	private static final Random RANDOM = new Random();

	public PessoaFisicaBuilder() {

		super(new PessoaFisica());

		this.model.estrangeiro = false;

		padrao();

	}

	public PessoaFisicaBuilder(Boolean estrangeiro) {

		super(new PessoaFisica());

		this.model.estrangeiro = estrangeiro;

		padrao();

	}

	public PessoaFisicaBuilder(PessoaFisica model) {
		super(model);
	}

	@Override
	protected PessoaFisicaBuilder padrao() {

		super.padrao();

		this.model.nome = "João da Silva";
		this.model.dataNascimento = new Date();
		this.model.nomeMae = "Jandira da Silva";
		this.model.sexo = Sexo.MASCULINO;
		this.model.estadoCivil = EstadoCivil.DIVORCIADO;

		if(this.model.estrangeiro)
			estrangeiro();
		else
			brasileiro();

		return this;

	}

	private void estrangeiro() {

		this.model.estrangeiro = true;

		Random randomico = new Random();
		Long passaporte = randomico.nextLong();

		if(passaporte < 0)
			passaporte = passaporte * -1;

		this.model.passaporte = passaporte.toString();

	}

	private void brasileiro() {

		this.model.naturalidade = "Lavrense";
		this.model.cpf = geraCpfRandomicoValido();
		this.model.rg = new RegistroGeral();
		this.model.rg.numero = "105152121";

	}

	public PessoaFisicaBuilder comFlagRemovido() {

		this.model.removido = true;

		return this;

	}

	public PessoaFisicaBuilder comId() {

		this.model.id = gerarId();

		return this;

	}

	public PessoaFisicaBuilder semFlagRemovido() {

		this.model.removido = false;

		return this;

	}


	public PessoaFisicaBuilder comPassaporte(String passaporte) {

		this.model.passaporte = passaporte;

		return this;

	}

	public PessoaFisicaBuilder comTituloEleitoralComNumeroValido() {

		this.model.tituloEleitoral = new TituloEleitoral();
		this.model.tituloEleitoral.numero = "103176370132";

		return this;

	}

	public PessoaFisicaBuilder comTituloEleitoralCompleto() {

		this.model.tituloEleitoral = new TituloEleitoral();
		this.model.tituloEleitoral.numero = "103176370132";
		this.model.tituloEleitoral.zona = "1232";
		this.model.tituloEleitoral.secao = "122";

		return this;

	}

	public PessoaFisicaBuilder comEnderecosValidos() {

		this.model.enderecos = new HashSet<>();

		Endereco enderecoPrincipal = new EnderecoBuilder()
				.zonaUrbana()
				.comTipo(TipoEndereco.ID_PRINCIPAL)
				.build();

		Endereco enderecoCorrespondencia = new EnderecoBuilder()
				.zonaUrbana()
				.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
				.build();

		this.model.enderecos.add(enderecoPrincipal);
		this.model.enderecos.add(enderecoCorrespondencia);

		return this;

	}

	public PessoaFisicaBuilder comEnderecoUrbanoValido() {

		this.model.enderecos = new HashSet<>();
		Endereco endereco = new EnderecoBuilder().zonaUrbana().build();
		this.model.enderecos.add(endereco);

		return this;

	}

	public PessoaFisicaBuilder comEnderecoRuralValido() {

		this.model.enderecos = new HashSet<>();
		Endereco endereco = new EnderecoBuilder().zonaRural().build();
		this.model.enderecos.add(endereco);

		return this;

	}

	public PessoaFisicaBuilder comRgCompleto() {

		this.model.rg = new RegistroGeral();
		this.model.rg.numero = "105152121";
		this.model.rg.orgaoExpedidor = "SSP";
		this.model.rg.dataExpedicao = new Date();

		return this;

	}

	public PessoaFisicaBuilder comRg(RegistroGeral rg) {

		this.model.rg = rg;

		return this;

	}

	public PessoaFisicaBuilder semNome() {

		this.model.nome = null;

		return this;

	}

	private String geraCpfRandomicoValido() {

		final StringBuilder digitos = new StringBuilder();

		for (int i = 0; i < 9; i++) {
			digitos.append(RANDOM.nextInt(10));
		}

		final String cpfSemDigitos = digitos.toString();
		final String cpfComDigitos = cpfSemDigitos + CPFDigitCalculator.calculate(cpfSemDigitos);

		return cpfComDigitos;

	}

	public PessoaFisicaBuilder comCpfValido() {

		this.model.cpf = geraCpfRandomicoValido();

		return this;

	}

	public PessoaFisicaBuilder semCpf() {

		this.model.cpf = null;

		return this;

	}

	public PessoaFisicaBuilder comCpfInvalido() {

		this.model.cpf = "11211111111";

		return this;

	}

	public PessoaFisicaBuilder semDataDeNascimento() {

		this.model.dataNascimento = null;

		return this;

	}

	public PessoaFisicaBuilder semNomeMae() {

		this.model.nomeMae = null;

		return this;

	}

	public PessoaFisicaBuilder semNaturalidade() {

		this.model.naturalidade = null;

		return this;

	}

	public PessoaFisicaBuilder semSexo() {

		this.model.sexo = null;

		return this;

	}

	public PessoaFisicaBuilder semEstadoCivil() {

		this.model.estadoCivil = null;

		return this;

	}

	public PessoaFisicaBuilder semTituloEleitoral() {

		this.model.tituloEleitoral = null;

		return this;

	}

	public PessoaFisicaBuilder comTituloEleitoralSemNumero() {

		this.model.tituloEleitoral = new TituloEleitoral();

		return this;

	}

	public PessoaFisicaBuilder comTituloEleitoralComNumeroInvalido() {

		this.model.tituloEleitoral = new TituloEleitoral();
		this.model.tituloEleitoral.numero = "1111111111111";

		return this;

	}

	public PessoaFisicaBuilder semRg() {

		this.model.rg = null;

		return this;

	}

	public PessoaFisicaBuilder semPassaporte() {

		this.model.passaporte = null;

		return this;

	}

	public PessoaFisicaBuilder comNaturalidade() {

		this.model.naturalidade = "Lavrense";

		return this;

	}

	public PessoaFisicaBuilder comNomeRandomico() {

		Random randomico = new Random();
		Long nome = randomico.nextLong();

		if(nome < 0)
			nome = nome * -1;

		model.nome = nome.toString();

		return this;
	}

	public PessoaFisicaBuilder comCpf(String cpf) {

		this.model.cpf = cpf;

		return this;

	}

	public PessoaFisicaBuilder comId(Integer id) {

		this.model.id = id;

		return this;

	}

	public PessoaFisicaBuilder comNome(String nome) {

		this.model.nome = nome;

		return this;

	}

	public PessoaFisicaBuilder tagBloqueado(Boolean bloqueado) {

		this.model.bloqueado = bloqueado;

		return this;

	}

}
