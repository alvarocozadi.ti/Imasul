package unit;

import builders.*;
import models.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.DatabaseCleaner;

import java.util.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EmpreendimentoTest {

	public static class Verificacoes {

		public static void verificaEmpreendimentos(Empreendimento empreendimento, Empreendimento empreendimentoEditar) {

			verificaPessoas(empreendimento.pessoa, empreendimentoEditar.pessoa);
			assertEquals(empreendimento.denominacao, empreendimentoEditar.denominacao);
			assertEquals(empreendimento.pessoa.id, empreendimentoEditar.pessoa.id);

			for(Endereco endereco : empreendimento.enderecos) {

				for(Endereco enderecoEditar : empreendimentoEditar.enderecos) {

					if(endereco.tipo == enderecoEditar.tipo) {

						verificaEnderecos(endereco, enderecoEditar);

					}

				}

			}

			assertEquals(empreendimento.localizacao.geometria, empreendimentoEditar.localizacao.geometria);

			if(empreendimento.contatos != null && empreendimentoEditar.contatos != null)
				verificaContatos(empreendimento.contatos.iterator().next(), empreendimentoEditar.contatos.iterator().next());

			if(empreendimento.proprietarios != null && empreendimentoEditar.proprietarios != null)
				verificaPessoas(empreendimento.proprietarios.iterator().next(), empreendimentoEditar.proprietarios.iterator().next());

			if(empreendimento.responsaveisTecnicos != null && empreendimentoEditar.responsaveisTecnicos != null)
				verificaPessoas(empreendimento.responsaveisTecnicos.iterator().next(), empreendimentoEditar.responsaveisTecnicos.iterator().next());

			if(empreendimento.representantesLegais != null && empreendimentoEditar.representantesLegais != null)
				verificaPessoas(empreendimento.representantesLegais.iterator().next(), empreendimentoEditar.representantesLegais.iterator().next());

			if(empreendimento.responsaveisLegais != null && empreendimentoEditar.responsaveisLegais != null)
				verificaPessoas(empreendimento.responsaveisLegais.iterator().next(), empreendimentoEditar.responsaveisLegais.iterator().next());

		}

		public static void verificaPessoa(Pessoa pessoa) {

			assertNotNull(pessoa);
			assertNotNull(pessoa.dataCadastro);
			assertNotNull(pessoa.dataAtualizacao);
			assertFalse(pessoa.removido);
			assertNotNull(pessoa.enderecos);

			Iterator<Endereco> enderecoIterator = pessoa.enderecos.iterator();

			verificaEndereco(enderecoIterator.next());

			verificaEndereco(enderecoIterator.next());

			if(pessoa.contatos != null && !pessoa.contatos.isEmpty()) {

				verificaContato(pessoa.contatos.iterator().next());

			}

			assertNotNull(pessoa.tipo);

			if(pessoa.tipo == TipoPessoa.FISICA) {

				PessoaFisica pessoaFisica = (PessoaFisica) pessoa;

				assertNotNull(pessoaFisica.cpf);
				assertNotNull(pessoaFisica.naturalidade);
				assertNotNull(pessoaFisica.nome);
				assertNotNull(pessoaFisica.nomeMae);
				assertNotNull(pessoaFisica.dataNascimento);
				assertNotNull(pessoaFisica.estadoCivil);
				assertNotNull(pessoaFisica.estrangeiro);
				assertNotNull(pessoaFisica.rg);
				assertNotNull(pessoaFisica.sexo);

			} else {

				PessoaJuridica pessoaJuridica = (PessoaJuridica) pessoa;

				assertNotNull(pessoaJuridica.razaoSocial);
				assertNotNull(pessoaJuridica.cnpj);
				assertNotNull(pessoaJuridica.dataConstituicao);

			}

		}

		public static void verificaPessoas(Pessoa pessoaOrigem, Pessoa pessoaSalva) {

			assertNotNull(pessoaOrigem);
			assertNotNull(pessoaSalva);

			assertEquals(pessoaOrigem.dataCadastro, pessoaSalva.dataCadastro);
			assertEquals(pessoaOrigem.dataAtualizacao, pessoaSalva.dataAtualizacao);
			assertEquals(pessoaOrigem.removido, pessoaSalva.removido);

			assertNotNull(pessoaOrigem.enderecos);
			assertNotNull(pessoaSalva.enderecos);

			for(Endereco endereco : pessoaOrigem.enderecos) {

				for(Endereco enderecoEditar : pessoaSalva.enderecos) {

					if(endereco.tipo == enderecoEditar.tipo) {

						verificaEnderecos(endereco, enderecoEditar);

					}

				}

			}

			if(pessoaOrigem.contatos != null
					&& pessoaSalva.contatos != null
					&& !pessoaOrigem.contatos.isEmpty()
					&& !pessoaSalva.contatos.isEmpty()) {

				verificaContatos(pessoaOrigem.contatos.iterator().next(), pessoaSalva.contatos.iterator().next());

			}

			assertEquals(pessoaOrigem.tipo, pessoaSalva.tipo);

			if(pessoaOrigem.tipo == TipoPessoa.FISICA) {

				PessoaFisica pessoaFisicaOrigem = (PessoaFisica) pessoaOrigem;
				PessoaFisica pessoaFisicaSalva = (PessoaFisica) pessoaSalva;

				assertEquals(pessoaFisicaOrigem.cpf, pessoaFisicaSalva.cpf);
				assertEquals(pessoaFisicaOrigem.naturalidade, pessoaFisicaSalva.naturalidade);
				assertEquals(pessoaFisicaOrigem.nome, pessoaFisicaSalva.nome);
				assertEquals(pessoaFisicaOrigem.nomeMae, pessoaFisicaSalva.nomeMae);
				assertEquals(pessoaFisicaOrigem.dataNascimento, pessoaFisicaSalva.dataNascimento);
				assertEquals(pessoaFisicaOrigem.estadoCivil, pessoaFisicaSalva.estadoCivil);
				assertEquals(pessoaFisicaOrigem.estrangeiro, pessoaFisicaSalva.estrangeiro);
				assertEquals(pessoaFisicaOrigem.rg.numero , pessoaFisicaSalva.rg.numero);
				assertEquals(pessoaFisicaOrigem.rg.orgaoExpedidor , pessoaFisicaSalva.rg.orgaoExpedidor);
				assertEquals(pessoaFisicaOrigem.rg.dataExpedicao , pessoaFisicaSalva.rg.dataExpedicao);
				assertEquals(pessoaFisicaOrigem.sexo, pessoaFisicaSalva.sexo);

			} else {

				PessoaJuridica pessoaJuridicaOrigem = (PessoaJuridica) pessoaOrigem;
				PessoaJuridica pessoaJuridicaSalva = (PessoaJuridica) pessoaSalva;

				assertEquals(pessoaJuridicaOrigem.razaoSocial, pessoaJuridicaSalva.razaoSocial);
				assertEquals(pessoaJuridicaOrigem.cnpj, pessoaJuridicaSalva.cnpj);
				assertEquals(pessoaJuridicaOrigem.dataConstituicao, pessoaJuridicaSalva.dataConstituicao);

			}

		}

		public static void verificaEnderecos(Endereco enderecoOrigem, Endereco enderecoSalvo) {

			assertEquals(enderecoOrigem.id, enderecoSalvo.id);
			assertEquals(enderecoOrigem.tipo, enderecoSalvo.tipo);
			assertEquals(enderecoOrigem.zonaLocalizacao, enderecoSalvo.zonaLocalizacao);
			assertEquals(enderecoOrigem.pais, enderecoSalvo.pais);
			assertEquals(enderecoOrigem.removido, enderecoSalvo.removido);

			if(enderecoOrigem.zonaLocalizacao.equals(ZonaLocalizacao.URBANA)) {

				assertEquals(enderecoOrigem.logradouro, enderecoSalvo.logradouro);
				assertEquals(enderecoOrigem.numero, enderecoSalvo.numero);
				assertEquals(enderecoOrigem.bairro, enderecoSalvo.bairro);
				assertEquals(enderecoOrigem.cep, enderecoSalvo.cep);
				assertEquals(enderecoOrigem.municipio, enderecoSalvo.municipio);

			} else {

				assertEquals(enderecoOrigem.descricaoAcesso, enderecoSalvo.descricaoAcesso);

			}

		}

		public static void verificaEndereco(Endereco endereco) {

			assertNotNull(endereco.id);
			assertNotNull(endereco.tipo);
			assertNotNull(endereco.zonaLocalizacao);
			assertNotNull(endereco.pais);
			assertNotNull(endereco.removido);
			assertFalse(endereco.removido);

			if(endereco.zonaLocalizacao.equals(ZonaLocalizacao.URBANA)) {

				assertNotNull(endereco.logradouro);
				assertNotNull(endereco.numero);
				assertNotNull(endereco.bairro);
				assertNotNull(endereco.cep);
				assertNotNull(endereco.municipio);

			} else {

				assertNotNull(endereco.descricaoAcesso);

			}

		}

		public static void verificaContatos(Contato contatoOrigem, Contato contatoSalvo) {

			if(contatoOrigem == null && contatoSalvo == null) {

				return;

			}

			assertEquals(contatoOrigem.tipo, contatoSalvo.tipo);
			assertEquals(contatoOrigem.valor, contatoSalvo.valor);
			assertEquals(contatoOrigem.removido, contatoSalvo.removido);

		}

		public static void verificaContato(Contato contato) {

			assertNotNull(contato.tipo);
			assertNotNull(contato.valor);
			assertNotNull(contato.removido);
			assertFalse(contato.removido);

		}

		public static void verificaLocalizacao(Localizacao localizacao) {

			assertNotNull(localizacao.id);
			assertNotNull(localizacao.geometria);

		}

		public static void verificaSetPessoa(Set pessoas) {

			Iterator<Pessoa> pessoasIterator = pessoas.iterator();
			Pessoa pessoa = pessoasIterator.next();
			verificaPessoa(pessoa);

		}

	}

	public static class Cadastro extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveCadastrarComPessoaFisica() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build();
			empreendimento.save();
			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComPessoaJuridica() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridica()
					.build()
					.save();
			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComTodosAtributos() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comContatos()
					.comProprietarios()
					.comResponsaveisTecnicos()
					.comRepresentantesLegais()
					.comResponsaveisLegais()
					.build()
					.save();

			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			assertNotEmpty(empreendimento.contatos);
			Iterator<Contato> contatosIterator = empreendimento.contatos.iterator();
			Contato contato = contatosIterator.next();
			Verificacoes.verificaContato(contato);

			assertNotNull(empreendimento.localizacao);
			Verificacoes.verificaLocalizacao(empreendimento.localizacao);

			assertNotNull(empreendimento.proprietarios);
			Verificacoes.verificaSetPessoa(empreendimento.proprietarios);

			assertNotNull(empreendimento.responsaveisTecnicos);
			Verificacoes.verificaSetPessoa(empreendimento.responsaveisTecnicos);

			assertNotNull(empreendimento.representantesLegais);
			Verificacoes.verificaSetPessoa(empreendimento.representantesLegais);

			assertNotNull(empreendimento.responsaveisLegais);
			Verificacoes.verificaSetPessoa(empreendimento.responsaveisLegais);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComContatos() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridica()
					.comContatos()
					.build()
					.save();
			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotEmpty(empreendimento.contatos);
			Iterator<Contato> contatosIterator = empreendimento.contatos.iterator();
			Contato contato = contatosIterator.next();
			Verificacoes.verificaContato(contato);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComProprietarios() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridica()
					.comProprietarios()
					.build()
					.save();

			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.proprietarios);
			Verificacoes.verificaSetPessoa(empreendimento.proprietarios);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComResponsaveisTecnicos() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comResponsaveisTecnicos()
					.build()
					.save();

			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.responsaveisTecnicos);
			Verificacoes.verificaSetPessoa(empreendimento.responsaveisTecnicos);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComRepresentantesLegais() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridica()
					.comRepresentantesLegais()
					.build()
					.save();

			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.representantesLegais);
			Verificacoes.verificaSetPessoa(empreendimento.representantesLegais);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComResponsaveisLegais() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comResponsaveisLegais()
					.build()
					.save();

			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoa(empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.responsaveisLegais);
			Verificacoes.verificaSetPessoa(empreendimento.responsaveisLegais);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void deveCadastrarComPessoaJaExistenteNoBanco() {

			Pessoa pessoa = new PessoaFisicaBuilder().build().save();

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoa(pessoa)
					.build()
					.save();

			pessoa.refresh();
			empreendimento.refresh();

			assertNotNull(empreendimento);
			assertNotNull(empreendimento.id);
			assertNotNull(empreendimento.denominacao);
			Verificacoes.verificaPessoas(pessoa, empreendimento.pessoa);

			assertNotEmpty(empreendimento.enderecos);
			Iterator<Endereco> enderecoIterator = empreendimento.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			Verificacoes.verificaEndereco(endereco);

			assertNotNull(empreendimento.dataCadastro);
			assertNotNull(empreendimento.dataAtualizacao);

			empreendimento._delete();

		}

		@Test
		public void naoDeveCadastrarSemDenominacao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semDenominacao()
					.build();

			assertValidationExceptionAndMessage("empreendimento.denominacao.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarSemPessoa() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semPessoa()
					.build();

			assertValidationExceptionAndMessage("empreendimento.pessoa.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComPessoaNaoUnica() {

			Empreendimento empreendimento = new EmpreendimentoBuilder().build().save();

			Empreendimento empreendimentoTeste = new EmpreendimentoBuilder()
					.comPessoa(empreendimento.pessoa)
					.build();

			assertValidationExceptionAndMessage("empreendimento.pessoa.unico");

			empreendimentoTeste.save();

		}

		@Test
		public void naoDeveCadastrarComPessoaInvalida() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comCpfInvalido()
					.build();

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoa(pessoaFisica)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.cpf.invalido");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComContatosInvalidos() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comContatosInvalidos()
					.build();

			assertValidationExceptionAndMessage("contato.tipo.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarSemEndereco() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semEndereco()
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComEnderecoInvalido() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comEnderecoInvalido()
					.build();

			assertValidationExceptionAndMessage("endereco.bairro.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComIdPessoaNaoExistenteNoBanco() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaIdInvalido()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.naoEncontrada");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarSemEnderecoPrincipal() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semEnderecoPrincipal()
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarSemEnderecoDeCorrespondencia() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semEnderecoCorrespondencia()
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComEnderecoCorrespondenciaRural() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comEnderecoCorrespondenciaRural()
					.build();

			assertValidationExceptionAndMessage("enderecos.correspondencia.urbanoObrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarSemLocalizacao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.semLocalizacao()
					.build();

			assertValidationExceptionAndMessage("empreendimento.localizacao.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComLocalizacaoInvalida() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comLocalizacaoSemGeometria()
					.build();

			assertValidationExceptionAndMessage("localizacao.geometria.obrigatorio");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarLocalizacaoComMaisDeUmPoligono() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comLocalizacaoComMaisDeUmPoligono()
					.build();

			assertValidationExceptionAndMessage("localizacao.geometria.erro");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComPessoaEstrangeira() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaEstrangeira()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.estrangeira");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComProprietarioEstrangeiro() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comProprietarioEstrangeiro()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.invalida");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComRepresentanteLegalEstrangeiro() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comRepresentanteLegalEstrangeiro()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.invalida");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComResponsavelLegalEstrangeiro() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comResponsavelLegalEstrangeiro()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.invalida");

			empreendimento.save();

		}

		@Test
		public void naoDeveCadastrarComResponsavelTecnicoEstrangeiro() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comResponsavelTecnicoEstrangeiro()
					.build();

			assertValidationExceptionAndMessage("empreendimento.cadastro.pessoa.invalida");

			empreendimento.save();

		}


	}

	public static class FindByFilter extends BaseUnitTest {

		private static final String cnpj = "84686082000160";
		private static final String cpf = "27347696623";
		private static final String denominacao = "COMÉRCIO DE CARROS";
		private static FiltroEmpreendimento filtro = null;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			filtro = new FiltroEmpreendimento();
			filtro.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;
			filtro.busca = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.comCnpj(cnpj)
					.save();

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comCpf(cpf)
					.save();

			new EmpreendimentoBuilder()
					.comPessoa(pessoaFisica)
					.comDenominacao(denominacao)
					.save();

			new EmpreendimentoBuilder()
					.comPessoa(pessoaJuridica)
					.comDenominacao(denominacao)
					.save();

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaFisicaSalva()
						.save();

			}

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaJuridicaSalva()
						.save();

			}

		}

		@AfterClass
		public static void afterClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveFiltrarPorDenominacao() {

			filtro.busca = denominacao;
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(2, lista.size());
			assertEquals(denominacao, lista.get(0).denominacao);

		}

		@Test
		public void deveFiltrarPorDenominacaoParcial() {

			filtro.busca = "CARROS";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(2, lista.size());
			assertEquals(denominacao, lista.get(0).denominacao);

		}

		@Test
		public void deveFiltrarPorDenominacaoIgnorandoAcentos() {

			filtro.busca = "COMERCIO";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(2, lista.size());
			assertEquals(denominacao, lista.get(0).denominacao);

		}

		@Test
		public void deveFiltrarPorDenominacaoIgnorandoCaixa() {

			filtro.busca = "comercio";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(2, lista.size());
			assertEquals(denominacao, lista.get(0).denominacao);

		}

		@Test
		public void deveFiltrarPorCpf() {

			filtro.busca = "27347696623";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(1, lista.size());

			PessoaFisica pessoaFisica = (PessoaFisica) lista.get(0).pessoa;

			assertEquals(cpf, pessoaFisica.cpf);

		}

		@Test
		public void deveFiltrarPorCnpj() {

			filtro.busca = "84686082000160";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);
			assertEquals(1, lista.size());

			PessoaJuridica pessoaJuridica = (PessoaJuridica) lista.get(0).pessoa;

			assertEquals(cnpj, pessoaJuridica.cnpj);

		}

		@Test
		public void naoDeveFiltrarPorCnpjParcial() {

			filtro.busca = cnpj.substring(0, cnpj.length() - 2);
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertEmpty(lista);

		}

		@Test
		public void naoDeveFiltrarPorCpfParcial() {

			filtro.busca = cpf.substring(0, cpf.length() - 2);
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertEmpty(lista);

		}

		@Test
		public void deveRetornarListaVaziaSeNenhumResultadoEncontrado() {

			filtro.busca = "teste";
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertEmpty(lista);

		}

		@Test
		public void deveRetornarTodosDeAcordoComTamPagSeFiltroVazio() {

			filtro.busca = null;
			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertEquals(filtro.tamanhoPagina, lista.size());

		}

		@Test
		public void deveListarComPaginacao() {

			FiltroEmpreendimento filtro2 = new FiltroEmpreendimento();
			filtro2.busca = null;
			filtro2.tamanhoPagina = 20;
			filtro2.numeroPagina  = 2;
			filtro2.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro2);

			assertNotEmpty(lista);

			assertEquals(12, lista.size());

		}

		@Test
		public void deveListarOrdenadoPorDenominacaoAscendentePorPadrao() {

			filtro.busca = null;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderAsc(lista.get(0).denominacao, lista.get(10).denominacao, lista.get(19).denominacao);

		}

		@Test
		public void deveListarOrdenadoPorDenominacaoAscendente() {

			FiltroEmpreendimento filtro2 = new FiltroEmpreendimento();
			filtro2.busca = null;
			filtro2.tamanhoPagina = 20;
			filtro2.numeroPagina  = 1;
			filtro2.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro2);

			assertNotEmpty(lista);

			assertOrderAsc(lista.get(0).denominacao, lista.get(10).denominacao, lista.get(19).denominacao);

		}

		@Test
		public void deveListarOrdenadoPorDenominacaoDescendente() {

			FiltroEmpreendimento filtro2 = new FiltroEmpreendimento();
			filtro2.busca = null;
			filtro2.tamanhoPagina = 20;
			filtro2.numeroPagina  = 1;
			filtro2.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_DESC;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro2);

			assertNotEmpty(lista);

			assertOrderDesc(lista.get(0).denominacao, lista.get(10).denominacao, lista.get(19).denominacao);

		}

		@Test
		public void deveListarOrdeadoPorMunicipioAscendente() {

			FiltroEmpreendimento filtro2 = new FiltroEmpreendimento();
			filtro2.busca = null;
			filtro2.tamanhoPagina = 20;
			filtro2.numeroPagina  = 1;
			filtro2.ordenacao = FiltroEmpreendimento.Ordenacao.MUNICIPIO_ASC;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro2);

			assertNotEmpty(lista);

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(10).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(19).enderecos);

			assertOrderAsc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

		}

		@Test
		public void deveListarOrdenadoPorMunicipioDescendente() {

			FiltroEmpreendimento filtro2 = new FiltroEmpreendimento();
			filtro2.busca = null;
			filtro2.tamanhoPagina = 20;
			filtro2.numeroPagina  = 1;
			filtro2.ordenacao = FiltroEmpreendimento.Ordenacao.MUNICIPIO_DESC;

			List<Empreendimento> lista = Empreendimento.findByFilter(filtro2);

			assertNotEmpty(lista);

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(10).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(19).enderecos);

			assertOrderDesc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

		}

	}

	public static class CountByFilter extends BaseUnitTest {

		private static final String cnpj = "84686082000160";
		private static final String cpf = "27347696623";
		private static final String denominacao = "COMÉRCIO DE CARROS";
		private static FiltroEmpreendimento filtro = null;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			filtro = new FiltroEmpreendimento();
			filtro.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;
			filtro.busca = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.comCnpj(cnpj)
					.save();

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comCpf(cpf)
					.save();

			new EmpreendimentoBuilder()
					.comPessoa(pessoaFisica)
					.comDenominacao(denominacao)
					.save();

			new EmpreendimentoBuilder()
					.comPessoa(pessoaJuridica)
					.comDenominacao(denominacao)
					.save();

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaFisicaSalva()
						.save();

			}

			for(int i = 0; i < 15; i++) {

				new EmpreendimentoBuilder()
						.comPessoaJuridicaSalva()
						.save();

			}

		}

		@AfterClass
		public static void afterClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveContarResultadosFiltradosPorDenominacao() {

			filtro.busca = "Denominacao";

			Long contagem = Empreendimento.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(30, contagem.longValue());

		}

		@Test
		public void deveContarResultadosFiltradosPorCpf() {

			filtro.busca = "27347696623";

			Long contagem = Empreendimento.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(1, contagem.longValue());

		}

		@Test
		public void deveContarResultadosFiltradosPorCnpj() {

			filtro.busca = "84686082000160";

			Long contagem = Empreendimento.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(1, contagem.longValue());

		}

		@Test
		public void deveContarResultadosSemFiltro() {

			filtro.busca = null;

			Long contagem = Empreendimento.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(32, contagem.longValue());

		}

	}

	public static class Remocao extends BaseUnitTest {

		private static Empreendimento empreendimentoNaoRemovido = null;

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveRemover() {

			empreendimentoNaoRemovido = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.semFlagRemovido()
					.save();

			empreendimentoNaoRemovido.remove();

			assertTrue(empreendimentoNaoRemovido.removido);

			empreendimentoNaoRemovido._delete();

		}

		@Test
		public void deveRemoverTambemSeusRelacionamentos() {

			empreendimentoNaoRemovido = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.semFlagRemovido()
					.save();

			empreendimentoNaoRemovido.remove();

			assertTrue(empreendimentoNaoRemovido.removido);
			assertNull(empreendimentoNaoRemovido.pessoa);
			assertNull(empreendimentoNaoRemovido.proprietarios);
			assertNull(empreendimentoNaoRemovido.representantesLegais);
			assertNull(empreendimentoNaoRemovido.responsaveisLegais);
			assertNull(empreendimentoNaoRemovido.responsaveisTecnicos);

			empreendimentoNaoRemovido._delete();

		}

		@Test
		public void naoDeveRemover() {

			empreendimentoNaoRemovido = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.comFlagRemovido()
					.save();

			assertValidationExceptionAndMessage("empreendimento.removido.erro");

			try {

				empreendimentoNaoRemovido.remove();

			} finally {

				empreendimentoNaoRemovido._delete();

			}

		}

	}

	public static class Edicao extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveEditarComPessoaFisica() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Pessoa pessoa = empreendimento.pessoa;

			((PessoaFisica) pessoa).nome = "Outro Nome";

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(pessoa)
					.comEnderecos(empreendimento.enderecos)
					.build();

			empreendimento.update(empreendimentoEditar, null);

			Verificacoes.verificaEmpreendimentos(empreendimento, empreendimentoEditar);

			empreendimento._delete();
			pessoa._delete();

		}

		@Test
		public void deveEditarComPessoaJuridica() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridicaSalva()
					.save();

			Pessoa pessoa = empreendimento.pessoa;

			((PessoaJuridica) pessoa).razaoSocial = "Outro Nome";

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(pessoa)
					.comEnderecos(empreendimento.enderecos)
					.build();

			empreendimento.update(empreendimentoEditar, null);

			Verificacoes.verificaEmpreendimentos(empreendimento, empreendimentoEditar);

			empreendimento._delete();
			pessoa._delete();

		}

		@Test
		public void deveEditarComListas() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaJuridicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(empreendimento.pessoa)
					.comProprietarios()
					.comRepresentantesLegais()
					.comResponsaveisLegais()
					.comResponsaveisTecnicos()
					.comEnderecos(empreendimento.enderecos)
					.build();

			empreendimento.update(empreendimentoEditar, null);

			Verificacoes.verificaEmpreendimentos(empreendimento, empreendimentoEditar);

			empreendimento._delete();

		}

		@Test
		public void naoDeveEditarSemLocalizacao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(empreendimento.pessoa)
					.semLocalizacao()
					.build();

			assertValidationExceptionAndMessage("empreendimento.localizacao.obrigatorio");

			try {

				empreendimento.update(empreendimentoEditar, null);

			} finally {

				empreendimento._delete();

			}

		}

		@Test
		public void naoDeveEditarSemDenominacao() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(empreendimento.pessoa)
					.semDenominacao()
					.build();

			assertValidationExceptionAndMessage("empreendimento.denominacao.obrigatorio");

			try {

				empreendimento.update(empreendimentoEditar, null);

			} finally {

				empreendimento._delete();

			}

		}

		@Test
		public void naoDeveEditarSemPessoa() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.semPessoa()
					.build();

			assertValidationExceptionAndMessage("empreendimento.pessoa.obrigatorio");

			try {

				empreendimento.update(empreendimentoEditar, null);

			} finally {

				empreendimento._delete();

			}

		}

		@Test
		public void naoDeveEditarComPessoaJaExistenteEmOutroEmpreendimento() {

			Empreendimento empreendimento1 = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimento2 = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento2.id)
					.comPessoa(empreendimento1.pessoa)
					.build();

			assertValidationExceptionAndMessage("empreendimento.pessoa.unico");

			try {

				empreendimento2.update(empreendimentoEditar, null);

			} finally {

				empreendimento1._delete();
				empreendimento2._delete();

			}

		}

		@Test
		public void naoDeveEditarAoTrocarTipoPessoa() {

			Empreendimento empreendimento = new EmpreendimentoBuilder()
					.comPessoaFisicaSalva()
					.save();

			Empreendimento empreendimentoEditar = new EmpreendimentoBuilder()
					.comId(empreendimento.id)
					.comPessoa(new PessoaJuridicaBuilder().save())
					.build();

			assertValidationExceptionAndMessage("empreendimento.pessoa.alteracaoNaoPermitida");

			try {

				empreendimento.update(empreendimentoEditar, null);

			} finally {

				empreendimento._delete();

			}

		}

	}

}
