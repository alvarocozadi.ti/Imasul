//package unit.secure.authorization;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import builders.AcaoSistemaBuilder;
//import models.permissoes.AcaoSistema;
//import models.portalSeguranca.Usuario;
//import play.test.UnitTest;
//import secure.models.Action;
//import secure.models.Permissible;
//import secure.models.User;
//import secure.services.AuthorizationService;
//import utils.DatabaseCleaner;
//
//public class AuthorizationTest extends UnitTest {
//
//	private static User usuarioLogado;
//
//	@Before
//	public void before() {
//
//		usuarioLogado = mock(Usuario.class);
//
//	}
//
//	private AcaoSistema createAcaoSistema(String regras) {
//
//		AcaoSistema acaoSistema = new AcaoSistema();
//		acaoSistema.id = 1;
//		acaoSistema.descricao = "Teste";
//		acaoSistema.permissao = "ROLE_TESTE";
//		acaoSistema.regras = regras;
//		acaoSistema.pacoteRegras = "unit.secure.authorization.rules";
//
//		return acaoSistema;
//
//	}
//
//	@Test
//	public void deveAutorizarPossuindoRoleSatisfazendoRegra() {
//
//		AcaoSistema action = this.createAcaoSistema("GrantRuleFake");
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(true);
//
//		boolean retorno = AuthorizationService.getInstance().checkPermission(action, usuarioLogado);
//
//		assertTrue(retorno);
//
//	}
//
//	@Test
//	public void naoDeveAutorizarPossuindoRoleNaoSatisfazendoRegra() {
//
//		AcaoSistema action = this.createAcaoSistema("NegateRuleFake");
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(true);
//
//		boolean retorno = AuthorizationService.getInstance().checkPermission(action, usuarioLogado);
//
//		assertFalse(retorno);
//
//	}
//
//	@Test
//	public void naoDeveAutorizarNaoPossuindoRoleSatisfazendoRegra() {
//
//		AcaoSistema action = this.createAcaoSistema("GrantRuleFake");
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(false);
//
//		boolean retorno = AuthorizationService.getInstance().checkPermission(action, usuarioLogado);
//
//		assertFalse(retorno);
//
//	}
//
//	@Test
//	public void deveAutorizarPossuindoRoleSatisfazendoApenasUmaRegra() {
//
//		AcaoSistema action = this.createAcaoSistema("GrantRuleFake|NegateRuleFake");
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(true);
//
//		boolean retorno = AuthorizationService.getInstance().checkPermission(action, usuarioLogado);
//
//		assertTrue(retorno);
//
//	}
//
//	/*@Test
//	public void devePrencherTodasAcoesDisponiveisNoPermissivel() {
//
//		AcaoSistema acaoSistema1 = new AcaoSistemaBuilder().comPermissao().save();
//		AcaoSistema acaoSistema2 = new AcaoSistemaBuilder().comPermissao().save();
//		AcaoSistema acaoSistema3 = new AcaoSistemaBuilder().comPermissao().save();
//		AcaoSistema acaoSistema4 = new AcaoSistemaBuilder().comPermissao().save();
//
//		List<Integer> actionsIds = new ArrayList<>();
//		actionsIds.add(acaoSistema1.id);
//		actionsIds.add(acaoSistema2.id);
//		actionsIds.add(acaoSistema3.id);
//		actionsIds.add(acaoSistema4.id);
//
//		PermissibleFake permissible = new PermissibleFake();
//		permissible.setAvailableActions(actionsIds);
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(true);
//
//		AuthorizationService.getInstance().fillPermittedActions(usuarioLogado, permissible);
//
//		this.assertPermissivelContemAcoes(permissible, actionsIds);
//	}*/
//
//	/*@Test
//	public void naoDevePreencherAcoesDisponiveisNoPermissivel() {
//
//		AcaoSistema acaoSistema1 = new AcaoSistemaBuilder().comPermissao().save();
//		AcaoSistema acaoSistema2 = new AcaoSistemaBuilder().comPermissao().save();
//
//		List<Integer> actionsIds = new ArrayList<>();
//		actionsIds.add(acaoSistema1.id);
//		actionsIds.add(acaoSistema2.id);
//
//		PermissibleFake permissible = new PermissibleFake();
//		permissible.setAvailableActions(actionsIds);
//
//		when(usuarioLogado.hasRole(any(String.class))).thenReturn(false);
//
//		AuthorizationService.getInstance().fillPermittedActions(usuarioLogado, permissible);
//
//		this.assertPermissivelContemAcoes(permissible, null);
//	}*/
//
//	/*@Test
//	public void devePreencherApenasPrimeiraAcaoNoPermissivel() {
//
//		AcaoSistema acaoSistema1 = new AcaoSistemaBuilder().comPermissao().save();
//		AcaoSistema acaoSistema2 = new AcaoSistemaBuilder().comPermissao().save();
//
//		List<Integer> actionsIds = new ArrayList<>();
//		actionsIds.add(acaoSistema1.id);
//		actionsIds.add(acaoSistema2.id);
//
//		PermissibleFake permissible = new PermissibleFake();
//		permissible.setAvailableActions(actionsIds);
//
//		List<Integer> actionsIdsExpected = new ArrayList<>();
//		actionsIdsExpected.add(acaoSistema1.id);
//
//		when(usuarioLogado.hasRole(acaoSistema1.getRole())).thenReturn(true);
//
//		AuthorizationService.getInstance().fillPermittedActions(usuarioLogado, permissible);
//
//		this.assertPermissivelContemAcoes(permissible, actionsIdsExpected);
//
//	}*/
//
//	@Test
//	public void deveRetornarApenasAcoesUsuarioTemPermissao() {
//
//		Action acaoComPermissao = new AcaoSistemaBuilder().comPermissao().build();
//		Action acaoSemPermissao = new AcaoSistemaBuilder().comPermissao().build();
//
//		when(usuarioLogado.hasRole(acaoComPermissao.getRole())).thenReturn(true);
//		when(usuarioLogado.hasRole(acaoSemPermissao.getRole())).thenReturn(false);
//
//		List<Action> acoes = new ArrayList<>();
//		acoes.add(acaoComPermissao);
//		acoes.add(acaoSemPermissao);
//
//		List<Integer> idsAcoesPermitidas = AuthorizationService.getInstance().checkPermittedActions(acoes, usuarioLogado);
//
//		assertTrue(idsAcoesPermitidas.contains(acaoComPermissao.getId()));
//		assertFalse(idsAcoesPermitidas.contains(acaoSemPermissao.getId()));
//	}
//
//	private void assertPermissivelContemAcoes(Permissible permissivel, List<Integer> idsAcoesEsperadas) {
//
//		List<Integer> idsAcoesPermissivel = permissivel.getPermittedActionsIds();
//
//		if (idsAcoesEsperadas == null || idsAcoesEsperadas.isEmpty()) {
//			assertTrue(idsAcoesPermissivel == null || idsAcoesPermissivel.isEmpty());
//			return;
//		}
//
//		assertTrue(idsAcoesPermissivel != null && !idsAcoesPermissivel.isEmpty());
//		assertEquals(idsAcoesEsperadas.size(), idsAcoesPermissivel.size());
//
//		for (Integer idAcaoEsperada : idsAcoesEsperadas)
//			assertTrue(idsAcoesPermissivel.contains(idAcaoEsperada));
//	}
//
//	public class PermissibleFake implements Permissible {
//
//		List<Action> availableActions;
//		List<Integer> permittedActionsIds;
//
//		public void setAvailableActions(List<Integer> actionsIds) {
//
//			if(this.availableActions == null)
//				this.availableActions = new ArrayList<>();
//
//			for(Integer actionId : actionsIds) {
//				this.availableActions.add((Action) AcaoSistema.findById(actionId));
//			}
//
//		}
//
//		@Override
//		public List<Action> getAvailableActions() {
//			return this.availableActions;
//		}
//
//		@Override
//		public void setPermittedActionsIds(List<Integer> actionsIds) {
//			this.permittedActionsIds = actionsIds;
//		}
//
//		@Override
//		public List<Integer> getPermittedActionsIds() {
//			return this.permittedActionsIds;
//		}
//
//	}
//
//}
