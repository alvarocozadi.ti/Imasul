package unit.secure.authorization.rules;

import secure.models.Permissible;
import secure.models.Rule;
import secure.models.User;

public class NegateRuleFake implements Rule {

	@Override
	public boolean check(User user, Permissible permissible) {
		return false;
	}

}
