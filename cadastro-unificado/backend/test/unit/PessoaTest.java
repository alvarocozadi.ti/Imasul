package unit;

import builders.ContatoBuilder;
import builders.EnderecoBuilder;
import builders.PessoaBuilder;
import models.Contato;
import models.Endereco;
import models.Pessoa;
import models.TipoEndereco;
import models.ZonaLocalizacao;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;

public abstract class PessoaTest extends BaseUnitTest {

	public static abstract class Save<T extends PessoaBuilder> extends BaseUnitTest {

		private Class<T> builderClass;

		public Save() {

			super();

			Type type = getClass().getGenericSuperclass();
			ParameterizedType paramType = (ParameterizedType) type;

			this.builderClass = (Class<T>) paramType.getActualTypeArguments()[0];

		}

		private static void verificaEndereco(Endereco endereco) {

			assertNotNull(endereco.id);
			assertNotNull(endereco.tipo);
			assertNotNull(endereco.zonaLocalizacao);
			assertNotNull(endereco.pais);
			assertNotNull(endereco.removido);
			assertFalse(endereco.removido);

			if (endereco.zonaLocalizacao.equals(ZonaLocalizacao.URBANA)) {

				assertNotNull(endereco.logradouro);
				assertNotNull(endereco.numero);
				assertNotNull(endereco.complemento);
				assertNotNull(endereco.bairro);
				assertNotNull(endereco.cep);
				assertNotNull(endereco.caixaPostal);
				assertNotNull(endereco.municipio);

			} else {

				assertNotNull(endereco.descricaoAcesso);

			}

		}

		private static void verificaContato(Contato contato) {

			assertNotNull(contato.id);
			assertNotNull(contato.tipo);
			assertNotNull(contato.valor);
			assertNotNull(contato.removido);
			assertFalse(contato.removido);

		}

		@Test
		public void deveCadastrarComTodosAtributos() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaRural()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.comComplemento()
					.comCaixaPostal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			Pessoa pessoaSalva = pessoa.save();
			pessoaSalva.refresh();

			assertNotNull(pessoaSalva);
			assertNotNull(pessoaSalva.id);

			assertNotEmpty(pessoaSalva.contatos);

			Iterator<Contato> contatosIterator = pessoaSalva.contatos.iterator();
			Contato contato = contatosIterator.next();
			verificaContato(contato);

			assertNotEmpty(pessoaSalva.enderecos);

			Iterator<Endereco> enderecoIterator = pessoaSalva.enderecos.iterator();
			Endereco endereco = enderecoIterator.next();
			verificaEndereco(endereco);
			endereco = enderecoIterator.next();
			verificaEndereco(endereco);

			assertNotNull(pessoaSalva.dataAtualizacao);
			assertNotNull(pessoaSalva.dataCadastro);
			assertNotNull(pessoaSalva.removido);
			assertFalse(pessoaSalva.removido);

			pessoaSalva._delete();

		}

		@Test
		public void deveCadastrarComContatos() throws InstantiationException, IllegalAccessException {

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos()
					.build();

			Pessoa pessoaSalva = pessoa.save();
			pessoaSalva.refresh();

			assertNotNull(pessoaSalva);
			assertNotNull(pessoaSalva.id);
			assertNotEmpty(pessoaSalva.contatos);

			pessoaSalva._delete();

		}

		@Test
		public void naoDeveCadastrarComContatosSemTipo() throws InstantiationException, IllegalAccessException {

			Contato contato = new ContatoBuilder()
					.semTipo()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.build();

			assertValidationExceptionAndMessage("contato.tipo.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarComContatosSemValor() throws InstantiationException, IllegalAccessException {

			Contato contato = new ContatoBuilder()
					.semValor()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos(false,contato)
					.build();

			assertValidationExceptionAndMessage("contato.valor.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarComEmailsSemEmailPrincipal() throws InstantiationException, IllegalAccessException {

			Contato contato = new ContatoBuilder()
					.tagFalsePrincipal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.build();

			assertValidationExceptionAndMessage("contatos.tipo.emailPrincipalObrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarComMaisDeUmEmailPrincipal() throws InstantiationException, IllegalAccessException {

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, new ContatoBuilder().build(), new ContatoBuilder().build())
					.build();

			assertValidationExceptionAndMessage("contatos.tipo.ApenasUmEmailPrincipal");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemEnderecos() throws InstantiationException, IllegalAccessException {

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.semEnderecos()
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemEnderecoPrincipal() throws InstantiationException, IllegalAccessException {

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemEnderecoDeCorrespondencia() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal)
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarComEnderecoCorrespondenciaRural() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaRural()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("enderecos.correspondencia.urbanoObrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemTipoEndereco() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.semTipo()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.tipo.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemZonaLocalizacaoEndereco() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semZonaLocalizacao()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.zonaLocalizacao.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemLogradouroEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semLogradouro()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.logradouro.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemNumeroEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semNumero()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.numero.obrigatorio");

			pessoa.save();

		}

		@Test
		public void deveCadastrarSemNumeroSeTagSemNumeroTrueEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semNumero()
					.tagSemNumeroTrue()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			Pessoa pessoaSalva = pessoa.save();
			pessoaSalva.refresh();

			assertNotNull(pessoaSalva);
			assertNotNull(pessoaSalva.id);

			pessoaSalva._delete();

		}

		@Test
		public void naoDeveCadastrarSemBairroComEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semBairro()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.bairro.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemCepComEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaUrbana()
					.semCep()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.cep.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemDescricaoAcessoEnderecoZonaRural() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaRural()
					.semDescricaoAcesso()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.descricaoAcesso.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemMunicipioEnderecoZonaRural() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaRural()
					.semMunicipio()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.municipio.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemMunicipioEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaUrbana()
					.semMunicipio()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.municipio.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemPaisEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaUrbana()
					.semPais()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.pais.obrigatorio");

			pessoa.save();

		}

		@Test
		public void naoDeveCadastrarSemPaisEnderecoZonaRural() throws InstantiationException, IllegalAccessException {

			Endereco endereco = new EnderecoBuilder()
					.zonaRural()
					.semPais()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, endereco)
					.build();

			assertValidationExceptionAndMessage("endereco.pais.obrigatorio");

			pessoa.save();

		}

	}

	public static abstract class Edicao<T extends PessoaBuilder> extends BaseUnitTest {

		private Class<T> builderClass;

		public Edicao() {

			super();

			Type type = getClass().getGenericSuperclass();
			ParameterizedType paramType = (ParameterizedType) type;

			this.builderClass = (Class<T>) paramType.getActualTypeArguments()[0];

		}

		@Test
		public void deveEditarComContatosValidos() throws Throwable {

			Contato contato = new ContatoBuilder().build();

			Contato contato2 = new ContatoBuilder()
					.comValor("3595821215")
					.comTipo(4)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato2)
					.build();

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());
			method.invoke(pessoa, pessoaEditada);

			assertNotEmpty(pessoa.contatos);
			assertNotEmpty(pessoaEditada.contatos);

			Iterator<Contato> contatoIteratorPessoaEditada = pessoaEditada.contatos.iterator();
			Iterator<Contato> contatoIteratorPessoaEditar = pessoa.contatos.iterator();

			Contato contatoPessoaEditar = contatoIteratorPessoaEditar.next();

			Contato contatoPessoaEditada = contatoIteratorPessoaEditada.next();

			assertEquals(contatoPessoaEditar.tipo, contatoPessoaEditada.tipo);
			assertEquals(contatoPessoaEditar.valor, contatoPessoaEditada.valor);
			assertEquals(contatoPessoaEditar.removido, contatoPessoaEditada.removido);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarComContatosSemValor() throws Throwable {

			Contato contato = new ContatoBuilder()
					.semValor()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.build();

			assertValidationExceptionAndMessage("contato.valor.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			}finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComEmailsSemEmailPrincipal() throws Throwable {

			Contato contato = new ContatoBuilder()
					.tagFalsePrincipal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.build();

			assertValidationExceptionAndMessage("contatos.tipo.emailPrincipalObrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComMaisDeUmEmailPrincipal() throws Throwable {

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, new ContatoBuilder().build(), new ContatoBuilder().build())
					.build();

			assertValidationExceptionAndMessage("contatos.tipo.ApenasUmEmailPrincipal");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComContatosSemTipo() throws Throwable {

			Contato contato = new ContatoBuilder()
					.semTipo()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos(false, contato)
					.build();

			assertValidationExceptionAndMessage("contato.tipo.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void deveEditarComEnderecosValidos() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaRural()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.comCaixaPostal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.comContatos()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.save();

			Endereco enderecoPrincipal2 = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaUrbana()
					.build();

			Endereco enderecoCorrespondencia2 = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.semCaixaPostal()
					.build();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comContatos()
					.comEnderecos(false, enderecoPrincipal2, enderecoCorrespondencia2)
					.build();

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());
			method.invoke(pessoa, pessoaEditada);

			assertNotEmpty(pessoa.enderecos);
			assertNotEmpty(pessoaEditada.enderecos);

			Iterator<Endereco> enderecoIteratorPessoaEditada = pessoaEditada.enderecos.iterator();
			Iterator<Endereco> enderecoIteratorPessoaEditar = pessoa.enderecos.iterator();

			Endereco principalPessoaEditar = enderecoIteratorPessoaEditar.next();

			Endereco correspondenciaPessoaEditar = enderecoIteratorPessoaEditar.next();

			Endereco principalPessoaEditada = enderecoIteratorPessoaEditada.next();

			Endereco correspondenciaPessoaEditada = enderecoIteratorPessoaEditada.next();

			assertEquals(principalPessoaEditada.numero, principalPessoaEditar.numero);
			assertEquals(principalPessoaEditada.bairro, principalPessoaEditar.bairro);
			assertEquals(principalPessoaEditada.caixaPostal, principalPessoaEditar.caixaPostal);
			assertEquals(principalPessoaEditada.cep, principalPessoaEditar.cep);
			assertEquals(principalPessoaEditada.complemento, principalPessoaEditar.complemento);
			assertEquals(principalPessoaEditada.descricaoAcesso, principalPessoaEditar.descricaoAcesso);
			assertEquals(principalPessoaEditada.logradouro, principalPessoaEditar.logradouro);
			assertEquals(principalPessoaEditada.municipio, principalPessoaEditar.municipio);
			assertEquals(principalPessoaEditada.pais, principalPessoaEditar.pais);
			assertEquals(principalPessoaEditada.removido, principalPessoaEditar.removido);
			assertEquals(principalPessoaEditada.tipo, principalPessoaEditar.tipo);
			assertEquals(principalPessoaEditada.zonaLocalizacao, principalPessoaEditar.zonaLocalizacao);

			assertEquals(correspondenciaPessoaEditada.numero, correspondenciaPessoaEditar.numero);
			assertEquals(correspondenciaPessoaEditada.bairro, correspondenciaPessoaEditar.bairro);
			assertEquals(correspondenciaPessoaEditada.caixaPostal, correspondenciaPessoaEditar.caixaPostal);
			assertEquals(correspondenciaPessoaEditada.cep, correspondenciaPessoaEditar.cep);
			assertEquals(correspondenciaPessoaEditada.complemento, correspondenciaPessoaEditar.complemento);
			assertEquals(correspondenciaPessoaEditada.descricaoAcesso, correspondenciaPessoaEditar.descricaoAcesso);
			assertEquals(correspondenciaPessoaEditada.logradouro, correspondenciaPessoaEditar.logradouro);
			assertEquals(correspondenciaPessoaEditada.municipio, correspondenciaPessoaEditar.municipio);
			assertEquals(correspondenciaPessoaEditada.pais, correspondenciaPessoaEditar.pais);
			assertEquals(correspondenciaPessoaEditada.removido, correspondenciaPessoaEditar.removido);
			assertEquals(correspondenciaPessoaEditada.tipo, correspondenciaPessoaEditar.tipo);
			assertEquals(correspondenciaPessoaEditada.zonaLocalizacao, correspondenciaPessoaEditar.zonaLocalizacao);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarSemEnderecos() throws Throwable {

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.semEnderecos()
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemEnderecoPrincipal() throws Throwable {

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaUrbana()
					.comCaixaPostal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemEnderecoDeCorrespondencia() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaUrbana()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal)
					.build();

			assertValidationExceptionAndMessage("enderecos.obrigatorios");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComEnderecoCorrespondenciaRural() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.zonaUrbana()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.zonaRural()
					.comCaixaPostal()
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("enderecos.correspondencia.urbanoObrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemTipoEndereco() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.semTipo()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.tipo.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemZonaLocalizacaoEndereco() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semZonaLocalizacao()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.zonaLocalizacao.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemLogradouroEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semLogradouro()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.logradouro.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}
		}

		@Test
		public void naoDeveEditarSemNumeroEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semNumero()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.numero.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void deveEditarSemNumeroSeTagSemNumeroTrueEnderecoZonaUrbana() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semNumero()
					.tagSemNumeroTrue()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());
			method.invoke(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarSemBairroComEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semBairro()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.bairro.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemCepComEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semCep()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.cep.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemDescricaoAcessoEnderecoZonaRural() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaRural()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semDescricaoAcesso()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.descricaoAcesso.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemMunicipioEnderecoZonaRural() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaRural()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semMunicipio()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.municipio.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemMunicipioEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semMunicipio()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.municipio.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemPaisEnderecoZonaUrbana() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semPais()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.pais.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}


		@Test
		public void naoDeveEditarSemPaisEnderecoZonaRural() throws Throwable {

			Endereco enderecoPrincipal = new EnderecoBuilder()
					.zonaRural()
					.comTipo(TipoEndereco.ID_PRINCIPAL)
					.semPais()
					.build();

			Endereco enderecoCorrespondencia = new EnderecoBuilder()
					.zonaUrbana()
					.comTipo(TipoEndereco.ID_CORRESPONDENCIA)
					.build();

			Pessoa pessoa = (Pessoa) this.builderClass.newInstance()
					.save();

			Pessoa pessoaEditada = (Pessoa) this.builderClass.newInstance()
					.comEnderecos(false, enderecoPrincipal, enderecoCorrespondencia)
					.build();

			assertValidationExceptionAndMessage("endereco.pais.obrigatorio");

			Method method = pessoa.getClass().getMethod("update", pessoa.getClass());

			try {
				method.invoke(pessoa, pessoaEditada);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				pessoa._delete();
			}

		}

	}

}
