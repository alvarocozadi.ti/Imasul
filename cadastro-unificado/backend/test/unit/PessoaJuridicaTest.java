package unit;

import builders.PessoaJuridicaBuilder;
import models.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.DatabaseCleaner;

import java.util.ArrayList;
import java.util.List;

public class PessoaJuridicaTest {

	public static class Save extends PessoaTest.Save<PessoaJuridicaBuilder> {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveCadastrar() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().build();

			PessoaJuridica pessoaJuridicaSalva = pessoaJuridica.save();
			pessoaJuridicaSalva.refresh();

			assertNotNull(pessoaJuridicaSalva);
			assertNotNull(pessoaJuridicaSalva.id);

			pessoaJuridicaSalva._delete();

		}

		@Test
		public void naoDeveCadastrarComCnpjInvalido() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.comCnpjInvalido()
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.invalido");

			pessoaJuridica.save();

		}

		@Test
		public void naoDeveCadastrarSemCnpj() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.semCnpj()
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.obrigatorio");

			pessoaJuridica.save();

		}

		@Test
		public void naoDeveCadastrarComCnpjExistente() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaJuridica1 = new PessoaJuridicaBuilder()
					.comCnpj(pessoaJuridica.cnpj)
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.unico");

			try {
				pessoaJuridica1.save();
			} finally {
				pessoaJuridica._delete();
			}

		}

		@Test
		public void naoDeveCadastrarSemNome() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.semRazaoSocial()
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.razaoSocial.obrigatoria");

			pessoaJuridica.save();

		}

		@Test
		public void naoDeveCadastrarSemDataConstituicao() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.semDataConstituicao()
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.dataConstituicao.obrigatoria");

			pessoaJuridica.save();

		}

		@Test
		public void naoDeveCadastrarComInscricaoEstadualExistente() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
					.comInscricaoEstadual()
					.save();

			PessoaJuridica pessoaJuridica1 = new PessoaJuridicaBuilder()
					.comInscricaoEstadual()
					.build();

			assertValidationExceptionAndMessage("pessoaJuridica.inscricaoEstadual.unica");

			try {
				pessoaJuridica1.save();
			} finally {
				pessoaJuridica._delete();
			}

		}

		@Test
		public void deveValidarDadosParaCadastro() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().save();

			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cnpj = pessoaJuridica.cnpj;
			dadosValidacao.razaoSocial = pessoaJuridica.razaoSocial;
			dadosValidacao.dataConstituicao = pessoaJuridica.dataConstituicao;
			dadosValidacao.nomeMunicipio = pessoaJuridica.enderecos.iterator().next().municipio.nome;

			pessoaJuridica.validateToCreate(dadosValidacao);

			pessoaJuridica.delete();

		}

		@Test
		public void naoDeveValidarDadosParaCadastroCasoCnpjIncorreto() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().save();
			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cnpj = "43155342000193";
			dadosValidacao.razaoSocial = pessoaJuridica.razaoSocial;
			dadosValidacao.dataConstituicao = pessoaJuridica.dataConstituicao;
			dadosValidacao.nomeMunicipio = pessoaJuridica.enderecos.iterator().next().municipio.nome;

			assertValidationExceptionAndMessage("dadosValidacao.validar.erro");

			try {

				pessoaJuridica.validateToCreate(dadosValidacao);

			} finally {

				BloqueioPessoa bloqueioPessoa = BloqueioPessoa.find("FROM BloqueioPessoa bp JOIN FETCH bp.pessoa p WHERE p.id = :id")
						.setParameter("id", pessoaJuridica.id)
						.first();

				bloqueioPessoa.delete();
				pessoaJuridica.delete();

			}

		}

		@Test
		public void naoDeveValidarDadosCasoBloqueieCnpj() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().save();
			DadosValidacao dadosValidacao = new DadosValidacao();
			dadosValidacao.cnpj = "43155342000193";
			dadosValidacao.razaoSocial = pessoaJuridica.razaoSocial;
			dadosValidacao.dataConstituicao = pessoaJuridica.dataConstituicao;
			dadosValidacao.nomeMunicipio = pessoaJuridica.enderecos.iterator().next().municipio.nome;
			BloqueioPessoa bloqueioPessoa = new BloqueioPessoa(pessoaJuridica);
			bloqueioPessoa.numeroTentativas = BloqueioPessoa.numeroMaxTentativas;
			bloqueioPessoa.pessoa = pessoaJuridica;
			bloqueioPessoa.save();

			assertValidationExceptionAndMessage("dadosValidacao.tentativas.excedido");

			try {

				pessoaJuridica.validateToCreate(dadosValidacao);

			} finally {

				bloqueioPessoa.delete();
				pessoaJuridica.delete();

			}

		}

		@Test
		public void naoDeveValidarDadosParaCadastroCasoPessoaBloqueada() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder().tagBloqueado(true).save();
			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cnpj = pessoaJuridica.cnpj;
			dadosValidacao.razaoSocial = pessoaJuridica.razaoSocial;
			dadosValidacao.dataConstituicao = pessoaJuridica.dataConstituicao;
			dadosValidacao.nomeMunicipio = pessoaJuridica.enderecos.iterator().next().municipio.nome;

			assertValidationExceptionAndMessage("dadosValidacao.tentativas.excedido");

			try {

				pessoaJuridica.validateToCreate(dadosValidacao);

			} finally {

				pessoaJuridica.delete();

			}

		}

	}

	public static class FindByFilter extends BaseUnitTest {

		private static final String cnpj = "84686082000160";
		private static final String razaoSocial = "COMÉRCIO DE PNEUS E TRANSPS MC LTDA";

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			new PessoaJuridicaBuilder()
					.comCnpj(cnpj)
					.comRazaoSocial(razaoSocial)
					.save();

			for(int i = 0; i < 30; i++) {

				new PessoaJuridicaBuilder()
						.comRazaoSocial("Empresa " + i + " SA")
						.save();

			}

		}

		@AfterClass
		public static void afterClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveFiltrarPorRazaoSocial() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = razaoSocial;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(razaoSocial, lista.get(0).razaoSocial);

		}

		@Test
		public void deveFiltrarPorRazaoSocialFiltroAvancado() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.razaoSocial = razaoSocial;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(razaoSocial, lista.get(0).razaoSocial);

		}

		@Test
		public void deveFiltrarPorRazaoSocialParcial() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "PNEUS";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(razaoSocial, lista.get(0).razaoSocial);

		}

		@Test
		public void deveFiltrarPorRazaoSocialIgnorandoAcentos() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "COMERCIO";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(razaoSocial, lista.get(0).razaoSocial);

		}

		@Test
		public void deveFiltrarPorRazaoSocialIgnorandoCaixa() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "pneus";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(razaoSocial, lista.get(0).razaoSocial);

		}

		@Test
		public void deveFiltrarPorCnpj() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = cnpj;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(cnpj, lista.get(0).cnpj);

		}

		@Test
		public void deveFiltrarPorCnpjFiltroAvancado() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.cnpj = cnpj;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(cnpj, lista.get(0).cnpj);

		}

		@Test
		public void naoDeveFiltrarPorCnpjParcial() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = cnpj.substring(0, cnpj.length() - 2);
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertEmpty(lista);

		}

		@Test
		public void deveRetornarListaVaziaSeNenhumResultadoEncontrado() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "Teste";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertEmpty(lista);

		}

		@Test
		public void deveRetornarTodosSeFiltroVazio() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(31, lista.size());

		}

		@Test
		public void deveListarComPaginacao() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = null;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(11, lista.size());

		}

		@Test
		public void deveListarOrdenadoPorRazaoSocialAscendentePorPadrao() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderAsc(lista.get(0).razaoSocial, lista.get(10).razaoSocial, lista.get(19).razaoSocial);

		}

		@Test
		public void deveListarOrdenadoPorRazaoSocialAscendente() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderAsc(lista.get(0).razaoSocial, lista.get(10).razaoSocial, lista.get(19).razaoSocial);

		}

		@Test
		public void deveListarOrdenadoPorRazaoSocialDescendente() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_DESC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderDesc(lista.get(0).razaoSocial, lista.get(10).razaoSocial, lista.get(19).razaoSocial);

		}

		@Test
		public void deveListarOrdeadoPorMunicipioAscendente() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.MUNICIPIO_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(31, lista.size());

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(10).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(30).enderecos);

			assertOrderAsc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

		}

		@Test
		public void deveListarOrdenadoPorMunicipioDescendente() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.MUNICIPIO_DESC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertEquals(31, lista.size());

			assertNotEmpty(lista);

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(11).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(29).enderecos);

			assertOrderDesc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

		}

		@Test
		public void deveFiltrarPorPerfilUsuarioFiltroAvancado() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = new FiltroUsuario();
			filtro.filtroUsuario.perfil = "Administrador-EU";
			filtro.cnpj = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			List<PessoaJuridica> lista = PessoaJuridica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(cnpj, lista.get(0).cnpj);

		}

	}

	public static class CountByFilter extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

			new PessoaJuridicaBuilder()
				.comCnpj("10491224000105")
				.comRazaoSocial("Empresa 0 SA")
				.save();

			for(int i = 1; i < 30; i++) {

				new PessoaJuridicaBuilder()
						.comRazaoSocial("Empresa " + i + " SA")
						.save();

			}

		}

		@AfterClass
		public static void afterClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveContarResultadosFiltradosPorRazaoSocial() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "Empresa 1";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			Long contagem = PessoaJuridica.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(11, contagem.longValue());

		}

		@Test
		public void deveContarResultadosFiltradosPorCnpj() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.filtroSimplificado = "10491224000105";
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			Long contagem = PessoaJuridica.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(1, contagem.longValue());

		}

		@Test
		public void deveContarResultadosSemFiltro() {

			FiltroPessoaJuridica filtro = new FiltroPessoaJuridica();
			filtro.ordenacao = FiltroPessoaJuridica.Ordenacao.RAZAO_SOCIAL_ASC;

			Long contagem = PessoaJuridica.countByFilter(filtro);

			assertNotNull(contagem);

			assertEquals(30, contagem.longValue());

		}

	}

	public static class Remocao extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {
			DatabaseCleaner.cleanAll();
		}

		@Test
		public void deveRemover() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.semFlagRemovido()
				.save();

			pessoaJuridica.remove();

			assertTrue(pessoaJuridica.removido);

			pessoaJuridica._delete();

		}

		@Test
		public void naoDeveRemover() {

			PessoaJuridica pessoaJuridica = new PessoaJuridicaBuilder()
				.comFlagRemovido()
				.save();

			assertValidationExceptionAndMessage("pessoaJuridica.removido.erro");

			pessoaJuridica.remove();

			pessoaJuridica._delete();

		}

	}

	public static class Edicao extends PessoaTest.Edicao<PessoaJuridicaBuilder> {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		private void verificarPessoa(PessoaJuridica pessoa, PessoaJuridica pessoaEditar) {

			assertEquals(pessoaEditar.nomeFantasia, pessoa.nomeFantasia);
			assertEquals(pessoaEditar.razaoSocial, pessoa.razaoSocial);
			assertEquals(pessoaEditar.cnpj, pessoa.cnpj);
			assertEquals(pessoaEditar.inscricaoEstadual, pessoa.inscricaoEstadual);
			assertEquals(pessoaEditar.dataConstituicao, pessoa.dataConstituicao);

		}

		@Test
		public void deveEditar() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.build();

			pessoa.update(pessoaEditar);

			verificarPessoa(pessoaEditar, pessoa);

			pessoa._delete();

		}

		@Test
		public void deveEditarComTodosAtributos() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.comInscricaoEstadual()
				.comNomeFantasia("Empresa xxxx")
				.build();

			pessoa.update(pessoaEditar);

			verificarPessoa(pessoaEditar, pessoa);

			pessoa._delete();

		}

		@Test
		public void deveEditarComNomeFantasia() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.comNomeFantasia("Empresa yyyy")
				.build();

			pessoa.update(pessoaEditar);

			verificarPessoa(pessoaEditar, pessoa);

			pessoa._delete();

		}

		@Test
		public void deveEditarComInscricaoEstadual() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.comInscricaoEstadual()
				.build();

			pessoa.update(pessoaEditar);

			verificarPessoa(pessoaEditar, pessoa);

			pessoa._delete();

		}

		@Test
		public void deveEditarComCnpjAlteradoMantendoCnpjInalterado() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.build();

			pessoa.update(pessoaEditar);

			assertEquals(pessoaEditar.nomeFantasia, pessoa.nomeFantasia);
			assertEquals(pessoaEditar.razaoSocial, pessoa.razaoSocial);
			assertEquals(pessoaEditar.inscricaoEstadual, pessoa.inscricaoEstadual);
			assertEquals(pessoaEditar.dataConstituicao, pessoa.dataConstituicao);
			assertNotEquals(pessoaEditar.cnpj, pessoa.cnpj);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarSemRazaoSocial() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.semRazaoSocial()
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.razaoSocial.obrigatoria");

			try {
				pessoa.update(pessoaEditar);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarSemCnpj() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.semCnpj()
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.obrigatorio");

			try {
				pessoa.update(pessoaEditar);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComCnpjInvalido() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj("1111111")
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.invalido");

			try {
				pessoa.update(pessoaEditar);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarComCnpjRepetido() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaCnpj = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoaCnpj.cnpj)
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.cnpj.unico");

			try {
				pessoa.update(pessoaEditar);
			} finally {

				pessoa._delete();
				pessoaCnpj._delete();

			}

		}

		@Test
		public void naoDeveEditarComInscricaoEstadualRepetida() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaInscricao = new PessoaJuridicaBuilder()
				.comInscricaoEstadual()
				.save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.comInscricaoEstadual(pessoaInscricao.inscricaoEstadual)
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.inscricaoEstadual.unica");

			try {
				pessoa.update(pessoaEditar);
			} finally {

				pessoa._delete();
				pessoaInscricao._delete();

			}

		}

		@Test
		public void naoDeveEditarSemDataConstituicao() {

			PessoaJuridica pessoa = new PessoaJuridicaBuilder().save();

			PessoaJuridica pessoaEditar = new PessoaJuridicaBuilder()
				.comId(pessoa.id)
				.comCnpj(pessoa.cnpj)
				.semDataConstituicao()
				.build();

			assertValidationExceptionAndMessage("pessoaJuridica.dataConstituicao.obrigatoria");

			try {
				pessoa.update(pessoaEditar);
			} finally {
				pessoa._delete();
			}

		}

	}

}
