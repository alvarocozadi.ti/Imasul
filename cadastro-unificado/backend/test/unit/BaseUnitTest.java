package unit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import play.data.validation.Validation;
import play.i18n.Messages;
import play.test.UnitTest;
import utils.AssertionUtils;
import utils.BaseExceptionMatcher;
import utils.ValidationExceptionMatcher;

public abstract class BaseUnitTest extends UnitTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Before
	public void cleanValidationErros() {
		Validation.current().clear();
	}

	protected void assertNotEmpty(Collection collection) {

		assertNotNull(collection);
		assertFalse(collection.isEmpty());

	}

	public void assertEmpty(Collection collection) {

		assertNotNull(collection);
		assertTrue(collection.isEmpty());

	}

	protected void assertFileEquals(File expects, File actuals) throws FileNotFoundException, IOException {
		AssertionUtils.assertFileEquals(expects, actuals);
	}

	protected void assertBaseExceptionAndMessage(String messageKey, Object ... args) {
		this.expectedException.expect(BaseExceptionMatcher.hasUserMessage(Messages.get(messageKey, args)));
	}

	protected void assertValidationExceptionAndMessage(String messageKey, Object ... args) {
		this.expectedException.expect(ValidationExceptionMatcher.hasUserMessage(Messages.get(messageKey, args)));
	}

	public void assertOrderAsc(String inicio, String meio, String fim) {
		assertTrue(inicio.compareTo(meio) <= 0 && meio.compareTo(fim) <= 0);
	}

	public void assertOrderDesc(String inicio, String meio, String fim) {
		assertTrue(inicio.compareTo(meio) >= 0 && meio.compareTo(fim) >= 0);
	}


}