package unit;

import models.*;
import org.apache.commons.lang.time.DateUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import builders.PessoaFisicaBuilder;
import play.Play;
import utils.DatabaseCleaner;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PessoaFisicaTest {

	public static class Save extends PessoaTest.Save<PessoaFisicaBuilder> {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveCadastrarBrasileiro() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNotNull(pessoaFisicaSalva.cpf);
			assertNotNull(pessoaFisicaSalva.naturalidade);
			assertNotNull(pessoaFisicaSalva.nome);
			assertNotNull(pessoaFisicaSalva.nomeMae);
			assertNotNull(pessoaFisicaSalva.contatos);
			assertNotNull(pessoaFisicaSalva.dataNascimento);
			assertNotNull(pessoaFisicaSalva.estadoCivil);
			assertNotNull(pessoaFisicaSalva.estrangeiro);
			assertNotNull(pessoaFisicaSalva.rg);
			assertNotNull(pessoaFisicaSalva.sexo);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void deveCadastrarBrasileiroComTodosAtributos() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comRgCompleto()
					.build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNotNull(pessoaFisicaSalva.nome);
			assertNotNull(pessoaFisicaSalva.dataNascimento);
			assertNotNull(pessoaFisicaSalva.nomeMae);
			assertNotNull(pessoaFisicaSalva.sexo);
			assertNotNull(pessoaFisicaSalva.estadoCivil);
			assertNotNull(pessoaFisicaSalva.estrangeiro);
			assertFalse(pessoaFisicaSalva.estrangeiro);
			assertNotNull(pessoaFisicaSalva.cpf);
			assertNotNull(pessoaFisicaSalva.naturalidade);

			assertNotNull(pessoaFisicaSalva.tituloEleitoral);
			assertNotNull(pessoaFisicaSalva.tituloEleitoral.numero);
			assertNotNull(pessoaFisicaSalva.tituloEleitoral.secao);
			assertNotNull(pessoaFisicaSalva.tituloEleitoral.zona);

			assertNotNull(pessoaFisicaSalva.rg);
			assertNotNull(pessoaFisicaSalva.rg.numero);
			assertNotNull(pessoaFisicaSalva.rg.orgaoExpedidor);
			assertNotNull(pessoaFisicaSalva.rg.dataExpedicao);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void deveCadastrarBrasileiroDescartandoCamposDeEstrangeiro() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comPassaporte("123456789")
					.build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNotNull(pessoaFisicaSalva.id);
			assertNull(pessoaFisicaSalva.passaporte);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void deveCadastrarEstrangeiro() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true).build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNotNull(pessoaFisicaSalva.nome);
			assertNotNull(pessoaFisicaSalva.dataNascimento);
			assertNotNull(pessoaFisicaSalva.nomeMae);
			assertNotNull(pessoaFisicaSalva.sexo);
			assertNotNull(pessoaFisicaSalva.estadoCivil);
			assertNotNull(pessoaFisicaSalva.estrangeiro);
			assertTrue(pessoaFisicaSalva.estrangeiro);
			assertNotNull(pessoaFisicaSalva.passaporte);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void deveCadastrarEstrangeiroDescartandoCamposDeBrasileiro() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.comCpfValido()
					.comNaturalidade()
					.comRgCompleto()
					.build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNull(pessoaFisicaSalva.cpf);
			assertNull(pessoaFisicaSalva.rg);
			assertNull(pessoaFisicaSalva.naturalidade);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void naoDeveCadastrarBrasileiroCpfInvalido() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comCpfInvalido()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.cpf.invalido");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemCpf() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semCpf()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.cpf.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroComCpfExistente() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().save();

			PessoaFisica pessoaFisica1 = new PessoaFisicaBuilder().build();

			pessoaFisica1.cpf = pessoaFisica.cpf;

			assertValidationExceptionAndMessage("pessoaFisica.cpf.unico");

			try {
				pessoaFisica1.save();
			} finally {
				pessoaFisica._delete();
			}

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemNome() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semNome()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nome.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemNome() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semNome()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nome.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemDataNascimento() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semDataDeNascimento()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.dataNascimento.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemDataNascimento() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semDataDeNascimento()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.dataNascimento.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemNomeMae() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semNomeMae()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nomeMae.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemNomeMae() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semNomeMae()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nomeMae.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemNaturalidade() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semNaturalidade()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.naturalidade.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemSexo() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semSexo()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.sexo.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemSexo() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semSexo()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.sexo.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemEstadoCivil() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semEstadoCivil()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.estadoCivil.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemEstadoCivil() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semEstadoCivil()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.estadoCivil.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroSemRg() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semRg()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.rg.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroComRgSemNumero() {

			RegistroGeral rg = new RegistroGeral();

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comRg(rg)
					.build();

			assertValidationExceptionAndMessage("rg.numero.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroSemPassaporte() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.semPassaporte()
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.passaporte.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarEstrangeiroComPassaporteExistente() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder(true)
					.save();

			PessoaFisica pessoaFisica1 = new PessoaFisicaBuilder(true)
					.comPassaporte(pessoaFisica.passaporte)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.passaporte.unico");

			try {
				pessoaFisica1.save();
			} finally {
				pessoaFisica._delete();
			}

		}

		@Test
		public void deveCadastrarBrasileiroComTituloEleitoralComNumeroValido() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comTituloEleitoralComNumeroValido()
					.build();

			PessoaFisica pessoaFisicaSalva = pessoaFisica.save();
			pessoaFisicaSalva.refresh();

			assertNotNull(pessoaFisicaSalva);
			assertNotNull(pessoaFisicaSalva.id);

			pessoaFisicaSalva._delete();

		}

		@Test
		public void naoDeveCadastrarBrasileiroComTituloEleitoralSemNumero() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comTituloEleitoralSemNumero()
					.build();

			assertValidationExceptionAndMessage("tituloEleitoral.numero.obrigatorio");

			pessoaFisica.save();

		}

		@Test
		public void naoDeveCadastrarBrasileiroComTituloEleitoralComNumeroInvalido() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comTituloEleitoralComNumeroInvalido()
					.build();

			assertValidationExceptionAndMessage("tituloEleitoral.numero.invalido");

			pessoaFisica.save();

		}

		@Test
		public void deveValidarDadosParaCadastro() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().save();

			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cpf = pessoaFisica.cpf;
			dadosValidacao.nome = pessoaFisica.nome;
			dadosValidacao.dataNascimento = pessoaFisica.dataNascimento;
			dadosValidacao.nomeMae = pessoaFisica.nomeMae.split(" ")[0];

			pessoaFisica.validateToCreate(dadosValidacao);

			pessoaFisica.delete();

		}

		@Test
		public void naoDeveValidarDadosParaCadastroCasoCpfIncorreto() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().save();
			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cpf = "28724321753";
			dadosValidacao.nome = pessoaFisica.nome;
			dadosValidacao.dataNascimento = pessoaFisica.dataNascimento;
			dadosValidacao.nomeMae = pessoaFisica.nomeMae;

			assertValidationExceptionAndMessage("dadosValidacao.validar.erro");

			try {

				pessoaFisica.validateToCreate(dadosValidacao);

			} finally {

				BloqueioPessoa bloqueioPessoa = BloqueioPessoa.find("FROM BloqueioPessoa bp JOIN FETCH bp.pessoa p WHERE p.id = :id")
						.setParameter("id", pessoaFisica.id)
						.first();

				bloqueioPessoa.delete();
				pessoaFisica.delete();

			}

		}

		@Test
		public void naoDeveValidarDadosCasoBloqueieCpf() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().save();
			DadosValidacao dadosValidacao = new DadosValidacao();
			dadosValidacao.cpf = "88869215350";
			dadosValidacao.nome = pessoaFisica.nome;
			dadosValidacao.dataNascimento = pessoaFisica.dataNascimento;
			dadosValidacao.nomeMae = pessoaFisica.nomeMae;
			BloqueioPessoa bloqueioPessoa = new BloqueioPessoa(pessoaFisica);
			bloqueioPessoa.numeroTentativas = BloqueioPessoa.numeroMaxTentativas;
			bloqueioPessoa.pessoa = pessoaFisica;
			bloqueioPessoa.save();

			assertValidationExceptionAndMessage("dadosValidacao.tentativas.excedido");

			try {

				pessoaFisica.validateToCreate(dadosValidacao);

			} finally {

				bloqueioPessoa.delete();
				pessoaFisica.delete();

			}

		}

		@Test
		public void naoDeveValidarDadosParaCadastroCasoPessoaBloqueada() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder().tagBloqueado(true).save();
			DadosValidacao dadosValidacao = new DadosValidacao();

			dadosValidacao.cpf = pessoaFisica.cpf;
			dadosValidacao.nome = pessoaFisica.nome;
			dadosValidacao.dataNascimento = pessoaFisica.dataNascimento;
			dadosValidacao.nomeMae = pessoaFisica.nomeMae;

			assertValidationExceptionAndMessage("dadosValidacao.tentativas.excedido");

			try {

				pessoaFisica.validateToCreate(dadosValidacao);

			} finally {

				pessoaFisica.delete();

			}

		}

	}

	public static class Listagem extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveFiltrarPorNome() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = "João da Silva";
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals("João da Silva", lista.get(0).nome);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorNomeFiltroAvancado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder().save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.nome = "João da Silva";
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals("João da Silva", lista.get(0).nome);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorNomeCaseInsensitive() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = "JOÃO DA SILVA";
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals("João da Silva", lista.get(0).nome);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorNomeSemAcento() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = "Joao da Silva";
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals("João da Silva", lista.get(0).nome);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorNomeParcial() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = "João";
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals("João da Silva", lista.get(0).nome);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorCpf() {

			PessoaFisica pessoa = new PessoaFisicaBuilder().save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = pessoa.cpf;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(pessoa.cpf, lista.get(0).cpf);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorCpfFiltroAvancado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder().save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.cpf = pessoa.cpf;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(pessoa.cpf, lista.get(0).cpf);

			pessoa._delete();

		}

		@Test
		public void deveFiltrarPorPerfilUsuarioFiltroAvancado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comCpf("06926933600")
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = new FiltroUsuario();
			filtro.filtroUsuario.perfil = "Administrador - EU";
			filtro.cpf = null;
			filtro.tamanhoPagina = 10;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(1, lista.size());
			assertEquals(pessoa.cpf, lista.get(0).cpf);

			pessoa._delete();

		}

		@Test
		public void deveRetornarListaVaziaSeNenhumResultadoEncontrado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.semTituloEleitoral()
					.save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = "Frango";
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertEmpty(lista);

			pessoa._delete();

		}

		@Test
		public void deveRetornarListaVaziaSeNenhumResultadoEncontradoNoFiltroAvancado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder().comCpf("06926933600").save();

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroUsuario = new FiltroUsuario();
			filtro.filtroUsuario.ativo = true;
			filtro.filtroUsuario.perfil = "Usuarinho";
			filtro.nome = "João da Silva";
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertEmpty(lista);

			pessoa._delete();

		}

		@Test
		public void deveRetornarTodosSeFiltroVazio() {

			for(int i = 0; i < 20; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.cpf = null;
			filtro.nome = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(20, lista.size());

			lista = PessoaFisica.findAll();

			for(PessoaFisica pessoa: lista)
				pessoa._delete();

		}

		@Test
		public void deveListarComPaginacao() {

			for(int i = 0; i < 30; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(10, lista.size());

			lista = PessoaFisica.findAll();

			for(PessoaFisica pessoa: lista)
				pessoa._delete();

		}

		@Test
		public void deveListarComOrdemNomeAscendente() {

			for(int i = 0; i < 30; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderAsc(lista.get(0).nome, lista.get(10).nome, lista.get(19).nome);

			lista = PessoaFisica.findAll();

			for(PessoaFisica pessoa: lista)
				pessoa._delete();

		}

		@Test
		public void deveListarComOrdemNomeDescendente() {

			for (int i = 0; i < 30; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.NOME_DESC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina = 1;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertOrderDesc(lista.get(0).nome, lista.get(10).nome, lista.get(19).nome);

			lista = PessoaFisica.findAll();

			for (PessoaFisica pessoa : lista)
				pessoa._delete();

		}

		@Test
		public void deveListarComOrdemMunicipioAscendente() {

			for(int i = 0; i < 30; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.MUNICIPIO_ASC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertNotEmpty(lista);

			assertEquals(10, lista.size());

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(5).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(9).enderecos);

			assertOrderAsc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

			lista = PessoaFisica.findAll();

			for(PessoaFisica pessoa: lista)
				pessoa._delete();

		}

		@Test
		public void deveListarComOrdemMunicipioDescendente() {

			for(int i = 0; i < 30; i++) {

				new PessoaFisicaBuilder()
						.semTituloEleitoral()
						.comNomeRandomico()
						.save();

			}

			FiltroPessoaFisica filtro = new FiltroPessoaFisica();
			filtro.ordenacao = FiltroPessoaFisica.Ordenacao.MUNICIPIO_DESC;
			filtro.filtroSimplificado = null;
			filtro.filtroUsuario = null;
			filtro.tamanhoPagina = 20;
			filtro.numeroPagina  = 2;

			List<PessoaFisica> lista = PessoaFisica.findByFilter(filtro);

			assertEquals(10, lista.size());

			assertNotEmpty(lista);

			List<Endereco> inicio = new ArrayList<>(lista.get(0).enderecos);
			List<Endereco> meio = new ArrayList<>(lista.get(5).enderecos);
			List<Endereco> fim = new ArrayList<>(lista.get(9).enderecos);

			assertOrderDesc(inicio.get(0).municipio.nome, meio.get(0).municipio.nome, fim.get(0).municipio.nome);

			lista = PessoaFisica.findAll();

			for(PessoaFisica pessoa: lista)
				pessoa._delete();

		}

	}

	public static class Remocao extends BaseUnitTest {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		@Test
		public void deveRemover() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.semFlagRemovido()
					.save();

			pessoaFisica.remove();

			assertTrue(pessoaFisica.removido);

			pessoaFisica._delete();

		}

		@Test
		public void naoDeveRemover() {

			PessoaFisica pessoaFisica = new PessoaFisicaBuilder()
					.comFlagRemovido()
					.save();

			assertValidationExceptionAndMessage("pessoaFisica.removido.erro");

			pessoaFisica.remove();

			pessoaFisica._delete();

		}

	}

	public static class Edicao extends PessoaTest.Edicao<PessoaFisicaBuilder> {

		@BeforeClass
		public static void beforeClass() {

			DatabaseCleaner.cleanAll();

		}

		private void verificarPessoaFisica(PessoaFisica pessoa, PessoaFisica pessoaEditada) {

			assertEquals(pessoaEditada.id, pessoa.id);
			assertEquals(pessoaEditada.cpf, pessoa.cpf);
			assertEquals(pessoaEditada.naturalidade, pessoa.naturalidade);
			assertEquals(pessoaEditada.nome, pessoa.nome);
			assertEquals(pessoaEditada.nomeMae, pessoa.nomeMae);
			assertEquals(pessoaEditada.dataNascimento, pessoa.dataNascimento);
			assertEquals(pessoaEditada.estadoCivil, pessoa.estadoCivil);
			assertEquals(pessoaEditada.estrangeiro, pessoa.estrangeiro);
			assertEquals(pessoaEditada.rg, pessoa.rg);
			assertEquals(pessoaEditada.tituloEleitoral, pessoa.tituloEleitoral);
			assertEquals(pessoaEditada.sexo, pessoa.sexo);
			assertEquals(pessoaEditada.passaporte, pessoa.passaporte);

		}

		@Test
		public void deveEditarBrasileiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.comTituloEleitoralCompleto()
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void deveEditarBrasileiroComTodosAtributos() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comRgCompleto()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void deveEditarBrasileiroDescartandoCamposDeEstrangeiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.comPassaporte("15411471")
					.comRgCompleto()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void deveEditarEstrangeiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comId(pessoa.id)
					.comPassaporte(pessoa.passaporte)
					.comNome("Juarez")
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void deveEditarEstrangeiroDescartandoCamposDeBrasileiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.comRgCompleto()
					.comTituloEleitoralCompleto()
					.comCpfValido()
					.comNaturalidade()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comId(pessoa.id)
					.comPassaporte(pessoa.passaporte)
					.comNome("Juarez")
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarBrasileiroCpfInvalido() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comCpfInvalido()
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.cpf.invalido");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemCpf() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semCpf()
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.cpf.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void deveEditarBrasileiroComCpfAlteradoMasDeveManterCpfInalterado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comCpfValido()
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			pessoa.update(pessoaEditada);

			assertEquals(pessoaEditada.id, pessoa.id);
			assertNotEquals(pessoaEditada.cpf, pessoa.cpf);
			assertEquals(pessoaEditada.naturalidade, pessoa.naturalidade);
			assertEquals(pessoaEditada.nome, pessoa.nome);
			assertEquals(pessoaEditada.nomeMae, pessoa.nomeMae);
			assertEquals(pessoaEditada.dataNascimento, pessoa.dataNascimento);
			assertEquals(pessoaEditada.estadoCivil, pessoa.estadoCivil);
			assertEquals(pessoaEditada.estrangeiro, pessoa.estrangeiro);
			assertEquals(pessoaEditada.rg, pessoa.rg);
			assertEquals(pessoaEditada.tituloEleitoral, pessoa.tituloEleitoral);
			assertEquals(pessoaEditada.sexo, pessoa.sexo);

			pessoa._delete();

		}

		@Test
		public void deveEditarBrasileiroComRgAlteradoMasDeveManterRgInalterado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			RegistroGeral rg = new RegistroGeral();
			rg.numero = "1115252";

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comId(pessoa.id)
					.comCpf(pessoa.cpf)
					.comRg(rg)
					.build();

			pessoa.update(pessoaEditada);

			assertEquals(pessoaEditada.id, pessoa.id);
			assertEquals(pessoaEditada.cpf, pessoa.cpf);
			assertEquals(pessoaEditada.naturalidade, pessoa.naturalidade);
			assertEquals(pessoaEditada.nome, pessoa.nome);
			assertEquals(pessoaEditada.nomeMae, pessoa.nomeMae);
			assertEquals(pessoaEditada.dataNascimento, pessoa.dataNascimento);
			assertEquals(pessoaEditada.estadoCivil, pessoa.estadoCivil);
			assertEquals(pessoaEditada.estrangeiro, pessoa.estrangeiro);
			assertNotEquals(pessoaEditada.rg, pessoa.rg);
			assertEquals(pessoaEditada.tituloEleitoral, pessoa.tituloEleitoral);
			assertEquals(pessoaEditada.sexo, pessoa.sexo);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarBrasileiroSemNome() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semNome()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nome.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemNome() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comTituloEleitoralCompleto()
					.semNome()
					.comPassaporte(pessoa.passaporte)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nome.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemDataNascimento() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semDataDeNascimento()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.dataNascimento.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemDataNascimento() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.semDataDeNascimento()
					.comPassaporte(pessoa.passaporte)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.dataNascimento.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemNomeMae() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semNomeMae()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nomeMae.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemNomeMae() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comTituloEleitoralCompleto()
					.semNomeMae()
					.comPassaporte(pessoa.passaporte)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.nomeMae.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemNaturalidade() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semNaturalidade()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.naturalidade.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemSexo() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semSexo()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.sexo.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemSexo() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comTituloEleitoralCompleto()
					.semSexo()
					.comPassaporte(pessoa.passaporte)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.sexo.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemEstadoCivil() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semEstadoCivil()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.estadoCivil.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemEstadoCivil() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comTituloEleitoralCompleto()
					.semEstadoCivil()
					.comPassaporte(pessoa.passaporte)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.estadoCivil.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroSemRg() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.semRg()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.rg.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroComRgSemNumero() {

			RegistroGeral rg = new RegistroGeral();

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralCompleto()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(rg)
					.build();

			assertValidationExceptionAndMessage("rg.numero.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarEstrangeiroSemPassaporte() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comTituloEleitoralCompleto()
					.semPassaporte()
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.passaporte.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void deveEditarEstrangeiroComPassaporteAlteradoMasDeveManterPassaporteInalterado() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comId(pessoa.id)
					.comPassaporte("151881332")
					.build();

			pessoa.update(pessoaEditada);

			assertEquals(pessoaEditada.id, pessoa.id);
			assertNull(pessoa.cpf);
			assertNull(pessoa.naturalidade);
			assertEquals(pessoaEditada.nome, pessoa.nome);
			assertEquals(pessoaEditada.nomeMae, pessoa.nomeMae);
			assertEquals(pessoaEditada.dataNascimento, pessoa.dataNascimento);
			assertEquals(pessoaEditada.estadoCivil, pessoa.estadoCivil);
			assertEquals(pessoaEditada.estrangeiro, pessoa.estrangeiro);
			assertNull(pessoa.rg);
			assertNull(pessoa.tituloEleitoral);
			assertEquals(pessoaEditada.sexo, pessoa.sexo);
			assertNotEquals(pessoaEditada.passaporte, pessoa.passaporte);

			pessoa._delete();

		}

		@Test
		public void deveEditarBrasileiroComTituloEleitoralComNumeroValido() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.comTituloEleitoralComNumeroValido()
					.build();

			pessoa.update(pessoaEditada);

			verificarPessoaFisica(pessoa, pessoaEditada);

			pessoa._delete();

		}

		@Test
		public void naoDeveEditarBrasileiroComTituloEleitoralSemNumero() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralSemNumero()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("tituloEleitoral.numero.obrigatorio");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDeveEditarBrasileiroComTituloEleitoralComNumeroInvalido() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comTituloEleitoralComNumeroInvalido()
					.comCpf(pessoa.cpf)
					.comId(pessoa.id)
					.comRg(pessoa.rg)
					.build();

			assertValidationExceptionAndMessage("tituloEleitoral.numero.invalido");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDevePermitirAlterarBrasileiroParaEstrangeiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder()
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder(true)
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.editar.nacionalidadeTrocaNaoPermitida");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

		@Test
		public void naoDevePermitirAlterarEstrangeiroParaBrasileiro() {

			PessoaFisica pessoa = new PessoaFisicaBuilder(true)
					.save()
					.refresh();

			PessoaFisica pessoaEditada = new PessoaFisicaBuilder()
					.comId(pessoa.id)
					.build();

			assertValidationExceptionAndMessage("pessoaFisica.editar.nacionalidadeTrocaNaoPermitida");

			try {
				pessoa.update(pessoaEditada);
			} finally {
				pessoa._delete();
			}

		}

	}

}
