package services;

import play.libs.WS;

import java.util.List;

public class ExternalModuloService extends BaseExternalService {

	protected static String FIND_ALL_PATH = "modulos/buscaModuloComPerfis";

	public static List findAllModulosWithPerfis() {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS.url(baseUrl + FIND_ALL_PATH);

		wsRequest.headers.put("authorization", getTokenWithType());

		WS.HttpResponse httpResponse = wsRequest.get();

		verifyResponse(httpResponse);

		return gson.fromJson(httpResponse.getJson().getAsJsonArray(), List.class);

	}

}
