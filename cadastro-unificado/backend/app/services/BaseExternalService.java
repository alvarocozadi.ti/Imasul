package services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import exceptions.BaseException;
import exceptions.ValidationException;
import models.oauth.Token;
import play.Play;
import play.cache.Cache;
import play.libs.WS;
import play.mvc.Before;
import play.mvc.Http;
import results.NotFound;
import results.Unauthorized;
import serializers.DateSerializer;

import java.util.Base64;
import java.util.Date;

public class BaseExternalService {

	private static final String GET_ACCESS_TOKEN_PATH = "token";
	protected static Integer HTTP_STATUS_UNPROCESSABLE_ENTITY = 422;
	protected static final String baseUrl = Play.configuration.getProperty("portalSeguranca.url") + "/external/";
	protected static final Gson gson;
	protected static Token token;

	static {

		GsonBuilder builder = new GsonBuilder()
				.serializeNulls()
				.setLenient()
				.registerTypeAdapter(Date.class, new DateSerializer());

		gson = builder.create();

	}

	@Before
	protected static void verifyAccessToken() {

		token = Cache.get("access_token", Token.class);

		if(token == null) {

			token = getAccessToken();

			Cache.set("access_token", token, token.expires_in);

		}

	}

	private static Token getAccessToken() {

		String clientId = Play.configuration.getProperty("oAuth.portalSeguranca.client.id");
		String clientSecret = Play.configuration.getProperty("oAuth.portalSeguranca.client.secret");

		if(clientId == null || clientSecret == null) {

			throw new BaseException().userMessage("oAuth.cadastroClient.naoRealizado");

		}

		String authorizationBasic = "Basic " + new String(Base64.getEncoder().encode((clientId + ":" + clientSecret).getBytes()));
		WS.WSRequest wsRequest = WS.url(baseUrl + GET_ACCESS_TOKEN_PATH);

		wsRequest.headers.put("authorization", authorizationBasic);
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");
		wsRequest.setParameter("grant_type", "client_credentials");

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

		return gson.fromJson(httpResponse.getJson(), Token.class);

	}

	protected static String getTokenWithType() {

		return token.token_type + " " + token.access_token;

	}

	protected static void verifyResponse(WS.HttpResponse httpResponse) {

		if(httpResponse.success()) {

			return;

		}

		if(httpResponse.getStatus().equals(Http.StatusCode.UNAUTHORIZED)) {

			throw new Unauthorized();

		} else if(httpResponse.getStatus().equals(Http.StatusCode.NOT_FOUND)) {

			throw new NotFound();

		} else if(httpResponse.getStatus().equals(HTTP_STATUS_UNPROCESSABLE_ENTITY)) {

			throw new ValidationException().userMessage(httpResponse.getString());

		} else {

			throw new BaseException().userMessage("erro.padrao");

		}

	}

}
