package services;

import models.portalSeguranca.Perfil;
import play.libs.WS;
import play.mvc.Http.Request;
import results.Unauthorized;

public abstract class ExternalAuthorizationService extends BaseExternalService {

	protected static String VALIDATE_TOKEN_PATH = "token/validate";
	protected static String FIND_MODULO_BY_TOKEN_PATH = "modulo/buscarIdPorToken";
	protected static String FIND_PERFIL_BY_CODIGO_E_MODULE = "perfil/buscarPorCodigoEModuloId";

	public static void isAccessTokenValid(Request request) {

		verifyAccessToken();

		if(request.headers.get("authorization") == null) {

			throw new Unauthorized();

		}

		WS.WSRequest wsRequest = WS.url(baseUrl + VALIDATE_TOKEN_PATH);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");

		wsRequest.setParameter("token", request.headers.get("authorization").value());

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

	}

	public static Integer findIdModuloByToken(String token) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS.url(baseUrl + FIND_MODULO_BY_TOKEN_PATH);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");

		wsRequest.setParameter("token", token);

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

		return httpResponse.getJson().getAsInt();

	}

	public static Perfil getPerfilByCodigoAndModule(String codigoPerfil, Integer idModule) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS.url(baseUrl + FIND_PERFIL_BY_CODIGO_E_MODULE);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");
		wsRequest.setParameter("codigoPerfil", codigoPerfil);
		wsRequest.setParameter("idModule", idModule);

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

		Perfil perfis = gson.fromJson(httpResponse.getJson(), Perfil.class);

		return perfis;

	}

}
