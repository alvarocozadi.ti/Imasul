package services;

import models.portalSeguranca.Usuario;
import play.cache.Cache;
import play.libs.Crypto;
import play.libs.WS;
import play.mvc.Http.Request;
import play.mvc.Scope.Session;
import results.Unauthorized;
import secure.services.IAuthenticatedUser;
import secure.services.IAuthenticationService;

public class AuthenticationUserService extends BaseExternalService implements IAuthenticationService {

	protected static String AUTHENTICATE_USER_PATH = "session/validate";
	protected static String BUSCAR_USUARIO_PATH = "usuario/MCUBuscarPorSessionKey";

	@Override
	public IAuthenticatedUser authenticate(Request request, Session session) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS
				.url(baseUrl + AUTHENTICATE_USER_PATH)
				.body(gson.toJson(request));

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/json");

		WS.HttpResponse httpResponse = wsRequest.post();

		if(!httpResponse.success()) {

			Cache.delete("access_token");
			Cache.delete(session.getId());
			session.clear();

			throw new Unauthorized();

		}

		return gson.fromJson(httpResponse.getJson(), Usuario.class);

	}

	@Override
	public IAuthenticatedUser getAuthenticatedUser(Request request, Session session) {

		return (IAuthenticatedUser) Cache.get(session.getId());

	}

	@Override
	public IAuthenticatedUser handShake(Request request, String sessionKeyEntradaUnica) {
		
		verifyAccessToken();
		
		WS.WSRequest wsRequest = WS
				.url(baseUrl + BUSCAR_USUARIO_PATH);
		
		wsRequest.parameters.put("sessionKeyEntradaUnica", sessionKeyEntradaUnica);
		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");
		
		WS.HttpResponse httpResponse = wsRequest.post();
		
		if(!httpResponse.success()) {

			throw new Unauthorized();

		}

		return gson.fromJson(httpResponse.getJson(), Usuario.class);
	}

}

