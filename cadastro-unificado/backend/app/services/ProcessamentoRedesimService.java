package services;

import br.ufla.lemaf.integracao.redesimples.WSE013.CodigoRetornoWSEnum;
import br.ufla.lemaf.integracao.redesimples.WSE013.DadosRedesimVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.EnderecoVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.RetornoRedesimVo;
import br.ufla.lemaf.integracao.redesimples.WSE031.IntegradorEstadualException_Exception;
import exceptions.UpdateDadosRedeSimplesException;
import exceptions.ValidationException;
import models.Cnae;
import models.Empreendimento;
import models.PessoaJuridica;
import play.i18n.Messages;
import redesimples.RedeSimplesWS;
import utils.Config;

public class ProcessamentoRedesimService {

	public static void atualizarDadosPessoasJuridicas(String cnpj) throws UpdateDadosRedeSimplesException, IllegalAccessException {

		RetornoRedesimVo response = null;

		try {

			response = RedeSimplesWS.getDadosCompletosCnpj(cnpj);

		} catch (IntegradorEstadualException_Exception e) {

			e.printStackTrace();
			throw new UpdateDadosRedeSimplesException(Messages.get("redeSimples.integracao.wse031.falha"));

		}

		if (response != null && response.getStatus().equals(CodigoRetornoWSEnum.OK)) {

			processarRegistroRedeSimples(response);

		} else {

			throw new UpdateDadosRedeSimplesException(Messages.get("redeSimples.integracao.wse031.falha"));

		}

	}

	private static void processarRegistroRedeSimples(RetornoRedesimVo retornoRedesim) throws IllegalAccessException {

		DadosRedesimVo dadosRedesim = retornoRedesim
				.getRegistrosRedesim()
				.getRegistroRedesim()
				.get(0)
				.getDadosRedesim();


		PessoaJuridica pessoaJuridicaEmpreendimento = PessoaJuridica.findByCnpj(dadosRedesim.getCnpj());

		validaEndereco(dadosRedesim.getEndereco());

		if (pessoaJuridicaEmpreendimento != null) {

			// PJ já existe e pode ter vindo do SIRIEMA antes da RedeSim, necessita sobrescrever os dados
			pessoaJuridicaEmpreendimento.atualizarPessoaJuridica(dadosRedesim);
			pessoaJuridicaEmpreendimento.save();

			Empreendimento empreendimento = Empreendimento.find("byPessoa", pessoaJuridicaEmpreendimento).first();

			if (empreendimento != null) {

				empreendimento.atualizarEmpreendimento(dadosRedesim);

			} else {

				empreendimento = new Empreendimento(dadosRedesim);
				empreendimento.save();
				Cnae.cnaesRedeSimples(dadosRedesim.getAtividadesEconomica(), empreendimento);

			}

		} else {

			// Por segurança, não permitir criação de pessoa pelo serviço sob-demanda
			throw new IllegalAccessException(Messages.get("redeSimples.integracao.wse031.proibido"));

		}

	}

	private static void validaEndereco(EnderecoVo endereco){

		if (endereco == null || endereco.getCodMunicipio() == null) {
			throw new ValidationException().userMessage(Config.SEM_ENDERECO);
		}

	}

}
