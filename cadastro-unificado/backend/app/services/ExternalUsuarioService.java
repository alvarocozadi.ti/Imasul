package services;

import models.portalSeguranca.AgendaDesativacao;
import models.portalSeguranca.Usuario;
import play.Play;
import play.libs.WS;
import play.mvc.Http;
import serializers.UsuariosSerializer;

public class ExternalUsuarioService extends BaseExternalService {

	private static final String CREATE_PATH = "usuario";
	private static final String UPDATE_PATH = "usuario";
	private static final String ACTIVATE_PATH = "usuario/ativar";
	private static final String DEACTIVATE_PATH = "agendaDesativacao";
	private static final String REMOVE_PATH = "usuario";
	private static final String UPDATE_PASSWORD_PATH = "usuario/alterarSenha";
	private static final String VERIRFY_AUTHENTICATED_USER = "session/verificarPorLogin";

	public static Boolean create(Usuario usuario) {

		verifyAccessToken();

		String usuarioJson = UsuariosSerializer.external.serialize(usuario);

		WS.WSRequest wsRequest = WS
				.url(baseUrl + CREATE_PATH)
				.body(usuarioJson);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/json");

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

		if(httpResponse.getStatus().equals(Http.StatusCode.ACCEPTED)) {

			Http.Response.current().status = Http.StatusCode.ACCEPTED;

			return true;

		}

		return false;

	}

	public static Boolean update(Usuario usuario) {

		verifyAccessToken();

		usuario.vincularPerfisPublicos(true);

//		if (usuario.id != null) {
//
//			Usuario usuarioPerfil = Usuario.findById(usuario.id);
//			usuario.perfis = usuarioPerfil.perfis;
//
//		}

		String usuarioJson = UsuariosSerializer.external.serialize(usuario);

		WS.WSRequest wsRequest = WS
				.url(baseUrl + UPDATE_PATH)
				.body(usuarioJson);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/json");

		WS.HttpResponse httpResponse = wsRequest.put();

		verifyResponse(httpResponse);

		if(httpResponse.getStatus().equals(Http.StatusCode.ACCEPTED)) {

			Http.Response.current().status = Http.StatusCode.ACCEPTED;

			return true;

		}

		return false;

	}

	public static void activate(Integer id) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS
				.url(baseUrl + ACTIVATE_PATH)
				.setParameter("id", id);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");

		WS.HttpResponse httpResponse = wsRequest.put();

		verifyResponse(httpResponse);

	}

	public static void deactivate(AgendaDesativacao agendaDesativacao) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS
				.url(baseUrl + DEACTIVATE_PATH)
				.body(gson.toJson(agendaDesativacao));

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/json");

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

	}

	public static void remove(String login) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS
				.url(baseUrl + REMOVE_PATH + "/" + login);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/json");

		WS.HttpResponse httpResponse = wsRequest.delete();

		verifyResponse(httpResponse);

	}

	public static void updatePassword(Integer id, String senhaAtual, String senhaNova) {

		verifyAccessToken();

		WS.WSRequest wsRequest = WS
				.url(baseUrl + UPDATE_PASSWORD_PATH)
				.setParameter("id", id)
				.setParameter("senhaAtual", senhaAtual)
				.setParameter("senhaNova", senhaNova);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

	}

	public static Boolean verifyAuthenticatedUser(String login, Http.Request request) {

		verifyAccessToken();

		Http.Cookie cookie = request.cookies.get(Play.configuration.getProperty("portalSeguranca.session.cookie"));

		String cookieSessionVal = cookie.value;
		cookieSessionVal = cookieSessionVal.substring(cookieSessionVal.indexOf("-") + 1);
		String[] session = cookieSessionVal.split("&");
		String sessionKey = session[1].substring(cookieSessionVal.indexOf("=") + 1);

		WS.WSRequest wsRequest = WS
				.url(baseUrl + VERIRFY_AUTHENTICATED_USER)
				.setParameter("login", login)
				.setParameter("sessionKey", sessionKey);

		wsRequest.headers.put("authorization", getTokenWithType());
		wsRequest.headers.put("Content-Type", "application/x-www-form-urlencoded");

		WS.HttpResponse httpResponse = wsRequest.post();

		verifyResponse(httpResponse);

		return Boolean.valueOf(httpResponse.getString());

	}

}
