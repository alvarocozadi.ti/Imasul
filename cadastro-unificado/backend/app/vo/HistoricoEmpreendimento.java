package vo;

import models.historico.EmpreendimentoHistorico;

import java.util.Collection;

public class HistoricoEmpreendimento {

    public Collection<EmpreendimentoHistorico> empreendimentos;

    public HistoricoEmpreendimento(Collection<EmpreendimentoHistorico> empreendimentos) {
        this.empreendimentos = empreendimentos;
    }
}
