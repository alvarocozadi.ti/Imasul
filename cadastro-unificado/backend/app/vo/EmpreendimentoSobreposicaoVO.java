package vo;

import com.vividsolutions.jts.geom.Geometry;
import models.Empreendimento;
import models.Pessoa;

import java.util.Collection;

public class EmpreendimentoSobreposicaoVO {

	public String cpfCnpj;
	public String denominacao;
	public Collection<Pessoa> proprietarios;
	public Geometry geometria;

	public EmpreendimentoSobreposicaoVO() {
	}

	public EmpreendimentoSobreposicaoVO(Empreendimento empreendimento) {
		this.cpfCnpj = empreendimento.pessoa.getCpfCnpj();
		this.denominacao = empreendimento.denominacao;
		this.proprietarios = empreendimento.proprietarios;
		this.geometria = empreendimento.localizacao.geometria;
	}
}
