package exceptions;

public class UpdateDadosRedeSimplesException extends Exception {

    public UpdateDadosRedeSimplesException(String msg) {
        super(msg);
    }

}
