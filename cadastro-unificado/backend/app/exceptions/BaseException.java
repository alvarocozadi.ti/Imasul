package exceptions;

import play.i18n.Messages;

public class BaseException extends RuntimeException {

	private String messageKey;
	private Object [] messageArgs;

	public BaseException() {

	}

	public BaseException(Throwable throwable) {
		super(throwable);
	}
	
	public String getUserMessage () {
		return Messages.get(messageKey, messageArgs);
	}
	
	public BaseException userMessage(String messageKey, Object ... messageArgs) {
		
		this.messageArgs = messageArgs;
		this.messageKey = messageKey;
		
		return this;
		
	}
	
}
