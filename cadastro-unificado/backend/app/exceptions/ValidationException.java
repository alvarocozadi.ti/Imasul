package exceptions;

import play.Logger;
import play.data.validation.Error;

import java.util.List;

public class ValidationException extends BaseException {

	public List<Error> errors;

	public ValidationException() {

		super();

	}

	public ValidationException(List<Error> errors) {

		this.errors = errors;

		for(Error error : errors) {

			Logger.error(error.toString());

		}

		super.userMessage("validacao.erro", getErrorsMessage());

	}

	public ValidationException(String motivoErro, Object ... args) {

		super.userMessage(motivoErro, args);

	}

	private String getErrorsMessage() {

		if (this.errors == null || this.errors.isEmpty()) {

			return super.getMessage();

		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < errors.size(); i++) {

			sb.append(errors.get(i).message()).append("\n");

		}

		return sb.toString();

	}

}
