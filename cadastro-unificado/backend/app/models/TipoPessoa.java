package models;

public enum TipoPessoa implements Tipo {

	FISICA(0, "Física"),
	JURIDICA(1, "Jurídica");

	private Integer codigo;
	private String descricao;

	private TipoPessoa(Integer codigo, String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;

	}

	@Override
	public Integer getCodigo() {
		return this.codigo;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}

}
