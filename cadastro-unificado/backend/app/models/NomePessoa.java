package models;

import exceptions.BaseException;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "nome_pessoa", schema = "cadastro_unificado")
public class NomePessoa extends GenericModel {

	@Id
	public Integer id;

	@Column(name = "tipo_pessoa")
	@Enumerated(EnumType.ORDINAL)
	public TipoPessoa tipoPessoa;

	@Enumerated(EnumType.ORDINAL)
	public Sexo sexo;

	public String nome;

	@Transient
	public static final Integer nroNomesRandomicos = 4;

	public NomePessoa() {

		super();

	}

	public static List<String> getNomesFromCpf(String cpf) {

		PessoaFisica pessoaFisica = PessoaFisica.find("cpf", cpf).first();

		if(pessoaFisica == null) {

			throw new BaseException().userMessage("pessoaFisica.buscar.naoEncontrado");

		}

		List<NomePessoa> pessoas;
		Set<String> pessoasUnicas;
		List<String> nomes;

		do {

			pessoas = NomePessoa.find("tipoPessoa = :tipoPessoa ORDER BY random()")
					.setParameter("tipoPessoa", TipoPessoa.FISICA)
					.fetch(nroNomesRandomicos);

			nomes = new ArrayList<>();

			for(NomePessoa pessoa : pessoas) {

				nomes.add(pessoa.nome);

			}

			pessoasUnicas = new HashSet<>(nomes);

		} while(pessoasUnicas.size() < nomes.size() || pessoasUnicas.contains(pessoaFisica.nome));

		nomes.add(pessoaFisica.nome);

		Collections.shuffle(nomes);

		return nomes;

	}

	public static List<String> getNomesFromCnpj(String cnpj) {

		PessoaJuridica pessoaJuridica = PessoaJuridica.find("cnpj", cnpj).first();

		if(pessoaJuridica == null) {

			throw new BaseException().userMessage("pessoaJuridica.buscar.naoEncontrado");

		}

		List<NomePessoa> pessoas;
		Set<String> pessoasUnicas;
		List<String> nomes;

		do {

			pessoas = NomePessoa.find("tipoPessoa = :tipoPessoa ORDER BY random()")
					.setParameter("tipoPessoa", TipoPessoa.JURIDICA)
					.fetch(nroNomesRandomicos);

			nomes = new ArrayList<>();

			for(NomePessoa pessoa : pessoas) {

				nomes.add(pessoa.nome);

			}

			pessoasUnicas = new HashSet<>(nomes);

		} while(pessoasUnicas.size() < nomes.size() || pessoasUnicas.contains(pessoaJuridica.razaoSocial));

		nomes.add(pessoaJuridica.razaoSocial);

		Collections.shuffle(nomes);

		return nomes;

	}

	public static List<String> getNomesMaes(String cpf) {

		PessoaFisica pessoaFisica = PessoaFisica.find("cpf", cpf).first();

		if(pessoaFisica == null) {

			throw new BaseException().userMessage("pessoaFisica.buscar.naoEncontrado");

		}

		List<NomePessoa> maes;
		Set<String> maesUnicas;
		List<String> nomes;
		String nomeMae = pessoaFisica.nomeMae.split(" ")[0];

		do {

			maes =  NomePessoa.find("sexo = :sexo AND tipoPessoa = :tipoPessoa ORDER BY random()")
					.setParameter("sexo", Sexo.FEMININO)
					.setParameter("tipoPessoa", TipoPessoa.FISICA)
					.fetch(nroNomesRandomicos);

			nomes = new ArrayList<>();

			for(NomePessoa mae : maes) {

				nomes.add(mae.nome.split(" ")[0]);

			}

			maesUnicas = new HashSet<>(nomes);

		} while(maesUnicas.size() < nomes.size() || maesUnicas.contains(nomeMae));

		nomes.add(nomeMae);

		Collections.shuffle(nomes);

		return nomes;

	}

}