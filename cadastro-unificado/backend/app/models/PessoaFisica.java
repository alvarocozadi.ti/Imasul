package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.RepresentanteLegalVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.SocioVo;
import common.models.Filter;
import common.models.Message;
import exceptions.ValidationException;
import models.integracaoSiriema.PessoaSiriemaVO;
import models.portalSeguranca.Perfil;
import models.portalSeguranca.Usuario;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import play.Logger;
import play.data.validation.CheckWith;
import play.data.validation.Required;
import play.db.jpa.JPA;
import play.mvc.Http;
import results.Error;
import services.ExternalUsuarioService;
import utils.Config;
import utils.DataUtils;
import utils.StringUtil;
import utils.ValidationUtil;
import validators.*;

import javax.persistence.*;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(schema = "cadastro_unificado", name = "pessoa_fisica")
@PrimaryKeyJoinColumn(name = "id_pessoa", referencedColumnName = "id")
public class PessoaFisica extends Pessoa {

	@Required(message = "pessoaFisica.nome.obrigatorio")
	public String nome;

	@UniqueIfNotRemoved(message = "pessoaFisica.cpf.unico")
	@CheckWith(value = CPFCheck.class, message="pessoaFisica.cpf.invalido")
	@RequiredIf(ifs = @If(option = If.IfOption.FALSE, attribute = "estrangeiro"), message = "pessoaFisica.cpf.obrigatorio")
	public String cpf;

	@Required(message = "pessoaFisica.dataNascimento.obrigatorio")
	@Temporal(TemporalType.DATE)
	@Column(name="data_nascimento")
	public Date dataNascimento;

	@Required(message = "pessoaFisica.nomeMae.obrigatorio")
	@Column(name="nome_mae")
	public String nomeMae;

	public Boolean estrangeiro;

	public String naturalidade;

	@Required(message = "pessoaFisica.sexo.obrigatorio")
	@Enumerated(EnumType.ORDINAL)
	public Sexo sexo;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "estado_civil")
	public EstadoCivil estadoCivil;

	@ValidIf(ifs = @If(option = If.IfOption.FALSE, attribute = "estrangeiro"))
	@Embedded
	public TituloEleitoral tituloEleitoral;

	@ValidIf(ifs = @If(option = If.IfOption.FALSE, attribute = "estrangeiro"))
	@Embedded
	public RegistroGeral rg;

	@UniqueIfNotRemoved(message="pessoaFisica.passaporte.unico")
	@RequiredIf(ifs = @If(option = If.IfOption.TRUE, attribute = "estrangeiro"), message = "pessoaFisica.passaporte.obrigatorio")
	public String passaporte;

	private static final String LOG_PREFIX = "[INTEGRACAO SIRIEMA] ";

	public PessoaFisica() {

		super();

		this.tipo = TipoPessoa.FISICA;
		this.estrangeiro = false;

	}

	public PessoaFisica(RepresentanteLegalVo representanteLegalVo) {

		super();

		// Atributos padrão
		this.tipo = TipoPessoa.FISICA;
		this.estrangeiro = false;

		// Atributos editáveis
		this.nome = representanteLegalVo.getNome();
		this.cpf = representanteLegalVo.getCpf();
		this.sexo = Sexo.sexoRedeSimples(representanteLegalVo.getSexo());
		this.contatos = Contato.contatosRedeSimples(representanteLegalVo.getContato());
		this.enderecos = Endereco.enderecosRedeSimples(representanteLegalVo.getEndereco());
		this.nomeMae = "N/A";
		this.dataNascimento = DataUtils.dataRedeSimples(representanteLegalVo.getDataNascimento());

		if(StringUtils.isNotEmpty(representanteLegalVo.getIdentidade())) {
			this.rg = new RegistroGeral();
			this.rg.numero = representanteLegalVo.getIdentidade();
			this.rg.orgaoExpedidor = representanteLegalVo.getOrgaoEmissor();
		}

	}

	public PessoaFisica(SocioVo socioVo) {

		super(socioVo);

		// Atributos padrão
		this.tipo = TipoPessoa.FISICA;
		this.estrangeiro = false;

		// Atributos editáveis
		this.nome = socioVo.getNome();
		this.cpf = socioVo.getCnpjCpfSocio();
		this.sexo = Sexo.sexoRedeSimples(socioVo.getSexo());
		this.nomeMae = "N/A";
		this.dataNascimento =  DataUtils.dataRedeSimples(socioVo.getDataNascimento());

		gerarLogRedeSimplesPF(socioVo);

		if(StringUtils.isNotEmpty(socioVo.getIdentidade())) {
			this.rg = new RegistroGeral();
			this.rg.numero = socioVo.getIdentidade();
			this.rg.orgaoExpedidor = socioVo.getOrgaoEmissor();
		}

	}

	public static void gerarLogRedeSimplesPF(SocioVo socio) {
		Config.RedeSimplesLogger.trace("== Pesso fisica socio");
		Config.RedeSimplesLogger.trace("=== Nome: "+ StringUtil.isNullObject(socio.getNome()));
		Config.RedeSimplesLogger.trace("=== CPF: "+ StringUtil.isNullObject(socio.getCnpjCpfSocio()));
		Config.RedeSimplesLogger.trace("=== DataNasc: "+ StringUtil.isNullObject(DataUtils.dataRedeSimples(socio.getDataNascimento())));
		Config.RedeSimplesLogger.trace("=== Sexo: "+ StringUtil.isNullObject(Sexo.sexoRedeSimples(socio.getSexo())));


	}

	public Usuario getUsuario() {

		if(this.usuario == null && this.isUsuario) {

			this.usuario = Usuario.find("login = :login AND removido = false").setParameter("login", this.cpf).first();

			if(this.usuario != null
					&& !this.usuario.ativo
					&& this.usuario.perfis != null
					&& FiltroPessoa.ID_MODULO_REQUISITOR != null) {

				this.usuario.perfis = Perfil.findPerfisByModuloRequest(this.usuario, FiltroPessoa.ID_MODULO_REQUISITOR);

			}

		}

		return this.usuario;

	}

	@Override
	public PessoaFisica save() {

		ValidationUtil.validate(this);

		if(this.isUsuario && this.usuario != null) {

			ValidateCpfLogin(this);

		}

		Endereco.validateEnderecos(this.enderecos);

		Contato.validateContatos(this.contatos);

		cleanAttributes();

		return super.save();

	}

	public static List<PessoaFisica> findByFilter(FiltroPessoaFisica filtro) {

		Criteria crit = ((Session) JPA.em().getDelegate()).createCriteria(PessoaFisica.class);
		crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
		crit.setMaxResults(filtro.tamanhoPagina);

		if((filtro.filtroSimplificado != null && filtro.filtroSimplificado.equals("%")) || (filtro.nome != null && filtro.nome.equals("%"))) {

			return new ArrayList<>();

		}

		filter(crit, filtro);

		Filter.addBaseRestrictionsAndOrder(crit, filtro);

		return crit.list();

	}

	public static Long countByFilter(FiltroPessoaFisica filtro) {

		Criteria crit = ((Session) JPA.em().getDelegate()).createCriteria(PessoaFisica.class);

		if((filtro.filtroSimplificado != null && filtro.filtroSimplificado.equals("%"))
				|| (filtro.nome != null && filtro.nome.equals("%"))
				|| (filtro.cpf != null && filtro.cpf.equals("%"))) {

			return 0L;

		}

		filter(crit, filtro);

		return Long.valueOf(crit.list().size());

	}

	private static void filter(Criteria crit, FiltroPessoaFisica filtro) {

		if(filtro.filtroSimplificado != null) {

				simplifiedFilterRestrictions(crit, filtro.filtroSimplificado);

		} else {

			DetachedCriteria critUsuario = null;

			if(filtro.filtroUsuario != null) {

				critUsuario = DetachedCriteria.forClass(Usuario.class).setProjection(org.hibernate.criterion.Property.forName("login"));
				crit.add(org.hibernate.criterion.Property.forName("cpf").in(critUsuario));

			}

			advancedFilterRestrictions(crit, critUsuario, filtro);

		}

	}

	private static void advancedFilterRestrictions(Criteria crit, DetachedCriteria critUsuario, FiltroPessoaFisica filtro) {

		if(filtro.cpf != null) {

			crit.add(Restrictions.eq("cpf", filtro.cpf.replaceAll("[.-]", "")));

		}

		if(filtro.nome != null) {

			String parametro = Normalizer.normalize(filtro.nome, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
			Criterion nome = Restrictions.sqlRestriction("f_remove_caracter_especial(lower({alias}.nome)) ILIKE f_remove_caracter_especial(lower(?))", "%" + parametro + "%", new StringType());
			crit.add(nome);

		}

		Filter.userRestrictions(critUsuario, filtro);

	}

	private static void simplifiedFilterRestrictions(Criteria crit, String filtro) {

		Criterion cpf = Restrictions.eq("cpf", filtro.replaceAll("[.-]", ""));
		String parametro = Normalizer.normalize(filtro, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
		Criterion nome = Restrictions.sqlRestriction("f_remove_caracter_especial(lower({alias}.nome)) ILIKE f_remove_caracter_especial(lower(?))", "%" + parametro + "%", new StringType());
		LogicalExpression orExp = Restrictions.or(cpf, nome);
		crit.add(orExp);

	}

	public PessoaFisica remove() {

		if(this.removido) {

			throw new ValidationException().userMessage("pessoaFisica.removido.erro");

		}

		Empreendimento.validateEmpreendimento(this);
		Empreendimento.unlinkPessoa(this);

		if(this.isUsuario) {

			ExternalUsuarioService.remove(this.cpf);

		}

		this.removido = true;

		return super.save();

	}

	public PessoaFisica update(PessoaFisica pessoaFisica) {

		ValidationUtil.validate(pessoaFisica);

		if(pessoaFisica.isUsuario && pessoaFisica.usuario != null)
			ValidateCpfLogin(pessoaFisica);

		if(this.estrangeiro != pessoaFisica.estrangeiro)
			throw new ValidationException().userMessage("pessoaFisica.editar.nacionalidadeTrocaNaoPermitida");

		Endereco.validateEnderecos(pessoaFisica.enderecos);
		Endereco.setEnderecos(this.enderecos, pessoaFisica.enderecos);

		Contato.validateContatos(pessoaFisica.contatos);
		Contato.setContatos(this.contatos, pessoaFisica.contatos);

		this.dataAtualizacao = new Date();
		this.dataNascimento = pessoaFisica.dataNascimento;
		this.estrangeiro = pessoaFisica.estrangeiro;
		this.estadoCivil = pessoaFisica.estadoCivil;
		this.nome = pessoaFisica.nome;
		this.nomeMae = pessoaFisica.nomeMae;
		this.sexo = pessoaFisica.sexo;
		this.naturalidade = pessoaFisica.naturalidade;
		this.tituloEleitoral = pessoaFisica.tituloEleitoral;
		this.isUsuario = pessoaFisica.isUsuario;

		if(!this.estrangeiro)
			this.rg = pessoaFisica.rg;

		cleanAttributes();

		return super.save();

	}

	private void cleanAttributes() {

		if(this.estrangeiro) {

			this.cpf = null;
			this.rg = null;
			this.tituloEleitoral = null;
			this.naturalidade = null;

		} else {

			this.passaporte = null;

		}

	}

	public void validateToCreate(DadosValidacao dadosValidacao) {

		if(this.bloqueado) {

			throw new ValidationException().userMessage("dadosValidacao.tentativas.excedido");

		}

		if(!this.nomeMae.split(" ")[0].equals(dadosValidacao.nomeMae)
			|| !this.nome.equals(dadosValidacao.nome)
			|| !this.cpf.equals(dadosValidacao.cpf)
			|| !DateUtils.isSameDay(this.dataNascimento, dadosValidacao.dataNascimento)) {

			BloqueioPessoa bloqueioPessoa = BloqueioPessoa.addNumeroTentativas(this);

			commitTransaction();

			if(bloqueioPessoa.numeroTentativas.equals(BloqueioPessoa.numeroMaxTentativas + 1)) {

				this.bloqueado = true;

				super.save();

				bloqueioPessoa.dataCadastro = new Date();

				bloqueioPessoa.save();

				commitTransaction();

				throw new Error(Http.StatusCode.FORBIDDEN, new Message("dadosValidacao.tentativas.excedido").getText());

			}

			throw new ValidationException().userMessage("dadosValidacao.validar.erro");

		}

	}

	public void ValidateCpfLogin(PessoaFisica pessoaFisica) {

		if(!pessoaFisica.cpf.equals(pessoaFisica.usuario.login)) {

			throw new ValidationException().userMessage("pessoaFisica.cpf.login");

		}

	}

	public static PessoaFisica findByCpf(String cpf) {

		return PessoaFisica.find("cpf = :cpf AND removido = :removido")
				.setParameter("cpf", cpf.replaceAll("[.-]", ""))
				.setParameter("removido", false)
				.first();

	}

	public static PessoaFisica findByPassaporte(String passaporte) {

		return PessoaFisica.find("passaporte = :passaporte").setParameter("passaporte", passaporte.replaceAll("[.-]", "")).first();

	}

	public static PessoaFisica findByPessoa(Pessoa pessoa) {

		return PessoaFisica.find("pessoa = :pessoa").setParameter("pessoa", pessoa).first();

	}

	public boolean createPessoaFisica(boolean isIntegracao, PessoaSiriemaVO pessoaSiriemaVO) {

		try {
			Logger.info(LOG_PREFIX + " - Processando Pessoa Física com CPF - " + pessoaSiriemaVO.cpf);

			this.tipo = TipoPessoa.FISICA;
			this.contatos = Pessoa.formataContatoSiriema(pessoaSiriemaVO.contato);
			this.enderecos = Pessoa.formataEnderecoSiriema(pessoaSiriemaVO.endereco);
			this.dataAtualizacao = DataUtils.formataData(pessoaSiriemaVO.dataUltimaAlteracao);
			this.cpf = pessoaSiriemaVO.cpf.replaceAll("[.\\-\\/]", "");
			this.nome = StringUtil.stringVazia(pessoaSiriemaVO.nome) ? "Sem nome informado" : pessoaSiriemaVO.nome;
			this.nomeMae = StringUtil.stringVazia(pessoaSiriemaVO.nomeMae) ? "Sem nome informado" : pessoaSiriemaVO.nomeMae;
			this.sexo = pessoaSiriemaVO.sexo.equalsIgnoreCase(Sexo.MASCULINO.getDescricao()) ? Sexo.MASCULINO : Sexo.FEMININO;
			this.dataNascimento = DataUtils.formataData(pessoaSiriemaVO.dataNascimento);
			this.estadoCivil = EstadoCivil.getEstadoCivil(pessoaSiriemaVO.estadoCivil);
			this.naturalidade = pessoaSiriemaVO.naturalidade;

//			if (!isIntegracao)
				this.isUsuario = true;

			return true;

		}catch (Exception e){
			Logger.info(LOG_PREFIX + " - [FALHA AO SALVAR PESSOA FÍSICA] - Erro ao cadastrar a pessoa de CPF " + pessoaSiriemaVO.cpf + " - Problema com dados cadastrais.");
			e.printStackTrace();
			return false;
		}
	}

}