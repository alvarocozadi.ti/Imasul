package models;

import br.ufla.lemaf.integracao.redesimples.Utils.SiglaPaisConverter;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado", name = "pais")
public class Pais extends GenericModel {

	@Id
	public Integer id;

	public String nome;

	public String sigla;

	@OrderBy(value = "nome")
	@OneToMany(mappedBy = "pais")
	public Set<Estado> estados;

	public Pais() {
		super();
	}

	@Override
	public Object _key() {
		return this.id;
	}

	public static Pais findBySigla(String sigla) {

		Pais pais = Pais.find("sigla = :sigla")
				.setParameter("sigla", sigla)
				.first();

		return pais;
	}

	public static Pais paisRedeSimples(String codigoPais) {

		String sigla = new SiglaPaisConverter().toSigla(codigoPais);

		if(sigla == null) {
			sigla = "BR";
		}

		Pais pais = Pais.findBySigla(sigla);

		return pais;
	}

}

