package models;

import org.apache.commons.lang.StringUtils;

public enum Sexo implements Tipo {

	MASCULINO(0, "Masculino"),
	FEMININO(1, "Feminino");

	private Integer codigo;
	private String descricao;

	private Sexo(Integer codigo, String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;

	}

	@Override
	public Integer getCodigo() {
		return this.codigo;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}

	public static Sexo sexoRedeSimples(String codigoSexo) {

		if(StringUtils.isBlank(codigoSexo))
			return Sexo.MASCULINO;

		if(codigoSexo.equals("M")) {
			return Sexo.MASCULINO;
		}
		else if(codigoSexo.equals("F")) {
			return Sexo.FEMININO;
		}

		return null;
	}
}
