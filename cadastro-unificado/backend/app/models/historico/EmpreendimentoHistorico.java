package models.historico;

import models.ClassificacaoEmpreendimento;
import models.Empreendimento;
import models.Pessoa;
import models.PessoaFisica;
import play.data.validation.CheckWith;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.data.validation.Valid;
import play.db.jpa.GenericModel;
import validators.EstrangeiroCheck;
import validators.If;
import validators.RequiredIf;
import validators.ValidIf;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(schema = "cadastro_unificado", name = "empreendimento_historico")
@org.hibernate.annotations.Filter(name = "entityRemovida")
public class EmpreendimentoHistorico extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.empreendimento_historico_id_seq")
	@SequenceGenerator(name = "cadastro_unificado.empreendimento_historico_id_seq", sequenceName = "cadastro_unificado.empreendimento_historico_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "empreendimento.denominacao.obrigatorio")
	public String denominacao;

	@Unique(message = "empreendimento.pessoa.unico")
	@RequiredIf(ifs = @If(option = If.IfOption.FALSE, attribute = "removido"), message = "empreendimento.pessoa.obrigatorio")
	@ValidIf(ifs = @If(option = If.IfOption.NULL, attribute = "id"))
	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.estrangeira")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_pessoa", referencedColumnName = "id")
	public Pessoa pessoa;

	@Unique(message = "empreendimento.pessoa.unico")
	@RequiredIf(ifs = @If(option = If.IfOption.FALSE, attribute = "removido"), message = "empreendimento.pessoa.obrigatorio")
	@ValidIf(ifs = @If(option = If.IfOption.NULL, attribute = "id"))
	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.estrangeira")
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_pessoa_modificadora", referencedColumnName = "id")
	public Pessoa pessoaModificadora;

	@Valid
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "contato_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_contato_historico", referencedColumnName = "id"))
	public Set<ContatoHistorico> contatos;

	@Valid
	@Required(message = "enderecos.obrigatorios")
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "endereco_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_endereco_historico", referencedColumnName = "id"))
	public Set<EnderecoHistorico> enderecos;

	@Required(message = "empreendimento.localizacao.obrigatorio")
	@Valid
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_localizacao_historico", referencedColumnName = "id", nullable = false)
	public LocalizacaoHistorico localizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro")
	public Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_atualizacao")
	public Date dataAtualizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_historico")
	public Date dataHistorico;

	public Boolean removido;

	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "proprietario_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_proprietario", referencedColumnName = "id"))
	public Set<Pessoa> proprietarios;

	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "responsavel_tecnico_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_responsavel_tecnico", referencedColumnName = "id"))
	public Set<Pessoa> responsaveisTecnicos;

	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "representante_legal_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_representante_legal", referencedColumnName = "id_pessoa"))
	public Set<PessoaFisica> representantesLegais;

	@CheckWith(value = EstrangeiroCheck.class, message="empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "responsavel_legal_empreendimento_historico", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name="id_empreendimento_historico", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_responsavel_legal", referencedColumnName = "id_pessoa"))
	public Set<PessoaFisica> responsaveisLegais;

	@OneToMany(mappedBy = "empreendimento", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<CnaeHistorico> cnaes;

	@Column(name = "porte")
	public String porte;

	@ManyToOne
	@JoinColumn(name = "id_classificacao_empreendimento", referencedColumnName = "id")
	public ClassificacaoEmpreendimento classificacaoEmpreendimento;

	public EmpreendimentoHistorico() {
		super();
	}

	public EmpreendimentoHistorico(Empreendimento empreendimento) {
		this.id = null;
		this.denominacao = empreendimento.denominacao;
		this.pessoa = empreendimento.pessoa;
		this.contatos = empreendimento.contatos.stream().map(ContatoHistorico::new).collect(Collectors.toSet());
		this.enderecos = empreendimento.enderecos.stream().map(EnderecoHistorico::new).collect(Collectors.toSet());
		this.localizacao = new LocalizacaoHistorico(empreendimento.localizacao);
		this.dataCadastro = empreendimento.dataCadastro;
		this.dataAtualizacao = empreendimento.dataAtualizacao;
		this.removido = empreendimento.removido;
		this.proprietarios = empreendimento.proprietarios != null ? new HashSet<>(empreendimento.proprietarios) : new HashSet<>();
		this.responsaveisTecnicos = empreendimento.responsaveisTecnicos != null ? new HashSet<>(empreendimento.responsaveisTecnicos) : new HashSet<>();
		this.representantesLegais = empreendimento.representantesLegais != null ? new HashSet<>(empreendimento.representantesLegais) : new HashSet<>();
		this.responsaveisLegais = empreendimento.responsaveisLegais != null ? new HashSet<>(empreendimento.responsaveisLegais) : new HashSet<>();
		this.cnaes = empreendimento.cnaes != null ? empreendimento.cnaes.stream().map(c -> new CnaeHistorico(c,this)).collect(Collectors.toList()) : null;
		this.porte = empreendimento.porte;
		this.classificacaoEmpreendimento = empreendimento.classificacaoEmpreendimento;
	}

	public static List<EmpreendimentoHistorico> findByCpfCnpj(String cpfCnpj){
		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);
		return find("byPessoa", pessoa).fetch();
	}
}
