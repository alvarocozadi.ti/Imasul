package models.historico;

import models.*;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import validators.If;
import validators.Reference;
import validators.RequiredIf;

import javax.persistence.*;

@Entity
@Table(schema = "cadastro_unificado", name = "endereco_historico")
public class EnderecoHistorico extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.endereco_historico_id_seq")
	@SequenceGenerator(name = "cadastro_unificado.endereco_historico_id_seq", sequenceName = "cadastro_unificado.endereco_historico_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "endereco.tipo.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_tipo_endereco", referencedColumnName = "id")
	public TipoEndereco tipo;

	@Required(message = "endereco.zonaLocalizacao.obrigatorio")
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "zona_localizacao")
	public ZonaLocalizacao zonaLocalizacao;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.logradouro.obrigatorio")
	public String logradouro;

	@RequiredIf(ifs = {@If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"),
		@If(option = If.IfOption.FALSE, attribute = "semNumero")},
		message = "endereco.numero.obrigatorio")
	public Integer numero;

	@Column(name = "caixa_postal")
	public String caixaPostal;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.bairro.obrigatorio")
	public String bairro;

	public String complemento;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.cep.obrigatorio")
	public String cep;

	@Column(name = "descricao_acesso")
	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "RURAL"), message = "endereco.descricaoAcesso.obrigatorio")
	public String descricaoAcesso;

	@Required(message = "endereco.municipio.obrigatorio")
	@Reference(modelClass = Municipio.class, message = "endereco.municipio.inexistente")
	@ManyToOne
	@JoinColumn(name = "id_municipio", referencedColumnName = "id")
	public Municipio municipio;

	@Required(message = "endereco.pais.obrigatorio")
	@Reference(modelClass = Pais.class, message = "endereco.pais.inexistente")
	@ManyToOne
	@JoinColumn(name = "id_pais", referencedColumnName = "id")
	public Pais pais;

	public Boolean removido;

	@Transient
	public Boolean semNumero;

	public EnderecoHistorico() {
		super();
	}

	public EnderecoHistorico(Endereco endereco) {
		this.id = null;
		this.tipo = endereco.tipo;
		this.zonaLocalizacao = endereco.zonaLocalizacao;
		this.logradouro = endereco.logradouro;
		this.numero = endereco.numero;
		this.caixaPostal = endereco.caixaPostal;
		this.bairro = endereco.bairro;
		this.complemento = endereco.complemento;
		this.cep = endereco.cep;
		this.descricaoAcesso = endereco.descricaoAcesso;
		this.municipio = endereco.municipio;
		this.pais = endereco.pais;
		this.removido = endereco.removido;
		this.semNumero = endereco.semNumero;
	}

}