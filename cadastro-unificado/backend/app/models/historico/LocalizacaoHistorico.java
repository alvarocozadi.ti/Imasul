package models.historico;

import com.vividsolutions.jts.geom.Geometry;
import models.Localizacao;
import org.hibernate.annotations.Type;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema = "cadastro_unificado", name = "localizacao_historico")
public class LocalizacaoHistorico extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.localizacao_historico_id_seq")
	@SequenceGenerator(name = "cadastro_unificado.localizacao_historico_id_seq", sequenceName = "cadastro_unificado.localizacao_historico_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "localizacao.geometria.obrigatorio")
	@Type(type="org.hibernate.spatial.GeometryType")
	public Geometry geometria;

	public LocalizacaoHistorico() {
		super();
	}

	public LocalizacaoHistorico(Localizacao localizacao) {
		this.id = null;
		this.geometria = localizacao.geometria;
	}

	@Override
	public Object _key() {
		return this.id;
	}

}
