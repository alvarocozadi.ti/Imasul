package models.historico;

import models.Cnae;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema = "cadastro_unificado", name = "cnae_historico")
public class CnaeHistorico extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.cnae_historico_id_seq")
	@SequenceGenerator(name = "cadastro_unificado.cnae_historico_id_seq", sequenceName = "cadastro_unificado.cnae_historico_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "cnae.codigo.obrigatorio")
	@Column(name = "codigo")
	public String codigo;

	@Required(message = "cnae.empreendimento.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_empreendimento_historico", referencedColumnName="id")
	public EmpreendimentoHistorico empreendimento;

	public CnaeHistorico() {
		super();
	}

	public CnaeHistorico(Cnae cnae, EmpreendimentoHistorico empreendimento) {
		this.id = null;
		this.codigo = cnae.codigo;
		this.empreendimento = empreendimento;
	}

}
