package models.historico;

import models.Contato;
import models.TipoContato;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;

@Entity
@Table(schema = "cadastro_unificado" , name = "contato_historico")
public class ContatoHistorico extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.contato_historico_id_seq")
	@SequenceGenerator(name = "cadastro_unificado.contato_historico_id_seq", sequenceName = "cadastro_unificado.contato_historico_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "contato.tipo.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_tipo_contato" , referencedColumnName = "id")
	public TipoContato tipo;

	@Required(message = "contato.valor.obrigatorio")
	public String valor;

	public Boolean principal;

	public Boolean removido;

	public ContatoHistorico() {
		super();
	}

	public ContatoHistorico(Contato contato) {
		this.id = null;
		this.tipo = contato.tipo;
		this.valor = contato.valor;
		this.principal = contato.principal;
		this.removido = contato.removido;
	}

}
