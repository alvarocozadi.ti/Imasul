package models;

import common.models.Filter;

import java.util.List;

public class FiltroEmpreendimento extends Filter {

	public enum Ordenacao implements Order {

		DENOMINACAO_ASC(OrderDirection.ASC, "denominacao"),
		DENOMINACAO_DESC(OrderDirection.DESC, "denominacao"),
		MUNICIPIO_ASC(OrderDirection.ASC, "mun.nome"),
		MUNICIPIO_DESC(OrderDirection.DESC, "mun.nome");

		private OrderDirection orderDirection;
		private String field;

		private Ordenacao(OrderDirection orderDirection, String field) {

			this.orderDirection = orderDirection;
			this.field = field;

		}

		@Override
		public OrderDirection getOrderDirection() {
			return this.orderDirection;
		}

		@Override
		public String getField() {
			return this.field;
		}

	}

	public enum TipoPessoaVinculada {

		PROPRIETARIO("PROPRIETARIO"),
		REPRESENTANTE_LEGAL("REPRESENTANTE_LEGAL"),
		RESPONSAVEL_LEGAL("RESPONSAVEL_LEGAL"),
		RESPONSAVEL_TECNICO("RESPONSAVEL_TECNICO");

		public String tipo;

		private TipoPessoaVinculada(String tipo) {

			this.tipo = tipo;

		}

	}

	public String busca;

	public int numeroPagina;

	public int tamanhoPagina;

	public Ordenacao ordenacao;

	public TipoPessoaVinculada tipoPessoaVinculada;

	public String nomePessoaVinculada;

	public String cpfCnpjPessoaVinculada;

	public String denominacao;

	public String cpfCnpj;

	public String cpfCnpjEmpreendedor;

	public Integer codigoIbge;

	public List<String> cpfsCnpjs;

	public Boolean filtrarCadastro;

}
