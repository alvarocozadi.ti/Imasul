package models;

import validators.If;
import validators.RequiredIf;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
public class RegistroGeral {

	@RequiredIf(ifs = @If(option = If.IfOption.NOT_NULL, attribute = "orgaoExpedidor"), message = "rg.numero.obrigatorio")
	@Column(name = "numero_rg")
	public String numero;

	@Column(name = "orgao_expedidor_rg")
	public String orgaoExpedidor;

	@Column(name = "data_expedicao_rg")
	public Date dataExpedicao;

	public RegistroGeral() {

		super();

	}

}
