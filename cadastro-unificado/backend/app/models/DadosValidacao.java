package models;

import java.util.Date;

public class DadosValidacao {

	public String cpf;
	public String cnpj;
	public String nome;
	public String razaoSocial;
	public String nomeMae;
	public String nomeMunicipio;
	public Date dataNascimento;
	public Date dataConstituicao;

	public DadosValidacao() {

		super();

	}

}