package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.ContatoVo;
import exceptions.ValidationException;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import utils.Config;
import utils.StringUtil;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado" , name = "contato")
public class Contato extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_contato")
	@SequenceGenerator(name = "sq_contato", sequenceName = "cadastro_unificado.sq_contato", allocationSize = 1)
	public Integer id;

	@Required(message = "contato.tipo.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_tipo_contato" , referencedColumnName = "id")
	public TipoContato tipo;

	@Required(message = "contato.valor.obrigatorio")
	public String valor;

	public Boolean principal;

	public Boolean removido;

	public Contato() {

		super();

		this.removido = false;

		this.principal = false;

	}

	@Override
	public Object _key() {
		return this.id;
	}

	public Contato setAttributes(Contato contato) {

		this.tipo = contato.tipo;
		this.valor = contato.valor;
		this.principal = contato.principal;

		return this;

	}

	protected static void setContatos(Set<Contato> objContatos, Set<Contato> contatosEditar) {

		if(contatosEditar == null) {

			if(objContatos != null) {

				objContatos.clear();

			}

			return;

		}

		if(objContatos == null) {

			objContatos = new HashSet<>();

		}

		Iterator<Contato> contatoIterator = contatosEditar.iterator();

		if(objContatos.size() <= contatosEditar.size()) {

			for(Contato contato : objContatos) {

				contato.setAttributes(contatoIterator.next());

			}

			while(contatoIterator.hasNext()) {

				objContatos.add(contatoIterator.next());

			}

		} else {

			Set<Contato> setAux = new HashSet<>();
			setAux.addAll(objContatos);

			for(Contato contato : objContatos) {

				if(contatoIterator.hasNext()) {

					contato.setAttributes(contatoIterator.next());
					setAux.remove(contato);

				} else {

					objContatos.removeAll(setAux);

					break;

				}

			}

		}

	}

	public static void validateContatos(Set<Contato> contatos) {

		if(contatos == null) {

			return;

		}

		Set<Contato> emails = getEmails(contatos);

		if(emails == null || emails.isEmpty()) {

			return;

		} else {

			if(!hasEmailPrincipal(emails)) {

				throw new ValidationException().userMessage("contatos.tipo.emailPrincipalObrigatorio");

			}

			Integer cont = 0;

			for(Contato contato : contatos) {

				if(contato.principal) {

					cont++;

				}

			}

			if(cont > 1) {

				throw new ValidationException().userMessage("contatos.tipo.ApenasUmEmailPrincipal");

			}

		}

	}

	public static Set<Contato> getEmails(Set<Contato> contatos) {

		Set<Contato> emails = new HashSet<>();

		for(Contato contato : contatos) {

			if(contato.tipo.id == TipoContato.ID_EMAIL) {

				emails.add(contato);

			}

		}

		return emails;

	}

	public static Boolean hasEmailPrincipal(Set<Contato> contatos) {

		for(Contato contato : contatos) {

			if((contato.tipo.id == TipoContato.ID_EMAIL) && contato.principal) {

				return true;

			}

		}

		return false;

	}

	public static Set<Contato> contatosRedeSimples(ContatoVo contatoVo) {
		Config.RedeSimplesLogger.trace(" >>>> Contato Inicio");
		if(contatoVo == null) {
			return new HashSet<>();
		}

		Set<Contato> listaContatos = new HashSet<>();

		TipoContato tipoContatoComercial = TipoContato.findById(TipoContato.ID_TELEFONE_COMERCIAL);
		TipoContato tipoContatoEmail = TipoContato.findById(TipoContato.ID_EMAIL);

		if(contatoVo.getDddTelefone1() != null && contatoVo.getTelefone1() != null) {
			Contato contato = new Contato();
			contato.valor = contatoVo.getDddTelefone1() + contatoVo.getTelefone1();
			contato.tipo = tipoContatoComercial;
			logRedeSimples(contato);
			contato.save();
			listaContatos.add(contato);
		}
		if(contatoVo.getDddTelefone2() != null && contatoVo.getTelefone2() != null) {
			Contato contato = new Contato();
			contato.valor = contatoVo.getDddTelefone2() + contatoVo.getTelefone2();
			contato.tipo = tipoContatoComercial;
			logRedeSimples(contato);
			contato.save();
			listaContatos.add(contato);
		}
		if(contatoVo.getDddFax() != null && contatoVo.getFax() != null) {
			Contato contato = new Contato();
			contato.valor = contatoVo.getDddFax() + contatoVo.getFax();
			contato.tipo = tipoContatoComercial;
			logRedeSimples(contato);
			contato.save();
			listaContatos.add(contato);
		}
		if(contatoVo.getCorreioEletronico() != null) {
			Contato contato = new Contato();
			contato.valor = contatoVo.getCorreioEletronico();
			contato.tipo = tipoContatoEmail;
			contato.principal = true;
			logRedeSimples(contato);
			contato.save();
			listaContatos.add(contato);
		}

		Config.RedeSimplesLogger.trace(" >>>> Contato fim ");
		return listaContatos;
	}

	private static void logRedeSimples(Contato contato) {

		Config.RedeSimplesLogger.trace(" ==== Contato " + StringUtil.isNullObject(contato.tipo.descricao));
		Config.RedeSimplesLogger.trace(" ==== == Valor " + StringUtil.isNullObject(contato.valor));

	}
}
