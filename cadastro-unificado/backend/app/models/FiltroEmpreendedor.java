package models;

import common.models.Filter;

import java.util.ArrayList;
import java.util.List;

public class FiltroEmpreendedor extends Filter{

    public enum Ordenacao implements Filter.Order {

        NOME_ASC(Filter.OrderDirection.ASC, "id"),
        NOME_DESC(Filter.OrderDirection.DESC, "id");

        private Filter.OrderDirection orderDirection;
        private String field;

        private Ordenacao(Filter.OrderDirection orderDirection, String field) {

            this.orderDirection = orderDirection;
            this.field = field;

        }

        @Override
        public Filter.OrderDirection getOrderDirection() {
            return this.orderDirection;
        }

        @Override
        public String getField() {
            return this.field;
        }

    }

    public enum TipoPessoaVinculada {

        PROPRIETARIO("PROPRIETARIO"),
        REPRESENTANTE_LEGAL("REPRESENTANTE_LEGAL"),
        RESPONSAVEL_LEGAL("RESPONSAVEL_LEGAL"),
        RESPONSAVEL_TECNICO("RESPONSAVEL_TECNICO");

        public String tipo;

        private TipoPessoaVinculada(String tipo) {

            this.tipo = tipo;

        }

    }

    public String busca;

    public Boolean contagem;

    public int numeroPagina;

    public int tamanhoPagina;

    public FiltroEmpreendedor.Ordenacao ordenacao;

    public FiltroEmpreendedor.TipoPessoaVinculada tipoPessoaVinculada;

    public String nomePessoaVinculada;

    public String cpfCnpjPessoaVinculada;

    public String cpfCnpj;

    public Integer codigoIbge;

    public List<String> cpfsCnpjs;

    public Boolean filtrarCadastro;

    public FiltroEmpreendedor clone() {

        FiltroEmpreendedor novo = new FiltroEmpreendedor();

        novo.busca = this.busca;
        novo.contagem = this.contagem;
        novo.numeroPagina = this.numeroPagina;
        novo.tamanhoPagina = this.tamanhoPagina;
        novo.ordenacao = this.ordenacao;
        novo.tipoPessoaVinculada = this.tipoPessoaVinculada;
        novo.nomePessoaVinculada = this.nomePessoaVinculada;
        novo.cpfCnpjPessoaVinculada = this.cpfCnpjPessoaVinculada;
        novo.cpfCnpj = this.cpfCnpj;
        novo.codigoIbge = this.codigoIbge;
        novo.cpfsCnpjs = new ArrayList<>(this.cpfsCnpjs);
        novo.filtrarCadastro = this.filtrarCadastro;

        return novo;

    }

    @Override
    public String toString() {

        return new StringBuilder()
                .append("busca: ").append(busca).append("\n")
                .append("contagem: ").append(contagem).append("\n")
                .append("numeroPagina: ").append(numeroPagina).append("\n")
                .append("tamanhoPagina: ").append(tamanhoPagina).append("\n")
                .append("ordenacao: ").append(ordenacao).append("\n")
                .append("tipoPessoaVinculada: ").append(tipoPessoaVinculada).append("\n")
                .append("nomePessoaVinculada: ").append(nomePessoaVinculada).append("\n")
                .append("cpfCnpjPessoaVinculada: ").append(cpfCnpjPessoaVinculada).append("\n")
                .append("cpfCnpj: ").append(cpfCnpj).append("\n")
                .append("codigoIbge: ").append(codigoIbge).append("\n")
                .append("cpfsCnpjs: ").append(cpfsCnpjs).append("\n")
                .append("filtrarCadastro: ").append(filtrarCadastro)
                .toString();

    }

}
