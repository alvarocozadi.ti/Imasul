package models;

import play.db.jpa.GenericModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "cadastro_unificado", name = "classificacao_empreendimento")
public class ClassificacaoEmpreendimento extends GenericModel {

	@Id
	public Integer id;

	public String descricao;

	public String codigo;

	public Boolean isento;

	public ClassificacaoEmpreendimento() {

		super();

	}

	@Override
	public Object _key() {
		return this.id;
	}

}