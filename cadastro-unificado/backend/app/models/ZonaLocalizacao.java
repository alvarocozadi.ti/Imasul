package models;

public enum ZonaLocalizacao implements Tipo {

	URBANA(0, "Urbana"),
	RURAL(1, "Rural");

	private Integer codigo;
	private String descricao;

	private ZonaLocalizacao(Integer codigo, String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;

	}

	@Override
	public Integer getCodigo() {
		return this.codigo;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}

}
