package models.portalSeguranca;

import org.hibernate.annotations.Immutable;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Immutable
@Table(schema = "portal_seguranca", name = "setor")
public class Setor extends GenericModel{

	@Id
	public Integer id;

	public String nome;

	public String sigla;

	public Setor() {

	}

}