package models.portalSeguranca;

import org.hibernate.annotations.Immutable;
import play.db.jpa.GenericModel;
import utils.Config;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Immutable
@Table(schema = "portal_seguranca", name = "perfil")
public class Perfil extends GenericModel{

	@Id
	public Integer id;

	public String nome;

	public String codigo;

	@ManyToMany
	@JoinTable(schema = "portal_seguranca", name = "permissao_perfil",
			joinColumns = @JoinColumn(name = "id_perfil", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_permissao", referencedColumnName = "id"))
	public List<Permissao> permissoes;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_modulo_pertencente", referencedColumnName = "id")
	public Modulo moduloPertencente;

	public Perfil() {

	}

	public Boolean isFromModulo(Integer idModulo) {

		boolean hasModulo = false;

		for(Permissao permissao : this.permissoes) {

			if(permissao.modulo.id.equals(idModulo)) {

				hasModulo = true;

			}

		}

		return hasModulo;

	}

	public static Set<Perfil> findPerfisByModuloRequest(Usuario usuario, Integer idModulo) {

		return new HashSet<>(usuario.getPerfisByModulo(idModulo));

	}

}