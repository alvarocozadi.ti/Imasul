package models.portalSeguranca;

import org.hibernate.annotations.Immutable;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Set;

@Entity
@Immutable
@Table(schema = "portal_seguranca", name = "permissao")
public class Permissao extends GenericModel {

	@Id
	public Integer id;

	public String nome;

	public String codigo;

	@ManyToOne(optional = false)
	@JoinColumn(name = "id_modulo", referencedColumnName = "id")
	@Required(message = "permissoes.validacao.modulo.req")
	public Modulo modulo;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(schema = "portal_seguranca", name = "permissao_perfil",
		joinColumns = @JoinColumn(name = "id_permissao", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "id_perfil", referencedColumnName = "id"))
	public Set<Perfil> perfis;

	public Permissao(){

	}

}
