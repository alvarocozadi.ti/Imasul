package models.portalSeguranca;

import common.models.Message;
import controllers.BaseController;
import exceptions.BaseException;
import exceptions.ValidationException;
import models.Empreendimento;
import models.Pessoa;
import models.PessoaFisica;
import models.PessoaJuridica;
import org.hibernate.annotations.Immutable;
import play.Play;
import play.db.jpa.GenericModel;
import secure.models.User;
import secure.services.IAuthenticatedUser;
import services.ExternalUsuarioService;
import utils.Config;
import utils.StringUtil;

import javax.persistence.*;
import java.util.*;

@Entity
@Immutable
@Table(schema = "portal_seguranca", name = "usuario")
public class Usuario extends GenericModel implements User, IAuthenticatedUser {

	@Id
	public Integer id;

	public String login;

	public String email;

	@Column(name = "data_cadastro")
	@Temporal(TemporalType.TIMESTAMP)
	public Date dataCadastro;

	public Boolean ativo;

	@Column(name = "data_atualizacao")
	@Temporal(TemporalType.TIMESTAMP)
	public Date dataAtualizacao;

	@ManyToMany
	@JoinTable(schema = "portal_seguranca", name = "perfil_usuario",
			joinColumns = @JoinColumn(name = "id_usuario", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_perfil", referencedColumnName = "id"))
	public Set<Perfil> perfis;

	@ManyToMany
	@JoinTable(schema = "portal_seguranca", name = "usuario_setor",
			joinColumns = @JoinColumn(name = "id_usuario"),
			inverseJoinColumns = @JoinColumn(name = "id_setor"))
	public List<Setor> setores;

	@OneToOne(mappedBy = "usuario")
	public AgendaDesativacao agendaDesativacao;

	@Transient
	public Perfil perfilSelecionado;

	@Transient
	public Pessoa pessoa;

	@Transient
	public Boolean temSenha;
	
	@Transient
	public String sessionKeyEntradaUnica;
	
	@Transient
	public String nome;

	@Transient
	public static final Integer ID_PERFIL_PUBLICO = 2;

	public Usuario() {

		this.dataCadastro = new Date();
		this.dataAtualizacao = new Date();
		this.ativo = false;

	}

	public Boolean getTemSenha() {

		return Usuario.find("id = :id AND senha is not null").setParameter("id", this.id).first() != null;

	}

	@Override
	public Integer getId() {

		return this.id;

	}

	@Override
	public String getName() {

		return this.login;

	}

	@Override
	public String[] getRoles() {

		Set<String> roles = new HashSet<>();

		Perfil perfil = this.perfilSelecionado;

		for(Permissao permissao : perfil.permissoes) {

			roles.add(permissao.codigo);

		}

		return roles.toArray(new String[roles.size()]);

	}

	@Override
	public boolean hasRole(String role) {

		Perfil perfil = this.perfilSelecionado;

		for(Permissao permissao : perfil.permissoes) {

			if(permissao.codigo.equals(role)) {

				return true;

			}

		}

		return false;

	}

	public void findPessoaByLogin() {

		if(this.login.length() == 11) {

			this.pessoa = PessoaFisica.find("cpf", this.login).first();

		} else if(this.login.length() == 14) {

			this.pessoa = PessoaJuridica.find("cnpj", this.login).first();

		}

	}

	public static Boolean isPessoaBloqued(String login) {

		if(login.length() == 11) {

			PessoaFisica pessoaFisica = PessoaFisica.find("cpf", login).first();

			if(pessoaFisica != null && pessoaFisica.bloqueado) {

				return true;

			}

		} else if(login.length() == 14) {

			PessoaJuridica pessoaJuridica = PessoaJuridica.find("cnpj", login).first();

			if(pessoaJuridica != null && pessoaJuridica.bloqueado) {

				return true;

			}

		}

		return false;

	}

	public static void validatePublicCreate(Usuario usuario) {

		if(usuario == null || usuario.perfis == null || usuario.perfis.size() != 1 || usuario.perfis.iterator().next().id != ID_PERFIL_PUBLICO) {

			throw new BaseException().userMessage("usuario.validacao.erro");

		}

	}

	public List<Perfil> getPerfisByModulo(Integer idModulo) {

		List<Perfil> perfis = new ArrayList<>();

		for(Perfil perfil : this.perfis) {

			if(perfil.isFromModulo(idModulo)) {

				perfis.add(perfil);

			} else if(perfil.isFromModulo(Config.ID_PORTAL) && perfil.nome.equals(Config.PERFIL_EXTERNO)) {

				perfis.add(perfil);

			}

		}

		return perfis;

	}

	public void save(Message message, Boolean createPessoa) {

		this.vincularPerfisPublicos(false);
		Boolean hasEmailError = ExternalUsuarioService.create(this);

		if(hasEmailError && !BaseController.isPublicResource()) {

			if(createPessoa) {

				message.setText("usuarios.cadastrar.envioEmail.erro", StringUtil.coverMail(this.email));

			} else {

				message.setText("usuarios.editar.envioEmail.erro", StringUtil.coverMail(this.email));

			}

		} else if(hasEmailError && BaseController.isPublicResource()) {

			message.setText("usuario.publico.cadastrar.envioEmail.erro", StringUtil.coverMail(this.email));

		} else if(!hasEmailError && BaseController.isPublicResource()) {

			message.setText("usuario.publico.cadastrar.sucesso", StringUtil.coverMail(this.email));

		}

	}

	public static void updateOrSave(Usuario usuario, Message message, Boolean create) {

		if(usuario != null && usuario.id != null) {

			ExternalUsuarioService.update(usuario);

		} else if(usuario != null) {

			usuario.save(message, create);

		} else {

			throw new ValidationException().userMessage("usuarios.nulo");

		}

	}

	/**
	 * Vincular a pessoa jurídica do empreendimento ao módulo de gestão de empreendimentos utilizando a sigla
	 * do módulo.
	 */
	public void vincularPerfisPublicos(Boolean isCadastroLicenciamento) {

		//vincularPerfilDoModuloGestaoEmpreendimentos();
		//vincularPerfilDoModuloLicenciamento(isCadastroLicenciamento);
	}

	/**
	 * Vincula a pessoa do empreendimento ao módulo de Gestão de Empreendimentos utilizando a sigla do módulo.
	 */
	private void vincularPerfilDoModuloGestaoEmpreendimentos() {

		Pessoa pessoa = Pessoa.findByCpfCnpj(this.login);
		Empreendimento empreendimentoVinculadoPessoaJuridica = Empreendimento.find("byPessoa", pessoa).first();

		if(empreendimentoVinculadoPessoaJuridica != null) {
			String siglaModuloGestaoEmpreendimentos = Play.configuration.getProperty("modulo.gestaoEmpreendimentos.sigla");
			List<String> codigosPerfis = new ArrayList<>();
			codigosPerfis.add(Play.configuration.getProperty("modulo.gestaoEmpreendimentos.perfilAdministrador.codigo"));
			this.adicionarPerfis(siglaModuloGestaoEmpreendimentos, codigosPerfis);
		}
	}

	/**
	 * Vincula a pessoa ao módulo de Licenciamento Ambiental utilizando a sigla do módulo.
	 */
	private void vincularPerfilDoModuloLicenciamento(Boolean isCadastroLicenciamento) {

		String siglaModuloLicenciamento = Play.configuration.getProperty("modulo.licenciamento.sigla");
		List<String> codigosPerfis = new ArrayList<>();
		codigosPerfis.add(Play.configuration.getProperty("modulo.licenciamento.perfilExterno.codigo"));
		if (isCadastroLicenciamento) {
			this.adicionarPerfis(siglaModuloLicenciamento, codigosPerfis);
		}

	}

	/**
	 * Vincular a pessoa jurídica do empreendimento ao módulo de gestão de empreendimentos utilizando a sigla
	 * do módulo.
	 * @param siglaModulo
	 * @param codigosPerfis
	 */
	private void adicionarPerfis(String siglaModulo, List<String> codigosPerfis) {

		if(siglaModulo != null && codigosPerfis != null) {
			Modulo modulo = Modulo.find("sigla = :siglaModulo").setParameter("siglaModulo", siglaModulo).first();

			if (modulo != null) {

				modulo.permissoes.forEach(p -> {
					p.perfis.forEach(pf -> {
						if (codigosPerfis.contains(pf.codigo)) {
							pf.permissoes = null;
							pf.moduloPertencente = null;
							if (!this.perfis.contains(pf)) {
								this.perfis.add(pf);
							}
						}
					});
				});
			} else {
				throw new ValidationException().userMessage("modulo.nao.cadastrado");
			}
		}

	}

}
