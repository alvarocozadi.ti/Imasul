package models.portalSeguranca;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "portal_seguranca", name = "modulo")
public class Modulo extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_modulo")
	@SequenceGenerator(name = "sq_modulo", sequenceName = "portal_seguranca.sq_modulo", allocationSize = 1)
	public Integer id;

	@Column(name = "sigla")
	public String sigla;

	@OneToMany(mappedBy = "modulo")
	public List<Permissao> permissoes;

	public Modulo() {

		super();

	}

}