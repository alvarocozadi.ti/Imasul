package models.portalSeguranca;

import controllers.Secure;
import org.hibernate.annotations.Immutable;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Immutable
@Table(schema = "portal_seguranca", name = "agenda_desativacao")
public class AgendaDesativacao extends GenericModel {

	@Id
	public Integer id;

	@OneToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	public Usuario usuario;

	@Temporal(TemporalType.DATE)
	@Column(name="data_inicio")
	public Date dataInicio;

	@Temporal(TemporalType.DATE)
	@Column(name="data_fim")
	public Date dataFim;

	@OneToOne
	@JoinColumn(name = "id_usuario_solicitante", referencedColumnName = "id")
	public Usuario usuarioSolicitante;

	@ManyToOne
	@JoinColumn(name = "id_motivo", referencedColumnName = "id")
	public Motivo motivo;

	@Column(name="descricao")
	public String descricao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_solicitacao")
	public Date dataSolicitacao;

	@Transient
	public Boolean tempoIndeterminado;

	public AgendaDesativacao() {

		super();

		this.dataSolicitacao = new Date();

		setUsuarioSolicitante();

	}

	private void setUsuarioSolicitante() {

		this.usuarioSolicitante = Secure.getAuthenticatedUser();

		if(this.usuarioSolicitante != null) {

			this.usuarioSolicitante.pessoa = null;

		}

	}

	public Boolean getTempoIndeterminado() {

		return dataFim == null && dataInicio == null;

	}

}
