package models.integracaoSiriema;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import exceptions.IntegracaoSiriemaException;
import exceptions.WebServiceException;
import play.Play;
import play.libs.WS;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static utils.Config.API_SIRIEMA_URL;

public class SiriemaWS {

    /**
     * Requisição GET - (Retorna dados do usuário cadastrados no sistema a partir do CPF ou CNPJ no SIRIEMA).
     */
    public static final String USUARIOS_DOCUMENTOS_URL = API_SIRIEMA_URL + "/usuarios/documento";

    /**
     * Requisição GET - (Busca lista de todos os usuários por data de última alteração cadastrados no sistema SIRIEMA).
     */
    public static final String USUARIOS_LISTAR_URL = API_SIRIEMA_URL + "/usuarios/listar";

    /**
     * Requisição POST - (Retorna dados do usuário cadastrados no sistema a partir do login e senha).
     */
    public static final String USUARIOS_LOGIN_URL = API_SIRIEMA_URL + "/usuarios/login";

    /**
     * Requisição POST - (Cadastro de usuário no sistema SIRIEMA).
     */
    public static final String BUSCAR_PESSOA_FISICA_URL = API_SIRIEMA_URL + "/usuarios/pessoa-fisica/adicionar";

    /**
     * Requisição POST - (Cadastro de usuário no sistema SIRIEMA).
     */
    public static final String BUSCAR_PESSOA_JURIDICA_URL = API_SIRIEMA_URL + "/usuarios/pessoa-juridica/adicionar";

    /**
     * Requisição GET - (Envia e-mail com link para alteração de senha de usuário cadastrado no sistema SIRIEMA).
     */
    public static final String USUARIOS_RECUPERAR_SENHA_URL = API_SIRIEMA_URL + "/usuarios/recuperar-senha";

    public static final String SIRIEMA_CLIENT_ID = Play.configuration.getProperty("siriema.client.id");
    public static final String SIRIEMA_CLIENT_SECRET = Play.configuration.getProperty("siriema.client.secret");
    public static final String SIRIEMA_TOKEN_AUTENTICACAO = Play.configuration.getProperty("siriema.url.token.autenticacao");
    public static final String SIRIEMA_GRANT_TYPE = Play.configuration.getProperty("siriema.grant.type");

    private static final String FORMATO_DATA_COMPLETO = "dd/MM/yyyy HH:mm";

    private SiriemaResponseTokenVO tokenSiriema;

    private GsonBuilder gsonBuilder = new GsonBuilder();
    private DateFormat dateFormat = new SimpleDateFormat(FORMATO_DATA_COMPLETO);


    public SiriemaWS(){
        this.tokenSiriema = this.buscarTokenAcesso();
    }

    public SiriemaResponseTokenVO buscarTokenAcesso (){

        Map<String, String> paramsAuth = new HashMap<String, String>();

        paramsAuth.put("grant_type", SIRIEMA_GRANT_TYPE);
        paramsAuth.put("client_id", SIRIEMA_CLIENT_ID);
        paramsAuth.put("client_secret", SIRIEMA_CLIENT_SECRET);

        WS.WSRequest request = WS.url(SIRIEMA_TOKEN_AUTENTICACAO);

        request.setHeader("Content-Type","application/x-www-form-urlencoded");
        request.setHeader("Accept","application/json");
        request.setParameters(paramsAuth);

        WS.HttpResponse responseAuth = request.post();

        if (!responseAuth.success())
            throw new WebServiceException(responseAuth);

        Type type = new TypeToken<SiriemaResponseTokenVO>(){}.getType();
        SiriemaResponseTokenVO retornoVO = gsonBuilder.create().fromJson(responseAuth.getJson(), type);

        return retornoVO;
    }

    public RetornoPessoasSiriemaVO findUsuariosAtualizados(Date dataUltimaIntegracao, Integer pagina) throws IntegracaoSiriemaException {

        dataUltimaIntegracao = fusoHorario(dataUltimaIntegracao);

        Map<String, String> params = new HashMap<String, String>();
        params.put("dataUltimaAlteracao",dateFormat.format(dataUltimaIntegracao));
        params.put("paginaAtual", pagina.toString());
        params.put("totalPorPagina", "50");

        WS.WSRequest request = WS.url(USUARIOS_LISTAR_URL);
        request.setHeader("Authorization", "Bearer "+ this.tokenSiriema.access_token);
        request.setParameters(params);

        WS.HttpResponse response = request.get();

        if (!response.success() && semAtualizacao(response))
            throw new WebServiceException(response);

        Type type = new TypeToken<RetornoPessoasSiriemaVO>(){}.getType();
        RetornoPessoasSiriemaVO retornoPessoasSiriemaVO = gsonBuilder.create().fromJson(response.getJson(), type);

//        // Requisição ocorreu, mas o webservice retornou um erro
//        if (retornoPessoasSiriemaVO.status == null || !retornoPessoasSiriemaVO.status.equals(MensagemSiriema.SUCESSO))
//            throw new WebServiceException("status: " + retornoPessoasSiriemaVO.status + " mensagem: " + retornoPessoasSiriemaVO.mensagem);

        return retornoPessoasSiriemaVO;

    }

    public Boolean verificaPessoaSiriema(String cpfCnpj){

        Map<String, String> params = new HashMap<String, String>();
        params.put("cpfCpnj",cpfCnpj);

        WS.WSRequest request = WS.url(USUARIOS_DOCUMENTOS_URL);
        
        request.setHeader("Authorization", "Bearer "+ this.tokenSiriema.access_token);
        request.setHeader("Accept","application/json");
        request.setParameters(params);

        WS.HttpResponse response = request.get();

        return response.success();
    }

    public boolean semAtualizacao(WS.HttpResponse response){

        int status = response.getStatus().intValue();

        return status != 404;
    }

    public Date fusoHorario(Date dataUltimaIntegracao){

        Calendar d = Calendar.getInstance();
        d.setTime(dataUltimaIntegracao);
        d.add(Calendar.HOUR, -1); // - 1 hora de fuso
        return d.getTime();
    }


}
