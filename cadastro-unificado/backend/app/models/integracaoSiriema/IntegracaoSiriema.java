package models.integracaoSiriema;

import exceptions.IntegracaoSiriemaException;
import models.PessoaJuridica;
import models.portalSeguranca.Usuario;
import org.apache.commons.lang.exception.ExceptionUtils;
import play.Logger;
import play.Play;
import play.db.jpa.GenericModel;
import play.libs.Time;
import utils.DataUtils;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(schema = "cadastro_unificado", name = "integracao_siriema")
public class IntegracaoSiriema extends GenericModel{

    private static boolean ATUALIZAR_IMOVEIS = Boolean.parseBoolean(
            Play.configuration.getProperty("integracaoSicar.atualizarImoveis"));

    private static final String CONFIG_DATA_FIM = Play.configuration.getProperty("integracaoSiriema.usuarios.dataFim");

    private static final Date DATA_FIM;

    public static final int MAX_TENTATIVAS = Integer.parseInt(Play.configuration.getProperty("integracaoSiriema.usuarios.tentativas"));
    public static final Date MIN_DATA_INICIAL = DataUtils.parseDate("dd/MM/yyyy HH:mm", Play.configuration.getProperty("integracaoSiriema.usuarios.dataInicio"));
    public static final Integer CONFIG_INTEGRACAO_DELAY = Time.parseDuration(Play.configuration.getProperty("integracaoSiriema.usuarios.delay"));

    public static final String PREFIXO_LOG = "--> INTEGRACAO SIRIEMA: ";
    public static final int PENDENTE = 1;
    public static final int FINALIZADO = 2;
    public static final int ERRO = 3;

    @Id
    @Column(name="id_integracao_siriema")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cadastro_unificado.integracao_siriema_id_seq")
    @SequenceGenerator(name = "cadastro_unificado.integracao_siriema_id_seq", sequenceName = "cadastro_unificado.integracao_siriema_id_seq", allocationSize = 1)
    public Long id;

    @Column(name = "dt_integracao")
    public Date dataIntegracao;

    @Column(name = "dt_inicial")
    public Date dataInicial;

    @Column(name = "dt_final")
    public Date dataFinal;

    @Column(name = "cod_ultima_pagina")
    public Integer ultimaPagina;

    @Column(name = "tx_descricao")
    public String descricao;

    @Column(name = "tx_erro")
    public String erro;

    @Column(name = "tp_status")
    public Integer status;

    @Transient
    private SiriemaWS siriemaWS;

    @Transient
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    static {

        if (CONFIG_DATA_FIM != null && !CONFIG_DATA_FIM.isEmpty())
            DATA_FIM = DataUtils.parseDate("dd/MM/yyyy HH:mm", CONFIG_DATA_FIM);
        else
            DATA_FIM = null;
    }

    public static IntegracaoSiriema findUltima() {

        return find("order by dataFinal desc").first();
    }

    public void executar(Date horaLimite) throws IntegracaoSiriemaException {

        inicializar();

        if (this.status == FINALIZADO) return;

        boolean executado = false;

        Logger.info("\n\n");
        Logger.info(PREFIXO_LOG + "de %s a %s", dateFormat.format(dataInicial), dateFormat.format(dataFinal));

        // executar proxima pagina
        while ( (status == PENDENTE || !executado) && horaLimite.after(new Date())) {

            executado = true;
            sincronizarProximaPagina();
        }
    }

    private void sincronizarProximaPagina() {

        int tentativas = 0;
        this.ultimaPagina++;
        RetornoPessoasSiriemaVO retornoPessoasSiriemaVO = null;

        do { // Buscando a pagina atual. Repetir em caso de erro

            tentativas++;
            retornoPessoasSiriemaVO = buscarPagina();

        } while (status == ERRO && tentativas < MAX_TENTATIVAS);

        if (status != ERRO)
            processarPagina(retornoPessoasSiriemaVO);

        // Se após buscar e processar a pagina o status for ERRO, decrementar para tentar esta pagina novamente.
        if (status == ERRO)
            this.ultimaPagina--;

        if (retornoPessoasSiriemaVO != null && retornoPessoasSiriemaVO.paginavel.totalPaginas - 1 == this.ultimaPagina)
            this.status = FINALIZADO;

        if (retornoPessoasSiriemaVO != null)
            Logger.info(PREFIXO_LOG + "Processada pagina " + (this.ultimaPagina + 1) + "/" + retornoPessoasSiriemaVO.paginavel.totalPaginas);

        this.save();
    }

    /**
     * Busca no SIRIEMA uma página de usuários da integração.
     */
    private RetornoPessoasSiriemaVO buscarPagina() {

        try {

            this.erro = null;
            this.status = PENDENTE;
            return siriemaWS.findUsuariosAtualizados(dataInicial , ultimaPagina);

        } catch (Exception e) {

            Logger.info(PREFIXO_LOG + "pagina = %s (erro ao buscar)", ultimaPagina);
            Logger.error(e, PREFIXO_LOG + e.getMessage());

            this.status = ERRO;
            this.erro = ExceptionUtils.getFullStackTrace(e);
            return null;

        }
    }

    private void processarPagina(RetornoPessoasSiriemaVO retornoPessoasSiriemaVO) {

        try {

            this.erro = null;
            this.status = PENDENTE;

            if(retornoPessoasSiriemaVO.retorno != null) {

                retornoPessoasSiriemaVO.retorno.forEach(pessoaSiriema -> {

                    if(pessoaSiriema == null){
                        return;
                    }

                    if (pessoaSiriema.cnpj != null) {
                        PessoaJuridica pessoa = PessoaJuridica.findByCnpj(pessoaSiriema.cnpj);
                        if (pessoa == null) {
                            try {
                                pessoaSiriema.convertToPessoaEU(true);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    } else if (pessoaSiriema.cpf != null) {

                        try {
                            pessoaSiriema.convertToPessoaEU(true);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }else{
                Logger.info(PREFIXO_LOG + "Nenhum novo resgistro encontrado");
                this.status = FINALIZADO;
            }

        } catch (Exception e) {

            Logger.info(PREFIXO_LOG + "pagina = %s (erro ao processar)", ultimaPagina);
            Logger.error(e, PREFIXO_LOG + e.getMessage());

            this.status = ERRO;
            this.erro = ExceptionUtils.getFullStackTrace(e);
        }
    }

    /**
     * Inicializa os atributos antes do processamento inserindo valores padrão, caso
     * não estejam preenchidos.
     */
    private void inicializar() throws IntegracaoSiriemaException {

        this.siriemaWS = new SiriemaWS();
        this.dataIntegracao = new Date();

        if (status == null)
            status = PENDENTE;

        if (ultimaPagina == null)
            ultimaPagina = -1;

        if (dataInicial == null)
            dataInicial = MIN_DATA_INICIAL;

        if (dataFinal == null) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.SECOND, 0);
            calendar.add(Calendar.SECOND, -CONFIG_INTEGRACAO_DELAY);
            dataFinal = calendar.getTime();
        }

        if (DATA_FIM != null) {

            if (dataFinal.after(DATA_FIM))
                dataFinal = DATA_FIM;

            if (!dataInicial.before(DATA_FIM))
                throw new IntegracaoSiriemaException(
                        "Integrações não executadas. A data de fim da execução foi excedida: " + CONFIG_DATA_FIM);
        }

        if (dataFinal.before(dataInicial))
            throw new IntegracaoSiriemaException(
                    "Integrações não executadas. A data de fim (" + dateFormat.format(dataFinal) + ") "
                            + "é anterior a data de início (" + dateFormat.format(dataInicial) + ")");

    }

}
