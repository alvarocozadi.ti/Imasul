package models.integracaoSiriema;

public class SiriemaResponseTokenVO {

    public String access_token;

    public String refresh_token;

    public String token_type;

    public String session_state;

    public int expires_in;

    public int refresh_expires_in;

}
