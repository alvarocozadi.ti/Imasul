package models.integracaoSiriema.enums;

public enum Status {

    ATIVO("Ativo","AT",false),
    CANCELADO("Cancelado","CA",false),
    INSCRITO("Inscrito", "IN", false),
    PENDENTE("Pendente","PE",false),
    RETIFICADO("Retificado","RE",true);

    public String nome;
    public String id;

    //Variavel que controla se o ENUM é somente para controle da lógica.
    public Boolean controleInterno;

    private Status(String status, String id, Boolean controleInterno) {
        this.id = id;
        this.nome = status;
        this.controleInterno = controleInterno;
    }

    public static Status getById(String id) {

        for (Status status : Status.values()) {

            if (status.id.equals(id))
                return status;

        }

        return null;

    }

    public static Status getByNome(String nome) {

        for (Status status : Status.values()) {

            if (status.nome.equals(nome))
                return status;
        }

        return null;
    }
}
