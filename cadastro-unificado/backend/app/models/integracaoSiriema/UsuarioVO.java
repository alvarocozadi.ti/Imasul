package models.integracaoSiriema;


import java.util.Date;

public class UsuarioVO {
    public Long id;
    public String login;
    public Date dataCadastro;
    public String status;
    public String senha;
    public Date dataAtualizacao;
}

