package models;

import java.util.ArrayList;
import java.util.List;

public class InformacaoValidacao {

	public List<String> nomesPessoasFisicas;
	public List<String> nomesPessoasJuridicas;
	public List<String> nomesMaes;
	public List<String> nomesMunicipios;
	public String estadoMunicipio;

	public InformacaoValidacao() {

		super();

		nomesPessoasFisicas = new ArrayList<>();
		nomesPessoasJuridicas = new ArrayList<>();
		nomesMaes = new ArrayList<>();
		nomesMunicipios = new ArrayList<>();

	}

}
