package models.oauth;

import java.io.Serializable;

public class Token implements Serializable {

	public String access_token;
	public String token_type;
	public String refresh_token;
	public String expires_in;

	public Token() {

		super();

	}

}
