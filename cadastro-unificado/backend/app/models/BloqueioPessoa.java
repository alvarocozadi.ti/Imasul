package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "controle_bloqueio_pessoa", schema = "cadastro_unificado")
public class BloqueioPessoa extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_controle_bloqueio_pessoa")
	@SequenceGenerator(name = "sq_controle_bloqueio_pessoa", sequenceName = "sq_controle_bloqueio_pessoa", allocationSize = 1)
	public Integer id;

	@OneToOne
	@JoinColumn(name = "id_pessoa", referencedColumnName = "id")
	public Pessoa pessoa;

	@Column(name = "numero_tentativas")
	public Integer numeroTentativas;

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro")
	public Date dataCadastro;

	@Transient
	public static Integer numeroMaxTentativas = 2;

	public BloqueioPessoa(Pessoa pessoa) {

		super();

		this.numeroTentativas = 0;
		this.pessoa = pessoa;
		this.dataCadastro = new Date();

	}

	public static BloqueioPessoa addNumeroTentativas(Pessoa pessoa) {

		BloqueioPessoa bloqueioPessoa = BloqueioPessoa.find("FROM BloqueioPessoa bp JOIN FETCH bp.pessoa p WHERE p.id = :id")
				.setParameter("id", pessoa.id)
				.first();

		if(bloqueioPessoa == null) {

			bloqueioPessoa = new BloqueioPessoa(pessoa);

		}

		bloqueioPessoa.numeroTentativas += 1;
		bloqueioPessoa.dataCadastro = new Date();

		return bloqueioPessoa.save();

	}

}