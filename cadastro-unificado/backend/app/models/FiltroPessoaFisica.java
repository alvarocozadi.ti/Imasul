package models;

import common.models.Filter;

public class FiltroPessoaFisica extends Filter {

	public enum Ordenacao implements Order {

		NOME_ASC(OrderDirection.ASC, "nome"),
		NOME_DESC(OrderDirection.DESC, "nome"),
		MUNICIPIO_ASC(OrderDirection.ASC, "municipio.nome"),
		MUNICIPIO_DESC(OrderDirection.DESC, "municipio.nome");

		private OrderDirection orderDirection;
		private String field;

		private Ordenacao(OrderDirection orderDirection, String field) {

			this.orderDirection = orderDirection;
			this.field = field;

		}

		@Override
		public OrderDirection getOrderDirection() {
			return this.orderDirection;
		}

		@Override
		public String getField() {
			return this.field;
		}

	}

	public String filtroSimplificado;

	public String cpf;

	public String nome;

	public int numeroPagina;

	public int tamanhoPagina;

	public Ordenacao ordenacao;

}