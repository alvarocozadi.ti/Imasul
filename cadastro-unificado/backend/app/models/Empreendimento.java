package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.DadosRedesimVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.IdentificadorTipoPessoaEnum;
import br.ufla.lemaf.integracao.redesimples.WSE013.ResponsavelPeranteCnpjVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.SocioVo;
import com.vividsolutions.jts.geom.Geometry;
import common.models.Filter;
import exceptions.ValidationException;
import models.historico.EmpreendimentoHistorico;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StringType;
import play.data.validation.CheckWith;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import utils.Config;
import utils.ListUtil;
import utils.ValidationUtil;
import validators.EstrangeiroCheck;
import validators.If;
import validators.RequiredIf;
import validators.ValidIf;
import vo.EmpreendimentoSobreposicao;
import vo.EmpreendimentoSobreposicaoVO;

import javax.persistence.*;
import java.text.Normalizer;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(schema = "cadastro_unificado", name = "empreendimento")
@org.hibernate.annotations.Filter(name = "entityRemovida")
public class Empreendimento extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_empreendimento")
	@SequenceGenerator(name = "sq_empreendimento", sequenceName = "cadastro_unificado.sq_empreendimento", allocationSize = 1)
	public Integer id;

	@Required(message = "empreendimento.denominacao.obrigatorio")
	public String denominacao;

//	@Unique(message = "empreendimento.pessoa.unico")
	@RequiredIf(ifs = @If(option = If.IfOption.FALSE, attribute = "removido"), message = "empreendimento.pessoa.obrigatorio")
	@ValidIf(ifs = @If(option = If.IfOption.NULL, attribute = "id"))
	@CheckWith(value = EstrangeiroCheck.class, message = "empreendimento.cadastro.pessoa.estrangeira")
	@OneToOne
	@JoinColumn(name = "id_pessoa", referencedColumnName = "id")
	public Pessoa pessoa;

	@Valid
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "contato_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_contato", referencedColumnName = "id"))
	public Set<Contato> contatos;

	@Valid
	@Required(message = "enderecos.obrigatorios")
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "endereco_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_endereco", referencedColumnName = "id"))
	public Set<Endereco> enderecos;

	@Required(message = "empreendimento.localizacao.obrigatorio")
	@Valid
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_localizacao", referencedColumnName = "id", nullable = false)
	public Localizacao localizacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_cadastro")
	public Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_atualizacao")
	public Date dataAtualizacao;

	public Boolean removido;

	@Column(name = "cpf_cnpj_cadastrante")
	public String cpfCnpjCadastrante;

	@CheckWith(value = EstrangeiroCheck.class, message = "empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "proprietario_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_proprietario", referencedColumnName = "id"))
	public Set<Pessoa> proprietarios;

	@CheckWith(value = EstrangeiroCheck.class, message = "empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "responsavel_tecnico_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_responsavel_tecnico", referencedColumnName = "id"))
	public Set<Pessoa> responsaveisTecnicos;

	@CheckWith(value = EstrangeiroCheck.class, message = "empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "representante_legal_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_representante_legal", referencedColumnName = "id_pessoa"))
	public Set<PessoaFisica> representantesLegais;

	@CheckWith(value = EstrangeiroCheck.class, message = "empreendimento.cadastro.pessoa.invalida")
	@ManyToMany
	@JoinTable(name = "responsavel_legal_empreendimento", schema = "cadastro_unificado",
			joinColumns = @JoinColumn(name = "id_empreendimento", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "id_responsavel_legal", referencedColumnName = "id_pessoa"))
	public Set<PessoaFisica> responsaveisLegais;

	@OneToMany(mappedBy = "empreendimento", fetch = FetchType.LAZY)
	public List<Cnae> cnaes;

	@Column(name = "porte")
	public String porte;

	@ManyToOne
	@JoinColumn(name = "id_classificacao_empreendimento", referencedColumnName = "id")
	public ClassificacaoEmpreendimento classificacaoEmpreendimento;

	@OneToOne
	@JoinColumn(name = "id_empreendedor", referencedColumnName = "id")
	public Empreendedor empreendedor;

	@Transient
	public boolean pessoaIsProprietario;

	@Transient
	public boolean pessoaIsResponsavelTecnico;

	@Transient
	public boolean pessoaIsRepresentanteLegal;

	@Transient
	public boolean pessoaIsResponsavelLegal;

	public Empreendimento() {

		super();

		this.dataCadastro = new Date();
		this.dataAtualizacao = new Date();
		this.removido = false;

	}

	public Empreendimento(DadosRedesimVo dadosRedesimVo) {

		super();
		Config.RedeSimplesLogger.trace("Construindo Empreendimento");
		PessoaJuridica pessoaJuridicaEmpreendimento = PessoaJuridica.findByCnpj(dadosRedesimVo.getCnpj());
		if (pessoaJuridicaEmpreendimento == null) {
			Config.RedeSimplesLogger.trace("Criado Pessoa Juridica");
			Config.RedeSimplesLogger.trace("= Endereco");
			Config.RedeSimplesLogger.trace("== Exterior" + dadosRedesimVo.getEndereco().getEnderecoExterior());
			Config.RedeSimplesLogger.trace("== Exterior" + dadosRedesimVo.getEndereco().getCodTipoLogradouro());

			pessoaJuridicaEmpreendimento = new PessoaJuridica(dadosRedesimVo);
			pessoaJuridicaEmpreendimento.save();
			PessoaJuridica pessoaJuridicaEmpreendimentoSalvo = PessoaJuridica.findByCnpj(dadosRedesimVo.getCnpj());
			Config.RedeSimplesLogger.trace("Pessoa Salva CNPJ: " + pessoaJuridicaEmpreendimentoSalvo.cnpj);
		}
		Config.RedeSimplesLogger.trace("## Informacoes adicionais");
		Config.RedeSimplesLogger.trace(" ###Proprietário");
		Set<Pessoa> listaProprietarios = this.proprietariosFromRedeSimples(dadosRedesimVo);

		Config.RedeSimplesLogger.trace(" ###Representantes");
		Set<PessoaFisica> listaRepresentantesLegais = this.representantesLegaisFromRedeSimples(dadosRedesimVo);

		Config.RedeSimplesLogger.trace(" ###Responsavei legais");
		Set<PessoaFisica> listaResponsaveisLegais = this.responsavelLegalFromRedeSimples(dadosRedesimVo);
		Config.RedeSimplesLogger.trace("### Informacoes adicionais - Fim");
		Date dataAtual = new Date();

		Config.RedeSimplesLogger.trace("### Localizacao empreendimento");
		// Criar Localizacação do Empreendimento
		Municipio municipioEmpreendimento = Municipio.municipioRedeSimples(dadosRedesimVo.getEndereco().getCodMunicipio());
		Localizacao localizacaoMunicipioEmpreendimento = new Localizacao();
		localizacaoMunicipioEmpreendimento.geometria = (municipioEmpreendimento.getGeometry().getCentroid()).buffer(0.00001, 2);

		// Atributos padrão
		this.dataCadastro = dataAtual;
		this.dataAtualizacao = dataAtual;
		this.removido = false;

		// Atributos editáveis
		this.denominacao = dadosRedesimVo.getNomeEmpresarial();
		this.pessoa = pessoaJuridicaEmpreendimento;
		Config.RedeSimplesLogger.trace("Contato e endereco Empreendimento Inicio");
		this.contatos = Contato.contatosRedeSimples(dadosRedesimVo.getContato());
		this.enderecos = Endereco.enderecosRedeSimples(dadosRedesimVo.getEndereco());
		Config.RedeSimplesLogger.trace("Contato e endereco Empreendimento Fim");
		this.localizacao = localizacaoMunicipioEmpreendimento;

		if(this.localizacao.geometria.getSRID() == 0) {
			this.localizacao.geometria.setSRID(municipioEmpreendimento.getGeometry().getSRID());
		}

		this.porte = dadosRedesimVo.getPorte() != null ? dadosRedesimVo.getPorte().value() : "";
		this.preencherClassificacaoEmpreendimento(dadosRedesimVo.getCodNaturezaJuridica());
		this.proprietarios = listaProprietarios;
		this.representantesLegais = listaRepresentantesLegais;
		this.responsaveisLegais = listaResponsaveisLegais;
		Config.RedeSimplesLogger.trace("Construindo Empreendimento FIM ");
	}

	public void atualizarEmpreendimento(DadosRedesimVo dadosRedesimVo) {

		this.salvarHistorico(null);

		/*
		PessoaJuridica pessoaJuridicaEmpreendimento = PessoaJuridica.findByCnpj(dadosRedesimVo.getCnpj());
		pessoaJuridicaEmpreendimento.atualizarPessoaJuridica(dadosRedesimVo);
		pessoaJuridicaEmpreendimento.save();
		*/

		Set<Pessoa> listaProprietarios = this.proprietariosFromRedeSimples(dadosRedesimVo);

		Set<PessoaFisica> listaRepresentantesLegais = this.representantesLegaisFromRedeSimples(dadosRedesimVo);

		Set<PessoaFisica> listaResponsaveisLegais = this.responsavelLegalFromRedeSimples(dadosRedesimVo);

		// Atributos padrão
		this.dataAtualizacao = new Date();

		// Atributos editáveis
		this.denominacao = dadosRedesimVo.getNomeEmpresarial();
		this.porte = dadosRedesimVo.getPorte() != null ? dadosRedesimVo.getPorte().value() : "";
		this.preencherClassificacaoEmpreendimento(dadosRedesimVo.getCodNaturezaJuridica());

		if (this.contatos != null && !this.contatos.isEmpty()) {

			this.contatos.clear();
			Set<Contato> contatos = Contato.contatosRedeSimples(dadosRedesimVo.getContato());
			this.contatos.addAll(contatos);
		}

		this.enderecos.clear();
		Set<Endereco> enderecos = Endereco.enderecosRedeSimples(dadosRedesimVo.getEndereco());
		this.enderecos.addAll(enderecos);

		/**
		 * Regra definida juntamente com o P.O - O vínculo da pessoa com o empreendimento não pode ser removido, somente inserido.
		 */

		if (listaProprietarios != null) {

			if (this.proprietarios == null)
				this.proprietarios = new HashSet<>(listaProprietarios);

			this.proprietarios.addAll(listaProprietarios);
		}

		if (listaRepresentantesLegais != null) {

			if (this.representantesLegais == null)
				this.representantesLegais = new HashSet<>();

			this.representantesLegais.addAll(listaRepresentantesLegais);
		}

		if (listaResponsaveisLegais != null) {

			if (this.responsaveisLegais == null)
				this.responsaveisLegais = new HashSet<>();

			this.responsaveisLegais.addAll(listaResponsaveisLegais);
		}

		Cnae.cnaesRedeSimples(dadosRedesimVo.getAtividadesEconomica(), this);
	}

	@Override
	public Object _key() {

		return this.id;

	}

	@Override
	public Empreendimento save() {

		if (this.pessoa == null) {

			throw new ValidationException("empreendimento.pessoa.obrigatorio");

		}

		if (this.pessoa.id != null) {

			Pessoa pessoaBusca = Pessoa.findById(this.pessoa.id);

			if (pessoaBusca == null) {

				throw new ValidationException("empreendimento.cadastro.pessoa.naoEncontrada");

			}

			for (Endereco endereco : pessoaBusca.enderecos) {

				if (endereco.numero == null) {

					endereco.semNumero = true;

				}

			}

			this.pessoa = pessoaBusca;

		}

		validatePapeis(this);

		if (this.pessoa.id == null) {

			this.pessoa.save();

		}

		this.pessoa.usuario = null;
		this.pessoa.refresh();

		if (this.proprietarios != null) {

			if (!this.proprietarios.isEmpty()) {
				this.proprietarios.forEach(proprietario -> {
					if (proprietario.id == null) {
						proprietario.enderecos = Endereco.formatarEndereco(proprietario.enderecos);
						proprietario.updateOrCreate(false);
					}
				});

			}

		}

		if (this.representantesLegais != null) {

			if (!this.representantesLegais.isEmpty()) {
				this.representantesLegais.forEach(representanteLegal -> {
					if (representanteLegal.id == null) {
						representanteLegal.enderecos = Endereco.formatarEndereco(representanteLegal.enderecos);
						representanteLegal.updateOrCreate(false);
					}
				});

			}

		}

		if (this.responsaveisLegais != null) {

			if (!this.responsaveisLegais.isEmpty()) {

				this.responsaveisLegais.forEach(responsavelLegal -> {

					if (responsavelLegal.id == null) {

						responsavelLegal.enderecos = Endereco.formatarEndereco(responsavelLegal.enderecos);
						responsavelLegal.updateOrCreate(false);
					}
				});

			}

		}

		if (this.responsaveisTecnicos != null) {

			if (!this.responsaveisTecnicos.isEmpty()) {

				this.responsaveisTecnicos.forEach(responsavelTecnico -> {

					if (responsavelTecnico.id == null) {

						responsavelTecnico.enderecos = Endereco.formatarEndereco(responsavelTecnico.enderecos);
						responsavelTecnico.updateOrCreate(false);
					}
				});

			}

		}

		Endereco.validateEnderecos(this.enderecos);

		ValidationUtil.validate(this);

		validateGeometria(this);

		if (this.empreendedor != null) {

			Empreendedor emp = Empreendedor.findByCpfCnpj(this.empreendedor.pessoa.getCpfCnpj());

			if (emp != null) {
				this.empreendedor = emp;
			}
			this.empreendedor.ativo = false;

			this.empreendedor.pessoa.enderecos.forEach(e -> {
				e.semNumero = e.numero == null;
			});

			this.empreendedor.save();
		}

		return super.save();

	}

	public static List<Empreendimento> findByFilter(FiltroEmpreendimento filtro) {

		Session session = (Session) JPA.em().getDelegate();
		Criteria crit = session.createCriteria(Empreendimento.class);
		crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
		crit.setMaxResults(filtro.tamanhoPagina);

		crit.createAlias("pessoa", "ps");

		Disjunction disjunction = Restrictions.disjunction();
		disjunction.add(Restrictions.eq("ps.class", PessoaFisica.class));
		disjunction.add(Restrictions.eq("ps.class", PessoaJuridica.class));

		crit.add(disjunction);

		crit.createAlias("enderecos", "en");
		crit.createAlias("en.municipio", "mun").add(Restrictions.eq("en.tipo.id", TipoEndereco.ID_PRINCIPAL));

		if(filtro.cpfCnpj != null){
			Criterion cpfCnpjCadastrante = Restrictions.like("cpfCnpjCadastrante", filtro.cpfCnpj);
			crit.add(cpfCnpjCadastrante);
		}

		if (filtro.busca != null) {

			if (filtro.busca.equals("%")) {

				return new ArrayList<>();

			}

			if (filtro.busca.equals(filtro.cpfCnpjEmpreendedor)) {

				crit.createAlias("empreendedor", "emp");
				crit.createAlias("emp.pessoa", "pessoaEmp");

				Criterion cpf = Restrictions.like("pessoaEmp.cpf", filtro.cpfCnpjEmpreendedor);
				Criterion cnpj = Restrictions.like("pessoaEmp.cnpj", filtro.cpfCnpjEmpreendedor);
				crit.add(Restrictions.or(cpf, cnpj));
			}

			if (filtro.cpfsCnpjs != null && filtro.cpfsCnpjs.size() > 0) {

				Criterion cpf = Restrictions.in("ps.cpf", filtro.cpfsCnpjs);
				Criterion cnpj = Restrictions.in("ps.cnpj", filtro.cpfsCnpjs);
				crit.add(Restrictions.or(cpf, cnpj));

			}

			if (filtro.filtrarCadastro == null && !filtro.busca.equals(filtro.cpfCnpjEmpreendedor)) {

				crit.createAlias("empreendedor", "emp");
				crit.createAlias("emp.pessoa", "pessoaEmp");

				if(filtro.cpfCnpjEmpreendedor != null) {
					Criterion cpfEmp = Restrictions.eq("pessoaEmp.cpf", filtro.cpfCnpjEmpreendedor);
					Criterion cnpjEmp = Restrictions.eq("pessoaEmp.cnpj", filtro.cpfCnpjEmpreendedor);
					crit.add(Restrictions.and(Restrictions.or(cpfEmp, cnpjEmp)));
				}

				Criterion cpf = Restrictions.like("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
				Criterion cnpj = Restrictions.like("ps.cnpj", "%" + filtro.busca.replaceAll("[.\\-\\/]", "") + "%");
				String parametro = Normalizer.normalize(filtro.busca, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				Criterion denominacao = Restrictions.sqlRestriction("f_remove_caracter_especial({alias}.denominacao) ILIKE f_remove_caracter_especial(?)", "%" + parametro + "%", new StringType());

				Criterion empreendedorNome = Restrictions.ilike("pessoaEmp.nome", "%" + filtro.busca + "%");
				Criterion empreendedorRazao = Restrictions.ilike("pessoaEmp.razaoSocial", "%" + filtro.busca + "%");
				Criterion municipio = Restrictions.ilike("mun.nome", "%" + filtro.busca + "%");

				LogicalExpression empreendimentoNomeExp = Restrictions.or(empreendedorNome, empreendedorRazao);

				LogicalExpression orExp = Restrictions.or(cpf, cnpj);

				LogicalExpression orExp1 = Restrictions.or(empreendimentoNomeExp, denominacao);

				LogicalExpression orExp2 = Restrictions.or(orExp1, orExp);

				LogicalExpression orExp3 = Restrictions.or(orExp2, municipio);

				crit.add(orExp3);

			} else {
				if (filtro.cpfCnpjEmpreendedor == null) {
					Criterion cpf = Restrictions.like("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
					Criterion cnpj = Restrictions.like("ps.cnpj", "%" + filtro.busca.replaceAll("[.\\-\\/]", "") + "%");
					String parametro = Normalizer.normalize(filtro.busca, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
					Criterion denominacao = Restrictions.sqlRestriction("f_remove_caracter_especial({alias}.denominacao) ILIKE f_remove_caracter_especial(?)", "%" + parametro + "%", new StringType());

					LogicalExpression orExp = Restrictions.or(Restrictions.or(cpf, cnpj), denominacao);

					crit.add(orExp);
				}
			}

		} else {

			empreendimentoFilters(crit, filtro);

		}


		if (filtro.ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

			crit.addOrder(Order.asc(filtro.ordenacao.getField()));

		} else {

			crit.addOrder(Order.desc(filtro.ordenacao.getField()));

		}

		return crit.list();

	}

	private static void empreendimentoFilters(Criteria crit, FiltroEmpreendimento filtro) {

		if (filtro.denominacao != null) {

			String parametro = Normalizer.normalize(filtro.denominacao, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
			Criterion denominacao = Restrictions.sqlRestriction("f_remove_caracter_especial({alias}.denominacao) ILIKE f_remove_caracter_especial(?)", "%" + parametro + "%", new StringType());
			crit.add(denominacao);

		}

//		if(filtro.cpfCnpj != null) {
//
//			Criterion cpf = Restrictions.eq("ps.cpf", filtro.cpfCnpj.replaceAll("[.-]", ""));
//			Criterion cnpj = Restrictions.eq("ps.cnpj", filtro.cpfCnpj.replaceAll("[.\\-\\/]", ""));
//			crit.add(Restrictions.or(cpf, cnpj));
//
//		}

		if (filtro.cpfsCnpjs != null && filtro.cpfsCnpjs.size() > 0) {

			Criterion cpf = Restrictions.in("ps.cpf", filtro.cpfsCnpjs);
			Criterion cnpj = Restrictions.in("ps.cnpj", filtro.cpfsCnpjs);
			crit.add(Restrictions.or(cpf, cnpj));

		}

		if (filtro.codigoIbge != null) {

			crit.add(Restrictions.eq("mun.codigoIbge", filtro.codigoIbge));

		}

		if (filtro.tipoPessoaVinculada != null) {

			empreendimentoComTipoPessoaVinculadaFilters(crit, filtro);

		} else {

			empreendimentoSemTipoPessoaVinculadaFilters(crit, filtro);

		}

	}

	private static void empreendimentoComTipoPessoaVinculadaFilters(Criteria crit, FiltroEmpreendimento filtro) {

		if (filtro.tipoPessoaVinculada.tipo.equals(FiltroEmpreendimento.TipoPessoaVinculada.PROPRIETARIO.tipo)) {

			proprietarioFilters(crit, filtro);

		}

		if (filtro.tipoPessoaVinculada.tipo.equals(FiltroEmpreendimento.TipoPessoaVinculada.REPRESENTANTE_LEGAL.tipo)) {

			representanteLegalFilters(crit, filtro);

		}

		if (filtro.tipoPessoaVinculada.tipo.equals(FiltroEmpreendimento.TipoPessoaVinculada.RESPONSAVEL_LEGAL.tipo)) {

			responsavelLegalFilters(crit, filtro);

		}

		if (filtro.tipoPessoaVinculada.tipo.equals(FiltroEmpreendimento.TipoPessoaVinculada.RESPONSAVEL_TECNICO.tipo)) {

			responsavelTecnicoFilters(crit, filtro);

		}

	}

	private static void empreendimentoSemTipoPessoaVinculadaFilters(Criteria crit, FiltroEmpreendimento filtro) {

		if (filtro.cpfCnpjPessoaVinculada != null || filtro.nomePessoaVinculada != null) {

			crit.createAlias("proprietarios", "prop", JoinType.LEFT_OUTER_JOIN);
			crit.createAlias("representantesLegais", "repl", JoinType.LEFT_OUTER_JOIN);
			crit.createAlias("responsaveisLegais", "resl", JoinType.LEFT_OUTER_JOIN);
			crit.createAlias("responsaveisTecnicos", "rest", JoinType.LEFT_OUTER_JOIN);

		}

		if (filtro.cpfCnpjPessoaVinculada != null) {

			Criterion cpfProprietario = Restrictions.eq("prop.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion cnpjProprietario = Restrictions.eq("prop.cnpj", filtro.cpfCnpjPessoaVinculada.replaceAll("[.\\-\\/]", ""));
			Criterion proprietario = Restrictions.or(cpfProprietario, cnpjProprietario);
			Criterion representanteLegal = Restrictions.eq("repl.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion responsavelLegal = Restrictions.eq("resl.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion cpfResponsavelTecnico = Restrictions.eq("rest.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion cnpjResponsavelTecnico = Restrictions.eq("rest.cnpj", filtro.cpfCnpjPessoaVinculada.replaceAll("[.\\-\\/]", ""));
			Criterion responsavelTecnico = Restrictions.or(cpfResponsavelTecnico, cnpjResponsavelTecnico);
			LogicalExpression orExp = Restrictions.or(proprietario, Restrictions.or(representanteLegal, Restrictions.or(responsavelLegal, responsavelTecnico)));
			crit.add(orExp);

		}

		if (filtro.nomePessoaVinculada != null) {

			Criterion nomeProprietario = Restrictions.eq("prop.nome", filtro.nomePessoaVinculada);
			Criterion razaoSocialProprietario = Restrictions.eq("prop.razaoSocial", filtro.nomePessoaVinculada);
			Criterion proprietario = Restrictions.or(nomeProprietario, razaoSocialProprietario);
			Criterion representanteLegal = Restrictions.eq("repl.nome", filtro.nomePessoaVinculada);
			Criterion responsavelLegal = Restrictions.eq("resl.nome", filtro.nomePessoaVinculada);
			Criterion nomeResponsavelTecnico = Restrictions.eq("rest.nome", filtro.nomePessoaVinculada);
			Criterion razaoSocialResponsavelTecnico = Restrictions.eq("rest.razaoSocial", filtro.nomePessoaVinculada);
			Criterion responsavelTecnico = Restrictions.or(nomeResponsavelTecnico, razaoSocialResponsavelTecnico);
			LogicalExpression orExp = Restrictions.or(proprietario, Restrictions.or(representanteLegal, Restrictions.or(responsavelLegal, responsavelTecnico)));
			crit.add(orExp);

		}

	}

	private static void proprietarioFilters(Criteria crit, FiltroEmpreendimento filtro) {

		crit.createAlias("proprietarios", "prop");

		if (filtro.cpfCnpjPessoaVinculada != null) {

			Criterion cpf = Restrictions.eq("prop.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion cnpj = Restrictions.eq("prop.cnpj", filtro.cpfCnpjPessoaVinculada.replaceAll("[.\\-\\/]", ""));
			crit.add(Restrictions.or(cpf, cnpj));

		}

		if (filtro.nomePessoaVinculada != null) {

			Criterion nome = Restrictions.eq("prop.nome", filtro.nomePessoaVinculada);
			Criterion razaoSocial = Restrictions.eq("prop.razaoSocial", filtro.nomePessoaVinculada);
			crit.add(Restrictions.or(nome, razaoSocial));

		}

	}

	private static void representanteLegalFilters(Criteria crit, FiltroEmpreendimento filtro) {

		crit.createAlias("representantesLegais", "repl");

		if (filtro.cpfCnpjPessoaVinculada != null) {

			Criterion cpf = Restrictions.eq("repl.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			crit.add(cpf);

		}

		if (filtro.nomePessoaVinculada != null) {

			Criterion nome = Restrictions.eq("repl.nome", filtro.nomePessoaVinculada);
			crit.add(nome);

		}

	}

	private static void responsavelLegalFilters(Criteria crit, FiltroEmpreendimento filtro) {

		crit.createAlias("responsaveisLegais", "resl");

		if (filtro.cpfCnpjPessoaVinculada != null) {

			Criterion cpf = Restrictions.eq("resl.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			crit.add(cpf);

		}

		if (filtro.nomePessoaVinculada != null) {

			Criterion nome = Restrictions.eq("resl.nome", filtro.nomePessoaVinculada);
			crit.add(nome);

		}

	}

	private static void responsavelTecnicoFilters(Criteria crit, FiltroEmpreendimento filtro) {

		crit.createAlias("responsaveisTecnicos", "rest");

		if (filtro.cpfCnpjPessoaVinculada != null) {

			Criterion cpf = Restrictions.eq("rest.cpf", filtro.cpfCnpjPessoaVinculada.replaceAll("[.-]", ""));
			Criterion cnpj = Restrictions.eq("rest.cnpj", filtro.cpfCnpjPessoaVinculada.replaceAll("[.\\-\\/]", ""));
			crit.add(Restrictions.or(cpf, cnpj));

		}

		if (filtro.nomePessoaVinculada != null) {

			Criterion nome = Restrictions.eq("rest.nome", filtro.nomePessoaVinculada);
			Criterion razaoSocial = Restrictions.eq("rest.razaoSocial", filtro.nomePessoaVinculada);
			crit.add(Restrictions.or(nome, razaoSocial));

		}

	}


	public Empreendimento remove() {

		this.salvarHistorico(null);

		if (this.removido) {

			throw new ValidationException().userMessage("empreendimento.removido.erro");

		}

		// TODO lembrar de retornar todos relacionamentos para o modulo de auditoria, quando o mesmo for integrado ao sistema.

		this.pessoa = null;
		this.proprietarios = null;
		this.responsaveisLegais = null;
		this.representantesLegais = null;
		this.responsaveisTecnicos = null;

		this.removido = true;

		return super.save();

	}

	private boolean collectionBlank(Collection collection) {
		return collection == null || collection.isEmpty();
	}

	public Empreendimento update(Empreendimento empreendimento, String login) {

		if (!empreendimento.proprietarios.isEmpty()) {

			empreendimento.proprietarios.forEach(proprietario -> {

				if (proprietario.id == null) {

					proprietario.enderecos = Endereco.formatarEndereco(proprietario.enderecos);
					proprietario.updateOrCreate(false);
				}
			});

		}
		if (!empreendimento.representantesLegais.isEmpty()) {

			empreendimento.representantesLegais.forEach(representanteLegal -> {

				if (representanteLegal.id == null) {

					representanteLegal.enderecos = Endereco.formatarEndereco(representanteLegal.enderecos);
					representanteLegal.updateOrCreate(false);
				}
			});

		}

		if (!empreendimento.responsaveisLegais.isEmpty()) {

			empreendimento.responsaveisLegais.forEach(responsavelLegal -> {

				if (responsavelLegal.id == null) {

					responsavelLegal.enderecos = Endereco.formatarEndereco(responsavelLegal.enderecos);
					responsavelLegal.updateOrCreate(false);
				}
			});

		}

		if (!empreendimento.responsaveisTecnicos.isEmpty()) {

			empreendimento.responsaveisTecnicos.forEach(responsavelTecnico -> {

				if (responsavelTecnico.id == null) {

					responsavelTecnico.enderecos = Endereco.formatarEndereco(responsavelTecnico.enderecos);
					responsavelTecnico.updateOrCreate(false);
				}
			});

		}

		this.salvarHistorico(login);

		validatePapeis(empreendimento);

		this.proprietarios.clear();
		this.proprietarios.addAll(empreendimento.proprietarios);

		this.representantesLegais.clear();
		this.representantesLegais.addAll(empreendimento.representantesLegais);

		this.responsaveisLegais.clear();
		this.responsaveisLegais.addAll(empreendimento.responsaveisLegais);

		this.responsaveisTecnicos.clear();
		this.responsaveisTecnicos.addAll(empreendimento.responsaveisTecnicos);

		ValidationUtil.validate(empreendimento);

		if (empreendimento.pessoa.tipo != this.pessoa.tipo || !empreendimento.pessoa.id.equals(this.pessoa.id)) {

			throw new ValidationException().userMessage("empreendimento.pessoa.alteracaoNaoPermitida");

		}

		Endereco.validateEnderecos(empreendimento.enderecos);
		Endereco.setEnderecos(this.enderecos, empreendimento.enderecos);

		Contato.validateContatos(empreendimento.contatos);
		Contato.setContatos(this.contatos, empreendimento.contatos);

		if (this.empreendedor != null) {
			Endereco.updateEnderecos(empreendimento.empreendedor.pessoa.enderecos);
			Contato.setContatos(this.empreendedor.pessoa.contatos, empreendimento.empreendedor.pessoa.contatos);

			Empreendedor emp = Empreendedor.findByCpfCnpj(this.empreendedor.pessoa.getCpfCnpj());

			if (emp != null) {

				this.empreendedor = emp;
			}

			this.empreendedor.ativo = true;
			this.empreendedor._save();

		} else {
			this.empreendedor = empreendimento.empreendedor;
		}

		this.localizacao.setAttributes(empreendimento.localizacao);
		this.dataAtualizacao = empreendimento.dataCadastro;
		this.denominacao = empreendimento.denominacao;
		this.cpfCnpjCadastrante = empreendimento.cpfCnpjCadastrante;
		this.removido = empreendimento.removido;

//		this.responsaveisLegais = this.collectionBlank(empreendimento.responsaveisLegais) ? new HashSet<>() : new HashSet<>(empreendimento.responsaveisLegais);
//		this.representantesLegais = this.collectionBlank(empreendimento.representantesLegais) ? new HashSet<>() : new HashSet<>(empreendimento.representantesLegais);
//		this.responsaveisTecnicos = this.collectionBlank(empreendimento.responsaveisTecnicos) ? new HashSet<>() : new HashSet<>(empreendimento.responsaveisTecnicos);
//		this.proprietarios = this.collectionBlank(empreendimento.proprietarios) ? new HashSet<>() : new HashSet<>(empreendimento.proprietarios);

		return super.save();

	}

	public static void validateEmpreendimento(Pessoa pessoa) {

		if (Empreendimento.count("pessoa.id", pessoa.id) > 0) {

			throw new ValidationException().userMessage("pessoa.removido.possuiEmpreendimento");

		}

	}

	public static void unlinkPessoa(Pessoa pessoa) {

		unlinkProprietario(pessoa);
		unlinkResponsavelTecnico(pessoa);
		unlinkResponsavelLegal(pessoa);
		unlinkRepresentanteLegal(pessoa);

	}

	private static void unlinkProprietario(Pessoa pessoa) {

		if (pessoa.proprietarioEmpreendimentos != null) {

			for (Empreendimento empreendimento : pessoa.proprietarioEmpreendimentos) {

				empreendimento.proprietarios.remove(pessoa);

				empreendimento._save();

			}

		}

	}

	private static void unlinkResponsavelTecnico(Pessoa pessoa) {

		if (pessoa.responsavelTecnicoEmpreendimentos != null) {

			for (Empreendimento empreendimento : pessoa.responsavelTecnicoEmpreendimentos) {

				empreendimento.responsaveisTecnicos.remove(pessoa);

				empreendimento._save();

			}

		}

	}

	private static void unlinkResponsavelLegal(Pessoa pessoa) {

		if (pessoa.responsavelLegalEmpreendimentos != null) {

			for (Empreendimento empreendimento : pessoa.responsavelLegalEmpreendimentos) {

				empreendimento.responsaveisLegais.remove(pessoa);

				empreendimento._save();

			}

		}

	}

	private static void unlinkRepresentanteLegal(Pessoa pessoa) {

		if (pessoa.representanteLegalEmpreendimentos != null) {

			for (Empreendimento empreendimento : pessoa.representanteLegalEmpreendimentos) {

				empreendimento.representantesLegais.remove(pessoa);

				empreendimento._save();

			}

		}

	}

	private void validatePapeis(Empreendimento empreendimento) {

		((Session) JPA.em().getDelegate()).disableFilter("entityRemovida");

		Set<Integer> ids = new HashSet<>();

		if (empreendimento.proprietarios != null) {

			ids.addAll(ListUtil.getIds(empreendimento.proprietarios));

		}

		if (empreendimento.responsaveisLegais != null) {

			ids.addAll(ListUtil.getIds(empreendimento.responsaveisLegais));

		}

		if (empreendimento.representantesLegais != null) {

			ids.addAll(ListUtil.getIds(empreendimento.representantesLegais));

		}

		if (empreendimento.responsaveisTecnicos != null) {

			ids.addAll(ListUtil.getIds(empreendimento.responsaveisTecnicos));

		}

		if (!ids.isEmpty()) {

			List<Pessoa> pessoas = Pessoa.find("select p from " + Pessoa.class.getSimpleName() + " p where p.id in (:ids) and p.removido = true")
					.setParameter("ids", ids)
					.fetch();

			if (pessoas.size() > 0) {

				throw new ValidationException().userMessage("empreendimento.cadastro.pessoas.removidas");

			}

		}

	}

	private void validateGeometria(Empreendimento empreendimento) {

		if (!empreendimento.localizacao.geometria.isValid()) {

			throw new ValidationException().userMessage("localizacao.geometria.erro");

		}
		;

	}

	public static Set<Empreendimento> setPessoaPapeis(Pessoa pessoa) {

		Set<Empreendimento> empreendimentosPessoa = new HashSet<>();

		empreendimentosPessoa.addAll(pessoa.proprietarioEmpreendimentos);
		empreendimentosPessoa.addAll(pessoa.responsavelTecnicoEmpreendimentos);
		empreendimentosPessoa.addAll(pessoa.representanteLegalEmpreendimentos);
		empreendimentosPessoa.addAll(pessoa.responsavelLegalEmpreendimentos);

		for (Empreendimento empreendimento : empreendimentosPessoa) {

			if ((empreendimento.proprietarios != null) && (empreendimento.proprietarios.contains(pessoa))) {

				empreendimento.pessoaIsProprietario = true;

			}

			if ((empreendimento.responsaveisTecnicos != null) && (empreendimento.responsaveisTecnicos.contains(pessoa))) {

				empreendimento.pessoaIsResponsavelTecnico = true;

			}

			if ((empreendimento.representantesLegais != null) && (empreendimento.representantesLegais.contains(pessoa))) {

				empreendimento.pessoaIsRepresentanteLegal = true;

			}

			if ((empreendimento.responsaveisLegais != null) && (empreendimento.responsaveisLegais.contains(pessoa))) {

				empreendimento.pessoaIsResponsavelLegal = true;

			}

		}

		return empreendimentosPessoa;

	}

	/**
	 * Proprietários a partir dos dados da Rede Simples
	 *
	 * @param dadosRedesimVo
	 * @return
	 */
	private Set<Pessoa> proprietariosFromRedeSimples(DadosRedesimVo dadosRedesimVo) {

		if (dadosRedesimVo.getSocios() == null) {
			return null;
		}

		// Proprietários
		Set<Pessoa> proprietarios = new HashSet<>();
		List<SocioVo> socioVos = dadosRedesimVo.getSocios().getSocio();
		Config.RedeSimplesLogger.trace("Quantidade de socios: " + socioVos.size());

		socioVos.forEach(s -> {

			if (StringUtils.isNotEmpty(s.getCnpjCpfSocio())) {
				Config.RedeSimplesLogger.trace("Tipo " + s.getIdentificadorTipoSocio());

				if (s.getEnderecoSocio() == null || s.getEnderecoSocio().getCodMunicipio() == null) {
					s.setEnderecoSocio(dadosRedesimVo.getEndereco());
					s.getEnderecoSocio().setComplemento("Endereço cadastrado do empreendimento");
					logRedeSimplesSocio(s);
				}
				// CPF
				if (s.getIdentificadorTipoSocio().equals(IdentificadorTipoPessoaEnum.PF)) {

					PessoaFisica pf = PessoaFisica.findByCpf(s.getCnpjCpfSocio());

					if (pf == null) {

						pf = new PessoaFisica(s);

						pf.preencheEnderecosInvalidos(dadosRedesimVo);

						pf.save();
					}

					proprietarios.add(pf);
				}
				// CNPJ
				else if (s.getIdentificadorTipoSocio().equals(IdentificadorTipoPessoaEnum.PJ)) {

					PessoaJuridica pj = PessoaJuridica.findByCnpj(s.getCnpjCpfSocio());

					if (pj == null) {

						pj = new PessoaJuridica(s);

						pj.preencheEnderecosInvalidos(dadosRedesimVo);

						pj.save();
					}

					proprietarios.add(pj);
				}
			}
			Config.RedeSimplesLogger.trace("Fim Socio " + s.getCnpjCpfSocio());
		});

		return proprietarios;
	}

	private void logRedeSimplesSocio(SocioVo socio) {
		Config.RedeSimplesLogger.trace("############ Dados do Socio ");
		Config.RedeSimplesLogger.trace("Nome " + isNullObject(socio.getNome()));
		Config.RedeSimplesLogger.trace("CPF/CNPJ " + isNullObject(socio.getCnpjCpfSocio()));
		Config.RedeSimplesLogger.trace("Profissão " + isNullObject(socio.getProfissao()));
		Config.RedeSimplesLogger.trace("Sexo " + isNullObject(socio.getSexo()));
		Config.RedeSimplesLogger.trace("Contato social " + isNullObject(socio.getContatoSocio()));
		Config.RedeSimplesLogger.trace("Pais " + isNullObject(socio.getEnderecoSocio().getCodPais()));
		Config.RedeSimplesLogger.trace("Endereco exterior " + isNullObject(socio.getEnderecoSocio().getEnderecoExterior()));
		Config.RedeSimplesLogger.trace("Capital social " + isNullObject(socio.getCapitalSocialSocio()));
		Config.RedeSimplesLogger.trace("Nacionalidade " + isNullObject(socio.getNacionalidade()));
		Config.RedeSimplesLogger.trace("############ Dados do Socio -- Fim");
	}

	private Set<PessoaFisica> representantesLegaisFromRedeSimples(DadosRedesimVo dadosRedesimVo) {

		if (dadosRedesimVo.getRepresentantesLegais() == null) {
			return null;
		}

		// Representantes legais
		Set<PessoaFisica> representantesLegais = new HashSet<>();
		dadosRedesimVo.getRepresentantesLegais().getRepresentanteLegal().forEach(r -> {

			if (StringUtils.isNotEmpty(r.getCpf())) {

				PessoaFisica pf = PessoaFisica.findByCpf(r.getCpf());

				if (pf == null) {
					if (r.getEndereco() == null || r.getEndereco().getCodMunicipio() == null) {
						r.setEndereco(dadosRedesimVo.getEndereco());
						r.getEndereco().setComplemento("Endereço cadastrado do empreendimento");
					}
					pf = new PessoaFisica(r);

					pf.preencheEnderecosInvalidos(dadosRedesimVo);

					pf.save();
				}

				representantesLegais.add(pf);
			}
		});

		return representantesLegais;
	}

	private Set<PessoaFisica> responsavelLegalFromRedeSimples(DadosRedesimVo dadosRedesimVo) {

		ResponsavelPeranteCnpjVo responsavelPeranteCnpjVo = dadosRedesimVo.getResponsavelPeranteCnpj();

		if (responsavelPeranteCnpjVo == null || this.proprietarios == null)
			return null;

		Set<PessoaFisica> responsaveisLegais = new HashSet<>();

		// Localiza o responsável perante cnpj na lista de proprietários,
		// por regra, na rede simples, um responsável perante o cnpj sempre é um sócio.
		for (Pessoa p : this.proprietarios) {

			if (p.tipo.equals(TipoPessoa.FISICA)) {

				PessoaFisica pf = (PessoaFisica) p;

				if (pf.cpf.equals(responsavelPeranteCnpjVo.getCpfResponsavel())) {
					responsaveisLegais.add(pf);
				}
			}
		}

		return responsaveisLegais;
	}

	private void preencherClassificacaoEmpreendimento(String codNaturezaJuridica) {

		ClassificacaoEmpreendimento classificacaoEmpreendimento = null;
		if (codNaturezaJuridica == null) {
			classificacaoEmpreendimento = ClassificacaoEmpreendimento.findById(0);
		} else {
			classificacaoEmpreendimento = ClassificacaoEmpreendimento.findById(Integer.parseInt(codNaturezaJuridica));
		}

		if (classificacaoEmpreendimento == null) {
			throw new ValidationException().userMessage("empreendimento.cadastro.classificacaoEmpreendimento.naoEncontrado");
		} else {
			this.classificacaoEmpreendimento = classificacaoEmpreendimento;
		}
	}

	public void salvarHistorico(String login) {

		EmpreendimentoHistorico eh = new EmpreendimentoHistorico(this);

		Pessoa pessoa = null;

		if (login != null) {
			pessoa = Pessoa.findByCpfCnpj(login);

			if (pessoa == null) {
				throw new ValidationException().userMessage("empreendimento.editar.pessoaModificadora.naoEncontrada");
			}
		}

		eh.pessoaModificadora = pessoa;
		eh.dataHistorico = new Date();
		eh.localizacao.geometria.setSRID(4674);


		eh.save();
	}

	private static String isNullObject(Object o) {
		return o != null ? o.toString() : "N/A";
	}

	public static EmpreendimentoSobreposicao intersects(Integer idEmpreendimento) {

		Empreendimento empreendimento = findById(idEmpreendimento);
		EmpreendimentoSobreposicao empreendimentoSobreposicao = new EmpreendimentoSobreposicao();
		List<Empreendimento> emps = getEmpreendimentosSobrepostos(empreendimento, 10);

		empreendimentoSobreposicao.sobreposicoes = emps.stream().map(EmpreendimentoSobreposicaoVO::new).collect(Collectors.toList());

		return empreendimentoSobreposicao;
	}

	public static EmpreendimentoSobreposicao intersects(String cpfCnpj) {

		FiltroEmpreendimento filtroEmpreendimento = new FiltroEmpreendimento();
		filtroEmpreendimento.cpfsCnpjs = Collections.singletonList(cpfCnpj);
		filtroEmpreendimento.ordenacao = FiltroEmpreendimento.Ordenacao.DENOMINACAO_ASC;

		List<Empreendimento> empreendimentos = findByFilter(filtroEmpreendimento);

		EmpreendimentoSobreposicao empreendimentoSobreposicao = new EmpreendimentoSobreposicao();

		if (empreendimentos == null || empreendimentos.isEmpty()) {
			empreendimentoSobreposicao.sobreposicoes = Collections.emptyList();
			return empreendimentoSobreposicao;
		}

		List<Empreendimento> emps = getEmpreendimentosSobrepostos(empreendimentos.get(0), 10);

		empreendimentoSobreposicao.sobreposicoes = emps.stream().map(EmpreendimentoSobreposicaoVO::new)
				.collect(Collectors.toList());

		return empreendimentoSobreposicao;
	}

	public static List<EmpreendimentoSobreposicaoVO> intersects(Geometry geometry) {

		List<Empreendimento> emps = getEmpreendimentosSobrepostos(geometry, 10);

		return emps.stream().map(EmpreendimentoSobreposicaoVO::new).collect(Collectors.toList());
	}

	private static List<Empreendimento> getEmpreendimentosSobrepostos(Empreendimento emp, Integer limit) {

		Endereco endereco = emp.enderecos.stream().filter(end -> end.tipo.id.equals(TipoEndereco.ID_PRINCIPAL))
				.findFirst().orElse(null);

		String sql = "SELECT emp.* FROM cadastro_unificado.empreendimento emp " +
				"INNER JOIN cadastro_unificado.endereco_empreendimento ee ON emp.id = ee.id_empreendimento " +
				"INNER JOIN cadastro_unificado.endereco e ON ee.id_endereco = e.id " +
				"INNER JOIN cadastro_unificado.localizacao l ON emp.id_localizacao = l.id " +
				"WHERE e.id_tipo_endereco = :idTipoEndereco AND e.id_municipio = :idMunicipio AND NOT e.removido AND " +
				"NOT emp.removido AND emp.id <> :idEmpreendimento AND ST_intersects(ST_SetSRID(l.geometria, 4674), :geom) LIMIT :limit";

		return JPA.em().createNativeQuery(sql, Empreendimento.class)
				.unwrap(SQLQuery.class)
				.setParameter("idTipoEndereco", TipoEndereco.ID_PRINCIPAL)
				.setParameter("idMunicipio", endereco.municipio.id)
				.setParameter("idEmpreendimento", emp.id)
				.setParameter("geom", emp.localizacao.geometria)
				.setParameter("limit", limit)
				.list();
	}


	private static List<Empreendimento> getEmpreendimentosSobrepostos(Geometry geom, Integer limit) {

		String sql = "SELECT emp.* FROM cadastro_unificado.empreendimento emp " +
				"INNER JOIN cadastro_unificado.endereco_empreendimento ee ON emp.id = ee.id_empreendimento " +
				"INNER JOIN cadastro_unificado.endereco e ON ee.id_endereco = e.id " +
				"INNER JOIN cadastro_unificado.localizacao l ON emp.id_localizacao = l.id " +
				"WHERE e.id_tipo_endereco = :idTipoEndereco AND NOT e.removido AND " +
				"NOT emp.removido AND ST_intersects(l.geometria, :geom) LIMIT :limit";

		return JPA.em().createNativeQuery(sql, Empreendimento.class)
				.unwrap(SQLQuery.class)
				.setParameter("idTipoEndereco", TipoEndereco.ID_PRINCIPAL)
				.setParameter("geom", geom)
				.setParameter("limit", limit)
				.list();
	}

	public static Long countByFilter(FiltroEmpreendimento filtro) {

		Session session = (Session) JPA.em().getDelegate();
		Criteria crit = session.createCriteria(Empreendimento.class);

		crit.createAlias("pessoa", "ps");

		Disjunction disjunction = Restrictions.disjunction();
		disjunction.add(Restrictions.eq("ps.class", PessoaFisica.class));
		disjunction.add(Restrictions.eq("ps.class", PessoaJuridica.class));

		crit.add(disjunction);

		crit.createAlias("enderecos", "en");
		crit.createAlias("en.municipio", "mun").add(Restrictions.eq("en.tipo.id", TipoEndereco.ID_PRINCIPAL));

		if(filtro.cpfCnpj != null){
			Criterion cpfCnpjCadastrante = Restrictions.like("cpfCnpjCadastrante", filtro.cpfCnpj);
			crit.add(cpfCnpjCadastrante);
		}

		if (filtro.busca != null) {

			if (filtro.busca.equals("%")) {

				return 0l;

			}

			if (filtro.busca.equals(filtro.cpfCnpjEmpreendedor)) {

				crit.createAlias("empreendedor", "emp");
				crit.createAlias("emp.pessoa", "pessoaEmp");

				Criterion cpf = Restrictions.like("pessoaEmp.cpf", filtro.cpfCnpjEmpreendedor);
				Criterion cnpj = Restrictions.like("pessoaEmp.cnpj", filtro.cpfCnpjEmpreendedor);
				crit.add(Restrictions.or(cpf, cnpj));
			}

			if (filtro.cpfsCnpjs != null && filtro.cpfsCnpjs.size() > 0) {

				Criterion cpf = Restrictions.in("ps.cpf", filtro.cpfsCnpjs);
				Criterion cnpj = Restrictions.in("ps.cnpj", filtro.cpfsCnpjs);
				crit.add(Restrictions.or(cpf, cnpj));

			}

			if (filtro.filtrarCadastro == null && !filtro.busca.equals(filtro.cpfCnpjEmpreendedor)) {

				crit.createAlias("empreendedor", "emp");
				crit.createAlias("emp.pessoa", "pessoaEmp");

				if(filtro.cpfCnpjEmpreendedor != null) {
					Criterion cpfEmp = Restrictions.eq("pessoaEmp.cpf", filtro.cpfCnpjEmpreendedor);
					Criterion cnpjEmp = Restrictions.eq("pessoaEmp.cnpj", filtro.cpfCnpjEmpreendedor);
					crit.add(Restrictions.and(Restrictions.or(cpfEmp, cnpjEmp)));
				}

				Criterion cpf = Restrictions.like("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
				Criterion cnpj = Restrictions.like("ps.cnpj", "%" + filtro.busca.replaceAll("[.\\-\\/]", "") + "%");
				String parametro = Normalizer.normalize(filtro.busca, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				Criterion denominacao = Restrictions.sqlRestriction("f_remove_caracter_especial({alias}.denominacao) ILIKE f_remove_caracter_especial(?)", "%" + parametro + "%", new StringType());

				Criterion empreendedorNome = Restrictions.ilike("pessoaEmp.nome", "%" + filtro.busca + "%");
				Criterion empreendedorRazao = Restrictions.ilike("pessoaEmp.razaoSocial", "%" + filtro.busca + "%");
				Criterion municipio = Restrictions.ilike("mun.nome", "%" + filtro.busca + "%");

				LogicalExpression empreendimentoNomeExp = Restrictions.or(empreendedorNome, empreendedorRazao);

				LogicalExpression orExp = Restrictions.or(cpf, cnpj);

				LogicalExpression orExp1 = Restrictions.or(empreendimentoNomeExp, denominacao);

				LogicalExpression orExp2 = Restrictions.or(orExp1, orExp);

				LogicalExpression orExp3 = Restrictions.or(orExp2, municipio);

				crit.add(orExp3);

			} else {
				if (filtro.cpfCnpjEmpreendedor == null) {
					Criterion cpf = Restrictions.like("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
					Criterion cnpj = Restrictions.like("ps.cnpj", "%" + filtro.busca.replaceAll("[.\\-\\/]", "") + "%");
					String parametro = Normalizer.normalize(filtro.busca, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
					Criterion denominacao = Restrictions.sqlRestriction("f_remove_caracter_especial({alias}.denominacao) ILIKE f_remove_caracter_especial(?)", "%" + parametro + "%", new StringType());

					LogicalExpression orExp = Restrictions.or(Restrictions.or(cpf, cnpj), denominacao);

					crit.add(orExp);
				}
			}

		} else {

			empreendimentoFilters(crit, filtro);

		}


		if (filtro.ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

			crit.addOrder(Order.asc(filtro.ordenacao.getField()));

		} else {

			crit.addOrder(Order.desc(filtro.ordenacao.getField()));

		}
		return Long.valueOf(crit.list().size());

	}


}
