package models;

public interface Tipo {

	String name();
	Integer getCodigo();
	String getDescricao();

}