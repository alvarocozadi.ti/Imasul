package models;

import play.db.jpa.GenericModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado", name = "estado")
public class Estado extends GenericModel {

	@Id
	public Integer id;
	public String nome;
	public String sigla;

	@ManyToOne
	@JoinColumn(name = "id_pais", referencedColumnName = "id")
	public Pais pais;

	@OrderBy(value = "nome")
	@OneToMany(mappedBy = "estado")
	public Set<Municipio> municipios;

	public Estado() {
		super();
	}

	@Override
	public Object _key() {
		return this.id;
	}

}