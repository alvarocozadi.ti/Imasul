package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.DadosRedesimVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.SocioVo;
import common.models.Filter;
import common.models.Message;
import exceptions.ValidationException;
import models.integracaoSiriema.PessoaSiriemaVO;
import models.portalSeguranca.Perfil;
import models.portalSeguranca.Usuario;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import play.Logger;
import play.data.validation.CheckWith;
import play.data.validation.Required;
import play.db.jpa.JPA;
import play.mvc.Http;
import results.Error;
import services.ExternalUsuarioService;
import utils.Config;
import utils.DataUtils;
import utils.StringUtil;
import utils.ValidationUtil;
import validators.CNPJCheck;
import validators.UniqueIfNotRemoved;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado", name = "pessoa_juridica")
@PrimaryKeyJoinColumn(name = "id_pessoa", referencedColumnName = "id")
public class PessoaJuridica extends Pessoa {

	@Required(message = "pessoaJuridica.razaoSocial.obrigatoria")
	@Column(name = "razao_social")
	public String razaoSocial;

	@Column(name = "nome_fantasia")
	public String nomeFantasia;

	@UniqueIfNotRemoved(message = "pessoaJuridica.cnpj.unico")
	@CheckWith(value = CNPJCheck.class, message="pessoaJuridica.cnpj.invalido")
	@Required(message = "pessoaJuridica.cnpj.obrigatorio")
	public String cnpj;

	@Column(name = "inscricao_estadual")
	@UniqueIfNotRemoved(message="pessoaJuridica.inscricaoEstadual.unica")
	public String inscricaoEstadual;

	@Temporal(TemporalType.DATE)
	@Required(message = "pessoaJuridica.dataConstituicao.obrigatoria")
	@Column(name = "data_constituicao")
	public Date dataConstituicao;

	@Column(name = "is_junta")
	public Boolean isJunta;

	private static final String LOG_PREFIX = "[INTEGRACAO SIRIEMA] ";

	public PessoaJuridica() {

		super();

		this.tipo = TipoPessoa.JURIDICA;

	}

	public PessoaJuridica(DadosRedesimVo dadosRedesimVo) {

		super();

		// Atributos padrão
		this.tipo = TipoPessoa.JURIDICA;
		dispararLogRedeSimples(dadosRedesimVo);
		// Atributos editáveis
		this.razaoSocial = dadosRedesimVo.getNomeEmpresarial();
		this.nomeFantasia = dadosRedesimVo.getNomeFantasia();
		this.isJunta = true;
		this.cnpj = dadosRedesimVo.getCnpj();
		this.inscricaoEstadual = dadosRedesimVo.getNuInscricaoEstadual();
		this.dataConstituicao = DataUtils.dataRedeSimples(dadosRedesimVo.getDataAberturaEstabelecimento());
		this.enderecos = Endereco.enderecosRedeSimples(dadosRedesimVo.getEndereco());
		this.contatos = Contato.contatosRedeSimples(dadosRedesimVo.getContato());

	}

	public PessoaJuridica(SocioVo socioVo) {

		super(socioVo);

		// Atributos padrão
		this.tipo = TipoPessoa.JURIDICA;

		// Atributos editáveis
		this.razaoSocial = socioVo.getNome();
		this.cnpj = socioVo.getCnpjCpfSocio();
		this.dataConstituicao = DataUtils.dataRedeSimples(socioVo.getDataNascimento());
		this.isJunta = true;
	}

	public Usuario getUsuario() {

		if(this.usuario == null && this.isUsuario) {

			this.usuario = Usuario.find("login = :login AND removido = false").setParameter("login", this.cnpj).first();

			if(this.usuario != null
					&& !this.usuario.ativo
					&& this.usuario.perfis != null
					&& FiltroPessoa.ID_MODULO_REQUISITOR != null) {

				this.usuario.perfis = Perfil.findPerfisByModuloRequest(this.usuario, FiltroPessoa.ID_MODULO_REQUISITOR);

			}

		}

		return this.usuario;

	}

	@Override
	public PessoaJuridica save() {

		ValidationUtil.validate(this);

		if(this.isUsuario && this.usuario != null) {

			ValidateCnpjLogin(this);

		}

		Endereco.validateEnderecos(this.enderecos);

		Contato.validateContatos(this.contatos);

		return super.save();

	}

	public void atualizarPessoaJuridica(DadosRedesimVo dadosRedesimVo) {

		// Atributos editáveis
		this.razaoSocial = dadosRedesimVo.getNomeEmpresarial();
		this.nomeFantasia = dadosRedesimVo.getNomeFantasia();
		this.dataConstituicao = DataUtils.dataRedeSimples(dadosRedesimVo.getDataAberturaEstabelecimento());
		this.isJunta = true;
		this.enderecos.clear();
		Set<Endereco> enderecos = Endereco.enderecosRedeSimples(dadosRedesimVo.getEndereco());
		this.enderecos.addAll(enderecos);
	}

	public PessoaJuridica update(PessoaJuridica pessoaJuridica) {

		ValidationUtil.validate(pessoaJuridica);

		if(pessoaJuridica.isUsuario && pessoaJuridica.usuario != null) {

			ValidateCnpjLogin(pessoaJuridica);

		}

		Endereco.validateEnderecos(pessoaJuridica.enderecos);
		Endereco.setEnderecos(this.enderecos, pessoaJuridica.enderecos);

		Contato.validateContatos(pessoaJuridica.contatos);
		Contato.setContatos(this.contatos, pessoaJuridica.contatos);

		this.isUsuario = pessoaJuridica.isUsuario;
		this.razaoSocial = pessoaJuridica.razaoSocial;
		this.nomeFantasia = pessoaJuridica.nomeFantasia;
		this.inscricaoEstadual = pessoaJuridica.inscricaoEstadual;
		this.dataConstituicao = pessoaJuridica.dataConstituicao;

		return super.save();

	}

	public PessoaJuridica remove() {

		if(this.removido) {

			throw new ValidationException().userMessage("pessoaJuridica.removido.erro");

		}

		Empreendimento.validateEmpreendimento(this);
		Empreendimento.unlinkPessoa(this);

		if(this.isUsuario) {

			ExternalUsuarioService.remove(this.cnpj);

		}

		this.removido = true;

		return super.save();

	}

	public static List<PessoaJuridica> findByFilter(FiltroPessoaJuridica filtro) {

		Criteria crit = ((Session) JPA.em().getDelegate()).createCriteria(PessoaJuridica.class);
		crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
		crit.setMaxResults(filtro.tamanhoPagina);

		if((filtro.filtroSimplificado != null && filtro.filtroSimplificado.equals("%"))
				|| (filtro.razaoSocial != null && filtro.razaoSocial.equals("%"))
				|| (filtro.cnpj != null && filtro.cnpj.equals("%"))) {

			return new ArrayList<>();

		}

		filter(crit, filtro);

		Filter.addBaseRestrictionsAndOrder(crit, filtro);

		return crit.list();

	}

	public static Long countByFilter(FiltroPessoaJuridica filtro) {

		Criteria crit = ((Session) JPA.em().getDelegate()).createCriteria(PessoaJuridica.class);

		if((filtro.filtroSimplificado != null && filtro.filtroSimplificado.equals("%"))
				|| (filtro.razaoSocial !=null && filtro.razaoSocial.equals("%"))
				|| (filtro.cnpj != null && filtro.cnpj.equals("%"))) {

			return 0L;

		}

		filter(crit, filtro);

		return Long.valueOf(crit.list().size());

	}

	private static void filter(Criteria crit, FiltroPessoaJuridica filtro) {

		if(filtro.filtroSimplificado != null) {

			simplifiedFilterRestrictions(crit, filtro.filtroSimplificado);

		} else {

			DetachedCriteria critUsuario = null;

			if(filtro.filtroUsuario != null) {

				critUsuario = DetachedCriteria.forClass(Usuario.class).setProjection(org.hibernate.criterion.Property.forName("login"));
				crit.add(org.hibernate.criterion.Property.forName("cnpj").in(critUsuario));

			}

			advancedFilterRestrictions(crit, critUsuario, filtro);

		}

	}

	private static void advancedFilterRestrictions(Criteria crit, DetachedCriteria critUsuario, FiltroPessoaJuridica filtro) {

		if(filtro.cnpj != null) {

			crit.add(Restrictions.eq("cnpj", filtro.cnpj.replaceAll("[.\\-\\/]", "")));

		}

		if(filtro.razaoSocial != null) {

			crit.add(Restrictions.sqlRestriction("f_remove_caracter_especial(upper(razao_social)) like f_remove_caracter_especial(upper(?))", "%" + filtro + "%", new StringType()));

		}

		Filter.userRestrictions(critUsuario, filtro);

	}

	private static void simplifiedFilterRestrictions(Criteria crit, String filtro) {

		Criterion cnpj = Restrictions.eq("cnpj", filtro.replaceAll("[.\\-\\/]", ""));
		Criterion razaoSocial = Restrictions.sqlRestriction("f_remove_caracter_especial(upper(razao_social)) like f_remove_caracter_especial(upper(?))", "%" + filtro + "%", new StringType());
		LogicalExpression orExp = Restrictions.or(cnpj, razaoSocial);
		crit.add(orExp);

	}

	public void validateToCreate(DadosValidacao dadosValidacao) {

		if(this.bloqueado) {

			throw new ValidationException().userMessage("dadosValidacao.tentativas.excedido");

		}

		String nomeMunicipio = null;

		for(Endereco endereco : this.enderecos) {

			if(endereco.tipo.id.equals(TipoEndereco.ID_PRINCIPAL)) {

				nomeMunicipio = endereco.municipio.nome;

			}

		}

		if(!this.razaoSocial.equals(dadosValidacao.razaoSocial)
				|| !nomeMunicipio.equals(dadosValidacao.nomeMunicipio)
				|| !this.cnpj.equals(dadosValidacao.cnpj)
				|| !DateUtils.isSameDay(this.dataConstituicao, dadosValidacao.dataConstituicao)) {

			BloqueioPessoa bloqueioPessoa = BloqueioPessoa.addNumeroTentativas(this);

			commitTransaction();

			if(bloqueioPessoa.numeroTentativas.equals(BloqueioPessoa.numeroMaxTentativas + 1)) {

				this.bloqueado = true;

				super.save();

				bloqueioPessoa.dataCadastro = new Date();

				bloqueioPessoa.save();

				commitTransaction();

				throw new Error(Http.StatusCode.FORBIDDEN, new Message("dadosValidacao.tentativas.excedido").getText());

			}

			throw new ValidationException().userMessage("dadosValidacao.validar.erro");

		}

	}

	public static PessoaJuridica findByCnpj(String cnpj) {

		return PessoaJuridica.find("cnpj = :cnpj AND removido = :removido")
				.setParameter("cnpj", cnpj.replaceAll("[.-]", ""))
				.setParameter("removido", false)
				.first();

	}

	public static PessoaJuridica findByPessoa(Pessoa pessoa) {

		return PessoaJuridica.find("pessoa = :pessoa").setParameter("pessoa", pessoa).first();

	}

	public static Long countByCnpj(String cnpj) {

		return PessoaJuridica.count("cnpj", cnpj.replaceAll("[.-]", ""));

	}

	public void ValidateCnpjLogin(PessoaJuridica pessoajuridica) {

		if(!pessoajuridica.cnpj.equals(pessoajuridica.usuario.login)) {

			throw new ValidationException().userMessage("pessoaJuridica.cnpj.login");

		}

	}

	private void dispararLogRedeSimples(DadosRedesimVo dadosRedesimVo){
		Config.RedeSimplesLogger.trace(" == Dados PJ Rede simples");
		Config.RedeSimplesLogger.trace(" ==== Razao social " + dadosRedesimVo.getNomeEmpresarial());
		Config.RedeSimplesLogger.trace(" ==== Nome Fantasia " + dadosRedesimVo.getNomeFantasia());
		Config.RedeSimplesLogger.trace(" ==== CNPJ " + dadosRedesimVo.getCnpj());
		Config.RedeSimplesLogger.trace(" ==== Inscrição estadual " + dadosRedesimVo.getNuInscricaoEstadual());
		Config.RedeSimplesLogger.trace(" ==== Data constituição " + DataUtils.dataRedeSimples(dadosRedesimVo.getDataAberturaEstabelecimento()));

	}

	public boolean createPessoaJuridica(boolean isIntegracao, PessoaSiriemaVO pessoaSiriemaVO) {

		try {
			Logger.info(LOG_PREFIX + " - Processando Pessoa Jurídica com CNPJ - " + pessoaSiriemaVO.cnpj);

			this.contatos = Pessoa.formataContatoSiriema(pessoaSiriemaVO.contato);
			this.enderecos = Pessoa.formataEnderecoSiriema(pessoaSiriemaVO.endereco);
			this.dataAtualizacao = DataUtils.formataData(pessoaSiriemaVO.dataUltimaAlteracao);
			this.tipo = TipoPessoa.JURIDICA;
			this.cnpj = pessoaSiriemaVO.cnpj.replaceAll("[.\\-\\/]", "");
			this.razaoSocial = StringUtil.stringVazia(pessoaSiriemaVO.razaoSocial) ? "Sem razão social informada" : pessoaSiriemaVO.razaoSocial;
//                pessoaJuridica.dataConstituicao = DataUtils.formataData(this.dataInicioAtividade != null ? this.dataInicioAtividade : "01/02/1999");
			this.dataConstituicao = DataUtils.formataData(pessoaSiriemaVO.dataInicioAtividade);
			this.isJunta = false;

//			if(!isIntegracao)
				this.isUsuario = true;

			return true;

		}catch (Exception e){
			Logger.info(LOG_PREFIX + " - [FALHA AO SALVAR PESSOA JURÍDICA] - Erro ao cadastrar a pessoa de CNPJ " + pessoaSiriemaVO.cnpj + " - Problema com dados cadastrais.");
			e.printStackTrace();
			return false;
		}
	}

}
