package models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import play.data.validation.CheckWith;
import play.data.validation.Required;
import validators.TituloEleitoralCheck;

@Embeddable
public class TituloEleitoral {

	@Required(message = "tituloEleitoral.numero.obrigatorio")
	@CheckWith(value=TituloEleitoralCheck.class, message="tituloEleitoral.numero.invalido")
	@Column(name = "numero_titulo_eleitoral")
	public String numero;

	@Column(name = "zona_titulo_eleitoral")
	public String zona;

	@Column(name = "secao_titulo_eleitoral")
	public String secao;

	public TituloEleitoral() {

		super();

	}

}
