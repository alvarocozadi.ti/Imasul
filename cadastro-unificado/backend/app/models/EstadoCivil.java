package models;


public enum EstadoCivil implements Tipo {

	SOLTEIRO(0, "Solteiro(a)"),
	CASADO(1, "Casado(a)"),
	DIVORCIADO(2, "Divorciado(a)"),
	VIUVO(3, "Viúvo(a)"),
	SEPARADO(4, "Separado(a)"),
	UNIAO_ESTAVEL(5, "União Estável");

	private Integer codigo;
	private String descricao;

	private EstadoCivil(Integer codigo, String descricao) {

		this.codigo = codigo;
		this.descricao = descricao;

	}

	@Override
	public Integer getCodigo() {
		return this.codigo;
	}

	@Override
	public String getDescricao() {
		return this.descricao;
	}

	public static EstadoCivil getEstadoCivil(String estadoCivil){

		if (estadoCivil == null){return SOLTEIRO;}

		if (estadoCivil.equalsIgnoreCase(SOLTEIRO.descricao)){return SOLTEIRO;}

		else if (estadoCivil.equalsIgnoreCase(CASADO.descricao)){return CASADO;}

		else if (estadoCivil.equalsIgnoreCase(DIVORCIADO.descricao)){return DIVORCIADO;}

		else if (estadoCivil.equalsIgnoreCase(VIUVO.descricao)){return VIUVO;}

		else if (estadoCivil.equalsIgnoreCase(SEPARADO.descricao)){return SEPARADO;}

		else if (estadoCivil.equalsIgnoreCase(UNIAO_ESTAVEL.descricao)){return UNIAO_ESTAVEL;}

		else return null;
	}

}
