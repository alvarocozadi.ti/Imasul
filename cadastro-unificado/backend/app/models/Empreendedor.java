package models;

import common.models.Filter;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;
import org.hibernate.criterion.*;
import org.yaml.snakeyaml.emitter.Emitter;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(schema = "cadastro_unificado", name = "empreendedor")
@FilterDefs(value = {
		@FilterDef( name = "empreendedorAtivo", parameters = @ParamDef(name = "ativo", type = "boolean"), defaultCondition = "ativo = TRUE" ),
})
public class Empreendedor extends GenericModel {

	private static final String SEQ = "cadastro_unificado.empreendedor_id_seq";
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQ)
	@SequenceGenerator(name = SEQ, sequenceName = SEQ, allocationSize = 1)
	public Long id;
	
	@Required
	@ManyToOne
	@JoinColumn(name = "id_pessoa", referencedColumnName = "id")
	public Pessoa pessoa;
	
	@Column(name = "data_cadastro")
	public Date dataCadastro;

	@Column(name = "ativo")
	public boolean ativo;

	@Transient
	public boolean podeSerExcluido;

	public Empreendedor() {}

	public Empreendedor(Empreendedor empreendedor) {

		this.pessoa = empreendedor.pessoa;
		this.dataCadastro = new Date();
		this.ativo = empreendedor.ativo;

	}

	@Override
	public Empreendedor save() {

		if(this.id == null) {

			this.dataCadastro = new Date();
			this.ativo = true;

			this.pessoa.updateOrCreate(false);

			this._save();

			return this;
		}

		this.pessoa.updateOrCreate(false);
		this._save();
		return this;

	}

	@Override
	public Empreendedor delete() {

		this.ativo = false;

		return super.save();

	}

	public boolean podeSerExcluido() {

		Long empreendimentosAtivosVinculados = Empreendimento
				.count("byEmpreendedorAndRemovido", this, false);

		return empreendimentosAtivosVinculados == 0;

	}

	public static Empreendedor findByCpfCnpj(String cpfCnpj){

		Pessoa p = Pessoa.findByCpfCnpj(cpfCnpj);

		Empreendedor empreendedor = Empreendedor.find("id_pessoa = :idPessoa")
				.setParameter("idPessoa", p.id).first();

		return p != null ? empreendedor : null;

	}

	public static List<Empreendedor> findByFilterBusca(FiltroEmpreendedor filtro) {

		Session session = (Session) JPA.em().getDelegate();
		Criteria crit = session.createCriteria(Empreendedor.class);

		if(filtro.contagem == null || !filtro.contagem) {
			crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
			crit.setMaxResults(filtro.tamanhoPagina);
		}

		crit.createAlias("pessoa", "ps");

		Disjunction disjunction = Restrictions.disjunction();
		disjunction.add(Restrictions.eq("ps.class", PessoaFisica.class));
		disjunction.add(Restrictions.eq("ps.class", PessoaJuridica.class));

		crit.add(disjunction);

		crit.createAlias("ps.enderecos", "en");
		crit.createAlias("en.municipio", "mun").add(Restrictions.eq("en.tipo.id", TipoEndereco.ID_PRINCIPAL));

		if(filtro.busca.equals("%")) {

			return new ArrayList<>();

		}

		Criterion empreendedorAtivo = Restrictions.eq("ativo", true);
		crit.add(empreendedorAtivo);

		if(filtro.cpfsCnpjs != null && filtro.cpfsCnpjs.size() > 0 ) {

			Criterion cpf = Restrictions.in("ps.cpf", filtro.cpfsCnpjs);
			Criterion cnpj = Restrictions.in("ps.cnpj", filtro.cpfsCnpjs);
			crit.add(Restrictions.or(cpf, cnpj));

		}

		if (filtro.filtrarCadastro == null) {

			Criterion empreendedorNome = Restrictions.ilike("ps.nome", "%" + filtro.busca + "%");
			Criterion empreendedorRazao = Restrictions.ilike("ps.razaoSocial", "%" + filtro.busca + "%");

			Criterion empreendedorCpf = Restrictions.ilike("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
			Criterion empreendedorCnpj = Restrictions.ilike("ps.cnpj", "%" + filtro.busca.replaceAll("[.-]", "") + "%");
			Criterion municipio = Restrictions.ilike("mun.nome", "%" + filtro.busca + "%");

			LogicalExpression empreendimentoNomeExp = Restrictions.or(empreendedorNome,empreendedorRazao);

			LogicalExpression orExp = Restrictions.or(empreendedorCpf, empreendedorCnpj);

			LogicalExpression orExp1 = Restrictions.or(empreendimentoNomeExp, orExp);

			LogicalExpression orExp2 = Restrictions.or(orExp1, municipio);

			crit.add(orExp2);

		} else {

			Criterion cpf = Restrictions.like("ps.cpf", "%" + filtro.busca.replaceAll("[.-]", "") + "%" );
			Criterion cnpj = Restrictions.like("ps.cnpj", "%" +filtro.busca.replaceAll("[.\\-\\/]", "")+"%");

			LogicalExpression orExp = Restrictions.or(cpf, cnpj);

			crit.add(orExp);

		}

		if(filtro.ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

			crit.addOrder(Order.asc(filtro.ordenacao.getField()));

		} else {

			crit.addOrder(Order.desc(filtro.ordenacao.getField()));

		}

		return crit.list();

	}

	public static List<Empreendedor> findByFilter(FiltroEmpreendedor filtro) {

		Session sessionEmpreendimento = (Session) JPA.em().getDelegate();
		Criteria critEmpreendimento = sessionEmpreendimento.createCriteria(Empreendedor.class);

		if(filtro.contagem == null || !filtro.contagem) {
			critEmpreendimento.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
			critEmpreendimento.setMaxResults(filtro.tamanhoPagina);
		}

		critEmpreendimento.createAlias("pessoa", "ps");

		Disjunction disjunction2 = Restrictions.disjunction();
		disjunction2.add(Restrictions.eq("ps.class", PessoaFisica.class));
		disjunction2.add(Restrictions.eq("ps.class", PessoaJuridica.class));

		critEmpreendimento.add(disjunction2);

		critEmpreendimento.createAlias("ps.enderecos", "en");
		critEmpreendimento.createAlias("en.municipio", "mun").add(Restrictions.eq("en.tipo.id", TipoEndereco.ID_PRINCIPAL));

		empreendedorFilters(critEmpreendimento, filtro);

		if(filtro.ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

			critEmpreendimento.addOrder(Order.asc(filtro.ordenacao.getField()));

		} else {

			critEmpreendimento.addOrder(Order.desc(filtro.ordenacao.getField()));

		}

		return critEmpreendimento.list();

	}

	private static Criteria empreendedorFilters(Criteria critEmpreendimento, FiltroEmpreendedor filtro) {

		if(filtro.cpfCnpj != null) {

			Criterion cpf = Restrictions.eq("ps.cpf", filtro.cpfCnpj.replaceAll("[.-]", ""));
			Criterion cnpj = Restrictions.eq("ps.cnpj", filtro.cpfCnpj.replaceAll("[.\\-\\/]", ""));

			Criterion empreendedorAtivo = Restrictions.eq("ativo", true);
			critEmpreendimento.add(empreendedorAtivo);

			critEmpreendimento.add(Restrictions.or( cpf, cnpj));

		}

		if(filtro.cpfsCnpjs != null && filtro.cpfsCnpjs.size() > 0 ) {

			Criterion cpf = Restrictions.in("ps.cpf", filtro.cpfsCnpjs);
			Criterion cnpj = Restrictions.in("ps.cnpj", filtro.cpfsCnpjs);

			Criterion empreendedorAtivo = Restrictions.eq("ativo", true);

			critEmpreendimento.add(empreendedorAtivo);
			critEmpreendimento.add(Restrictions.or(cpf, cnpj));

		}

		return critEmpreendimento;
	}

	public static Long countByFilter(FiltroEmpreendedor filtro) {

		FiltroEmpreendedor filtroContagem = filtro.clone();
		filtroContagem.contagem = true;

		if(filtro.busca != null){
			return (long) findByFilterBusca(filtroContagem).size();
		} else {
			return (long) findByFilter(filtroContagem).size();
		}

	}


	public static List<Empreendedor> getEmpreendedores(String cpfCnpj){

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		List<Empreendimento> empreendimentosCadastrante = Empreendimento.find("cpf_cnpj_cadastrante =:cpfCnpj and removido =:removido")
				.setParameter("removido", false)
				.setParameter("cpfCnpj",cpfCnpj).fetch();

		List<Empreendimento> empreendimentosPessoa = Empreendimento.find("id_pessoa =:idPessoa")
				.setParameter("idPessoa",pessoa.id).fetch();

		Empreendedor empreendedor = Empreendedor.find("id_pessoa = :idPessoa")
				.setParameter("idPessoa",pessoa).first();

		List<Empreendedor> empreendedorCadastrante = empreendimentosCadastrante.stream().map(empreendimento -> empreendimento.empreendedor).distinct().collect(Collectors.toList());
		List<Empreendedor> empreendedorPessoa = empreendimentosPessoa.stream().map(empreendimento -> empreendimento.empreendedor).distinct().collect(Collectors.toList());

		empreendedorCadastrante.addAll(empreendedorPessoa);
		
		if(empreendedor != null)
			empreendedorCadastrante.add(empreendedor);

		return empreendedorCadastrante;
	}
}
