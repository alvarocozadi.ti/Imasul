package models;

import common.models.Filter;

import java.util.Date;

public class FiltroPessoa extends Filter {

	public static Integer ID_MODULO_REQUISITOR = null;

	public String login;

	public String nome;

	public String nomePerfil;

	public String codigoPerfil;

	public String codigoPermissao;

	public Integer numeroPagina;

	public Integer tamanhoPagina;

	public Date dataAtualizacaoInicio;

	public Date dataAtualizacaoFim;

	public String passaporte;

	public boolean isUsuario;

	public boolean todosUsuarios;

	public Boolean somentePessoaJuridica;

	public Boolean somentePessoaFisica;

	public Boolean somenteCidadaosBrasileiro;


}
