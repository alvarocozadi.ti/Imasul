package models.permissoes;

import secure.models.Action;

import java.util.ArrayList;
import java.util.List;

public class AcaoSistema implements Action {

	public enum Acao {

		PESQUISAR_USUARIOS(1,"pesquisarUsuarios"),
		CADASTRAR_USUARIO(2,"cadastrarUsuario"),
		REMOVER_USUARIO(3,"remover_usuario"),
		ATIVAR_DESATIVAR_USUARIO(4,"ativarDesativarUsuario"),
		EDITAR_USUARIO(5,"editarUsuario"),
		VISUALIZAR_USUARIO(6,"visualizarUsuario"),
		PESQUISAR_MODULOS(7,"pesquisarModulos"),
		CADASTRAR_MODULO(8,"cadastrarModulo"),
		REMOVER_MODULO(9,"removerModulos"),
		ATIVAR_DESATIVAR_MODULO(10,"ativarDesativarModulo"),
		EDITAR_MODULO(11,"editarModulo"),
		VISUALIZAR_MODULO(12,"visualizarModulo"),
		PESQUISAR_PERFIS(13,"pesquisarPerfis"),
		CADASTRAR_PERFIL(14,"cadastrarPerfil"),
		REMOVER_PERFIL(15,"removerPerfil"),
		EDITAR_PERFIL(16,"editarPerfil"),
		VISUALIZAR_PERFIL(17,"visualizarPerfil"),
		PESQUISAR_PERMISSOES(18,"pesquisarPermissoes"),
		CADASTRAR_PERMISSAO(19,"cadastrarPermissao"),
		REMOVER_PERMISSAO(20,"removerPermissao"),
		EDITAR_PERMISSAO(21,"editarPermissao"),
		VISUALIZAR_PERMISSAO(22,"visualizarPermissao"),
		CRIAR_NOVAS_CREDENCIAIS(23,"criarNovasCredenciais"),
		CADASTRAR_SETOR (24, "cadastrarSetor"),
		PESQUISAR_SETOR (25, "pesquisarSetores"),
		EDITAR_SETOR (26, "editarSetor"),
		VISUALIZAR_SETOR (27, "visualizarSetor"),
		REMOVER_SETOR (28, "removerSetor"),
		ATIVAR_DESATIVAR_SETOR(29, "ativarDesativarSetor"),
		CADASTRAR_PESSOA_FISICA(101, "cadastrarPessoaFisica"),
		LISTAR_PAISES(102, "listarPaises"),
		LISTAR_ESTADOS(103, "listarEstados"),
		LISTAR_MUNICIPIOS(104, "listarMunicipios"),
		LISTAR_TIPOS_CONTATO(105, "listarTiposContato"),
		FILTRAR_PESSOA_FISICA(106, "listarPessoasFisicas"),
		VISUALIZAR_PESSOA_FISICA(107, "visualizarPessoaFisica"),
		EDITAR_PESSOA_FISICA(108, "editarPessoaFisica"),
		REMOVER_PESSOA_FISICA(109, "removerPessoaFisica"),
		CADASTRAR_PESSOA_JURIDICA(110, "cadastrarPessoaJuridica"),
		LISTAR_PESSOAS_JURIDICAS(111, "listarPessoasJuridicas"),
		VISUALIZAR_PESSOA_JURIDICA(112, "visualizarPessoaJuridica"),
		REMOVER_PESSOA_JURIDICA(113, "removerPessoaJuridica"),
		EDITAR_PESSOA_JURIDICA(114, "editarPessoaJuridica"),
		CADASTRAR_EMPREENDIMENTO(115, "cadastrarEmpreendimento"),
		LISTAR_EMPREENDIMENTOS(116, "listarEmpreendimentos"),
		VISUALIZAR_EMPREENDIMENTO(117, "visualizarEmpreendimento"),
		REMOVER_EMPREENDIMENTO(118, "removerEmpreendimento"),
		EDITAR_EMPREENDIMENTO(119, "editarEmpreendimento");

		private Integer codigo;

		private String permissao;
		private Acao(Integer codigo, String permissao) {

			this.codigo = codigo;
			this.permissao = permissao;

		}


	}
	private Integer codigo;

	private String permissao;
	private String regras;
	private String pacoteRegras;

	public AcaoSistema(Acao acao) {

		this.codigo = acao.codigo;
		this.permissao = acao.permissao;

	}

	@Override
	public Integer getId() {
		return this.codigo;
	}

	@Override
	public String getDescription() {
		return this.permissao;
	}

	@Override
	public String getRole() {
		return this.permissao;
	}

	@Override
	public String getRules() {
		return null;
	}

	@Override
	public String getRulesPackage() {
		return null;
	}

	@Override
	public String toString() {
		return " " + this.codigo + " - " + this.permissao;
	}

	public static List<AcaoSistema> getAllAsList() {

		List<AcaoSistema> acoesSistema = new ArrayList<>();

		for(Acao acao: Acao.values()) {

			acoesSistema.add(new AcaoSistema(acao));

		}

		return acoesSistema;

	}

}
