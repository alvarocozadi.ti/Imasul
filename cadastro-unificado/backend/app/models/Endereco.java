package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.EnderecoVo;
import exceptions.ValidationException;
import org.apache.commons.lang.math.NumberUtils;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import utils.Config;
import utils.StringUtil;
import validators.If;
import validators.Reference;
import validators.RequiredIf;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado", name = "endereco")
public class Endereco extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_endereco")
	@SequenceGenerator(name = "sq_endereco", sequenceName = "cadastro_unificado.sq_endereco", allocationSize = 1)
	public Integer id;

	@Required(message = "endereco.tipo.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_tipo_endereco", referencedColumnName = "id")
	public TipoEndereco tipo;

	@Required(message = "endereco.zonaLocalizacao.obrigatorio")
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "zona_localizacao")
	public ZonaLocalizacao zonaLocalizacao;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.logradouro.obrigatorio")
	public String logradouro;

	@RequiredIf(ifs = {@If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"),
		@If(option = If.IfOption.FALSE, attribute = "semNumero")},
		message = "endereco.numero.obrigatorio")
	public Integer numero;

	@Column(name = "caixa_postal")
	public String caixaPostal;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.bairro.obrigatorio")
	public String bairro;

	public String complemento;

	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "URBANA"), message = "endereco.cep.obrigatorio")
	public String cep;

	@Column(name = "descricao_acesso")
	@RequiredIf(ifs = @If(option = If.IfOption.EQUALS, attribute = "zonaLocalizacao", value = "RURAL"), message = "endereco.descricaoAcesso.obrigatorio")
	public String descricaoAcesso;

	@Required(message = "endereco.municipio.obrigatorio")
	@Reference(modelClass = Municipio.class, message = "endereco.municipio.inexistente")
	@ManyToOne
	@JoinColumn(name = "id_municipio", referencedColumnName = "id")
	public Municipio municipio;

	@Required(message = "endereco.pais.obrigatorio")
	@Reference(modelClass = Pais.class, message = "endereco.pais.inexistente")
	@ManyToOne
	@JoinColumn(name = "id_pais", referencedColumnName = "id")
	public Pais pais;

	public Boolean removido;

	@Transient
	public Boolean semNumero;

	public Endereco() {

		super();

		this.removido = false;
		this.semNumero = false;

	}

	public Endereco(Endereco endereco, Integer tipoEndereco) {
		this.removido = endereco.removido;
		this.cep = endereco.cep;
		this.bairro = endereco.bairro;
		this.zonaLocalizacao = ZonaLocalizacao.URBANA;
		this.pais = endereco.pais;
		this.municipio = endereco.municipio;
		this.numero = endereco.numero;
		this.logradouro = endereco.logradouro;
		this.semNumero = endereco.semNumero;
		this.tipo = new TipoEndereco(tipoEndereco);

	}

	public Endereco(EnderecoVo enderecoVo, TipoEndereco tipoEndereco) {

		super();

		this.removido = false;
		this.tipo = tipoEndereco;
		this.zonaLocalizacao = ZonaLocalizacao.URBANA;
		this.logradouro = enderecoVo.getLogradouro() != null ? enderecoVo.getLogradouro() : "N/A";
		this.bairro = enderecoVo.getBairro() != null ? enderecoVo.getBairro() : "N/A";
		this.cep = enderecoVo.getCep() != null ? enderecoVo.getCep() : "N/A";
		if(enderecoVo.getNumLogradouro() != null && NumberUtils.isNumber(enderecoVo.getNumLogradouro())) {
			this.numero = Integer.parseInt(StringUtil.onlyNumber(enderecoVo.getNumLogradouro()));
			this.semNumero = false;
		}
		else {
			this.numero = null;
			this.semNumero = true;
		}

		this.pais = Pais.paisRedeSimples(enderecoVo.getCodPais());
		this.municipio = enderecoVo.getCodMunicipio() != null ? Municipio.municipioRedeSimples(enderecoVo.getCodMunicipio()) : null;

	}

	public Endereco(EnderecoVo enderecoVo, TipoEndereco tipoEndereco, String na) {

		super();
		Config.RedeSimplesLogger.trace("Preenchendo Endereco Invalido");

		this.removido = false;
		this.tipo = tipoEndereco;
		this.zonaLocalizacao = ZonaLocalizacao.URBANA;
		this.logradouro = na;
		this.bairro = na;
		this.cep = na;
		this.numero = null;
		this.semNumero = true;
		this.pais = Pais.paisRedeSimples(enderecoVo.getCodPais());
		this.municipio = enderecoVo.getCodMunicipio() != null ? Municipio.municipioRedeSimples(enderecoVo.getCodMunicipio()) : processar();

	}

	private Municipio processar() {
		Config.RedeSimplesLogger.trace("Sem municipio e sem codigo municipio");
		return null;
	}

	public Endereco setAttributes(Endereco endereco) {

		this.tipo = endereco.tipo;
		this.zonaLocalizacao = endereco.zonaLocalizacao;
		this.municipio = endereco.municipio;
		this.descricaoAcesso = endereco.descricaoAcesso;
		this.pais = endereco.pais;
		this.caixaPostal = endereco.caixaPostal;
		this.bairro = endereco.bairro;
		this.complemento = endereco.complemento;
		this.logradouro = endereco.logradouro;
		this.numero = endereco.numero;
		this.cep = endereco.cep;
		this.semNumero = endereco.semNumero;

		return this;

	}

	public Endereco saveAttributes(Endereco endereco) {

		this.setAttributes(endereco);
		this._save();
		return this;

	}

	@Override
	public Object _key() {

		return this.id;

	}

	protected static void setEnderecos(Set<Endereco> objEnderecos, Set<Endereco> enderecosEditar) {

		for(Endereco endereco : objEnderecos) {

			for(Endereco enderecoEditado : enderecosEditar) {

				if(endereco.tipo.id == enderecoEditado.tipo.id) {

					endereco.setAttributes(enderecoEditado);

				}

			}

		}

	}

	protected static void updateEnderecos(Set<Endereco> enderecosEditar) {

		enderecosEditar.forEach(enderecoEditado -> {
			Endereco endereco = findById(enderecoEditado.id);
			endereco.saveAttributes(enderecoEditado);
		});

	}

	protected static Set<Endereco> formatarEndereco(Set<Endereco> enderecos) {

		enderecos.forEach(endereco -> {
			endereco.pais = Pais.findBySigla("BR");
			endereco.municipio = Municipio.findByCodigoIbge(endereco.municipio.id.toString()) != null ? Municipio.findByCodigoIbge(endereco.municipio.id.toString()) : Municipio.findById(endereco.municipio.id);

		});
		return enderecos;

	}

	public static void validateEnderecos(Set<Endereco> enderecos) {

		if(enderecos == null) {

			throw new ValidationException().userMessage("enderecos.obrigatorios");

		}

		if(!hasPrincipal(enderecos) || !hasCorrespondencia(enderecos)) {

			throw new ValidationException().userMessage("enderecos.obrigatorios");

		}

		verifyAndCleanInvalidAttributes(enderecos);

	}

	public static void verifyAndCleanInvalidAttributes(Set<Endereco> enderecos) {

		for(Endereco endereco : enderecos) {

			if(endereco.zonaLocalizacao == ZonaLocalizacao.RURAL && endereco.tipo.id == TipoEndereco.ID_CORRESPONDENCIA) {

				throw new ValidationException().userMessage("enderecos.correspondencia.urbanoObrigatorio");

			}

			if(endereco.semNumero == null) {

				throw new ValidationException().userMessage("endereco.semNumero.obrigatorio");

			}

			if(endereco.semNumero != null && endereco.semNumero) {

				endereco.numero = null;

			}

			if(endereco.zonaLocalizacao != ZonaLocalizacao.RURAL)
				endereco.descricaoAcesso = null;

		}

	}

	public static Boolean hasPrincipal(Set<Endereco> enderecos) {

		for(Endereco endereco : enderecos) {

			if(endereco.tipo.id.equals(TipoEndereco.ID_PRINCIPAL)) {

				return true;

			}

		}

		return false;

	}

	public static Boolean hasCorrespondencia(Set<Endereco> enderecos) {

		for(Endereco endereco : enderecos) {

			if(endereco.tipo.id.equals(TipoEndereco.ID_CORRESPONDENCIA)) {

				return true;

			}

		}

		return false;

	}

	public static Set<Endereco> enderecosRedeSimples(EnderecoVo enderecoVo) {

		if(enderecoVo == null) {
			Config.RedeSimplesLogger.trace(" ==== Sem Endereco");
			return null;
		}

		TipoEndereco tipoEnderecoPrincipal = TipoEndereco.findById(TipoEndereco.ID_PRINCIPAL);
		TipoEndereco tipoEnderecoCorrespondencia = TipoEndereco.findById(TipoEndereco.ID_CORRESPONDENCIA);
		Set<Endereco> listaEnderecos = new HashSet<>();
		Endereco enderecoPrincipal = new Endereco(enderecoVo, tipoEnderecoPrincipal);
		logRedeSimples(enderecoPrincipal);
		enderecoPrincipal.save();
		listaEnderecos.add(enderecoPrincipal);
		Endereco enderecoCorrespondencia = new Endereco(enderecoVo, tipoEnderecoCorrespondencia);
		logRedeSimples(enderecoCorrespondencia);
		enderecoCorrespondencia.save();
		listaEnderecos.add(enderecoCorrespondencia);

		return listaEnderecos;
	}

	private static void logRedeSimples(Endereco endereco){
		Config.RedeSimplesLogger.trace(" ############################################### ");
		Config.RedeSimplesLogger.trace(" ==== Endereco " + isNullObject(endereco.tipo.descricao));
		Config.RedeSimplesLogger.trace(" ==== == Logradouro " + isNullObject(endereco.logradouro));
		Config.RedeSimplesLogger.trace(" ==== == Numero " + isNullObject(endereco.numero));
		Config.RedeSimplesLogger.trace(" ==== == Bairro " + isNullObject(endereco.bairro));
		Config.RedeSimplesLogger.trace(" ==== == Caixa Postal " + isNullObject(endereco.caixaPostal));
		Config.RedeSimplesLogger.trace(" ==== == CEP " + isNullObject(endereco.cep));
		Config.RedeSimplesLogger.trace(" ==== == Descricao de acesso " + isNullObject(endereco.descricaoAcesso));
		Config.RedeSimplesLogger.trace(" ==== == Pais id " + isNullObject(endereco.pais.id));
		if (endereco.municipio == null) {
			Config.RedeSimplesLogger.trace(" ==== == Sem municipio ");
		}else {
			Config.RedeSimplesLogger.trace(" ==== == Municipio nome " + isNullObject(endereco.municipio.nome));
			Config.RedeSimplesLogger.trace(" ==== == Municipio id " + isNullObject(endereco.municipio.id));
			Config.RedeSimplesLogger.trace(" ==== == Municipio codigoIBGE " + isNullObject(endereco.municipio.codigoIbge));
			Config.RedeSimplesLogger.trace(" ==== == Estado nome " + isNullObject(endereco.municipio.estado.nome));
			Config.RedeSimplesLogger.trace(" ==== == Estado id " + isNullObject(endereco.municipio.estado.id));
		}

		Config.RedeSimplesLogger.trace(" ==== == Zona Localizacao " + isNullObject(endereco.zonaLocalizacao.getDescricao()));
		Config.RedeSimplesLogger.trace(" ==== == Descricao acesso " + isNullObject(endereco.descricaoAcesso));
		Config.RedeSimplesLogger.trace(" ############################################### ");

	}

	private static String isNullObject(Object o){
		return o != null ? o.toString() : "N/A";
	}

}