package models;

import play.Play;
import play.Play.Mode;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Configuracao {

	public Mode mode;
	public String versao;
	public String dataVersao;
	public Map<String, Tipo[]> tipos;

	public Configuracao() {

		super();

		this.mode = Play.mode;
		this.versao = Play.configuration.getProperty("server.version");
		this.dataVersao = Play.configuration.getProperty("server.update");

		this.populateTipos();

	}

	private void populateTipos() {

		this.tipos = new HashMap<>();

		this.tipos.put(Sexo.class.getSimpleName(), Sexo.values());
		this.tipos.put(ZonaLocalizacao.class.getSimpleName(), ZonaLocalizacao.values());
		this.tipos.put(EstadoCivil.class.getSimpleName(), EstadoCivil.values());
		this.tipos.put(TipoPessoa.class.getSimpleName(), TipoPessoa.values());

	}

	public String getVersaoCliente() {

		return this.versao.split("\\.")[0];

	}

}
