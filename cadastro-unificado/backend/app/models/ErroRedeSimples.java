package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;


@Entity
@Table(schema = "cadastro_unificado", name = "erro_rede_simples")
public class ErroRedeSimples extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "erro_rede_simples_id_seq")
	@SequenceGenerator(name = "erro_rede_simples_id_seq", sequenceName = "erro_rede_simples_id_seq", allocationSize = 1)
	public Integer id;

	@Column(name = "json_response")
	public String jsonResponse;

	@Column(name = "cnpj_empreendimento")
	public String cnpj;


	public ErroRedeSimples(String json, String cnpj){
		this.cnpj = cnpj;
		this.jsonResponse = json;
	}

}
