package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.jpa.GenericModel;

@Entity
@Table(schema = "cadastro_unificado", name = "tipo_endereco")
public class TipoEndereco extends GenericModel {

	public static final Integer ID_PRINCIPAL = 1;
	public static final Integer ID_CORRESPONDENCIA = 2;

	@Id
	public Integer id;

	public String descricao;

	public Boolean removido;

	public TipoEndereco() {

		super();

		this.removido = false;

	}

	public TipoEndereco(Integer tipoEndereco) {
		if(tipoEndereco == 1)
			this.id = ID_PRINCIPAL;
		else
			this.id = ID_CORRESPONDENCIA;

	}

	@Override
	public Object _key() {
		return this.id;
	}

}
