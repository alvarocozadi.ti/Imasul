package models;

import play.db.jpa.GenericModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "cadastro_unificado", name = "tipo_contato")
public class TipoContato extends GenericModel {

	public static final Integer ID_EMAIL = 1;
	public static final Integer ID_TELEFONE_RESIDENCIAL = 2;
	public static final Integer ID_TELEFONE_COMERCIAL = 3;
	public static final Integer ID_TELEFONE_CELULAR = 4;

	@Id
	public Integer id;

	public String descricao;

	public Boolean removido;

	public TipoContato() {

		super();

		this.removido = false;

	}
	public TipoContato(Integer idTipoContato) {
		this.id = idTipoContato;
	}
	@Override
	public Object _key() {
		return this.id;
	}

}