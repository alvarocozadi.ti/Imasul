package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.*;
import play.data.validation.Required;
import play.db.jpa.GenericModel;
import utils.Config;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(schema = "cadastro_unificado", name = "cnae")
public class Cnae extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cnae_id_seq")
	@SequenceGenerator(name = "cnae_id_seq", sequenceName = "cadastro_unificado.cnae_id_seq", allocationSize = 1)
	public Integer id;

	@Required(message = "cnae.codigo.obrigatorio")
	@Column(name = "codigo")
	public String codigo;

	@Required(message = "cnae.empreendimento.obrigatorio")
	@ManyToOne
	@JoinColumn(name = "id_empreendimento", referencedColumnName="id")
	public Empreendimento empreendimento;

	public Cnae() {

		super();

	}

	public Cnae(CnaeVo cnaeVo, Empreendimento empreendimento) {

		super();
		this.codigo = cnaeVo.getCodigo();
		this.empreendimento = empreendimento;

	}

	@Override
	public Object _key() {

		return this.id;

	}

	public Cnae setAttributes(Cnae cnae) {

		this.codigo = cnae.codigo;

		return this;

	}

	public static void cnaesRedeSimples(AtividadesEconomicaVo atividadesEconomicaVo, Empreendimento empreendimento) {

		List<CnaeVo> cnaesRecebidas = new ArrayList<>();

		if (atividadesEconomicaVo != null) {

			cnaesRecebidas.add(atividadesEconomicaVo.getCnaeFiscal());

			if (atividadesEconomicaVo.getCnaesSecundarias() != null) {
				cnaesRecebidas.addAll(atividadesEconomicaVo.getCnaesSecundarias().getCnaeSecundaria());
			}

			List<Cnae> cnaeDoEmpreendimento = Cnae.find("empreendimento = :empreendimento")
					.setParameter("empreendimento", empreendimento)
					.fetch();

			cnaeDoEmpreendimento.forEach(c -> c.delete());
			cnaesRecebidas.forEach(c -> new Cnae(c, empreendimento).save());
		} else {
			Config.RedeSimplesLogger.trace("Atividade economica nula");
			CnaeVo cnaeVo = new CnaeVo();
			cnaeVo.setCodigo("N/A");
			new Cnae(cnaeVo, empreendimento).save();
		}
	}

}
