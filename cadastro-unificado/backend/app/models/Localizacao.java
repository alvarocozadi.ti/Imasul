package models;

import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.Type;
import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(schema = "cadastro_unificado", name = "localizacao")
public class Localizacao extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_localizacao")
	@SequenceGenerator(name = "sq_localizacao", sequenceName = "cadastro_unificado.sq_localizacao", allocationSize = 1)
	public Integer id;

	@Required(message = "localizacao.geometria.obrigatorio")
	@Type(type="org.hibernate.spatial.GeometryType")
	public Geometry geometria;

	public Localizacao() {

		super();

	}

	@Override
	public Object _key() {

		return this.id;

	}

	public Localizacao setAttributes(Localizacao localizacao) {

		this.geometria = localizacao.geometria;

		return this;

	}

}
