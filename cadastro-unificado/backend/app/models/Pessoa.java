package models;

import br.ufla.lemaf.integracao.redesimples.WSE013.DadosRedesimVo;
import br.ufla.lemaf.integracao.redesimples.WSE013.SocioVo;
import models.integracaoSiriema.ContatoPessoaSiriemaVO;
import models.integracaoSiriema.EnderecoPessoaSiriemaVO;
import models.portalSeguranca.Perfil;
import models.portalSeguranca.Usuario;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;
import org.hibernate.type.StringType;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import play.mvc.Http;
import services.ExternalAuthorizationService;
import services.ExternalUsuarioService;
import utils.Identificavel;
import utils.StringUtil;

import javax.persistence.*;
import java.text.Normalizer;
import java.util.*;

@Entity
@Table(schema = "cadastro_unificado", name = "pessoa")
@Inheritance(strategy = InheritanceType.JOINED)
@org.hibernate.annotations.Filter(name = "entityRemovida")
public abstract class Pessoa extends GenericModel implements Identificavel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_pessoa")
	@SequenceGenerator(name = "sq_pessoa", sequenceName = "cadastro_unificado.sq_pessoa", allocationSize = 1)
	public Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_cadastro")
	public Date dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	public Date dataAtualizacao;

	@Column
	public Boolean removido;

	@Valid
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "contato_pessoa", schema = "cadastro_unificado",
		joinColumns = @JoinColumn(name="id_pessoa", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "id_contato", referencedColumnName = "id"))
	public Set<Contato> contatos;

	@Valid
	@Required(message = "enderecos.obrigatorios")
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "endereco_pessoa", schema = "cadastro_unificado",
		joinColumns = @JoinColumn(name="id_pessoa", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "id_endereco", referencedColumnName = "id"))
	public Set<Endereco> enderecos;

	@Enumerated(EnumType.ORDINAL)
	public TipoPessoa tipo;

	@Column(name = "is_usuario")
	public Boolean isUsuario;

	@Column
	public Boolean bloqueado;

	@Transient
	public Usuario usuario;

	@ManyToMany(mappedBy = "proprietarios")
	public Set<Empreendimento> proprietarioEmpreendimentos;

	@ManyToMany(mappedBy = "responsaveisTecnicos")
	public Set<Empreendimento> responsavelTecnicoEmpreendimentos;

	@ManyToMany(mappedBy = "representantesLegais")
	public Set<Empreendimento> representanteLegalEmpreendimentos;

	@ManyToMany(mappedBy = "responsaveisLegais")
	public Set<Empreendimento> responsavelLegalEmpreendimentos;

//	@OneToMany(mappedBy = "pessoa")
//	public Empreendedor empreendedor;

	public Pessoa() {

		super();

		this.dataCadastro = new Date();
		this.dataAtualizacao = new Date();
		this.removido = false;
		this.isUsuario = false;
		this.bloqueado = false;

	}

	public Pessoa(SocioVo socioVo) {

		super();

		this.dataCadastro = new Date();
		this.dataAtualizacao = new Date();
		this.removido = false;
		this.isUsuario = false;
		this.bloqueado = false;
		this.enderecos = Endereco.enderecosRedeSimples(socioVo.getEnderecoSocio());
		this.contatos = Contato.contatosRedeSimples(socioVo.getContatoSocio());

	}

	@Override
	public Object _key() {

		return this.id;

	}

	@Override
	public Integer getId() {

		return this.id;

	}

	protected static void commitTransaction() {

		if (JPA.isInsideTransaction()) {

			JPA.em().getTransaction().commit();
			JPA.em().getTransaction().begin();

		}

	}

	public static Pessoa findByCpfCnpj(String cpfCnpj) {

		return Pessoa.find("(cpf = :cpfCnpj OR cnpj = :cpfCnpj)")
				.setParameter("cpfCnpj", cpfCnpj).first();

	}

	public static Pessoa getAtualizacaoByDataAndCpfCnpj(Date dataAtualizacao, String cpfCnpj) {

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		if((pessoa != null) && (pessoa.dataAtualizacao.after(dataAtualizacao))) {

			return pessoa;

		}

		return null;

	}

	public static List<Pessoa> getPessoasAtualizadasByData(Date dataAtualizacao) {

		return Pessoa.find("dataAtualizacao >= :data")
				.setParameter("data", dataAtualizacao).fetch();

	}

	public static List<Pessoa> getPessoasAtualizadasByDataAndCpfCnpjList(Date dataAtualizacao, String cpfsCnpjs) {

		return Pessoa.find("dataAtualizacao >= :data AND ( :cpfsCnpjs LIKE CONCAT('%',cpf,'%') OR :cpfsCnpjs LIKE CONCAT('%',cnpj,'%') )")
				.setParameter("data", dataAtualizacao)
				.setParameter("cpfsCnpjs", cpfsCnpjs)
				.fetch();

	}

	public static List<Pessoa> findByFilterIsUsuarios(FiltroPessoa filtro) {

		Session session = (Session) JPA.em().getDelegate();

		Criteria crit = session.createCriteria(Pessoa.class);

		filter(crit, filtro);

		return crit.list();

	}

	private static void filter(Criteria crit, FiltroPessoa filtro) {

		crit.add(Restrictions.eq("isUsuario", true));

		usuarioFilters(crit, filtro);

		if(filtro != null) {

			if (!(filtro.numeroPagina == null) && !(filtro.tamanhoPagina == null)) {

				crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
				crit.setMaxResults(filtro.tamanhoPagina);
			}

			loginFilters(crit, filtro);

			if (filtro.nome != null) {

				String parametro = Normalizer.normalize(filtro.nome, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				Disjunction disjunction = Restrictions.disjunction();
				disjunction.add(Restrictions.sqlRestriction("unaccent(nome) ILIKE ?", "%" + parametro + "%", new StringType()));
				disjunction.add(Restrictions.sqlRestriction("unaccent(razao_social) ILIKE ?", "%" + parametro + "%", new StringType()));

				crit.add(disjunction);
			}
		}

		crit.addOrder(Order.asc("nome"))
				.addOrder(Order.asc("razaoSocial"));

	}

	public static List<Pessoa> findByFilterAllModule(FiltroPessoa filtro) {

		Session session = (Session) JPA.em().getDelegate();

		Criteria crit = session.createCriteria(Pessoa.class);

		filterAllModule(crit, filtro);

		return crit.list();

	}

	private static void filterAllModule(Criteria crit, FiltroPessoa filtro) {

		if(filtro != null && filtro.isUsuario) {
			crit.add(Restrictions.eq("isUsuario", true));
			usuarioFiltersAllModule(crit, filtro);
		}

		if(filtro != null) {
			setCriteria(filtro, crit);
		}

	}

	private static void loginFilters(Criteria crit, FiltroPessoa filtro) {

		if(filtro.login != null) {

			String login = filtro.login.replaceAll("[.\\-\\/]", "");

			Criterion condicaoBuscaCpf = Restrictions.eq("cpf", login);
			Criterion condicaoBuscaCnpj = Restrictions.eq("cnpj", login);

			crit.add(Restrictions.or(condicaoBuscaCpf, condicaoBuscaCnpj));
		}

	}

	public Usuario getUsuario() {

		if(this.usuario == null && this.isUsuario) {

			String cpfCnpj = "";

			PessoaFisica pessoaFisica = PessoaFisica.findByPessoa(this);
			if(pessoaFisica == null) {
				PessoaJuridica pessoaJuridica = PessoaJuridica.findByPessoa(this);
				if(pessoaJuridica != null) {
					cpfCnpj = pessoaJuridica.cnpj;
				}
			}
			else {
				cpfCnpj = pessoaFisica.cpf;
			}

			this.usuario = Usuario.find("login = :login AND removido = false").setParameter("login", cpfCnpj).first();

			if(this.usuario != null
					&& !this.usuario.ativo
					&& this.usuario.perfis != null
					&& FiltroPessoa.ID_MODULO_REQUISITOR != null) {

				this.usuario.perfis = Perfil.findPerfisByModuloRequest(this.usuario, FiltroPessoa.ID_MODULO_REQUISITOR);

			}

		}

		return this.usuario;

	}

	private static void usuarioFilters(Criteria crit, FiltroPessoa filtro) {

		FiltroPessoa.ID_MODULO_REQUISITOR = ExternalAuthorizationService.findIdModuloByToken(Http.Request.current().headers.get("authorization").value());
		DetachedCriteria critUsuario = DetachedCriteria.forClass(Usuario.class)
				.setProjection(org.hibernate.criterion.Property.forName("login"));

		Disjunction disjunction = Restrictions.disjunction();
		disjunction.add(org.hibernate.criterion.Property.forName("cpf").in(critUsuario));
		disjunction.add(org.hibernate.criterion.Property.forName("cnpj").in(critUsuario));

		crit.add(disjunction);

		critUsuario.createAlias("perfis", "perfil")
				.createAlias("perfil.moduloPertencente", "modulo")
				.add(Restrictions.eq("modulo.id", FiltroPessoa.ID_MODULO_REQUISITOR));

		if(filtro != null) {

			if (filtro.nomePerfil != null) {

				critUsuario.add(Restrictions.eq("perfil.nome", filtro.nomePerfil));

			}

			if (filtro.codigoPerfil != null) {

				critUsuario.add(Restrictions.eq("perfil.codigo", filtro.codigoPerfil));

			}

			critUsuario.add(Restrictions.eq("ativo", true));

			if (filtro.codigoPermissao != null) {

				critUsuario.createAlias("perfil.permissoes", "permissao")
						.add(Restrictions.eq("permissao.codigo", filtro.codigoPermissao));

			}

			if(filtro.dataAtualizacaoInicio != null) {

				critUsuario.add(Restrictions.ge("dataAtualizacao", filtro.dataAtualizacaoInicio));
			}

			if(filtro.dataAtualizacaoFim != null) {

				critUsuario.add(Restrictions.le("dataAtualizacao", filtro.dataAtualizacaoFim));
			}
		}

	}

	private static void usuarioFiltersAllModule(Criteria crit, FiltroPessoa filtro) {

		DetachedCriteria critUsuario = DetachedCriteria.forClass(Usuario.class).setProjection(org.hibernate.criterion.Property.forName("login"));

		Disjunction disjunction = Restrictions.disjunction();
		disjunction.add(org.hibernate.criterion.Property.forName("cpf").in(critUsuario));
		disjunction.add(org.hibernate.criterion.Property.forName("cnpj").in(critUsuario));

		crit.add(disjunction);

		critUsuario.createAlias("perfis", "perfil");

		if(filtro != null) {

			if (filtro.nomePerfil != null) {

				critUsuario.add(Restrictions.eq("perfil.nome", filtro.nomePerfil));

			}

			if (filtro.codigoPerfil != null) {

				critUsuario.add(Restrictions.eq("perfil.codigo", filtro.codigoPerfil));

			}

			if (!filtro.todosUsuarios) {
				critUsuario.add(Restrictions.eq("ativo", true));
			}

			if (filtro.codigoPermissao != null) {

				critUsuario.createAlias("perfil.permissoes", "permissao")
					.add(Restrictions.eq("permissao.codigo", filtro.codigoPermissao));

			}

			if(filtro.dataAtualizacaoInicio != null) {

				critUsuario.add(Restrictions.ge("dataAtualizacao", filtro.dataAtualizacaoInicio));
			}

			if(filtro.dataAtualizacaoFim != null) {

				critUsuario.add(Restrictions.le("dataAtualizacao", filtro.dataAtualizacaoFim));
			}

		}

	}

	public static Long countByFilterIsUsuarios(FiltroPessoa filtro) {

		Session session = (Session) JPA.em().getDelegate();

		Criteria crit = session.createCriteria(Pessoa.class);

		if(filtro != null) {

			filtro.tamanhoPagina = null;
			filtro.numeroPagina = null;
		}

		filter(crit, filtro);

		return Long.valueOf(crit.list().size());

	}

	public static Long countByFilterAllModule(FiltroPessoa filtro) {

		Session session = (Session) JPA.em().getDelegate();

		Criteria crit = session.createCriteria(Pessoa.class);

		if(filtro != null) {

			filtro.tamanhoPagina = null;
			filtro.numeroPagina = null;
		}

		filterAllModule(crit, filtro);

		return Long.valueOf(crit.list().size());

	}

	public String updateOrCreate(Boolean isIntegracao) {

		String msg = "";

		if(this.tipo == TipoPessoa.FISICA || this instanceof PessoaFisica) {

			PessoaFisica pessoaFisica = (PessoaFisica) this;
			PessoaFisica pessoaFisicaEditar = null;

			if(pessoaFisica.cpf != null)
				pessoaFisicaEditar = PessoaFisica.findByCpf(pessoaFisica.cpf);
			else
				pessoaFisicaEditar = PessoaFisica.findByPassaporte(pessoaFisica.passaporte);

			if(pessoaFisicaEditar == null) {

				if(isIntegracao)
					pessoaFisica.isUsuario = true;
				else
					pessoaFisica.isUsuario = false;

				pessoaFisica.save();

				msg = "pessoa.cadastrar.sucesso";

			} else {

				pessoaFisica.id = pessoaFisicaEditar.id;

				if(!pessoaFisicaEditar.isUsuario) {

					pessoaFisica.isUsuario = false;

				} else {
					if (isIntegracao == false) {
						pessoaFisica.usuario = pessoaFisicaEditar.usuario;

						if (pessoaFisica.usuario != null) {

							for (Contato contato : pessoaFisica.contatos) {

								if (contato.principal)
									pessoaFisica.usuario.email = contato.valor;

//								ExternalUsuarioService.update(pessoaFisica.usuario);

							}

						}

					}

					pessoaFisicaEditar.update(pessoaFisica);

					msg = "pessoa.editar.sucesso";
				}
			}

		} else if(this.tipo == TipoPessoa.JURIDICA || this instanceof PessoaJuridica) {

			PessoaJuridica pessoaJuridica = (PessoaJuridica) this;
			PessoaJuridica pessoaJuridicaEditar = PessoaJuridica.findByCnpj(pessoaJuridica.cnpj);

			if(pessoaJuridicaEditar == null) {

				if(isIntegracao)
					pessoaJuridica.isUsuario = true;
				else
					pessoaJuridica.isUsuario = false;

				pessoaJuridica.save();

				msg = "pessoa.cadastrar.sucesso";

			} else if (!pessoaJuridicaEditar.isJunta) {

				pessoaJuridica.id = pessoaJuridicaEditar.id;

				if(!pessoaJuridicaEditar.isUsuario) {

					pessoaJuridica.isUsuario = false;

				} else {

					if (isIntegracao == false) {

						pessoaJuridica.usuario = pessoaJuridicaEditar.usuario;

						if (pessoaJuridica.usuario != null) {

							for (Contato contato : pessoaJuridica.contatos) {

								if (contato.principal)
									pessoaJuridica.usuario.email = contato.valor;

								ExternalUsuarioService.update(pessoaJuridica.usuario);

							}

						}

					}

					pessoaJuridicaEditar.update(pessoaJuridica);

					msg = "pessoa.editar.sucesso";
				}
			}

		}

		return msg;

	}

	private static void setCriteria(FiltroPessoa filtro, Criteria crit) {

		if (!(filtro.numeroPagina == null) && !(filtro.tamanhoPagina == null)) {

			crit.setFirstResult((filtro.numeroPagina - 1) * filtro.tamanhoPagina);
			crit.setMaxResults(filtro.tamanhoPagina);
		}

		loginFilters(crit, filtro);

		if (filtro.nome != null) {

			String parametro = Normalizer.normalize(filtro.nome, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.sqlRestriction("unaccent(nome) ILIKE ?", "%" + parametro + "%", new StringType()));
			disjunction.add(Restrictions.sqlRestriction("unaccent(cpf) ILIKE ?", "%" + parametro + "%", new StringType()));
			disjunction.add(Restrictions.sqlRestriction("unaccent(razao_social) ILIKE ?", "%" + parametro + "%", new StringType()));
			disjunction.add(Restrictions.sqlRestriction("unaccent(cnpj) ILIKE ?", "%" + parametro + "%", new StringType()));

			crit.add(disjunction);
		}

		if(filtro.passaporte != null) {

			crit.add(Restrictions.ilike("passaporte", filtro.passaporte));
		}


		if(filtro.somentePessoaFisica != null && filtro.somentePessoaFisica) {

			crit.add(Restrictions.eq("tipo", TipoPessoa.FISICA));
		}

		if(filtro.somentePessoaJuridica != null && filtro.somentePessoaJuridica) {

			crit.add(Restrictions.eq("tipo", TipoPessoa.JURIDICA));
		}

		if(filtro.somenteCidadaosBrasileiro != null && filtro.somenteCidadaosBrasileiro) {

			crit.add(Restrictions.disjunction().add(Restrictions.isNotNull("cpf"))
					.add(Restrictions.isNotNull("cnpj")));
		}

		if(filtro.dataAtualizacaoInicio != null) {

			crit.add(Restrictions.ge("dataAtualizacao", filtro.dataAtualizacaoInicio));
		}

		if(filtro.dataAtualizacaoFim != null) {

			crit.add(Restrictions.le("dataAtualizacao", filtro.dataAtualizacaoFim));
		}


//		crit.addOrder(Order.asc("nome"))
//			.addOrder(Order.asc("razaoSocial"));
		crit.addOrder(Order.asc("id"));

	}

	protected void preencheEnderecosInvalidos(DadosRedesimVo dadosRedesimVo) {

		if(this.enderecos == null || this.enderecos.isEmpty()) {

			Endereco ep = new Endereco(dadosRedesimVo.getEndereco(), TipoEndereco.findById(TipoEndereco.ID_PRINCIPAL), "N/A");
			ep.save();

			Endereco ec = new Endereco(dadosRedesimVo.getEndereco(), TipoEndereco.findById(TipoEndereco.ID_CORRESPONDENCIA), "N/A");
			ec.save();

			if(this.enderecos == null)
				this.enderecos = new HashSet<>();

			this.enderecos.add(ep);
			this.enderecos.add(ec);
		}
	}

	public String getCpfCnpj() {

		if (this instanceof PessoaFisica) {
			return ((PessoaFisica) this).cpf;
		} else if (this instanceof PessoaJuridica) {
			return ((PessoaJuridica) this).cnpj;
		}
		return "";

	}

	public static Set<Contato> formataContatoSiriema(ContatoPessoaSiriemaVO contatoPessoaSiriemaVO){

		Set<Contato> contatos = new HashSet<>();

		if(contatoPessoaSiriemaVO.telefone != null){

			Contato telefone = new Contato();

			telefone.valor = contatoPessoaSiriemaVO.telefone;
			telefone.tipo = new TipoContato(TipoContato.ID_TELEFONE_CELULAR);

			contatos.add(telefone);

		}

		Contato email = new Contato();
		email.valor = contatoPessoaSiriemaVO.email;
		email.principal = true;
		email.tipo = new TipoContato(TipoContato.ID_EMAIL);

		contatos.add(email);
		return contatos;
	}

	public static Set<Endereco> formataEnderecoSiriema(EnderecoPessoaSiriemaVO enderecoPessoaSiriemaVO) throws Exception {

		Endereco enderecoPrincipal = new Endereco();

		Set<Endereco> enderecos = new HashSet<>();

		enderecoPrincipal.cep = enderecoPessoaSiriemaVO.cep != null ? enderecoPessoaSiriemaVO.cep : "00000-000";
		enderecoPrincipal.bairro = enderecoPessoaSiriemaVO.bairro != null && enderecoPessoaSiriemaVO.bairro.length() < 200 ? enderecoPessoaSiriemaVO.bairro : "Não informado.";
		enderecoPrincipal.zonaLocalizacao = ZonaLocalizacao.URBANA;
		enderecoPrincipal.pais = Pais.findBySigla("BR");
		enderecoPrincipal.logradouro = enderecoPessoaSiriemaVO.logradouro != null && enderecoPessoaSiriemaVO.logradouro.length() < 200? enderecoPessoaSiriemaVO.logradouro : "Não informado.";
		enderecoPrincipal.tipo = new TipoEndereco(TipoEndereco.ID_PRINCIPAL);

		if (enderecoPessoaSiriemaVO.cidadeIBGE == null){

			Logger.info("NOME DA CIDADE COM CÓDIGO IBGE NULL = " + enderecoPessoaSiriemaVO.cidade + " --- " + enderecoPessoaSiriemaVO.uf );
		}

		enderecoPrincipal.municipio = Municipio.findByCodigoIbge(enderecoPessoaSiriemaVO.cidadeIBGE);
		if(enderecoPrincipal.municipio == null)
			throw new Exception("Nenhum município foi encontrado com esse código IBGE " + enderecoPessoaSiriemaVO.cidadeIBGE);


		if (enderecoPessoaSiriemaVO.numero == null || enderecoPessoaSiriemaVO.numero.isEmpty() || StringUtil.onlyNumber(enderecoPessoaSiriemaVO.numero).length() == 0) {
			enderecoPrincipal.semNumero = true;
		}else{
			enderecoPrincipal.numero = Integer.parseInt(StringUtil.onlyNumber(enderecoPessoaSiriemaVO.numero));
		}

		enderecos.add(enderecoPrincipal);

		Endereco enderecoCorrespondencia = new Endereco(enderecoPrincipal, TipoEndereco.ID_CORRESPONDENCIA);

		enderecos.add(enderecoCorrespondencia);

		return enderecos;

	}
}
