package models;

import br.ufla.lemaf.integracao.redesimples.Utils.CodigoMunicipioConverter;
import com.vividsolutions.jts.geom.Geometry;
import exceptions.BaseException;
import org.hibernate.SQLQuery;
import org.hibernate.spatial.GeometryType;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(schema = "cadastro_unificado", name = "municipio")
public class Municipio extends GenericModel {

	@Id
	public Integer id;

	public String nome;

	@Column(name = "codigo_ibge")
	public Integer codigoIbge;

	@ManyToOne
	@JoinColumn(name = "id_estado", referencedColumnName = "id")
	public Estado estado;

	public Municipio() {

		super();

	}

	@Override
	public Object _key() {
		return this.id;
	}

	public static List<String> getNomesFromCnpj(String cnpj) {

		PessoaJuridica pessoaJuridica = PessoaJuridica.find("cnpj", cnpj).first();

		if(pessoaJuridica == null) {

			throw new BaseException().userMessage("pessoaJuridica.buscar.naoEncontrado");

		}

		List<String> nomes = new ArrayList<>();
		Municipio municipioEndPrincipal = null;

		for(Endereco endereco : pessoaJuridica.enderecos) {

			if(endereco.tipo.id == TipoEndereco.ID_PRINCIPAL) {

				municipioEndPrincipal = endereco.municipio;

				nomes.add(endereco.municipio.nome);

			}

		}

		List<Municipio> municipios = Municipio.find("FROM "
				+ Municipio.class.getSimpleName()
				+ " m JOIN FETCH m.estado e where e.id = :id ORDER BY random()")
				.setParameter("id", municipioEndPrincipal.estado.id)
				.fetch(NomePessoa.nroNomesRandomicos);

		for(Municipio municipio : municipios) {

			nomes.add(municipio.nome);

		}

		Collections.shuffle(nomes);

		return nomes;

	}

	public static String getSiglaEstadoFromCnpj(String cnpj) {

		PessoaJuridica pessoaJuridica = PessoaJuridica.find("cnpj", cnpj).first();
		String siglaEstado = null;

		for(Endereco endereco : pessoaJuridica.enderecos) {

			if(endereco.tipo.id == TipoEndereco.ID_PRINCIPAL) {

				siglaEstado = endereco.municipio.estado.sigla;

			}

		}

		return siglaEstado;

	}

	public static Municipio findByCodigoIbge(String codigoIbge) {

		Municipio municipio = Municipio.find("codigoIbge = :codigoIbge")
			.setParameter("codigoIbge", new Integer(codigoIbge))
			.first();

		return municipio;
	}

	public static Municipio municipioRedeSimples(String codigo) {

		Integer iCodigo = Integer.parseInt(codigo);

		String codigoIbge = new CodigoMunicipioConverter().toIBGE(iCodigo.toString());

		Municipio municipio = Municipio.findByCodigoIbge(codigoIbge);

		return municipio;
	}

	public Geometry getGeometry() {

		String sql = "SELECT the_geom FROM cadastro_unificado.municipio WHERE id = :id";

		return (Geometry) JPA.em().createNativeQuery(sql)
				.unwrap(SQLQuery.class)
				.addScalar("the_geom", GeometryType.INSTANCE)
				.setParameter("id", this.id)
				.uniqueResult();
	}


}