package models;

import common.models.Filter;

public class FiltroPessoaJuridica extends Filter {

	public enum Ordenacao implements Order {

		RAZAO_SOCIAL_ASC(OrderDirection.ASC, "razaoSocial"),
		RAZAO_SOCIAL_DESC(OrderDirection.DESC, "razaoSocial"),
		MUNICIPIO_ASC(OrderDirection.ASC, "municipio.nome"),
		MUNICIPIO_DESC(OrderDirection.DESC, "municipio.nome");

		private OrderDirection orderDirection;
		private String field;

		private Ordenacao(OrderDirection orderDirection, String field) {

			this.orderDirection = orderDirection;
			this.field = field;

		}

		@Override
		public OrderDirection getOrderDirection() {
			return this.orderDirection;
		}

		@Override
		public String getField() {
			return this.field;
		}

	}

	public String filtroSimplificado;

	public String cnpj;

	public String razaoSocial;

	public int numeroPagina;

	public int tamanhoPagina;

	public Ordenacao ordenacao;

}
