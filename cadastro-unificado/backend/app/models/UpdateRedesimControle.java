package models;

import play.data.validation.Required;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(schema = "cadastro_unificado" , name = "update_redesim_controle")
public class UpdateRedesimControle extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "update_redesim_controle_id_seq")
	@SequenceGenerator(name = "update_redesim_controle_id_seq", sequenceName = "cadastro_unificado.update_redesim_controle_id_seq", allocationSize = 1)
	public Long id;

	@Required
	@Column(name = "data_inicio")
	public Date dataInicio;

	@Required
	@Column(name = "data_fim")
	public Date dataFim;

	@Required
	@Column(name = "qnt_registros_processados")
	public Long quantidadeRegistrosProcessados;

	@Required
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_ultima_pj_processada", referencedColumnName = "id_pessoa")
	public PessoaJuridica ultimaPessoaJuridicaProcessada;

	public static UpdateRedesimControle findUltima() {

		return UpdateRedesimControle.find("ORDER BY id DESC").first();

	}

}
