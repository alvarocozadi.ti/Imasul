package results;

import play.exceptions.UnexpectedException;
import play.mvc.Http;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.results.Result;

/**
 * 500 Error
 */
public class Error extends Result {

	private final int status;

	public Error(String reason) {
	    
		super(reason);
		
		this.status = Http.StatusCode.INTERNAL_ERROR;
		
	}

	public Error(int status, String reason) {
	    
		super(reason);
		
		this.status = status;
		
	}

	@Override
	public void apply(Request request, Response response) {

		response.status = this.status;
		response.contentType = "text/plain; charset=" + Http.Response.current().encoding;

		try {
			response.out.write(getMessage().getBytes(getEncoding()));
		} catch (Exception e) {
			throw new UnexpectedException(e);
		}

	}

	public int getStatus() {

		return this.status;

	}

}
