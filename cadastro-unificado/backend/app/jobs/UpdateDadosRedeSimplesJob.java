package jobs;

import exceptions.UpdateDadosRedeSimplesException;
import models.*;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.jobs.On;
import play.libs.Time;
import services.ProcessamentoRedesimService;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@On("cron.sincronismo.redesimples.empreendimento.update")
public class UpdateDadosRedeSimplesJob extends Job {

	// Variáveis de configuração
	private static final boolean JOB_ENABLED = Boolean.parseBoolean(Play.configuration.getProperty("cron.sincronismo.redesimples.empreendimento.update.enabled"));
	private static final long TEMPO_LIMITE = Time.parseDuration(Play.configuration.getProperty("cron.sincronismo.redesimples.empreendimento.update.tempoLimite"));
	private static final long DELAY = Time.parseDuration(Play.configuration.getProperty("cron.sincronismo.redesimples.empreendimento.update.delay"));

	// Variáveis de estado
	private Date horaInicio;
	private Date horaLimite;
	private Long quantidadeCnpjsProcessados;
	private Integer ultimaPessoaJuridicaId;

	@Override
	public void doJob() throws Exception {

		if (JOB_ENABLED) {

			Logger.info("[UPDATE EMPREENDIMENTOS REDESIM] :: Inicio Job");

			inicializar();

			String cnpj = getProximoCnpjPendente();

			while (cnpj != null && !passouTempoLimite()) {

				try {

					begin();

					Thread.sleep(DELAY * 1000);
					ProcessamentoRedesimService.atualizarDadosPessoasJuridicas(cnpj);
					quantidadeCnpjsProcessados++;

					Logger.info("--> Atualização dos dados pela WSE031 concluída. CNPJ: " + cnpj);

					commit();

				} catch (UpdateDadosRedeSimplesException | IllegalAccessException e) {

					Logger.info("--> Não foi possível atualizar os dados pela WSE031. CNPJ: " + cnpj);
					e.printStackTrace();

					rollback();

				} catch (Exception e) {

					Logger.info("--> Erro inesperado ao atualizar os dados pela WSE031. CNPJ: " + cnpj);
					e.printStackTrace();

					rollback();

				}

				cnpj = getProximoCnpjPendente();

			}

			finalizar();

			Logger.info("[UPDATE EMPREENDIMENTOS REDESIM] :: Fim Job");

		}

	}

	private String getProximoCnpjPendente() {

		String sql  = "SELECT pj.id_pessoa, pj.cnpj FROM cadastro_unificado.pessoa_juridica pj "
					+ "WHERE pj.id_pessoa > :id ORDER BY pj.id_pessoa ASC";

		Query query = JPA.em().createNativeQuery(sql)
				.setParameter("id", ultimaPessoaJuridicaId)
				.setMaxResults(1);

		List<Object[]> resultList = query.getResultList();

		if (!resultList.isEmpty()) {

			int idPessoa = (int) resultList.get(0)[0]; // id_pessoa é o #0 do result set
			String cnpj = (String) resultList.get(0)[1]; // cnpj é o #1 do result set

			ultimaPessoaJuridicaId = idPessoa;

			return cnpj;

		} else {

			return null;

		}

	}

	private void inicializar() {

		UpdateRedesimControle ultimaExecucao = UpdateRedesimControle.findUltima();

		if (ultimaExecucao != null) {

			ultimaPessoaJuridicaId =  ultimaExecucao.ultimaPessoaJuridicaProcessada.id;

		} else {

			ultimaPessoaJuridicaId = 0;

		}

		quantidadeCnpjsProcessados = 0L;
		horaInicio = Calendar.getInstance().getTime();
		horaLimite = new Date(horaInicio.getTime() + TEMPO_LIMITE * 1000);

	}

	private void finalizar() {

		begin();

		UpdateRedesimControle controle = new UpdateRedesimControle();

		controle.dataInicio = horaInicio;
		controle.dataFim = Calendar.getInstance().getTime();
		controle.quantidadeRegistrosProcessados = quantidadeCnpjsProcessados;
		controle.ultimaPessoaJuridicaProcessada = PessoaJuridica.findById(ultimaPessoaJuridicaId);

		controle.save();

		commit();

	}

	private boolean passouTempoLimite() {

		Date horaAtual = Calendar.getInstance().getTime();

		return horaAtual.after(horaLimite);

	}

	private void begin() {

		EntityTransaction currentTransaction = JPA.em().getTransaction();

		if (!currentTransaction.isActive()) {
			currentTransaction.begin();
		}

	}

	private void commit() {

		EntityTransaction currentTransaction = JPA.em().getTransaction();

		if (currentTransaction.isActive()) {
			currentTransaction.commit();
		}

	}

	private void rollback() {

		if (JPA.isInsideTransaction()) {
			JPA.em().getTransaction().rollback();
		}

	}

}
