package jobs;

import exceptions.IntegracaoSiriemaException;
import models.integracaoSiriema.IntegracaoSiriema;
import play.Logger;
import play.Play;
import play.db.jpa.JPAPlugin;
import play.jobs.Job;
import play.jobs.On;
import play.jobs.OnApplicationStart;
import play.libs.Time;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static models.integracaoSiriema.IntegracaoSiriema.FINALIZADO;
import static play.Logger.info;

@On("cron.integracaoSiriema.usuarios")
//@OnApplicationStart
public class IntegracaoUsuariosSiriema extends Job {

    private static final String LOG_PREFIX = "[INTEGRACAO SIRIEMA] ";

    private static final ReentrantLock jobLock = new ReentrantLock();

    // Configura se o job deve buscar os novos usuário/pessoas no Siriema ou não.
    public static final boolean BUSCAR_USUARIOS_SIRIEMA = Boolean.parseBoolean(Play.configuration.getProperty("integracaoSiriema.usuarios.buscarUsuarios"));

    @Override
    public void doJob() throws Exception {

        if (BUSCAR_USUARIOS_SIRIEMA)
            buscarUsuarios();

    }

    private void buscarUsuarios() throws Exception {

        try {

            if (!getLock())
                return;

            Logger.info(LOG_PREFIX + "Inicio");

            executarIntegracoes();

            JPAPlugin.closeTx(false);

        } catch (IntegracaoSiriemaException e) {

            Logger.error(e, LOG_PREFIX + e.getMessage());

        } finally {

            // libera o processamento para que o job possa ser executado novamente
            jobLock.unlock();

            Logger.info(LOG_PREFIX + "Fim");
        }
    }

    private void executarIntegracoes() throws IntegracaoSiriemaException {

        Date limiteFim = getLimiteFimIntegracao();

        IntegracaoSiriema ultimaIntegracao = IntegracaoSiriema.findUltima();
        IntegracaoSiriema integracao = new IntegracaoSiriema();

        if (ultimaIntegracao != null) {

            // se existe uma integração não finalizada é necessário finalizá-la
            if (ultimaIntegracao.status != FINALIZADO)
                ultimaIntegracao.executar(limiteFim);

            // se finalizou a integracao pendente sem erro, executar a proxima
            if (ultimaIntegracao.status == FINALIZADO) {

                integracao.dataInicial = ultimaIntegracao.dataFinal;
                integracao.executar(limiteFim);
            }

        } else { // Caso seja a primeira integracao executada

            integracao.executar(limiteFim);
        }
    }

    private Date getLimiteFimIntegracao() {

        int limiteSegundos = Time.parseDuration(Play.configuration.getProperty("integracaoSiriema.usuarios.tempoLimite"));
        Date inicio = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(inicio);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.SECOND, limiteSegundos);

        return calendar.getTime();
    }

    private boolean getLock() throws InterruptedException {

        // pega o lock do processamento para evitar que o job seja executado mais de 1x em paralelo
        if (!jobLock.tryLock(500, TimeUnit.MILLISECONDS)) {

            info(LOG_PREFIX + "Job já está sendo executado!!");
            return false;
        }

        return true;
    }

}
