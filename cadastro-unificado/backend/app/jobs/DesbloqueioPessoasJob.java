package jobs;

import models.BloqueioPessoa;
import models.Endereco;
import org.apache.commons.lang.time.DateUtils;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import utils.Config;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

// Dispara de meia hora em meia hora
@On("cron.desbloqueioPessoa")
public class DesbloqueioPessoasJob extends Job {

	private static final int LIMITE_TEMPO_LIMPAR_REGISTROS = 15;

	@Override
	public void doJob() {

		Logger.info("Iniciando Job de desbloqueio de pessoas");

		List<BloqueioPessoa> bloqueioPessoas = BloqueioPessoa.findAll();

		for(BloqueioPessoa bloqueioPessoa : bloqueioPessoas) {

			Date dataLimiteRegistros = DateUtils.addMinutes(bloqueioPessoa.dataCadastro, LIMITE_TEMPO_LIMPAR_REGISTROS);
			Date dataLimiteBloqueio = DateUtils.addHours(bloqueioPessoa.dataCadastro, Config.HOURS_BLOQUED);

			if(dataLimiteRegistros.before(new Timestamp(System.currentTimeMillis())) && !bloqueioPessoa.pessoa.bloqueado) {

				bloqueioPessoa.delete();

			} else if(dataLimiteBloqueio.before(new Timestamp(System.currentTimeMillis()))
					&& bloqueioPessoa.pessoa.bloqueado) {

				bloqueioPessoa.pessoa.bloqueado = false;

				for(Endereco endereco : bloqueioPessoa.pessoa.enderecos) {

					if(endereco.numero == null) {

						endereco.semNumero = true;

					}

				}

				bloqueioPessoa.pessoa.save();
				bloqueioPessoa.delete();

			}

		}

	}

}
