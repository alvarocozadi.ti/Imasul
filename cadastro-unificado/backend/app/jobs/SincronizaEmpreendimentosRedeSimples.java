package jobs;

import br.ufla.lemaf.integracao.redesimples.WSE013.*;
import com.google.gson.Gson;
import exceptions.ValidationException;
import models.Cnae;
import models.Empreendimento;
import models.ErroRedeSimples;
import models.PessoaJuridica;
import play.Play;
import play.db.jpa.JPA;
import play.jobs.Job;
import play.jobs.On;
import redesimples.RedeSimplesWS;
import utils.Config;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

@On("cron.sincronismo.redesimples.empreendimento")
public class SincronizaEmpreendimentosRedeSimples extends Job {

	private static final String PREFIX = "SINCRONIA_REDE_SIMPLES";
	private static final boolean SINCRONIA_ATIVA = Boolean.parseBoolean(Play.configuration.getProperty("integracao.redeSimples.sincronia.ativa"));
	private static final boolean INFORMAR_RECEBIMENTO = Boolean.parseBoolean(Play.configuration.getProperty("integracao.redeSimples.sincronia.informarRecebimento"));

	@Override
	public void doJob() {

		if(SINCRONIA_ATIVA) {

			Config.RedeSimplesLogger.info(PREFIX + " - Inicio");

			Integer quantidade = 0;

			do {

				RetornoQuantidadeDeRegistrosNaFilaVo quantidadeDeRegistrosNaFilaVo = RedeSimplesWS.recuperaQuantidadeDeRegistrosNaFilaNaoRespondidos();
				quantidade = Integer.parseInt(quantidadeDeRegistrosNaFilaVo.getQuantidadeDeRegistrosNaFila());

				Config.RedeSimplesLogger.info(PREFIX + " - Número de registros pendentes: " + quantidade);

				if (quantidade > 0) {

					RetornoRedesimVo retornoServicoRedeSim = RedeSimplesWS.recuperaEstabelecimentos();
					processarRegistrosRedeSimples(retornoServicoRedeSim);
				}

			} while (quantidade > 0);

			Config.RedeSimplesLogger.info((PREFIX + " - Fim"));
		}
	}

	private void processarRegistrosRedeSimples(RetornoRedesimVo retornoRedesim) {

		List<RegistroRedesimVo> registrosRedeSim = retornoRedesim.getRegistrosRedesim().getRegistroRedesim();

		List<String> identificadores = new ArrayList<>();

		for (RegistroRedesimVo registro : registrosRedeSim) {

			Config.RedeSimplesLogger.info(PREFIX + " - Processando registro: " + registro.getIdentificador() + " - Início");

			try {
				processarDadosRedeSimples(registro.getDadosRedesim());
			} catch (ValidationException ve) {
				Config.RedeSimplesLogger.error("###### Erro da validação padrão: " + ve.getUserMessage());
				ve.printStackTrace();

				if(INFORMAR_RECEBIMENTO) {
					RedeSimplesWS.enviaInconsistencia(registro, ve.getUserMessage());
					if (ve.getUserMessage().equals(Config.SEM_ENDERECO)) {
						String json = new Gson().toJson(registro.getDadosRedesim(), DadosRedesimVo.class);
						new ErroRedeSimples(json, registro.getDadosRedesim().getCnpj()).save();
					}
				}
			} catch (PersistenceException e) {

				Throwable erro = e.getCause().getCause();
				if (erro == null){
					erro = e.getCause();
				}
				String erros = erro.toString();

				Config.RedeSimplesLogger.error("###### Erro de persistencia " + erros);

				if(INFORMAR_RECEBIMENTO && !erros.contains("Found two representations of same collection")) {

					Config.RedeSimplesLogger.info("##### Retorno a Rede Simples #####");
					RedeSimplesWS.enviaInconsistencia(registro, erros);
				}else {

					e.printStackTrace();
				}
			}

			identificadores.add(registro.getIdentificador());
			Config.RedeSimplesLogger.info(PREFIX + " Processando registro: " + registro.getIdentificador() + " - Fim");
		}

		this.commitTransaction();

		if(INFORMAR_RECEBIMENTO) {
			RedeSimplesWS.informaRecebimento(identificadores);
		}
	}

	private void processarDadosRedeSimples(DadosRedesimVo dadosRedesim) {

		PessoaJuridica pessoaJuridicaEmpreendimento = PessoaJuridica.findByCnpj(dadosRedesim.getCnpj());
		Config.RedeSimplesLogger.trace(" Processando pessoa Jurídica: " + dadosRedesim.getCnpj());

		validaEndereco(dadosRedesim.getEndereco());

		if(pessoaJuridicaEmpreendimento != null) {

			Config.RedeSimplesLogger.trace(" PJ existe");

			// PJ já existe e pode ter vindo do SIRIEMA antes da RedeSim, necessita sobrescrever os dados
			pessoaJuridicaEmpreendimento.atualizarPessoaJuridica(dadosRedesim);
			pessoaJuridicaEmpreendimento.save();

			Empreendimento empreendimento = Empreendimento.find("byPessoa", pessoaJuridicaEmpreendimento).first();

			if(empreendimento != null) {
				Config.RedeSimplesLogger.trace(" Empreendimento existe");
				empreendimento.atualizarEmpreendimento(dadosRedesim);
			}
			else {
				Config.RedeSimplesLogger.trace(" Empreendimento nao existe");
				empreendimento = new Empreendimento(dadosRedesim);
				empreendimento.save();
				Cnae.cnaesRedeSimples(dadosRedesim.getAtividadesEconomica(), empreendimento);
			}

		}
		else {

			Config.RedeSimplesLogger.trace(" PJ nao existe");
			Empreendimento empreendimento = new Empreendimento(dadosRedesim);
			empreendimento.save();
			Cnae.cnaesRedeSimples(dadosRedesim.getAtividadesEconomica(), empreendimento);

		}

	}

	private void commitTransaction() {

		if (JPA.isInsideTransaction()) {

			JPA.em().getTransaction().commit();
			JPA.em().getTransaction().begin();

		}

	}

	private void validaEndereco(EnderecoVo endereco){
		if (endereco == null || endereco.getCodMunicipio() == null) {
			throw new ValidationException().userMessage(Config.SEM_ENDERECO);
		}
	}
}
