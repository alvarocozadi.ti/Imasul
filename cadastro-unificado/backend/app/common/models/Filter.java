package common.models;

import models.*;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

public abstract class Filter {

	public enum OrderDirection {
		ASC,
		DESC
	}

	public interface Order {

		OrderDirection getOrderDirection();
		String getField();

	}

	public FiltroUsuario filtroUsuario;

	public static void userRestrictions(DetachedCriteria critUsuario, Filter filtro) {

		if(filtro.filtroUsuario != null && critUsuario != null) {

			if(filtro.filtroUsuario.perfil != null) {

				critUsuario.createAlias("perfis", "perfil").add(Restrictions.eq("perfil.nome", filtro.filtroUsuario.perfil));

			}

			if(filtro.filtroUsuario.ativo != null) {

				critUsuario.add(Restrictions.eq("ativo", filtro.filtroUsuario.ativo));

			}

		}

	}

	public static void addBaseRestrictionsAndOrder(Criteria crit, Filter filtro) {

		if(filtro instanceof FiltroPessoaFisica) {

			if(((FiltroPessoaFisica) filtro).ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

				crit.addOrder(org.hibernate.criterion.Order.asc(((FiltroPessoaFisica) filtro).ordenacao.getField()));

			} else {

				crit.addOrder(org.hibernate.criterion.Order.desc(((FiltroPessoaFisica) filtro).ordenacao.getField()));

			}

		} else if (filtro instanceof FiltroPessoaJuridica) {

			if(((FiltroPessoaJuridica) filtro).ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

				crit.addOrder(org.hibernate.criterion.Order.asc(((FiltroPessoaJuridica) filtro).ordenacao.getField()));

			} else {

				crit.addOrder(org.hibernate.criterion.Order.desc(((FiltroPessoaJuridica) filtro).ordenacao.getField()));

			}

		} else {

			if(((FiltroEmpreendimento) filtro).ordenacao.getOrderDirection().equals(Filter.OrderDirection.ASC)) {

				crit.addOrder(org.hibernate.criterion.Order.asc(((FiltroEmpreendimento) filtro).ordenacao.getField()));

			} else {

				crit.addOrder(org.hibernate.criterion.Order.desc(((FiltroEmpreendimento) filtro).ordenacao.getField()));

			}

		}

		crit.createAlias("enderecos", "endereco")
				.createAlias("endereco.municipio", "municipio")
				.add(Restrictions.eq("endereco.tipo.id", TipoEndereco.ID_PRINCIPAL));

	}

}
