package controllers;

import common.models.Message;
import common.models.Pagination;
import models.*;
import models.historico.EmpreendimentoHistorico;
import models.permissoes.AcaoSistema;
import play.Logger;
import play.Play;
import play.mvc.With;
import redesimples.RedeSimplesWS;
import serializers.EmpreendimentoHistoricoSerializer;
import serializers.EmpreendimentosSerializer;
import serializers.PessoasSerializer;
import vo.HistoricoEmpreendimento;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@With(Secure.class)
public class Empreendimentos extends BaseController {

	public static void create(Empreendimento empreendimento) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.CADASTRAR_EMPREENDIMENTO);
		}

		notFoundIfNull(empreendimento);

		if(empreendimento.pessoa.id == null && !isExternalResource()) {

				Secure.executeAuthorization(AcaoSistema.Acao.CADASTRAR_PESSOA_FISICA, AcaoSistema.Acao.CADASTRAR_PESSOA_JURIDICA);

		}

		empreendimento.enderecos.forEach(endereco -> {

			if (endereco.municipio.codigoIbge == null) {
				endereco.municipio = Municipio.findByCodigoIbge(endereco.municipio.id.toString());
			}

		});

		empreendimento.save();
		if(empreendimento.empreendedor.ativo == false) {

			empreendimento.empreendedor.ativo = true;
			empreendimento.empreendedor._save();
		}
		renderJSON(empreendimento, EmpreendimentosSerializer.find);

	}

	public static void findByFilter(FiltroEmpreendimento filtro) {

		if(!isExternalResource()) {
			Logger.info("find by filter #########");
			Logger.info("filtro busca :"+filtro.busca);
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);
		}

		notFoundIfNull(filtro);

		List<Empreendimento> empreendimentos = Empreendimento.findByFilter(filtro);

		List<Empreendimento> emp = new ArrayList<>();
		Logger.info("qt empreendimentos antes filtro linha 69 :"+ empreendimentos.size());
		if (filtro.cpfCnpj != null && filtro.cpfCnpjEmpreendedor != null) {
            empreendimentos.forEach(empreendimento -> {
                if(!filtro.cpfCnpjEmpreendedor.equals(filtro.cpfCnpj)) {
                    if(empreendimento.pessoa.getCpfCnpj().equals(filtro.cpfCnpj)) {
                        emp.add(empreendimento);
                    }
                }
            });

            if (!emp.isEmpty()) {
                empreendimentos.clear();
                empreendimentos.addAll(emp);
            }
        }

		Logger.info("qt empreendimentos depois filtro linha 84 :"+ empreendimentos.size());
		empreendimentos.forEach(empreendimento -> {
			if(empreendimento.empreendedor != null){
				if (empreendimento.empreendedor.podeSerExcluido()){
					empreendimento.empreendedor.podeSerExcluido = true;
				} else {
					empreendimento.empreendedor.podeSerExcluido = false;
				}
			}
		});


		Pagination<Empreendimento> pagination = new Pagination<>((long) Empreendimento.countByFilter(filtro), empreendimentos);
		renderJSON(pagination, EmpreendimentosSerializer.findByFilter);

	}

	public static void remove(Integer id) {

		if(!isExternalResource()) {
//			Secure.executeAuthorization(AcaoSistema.Acao.REMOVER_EMPREENDIMENTO);
			forbidden();
		}

		Empreendimento empreendimentoRecuperado = Empreendimento.findById(id);

		notFoundIfNull(empreendimentoRecuperado);

		empreendimentoRecuperado.remove();

		renderJSON(new Message("empreendimento.removido.sucesso"));

	}

	public static void find(Integer id) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.VISUALIZAR_EMPREENDIMENTO);
		}

		notFoundIfNull(id);

		Empreendimento empreendimento = Empreendimento.findById(id);

		renderJSON(empreendimento, EmpreendimentosSerializer.find);

	}

	public static void findEmpreendimentosByIdPessoa(Integer id) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);
		}

		notFoundIfNull(id);

		Pessoa pessoa = Pessoa.findById(id);

		Set<Empreendimento> empreendimentos = Empreendimento.setPessoaPapeis(pessoa);

		renderJSON(empreendimentos, EmpreendimentosSerializer.findEmpreendimentosByPessoa);

	}

	public static void update(String login, Empreendimento empreendimento) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.EDITAR_EMPREENDIMENTO);
		}

		Empreendimento empreendimentoEditar = Empreendimento.findById(empreendimento.id);

		notFoundIfNull(empreendimentoEditar, new Message("empreendimento.buscar.naoEncontrado").getText());

		empreendimento.enderecos.forEach(endereco -> {

			if (endereco.municipio.codigoIbge == null) {
				endereco.municipio = Municipio.findByCodigoIbge(endereco.municipio.id.toString());
			}

		});

		if (empreendimento.empreendedor.id == null) {
			empreendimento.empreendedor.save();
		}

		empreendimentoEditar.update(empreendimento, login);

		renderJSON(empreendimentoEditar, EmpreendimentosSerializer.find);
	}

	public static void confirmaRespostaOrgao(ConfirmaRespostaOrgaoRedeSimples confirmaRespostaOrgaoRedeSimples) {

		notFoundIfNull(confirmaRespostaOrgaoRedeSimples);

		boolean INFORMAR_RESPOSTA_ORGAO = Boolean.parseBoolean(Play.configuration.getProperty("integracao.redeSimples.sincronia.informarRespostaOrgao"));

		if(INFORMAR_RESPOSTA_ORGAO) {
			RedeSimplesWS.confirmaRespostaOrgao(confirmaRespostaOrgaoRedeSimples);
			renderJSON(new Message("empreendimento.redeSimples.confirmaRespostaOrgao.sucesso"));
		}
		else {
			renderJSON(new Message("empreendimento.redeSimples.confirmaRespostaOrgao.naoEnviado"));
		}

	}

	public static void findEmpreendimentosByCpfCnpj(String cpfCnpj) {

		notFoundIfNull(cpfCnpj);

		if(!isExternalResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);

		}

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		renderJSON(pessoa, PessoasSerializer.getEmpreendimentos);

	}

	public static void findHistoricoByCpfCnpj(String cpfCnpj) {

		notFoundIfNull(cpfCnpj);

		if(!isExternalResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);

		}

		renderJSON(new HistoricoEmpreendimento(EmpreendimentoHistorico.findByCpfCnpj(cpfCnpj)), EmpreendimentoHistoricoSerializer.find);
	}

	public static void findEmpreendimentoHistoricoById(Integer idEmpreendimentoHistorico) {

		notFoundIfNull(idEmpreendimentoHistorico);

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);
		}

		renderJSON(EmpreendimentoHistorico.findById(idEmpreendimentoHistorico), EmpreendimentoHistoricoSerializer.findById);
	}

	public static void intersectsById(Integer idEmpreendimento) {

		notFoundIfNull(idEmpreendimento);

		renderJSON(Empreendimento.intersects(idEmpreendimento), EmpreendimentosSerializer.findEmpreendimentoSobreposicao);

	}

	public static void intersectsByCpfCnpj(String cpfCnpj) {

		notFoundIfNull(cpfCnpj);

//		if(!isExternalResource()) {
//			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);
//		}

		renderJSON(Empreendimento.intersects(cpfCnpj), EmpreendimentosSerializer.findEmpreendimentoSobreposicao);
	}

	public static void findEmpreendimentoByIdEU(Long id) {

		notFoundIfNull(id);

		if(!isExternalResource())
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);

		Empreendimento empreendimento = Empreendimento.findById(Integer.parseInt(id.toString()));

		renderJSON(empreendimento, EmpreendimentosSerializer.find);

	}

	public static void findEmpreendimentosByEmpreendedor(Long id) {

		Empreendedor empreendedor = Empreendedor.findById(id);

		Empreendimento empreendimento = Empreendimento.find("id_empreendedor = :idEmpreendedor")
				.setParameter("idEmpreendedor", empreendedor.id).first();

		renderJSON(empreendimento, EmpreendimentosSerializer.find);

	}


}
