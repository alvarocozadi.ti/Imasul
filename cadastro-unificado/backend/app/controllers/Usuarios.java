package controllers;

import common.models.Message;
import models.Pessoa;
import models.permissoes.AcaoSistema;
import models.portalSeguranca.Usuario;
import play.mvc.With;
import secure.services.AuthorizationService;
import serializers.SecureSerializer;
import serializers.UsuariosSerializer;
import services.ExternalUsuarioService;

import java.util.List;

@With(Secure.class)
public class Usuarios extends BaseController {

	public static void getAcoesPermitidas() {

		List acoes = AcaoSistema.getAllAsList();

		List<Integer> idsAcoesPermitidas = AuthorizationService.getInstance().checkPermittedActions(acoes, Secure.getAuthenticatedUser());

		renderJSON(idsAcoesPermitidas);

	}

	public static void findUserByLogin(String login) {

		Usuario usuario = Usuario.find("login", login).first();

		renderJSON(usuario, UsuariosSerializer.find);

	}

	public static void getAuthenticatedUser() {

		Usuario usuario = Secure.getAuthenticatedUser();

		usuario.findPessoaByLogin();

		renderJSON(usuario, SecureSerializer.authenticate);

	}

	public static void activate(Integer id){

		notFoundIfNull(id);

		ExternalUsuarioService.activate(id);

		renderJSON(new Message("usuario.ativado.sucesso"));

	}

	public static void isPessoaBloqued(String login) {

		renderJSON(Usuario.isPessoaBloqued(login));

	}

	public static void findPessoaByLoginForHeader(String login) {

		Usuario usuario = Usuario.find("login", login).first();

		usuario.findPessoaByLogin();

		renderJSON(usuario, UsuariosSerializer.findPessoaForHeader);

	}

	public static void updatePassword(Integer id, String senhaAtual, String senhaNova) throws Exception {

		notFoundIfNull(id);
		notFoundIfNull(senhaAtual);
		notFoundIfNull(senhaNova);

		ExternalUsuarioService.updatePassword(id, senhaAtual, senhaNova);

		renderJSON(new Message("usuario.senha.alterada.sucesso"));

	}

	public static void vincularModuloGestaoEmpreendimentos(String cpfCnpj) {

		notFoundIfNull(cpfCnpj);

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		if(pessoa != null && pessoa.isUsuario) {
			Message message = new Message("pessoa.editar.sucesso");
			Usuario.updateOrSave(pessoa.usuario, message, pessoa.id == null);
		}
		else {
			renderJSON(new Message("usuarios.nulo"));
		}

		renderJSON(new Message("usuarios.vincular.modulo.gestaoEmpreendimento"));

	}

}
