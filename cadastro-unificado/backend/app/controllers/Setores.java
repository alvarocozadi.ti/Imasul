package controllers;

import play.mvc.With;
import services.ExternalModuloService;
import services.ExternalSetorService;

@With(Secure.class)
public class Setores extends BaseController {

	public static void buscaTodosSetores() {

		renderJSON(ExternalSetorService.findAllSetores());
	}

}
