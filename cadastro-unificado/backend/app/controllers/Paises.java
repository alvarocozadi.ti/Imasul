package controllers;

import models.Pais;
import models.permissoes.AcaoSistema;
import play.mvc.With;
import serializers.PaisesSerializer;

import java.util.List;

@With(Secure.class)
public class Paises extends BaseController {

	public static void find() {

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_PAISES);

		}

		List<Pais> paises = Pais.findAll();

		renderJSON(paises, PaisesSerializer.find);

	}

	public static void findEstados(Integer id) {

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_ESTADOS);

		}

		Pais pais = Pais.findById(id);

		renderJSON(pais.estados, PaisesSerializer.findEstados);

	}

}