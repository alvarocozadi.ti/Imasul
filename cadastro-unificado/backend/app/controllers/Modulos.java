package controllers;

import play.mvc.With;
import services.ExternalModuloService;

@With(Secure.class)
public class Modulos extends BaseController {

	public static void buscaTodosModulosComPerfis() {

		renderJSON(ExternalModuloService.findAllModulosWithPerfis());
	}

}
