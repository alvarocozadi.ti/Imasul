package controllers;

import common.models.Pagination;
import common.models.Message;
import models.*;
import models.permissoes.AcaoSistema;
import serializers.EmpreendedorSerializer;
import java.util.List;
import java.util.stream.Collectors;

public class Empreendedores extends BaseController {

	public static void findByCpfCnpj(String cpfCnpj){

		Empreendedor empreendedor = Empreendedor.findByCpfCnpj(cpfCnpj);

		renderJSON(empreendedor, EmpreendedorSerializer.find);

	}

	public static void findByFilter(FiltroEmpreendedor filtro) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_EMPREENDIMENTOS);
		}

		notFoundIfNull(filtro);

		List<Empreendedor> empreendedores = null;

		if (filtro.busca != null) {
			empreendedores = Empreendedor.findByFilterBusca(filtro);
		} else {
			empreendedores = Empreendedor.findByFilter(filtro);
		}

		Long numeroItems = Empreendedor.countByFilter(filtro);
		Pagination<Empreendedor> pagination = new Pagination<>(numeroItems, empreendedores);

		renderJSON(pagination, EmpreendedorSerializer.findByFilter);

	}

	public static void inativarEmpreendedor(Long idEmpreendedor) {

		Empreendedor empreendedor = Empreendedor.findById(idEmpreendedor);

		empreendedor.delete();
		renderJSON(new Message("empreendedor.inativar.sucesso"));
	}

	public static void getEmpreendedoresCadastrante(String cpfCnpj) {

		List<Empreendedor> listaEmpreendedores = Empreendedor.getEmpreendedores(cpfCnpj);


		List<Empreendedor> listaFinal = listaEmpreendedores.stream().map(empreendedor1 -> empreendedor1).distinct().collect(Collectors.toList());

		Long numeroItems = Long.valueOf(listaFinal.size());
		
		Pagination<Empreendedor> pagination = new Pagination<>(numeroItems, listaFinal);

		renderJSON(pagination, EmpreendedorSerializer.findByFilter);
	}
}
