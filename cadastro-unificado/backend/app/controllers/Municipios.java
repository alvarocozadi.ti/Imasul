package controllers;

import models.Municipio;
import models.permissoes.AcaoSistema;
import play.mvc.With;
import serializers.MunicipiosSerializer;

@With(Secure.class)
public class Municipios extends BaseController {

	public static void getMunicipioGeometryById(Integer id) {

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_MUNICIPIOS);

		}

		Municipio municipio = Municipio.findById(id);
		renderJSON(municipio, MunicipiosSerializer.getMunicipioGeometryById);

	}

}
