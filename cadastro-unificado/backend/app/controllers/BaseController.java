package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSerializer;
import com.vividsolutions.jts.geom.Geometry;
import deserializers.*;
import exceptions.BaseException;
import exceptions.ValidationException;
import flexjson.JSONSerializer;
import models.Pessoa;
import models.Tipo;
import play.Logger;
import play.Play;
import play.db.jpa.JPA;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Catch;
import play.mvc.Controller;
import results.Error;
import results.Redirect;
import results.RenderJson;
import results.Unauthorized;

import java.util.Date;

import org.hibernate.Session;

public abstract class BaseController extends Controller {

	protected static Gson gson;

	static {

		GsonBuilder builder = new GsonBuilder()
				.serializeSpecialFloatingPointValues()
				.registerTypeAdapter(Date.class, new DateDeserializer())
				.registerTypeAdapter(String.class, new StringDeserializer())
				.registerTypeHierarchyAdapter(Tipo.class, new TipoEnumDeserializer())
				.registerTypeAdapter(Pessoa.class, new PessoaDeserializer())
				.registerTypeAdapter(Geometry.class, new GeometryDeserializer());

		gson = builder.create();

	}

	public static boolean isPublicResource() {

		return request.path.contains(Play.configuration.getProperty("authentication.url.public"));

	}

	protected static boolean isExternalResource() {

		return request.path.contains(Play.configuration.getProperty("authentication.url.external"));

	}

	protected static boolean isOwnResource() {

		return !isPublicResource() && !isExternalResource();

	}

	/**
	 * Tratamento de excessão do tipo ValidationException.
	 *
	 * A mensagem de erro para o usuário estará contida na exceção
	 *
	 * É retornado o status 422 e o texto de mensagem.
	 *
	 * @param exception
	 *            Excessão capturada.
	 */
	@Catch(value = ValidationException.class, priority = 1)
	protected static void returnIfExceptionRaised(ValidationException exception) {

		Logger.error(exception, exception.getMessage());

		JPA.setRollbackOnly();

		error(422, exception.getUserMessage());

	}

	/**
	 * Tratamento de excessão do tipo BaseException.
	 *
	 * A mensagem de erro para o usuário estará contida na exceção,
	 * caso contrário será utilizada uma mensagem padrão.
	 *
	 * É retornado o status 504 e o texto de mensagem.
	 *
	 * @param exception
	 *            Excessão capturada.
	 */
	@Catch(value = BaseException.class, priority = 2)
	protected static void returnIfExceptionRaised(BaseException exception) {

		Logger.error(exception, exception.getMessage());

		JPA.setRollbackOnly();

		error(504, exception.getUserMessage());

	}

	/**
	 * Tratamento de excessão geral.
	 *
	 * As exceções não tratadas que chegam à controller serão tratadas por este método.
	 *
	 * É retornado o status 500 e o texto de mensagem.
	 *
	 * @param exception
	 *            Excessão capturada.
	 */
	@Catch(value = Exception.class, priority = 3)
	protected static void returnIfExceptionRaised(Exception exception) {

		Logger.error(exception, exception.getMessage());

		JPA.setRollbackOnly();

		error(Messages.get("erro.padrao"));

	}

	/**
	 * Render a 200 OK application/json response.
	 *
	 * @param model
	 *            The Java object to serialize
	 *
	 */
	protected static void renderJSON(Object model) {
		throw new RenderJson(model);
	}

	/**
	 * Render a 200 OK application/json response.
	 *
	 * @param model
	 *            The Java object to serialize
	 * @param adapters
	 *            A set of GSON serializers/deserializers/instance creator to use
	 *
	 */
	protected static void renderJSON(Object model, JsonSerializer<?>... adapters) {
		throw new RenderJson(model, adapters);
	}

	/**
	 * Render a 200 OK application/json response.
	 *
	 * @param model
	 *            The Java object to serialize
	 * @param jsonSerializer
	 *            A Flexjson serializers to use
	 *
	 */
	protected static void renderJSON(Object model, JSONSerializer jsonSerializer) {
		throw new RenderJson(model, jsonSerializer);
	}

	/**
	 * Send a 401 Unauthorized response
	 */
	protected static void unauthorized() {
		throw new Unauthorized();
	}

	/**
	 * Send a 307 Redirect response
	 */
	protected static void frontEndRedirect(String url) {
		throw new Redirect(url);
	}

	/**
	 * Send a 5xx Error response
	 *
	 * @param status
	 *            The exact status code
	 * @param reason
	 *            The reason
	 */
	protected static void error(int status, String reason) {
		throw new Error(status, reason);
	}

	/**
	 * Send a 500 Error response
	 *
	 * @param reason
	 *            The reason
	 */
	protected static void error(String reason) {
		throw new Error(reason);
	}

	/**
	 * Send a 500 Error response
	 *
	 * @param reason
	 *            The reason
	 */
	protected static void error(Exception reason) {

		Logger.error(reason, "error()");
		throw new Error(reason.toString());

	}

	/**
	 * Send a 500 Error response
	 */
	protected static void error() {
		throw new Error("Internal Error");
	}

	@Before
	protected static void setFilters() {

		((Session) JPA.em().getDelegate()).enableFilter("entityRemovida").setParameter("removido", false);

	}

}
