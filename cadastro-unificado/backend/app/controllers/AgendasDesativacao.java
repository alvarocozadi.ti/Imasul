package controllers;

import common.models.Message;
import models.portalSeguranca.AgendaDesativacao;
import org.apache.commons.lang.time.DateUtils;
import play.mvc.With;
import services.ExternalUsuarioService;

import java.util.Date;

@With(Secure.class)
public class AgendasDesativacao extends BaseController {

	public static void create(AgendaDesativacao agendaDesativacao) {

		ExternalUsuarioService.deactivate(agendaDesativacao);

		if(agendaDesativacao.dataInicio != null) {

			Date dataAtual = new Date();

			if(agendaDesativacao.dataInicio.after(dataAtual) && !DateUtils.isSameDay(agendaDesativacao.dataInicio, dataAtual)) {

				renderJSON(new Message("agenda.cadastro.sucesso"));

			}

		}

		renderJSON(new Message("usuario.desativado.sucesso"));

	}

}