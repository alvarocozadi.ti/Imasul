package controllers;

import models.Configuracao;
import serializers.ConfiguracoesSerializer;

public class Configuracoes extends BaseController {

	public static void get() {

		Configuracao config = new Configuracao();

		renderJSON(config, ConfiguracoesSerializer.get);

	}

}
