package controllers;

import com.ning.http.client.HttpResponseBodyPart;
import common.models.Message;
import common.models.Pagination;
import models.FiltroPessoa;
import models.Pessoa;
import models.integracaoSiriema.PessoaSiriemaVO;
import models.integracaoSiriema.SiriemaWS;
import models.portalSeguranca.Perfil;
import play.mvc.Http;
import play.mvc.With;
import serializers.PessoasFisicasSerializer;
import serializers.PessoasSerializer;
import services.ExternalAuthorizationService;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.RequestWrapper;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@With(Secure.class)
public class Pessoas extends BaseController {

	private static Object pessoa;

	public static void getPessoasAtualizadas(Date dataAtualizacao, String cpfCnpj) {

		List<Pessoa> pessoasAtualizadas = new ArrayList<>();

		if(cpfCnpj != null) {

			Pessoa pessoa = Pessoa.getAtualizacaoByDataAndCpfCnpj(dataAtualizacao, cpfCnpj);

			if(pessoa != null) {

				pessoasAtualizadas.add(pessoa);

			}

		} else {

			pessoasAtualizadas.addAll(Pessoa.getPessoasAtualizadasByData(dataAtualizacao));

		}

		renderJSON(pessoasAtualizadas, PessoasSerializer.getPessoasAtualizadas);

	}

	public static void verificaPessoaCadastradaSiriema(String cpfCnpj){

		SiriemaWS siriemaWS = new SiriemaWS();
		
		renderJSON(siriemaWS.verificaPessoaSiriema(cpfCnpj));
	}

	public static void findPessoasAtualizadasByCpfsCnpjs(Date dataAtualizacao, String cpfsCnpjs) {

		List<Pessoa> pessoasAtualizadas = new ArrayList<>(Pessoa.getPessoasAtualizadasByDataAndCpfCnpjList(dataAtualizacao, cpfsCnpjs));

		renderJSON(pessoasAtualizadas, PessoasSerializer.getPessoasAtualizadas);

	}

	public static void createPessoaPrimeiroAcesso(PessoaSiriemaVO pessoaSiriemaVO) throws ParseException {

		notFoundIfNull(pessoaSiriemaVO);

		pessoaSiriemaVO.convertToPessoaEU(false);

		renderJSON(new Message("pessoa.cadastrar.sucesso"));

	}

	public static void findByFilterIsUsuarios(FiltroPessoa filtroPessoa) {

		List<Pessoa> pessoas = Pessoa.findByFilterIsUsuarios(filtroPessoa);

		Long numeroItems = Pessoa.countByFilterIsUsuarios(filtroPessoa);

		Pagination<Pessoa> pagination = new Pagination<>(numeroItems, pessoas);

		renderJSON(pagination, PessoasFisicasSerializer.externalFindByFilter);

	}

	/**
	 * A diferença desse metodo para o "findByFilterIsUsuario" é que ele não depende do modulo que está pedindo e também o cpf/cnpj não precisa ser de um usuário,
	 * ou seja ira retorna o resultado sem levar em consideração se o usuario/pessoa pertence ao modulo que está fazendo a requisição.
	 * @param filtroPessoa
	 */
	public static void findByFilterAllModule(FiltroPessoa filtroPessoa) {

		List<Pessoa> pessoas = Pessoa.findByFilterAllModule(filtroPessoa);

		Long numeroItems = Pessoa.countByFilterAllModule(filtroPessoa);

		Pagination<Pessoa> pagination = new Pagination<>(numeroItems, pessoas);

		renderJSON(pagination, PessoasFisicasSerializer.externalFindByFilter);

	}

	/**
	 * Método para buscar todos as pessoas SEM os dados de usuário
	 * A diferença desse metodo para o "findByFilterIsUsuario" é que ele não depende do modulo que está pedindo e também o cpf/cnpj não precisa ser de um usuário,
	 * ou seja ira retorna o resultado sem levar em consideração se o usuario/pessoa pertence ao modulo que está fazendo a requisição.
	 * @param filtroPessoa
	 */
	public static void buscarPessoasFiltroNovoComFiltroAll(FiltroPessoa filtroPessoa) {

		List<Pessoa> pessoas = Pessoa.findByFilterAllModule(filtroPessoa);

		Long numeroItems = Pessoa.countByFilterAllModule(filtroPessoa);

		Pagination<Pessoa> pagination = new Pagination<>(numeroItems, pessoas);

		renderJSON(pagination, PessoasFisicasSerializer.findByFilterNoUsers);

	}


	public static void updateOrCreate(Pessoa pessoa) {

		notFoundIfNull(pessoa);

		String retorno = pessoa.updateOrCreate(false);

		renderJSON(new Message(retorno));

	}

	public static void updateProfile(String cpfCnpj, String codigoPerfil) {

		notFoundIfNull(cpfCnpj, codigoPerfil);

		Integer idModule = ExternalAuthorizationService.findIdModuloByToken(Http.Request.current().headers.get("authorization").value());

		notFoundIfNull(idModule);

		Perfil perfil = ExternalAuthorizationService.getPerfilByCodigoAndModule(codigoPerfil, idModule);

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		String retorno = null;

		if (pessoa != null && pessoa.isUsuario) {

			if(pessoa.usuario.perfis.contains(perfil)) {

				pessoa.usuario.perfis.remove(perfil);
			} else {

				pessoa.usuario.perfis.add(perfil);
			}

			retorno = pessoa.updateOrCreate(false);
		} else {

			retorno = "Não foi possível encontrar nenhum usuário com o CPF/CNPJ informado";
		}


		renderJSON(new Message(retorno));
	}

	public static void addProfile(String cpfCnpj, String codigoPerfil) {

		notFoundIfNull(cpfCnpj, codigoPerfil);

		Integer idModule = ExternalAuthorizationService.findIdModuloByToken(Http.Request.current().headers.get("authorization").value());

		notFoundIfNull(idModule);

		Perfil perfil = ExternalAuthorizationService.getPerfilByCodigoAndModule(codigoPerfil, idModule);

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		String retorno = null;

		if (pessoa != null && pessoa.isUsuario) {

			if(!pessoa.usuario.perfis.contains(perfil)) {

				pessoa.usuario.perfis.add(perfil);
			}

			retorno = pessoa.updateOrCreate(false);

		} else {

			retorno = "Não foi possível encontrar nenhum usuário com o CPF/CNPJ informado";
		}

		renderJSON(new Message(retorno));
	}

	public static void removeProfile(String cpfCnpj, String codigoPerfil) {

		notFoundIfNull(cpfCnpj, codigoPerfil);

		Integer idModule = ExternalAuthorizationService.findIdModuloByToken(Http.Request.current().headers.get("authorization").value());

		notFoundIfNull(idModule);

		Perfil perfil = ExternalAuthorizationService.getPerfilByCodigoAndModule(codigoPerfil, idModule);

		Pessoa pessoa = Pessoa.findByCpfCnpj(cpfCnpj);

		String retorno = null;

		if (pessoa != null && pessoa.isUsuario) {

			if(pessoa.usuario.perfis.contains(perfil)) {

				pessoa.usuario.perfis.remove(perfil);
			}

			retorno = pessoa.updateOrCreate(false);

		} else {

			retorno = "Não foi possível encontrar nenhum usuário com o CPF/CNPJ informado";
		}

		renderJSON(new Message(retorno));
	}

}
