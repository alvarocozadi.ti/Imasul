package controllers;

import models.InformacaoValidacao;
import models.Municipio;
import models.NomePessoa;
import play.mvc.With;
import serializers.InformacoesValidacaoSerializer;

@With(Secure.class)
public class InformacoesValidacao extends BaseController {

	public static void getInformacoesByCpf(String cpf) {

		notFoundIfNull(cpf);

		InformacaoValidacao informacoes = new InformacaoValidacao();

		informacoes.nomesPessoasFisicas = NomePessoa.getNomesFromCpf(cpf);
		informacoes.nomesMaes = NomePessoa.getNomesMaes(cpf);

		renderJSON(informacoes, InformacoesValidacaoSerializer.getByCpf);

	}

	public static void getInformacoesByCnpj(String cnpj) {

		notFoundIfNull(cnpj);

		InformacaoValidacao informacoes = new InformacaoValidacao();

		informacoes.nomesPessoasJuridicas = NomePessoa.getNomesFromCnpj(cnpj);
		informacoes.nomesMunicipios = Municipio.getNomesFromCnpj(cnpj);
		informacoes.estadoMunicipio = Municipio.getSiglaEstadoFromCnpj(cnpj);

		renderJSON(informacoes, InformacoesValidacaoSerializer.getByCnpj);

	}

}