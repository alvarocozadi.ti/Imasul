package controllers;

import common.models.Message;
import common.models.Pagination;
import exceptions.ValidationException;
import models.DadosValidacao;
import models.FiltroPessoaFisica;
import models.PessoaFisica;
import models.permissoes.AcaoSistema;
import models.portalSeguranca.Usuario;
import play.mvc.With;
import serializers.PessoasFisicasSerializer;
import services.ExternalUsuarioService;

import java.util.List;

@With(Secure.class)
public class PessoasFisicas extends BaseController {

	public static void create(PessoaFisica pessoaFisica) {

		notFoundIfNull(pessoaFisica);

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.CADASTRAR_PESSOA_FISICA);

		} else if(isPublicResource()) {

			Usuario.validatePublicCreate(pessoaFisica.usuario);

		}

		pessoaFisica.save();

		Message message = new Message("pessoa.cadastrar.sucesso");

		if(pessoaFisica.isUsuario) {

			pessoaFisica.usuario.save(message, pessoaFisica.id == null);

		}

		renderJSON(message);

	}

	public static void findByFilter(FiltroPessoaFisica filtro) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.FILTRAR_PESSOA_FISICA);
		}

		notFoundIfNull(filtro);

		List<PessoaFisica> pessoas = PessoaFisica.findByFilter(filtro);
		Long numeroItems = PessoaFisica.countByFilter(filtro);

		Pagination<PessoaFisica> pagination = new Pagination<>(numeroItems, pessoas);

		renderJSON(pagination, PessoasFisicasSerializer.findByFilter);

	}

	public static void findByCpf(String cpf) {

		Secure.executeAuthorization(AcaoSistema.Acao.VISUALIZAR_PESSOA_FISICA);

		notFoundIfNull(cpf);

		PessoaFisica pessoa = PessoaFisica.find("cpf = :cpf").setParameter("cpf", cpf.replaceAll("[.-]", "")).first();

		renderJSON(pessoa, PessoasFisicasSerializer.find);

	}

	public static void remove(Integer id) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.REMOVER_PESSOA_FISICA);
		}

		PessoaFisica pessoaFisicaRecuperada = PessoaFisica.findById(id);

		notFoundIfNull(pessoaFisicaRecuperada);

		pessoaFisicaRecuperada.remove();

		renderJSON(new Message("pessoaFisica.removido.sucesso"));

	}

	public static void find(Integer id) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.VISUALIZAR_PESSOA_FISICA);
		}

		notFoundIfNull(id);

		PessoaFisica pessoa = PessoaFisica.findById(id);

		renderJSON(pessoa, PessoasFisicasSerializer.find);

	}


	public static void findToUpdateCadastro(Integer id) {

		notFoundIfNull(id);

		PessoaFisica pessoaFisica = PessoaFisica.findById(id);

		if(!Secure.getAuthenticatedUser().login.equals(pessoaFisica.cpf)) {

			unauthorized();

		}

		renderJSON(pessoaFisica, PessoasFisicasSerializer.find);

	}

	public static void update(PessoaFisica pessoaFisica) {

		notFoundIfNull(pessoaFisica);

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.EDITAR_PESSOA_FISICA);

		} else if(isPublicResource()) {

			Usuario.validatePublicCreate(pessoaFisica.usuario);

		}

		PessoaFisica pessoaFisicaEditar = PessoaFisica.findById(pessoaFisica.id);

		notFoundIfNull(pessoaFisicaEditar, new Message("pessoaFisica.buscar.naoEncontrado").getText());

		pessoaFisicaEditar.update(pessoaFisica);

		Message message = new Message("pessoa.editar.sucesso");

		if(pessoaFisica.isUsuario) {

			Usuario.updateOrSave(pessoaFisica.usuario, message, pessoaFisica.id == null);

		}

		renderJSON(message);

	}

	public static void validateToCreate(DadosValidacao dadosValidacao) {

		notFoundIfNull(dadosValidacao);

		PessoaFisica pessoaFisica = PessoaFisica.find("cpf", dadosValidacao.cpf).first();

		if(pessoaFisica == null) {

			throw new ValidationException().userMessage("dadosValidacao.validar.erro");

		}

		pessoaFisica.validateToCreate(dadosValidacao);

		ok();

	}

	public static void publicFindByCpf(String cpf) {

		notFoundIfNull(cpf);

		PessoaFisica pessoa = PessoaFisica.find("cpf = :cpf").setParameter("cpf", cpf.replaceAll("[.-]", "")).first();

		renderJSON(pessoa, PessoasFisicasSerializer.find);

	}

	public static void publicHasPessoaWithCpf(String cpf) {

		notFoundIfNull(cpf);

		renderJSON(PessoaFisica.count("cpf", cpf.replaceAll("[.-]", "")) == 1);

	}

	public static void publicFind(Integer id) {

		notFoundIfNull(id);

		PessoaFisica pessoa = PessoaFisica.findById(id);

		if(ExternalUsuarioService.verifyAuthenticatedUser(pessoa.cpf, request)) {

			renderJSON(pessoa, PessoasFisicasSerializer.find);

		}

		unauthorized();

	}

	public static void publicUpdate(PessoaFisica pessoaFisica) {

		notFoundIfNull(pessoaFisica);

		PessoaFisica pessoaFisicaEditar = PessoaFisica.findById(pessoaFisica.id);

		notFoundIfNull(pessoaFisicaEditar, new Message("pessoaFisica.buscar.naoEncontrado").getText());

		pessoaFisicaEditar.update(pessoaFisica);

		renderJSON(new Message("pessoa.editar.sucesso"));

	}

	public static void externalFindByCpf(String cpf) {

		notFoundIfNull(cpf);

		PessoaFisica pessoa = PessoaFisica.find("cpf = :cpf").setParameter("cpf", cpf.replaceAll("[.-]", "")).first();

		renderJSON(pessoa, PessoasFisicasSerializer.externalFindByCpf);

	}

}
