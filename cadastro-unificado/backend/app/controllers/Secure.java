package controllers;

import models.permissoes.AcaoSistema;
import models.portalSeguranca.Usuario;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.mvc.Before;
import secure.factories.AuthenticationServiceFactory;
import secure.models.Permissible;
import secure.models.User;
import secure.services.AuthorizationService;
import services.ExternalAuthorizationService;

public class Secure extends BaseController {

	private static AuthenticationServiceFactory authenticationServiceFactory = new AuthenticationServiceFactory();

	@Before(unless = { "login", "logout", "authenticate" })
	public static void isAuthenticated() {

		if (isPublicResource()) {

			return;

		} else if (isExternalResource()) {

			ExternalAuthorizationService.isAccessTokenValid(request);

		} else {

			if(request.cookies.get(Play.configuration.getProperty("portalSeguranca.session.cookie")) == null) {

				Logger.debug("Permissão negada! Usuario não autenticado.");

				clearUserSession();

				unauthorized();

			} else {

				User usuario = getAuthenticatedUser();

				if (usuario == null) {

					usuario = (User) authenticationServiceFactory.getInstance().authenticate(request, session);

					if (usuario == null || usuario.getId() == null) {

						Logger.debug("Permissão negada! Usuario não autenticado.");

						clearUserSession();

						unauthorized();

					}

					Cache.set(session.getId(), usuario, Play.configuration.getProperty("application.session.maxAge"));

				}

			}

		}

	}

	private static void clearUserSession() {

		Cache.delete(session.getId());

		session.clear();

	}

	public static Usuario getAuthenticatedUser() {

		Logger.debug("ID da Sessão: %s", new Object[]{session.getId()});

		return Cache.get(session.getId(), Usuario.class);

	}

	public static void login(){

		redirect(Play.configuration.getProperty("portalSeguranca.url"));

	}

	public static void logout(){

		clearUserSession();

		redirect(Play.configuration.getProperty("portalSeguranca.url") + "/logout");

	}

	protected static void executeAuthorization(AcaoSistema.Acao... acoesSistema) {

		executeAuthorization(null, acoesSistema);

	}

	protected static void executeAuthorization(Permissible permissible, AcaoSistema.Acao... acoesSistema) {

		for (AcaoSistema.Acao acao : acoesSistema) {

			AcaoSistema acaoSistema = new AcaoSistema(acao);

			if (AuthorizationService.getInstance().checkPermission(acaoSistema, Secure.getAuthenticatedUser(), permissible)) {

				return;

			}

		}

		forbidden();

	}

	public static void authenticate(String sessionKeyEntradaUnica) {

		Usuario usuario = (Usuario) authenticationServiceFactory.getInstance().handShake(request, sessionKeyEntradaUnica);

		if (usuario == null || usuario.getId() == null) {

			Logger.debug("Permissão negada! Usuario não autenticado.");

			clearUserSession();

			login();

		}

		Cache.set(session.getId(), usuario, Play.configuration.getProperty("application.session.maxAge"));

		redirect(Play.configuration.getProperty("http.path"));

	}

}
