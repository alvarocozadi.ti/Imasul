package controllers;

import models.portalSeguranca.Motivo;
import play.mvc.With;
import serializers.MotivosSerializer;

import java.util.List;

@With(Secure.class)
public class Motivos extends BaseController {

	public static void findAll() {

		List<Motivo> lista = Motivo.findAll();

		renderJSON(lista, MotivosSerializer.findAll);

	}

}
