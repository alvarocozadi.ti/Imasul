package controllers;

import models.TipoContato;
import models.permissoes.AcaoSistema;
import play.mvc.With;
import serializers.TiposContatoSerializer;

import java.util.List;

@With(Secure.class)
public class TiposContato extends BaseController {

	public static void find() {

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_TIPOS_CONTATO);

		}

		List<TipoContato> lista = TipoContato.findAll();

		renderJSON(lista, TiposContatoSerializer.find);

	}

}