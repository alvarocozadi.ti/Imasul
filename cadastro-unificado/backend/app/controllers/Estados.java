package controllers;

import models.Estado;
import models.permissoes.AcaoSistema;
import play.mvc.With;
import serializers.EstadosSerializer;

@With(Secure.class)
public class Estados extends BaseController {

	public static void findMunicipios(Integer id) {

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_MUNICIPIOS);

		}

		Estado estados = Estado.findById(id);

		renderJSON(estados.municipios, EstadosSerializer.findMunicipios);

	}

}
