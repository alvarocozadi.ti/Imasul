package controllers;

import play.mvc.With;
import services.ExternalPerfilService;

@With(Secure.class)
public class Perfis extends BaseController {

	public static void findAll() {

		renderJSON(ExternalPerfilService.findAll());

	}

}
