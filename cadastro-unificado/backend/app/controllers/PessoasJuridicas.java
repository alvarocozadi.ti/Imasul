package controllers;

import common.models.Message;
import common.models.Pagination;
import exceptions.ValidationException;
import models.DadosValidacao;
import models.FiltroPessoaJuridica;
import models.PessoaJuridica;
import models.permissoes.AcaoSistema;
import models.portalSeguranca.Usuario;
import play.mvc.With;
import serializers.PessoasJuridicasSerializer;
import services.ExternalUsuarioService;
import services.ProcessamentoRedesimService;

import java.util.List;

@With(Secure.class)
public class PessoasJuridicas extends BaseController {

	public static void create(PessoaJuridica pessoaJuridica) {

		notFoundIfNull(pessoaJuridica);

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.CADASTRAR_PESSOA_JURIDICA);

		} else if(isPublicResource()) {

			Usuario.validatePublicCreate(pessoaJuridica.usuario);

		}

		pessoaJuridica.save();

		Message message = new Message("pessoa.cadastrar.sucesso");

		if(pessoaJuridica.isUsuario) {

			pessoaJuridica.usuario.save(message, pessoaJuridica.id == null);

		}

		renderJSON(message);

	}

	public static void find(Integer id) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.VISUALIZAR_PESSOA_JURIDICA);
		}

		notFoundIfNull(id);

		PessoaJuridica pessoaJuridica = PessoaJuridica.findById(id);

		renderJSON(pessoaJuridica, PessoasJuridicasSerializer.find);

	}

	public static void findToUpdateCadastro(Integer id) {

		notFoundIfNull(id);

		PessoaJuridica pessoaJuridica = PessoaJuridica.findById(id);

		if(!Secure.getAuthenticatedUser().login.equals(pessoaJuridica.cnpj)) {

			unauthorized();

		}

		renderJSON(pessoaJuridica, PessoasJuridicasSerializer.find);

	}


	public static void findByFilter(FiltroPessoaJuridica filtro) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.LISTAR_PESSOAS_JURIDICAS);
		}

		notFoundIfNull(filtro);

		List<PessoaJuridica> pageItems = PessoaJuridica.findByFilter(filtro);
		Long totalItems = PessoaJuridica.countByFilter(filtro);

		Pagination<PessoaJuridica> pagination = new Pagination<>(totalItems, pageItems);

		renderJSON(pagination, PessoasJuridicasSerializer.findByFilter);

	}

	public static void findByCnpj(String cnpj) {

		if(!isExternalResource()) {
			Secure.executeAuthorization(AcaoSistema.Acao.VISUALIZAR_PESSOA_JURIDICA);
		}

		notFoundIfNull(cnpj);

		PessoaJuridica pessoa = PessoaJuridica.findByCnpj(cnpj);

		renderJSON(pessoa, PessoasJuridicasSerializer.findByCnpj);

	}

	public static void remove(Integer id) {

		if(!isExternalResource()) {

//			Secure.executeAuthorization(AcaoSistema.Acao.REMOVER_PESSOA_JURIDICA);
			forbidden();
		}

		PessoaJuridica pessoaJuridica = PessoaJuridica.findById(id);

		notFoundIfNull(pessoaJuridica);

		pessoaJuridica.remove();

		renderJSON(new Message("pessoaJuridica.removido.sucesso"));

	}

	public static void update(PessoaJuridica pessoaJuridica) {

		notFoundIfNull(pessoaJuridica);

		if(isOwnResource()) {

			Secure.executeAuthorization(AcaoSistema.Acao.EDITAR_PESSOA_JURIDICA);

		} else if(isPublicResource()) {

			Usuario.validatePublicCreate(pessoaJuridica.usuario);

		}

		PessoaJuridica pessoaJuridicaEditar = PessoaJuridica.findById(pessoaJuridica.id);

		notFoundIfNull(pessoaJuridicaEditar, new Message("pessoaJuridica.buscar.naoEncontrado").getText());

		pessoaJuridicaEditar.update(pessoaJuridica);

		Message message = new Message("pessoa.editar.sucesso");

		if(pessoaJuridica.isUsuario) {

			Usuario.updateOrSave(pessoaJuridica.usuario, message, pessoaJuridica.id == null);
		}

		renderJSON(message);

	}

	public static void validateToCreate(DadosValidacao dadosValidacao) {

		notFoundIfNull(dadosValidacao);

		PessoaJuridica pessoaJuridica = PessoaJuridica.findByCnpj(dadosValidacao.cnpj);

		if(pessoaJuridica == null) {

			throw new ValidationException().userMessage("dadosValidacao.validar.erro");

		}

		pessoaJuridica.validateToCreate(dadosValidacao);

		ok();

	}

	public static void publicFindByCnpj(String cnpj) {

		notFoundIfNull(cnpj);

		PessoaJuridica pessoa = PessoaJuridica.findByCnpj(cnpj);

		renderJSON(pessoa, PessoasJuridicasSerializer.findByCnpj);

	}

	public static void publicHasPessoaWithCnpj(String cnpj) {

		renderJSON(PessoaJuridica.countByCnpj(cnpj) == 1);

	}

	public static void publicFind(Integer id) {

		notFoundIfNull(id);

		PessoaJuridica pessoa = PessoaJuridica.findById(id);

		if(ExternalUsuarioService.verifyAuthenticatedUser(pessoa.cnpj, request)) {

			renderJSON(pessoa, PessoasJuridicasSerializer.find);

		}

		unauthorized();

	}

	public static void publicUpdate(PessoaJuridica pessoaJuridica) {

		notFoundIfNull(pessoaJuridica);

		PessoaJuridica pessoaJuridicaEditar = PessoaJuridica.findById(pessoaJuridica.id);

		notFoundIfNull(pessoaJuridicaEditar, new Message("pessoaJuridica.buscar.naoEncontrado").getText());

		pessoaJuridicaEditar.update(pessoaJuridica);

		renderJSON(new Message("pessoa.editar.sucesso"));

	}

	public static void externalfindByCnpj(String cnpj) {

		notFoundIfNull(cnpj);

		PessoaJuridica pessoa = PessoaJuridica.findByCnpj(cnpj);

		renderJSON(pessoa, PessoasJuridicasSerializer.externalFindByCnpj);

	}

	public static void dadosRedesimAtualizados(String cnpj) {

		if (!isExternalResource()) {
			forbidden();
		}

		boolean atualizado;

		try {

			ProcessamentoRedesimService.atualizarDadosPessoasJuridicas(cnpj);

			// Retorna TRUE se atualização der êxito
			atualizado = true;

		} catch (Exception e) {

			e.printStackTrace();

			// Retorna FALSE se ocorrer erro ao atualizar via WSE031 sob demanda
			atualizado = false;

		}

		renderJSON(atualizado);

	}

}
