package secure.services;

import java.io.Serializable;

import play.mvc.Http;
import play.mvc.Scope;
import secure.models.User;

public interface AuthenticationService extends Serializable {

	User authenticate(Http.Request request, Scope.Session session) throws Exception;

	Object login();

	Object logout();

}
