package secure.services;

public interface IAuthenticatedUser {

	public abstract Integer getId();

	public abstract String getName();

	public abstract String[] getRoles();

	public boolean hasRole(String role);

}
