package secure.services;

import play.mvc.Http;
import play.mvc.Scope;

import java.io.Serializable;

public interface IAuthenticationService extends Serializable {

    IAuthenticatedUser authenticate(Http.Request request, Scope.Session session);
    
    IAuthenticatedUser getAuthenticatedUser(Http.Request request, Scope.Session session);
    
    IAuthenticatedUser handShake(Http.Request request, String sessionKey);
}