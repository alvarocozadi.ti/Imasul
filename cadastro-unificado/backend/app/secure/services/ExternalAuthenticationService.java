package secure.services;

import java.io.Serializable;

import play.mvc.Http;
import play.mvc.Scope;

public interface ExternalAuthenticationService extends Serializable {

	void authenticate(Http.Request request, Scope.Session session) throws Exception;

}
