package secure.models;

import java.io.Serializable;

public interface User extends Serializable {

	Integer getId();

	String getName();

	String[] getRoles();

	boolean hasRole(String role);

}
