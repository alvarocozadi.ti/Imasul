package secure.models;


public interface Action {

	Integer getId();
	String getDescription();
	String getRole();
	String getRules();
	String getRulesPackage();

}
