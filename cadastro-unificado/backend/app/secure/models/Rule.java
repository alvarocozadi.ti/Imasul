package secure.models;

public interface Rule {

	boolean check(User user, Permissible permissible);
	
}
