package deserializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import models.Pessoa;
import models.PessoaFisica;
import models.PessoaJuridica;
import models.Tipo;
import models.TipoPessoa;

import java.lang.reflect.Type;
import java.util.Date;

public class PessoaDeserializer implements JsonDeserializer<Pessoa> {

	private Gson gson = new GsonBuilder()
			.registerTypeAdapter(Date.class, new DateDeserializer())
			.registerTypeHierarchyAdapter(Tipo.class, new TipoEnumDeserializer())
			.create();

	@Override
	public Pessoa deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

		JsonObject jsonObject = (JsonObject) jsonElement;
		JsonObject tipo = jsonObject.get("tipo").getAsJsonObject();
		Integer codigo = tipo.get("codigo").getAsInt();

		if(codigo.equals(TipoPessoa.FISICA.getCodigo())) {

			return gson.fromJson(jsonElement, PessoaFisica.class);

		} else {

			return gson.fromJson(jsonElement, PessoaJuridica.class);

		}

	}
}