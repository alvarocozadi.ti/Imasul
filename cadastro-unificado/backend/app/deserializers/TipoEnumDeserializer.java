package deserializers;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import models.Tipo;
import play.Logger;

public class TipoEnumDeserializer implements JsonDeserializer<Tipo> {

	@Override
	public Tipo deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

		if(json.isJsonObject()) {

			Class<Tipo> classeEnum = null;
			JsonObject jsonObject = (JsonObject) json;

			String nome = jsonObject.get("nome").getAsString();
			Integer codigo = jsonObject.get("codigo").getAsInt();

			try {

				classeEnum = (Class<Tipo>) Class.forName(type.getTypeName());

			} catch (ClassNotFoundException e) {
				Logger.debug(e,"Erro ao deserializar objeto: ");
				return null;
			}

			for(Tipo constant: classeEnum.getEnumConstants()) {

				if(constant.name().equals(nome) || constant.getCodigo().equals(codigo))
					return constant;

			}

		}

		return null;

	}

}