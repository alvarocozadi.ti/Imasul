package transformers;

import flexjson.transformer.AbstractTransformer;
import models.Tipo;

public class TipoEnumTransformer extends AbstractTransformer {

	@Override
	public void transform(Object object) {

		Tipo tipo = (Tipo) object;

		this.getContext().writeOpenObject();
		this.getContext().writeName("nome");
		this.getContext().writeQuoted(tipo.name());
		this.getContext().writeComma();
		this.getContext().writeName("codigo");
		this.getContext().write(tipo.getCodigo().toString());
		this.getContext().writeComma();
		this.getContext().writeName("descricao");
		this.getContext().writeQuoted(tipo.getDescricao());
		this.getContext().writeCloseObject();

	}

}

