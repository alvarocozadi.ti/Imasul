package redesimples;

import br.ufla.lemaf.integracao.redesimples.WSE013.*;
import br.ufla.lemaf.integracao.redesimples.WSE031.WSE031Service;
import br.ufla.lemaf.integracao.redesimples.WSE031.Wse031RequestVo;
import common.models.Message;
import models.ConfirmaRespostaOrgaoRedeSimples;
import play.Logger;
import play.Play;
import utils.Config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class RedeSimplesWS {

	private static final String PREFIX = "SINCRONIA_REDE_SIMPLES";
	private static final String WSDL013_URL = Play.configuration.getProperty("integracao.redeSimples.wsdl.url");
	private final static String WSDL031_URL = Play.configuration.getProperty("integracao.redeSimples.ws31.url");
	private static final String ACCESS_KEY_ID = Play.configuration.getProperty("integracao.redeSimples.accessKeyId");
	private static final String SECRET_ACCESS_KEY = Play.configuration.getProperty("integracao.redeSimples.secretAccessKey");
	private static final Integer BUSCA_EMPREENDIMENTO_MAXIMO_REGISTROS = Integer.parseInt(Play.configuration.getProperty("integracao.redeSimples.buscaEmpreendimento.maximo"));
	private static final String BUSCA_EMPREENDIMENTO_VERSAO = Play.configuration.getProperty("integracao.redeSimples.buscaEmpreendimento.versao");

	/**
	 * Serviço para busca de empreendimentos.
	 */
	public static RetornoRedesimVo recuperaEstabelecimentos() {

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText());
		}

		Wse013RecuperaEstabelecimentosRequestVo recuperaEstabelecimentosRequest = new Wse013RecuperaEstabelecimentosRequestVo();
		recuperaEstabelecimentosRequest.setAccessKeyId(ACCESS_KEY_ID);
		recuperaEstabelecimentosRequest.setSecretAccessKey(SECRET_ACCESS_KEY);
		recuperaEstabelecimentosRequest.setMaximoRegistros(BUSCA_EMPREENDIMENTO_MAXIMO_REGISTROS);
		recuperaEstabelecimentosRequest.setVersao(BUSCA_EMPREENDIMENTO_VERSAO);

		RetornoRedesimVo retornoRedesimVo = null;

		try {

			retornoRedesimVo = service.getWSE013Port().recuperaEstabelecimentos(recuperaEstabelecimentosRequest);

		} catch (IntegradorEstadualException_Exception e) {

			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		verificarBusca(retornoRedesimVo.getStatus(), retornoRedesimVo.getMensagem());

		return retornoRedesimVo;
	}

	/**
	 * Serviço para informar recebimento de empreendimentos.
	 */
	public static WsRetornoVo informaRecebimento(List<String> identificadores) {

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText());
		}

		Wse013InformaRecebimentoRequestVo informaRecebimentoRequestVo = new Wse013InformaRecebimentoRequestVo();
		informaRecebimentoRequestVo.setAccessKeyId(ACCESS_KEY_ID);
		informaRecebimentoRequestVo.setSecretAccessKey(SECRET_ACCESS_KEY);
		informaRecebimentoRequestVo.getIdentificador().addAll(identificadores);

		WsRetornoVo wsRetornoVo = null;

		try {
			wsRetornoVo = service.getWSE013Port().informaRecebimento(informaRecebimentoRequestVo);
		} catch (IntegradorEstadualException_Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		verificarBusca(wsRetornoVo.getCodigoRetornoWSEnum(), wsRetornoVo.getMensagem());

		return wsRetornoVo;
	}

	/**
	 * Serviço para informar erro em empreendimentos.
	 */
	public static WsRetornoVo enviaInconsistencia(RegistroRedesimVo registro, String descricaoErro) {

		Logger.info(PREFIX + " Processando registro: " + registro.getIdentificador() + " - Enviar inconsistência - Início");

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText());
		}

		Wse013EnviaInconsistenciaRequestVo enviaInconsistenciaRequestVo = new Wse013EnviaInconsistenciaRequestVo();
		enviaInconsistenciaRequestVo.setAccessKeyId(ACCESS_KEY_ID);
		enviaInconsistenciaRequestVo.setSecretAccessKey(SECRET_ACCESS_KEY);
		enviaInconsistenciaRequestVo.setCnpj(registro.getDadosRedesim().getCnpj());
		enviaInconsistenciaRequestVo.setNumeroOrgaoRegistro(registro.getDadosRedesim().getNumeroOrgaoRegistro());
		enviaInconsistenciaRequestVo.setDescricaoInconsistencia(descricaoErro);

		WsRetornoVo wsRetornoVo = null;

		try {
			wsRetornoVo = service.getWSE013Port().enviaInconsistencia(enviaInconsistenciaRequestVo);
		} catch (IntegradorEstadualException_Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		verificarBusca(wsRetornoVo.getCodigoRetornoWSEnum(), wsRetornoVo.getMensagem(), enviaInconsistenciaRequestVo);

		Logger.info(PREFIX + " Processando registro: " + registro.getIdentificador() + " - Enviar inconsistência - Fim");

		return wsRetornoVo;
	}

	/**
	 * Serviço para obter o número de registros pendentes de integração
	 */
	public static RetornoQuantidadeDeRegistrosNaFilaVo recuperaQuantidadeDeRegistrosNaFilaNaoRespondidos() {

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText(), e);
		}

		Wse013RecuperaQuantidadeDeRegistrosNaFilaRequestVo recuperaQuantidadeDeRegistrosNaFilaRequestVo = new Wse013RecuperaQuantidadeDeRegistrosNaFilaRequestVo();
		recuperaQuantidadeDeRegistrosNaFilaRequestVo.setAccessKeyId(ACCESS_KEY_ID);
		recuperaQuantidadeDeRegistrosNaFilaRequestVo.setSecretAccessKey(SECRET_ACCESS_KEY);

		RetornoQuantidadeDeRegistrosNaFilaVo retornoQuantidadeDeRegistrosNaFilaVo = null;

		try {
			retornoQuantidadeDeRegistrosNaFilaVo = service.getWSE013Port().recuperaQuantidadeDeRegistrosNaFila(recuperaQuantidadeDeRegistrosNaFilaRequestVo);
		} catch (IntegradorEstadualException_Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		return retornoQuantidadeDeRegistrosNaFilaVo;
	}

	/**
	 * Serviço para busca de empreendimento pelo identificador da ação
	 */
	public static RetornoRedesimVo recuperaEstabelecimentoPorIdentificador(Long identificador) {

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText());
		}

		Wse013RecuperaEstabelecimentoPorIdentificadorRequestVo recuperaEstabelecimentoPorIdentificadorRequest = new Wse013RecuperaEstabelecimentoPorIdentificadorRequestVo();
		recuperaEstabelecimentoPorIdentificadorRequest.setAccessKeyId(ACCESS_KEY_ID);
		recuperaEstabelecimentoPorIdentificadorRequest.setSecretAccessKey(SECRET_ACCESS_KEY);
		recuperaEstabelecimentoPorIdentificadorRequest.setIdentificador(identificador);
		recuperaEstabelecimentoPorIdentificadorRequest.setVersao(BUSCA_EMPREENDIMENTO_VERSAO);

		RetornoRedesimVo retornoRedesimVo = null;

		try {
			retornoRedesimVo = service.getWSE013Port().recuperaEstabelecimentoPorIdentificador(recuperaEstabelecimentoPorIdentificadorRequest);
		} catch (IntegradorEstadualException_Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		verificarBusca(retornoRedesimVo.getStatus(), retornoRedesimVo.getMensagem());

		return retornoRedesimVo;
	}

	/**
	 * Serviço para informar confirmação de resposta do órgão
	 */
	public static WsRetornoVo confirmaRespostaOrgao(ConfirmaRespostaOrgaoRedeSimples confirmaRespostaOrgaoRedeSimples) {

		WSE013Service service = null;

		try {
			service = new WSE013Service(WSDL013_URL);
		} catch (Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText());
		}

		Wse013ConfirmaRespostaRequestVo confirmaRespostaRequest = new Wse013ConfirmaRespostaRequestVo();
		confirmaRespostaRequest.setAccessKeyId(ACCESS_KEY_ID);
		confirmaRespostaRequest.setSecretAccessKey(SECRET_ACCESS_KEY);
		confirmaRespostaRequest.setCnpj(confirmaRespostaOrgaoRedeSimples.cnpj);
		confirmaRespostaRequest.setCodSituacao(confirmaRespostaOrgaoRedeSimples.codSituacao);
		confirmaRespostaRequest.setNumeroLicenca(confirmaRespostaOrgaoRedeSimples.numeroLicenca);
		confirmaRespostaRequest.setDataExpedicao(confirmaRespostaOrgaoRedeSimples.dataExpedicao);
		confirmaRespostaRequest.setArquivoPDF(confirmaRespostaOrgaoRedeSimples.arquivoPDF);

		WsRetornoVo wsRetornoVo = null;

		try {
			wsRetornoVo = service.getWSE013Port().confirmaRespostaOrgao(confirmaRespostaRequest);
		} catch (IntegradorEstadualException_Exception e) {
			throw new RuntimeException(new Message("redeSimples.integracao.comunicacao.falha").getText() + " - " + e.getMessage());
		}

		verificarBusca(wsRetornoVo.getCodigoRetornoWSEnum(), wsRetornoVo.getMensagem());

		return wsRetornoVo;
	}

	/**
	 * Verifica se o retorno do serviço foi erro e exibe mensagem de erro.
	 */
	private static void verificarBusca(CodigoRetornoWSEnum codigoRetornoWSEnum, String mensagemRetorno) {

		if (codigoRetornoWSEnum.equals(CodigoRetornoWSEnum.NOK)) {
			throw new RuntimeException(new Message("redeSimples.integracao.busca.status.nok").getText() + " - " + mensagemRetorno);
		}
	}

	private static void verificarBusca(CodigoRetornoWSEnum codigoRetornoWSEnum, String mensagemRetorno, Wse013EnviaInconsistenciaRequestVo enviaInconsistenciaRequestVo) {

		if (codigoRetornoWSEnum.equals(CodigoRetornoWSEnum.NOK)) {
			Config.RedeSimplesLogger.trace("# Erro ao retornar a Rede Simples #");
			Config.RedeSimplesLogger.trace("CNPJ da Requisição: " + enviaInconsistenciaRequestVo.getCnpj());
			Config.RedeSimplesLogger.trace("Descrição da Requisição: " + enviaInconsistenciaRequestVo.getDescricaoInconsistencia());
			Config.RedeSimplesLogger.trace("Numero Orgao de Registro da Requisição: " + enviaInconsistenciaRequestVo.getNumeroOrgaoRegistro());
			Config.RedeSimplesLogger.trace("# Erro ao retornar a Rede Simples - Fim #\n");
			throw new RuntimeException(new Message("redeSimples.integracao.busca.status.nok").getText() + " - " + mensagemRetorno);
		}
	}

	public static RetornoRedesimVo getDadosCompletosCnpj(String cnpj)
			throws br.ufla.lemaf.integracao.redesimples.WSE031.IntegradorEstadualException_Exception {

		URL url = getUrlWSE031();
		WSE031Service service = new WSE031Service(url);

		Wse031RequestVo request = new Wse031RequestVo();
		request.setAccessKeyId(ACCESS_KEY_ID);
		request.setSecretAccessKey(SECRET_ACCESS_KEY);
		request.setVersao(BUSCA_EMPREENDIMENTO_VERSAO);
		request.setCnpj(cnpj);

		return service.getWSE031Port().wsE031(request).toWSE013();

	}

	private static URL getUrlWSE031() {

		URL url = null;

		try {
			 url = new URL(WSDL031_URL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return url;

	}

}
