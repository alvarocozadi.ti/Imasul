package validators;

import play.data.validation.Check;
import utils.CNPJDigitCalculator;

public class CNPJCheck extends Check {

	@Override
	public boolean isSatisfied(Object pessoaJuridica, Object value) {

		if(value == null) {

			return true;

		}

		String cnpj = value.toString();

		if (cnpj.length() != 14 || !cnpj.matches("[0-9]*")) {

			return false;

		}

		String cnpjNoDigits = cnpj.substring(0, cnpj.length() - 2);
		String digits = cnpj.substring(cnpj.length() - 2);
		String calculatedDigits = CNPJDigitCalculator.calculate(cnpjNoDigits);

		if (!digits.equals(calculatedDigits)) {

			return false;

		}

		return true;

	}

}