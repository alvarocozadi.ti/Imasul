package validators;

import exceptions.ValidationException;
import models.Pessoa;
import models.PessoaFisica;
import models.TipoPessoa;
import play.data.validation.Check;

import java.util.Collection;

public class EstrangeiroCheck extends Check {

	@Override
	public boolean isSatisfied(Object empreendimento, Object pessoas) {

		if(pessoas == null) {

			return true;

		}

		if (pessoas instanceof Collection) {

			for (Pessoa pessoa : (Collection<Pessoa>) pessoas) {

				if (pessoa.tipo == TipoPessoa.FISICA) {

					PessoaFisica pessoaFisica = PessoaFisica.findById(pessoa.id);

					if(pessoaFisica == null) {

						return false;

					}

					if (pessoaFisica.estrangeiro) {

						return false;

					}

				}

			}

		} else {

			if(((Pessoa)pessoas).tipo == TipoPessoa.FISICA) {

				if(((PessoaFisica) pessoas).estrangeiro) {

					return false;

				}

			}

		}

		return true;

	}

}
