package validators;

import play.data.validation.Check;
import utils.TituloEleitoralDigitCalculator;

public class TituloEleitoralCheck extends Check {

	@Override
	public boolean isSatisfied(Object pessoaFisica, Object value) {

		if (value == null)
			return true;

		String titulo = String.valueOf(value);

		if (titulo.length() != 12 || !titulo.matches("[0-9]*")) {
			return false;
		}

		if (this.hasCodigoDeEstadoInvalido(titulo)) {
			return false;
		} else {

			String tituloSemDigito = titulo.substring(0, titulo.length() - 2);
			String digitos = titulo.substring(titulo.length() - 2);

			String digitosCalculados = TituloEleitoralDigitCalculator.calculate(tituloSemDigito);

			if (!digitos.equals(digitosCalculados)) {
				return false;
			}

		}

		return true;

	}

	private boolean hasCodigoDeEstadoInvalido(String tituloDeEleitor) {

		int codigo = Integer.parseInt(tituloDeEleitor.substring(tituloDeEleitor.length() - 4, tituloDeEleitor.length() - 2));

		return !(codigo >= 01 && codigo <= 28);

	}

}
