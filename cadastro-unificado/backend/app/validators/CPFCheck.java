package validators;

import play.data.validation.Check;
import utils.CPFDigitCalculator;

public class CPFCheck extends Check {

	@Override
	public boolean isSatisfied(Object pessoaFisica, Object value) {

		if(value == null) {

			return true;

		}

		String cpf = value.toString();

		if (cpf.length() != 11 || !cpf.matches("[0-9]*") || cpf.matches("(\\d)\\1{10}")) {

			return false;

		}

		String cpfNoDigits = cpf.substring(0, cpf.length() - 2);
		String digits = cpf.substring(cpf.length() - 2);
		String calculatedDigits = CPFDigitCalculator.calculate(cpfNoDigits);

		if (!digits.equals(calculatedDigits)) {

			return false;

		}

		return true;

	}

}