package serializers;

import flexjson.JSONSerializer;
import play.Play;
import play.Play.Mode;

public class SecureSerializer {

	public static JSONSerializer authenticate;

	static {

		boolean prettyPrint = Play.mode == Mode.DEV;

		authenticate = new JSONSerializer()
			.include(
					"id",
					"login",
					"email",
					"nome",
					"pessoa.id",
					"pessoa.cpf",
					"pessoa.cnpj",
					"perfilSelecionado.id",
					"perfilSelecionado.nome",
					"perfilSelecionado.codigo",
					"perfilSelecionado.permissoes.id",
					"perfilSelecionado.permissoes.codigo"
			)
			.exclude("*")
			.prettyPrint(prettyPrint);

	}

}
