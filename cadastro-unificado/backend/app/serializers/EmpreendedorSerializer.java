package serializers;

import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.GeometryTransformer;
import transformers.TipoEnumTransformer;

public class EmpreendedorSerializer {

	public static JSONSerializer find;
	public static JSONSerializer findByFilter;
	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		find = new JSONSerializer()
				.include(
						"id",
						"dataCadastro",
						"ativo",
						"pessoa.cpf",
						"pessoa.cnpj",
						"pessoa.tipo",
						"pessoa.id",
						"pessoa.sexo.nome",
						"pessoa.sexo.codigo",
						"pessoa.sexo.descricao",
						"pessoa.enderecos.zonaLocalizacao.codigo",
						"pessoa.dataNascimento",
						"pessoa.estadoCivil.descricao",
						"pessoa.estadoCivil.nome",
						"pessoa.estadoCivil.codigo",
						"pessoa.nomeMae",
						"pessoa.nome",
						"pessoa.naturalidade",
						"pessoa.rg.numero",
						"pessoa.rg.orgaoExpedidor",
						"pessoa.tituloEleitoral.numero",
						"pessoa.tituloEleitoral.secao",
						"pessoa.tituloEleitoral.zona",
						"pessoa.cnpj",
						"pessoa.municipio.codigoIbge",
						"pessoa.nomeFantasia",
						"pessoa.razaoSocial",
						"pessoa.dataConstituicao",
						"pessoa.inscricaoEstadual",
						"pessoa.contatos.id",
						"pessoa.contatos.tipo.id",
						"pessoa.contatos.tipo.descricao",
						"pessoa.contatos.valor",
						"pessoa.contatos.principal",
						"pessoa.enderecos.id",
						"pessoa.enderecos.tipo.id",
						"pessoa.enderecos.cep",
						"pessoa.enderecos.logradouro",
						"pessoa.enderecos.numero",
						"pessoa.enderecos.semNumero",
						"pessoa.enderecos.zonaLocalizacao.codigo",
						"pessoa.enderecos.complemento",
						"pessoa.enderecos.bairro",
						"pessoa.enderecos.caixaPostal",
						"pessoa.enderecos.roteiroAcesso",
						"pessoa.enderecos.municipio.id",
						"pessoa.enderecos.municipio.nome",
						"pessoa.enderecos.municipio.estado.sigla",
						"pessoa.enderecos.municipio.estado.nome",
						"pessoa.enderecos.correspondencia"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

		findByFilter = new JSONSerializer()
				.include(
						"totalItems",
						"pageItems.id",
						"pageItems.pessoa.tipo",
						"pageItems.pessoa.id",
						"pageItems.pessoa.nome",
						"pageItems.pessoa.cpf",
						"pageItems.pessoa.sexo.descricao",
						"pageItems.pessoa.sexo.codigo",
						"pageItems.pessoa.dataNascimento",
						"pageItems.pessoa.estadoCivil.descricao",
						"pageItems.pessoa.estadoCivil.codigo",
						"pageItems.pessoa.estadoCivil.nome",
						"pageItems.pessoa.nomeMae",
						"pageItems.pessoa.naturalidade",
						"pageItems.pessoa.rg.numero",
						"pageItems.pessoa.rg.orgaoExpedidor",
						"pageItems.pessoa.tituloEleitoral.numero",
						"pageItems.pessoa.tituloEleitoral.secao",
						"pageItems.pessoa.tituloEleitoral.zona",
						"pageItems.pessoa.cnpj",
						"pageItems.pessoa.municipio.codigoIbge",
						"pageItems.pessoa.nomeFantasia",
						"pageItems.pessoa.razaoSocial",
						"pageItems.pessoa.dataConstituicao",
						"pageItems.pessoa.inscricaoEstadual",
						"pageItems.pessoa.contatos.valor",
						"pageItems.pessoa.contatos.principal",
						"pageItems.pessoa.contatos.tipo.id",
						"pageItems.pessoa.contatos.tipo.descricao",
						"pageItems.pessoa.enderecos.id",
						"pageItems.pessoa.enderecos.tipo.id",
						"pageItems.pessoa.enderecos.logradouro",
						"pageItems.pessoa.enderecos.numero",
						"pageItems.pessoa.enderecos.caixaPostal",
						"pageItems.pessoa.enderecos.bairro",
						"pageItems.pessoa.enderecos.complemento",
						"pageItems.pessoa.enderecos.cep",
						"pageItems.pessoa.enderecos.descricaoAcesso",
						"pageItems.pessoa.enderecos.municipio.id",
						"pageItems.pessoa.enderecos.municipio.nome",
						"pageItems.pessoa.enderecos.municipio.codigoIbge",
						"pageItems.pessoa.enderecos.municipio.estado.id",
						"pageItems.pessoa.enderecos.municipio.estado.sigla",
						"pageItems.pessoa.enderecos.municipio.estado.nome",
						"pageItems.pessoa.enderecos.municipio.codigoIbge",
						"pageItems.pessoa.enderecos.zonaLocalizacao.nome",
						"pageItems.pessoa.enderecos.pais.id",
						"pageItems.ativo"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

	}

}
