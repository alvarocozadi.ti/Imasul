package serializers;

import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.GeometryTransformer;
import transformers.TipoEnumTransformer;

public class EmpreendimentoHistoricoSerializer {

	public static JSONSerializer find;
	public static JSONSerializer findById;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		find = new JSONSerializer()
				.include(
						"empreendimentos.id",
						"empreendimentos.denominacao",
						"empreendimentos.dataHistorico",
						"empreendimentos.pessoa.tipo",
						"empreendimentos.pessoa.nome",
						"empreendimentos.pessoa.cpf",
						"empreendimentos.pessoa.cnpj",
						"empreendimentos.pessoa.nomeFantasia",
						"empreendimentos.pessoa.razaoSocial",
						"empreendimentos.pessoa.dataConstituicao",
						"empreendimentos.pessoaModificadora.tipo",
						"empreendimentos.pessoaModificadora.nome",
						"empreendimentos.pessoaModificadora.cpf",
						"empreendimentos.pessoaModificadora.cnpj",
						"empreendimentos.pessoaModificadora.nomeFantasia",
						"empreendimentos.pessoaModificadora.razaoSocial",
						"empreendimentos.pessoaModificadora.dataConstituicao",
						"empreendimentos.classificacaoEmpreendimento.id",
						"empreendimentos.classificacaoEmpreendimento.descricao",
						"empreendimentos.classificacaoEmpreendimento.codigo",
						"empreendimentos.classificacaoEmpreendimento.isento"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);


		findById = new JSONSerializer()
				.include(
						"id",
						"denominacao",
						"dataHistorico",
						"proprietarios.id",
						"proprietarios.tipo",
						"proprietarios.nome",
						"proprietarios.razaoSocial",
						"proprietarios.cpf",
						"proprietarios.cnpj",
						"proprietarios.municipio.codigoIbge",
						"proprietarios.inscricaoEstadual",
						"proprietarios.sexo",
						"proprietarios.dataNascimento",
						"proprietarios.dataConstituicao",
						"proprietarios.estadoCivil",
						"proprietarios.nomeMae",
						"proprietarios.naturalidade",
						"proprietarios.rg.numero",
						"proprietarios.rg.orgaoExpedidor",
						"proprietarios.tituloEleitoral.numero",
						"proprietarios.tituloEleitoral.secao",
						"proprietarios.tituloEleitoral.zona",
						"proprietarios.enderecos.cep",
						"proprietarios.enderecos.logradouro",
						"proprietarios.enderecos.numero",
						"proprietarios.enderecos.complemento",
						"proprietarios.enderecos.bairro",
						"proprietarios.enderecos.tipo.id",
						"proprietarios.enderecos.municipio.nome",
						"proprietarios.enderecos.municipio.estado.sigla",
						"proprietarios.enderecos.municipio.codigoIbge",
						"proprietarios.enderecos.zonaLocalizacao.codigo",
						"proprietarios.contatos.valor",
						"proprietarios.contatos.principal",
						"proprietarios.contatos.tipo.id",
						"proprietarios.contatos.tipo.descricao",
						"representantesLegais.id",
						"representantesLegais.tipo",
						"representantesLegais.nome",
						"representantesLegais.cpf",
						"representantesLegais.enderecos.cep",
						"representantesLegais.enderecos.logradouro",
						"representantesLegais.enderecos.numero",
						"representantesLegais.enderecos.complemento",
						"representantesLegais.enderecos.bairro",
						"representantesLegais.enderecos.tipo.id",
						"representantesLegais.enderecos.municipio.nome",
						"representantesLegais.enderecos.municipio.estado.sigla",
						"representantesLegais.enderecos.municipio.codigoIbge",
						"representantesLegais.enderecos.zonaLocalizacao.codigo",
						"representantesLegais.contatos.valor",
						"representantesLegais.contatos.principal",
						"representantesLegais.contatos.tipo.id",
						"representantesLegais.contatos.tipo.descricao",
						"responsaveisLegais.id",
						"responsaveisLegais.tipo",
						"responsaveisLegais.nome",
						"responsaveisLegais.cpf",
						"responsaveisLegais.usuario.email",
						"responsaveisLegais.enderecos.cep",
						"responsaveisLegais.enderecos.logradouro",
						"responsaveisLegais.enderecos.numero",
						"responsaveisLegais.enderecos.complemento",
						"responsaveisLegais.enderecos.bairro",
						"responsaveisLegais.enderecos.tipo.id",
						"responsaveisLegais.enderecos.municipio.nome",
						"responsaveisLegais.enderecos.municipio.estado.sigla",
						"responsaveisLegais.enderecos.municipio.codigoIbge",
						"responsaveisLegais.enderecos.zonaLocalizacao.codigo",
						"responsaveisLegais.contatos.valor",
						"responsaveisLegais.contatos.principal",
						"responsaveisLegais.contatos.tipo.id",
						"responsaveisLegais.contatos.tipo.descricao",
						"responsaveisTecnicos.id",
						"responsaveisTecnicos.tipo",
						"responsaveisTecnicos.nome",
						"responsaveisTecnicos.razaoSocial",
						"responsaveisTecnicos.cpf",
						"responsaveisTecnicos.cnpj",
						"responsaveisTecnicos.municipio.codigoIbge",
						"responsaveisTecnicos.inscricaoEstadual",
						"responsaveisTecnicos.usuario.email",
						"responsaveisTecnicos.enderecos.cep",
						"responsaveisTecnicos.enderecos.logradouro",
						"responsaveisTecnicos.enderecos.numero",
						"responsaveisTecnicos.enderecos.complemento",
						"responsaveisTecnicos.enderecos.bairro",
						"responsaveisTecnicos.enderecos.tipo.id",
						"responsaveisTecnicos.enderecos.municipio.nome",
						"responsaveisTecnicos.enderecos.municipio.estado.sigla",
						"responsaveisTecnicos.enderecos.municipio.codigoIbge",
						"responsaveisTecnicos.enderecos.zonaLocalizacao.codigo",
						"responsaveisTecnicos.contatos.valor",
						"responsaveisTecnicos.contatos.principal",
						"responsaveisTecnicos.contatos.tipo.id",
						"responsaveisTecnicos.contatos.tipo.descricao",
						"pessoa.tipo",
						"pessoa.id",
						"pessoa.nome",
						"pessoa.cpf",
						"pessoa.sexo",
						"pessoa.dataNascimento",
						"pessoa.estadoCivil",
						"pessoa.nomeMae",
						"pessoa.naturalidade",
						"pessoa.rg.numero",
						"pessoa.rg.orgaoExpedidor",
						"pessoa.tituloEleitoral.numero",
						"pessoa.tituloEleitoral.secao",
						"pessoa.tituloEleitoral.zona",
						"pessoa.cnpj",
						"pessoa.municipio.codigoIbge",
						"pessoa.nomeFantasia",
						"pessoa.razaoSocial",
						"pessoa.dataConstituicao",
						"pessoa.inscricaoEstadual",
						"contatos.id",
						"contatos.tipo.id",
						"contatos.tipo.descricao",
						"contatos.valor",
						"contatos.principal",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.caixaPostal",
						"enderecos.bairro",
						"enderecos.complemento",
						"enderecos.cep",
						"enderecos.descricaoAcesso",
						"enderecos.municipio.id",
						"enderecos.municipio.nome",
						"enderecos.municipio.codigoIbge",
						"enderecos.municipio.estado.id",
						"enderecos.municipio.estado.sigla",
						"enderecos.municipio.codigoIbge",
						"enderecos.zonaLocalizacao.codigo",
						"enderecos.pais.id",
						"localizacao.geometria",
						"cnaes.codigo",
						"porte",
						"classificacaoEmpreendimento.id",
						"classificacaoEmpreendimento.descricao",
						"classificacaoEmpreendimento.codigo",
						"classificacaoEmpreendimento.isento"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

	}

}
