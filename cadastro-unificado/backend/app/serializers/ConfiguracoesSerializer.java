package serializers;

import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import play.Play.Mode;
import transformers.TipoEnumTransformer;

public class ConfiguracoesSerializer {

	public static JSONSerializer get;

	static {

		boolean prettyPrint = Play.mode == Mode.DEV;

		get = new JSONSerializer()
			.include(
					"mode",
					"versao",
					"dataVersao",
					"tipos.EstadoCivil.nome",
					"tipos.EstadoCivil.codigo",
					"tipos.EstadoCivil.descricao",
					"tipos.Sexo.nome",
					"tipos.Sexo.codigo",
					"tipos.Sexo.descricao",
					"tipos.TipoPessoa.nome",
					"tipos.TipoPessoa.codigo",
					"tipos.TipoPessoa.descricao",
					"tipos.ZonaLocalizacao.nome",
					"tipos.ZonaLocalizacao.codigo",
					"tipos.ZonaLocalizacao.descricao"
			)
			.exclude("*")
			.prettyPrint(prettyPrint)
			.transform(new TipoEnumTransformer(), Tipo.class);

	}

}
