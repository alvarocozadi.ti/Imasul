package serializers;

import flexjson.JSONSerializer;
import play.Play;

public class TiposContatoSerializer {

	public static JSONSerializer find;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		find = new JSONSerializer()
			.include(
				"id",
				"descricao"
			)
			.exclude("*")
			.prettyPrint(prettyPrint);

	}

}