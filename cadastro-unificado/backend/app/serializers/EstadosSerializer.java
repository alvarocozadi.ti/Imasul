package serializers;

import flexjson.JSONSerializer;
import play.Play;

public class EstadosSerializer {

	public static JSONSerializer findMunicipios;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		findMunicipios = new JSONSerializer()
			.include(
				"id",
				"nome",
				"estado.id",
				"codigoIbge"
			)
			.exclude("*")
			.prettyPrint(prettyPrint);

	}

}
