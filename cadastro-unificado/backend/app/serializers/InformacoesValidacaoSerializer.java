package serializers;

import flexjson.JSONSerializer;
import play.Play;

public class InformacoesValidacaoSerializer {

	public static JSONSerializer getByCpf;
	public static JSONSerializer getByCnpj;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		getByCpf = new JSONSerializer()
				.include(
						"nomesPessoasFisicas",
						"nomesMaes"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

		getByCnpj = new JSONSerializer()
				.include(
						"nomesPessoasJuridicas",
						"nomesMunicipios",
						"estadoMunicipio"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

	}

}
