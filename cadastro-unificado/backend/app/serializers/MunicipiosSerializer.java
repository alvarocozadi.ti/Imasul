package serializers;

import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import play.Play;
import transformers.GeometryTransformer;

public class MunicipiosSerializer {

	public static JSONSerializer getMunicipioGeometryById;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		getMunicipioGeometryById = new JSONSerializer()
				.include(
						"geometry"
				)
				.exclude("*")
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

	}
}
