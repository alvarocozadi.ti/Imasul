package serializers;

import com.vividsolutions.jts.geom.Geometry;
import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.GeometryTransformer;
import transformers.TipoEnumTransformer;

public class PessoasSerializer {

	public static JSONSerializer getPessoasAtualizadas;
	public static JSONSerializer getEmpreendimentos;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		getPessoasAtualizadas = new JSONSerializer()
				.include(
						/* Atributos em comum */
						"id",
						"tipo",
						"dataCadastro",
						"dataAtualizacao",
						"isUsuario",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao.codigo",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.caixaPostal",
						"enderecos.bairro",
						"enderecos.complemento",
						"enderecos.cep",
						"enderecos.descricaoAcesso",
						"enderecos.municipio.id",
						"enderecos.municipio.nome",
						"enderecos.municipio.codigoIbge",
						"enderecos.municipio.estado.id",
						"enderecos.municipio.estado.nome",
						"enderecos.municipio.estado.sigla",
						"enderecos.pais.id",
						"enderecos.pais.nome",
						"enderecos.pais.sigla",
						"enderecos.semNumero",
						"contatos.id",
						"contatos.tipo.id",
						"contatos.tipo.descricao",
						"contatos.valor",
						"contatos.principal",

						/* Atributos Pessoa Juridica */
						"razaoSocial",
						"cnpj",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual",

						/* Atributos Pessoa Fisica */
						"nome",
						"cpf",
						"dataNascimento",
						"nomeMae",
						"estrangeiro",
						"naturalidade",
						"sexo",
						"estadoCivil",
						"passaporte",
						"tituloEleitoral.numero",
						"tituloEleitoral.zona",
						"tituloEleitoral.secao",
						"rg.numero",
						"rg.orgaoExpedidor"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

		getEmpreendimentos= new JSONSerializer()
				.include(
						// proprietariosEmpreendimentos
						"proprietarioEmpreendimentos.id",
						"proprietarioEmpreendimentos.denominacao",
						"proprietarioEmpreendimentos.contatos.valor",
						"proprietarioEmpreendimentos.contatos.principal",
						"proprietarioEmpreendimentos.contatos.tipo.id",
						"proprietarioEmpreendimentos.contatos.tipo.descricao",
						"proprietarioEmpreendimentos.localizacao.geometria",
						"proprietarioEmpreendimentos.pessoa.tipo",
						"proprietarioEmpreendimentos.pessoa.nome",
						"proprietarioEmpreendimentos.pessoa.cpf",
						"proprietarioEmpreendimentos.pessoa.sexo",
						"proprietarioEmpreendimentos.pessoa.dataNascimento",
						"proprietarioEmpreendimentos.pessoa.nomeMae",
						"proprietarioEmpreendimentos.pessoa.naturalidade",
						"proprietarioEmpreendimentos.pessoa.rg.numero",
						"proprietarioEmpreendimentos.pessoa.rg.orgaoExpedidor",
						"proprietarioEmpreendimentos.pessoa.cnpj",
						"proprietarioEmpreendimentos.pessoa.nomeFantasia",
						"proprietarioEmpreendimentos.pessoa.razaoSocial",
						"proprietarioEmpreendimentos.pessoa.dataConstituicao",
						"proprietarioEmpreendimentos.pessoa.inscricaoEstadual",
						"proprietarioEmpreendimentos.localizacao.geometria",
						"proprietarioEmpreendimentos.enderecos.cep",
						"proprietarioEmpreendimentos.enderecos.logradouro",
						"proprietarioEmpreendimentos.enderecos.numero",
						"proprietarioEmpreendimentos.enderecos.complemento",
						"proprietarioEmpreendimentos.enderecos.bairro",
						"proprietarioEmpreendimentos.enderecos.tipo.descricao",
						"proprietarioEmpreendimentos.enderecos.zonaLocalizacao.codigo",
						"proprietarioEmpreendimentos.enderecos.zonaLocalizacao.descricao",
						"proprietarioEmpreendimentos.enderecos.municipio.nome",
						"proprietarioEmpreendimentos.enderecos.municipio.codigoIbge",
						"proprietarioEmpreendimentos.enderecos.municipio.estado.nome",
						"proprietarioEmpreendimentos.enderecos.municipio.estado.sigla",
						"proprietarioEmpreendimentos.responsaveisTecnicos.isUsuario",
						"proprietarioEmpreendimentos.responsaveisTecnicos.cpf",
						"proprietarioEmpreendimentos.responsaveisTecnicos.cnpj",

						// responsavelTecnicoEmpreendimentos

						"responsavelTecnicoEmpreendimentos.id",
						"responsavelTecnicoEmpreendimentos.denominacao",
						"responsavelTecnicoEmpreendimentos.enderecos.cep",
						"responsavelTecnicoEmpreendimentos.enderecos.logradouro",
						"responsavelTecnicoEmpreendimentos.enderecos.numero",
						"responsavelTecnicoEmpreendimentos.enderecos.complemento",
						"responsavelTecnicoEmpreendimentos.enderecos.bairro",
						"responsavelTecnicoEmpreendimentos.enderecos.tipo.descricao",
						"responsavelTecnicoEmpreendimentos.enderecos.zonaLocalizacao.codigo",
						"responsavelTecnicoEmpreendimentos.enderecos.zonaLocalizacao.descricao",
						"responsavelTecnicoEmpreendimentos.enderecos.municipio.nome",
						"responsavelTecnicoEmpreendimentos.enderecos.municipio.codigoIbge",
						"responsavelTecnicoEmpreendimentos.enderecos.municipio.estado.nome",
						"responsavelTecnicoEmpreendimentos.enderecos.municipio.estado.sigla",
						"responsavelTecnicoEmpreendimentos.contatos.valor",
						"responsavelTecnicoEmpreendimentos.contatos.principal",
						"responsavelTecnicoEmpreendimentos.contatos.tipo.id",
						"responsavelTecnicoEmpreendimentos.contatos.tipo.descricao",
						"responsavelTecnicoEmpreendimentos.pessoa.tipo",
						"responsavelTecnicoEmpreendimentos.pessoa.nome",
						"responsavelTecnicoEmpreendimentos.pessoa.cpf",
						"responsavelTecnicoEmpreendimentos.pessoa.sexo",
						"responsavelTecnicoEmpreendimentos.pessoa.dataNascimento",
						"responsavelTecnicoEmpreendimentos.pessoa.nomeMae",
						"responsavelTecnicoEmpreendimentos.pessoa.naturalidade",
						"responsavelTecnicoEmpreendimentos.pessoa.rg.numero",
						"responsavelTecnicoEmpreendimentos.pessoa.rg.orgaoExpedidor",
						"responsavelTecnicoEmpreendimentos.pessoa.cnpj",
						"responsavelTecnicoEmpreendimentos.pessoa.nomeFantasia",
						"responsavelTecnicoEmpreendimentos.pessoa.razaoSocial",
						"responsavelTecnicoEmpreendimentos.pessoa.dataConstituicao",
						"responsavelTecnicoEmpreendimentos.pessoa.inscricaoEstadual",
						"responsavelTecnicoEmpreendimentos.localizacao.geometria",

						// representateLegalEmpreendimentos

						"representanteLegalEmpreendimentos.id",
						"representanteLegalEmpreendimentos.denominacao",
						"representanteLegalEmpreendimentos.contatos.valor",
						"representanteLegalEmpreendimentos.contatos.principal",
						"representanteLegalEmpreendimentos.contatos.tipo.id",
						"representanteLegalEmpreendimentos.contatos.tipo.descricao",
						"representanteLegalEmpreendimentos.localizacao.geometria",
						"representanteLegalEmpreendimentos.pessoa.tipo",
						"representanteLegalEmpreendimentos.pessoa.nome",
						"representanteLegalEmpreendimentos.pessoa.cpf",
						"representanteLegalEmpreendimentos.pessoa.sexo",
						"representanteLegalEmpreendimentos.pessoa.dataNascimento",
						"representanteLegalEmpreendimentos.pessoa.nomeMae",
						"representanteLegalEmpreendimentos.pessoa.naturalidade",
						"representanteLegalEmpreendimentos.pessoa.rg.numero",
						"representanteLegalEmpreendimentos.pessoa.rg.orgaoExpedidor",
						"representanteLegalEmpreendimentos.pessoa.cnpj",
						"representanteLegalEmpreendimentos.pessoa.nomeFantasia",
						"representanteLegalEmpreendimentos.pessoa.razaoSocial",
						"representanteLegalEmpreendimentos.pessoa.dataConstituicao",
						"representanteLegalEmpreendimentos.pessoa.inscricaoEstadual",
						"representanteLegalEmpreendimentos.localizacao.geometria",
						"representanteLegalEmpreendimentos.enderecos.cep",
						"representanteLegalEmpreendimentos.enderecos.logradouro",
						"representanteLegalEmpreendimentos.enderecos.numero",
						"representanteLegalEmpreendimentos.enderecos.complemento",
						"representanteLegalEmpreendimentos.enderecos.bairro",
						"representanteLegalEmpreendimentos.enderecos.tipo.descricao",
						"representanteLegalEmpreendimentos.enderecos.zonaLocalizacao.codigo",
						"representanteLegalEmpreendimentos.enderecos.zonaLocalizacao.descricao",
						"representanteLegalEmpreendimentos.enderecos.municipio.nome",
						"representanteLegalEmpreendimentos.enderecos.municipio.codigoIbge",
						"representanteLegalEmpreendimentos.enderecos.municipio.estado.nome",
						"representanteLegalEmpreendimentos.enderecos.municipio.estado.sigla",

						// responsavelLegalEmpreendimentos

						"responsavelLegalEmpreendimentos.id",
						"responsavelLegalEmpreendimentos.denominacao",
						"responsavelLegalEmpreendimentos.contatos.valor",
						"responsavelLegalEmpreendimentos.contatos.principal",
						"responsavelLegalEmpreendimentos.contatos.tipo.id",
						"responsavelLegalEmpreendimentos.contatos.tipo.descricao",
						"responsavelLegalEmpreendimentos.localizacao.geometria",
						"responsavelLegalEmpreendimentos.pessoa.tipo",
						"responsavelLegalEmpreendimentos.pessoa.nome",
						"responsavelLegalEmpreendimentos.pessoa.cpf",
						"responsavelLegalEmpreendimentos.pessoa.sexo",
						"responsavelLegalEmpreendimentos.pessoa.dataNascimento",
						"responsavelLegalEmpreendimentos.pessoa.nomeMae",
						"responsavelLegalEmpreendimentos.pessoa.naturalidade",
						"responsavelLegalEmpreendimentos.pessoa.rg.numero",
						"responsavelLegalEmpreendimentos.pessoa.rg.orgaoExpedidor",
						"responsavelLegalEmpreendimentos.pessoa.cnpj",
						"responsavelLegalEmpreendimentos.pessoa.nomeFantasia",
						"responsavelLegalEmpreendimentos.pessoa.razaoSocial",
						"responsavelLegalEmpreendimentos.pessoa.dataConstituicao",
						"responsavelLegalEmpreendimentos.pessoa.inscricaoEstadual",
						"responsavelLegalEmpreendimentos.localizacao.geometria",
						"responsavelLegalEmpreendimentos.enderecos.cep",
						"responsavelLegalEmpreendimentos.enderecos.logradouro",
						"responsavelLegalEmpreendimentos.enderecos.numero",
						"responsavelLegalEmpreendimentos.enderecos.complemento",
						"responsavelLegalEmpreendimentos.enderecos.bairro",
						"responsavelLegalEmpreendimentos.enderecos.tipo.descricao",
						"responsavelLegalEmpreendimentos.enderecos.zonaLocalizacao.codigo",
						"responsavelLegalEmpreendimentos.enderecos.zonaLocalizacao.descricao",
						"responsavelLegalEmpreendimentos.enderecos.municipio.nome",
						"responsavelLegalEmpreendimentos.enderecos.municipio.codigoIbge",
						"responsavelLegalEmpreendimentos.enderecos.municipio.estado.nome",
						"responsavelLegalEmpreendimentos.enderecos.municipio.estado.sigla"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(new GeometryTransformer(), Geometry.class)
				.prettyPrint(prettyPrint);

	}

}
