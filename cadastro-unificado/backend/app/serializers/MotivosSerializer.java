package serializers;

import flexjson.JSONSerializer;
import play.Play;

public class MotivosSerializer {

	public static JSONSerializer findAll;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		findAll = new JSONSerializer()
				.include(
						"id",
						"descricao"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

	}

}
