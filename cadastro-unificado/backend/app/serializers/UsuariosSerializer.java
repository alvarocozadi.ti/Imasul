package serializers;

import flexjson.JSONSerializer;
import play.Play;
import play.Play.Mode;

import java.util.Date;

public class UsuariosSerializer {

	public static JSONSerializer find;
	public static JSONSerializer findPessoaForHeader;
	public static JSONSerializer external;

	static {

		boolean prettyPrint = Play.mode == Mode.DEV;

		find = new JSONSerializer()
				.include(
						"id",
						"ativo",
						"email",
						"temSenha",
						"perfis.id",
						"perfis.nome"
				)
				.exclude("*")
				.transform(DateSerializer.getTransformer(), Date.class)
				.prettyPrint(prettyPrint);

		findPessoaForHeader = new JSONSerializer()
				.include(
						"pessoa.id",
						"pessoa.nome",
						"pessoa.razaoSocial"
				)
				.exclude("*")
				.transform(DateSerializer.getTransformer(), Date.class)
				.prettyPrint(prettyPrint);

		external = new JSONSerializer()
			.include(
				"id",
				"login",
				"email",
				"dataCadastro",
				"ativo",
				"dataAtualizacao",
				"perfis.id",
				"perfis.codigo",
				"perfis.nome",
				"perfis.permissoes.id",
				"perfis.permissoes.codigo",
				"perfis.permissoes.nome",
				"perfis.moduloPertencente.id",
				"perfis.moduloPertencente.sigla",
				"perfis.moduloPertencente.permissoes.id",
				"perfis.moduloPertencente.permissoes.codigo",
				"perfis.moduloPertencente.permissoes.nome",
				"perfis.agendaDesativacao.id",
				"perfis.agendaDesativacao.dataInicio",
				"perfis.agendaDesativacao.dataFim",
				"perfis.agendaDesativacao.motivo.id",
				"perfis.agendaDesativacao.motivo.descricao",
				"perfis.agendaDesativacao.descricao",
				"perfis.agendaDesativacao.dataSolicitacao",
				"setores.id",
				"setores.nome",
				"setores.sigla"
			)
			.exclude("*")
			.transform(DateSerializer.getTransformer(), Date.class)
			.prettyPrint(prettyPrint);

	}

}
