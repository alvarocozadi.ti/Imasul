package serializers;

import flexjson.JSONSerializer;
import play.Play;

public class PaisesSerializer {

	public static JSONSerializer find;
	public static JSONSerializer findEstados;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		find = new JSONSerializer()
			.include(
				"id",
				"nome",
				"sigla"
			)
			.exclude("*")
			.prettyPrint(prettyPrint);

		findEstados = new JSONSerializer()
			.include(
				"id",
				"sigla",
				"nome"
			)
			.exclude("*")
			.prettyPrint(prettyPrint);
		
	}

}
