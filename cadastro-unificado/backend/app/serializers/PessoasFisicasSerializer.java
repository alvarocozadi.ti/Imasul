package serializers;

import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.TipoEnumTransformer;

import java.util.Date;

public class PessoasFisicasSerializer {

    public static JSONSerializer findByFilter;
    public static JSONSerializer findByCpf;
    public static JSONSerializer externalFindByCpf;
    public static JSONSerializer find;
    public static JSONSerializer externalFindByFilter;
    public static JSONSerializer findByFilterNoUsers;

    static {

        boolean prettyPrint = Play.mode == Play.Mode.DEV;

        findByFilter = new JSONSerializer()
                .include(
                        "totalItems",
                        "pageItems.id",
                        "pageItems.nome",
                        "pageItems.cpf",
						"pageItems.tipo.nome",
						"pageItems.tipo.codigo",
                        "pageItems.rg.numero",
                        "pageItems.isUsuario",
						"pageItems.tituloEleitoral.numero",
						"pageItems.tituloEleitoral.zona",
						"pageItems.tituloEleitoral.secao",
						"pageItems.estadoCivil.descricao",
						"pageItems.estadoCivil.codigo",
						"pageItems.razaoSocial",
                        "pageItems.usuario.id",
                        "pageItems.usuario.ativo",
                        "pageItems.usuario.temSenha",
                        "pageItems.usuario.agendaDesativacao.id",
                        "pageItems.usuario.agendaDesativacao.dataInicio",
                        "pageItems.usuario.agendaDesativacao.dataFim",
                        "pageItems.enderecos.tipo.id",
						"pageItems.enderecos.tipo.descricao",
						"pageItems.enderecos.zonaLocalizacao.codigo",
						"pageItems.enderecos.zonaLocalizacao.nome",
                        "pageItems.enderecos.municipio.nome",
                        "pageItems.enderecos.municipio.estado.sigla"
                )
                .exclude("*")
                .prettyPrint(prettyPrint);

        findByCpf = new JSONSerializer()
                .include(
                        "id",
                        "tipo",
                        "nome",
                        "cpf"
                )
                .exclude("*")
                .transform(new TipoEnumTransformer(), Tipo.class)
                .prettyPrint(prettyPrint);

        find = new JSONSerializer()
                .include(
                        "id",
                        "tipo",
                        "dataCadastro",
                        "dataAtualizacao",
                        "contatos.id",
                        "contatos.tipo.id",
                        "contatos.tipo.descricao",
                        "contatos.valor",
                        "contatos.principal",
                        "enderecos.id",
                        "enderecos.tipo.id",
                        "enderecos.zonaLocalizacao",
                        "enderecos.logradouro",
                        "enderecos.numero",
                        "enderecos.caixaPostal",
                        "enderecos.bairro",
                        "enderecos.complemento",
                        "enderecos.cep",
                        "enderecos.descricaoAcesso",
                        "enderecos.municipio.id",
                        "enderecos.municipio.nome",
                        "enderecos.municipio.estado.id",
                        "enderecos.municipio.estado.sigla",
                        "enderecos.municipio.estado.nome",
                        "enderecos.pais.id",
                        "nome",
                        "cpf",
                        "dataNascimento",
                        "nomeMae",
                        "estrangeiro",
                        "naturalidade",
                        "sexo",
                        "estadoCivil",
                        "tituloEleitoral.numero",
                        "tituloEleitoral.zona",
                        "tituloEleitoral.secao",
                        "rg.numero",
                        "rg.orgaoExpedidor",
                        "passaporte",
                        "usuario.id",
                        "usuario.ativo",
                        "usuario.temSenha",
                        "usuario.email",
                        "usuario.perfis.id",
                        "usuario.perfis.nome",
                        "usuario.setores.id",
                        "usuario.setores.nome",
                        "usuario.setores.sigla",
                        "isUsuario",
                        "usuario.agendaDesativacao.id",
                        "usuario.agendaDesativacao.motivo.id",
                        "usuario.agendaDesativacao.motivo.descricao",
                        "usuario.agendaDesativacao.descricao",
                        "usuario.agendaDesativacao.tempoIndeterminado",
                        "usuario.agendaDesativacao.dataInicio",
                        "usuario.agendaDesativacao.dataFim"
                )
                .exclude("*")
                .transform(new TipoEnumTransformer(), Tipo.class)
                .prettyPrint(prettyPrint);

        externalFindByCpf = new JSONSerializer()
                .include(
                        "id",
                        "tipo",
                        "dataCadastro",
                        "dataAtualizacao",
                        "contatos.id",
                        "contatos.tipo.id",
                        "contatos.tipo.descricao",
                        "contatos.valor",
                        "contatos.principal",
                        "enderecos.id",
                        "enderecos.tipo.id",
                        "enderecos.zonaLocalizacao",
                        "enderecos.logradouro",
                        "enderecos.numero",
						"enderecos.semNumero",
                        "enderecos.caixaPostal",
                        "enderecos.bairro",
                        "enderecos.complemento",
                        "enderecos.cep",
                        "enderecos.descricaoAcesso",
                        "enderecos.municipio.id",
                        "enderecos.municipio.nome",
                        "enderecos.municipio.codigoIbge",
                        "enderecos.municipio.estado.id",
                        "enderecos.municipio.estado.sigla",
                        "enderecos.municipio.estado.nome",
                        "enderecos.pais.id",
						"enderecos.pais.nome",
						"enderecos.pais.sigla",
                        "nome",
                        "cpf",
                        "dataNascimento",
                        "nomeMae",
                        "estrangeiro",
                        "naturalidade",
                        "sexo",
                        "estadoCivil",
                        "tituloEleitoral.numero",
                        "tituloEleitoral.zona",
                        "tituloEleitoral.secao",
                        "rg.numero",
                        "rg.orgaoExpedidor",
                        "passaporte",
                        "isUsuario"
                )
                .exclude("*")
                .transform(new TipoEnumTransformer(), Tipo.class)
                .prettyPrint(prettyPrint);

		findByFilterNoUsers = new JSONSerializer()
				.include(
						"totalItems",
						"pageItems.id",
						"pageItems.tipo",
						"pageItems.dataCadastro",
						"pageItems.dataAtualizacao",
						"pageItems.contatos.id",
						"pageItems.contatos.tipo.id",
						"pageItems.contatos.tipo.descricao",
						"pageItems.contatos.valor",
						"pageItems.contatos.principal",
						"pageItems.enderecos.id",
						"pageItems.enderecos.tipo.id",
						"pageItems.enderecos.tipo.descricao",
						"pageItems.enderecos.zonaLocalizacao",
						"pageItems.enderecos.logradouro",
						"pageItems.enderecos.numero",
						"pageItems.enderecos.caixaPostal",
						"pageItems.enderecos.bairro",
						"pageItems.enderecos.complemento",
						"pageItems.enderecos.cep",
						"pageItems.enderecos.descricaoAcesso",
						"pageItems.enderecos.municipio.id",
						"pageItems.enderecos.municipio.nome",
						"pageItems.enderecos.municipio.estado.id",
						"pageItems.enderecos.municipio.estado.sigla",
						"pageItems.enderecos.municipio.estado.nome",
						"pageItems.enderecos.municipio.codigoIbge",
						"pageItems.enderecos.pais.id",
						"pageItems.nome",
						"pageItems.cpf",
						"pageItems.cnpj",
						"pageItems.razaoSocial",
						"pageItems.dataNascimento",
						"pageItems.nomeMae",
						"pageItems.estrangeiro",
						"pageItems.naturalidade",
						"pageItems.sexo",
						"pageItems.estadoCivil",
						"pageItems.tituloEleitoral.numero",
						"pageItems.tituloEleitoral.zona",
						"pageItems.tituloEleitoral.secao",
						"pageItems.rg.numero",
						"pageItems.rg.orgaoExpedidor",
						"pageItems.passaporte"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.transform(DateSerializer.getTransformer(), Date.class)
				.prettyPrint(prettyPrint);

        externalFindByFilter = new JSONSerializer()
                .include(
						"totalItems",
						"pageItems.id",
						"pageItems.tipo",
						"pageItems.dataCadastro",
						"pageItems.dataAtualizacao",
						"pageItems.contatos.id",
						"pageItems.contatos.tipo.id",
						"pageItems.contatos.tipo.descricao",
						"pageItems.contatos.valor",
						"pageItems.contatos.principal",
						"pageItems.enderecos.id",
						"pageItems.enderecos.tipo.id",
						"pageItems.enderecos.zonaLocalizacao",
						"pageItems.enderecos.logradouro",
						"pageItems.enderecos.numero",
						"pageItems.enderecos.caixaPostal",
						"pageItems.enderecos.bairro",
						"pageItems.enderecos.complemento",
						"pageItems.enderecos.cep",
						"pageItems.enderecos.descricaoAcesso",
						"pageItems.enderecos.municipio.id",
						"pageItems.enderecos.municipio.nome",
						"pageItems.enderecos.municipio.estado.id",
						"pageItems.enderecos.municipio.estado.sigla",
						"pageItems.enderecos.municipio.estado.nome",
						"pageItems.enderecos.municipio.codigoIbge",
						"pageItems.enderecos.pais.id",
						"pageItems.nome",
						"pageItems.cpf",
						"pageItems.cnpj",
						"pageItems.razaoSocial",
						"pageItems.dataNascimento",
						"pageItems.nomeMae",
						"pageItems.estrangeiro",
						"pageItems.naturalidade",
						"pageItems.sexo",
						"pageItems.estadoCivil",
						"pageItems.tituloEleitoral.numero",
						"pageItems.tituloEleitoral.zona",
						"pageItems.tituloEleitoral.secao",
						"pageItems.rg.numero",
						"pageItems.rg.orgaoExpedidor",
						"pageItems.passaporte",
						"pageItems.usuario.id",
						"pageItems.usuario.login",
						"pageItems.usuario.email",
						"pageItems.usuario.perfis.id",
						"pageItems.usuario.perfis.nome",
						"pageItems.usuario.perfis.codigo",
						"pageItems.usuario.perfis.permissoes.id",
						"pageItems.usuario.perfis.permissoes.nome",
						"pageItems.usuario.perfis.permissoes.codigo"
                )
                .exclude("*")
                .transform(new TipoEnumTransformer(), Tipo.class)
                .transform(DateSerializer.getTransformer(), Date.class)
                .prettyPrint(prettyPrint);

    }
}
