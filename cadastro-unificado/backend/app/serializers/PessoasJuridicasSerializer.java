package serializers;

import flexjson.JSONSerializer;
import models.Tipo;
import play.Play;
import transformers.TipoEnumTransformer;

public class PessoasJuridicasSerializer {

	public static JSONSerializer find;
	public static JSONSerializer findByCnpj;
	public static JSONSerializer findByFilter;
	public static JSONSerializer externalFindByCnpj;

	static {

		boolean prettyPrint = Play.mode == Play.Mode.DEV;

		find = new JSONSerializer()
				.include(
						"id",
						"tipo",
						"cnpj",
						"razaoSocial",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.caixaPostal",
						"enderecos.bairro",
						"enderecos.complemento",
						"enderecos.cep",
						"enderecos.descricaoAcesso",
						"enderecos.municipio.id",
						"enderecos.municipio.nome",
						"enderecos.municipio.codigoIbge",
						"enderecos.municipio.estado",
						"enderecos.municipio.estado.id",
						"enderecos.municipio.estado.nome",
						"enderecos.municipio.estado.sigla",
						"enderecos.pais.id",
						"enderecos.pais.nome",
						"enderecos.pais.sigla",
						"enderecos.semNumero",
						"contatos.tipo.id",
						"contatos.tipo.descricao",
						"contatos.valor",
						"contatos.principal",
						"usuario.id",
						"usuario.ativo",
						"usuario.temSenha",
						"usuario.email",
						"usuario.perfis.id",
						"usuario.perfis.nome",
						"usuario.setores.id",
						"usuario.setores.nome",
						"usuario.setores.sigla",
						"isUsuario",
						"usuario.agendaDesativacao.id",
						"usuario.agendaDesativacao.motivo.id",
						"usuario.agendaDesativacao.motivo.descricao",
						"usuario.agendaDesativacao.descricao",
						"usuario.agendaDesativacao.tempoIndeterminado",
						"usuario.agendaDesativacao.dataInicio",
						"usuario.agendaDesativacao.dataFim"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

		findByCnpj = new JSONSerializer()
				.include(
						"id",
						"tipo",
						"razaoSocial",
						"cnpj",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.caixaPostal",
						"enderecos.bairro",
						"enderecos.complemento",
						"enderecos.cep",
						"enderecos.descricaoAcesso",
						"enderecos.municipio.id",
						"enderecos.municipio.nome",
						"enderecos.municipio.codigoIbge",
						"enderecos.municipio.estado",
						"enderecos.municipio.estado.id",
						"enderecos.municipio.estado.nome",
						"enderecos.municipio.estado.sigla",
						"enderecos.pais.id",
						"enderecos.pais.nome",
						"enderecos.pais.sigla",
						"enderecos.semNumero",
						"contatos.tipo.id",
						"contatos.tipo.descricao",
						"contatos.valor",
						"contatos.principal",
						"usuario.id",
						"usuario.ativo",
						"usuario.temSenha",
						"usuario.email",
						"usuario.perfis.id",
						"usuario.perfis.nome",
						"isUsuario",
						"usuario.agendaDesativacao.id",
						"usuario.agendaDesativacao.motivo.id",
						"usuario.agendaDesativacao.motivo.descricao",
						"usuario.agendaDesativacao.descricao",
						"usuario.agendaDesativacao.tempoIndeterminado",
						"usuario.agendaDesativacao.dataInicio",
						"usuario.agendaDesativacao.dataFim"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);
		
		findByFilter = new JSONSerializer()
				.include(
						"totalItems",
						"pageItems.id",
						"pageItems.razaoSocial",
						"pageItems.cnpj",
						"pageItems.isUsuario",
						"pageItems.enderecos.tipo.id",
						"pageItems.usuario.id",
						"pageItems.usuario.ativo",
						"pageItems.usuario.temSenha",
						"pageItems.usuario.agendaDesativacao.id",
						"pageItems.usuario.agendaDesativacao.dataInicio",
						"pageItems.usuario.agendaDesativacao.dataFim",
						"pageItems.nomeFantasia",
						"pageItems.enderecos.tipo.id",
						"pageItems.enderecos.municipio.nome",
						"pageItems.enderecos.municipio.codigoIbge",
						"pageItems.enderecos.municipio.estado.sigla",
						"pageItems.enderecos.municipio.estado.nome"
				)
				.exclude("*")
				.prettyPrint(prettyPrint);

		externalFindByCnpj = new JSONSerializer()
				.include(
						"id",
						"tipo",
						"razaoSocial",
						"cnpj",
						"isJunta",
						"nomeFantasia",
						"dataConstituicao",
						"inscricaoEstadual",
						"enderecos.id",
						"enderecos.tipo.id",
						"enderecos.zonaLocalizacao",
						"enderecos.logradouro",
						"enderecos.numero",
						"enderecos.caixaPostal",
						"enderecos.bairro",
						"enderecos.complemento",
						"enderecos.cep",
						"enderecos.descricaoAcesso",
						"enderecos.municipio.id",
						"enderecos.municipio.nome",
						"enderecos.municipio.codigoIbge",
						"enderecos.municipio.estado",
						"enderecos.municipio.estado.id",
						"enderecos.municipio.estado.nome",
						"enderecos.municipio.estado.sigla",
						"enderecos.pais.id",
						"enderecos.pais.nome",
						"enderecos.pais.sigla",
						"enderecos.semNumero",
						"contatos.tipo.id",
						"contatos.tipo.descricao",
						"contatos.valor",
						"contatos.principal",
						"isUsuario"
				)
				.exclude("*")
				.transform(new TipoEnumTransformer(), Tipo.class)
				.prettyPrint(prettyPrint);

	}

}
