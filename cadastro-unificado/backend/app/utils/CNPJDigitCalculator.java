package utils;


public class CNPJDigitCalculator {

	public static String calculate(String cnpjNoDigits) {

		DigitCalculator digitoPara = new DigitCalculator(cnpjNoDigits);
		digitoPara.useModuloComplement().withReplacement("0",10,11).withModulus(11);

		String digito1 = digitoPara.calculate();
		digitoPara.addDigit(digito1);
		String digito2 = digitoPara.calculate();

		return digito1 + digito2;

	}

}
