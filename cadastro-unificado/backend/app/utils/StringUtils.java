package utils;

import java.text.Normalizer;

public class StringUtils {

	public static String removeAcentos(String string) {

		return Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

	}

}