package utils;

public class TituloEleitoralDigitCalculator {

	public static String calculate(String tituloNoDigit) {

		int length = tituloNoDigit.length();

		String sequencial = tituloNoDigit.substring(0, length - 2);

		String digito1 = new DigitCalculator(sequencial)
				.useModuloComplement()
				.withReplacement("0", 10, 11)
				.withModulus(11)
				.calculate();

		String codigoEstado = tituloNoDigit.substring(length - 2, length);

		String digito2 = new DigitCalculator(codigoEstado + digito1)
				.useModuloComplement()
				.withReplacement("0", 10, 11)
				.withModulus(11)
				.calculate();

		return digito1 + digito2;
	}

}
