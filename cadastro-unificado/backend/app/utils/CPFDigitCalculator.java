package utils;


public class CPFDigitCalculator {

	public static String calculate(String cpfNoDigits) {

		DigitCalculator digitoPara = new DigitCalculator(cpfNoDigits);
		digitoPara.withMultipliers(2, 11).useModuloComplement().withReplacement("0",10,11).withModulus(11);

		String digito1 = digitoPara.calculate();
		digitoPara.addDigit(digito1);
		String digito2 = digitoPara.calculate();

		return digito1 + digito2;

	}

}
