package utils;

import play.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DataUtils {

	public static Date formataData(String data) {

		String[] formatos = {
				"dd/MM/yyyy",
				"dd/MM/yyyy hh:mm:ss",
				"yyyy/MM/dd",
				"yyyy/MM/dd hh:mm:ss",
				"yyyy-MM-dd",
				"yyyy-MM-dd hh:mm:ss",
				"yyyy-MM-dd hh:mm:ss.ms",
				"yyyy.MM.dd",
				"yyyy.MM.dd hh:mm:ss",
				"yyyyMMdd"
		};

		for(String formato : formatos) {

			try {

				DateFormat formatter = new SimpleDateFormat(formato);

				java.util.Date dataFormatada = formatter.parse(data);

				return dataFormatada;

			} catch (ParseException e) {

			}
		}

		Logger.error("formataData - " + data);

		return null;
	}

	public static Date dataRedeSimples(String data) {

		if(data == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(1900, Calendar.JANUARY, 1);
			return cal.getTime();
		}

		return formataData(data);
	}

	public static Date parseDate(String formato, String data) {

		try {
			return new SimpleDateFormat(formato).parse(data);

		} catch (ParseException e) {

			throw new RuntimeException(e);
		}

	}
}
