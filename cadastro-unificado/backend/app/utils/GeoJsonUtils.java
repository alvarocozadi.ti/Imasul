package utils;

import com.vividsolutions.jts.geom.Geometry;
import org.geotools.geojson.geom.GeometryJSON;
import play.Play;

import java.io.IOException;

public class GeoJsonUtils {

	public static final Integer SRID = Integer.valueOf(Play.configuration.getProperty("application.geo.srid"));

	public static final Integer PRECISION = Integer.valueOf(Play.configuration.getProperty("application.geo.precision"));

	public static Geometry toGeometry(String geoJson) {

		try {

			GeometryJSON gjson = new GeometryJSON();
			Geometry geometry;
			geometry = gjson.read(geoJson);
			geometry.setSRID(SRID);

			return geometry;

		} catch (IOException e) {

			throw new RuntimeException(e);
		}

	}

	public static String toGeoJson(Geometry geometry){

		GeometryJSON gjson = new GeometryJSON(PRECISION);

		return gjson.toString(geometry);

	}
}