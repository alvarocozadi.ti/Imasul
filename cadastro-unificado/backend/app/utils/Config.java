package utils;

import org.apache.log4j.Logger;
import play.Play;

public class Config {

	/*	Configuração da sincronia com Siriema	 */
	public static final String API_SIRIEMA_URL = Play.configuration.getProperty("api.siriema.url");

	/*	Configuração de Log rede Simples	 */

	public static final Logger RedeSimplesLogger = Logger.getLogger("rs");

	/*	Config de	de cadastro da rede simples */
	public static final String SEM_ENDERECO = "SEM_ENDERECO";


	/* Configuracões de arquivos temporários */

	public static final int HOURS_BLOQUED = getIntConfig("bloqueioPessoas.hoursBloqued");
	public static final Integer ID_PORTAL = 1;
	public static final String PERFIL_EXTERNO = "Externo";
	public static final Integer ID_CADASTRO = 2;

	/*
	 * Métodos utilitários
	 */

	private static Long getLongConfig(String configKey) {

		String config = Play.configuration.getProperty(configKey);

		if(config != null && !config.isEmpty()) {

			return Long.parseLong(config);

		}

		return null;

	}

	private static int getIntConfig(String configKey) {

		String config = Play.configuration.getProperty(configKey);

		if(config != null && !config.isEmpty()) {

			return Integer.parseInt(config);

		}

		return 0;

	}

}
