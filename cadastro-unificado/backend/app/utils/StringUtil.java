package utils;

import exceptions.BaseException;

public class StringUtil {

	public static String coverMail(String email) {

		if(email == null) {

			throw new BaseException().userMessage("erro.padrao");

		}

		String antesArroba = email.split("@")[0];

		if(antesArroba.length() < 3) {

			return email;

		}

		String emailCoberto = antesArroba.substring(0,1);

		for(int i = 1; i < antesArroba.length() - 1; i++) {

			emailCoberto += "*";

		}

		return emailCoberto.concat(antesArroba.substring(antesArroba.length() - 1, antesArroba.length())).concat("@").concat(email.split("@")[1]);

	}

	public static String onlyNumber(String text) {
		return text.replaceAll("[^0-9]", "");
	}
	public static String isNullObject(Object o){
		return o != null ? o.toString():"N/A";
	}

	public static Boolean stringVazia(String text){
		 return text.trim().isEmpty();
	}

}