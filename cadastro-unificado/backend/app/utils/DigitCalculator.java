package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A fluent interface to calculate digits, used by many documents.
 *
 * To exemplify, the digit for the sequence 0000039104766 with multipliers from
 * 2 to 7 and using the modulus 11 is:
 *
 * <pre>
 *	0  0  0  0  0  3  9  1  0  4  7  6  6 (numeric sequence)
 *	2  7  6  5  4  3  2  7  6  5  4  3  2 (multipliers, from the right to the left and repeating)
 *	----------------------------------------- multiplications digit by digit
 *	 0  0  0  0  0  9 18  7  0 20 28 18 12 -- sum = 112
 * </pre>
 *
 * Calculate the modulo of the sum, then calculate the complement of the modulo and if the result
 * is 0, 10 or 11, then the digit will be 1.
 *
 * <pre>
 * 		sum = 112
 * 		modulo = sum % 11 = 2
 * 		complement = 11 - modulo = 9
 * </pre>
 */
public class DigitCalculator {

	private LinkedList<Integer> number;
	private List<Integer> multipliers = new ArrayList<>();
	private boolean complement;
	private int modulus;
	private boolean individualSum;
	private Map<Integer,String> replacements;

	/**
	 * Cria o objeto a ser preenchido com interface fluente e armazena o trecho numérico
	 * em uma lista de algarismos. Isso é necessário porque a linha digitada pode ser
	 * muito maior do que um int suporta.
	 *
	 * @param sequence Refere-se à linha numérica sobre a qual o dígito deve ser calculado
	 */
	public DigitCalculator(String sequence) {

		this.withMultipliers(2, 9);
		this.withModulus(11);

		this.replacements = new HashMap<>();
		this.number = new LinkedList<>();

		char[] digits = sequence.toCharArray();

		for (char digit : digits) {
			this.number.add(Character.getNumericValue(digit));
		}

		Collections.reverse(this.number);

	}

	/**
	 * Para multiplicadores (ou pesos) sequenciais e em ordem crescente, esse método permite
	 * criar a lista de multiplicadores que será usada ciclicamente, caso o número base seja
	 * maior do que a sequência de multiplicadores. Por padrão os multiplicadores são iniciados
	 * de 2 a 9. No momento em que você inserir outro valor este default será sobrescrito.
	 *
	 * @param from Primeiro número do intervalo sequencial de multiplicadores
	 * @param to Último número do intervalo sequencial de multiplicadores
	 * @return this
	 */
	public DigitCalculator withMultipliers(int from, int to) {

		this.multipliers.clear();

		for (int i = from; i <= to; i++) {
			this.multipliers.add(i);
		}

		return this;

	}

	/**
	 * Há documentos em que os multiplicadores não usam todos os números de um intervalo
	 * ou alteram sua ordem. Nesses casos, a lista de multiplicadores pode ser passada
	 * através de varargs.
	 *
	 * @param orderedMultipliers Sequência de inteiros com os multiplicadores em ordem
	 * @return this
	 */
	public DigitCalculator withMultipliers(Integer... orderedMultipliers) {

		this.multipliers = Arrays.asList(orderedMultipliers);

		return this;

	}

	/**
	 * É comum que os geradores de dígito precisem do complementar do módulo em vez
	 * do módulo em sí. Então, a chamada desse método habilita a flag que é usada
	 * no método mod para decidir se o resultado devolvido é o módulo puro ou seu
	 * complementar.
	 *
	 * @return this
	 */
	public DigitCalculator useModuloComplement() {

		this.complement = true;

		return this;

	}

	public DigitCalculator withReplacement(String replacer, Integer... replaceables) {

		for (Integer replaceable : replaceables) {
			this.replacements.put(replaceable, replacer);
		}

		return this;

	}

	/**
	 * @param modulus Inteiro pelo qual o resto será tirado e também seu complementar.
	 * 			O valor padrão é 11.
	 *
	 * @return this
	 */
	public DigitCalculator withModulus(int modulus) {

		this.modulus = modulus;

		return this;

	}

	/**
	 * Indica se ao calcular o módulo, se a soma dos resultados da multiplicação deve ser
	 * considerado digito a dígito.
	 *
	 * Ex: 2 X 9 = 18, irá somar 9 (1 + 8) invés de 18 ao total.
	 *
	 * @return this
	 */
	public DigitCalculator withIndividualSum(){

		this.individualSum = true;

		return this;

	}

	/**
	 * Faz a soma geral das multiplicações dos algarismos pelos multiplicadores, tira o
	 * módulo e devolve seu complementar.
	 *
	 * @return String o dígito vindo do módulo com o número passado e configurações extra.
	 */
	public String calculate() {

		int soma = 0;
		int multiplicadorDaVez = 0;

		for (int algarismo : this.number) {

			int multiplicador = this.multipliers.get(multiplicadorDaVez);
			int total = algarismo * multiplicador;

			soma += this.individualSum ? this.digitosSum(total) : total;
			multiplicadorDaVez = this.getNextMultiplier(multiplicadorDaVez);

		}

		int resultado = soma % this.modulus;

		if (this.complement)
			resultado = this.modulus - resultado;

		if (this.replacements.containsKey(resultado)) {
			return this.replacements.get(resultado);
		}

		return String.valueOf(resultado);

	}


	/**
	 * soma os dígitos do número (até 2)
	 *
	 * Ex: 18 => 9 (1+8), 12 => 3 (1+2)
	 *
	 * @param total
	 * @return
	 */
	private int digitosSum(int total) {
		return (total / 10) + (total % 10);
	}

	/**
	 * Devolve o próximo multiplicador a ser usado, isto é, a próxima posição da lista de
	 * multiplicadores ou, se chegar ao fim da lista, a primeira posição, novamente.
	 *
	 * @param actualMultiplier Essa é a posição do último multiplicador usado.
	 * @return próximo multiplicador
	 */
	private int getNextMultiplier(int actualMultiplier) {

		actualMultiplier++;

		if (actualMultiplier == this.multipliers.size())
			actualMultiplier = 0;

		return actualMultiplier;

	}

	/**
	 * Adiciona um dígito no final do trecho numérico.
	 *
	 * @param digit É o dígito a ser adicionado.
	 * @return this
	 */
	public DigitCalculator addDigit(String digit) {

		this.number.addFirst(Integer.valueOf(digit));

		return this;

	}

}
