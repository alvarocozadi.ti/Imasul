// spec.js
describe('Cadastrar Pessoa Física', function() {
	var EC = protractor.ExpectedConditions;

  it('Login', function() {
    browser.get('http://localhost:9901/');
    browser.manage().window().maximize();
    //browser.waitForAngularEnabled();
   	//browser.waitForAngular();
    //browser.debugger();
    //browser.pause();
  
  	//Wait para a modal abrir
 	var modal = element(by.id('modalLogin'));
	browser.wait(EC.visibilityOf(modal), 5000);
    
    //Insere login
    var nome = browser.findElement(by.name('login'));
    nome.sendKeys('1234');
    nome.isPresent;

    //Seleciona o primeiro elemento do combobox "Perfil"
    var perfil = element.all(by.options('profile for profile in profiles'));
	perfil.isPresent();
    expect(perfil.count()).toEqual(2);
  	perfil.get(1).click();
  	
  	//Clica no botão "Entrar"
	var entrar = browser.findElement(by.xpath('//*[@id="modalLogin"]/div/div/div[3]/button'));
	browser.actions().mouseMove(entrar).perform();
	entrar.click();

	//Wait até a modal fechar
	var blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	//Clica no link "Pessoa Física" no menu lateral
	var pessoafisicalink = browser.findElement(by.xpath('/html/body/div[1]/div/div[1]/ul/li[1]/a'));
	pessoafisicalink.click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	});

	it('CadastraPessoaFisica', function() {
	//Clica em "Cadastrar pessoa física"
	botaocadastro = browser.findElement(by.id('botaoCadastro'));
	botaocadastro.click();

	//Variáveis para os campos de "Dados Pessoais"
	var nomecompleto = browser.findElement(by.name('nome'));
	var cpf = browser.findElement(by.name('cpf'));
	var datadenascimento = browser.findElement(by.name('dataNascimento'));
	var sexo = browser.findElement(by.name('sexo'));
	var nomedamae = browser.findElement(by.name('nome_mae'));
	var estadocivil = element.all(by.options('estadoCivil as estadoCivil.descricao for estadoCivil in estadosCivis track by estadoCivil.nome'));
	var naturalidade = browser.findElement(by.name('naturalidade'));
	var rg = browser.findElement(by.name('rg'));
	var orgaoexpedidor = browser.findElement(by.name('orgao_expedidor'));
	var num_titulo = browser.findElement(by.name('num_titulo'));
	var zona = browser.findElement(by.name('zona'));
	var secao = browser.findElement(by.name('secao'));
	var botaosalvar = browser.findElement(by.className('btn btn-success pull-right'));

	//Dados pessoais
	nomecompleto.sendKeys('Bruno Mazzi');
	cpf.sendKeys('04186154384');
	datadenascimento.sendKeys('02091992');
	sexo.click();
	nomedamae.sendKeys('Elza Mazzi');
	estadocivil.get(1).click();
	naturalidade.sendKeys('Tautabé - SP');
	rg.sendKeys('487295353');
	orgaoexpedidor.sendKeys('SSP');
	num_titulo.sendKeys('376323150191');
	zona.sendKeys('141');
	secao.sendKeys('0324');

	//Endereço principal
	var logradouroprincipal = browser.findElement(by.name('logradouroprincipal'));
	var numeroprincipal = browser.findElement(by.name('numeroprincipal'));
	var bairroprincipal = browser.findElement(by.name('bairroprincipal'));
	var complementoprincipal = browser.findElement(by.name('complementoprincipal'));
	var cepprincipal = browser.findElement(by.name('cepprincipal'));
	var ufprincipal = element.all(by.options('uf.nome for uf in estados track by uf.id'));
	var municipioprincipal = element.all(by.options('municipio.nome for municipio in municipios track by municipio.id'));
	var correspondencia = browser.findElement(by.name('correspondencia'));

	logradouroprincipal.sendKeys('Rua Angelo Padovani');
	numeroprincipal.sendKeys('178');
	bairroprincipal.sendKeys('Dona Wanda');
	complementoprincipal.sendKeys('Casa A');
	cepprincipal.sendKeys('37200000');
	
	ufprincipal.get(13).click();
	municipioprincipal.get(436).click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	correspondencia.click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	botaosalvar.click();
	browser.actions().mouseMove(botaosalvar).perform();
	browser.pause();
	});
	
});
