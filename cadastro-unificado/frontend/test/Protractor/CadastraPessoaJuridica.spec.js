// spec.js
describe('Cadastrar Pessoa Jurídica', function() {
	var EC = protractor.ExpectedConditions;

  it('Login', function() {
    browser.get('http://localhost:9901/');
    browser.manage().window().maximize();
    //browser.waitForAngularEnabled();
   	//browser.waitForAngular();
    //browser.debugger();
    //browser.pause();
  
  	//Wait para a modal abrir
 	var modal = element(by.id('modalLogin'));
	browser.wait(EC.visibilityOf(modal), 5000);
    
    //Insere login
    var nome = browser.findElement(by.name('login'));
    nome.sendKeys('1234');
    nome.isPresent;

    //Seleciona o primeiro elemento do combobox "Perfil"
    var perfil = element.all(by.options('profile for profile in profiles'));
	perfil.isPresent();
    expect(perfil.count()).toEqual(2);
  	perfil.get(1).click();
  	
  	//Clica no botão "Entrar"
	var entrar = browser.findElement(by.xpath('//*[@id="modalLogin"]/div/div/div[3]/button'));
	browser.actions().mouseMove(entrar).perform();
	entrar.click();

	//Wait até a modal fechar
	var blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	});

	it('CadastraPessoaJuridica', function() {
	
	//Clica no link "Pessoa Física" no menu lateral
	var pessoajuridicalink = browser.findElement(by.xpath('/html/body/div[1]/div/div[1]/ul/li[2]/a'));
	pessoajuridicalink.click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	//Clica em "Cadastrar pessoa física"
	botaocadastro = browser.findElement(by.id('botaoCadastro'));
	botaocadastro.click();

	//Variáveis para os campos de "Dados Pessoais"
	var razaosocial = browser.findElement(by.name('razaoSocial'));
	var nomefantasia = browser.findElement(by.name('nomeFantasia'));
	var cnpj =  browser.findElement(by.name('cnpj'));
	var datadeconstituicao = browser.findElement(by.name('dataConstituicao'));
	var inscricaoestadual =  browser.findElement(by.name('inscricaoEstadual'));
	var botaosalvar = browser.findElement(by.className('btn btn-success pull-right'));

	//Dados pessoais
	razaosocial.sendKeys('LEMAF');
	nomefantasia.sendKeys('Laboratório de Estudos e Projetos em Manejo Florestal')
	cnpj.sendKeys('71663888000190');
	datadeconstituicao.sendKeys('01011970');
	inscricaoestadual.sendKeys('1');

	//Endereço principal
	var logradouroprincipal = browser.findElement(by.name('logradouroprincipal'));
	var numeroprincipal = browser.findElement(by.name('numeroprincipal'));
	var bairroprincipal = browser.findElement(by.name('bairroprincipal'));
	var complementoprincipal = browser.findElement(by.name('complementoprincipal'));
	var cepprincipal = browser.findElement(by.name('cepprincipal'));
	var ufprincipal = element.all(by.options('uf.nome for uf in estados track by uf.id'));
	var municipioprincipal = element.all(by.options('municipio.nome for municipio in municipios track by municipio.id'));
	var correspondencia = browser.findElement(by.name('correspondencia'));

	logradouroprincipal.sendKeys('Rua Angelo Padovani');
	numeroprincipal.sendKeys('178');
	bairroprincipal.sendKeys('Dona Wanda');
	complementoprincipal.sendKeys('Casa A');
	cepprincipal.sendKeys('37200000');
	
	ufprincipal.get(13).click();
	municipioprincipal.get(436).click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	correspondencia.click();

	//Wait
	blockuioverlay = element(by.className('blockUI blockOverlay'));
	browser.wait(EC.stalenessOf(blockuioverlay), 5000);

	botaosalvar.click();

	//Verifica mensagem de sucesso do cadastro

	//var mensagem = element(by.className);
	browser.wait(EC.presenceOf(element(by.className('col-xs-11 col-sm-4 alert alert-danger animated fadeInDown'))), 10000);
	//browser.wait(EC.textToBePresentInElement(element(by.className('col-xs-11 col-sm-4 alert alert-danger animated fadeInDown')), 'A inscrição estadual informada já esta cadastrada para outra pessoa jurídica.'),  10000);
	//var mensagem = element(by.className('col-xs-11 col-sm-4 alert alert-danger animated fadeInDown'));
	//expect(mensagem).toMatch('A inscrição estadual informada já esta cadastrada para outra pessoa jurídica.');
	
	browser.pause();
	});

});


