(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/',
		LOGIN_REDIRECT_URL: '/',
		PORTAL_SEGURANCA: 'http://localhost:9900/',
		REDE_SIMPLES: 'http://portalservicos.jucea.am.gov.br',
		COOKIE_DOMAIN: 'localhost',
		COOKIE_PATH: '/'
	});

})();