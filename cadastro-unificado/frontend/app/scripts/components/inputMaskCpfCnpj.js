(function() {

	var module = angular.module('appModule');

	module.directive('inputMaskCpfCnpj', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {

				var model = attrs.ngModel;
				var relacionamento = attrs.name;

				scope.$watch(model, function(newValue) {

					if(newValue) {

						if(relacionamento == "representantesLegais" || relacionamento == "responsaveisLegais") {

							if(newValue.length < 4) {

								scope[relacionamento + 'mask'] = "?9?9?9?9?9?9?9?9?9?9?9";

							} else if(newValue.length >= 4 && newValue.length < 7) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9?9?9?9?9?9";

							} else if(newValue.length >= 7 && newValue.length < 10) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9.?9?9?9?9?9";

							} else if(newValue.length >= 10 && newValue.length <= 11) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9.?9?9?9-?9?9?";

							}

						} else {

							if(newValue.length < 4) {

								scope[relacionamento + 'mask'] = "?9?9?9?9?9?9?9?9?9?9?9?9?9?9";

							} else if(newValue.length >= 4 && newValue.length < 7) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9?9?9?9?9?9?9?9?9";

							} else if(newValue.length >= 7 && newValue.length < 10) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9.?9?9?9?9?9?9?9?9";

							} else if(newValue.length >= 10 && newValue.length < 12) {

								scope[relacionamento + 'mask'] = "?9?9?9.?9?9?9.?9?9?9-?9?9?9?9?9";

							} else {

								if(newValue.length == 12) {

									scope[relacionamento + 'mask'] = "?9?9.?9?9?9.?9?9?9/?9?9?9?9?9?9";

								} else {

									scope[relacionamento + 'mask'] = "?9?9.?9?9?9.?9?9?9/?9?9?9?9-?9?9";

								}

							}

						}

					} else if(relacionamento == "representantesLegais" || relacionamento == "responsaveisLegais") {

						scope[relacionamento + 'mask'] = "?9?9?9?9?9?9?9?9?9?9?9";

					} else {

						scope[relacionamento + 'mask'] = "?9?9?9?9?9?9?9?9?9?9?9?9?9?9?9?9";

					}

				});

			}

		};

	});

})();