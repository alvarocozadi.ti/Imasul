(function() {

	var module = angular.module('appModule');

	module.directive('endereco', function(constantes, paisService, estadoService) {
		return {
			require: ['ngModel'],
			restrict: 'E',
			transclude: true,
			templateUrl: 'sections/components/endereco.html',
			scope: {
				form: '=',
				name: '@',
				ngModel: '=',
				zonasLocalizacao: '=',
				exibirCaixaPostal: '=',
				changeZona: '=',
				cadastroPublico: '='
			},
			link: function($scope, $rootScope, $element, $attrs, $controllers) {

				paisService.findEstados(constantes.ID_BRASIL).then(

					function(response) {

						$scope.estados = response.data;

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

				$scope.findMunicipios = function() {

					estadoService.findMunicipios($scope.ngModel.uf.id).then(
						function(response) {

							$scope.municipios = response.data;

						},
						function(error) {

							$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

						}
					);

				};

				$scope.setZonaLocalizacao = function(zona) {

					$scope.ngModel.zonaLocalizacao = zona;

					$scope.changeZona();

				};

				$scope.$watch('ngModel.uf', function(newValue, oldValue) {

					if($scope.ngModel) {

						if($scope.ngModel.uf && !angular.equals(angular.equals(newValue, oldValue), $scope.ngModel.uf)) {

							$scope.findMunicipios();

						}

					}

				});

				$scope.$watch('ngModel.semNumero', function(newValue) {

					if(newValue) {

						$scope.ngModel.numero = null;

					}

				});

			}

		};

	});

})();