var modulo = angular.module('appModule');

modulo.directive('minDate', function() {

	return {

		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.minDate = function(modelValue, viewValue) {

				if(ngModel.$isEmpty(modelValue)) {

					return true;

				}

				if(modelValue < scope.$eval(attributes.minDate)) {

					if(viewValue != scope.$eval(attributes.minDate).toLocaleString("pt-br").split(' ')[0]) {

						return false;

					}

				}

				return true;

			};

		}

	};

});