var modulo = angular.module('appModule');

modulo.directive('maxDate', function() {

	return {

		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attributes, ngModel) {

			ngModel.$validators.maxDate = function(modelValue, viewValue) {

				if(ngModel.$isEmpty(modelValue))
					return true;

				if(modelValue > scope.$eval(attributes.maxDate))
					return false;

				return true;

			};

		}

	};

});