(function() {

	var module = angular.module('appModule');

	module.directive('contatos', function(tipoContatoService, constantes) {
		return {
			require: ['ngModel'],
			restrict: 'E',
			transclude: true,
			templateUrl: 'sections/components/contatos.html',
			scope: {
				ngModel: '=',
				form: '=',
				isUsuario: '=',
				cadastroPublico: '='
			},
			link: function($scope, $rootScope, $element, $attrs, $controllers) {

				$scope.constantes = constantes;

				$scope.regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

				var variables = {
					1: 'emailAdicional',
					2: 'telResidencial',
					3: 'telComercial',
					4: 'celular'
				};

				$scope.$watch('ngModel', function() {

					if(!$scope.emailPrincipal)
						fillTypeLists();

				});

				tipoContatoService.find().then(

					function(response) {

						$scope.tiposContato = response.data;

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

				$scope.setEmailPrincipal = function() {

					var principal = _.find($scope.ngModel, function(contato) { return contato.principal; });

					if(principal) {

						if(!$scope.emailPrincipal) {

							$scope.ngModel.splice($scope.ngModel.indexOf(principal), 1);

						} else {

							$scope.ngModel[$scope.ngModel.indexOf(principal)].valor =  $scope.emailPrincipal;

						}

					} else if($scope.emailPrincipal) {

						if(!$scope.ngModel)
							$scope.ngModel = [];

						$scope.ngModel.push({
							tipo: _.find($scope.tiposContato, function(tipo) { return tipo.id == constantes.CODIGO_EMAIL; }),
							valor: $scope.emailPrincipal,
							principal: true
						});

					}

				};

				$scope.addToList = function(list, element) {

					angular.element('.btn-blur').blur();

					if(!element)
						return;

					if(!$scope.contatos[list])
						$scope.contatos[list] = [];

					if(!$scope.ngModel)
						$scope.ngModel = [];

					var contato = {
						tipo: _.find($scope.tiposContato, function(tipo) { return tipo.id == list; }),
						valor: element
					};

					$scope.contatos[list].push(contato);
					$scope.ngModel.push(contato);

					$scope[variables[list]] = null;

				};

				$scope.removeFromList = function(list, $index, contato) {

					$scope.contatos[list].splice($index,1);

					$scope.ngModel.splice($scope.ngModel.indexOf(contato),1);

				};

				function fillTypeLists() {

					$scope.contatos = _.groupBy($scope.ngModel, function(contato) {
						return contato.tipo.id;
					});

					var contatoPrincipal = _.find($scope.ngModel, function(contato) {
						return contato.principal;
					});

					$scope.emailPrincipal = contatoPrincipal ? contatoPrincipal.valor : null;

					$scope.contatos[constantes.CODIGO_EMAIL] = _.filter($scope.contatos[constantes.CODIGO_EMAIL], function(contato) { return !contato.principal; });

				}
			}
		};
	});

})();