(function() {

	var nullCharacter = '-';

	angular.module('appModule')

		.filter('cpfCnpj', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				var v = input.toString().replace(/\D/g, "");

				if (v.length <= 11) { //CPF

					v = v.replace(/(\d{3})(\d)/, "$1.$2");
					v = v.replace(/(\d{3})(\d)/, "$1.$2");
					v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");

				} else {

					v = v.replace(/^(\d{2})(\d)/, "$1.$2");
					v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
					v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
					v = v.replace(/(\d{4})(\d)/, "$1-$2");

				}

				return v;

			};
		})

		.filter('cep', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				return input.replace(/\D/g,"").replace(/^(\d{5})(\d)/,"$1-$2");

			};
		})

		.filter('telefoneIfNotEmail', function() {
			return function(input) {

				if(!input.isEmail()) {

					if(input.length > 9)
						return input.replace(/\D/g,"").replace(/^(\d{2})(\d)/g,"($1) $2").replace(/(\d)(\d{4})$/,"$1-$2");

					else
						return input.replace(/(\d)(\d{4})$/,"$1-$2");

				} else {

					return input;

				}

			};
		})

		.filter('tituloEleitor', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				return input.substring(0,4) + " " + input.substring(4,8) + " " + input.substring(8,12);

			};
		})

		.filter('naturalidade', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				return input;

			};
		})

		.filter('estadoCivil', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				return input;

			};
		})

		.filter('numeroRG', function() {
			return function(input) {

				if(!input)
					return nullCharacter;

				return input;

			};
		});

})();