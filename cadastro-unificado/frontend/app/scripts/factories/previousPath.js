(function() {

	angular.module('appModule').factory('previousPath', function($rootScope) {

		$rootScope.lastPath = '';

		$rootScope.$on('$locationChangeStart', function (event, newPath, oldPath) {

			$rootScope.lastPath = oldPath.split('#')[1];

		});

		return {
			getLastPath: function() { return $rootScope.lastPath; }
		};

	});

})();
