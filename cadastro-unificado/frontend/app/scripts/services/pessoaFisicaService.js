(function() {

	'use strict';

	angular
		.module('appModule')
		.service('pessoaFisicaService', function($http, $rootScope) {

			this.cadastrar = function(pessoaFisica, publico) {

				if(publico) {

					return $http.post("public/pessoaFisica", pessoaFisica);

				} else {

					return $http.post("pessoaFisica", pessoaFisica);

				}

			};

			this.listarPorFiltro = function(filtro) {

				return $http.post("pessoasFisicas", filtro);

			};

			this.buscar = function(idPessoa) {

					return $http.get("pessoaFisica/" + idPessoa);

			};


			this.buscarPublic = function(idPessoa) {

				return $http.get("public/pessoaFisica/" + idPessoa);

			};

			this.excluir = function(idPessoa) {

				return $http.delete("pessoaFisica/" + idPessoa);

			};

			this.buscarPorCpf = function(cpf) {

				return $http.get("pessoaFisica/vincular/" + cpf);

			};

			this.publicoBuscarPorCpf = function(cpf) {

				return $http.get("public/pessoaFisica/vincular/" + cpf);

			};

			this.editar = function(pessoaFisica, publico) {

				if(publico) {

					return $http.put("public/pessoaFisica", pessoaFisica);

				} else if($rootScope.rota.public) {

					return $http.put("public/pessoaFisica/atualizacao", pessoaFisica);

				} else {

					return $http.put("pessoaFisica", pessoaFisica);

				}

			};

			this.getInformacoesValidacao = function(cpf) {

				return $http.get('public/informacaoPorCpf/' + cpf);

			};

			this.validarDados = function(dadosValidacao) {

				return $http.post('public/pessoaFisica/validarDados', dadosValidacao);

			};

			this.existePessoaComCpf = function(cpf) {

				return $http.get('public/pessoaFisica/temPessoaComCpf/' + cpf);

			};

		});

})();