(function() {

	var modulo = angular.module('appModule');

	modulo.service('authService', function($http, $rootScope) {

		this.getAuthenticatedUser = function() {

			return $http.get("usuario/usuarioAutenticado");

		};

		this.getAcoesPermitidas = function() {

			return $http.get("usuario/acoesPermitidas");

		};

	});

})();