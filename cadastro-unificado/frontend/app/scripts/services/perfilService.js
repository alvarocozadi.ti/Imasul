(function() {

	'use strict';

	angular
		.module('appModule')
		.service('perfilService', function($http, $rootScope) {

			this.loadPerfis = function() {

				return $http.get("perfis");

			};

			this.loadPerfisPublic = function() {

				return $http.get("public/perfis");

			};

		});

})();