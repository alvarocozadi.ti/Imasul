(function() {

	'use strict';

	angular
		.module('appModule')
		.service('paisService', function($http, $rootScope) {

			this.findEstados = function(id) {

				return $http.get("public/pais/" + id + "/estados");

			};

		});

})();