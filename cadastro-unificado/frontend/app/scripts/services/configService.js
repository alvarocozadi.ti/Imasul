(function() {

	var modulo = angular.module('appModule');

	modulo.service('configService', function($http) {

		this.get = function() {

			return $http.get("public/config");

		};

	});

})();