(function() {

	'use strict';

	angular
		.module('appModule')
		.service('usuarioService', function($http, $rootScope) {

			this.find = function(login) {

				return $http.get("usuario/" + login);

			};

			this.ativar = function(idUsuario) {

				var parameter = 'id=' + idUsuario;

				return $http({
					url: 'usuario/ativar',
					method: "PUT",
					data: parameter,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				});

			};

			this.atualizarSenha = function(parametros) {

				if($rootScope.rota.public) {

					return $http({
						url: "public/usuario/alterarSenha",
						method: "POST",
						data: parametros,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						}
					});

				} else {

					return $http({
						url: "usuario/alterarSenha",
						method: "POST",
						data: parametros,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded'
						}
					});

				}


			};

		});

})();