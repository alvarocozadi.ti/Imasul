(function() {

	'use strict';

	angular
		.module('appModule')
		.service('estadoService', function($http, $rootScope) {

			this.findMunicipios = function(id) {

				return $http.get("public/estado/" + id + "/municipios");

			};

		});

})();