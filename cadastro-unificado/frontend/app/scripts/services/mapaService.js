(function() {

	const measureOptions = {
		localization: 'pt_PT',
		decPoint: '.',
		position: 'topright',
		primaryLengthUnit: 'meters',
		secondaryLengthUnit: 'kilometers',
		primaryAreaUnit: 'hectares',
		secondaryAreaUnit: 'sqmeters'
	};

	const coordinateOptions = {
		position: "bottomright",
		decimals: 6,
		decimalSeperator: ".",
		enableUserInput: false,
		useDMS: false,
		useLatLngOrder: true
	};

	var modulo = angular.module('appModule');

	modulo.service('mapaService', function($document) {

		L.drawLocal = {

			draw: {
				toolbar: {
					actions: {
						title: 'Cancelar desenho',
						text: 'Cancelar'
					},
					undo: {
						title: 'Apagar último ponto desenhado',
						text: 'Apagar último ponto'
					},
					buttons: {
						polyline: 'Desenhar uma linha',
						polygon: 'Desenhar um polígono',
						rectangle: 'Desenhar um retângulo',
						circle: 'Desenhar um círculo',
						marker: 'Desenhar um marcador'
					},
					finish: {
						title: "Concluir desenho",
						text: "Concluir"
					}
				},
				handlers: {
					circle: {
						tooltip: {
							start: 'Clique e arraste para desenhar um círculo.'
						}
					},
					marker: {
						tooltip: {
							start: 'Clique no mapa para posicionar um marcador.'
						}
					},
					polygon: {
						tooltip: {
							start: 'Clique para começar a desenhar um polígono.',
							cont: 'Clique para continuar desenhando.',
							end: 'Clique no primeiro ponto para terminar.'
						}
					},
					polyline: {
						error: '<strong>Erro:</strong> Arestas não podem se cruzar!',
						tooltip: {
							start: 'Clique para desenhar linha.',
							cont: 'Clique para continuar desenhando.',
							end: 'Clique duas vezes para terminar.'
						}
					},
					rectangle: {
						tooltip: {
							start: 'Clique e arraste para desenhar um retângulo.'
						}
					},
					simpleshape: {
						tooltip: {
							end: 'Solte o botão do mouse para terminar.'
						}
					}
				}
			},
			edit: {
				toolbar: {
					actions: {
						save: {
							title: 'Salvar mudanças.',
							text: 'Salvar'
						},
						cancel: {
							title: 'Cancelar edição, descartar mudanças.',
							text: 'Cancelar'
						}
					},
					buttons: {
						edit: 'Editar camada.',
						editDisabled: 'Sem camadas para editar.',
						remove: 'Apagar desenho.',
						removeDisabled: 'Sem camadas para apagar.'
					}
				},
				handlers: {
					edit: {
						tooltip: {
							text: 'Arraste os quadrados para editar.',
							subtext: 'Clique cancelar para desfazer.'
						}
					},
					remove: {
						tooltip: {
							text: 'Clique em um desenho para remover.'
						}
					}
				}
			}

		};

		var newTileLayer = function(){

			return L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				maxZoom: 20,
				minZoom: 3,
				noWrap: true,
				maxNativeZoom:15
				//tms: true
			});

		};

		this.novoMapa = function(idElemento, options) {

			var baseOpts = {
				attributionControl: false,
				zoomControl: false,
				center: [-9.480550, -51.521217],
				zoom: 4
			};

			return L.map(idElemento, Object.assign(baseOpts,options)).setMaxBounds([[84.67351256610522, -174.0234375], [-58.995311187950925, 223.2421875]]);

		};

		this.setMapaAtual = function(mapa) {

			this.mapaAtual = mapa;

			return this;

		};

		this.comMapaFundo = function() {
			this.mapaAtual._mapaFundo = newTileLayer();
			this.mapaAtual.addLayer(this.mapaAtual._mapaFundo);

			return this;

		};

		this.semMapaFundo = function() {

			this.mapaAtual.removeLayer(this.mapaAtual._mapaFundo);

			return this;

		};

		this.comControleZoom = function(options) {

			var zoomOptions = options || {
				position: 'topright',
				zoomInTitle: 'Aumentar zoom',
				zoomOutTitle: 'Diminuir zoom'
			};

			L.control.zoom(zoomOptions).addTo(this.mapaAtual);

			return this;

		};

		this.comControleFullscreen = function(options) {

			var fullscreenOptions = options || {
				position: 'topright',
				title: 'Ativar modo de tela cheia',
				titleCancel: 'Desativar modo de tela cheia'
			};

			L.control.fullscreen(fullscreenOptions).addTo(this.mapaAtual);

			return this;

		};

		this.comControleCoordenadas = function(options) {

			var coordOptions = options || coordinateOptions;

			L.control.coordinates(coordOptions).addTo(this.mapaAtual);

			return this;

		};

		/*this.comControleMedicao = function(options) {

			var msOptions = options || measureOptions;

			new L.Control.Measure(msOptions).addTo(this.mapaAtual);

		};*/

		/*this.comEscala = function() {

			new L.control.betterscale({
				metric: true,
				imperial: false,
				nautic: false
			}).addTo(this.mapaAtual);

		};*/

		this.disableMap = function() {

			this.mapaAtual.dragging.disable();
			this.mapaAtual.touchZoom.disable();
			this.mapaAtual.doubleClickZoom.disable();
			this.mapaAtual.scrollWheelZoom.disable();
			this.mapaAtual.boxZoom.disable();
			this.mapaAtual.keyboard.disable();
			if (this.mapaAtual.tap) this.mapaAtual.tap.disable();
			$document[0].getElementById(this.mapaAtual._container.id).style.cursor='default';


		};

		this.enableMap = function() {

			this.mapaAtual.dragging.enable();
			this.mapaAtual.touchZoom.enable();
			this.mapaAtual.doubleClickZoom.enable();
			this.mapaAtual.scrollWheelZoom.enable();
			this.mapaAtual.boxZoom.enable();
			this.mapaAtual.keyboard.enable();
			if (this.mapaAtual.tap) this.mapaAtual.tap.enable();
			$document[0].getElementById(this.mapaAtual._container.id).style.cursor='';

		};

	});

})();