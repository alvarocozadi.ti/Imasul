(function() {

	'use strict';

	angular
		.module('appModule')
		.service('tipoMotivoService', function($http, $rootScope) {

			this.find = function() {

				return $http.get("public/motivos");

			};

		});

})();