(function() {

	'use strict';

	angular
		.module('appModule')
		.service('agendaDesativacaoService', function($http, $rootScope) {

			this.desativar = function(desativacao) {

				return $http.post("agendaDesativacao", desativacao);

			};

		});

})();