(function() {

	'use strict';

	angular
		.module('appModule')
		.service('pessoaJuridicaService', function($http, $rootScope) {

			this.cadastrar = function(pessoaJuridica, publico) {

				if(publico) {

					return $http.post("public/pessoaJuridica", pessoaJuridica);

				} else {

					return $http.post("pessoaJuridica", pessoaJuridica);

				}

			};

			this.listarPorFiltro = function(filtro) {

				return $http.post("pessoasJuridicas", filtro);

			};

			this.excluir = function(idPessoa) {

				return $http.delete("pessoaJuridica/" + idPessoa);

			};

			this.buscar = function(idPessoa) {

				return $http.get("pessoaJuridica/"+idPessoa);

			};

			this.buscarPublic = function(idPessoa) {

				return $http.get("public/pessoaJuridica/" + idPessoa);

			};

			// this.find = function(idPessoa) {

			// 	return $http.get("pessoaJuridica/" + idPessoa);

			// };

			this.buscarPorCnpj = function(cnpj) {

				return $http.get("pessoaJuridica/vincular/" + cnpj);

			};

			this.publicoBuscarPorCnpj = function(cnpj) {

				return $http.get("public/pessoaJuridica/vincular/" + cnpj);

			};

			this.editar = function(pessoaJuridica, publico) {

				if(publico) {

					return $http.put("public/pessoaJuridica", pessoaJuridica);

				} else if($rootScope.rota.public) {

					return $http.put("public/pessoaJuridica/atualizacao", pessoaJuridica);

				} else {

					return $http.put("pessoaJuridica", pessoaJuridica);

				}

			};

			this.getInformacoesValidacao = function(cnpj) {

				return $http.get('public/informacaoPorCnpj/' + cnpj);

			};

			this.validarDados = function(dadosValidacao) {

				return $http.post('public/pessoaJuridica/validarDados', dadosValidacao);

			};

			this.existePessoaComCnpj = function(cnpj) {

				return $http.get('public/pessoaJuridica/temPessoaComCnpj/' + cnpj);

			};

		});

})();