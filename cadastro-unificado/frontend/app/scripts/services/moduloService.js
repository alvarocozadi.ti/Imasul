(function() {

	'use strict';

	angular
		.module('appModule')
		.service('moduloService', function($http) {

			this.loadModulosComPerfis= function() {

				return $http.get("modulos/buscaModuloComPerfis");

			};

			this.loadModulosComPerfisPublic = function() {

				return $http.get("public/modulos/buscaModuloComPerfis");

			};

		});

})();
