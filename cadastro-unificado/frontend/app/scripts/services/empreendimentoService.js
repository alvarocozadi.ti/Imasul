(function() {

	'use strict';

	angular
		.module('appModule')
		.service('empreendimentoService', function($http, $rootScope) {

			this.listarPorFiltro = function(filtro) {

				return $http.post("empreendimentos", filtro);

			};

			this.buscar = function(id) {

				return $http.get("empreendimento/" + id);

			};

			this.editar = function(empreendimento) {

				return $http.put("empreendimento", empreendimento);

			};

			this.excluir = function(id) {

				return $http.delete("empreendimento/" + id);

			};

			this.buscar = function(idEmpreendimento) {

				return $http.get("empreendimento/" + idEmpreendimento);

			};

			this.buscarPorIdPessoa = function(idPessoa) {

				return $http.get("empreendimentos/pessoa/" + idPessoa);

			};

			this.cadastrar = function(empreendimento) {

				return $http.post("empreendimento", empreendimento);

			};

		});

})();