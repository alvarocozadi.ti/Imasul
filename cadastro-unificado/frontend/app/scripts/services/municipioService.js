(function() {

	'use strict';

	angular
		.module('appModule')
		.service('municipioService', function($http, $rootScope) {

			this.findGeometryMunicipioById = function(id) {

				return $http.get("municipio/" + id + "/geometria");

			};

		});

})();