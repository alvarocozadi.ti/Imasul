(function() {

	'use strict';

	angular
		.module('appModule')
		.service('tipoContatoService', function($http, $rootScope) {

			this.find = function() {

				return $http.get("public/tiposContato");

			};

		});

})();