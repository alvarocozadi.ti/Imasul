(function() {

	'use strict';

	angular
		.module('appModule')
		.service('setorService', function($http) {

			this.loadSetores = function() {

				return $http.get("setores/buscaSetores");

			};

			this.loadSetoresPublic = function() {

				return $http.get("public/setores/buscaSetores");

			};

		});

})();
