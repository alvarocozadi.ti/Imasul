var exports = {};
var app = exports;

(function($) {

	var modulo = angular.module('appModule', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'ui.utils.masks', 'ui.mask', 'ngMask']);

	function Config($routeProvider, $httpProvider, $locationProvider, $uibTooltipProvider) {

		// register the interceptor as a service, intercepts ALL angular ajax http calls
		$httpProvider.interceptors.push('httpInterceptor');

		$locationProvider.hashPrefix('');

		$uibTooltipProvider.options({
			popupDelay: 400
		});

		$routeProvider
			.when('/pessoaFisica/gestao', {
				templateUrl: 'views/sections/pessoaFisica/gestao.html',
				controller: 'GestaoPessoaFisicaController'
			})
			.when('/pessoaFisica/cadastro' , {
				templateUrl: 'views/sections/pessoaFisica/cadastro.html',
				controller: 'CadastroPessoaFisicaController'
			})
			.when('/pessoaFisica/visualizacao/:id', {
				templateUrl: 'views/sections/pessoaFisica/visualizacao.html',
				controller: 'VisualizacaoPessoaFisicaController'
			})
			.when('/pessoaFisica/cadastro/:id' , {
				templateUrl: 'views/sections/pessoaFisica/cadastro.html',
				controller: 'CadastroPessoaFisicaController',
				resolve: {
					isNotPublic: ['$rootScope', function($rootScope) {
						$rootScope.rota.public = false;
						$rootScope.isAtualizacao = false;
						return;
					}]
				}
			})
			.when('/pessoaFisica/cadastro/:id/edicao' , {
				templateUrl: 'views/sections/pessoaFisica/cadastro.html',
				controller: 'CadastroPessoaFisicaController',
				resolve:{
					isNotPublic: ['$rootScope', function($rootScope) {
						$rootScope.rota.public = false;
						$rootScope.isAtualizacao = true;
						return;
					}]
				}
			})
			.when('/public/pessoaFisica/cadastro/:id' , {
				templateUrl: 'views/sections/pessoaFisica/cadastro.html',
				controller: 'CadastroPessoaFisicaController'
			})
			.when('/pessoaJuridica/gestao', {
				templateUrl: 'views/sections/pessoaJuridica/gestao.html',
				controller: 'GestaoPessoaJuridicaController'
			})
			.when('/pessoaJuridica/cadastro', {
				templateUrl: 'views/sections/pessoaJuridica/cadastro.html',
				controller: 'CadastroPessoaJuridicaController'
			})
			.when('/pessoaJuridica/visualizacao/:id', {
				templateUrl: 'views/sections/pessoaJuridica/visualizacao.html',
				controller: 'VisualizacaoPessoaJuridicaController'
			})
			.when('/pessoaJuridica/cadastro/:id', {
				templateUrl: 'views/sections/pessoaJuridica/cadastro.html',
				controller: 'CadastroPessoaJuridicaController',
				resolve: {
					isNotPublic: ['$rootScope', function($rootScope) {
						$rootScope.rota.public = false;
						$rootScope.isAtualizacao = false;
						return;
					}]
				}
			})
			.when('/pessoaJuridica/cadastro/:id/edicao', {
				templateUrl: 'views/sections/pessoaJuridica/cadastro.html',
				controller: 'CadastroPessoaJuridicaController'
			})
			.when('/public/pessoaJuridica/cadastro/:id', {
				templateUrl: 'views/sections/pessoaJuridica/cadastro.html',
				controller: 'CadastroPessoaJuridicaController'
			})
			.when('/empreendimento/gestao', {
				templateUrl: 'views/sections/empreendimento/gestao.html',
				controller: 'GestaoEmpreendimentoController'
			})
			.when('/empreendimento/visualizacao/:id', {
				templateUrl: 'views/sections/empreendimento/visualizacao.html',
				controller: 'VisualizacaoEmpreendimentoController'
			})
			.when('/empreendimento/cadastro', {
				templateUrl: 'views/sections/empreendimento/cadastro.html',
				controller: 'CadastroEmpreendimentoController'
			})
			.when('/empreendimento/cadastro/:id', {
				templateUrl: 'views/sections/empreendimento/cadastro.html',
				controller: 'CadastroEmpreendimentoController'
			})
			.when('/public/validacao/:login', {
				templateUrl: 'views/sections/conta/validacao.html',
				controller: 'ValidacaoContaController'
			})
			.otherwise({
				resolve: {
					redirect: ['$rootScope','$location','authService', function($rootScope, $location, authService) {

						authService.getAuthenticatedUser().then(
							function(response) {

								$rootScope.setUsuario(response.data);

								if(response.data)
									authService.getAcoesPermitidas().then(
										function(response) {

											var usuario = $rootScope.getUsuario();
											usuario.permittedActionsIds = response.data;
											$rootScope.setUsuario(usuario);

											if($rootScope.verificarPermissao($rootScope.AcaoSistema.FILTRAR_PESSOA_FISICA)) {

												return $location.path('/pessoaFisica/gestao');

											} else if($rootScope.verificarPermissao($rootScope.AcaoSistema.LISTAR_PESSOAS_JURIDICAS)) {

												return $location.path('/pessoaJuridica/gestao');

											} else if($rootScope.verificarPermissao($rootScope.AcaoSistema.LISTAR_EMPREENDIMENTOS)) {

												return $location.path('/empreendimento/gestao');

											} else if($rootScope.verificarPermissao($rootScope.AcaoSistema.PESQUISAR_MODULOS)) {

												return $rootScope.goToRoutePortalSeguranca('#/modulo/gestao');

											}

										}

									);
							}

						);

					}]
				}

			});

	}

	modulo.config(Config);
	Config.$inject = ['$routeProvider', '$httpProvider', '$locationProvider', '$uibTooltipProvider'];

	modulo.constant('constantes', {
		"ID_BRASIL": 29,
		"TIPO_ENDERECO_PRINCIPAL": 1,
		"TIPO_ENDERECO_CORRESPONDENCIA": 2,
		"ZONA_URBANA": 0,
		"ZONA_RURAL": 1,
		"CODIGO_PESSOA_FISICA": 0,
		"CODIGO_PESSOA_JURIDICA": 1,
		"CODIGO_EMAIL": 1,
		"CODIGO_RESIDENCIAL": 2,
		"CODIGO_COMERCIAL": 3,
		"CODIGO_CELULAR": 4,
		"statusUsuario": [
			{
				"nome": "Ativo",
				"valor": true
			},
			{
				"nome": "Inativo",
				"valor": false
			}
		],
		"tamanhoPaginacao": 10,
		"PAGINA_VALIDACAO_PESSOA_FISICA": 0,
		"PAGINA_VALIDACAO_PESSOA_JURIDICA": 1,
		"PAGINA_CADASTRO_PESSOA_FISICA": 2,
		"PAGINA_CADASTRO_PESSOA_JURIDICA": 3,
		"PAGINA_REDIRECIONAR_REDE_SIMPLES": 4,
		"ID_PERFIL_PUBLICO": 2,
		"HTTPStatus" : {
			"OK": '200',
			"REDIRECT": '312',
			"UNAUTHORIZED": '401',
			"FORBIDDEN": '403',
			"ACCEPTED": '202'
		},
		"ALERT_WARNING_DELAY_TIME": 15000,
		"ALERT_DEFAULT_DELAY_TIME": 5000
	});

	modulo.controller('AppCtrl', ["$scope", "$rootScope", "$cookies", "constantes", "$location", '$window', "config", "configService",

		function($scope, $rootScope, $cookies, constantes, $location, $window, config, configService) {

			$rootScope.tipos = null;
			$rootScope.rota = {
				entradaUnica: null,
				public: null
			};

			configService.get().then(function(response) {

				$rootScope.tipos = response.data.tipos;

			});

			$.material.ripples();
			$.material.checkbox();
			$.material.radio();

			if($location.path().split('/')[1] == 'public') {

				$rootScope.rota.public = true;

			} else {

				$rootScope.rota.entradaUnica = true;

			}

			$scope.pathAtual = $location.path().split('/')[1] || 'pessoaFisica';

			$scope.goToRoute = function(path) {

				$scope.pathAtual = path.split('/')[0];

				$location.path(path);

			};

			$rootScope.goToRoutePortalSeguranca = function(path) {

				$window.location.href = config.PORTAL_SEGURANCA + path;

			};

			$rootScope.goToRouteRedeSimples = function() {

				$window.open(config.REDE_SIMPLES, '_blank');

			};

			$rootScope.getLinkRedeSimples = function() {

				return config.REDE_SIMPLES;

			};

			$rootScope.cheangePanel = function (panel) {

				panel = !panel;

			};

			$rootScope.AcaoSistema = app.AcaoSistema;

			$rootScope.Mensagens = app.Mensagens;

			$rootScope.setConfig = function(configApp) {

				$cookies.putObject("config", configApp, { path: config.COOKIE_PATH, domain: config.COOKIE_DOMAIN });

			};

			$rootScope.getConfig = function() {

				return $cookies.getObject("config");

			};

			$rootScope.setUsuario = function(usuario) {

				$cookies.putObject("usuario", usuario, { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.getUsuario = function() {

				return $cookies.getObject("usuario");

			};

			$rootScope.removeUsuario = function() {

				$cookies.remove("usuario", { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.setUsuarioPortalSeguranca = function(usuario) {

				$cookies.putObject("usuarioEntradaUnica", usuario, { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.getUsuarioPortalSeguranca = function() {

				return $cookies.getObject("usuarioEntradaUnica", { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.removeUsuarioPortalSeguranca = function() {

				return $cookies.remove("usuarioEntradaUnica", { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.getRedirect = function() {

				return $cookies.get("redirect", { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.removeRedirect = function() {

				$cookies.remove("redirect", { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.setErrorMessage = function(message) {

				return $cookies.putObject("errorMessageAfterRedirect", message, { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.setSuccessMessage = function(message) {

				return $cookies.putObject("successMessageAfterRedirect", message, { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.setWarningMessage = function(message) {

				return $cookies.putObject("warningMessageAfterRedirect", message, { path: '/', domain: config.COOKIE_DOMAIN });

			};

			$rootScope.verifyAndSendMessageToPortal = function(response) {

				if(response.status == constantes.HTTPStatus.OK) {

					$rootScope.setSuccessMessage(response.data.text);

				} else if(response.status == constantes.HTTPStatus.ACCEPTED) {

					$rootScope.setWarningMessage(response.data.text);

				}

			};

			$rootScope.verifyAndSendMessage = function(response) {

				if(response.status == constantes.HTTPStatus.OK) {

					$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

				} else if(response.status == constantes.HTTPStatus.ACCEPTED) {

					$rootScope.$broadcast('showMessageEvent', response.data.text, 'warning', constantes.ALERT_WARNING_DELAY_TIME);

				}

			};

			$rootScope.logout = function() {

				$rootScope.removeUsuario();
				$rootScope.removeUsuarioPortalSeguranca();

				location.href = "logout";

			};

			$rootScope.abrirModal = function(nomeModal) {

				$('.modalVisualizar').collapse('show');

				$('.modalEsconder').collapse('hide');

				$("#" + nomeModal).modal('show').collapse('show');

			};

			$rootScope.fecharModal = function(nomeModal) {
				$("#" + nomeModal).modal('hide');
			};

			$scope.msg = {
				show: false,
				target: 'all'
			};

			$rootScope.httpVaiRedirecionar = false;

			$rootScope.setHttpVaiRedirecionar = function(value){

				$rootScope.httpVaiRedirecionar = value;

			};

			function showMessage(texto, tipo, showTime, hideOnRouteChange, target) {

				if($rootScope.httpVaiRedirecionar) {

					return;

				}

				var delay;

				if(Number.isInteger(showTime)) {

					delay = showTime;

				} else {

					delay = constantes.ALERT_DEFAULT_DELAY_TIME;

				}

				$.notify({ message: texto },{ delay: delay, type: tipo });

			}

			function hideMessage() {

				$scope.msg.show = false;

			}

			$rootScope.$on('$locationChangeStart', function (event, newPath, oldPath) {

				$rootScope.lastPath = oldPath.split('#')[1];

			});

			$scope.$on('$locationChangeStart', function(evt, newUrl, oldUrl) {

				if($rootScope.rota.public) {

					if(newUrl != oldUrl && oldUrl.split('#')[1].split('/')[0] == 'public') {

						$rootScope.goToRoutePortalSeguranca('#/home');

					}

				}

			});

			// funcao usada para mudar a visibilidade da sidenav
			$rootScope.$on('$locationChangeStart', function(evt, newUrl, oldUrl) {

				if($rootScope.rota.public) {

					// verifica se a url buscada é sobre gestao, o qual é privada
					if(newUrl != oldUrl && newUrl.split("#")[1].split('/')[1] == 'gestao') {

						$rootScope.rota.public = false;
						$rootScope.isAtualizacao = false;

					}

				} else if(!$rootScope.rota.public){

					// verifica se a nova url é para uma rota considerada publica para edição do cadastro
					if(newUrl != oldUrl && newUrl.split('#')[1].split('/')[0] == 'public') {

						$rootScope.rota.public = true;
						$rootScope.isAtualizacao = true;

					} else {

						$rootScope.rota.public = false;
						$rootScope.isAtualizacao = false;

					}
				}

			});

			// Verifica se possui a ação sistema. Se não for informado o Permissivel será utilizado o usuário logado
			$rootScope.verificarPermissao = function(idAcao, permissivel) {

				if($rootScope.rota.public) {

					return true;

				}

				if(!permissivel) {

					permissivel = $rootScope.getUsuario();

				}

				return permissivel && permissivel.permittedActionsIds && permissivel.permittedActionsIds.indexOf(idAcao) >= 0;

			};

			// Verifica as permissoes configuradas nas rotas
			$scope.$on('$routeChangeStart', function(event, next) {

				var permission = next.permission;
				var permissions = next.permissions;

				if((permission && !$rootScope.verificarPermissao(permission)) ||
					(permissions && !$rootScope.verificarPermissoes(permissions)) ) {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

				}

				if($rootScope.getUsuarioPortalSeguranca()) {

					return;

				}

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			});

			$rootScope.setUnauthorizedMessageAndRedirectToLogin = function() {

				if($rootScope.rota.public) {

					return;

				}

				$rootScope.setErrorMessage($rootScope.Mensagens.SESSION_TIMEOUT);
				$rootScope.goToRoutePortalSeguranca("#/");

			};

			// Evento de mudanca de rota
			$scope.$on('$routeChangeSuccess', function(scope, next) {

				$rootScope.esconderBotaoMenu = next.esconderBotaoMenu;
				$rootScope.esconderCabecalhoUsuario = next.esconderCabecalhoUsuario;

				if (!$scope.msg)
					return;

				if (!$scope.msg.hideOnRouteChange)
					$scope.msg.hideOnRouteChange = true;
				else
					hideMessage();

			});

			// Evento de exibição de mensagem
			$scope.$on('showMessageEvent', function(event, texto, tipo, showTime, hideOnRouteChange, target) {
				showMessage(texto, tipo, showTime, hideOnRouteChange, target);
			});

			// Evento para remover mensagem
			$scope.$on('hideMessageEvent', function() {
				hideMessage();
			});

		}

	]);

	//for DatePicker options
	modulo.config(['uibDatepickerConfig', function (uibDatepickerConfig) {
		uibDatepickerConfig.showWeeks = false;
	}]);

	//for DatePickerPopup options
	modulo.config(['uibDatepickerPopupConfig', function (uibDatepickerPopupConfig) {
		uibDatepickerPopupConfig.showButtonBar = false;
	}]);

})(jQuery);