(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/cadastro-unificado/',
		LOGIN_REDIRECT_URL: '/cadastro-unificado/',
		PORTAL_SEGURANCA: 'http://licenciamento.imasul.ms.gov.br//portal-seguranca/',
		REDE_SIMPLES: 'https://portalservicos.jucems.ms.gov.br',
		COOKIE_DOMAIN: '.imasul.ms.gov.br',
		COOKIE_PATH: '/cadastro-unificado/'
	});

})();