(function() {

	var modulo = angular.module('appModule');

	modulo.value('config', {

		BASE_URL: '/cadastro-unificado/',
		LOGIN_REDIRECT_URL: '/cadastro-unificado/',
		PORTAL_SEGURANCA: 'http://imasul.ti.lemaf.ufla.br/portal-seguranca/',
		REDE_SIMPLES: 'http://portalservicos.jucea.am.gov.br',
		COOKIE_DOMAIN: 'ti.lemaf.ufla.br',
		COOKIE_PATH: '/cadastro-unificado/'
	});

})();