(function() {

	var modulo = angular.module('appModule');

	modulo.controller('CadastroPessoaFisicaController', function($rootScope, $scope, $location, $routeParams, paisService, estadoService, tipoContatoService, pessoaFisicaService, constantes, moduloService, configService, setorService) {

		$scope.datepickerOptions = {
			maxDate: new Date()
		};

		configService.get().then(function(response) {

			$rootScope.tipos = response.data.tipos;

			init();

		});

		var init = function() {

			var cpfPessoa = $scope.$parent.cpf;
			var tipos = null;

			if(cpfPessoa || $rootScope.rota.public) {

				tipos = $rootScope.tipos;

			} else {

				tipos = $rootScope.getConfig().tipos;

			}

			$scope.constantes = constantes;

			var zonaUrbana = _.find(tipos.ZonaLocalizacao, function(zona){
				return zona.codigo == constantes.ZONA_URBANA;
			});

			var zonaRural = _.find(tipos.ZonaLocalizacao, function(zona){
				return zona.codigo == constantes.ZONA_RURAL;
			});

			var idCorrespondenciaCopia = null;
			var idPrincipalCopia = null;

			$scope.setTituloPagina = function(title) {

				if(title != $scope.tituloPagina) {

					$scope.tituloPagina = title;

				}

			};

			$scope.tituloPagina = '';
			$scope.cadastroPublico = false;
			$scope.editar = false;
			$scope.estadosCivis = tipos.EstadoCivil;
			$scope.sexos = tipos.Sexo;
			$scope.zonasLocalizacao = tipos.ZonaLocalizacao;
			$scope.pessoaFisica = {};
			$scope.pessoaFisica.tituloEleitoral = {};
			$scope.pessoaFisica.estrangeiro = false;
			$scope.pessoaFisica.usuario = {};
			$scope.pessoaFisica.usuario.perfis = [];
			$scope.pessoaFisica.usuario.setores = [];
			$scope.isUsuarioEdicao = false;

			$scope.enderecoPrincipal = {

				tipo: {
					id: constantes.TIPO_ENDERECO_PRINCIPAL
				},
				zonaLocalizacao: zonaUrbana,
				pais: {
					id: constantes.ID_BRASIL
				},
				semNumero: false

			};

			$scope.enderecoCorrespondencia = {

				tipo: {
					id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
				},
				pais: {
					id: constantes.ID_BRASIL
				},
				usarOutro: false,
				zonaLocalizacao: zonaUrbana,
				semNumero: false

			};

			function carregarSetores() {

				setorService.loadSetoresPublic().then(
					function (response) {

						$scope.setoresUsuario = response.data;
						trimSetores($scope.pessoaFisica.usuario.setores);
					}
				);
			}

			function recuperarPessoaPorCpf(cpfPessoa) {

				if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_FISICA)) {

					pessoaFisicaService.publicoBuscarPorCpf(cpfPessoa).then(

						function(response) {

							if(response.data) {

								preencherDadosRecuperados(response);
								$scope.cadastroPublico = true;
								$scope.pessoaFisica.isUsuario = true;
								$scope.pessoaFisica.usuario.perfis = [
									{
										id : constantes.ID_PERFIL_PUBLICO
									}
								];

							} else {

								$scope.cadastroPublico = true;
								$scope.pessoaFisica.cpf = cpfPessoa;
								$scope.pessoaFisica.isUsuario = true;
								$scope.pessoaFisica.usuario.perfis = [
									{
										id : constantes.ID_PERFIL_PUBLICO
									}
								];

							}

							carregarSetores();

						},
						function(response) {

							$rootScope.$broadcast('showMessageEvent', response.data, 'danger');

						}

					);

				} else {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

				}

			}

			if (cpfPessoa) {

				recuperarPessoaPorCpf(cpfPessoa);

			} else if ($rootScope.rota.public) {

				moduloService.loadModulosComPerfisPublic().then(
					function (response) {

						$scope.modulos = response.data;

						var idPessoa = $routeParams.id;

						if (idPessoa) {

							recuperarPessoaPorId(idPessoa);

						}

					},
					function (error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}
				);

			} else {

				moduloService.loadModulosComPerfis().then(
					function (response) {

						$scope.modulos = response.data;

						var idPessoa = $routeParams.id;

						if (idPessoa) {

							recuperarPessoa(idPessoa);

						} else {

							$scope.setTituloPagina('Cadastro de pessoa física');

						}

					},
					function (error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}
				);

			}

			function preencherDadosRecuperados(response) {

				$scope.pessoaFisica = response.data;

				if($rootScope.rota.public) {

					$rootScope.usuario = {};
					$rootScope.usuario.pessoaId = $scope.pessoaFisica.id;
					$rootScope.usuario.nome = $scope.pessoaFisica.nome;
					$rootScope.usuario.cpf = $scope.pessoaFisica.cpf;

				}

				var title = 'Atualização de cadastro de pessoa física';

				if($location.path().split('/').pop() == 'edicao') {

					$rootScope.isAtualizacao = true;

				} else if($rootScope.rota.public) {

					$rootScope.isAtualizacao = false;

				} else {

					title = 'Edição de pessoa física';

				}

				$scope.setTituloPagina(title);

				if($rootScope.rota.public && $rootScope.isAtualizacao) {

					$rootScope.usuario = {};
					$rootScope.usuario.id = $scope.pessoaFisica.id;
					$rootScope.usuario.nome = $scope.pessoaFisica.nome;
					$rootScope.usuario.cpf = $scope.pessoaFisica.cpf;
					$rootScope.usuario.idUsuario = $scope.pessoaFisica.usuario.id;

				}

				$scope.editar = true;

				if($scope.pessoaFisica.isUsuario && $scope.pessoaFisica.usuario) {

					$scope.isUsuarioEdicao = true;

				} else {

					$scope.pessoaFisica.usuario = {};
					$scope.pessoaFisica.usuario.perfis = [];
					$scope.pessoaFisica.usuario.setores = [];

				}

				$scope.enderecoPrincipal =  _.find($scope.pessoaFisica.enderecos, function(endereco) {
					return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
				});

				$scope.enderecoCorrespondencia =  _.find($scope.pessoaFisica.enderecos, function(endereco) {
					return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				});

				tratarDadosRecuperados();

			}

			function recuperarPessoa(idPessoa) {

				pessoaFisicaService.buscar(idPessoa).then(

					function(response) {

						preencherDadosRecuperados(response);
						carregarSetores();

					},
					function(response) {

						$rootScope.$broadcast('showMessageEvent', response.data, 'danger');

					}

				);

			}

			function recuperarPessoaPorId(idPessoa) {

				pessoaFisicaService.buscarPublic(idPessoa).then(

					function(response) {

						preencherDadosRecuperados(response);
						$scope.pessoaFisica.isUsuario = true;
						carregarSetores();

					},
					function(response) {

						$rootScope.$broadcast('showMessageEvent', response.data, 'danger');

					}

				);

			}

			function tratarDadosRecuperados() {

				if($scope.enderecoPrincipal.numero === null && $scope.enderecoPrincipal.zonaLocalizacao.codigo == constantes.ZONA_URBANA) {

					$scope.enderecoPrincipal.semNumero = true;

				}

				if($scope.enderecoCorrespondencia.numero === null) {

					$scope.enderecoCorrespondencia.semNumero = true;

				}

				verifyEnderecos();

			}

			$scope.atualizarNomeHeaders = function() {

				var usuario = $rootScope.getUsuarioPortalSeguranca();

				if(!usuario) {

					return;
				}

				if($scope.pessoaFisica.id !== usuario.pessoaId) {

					return;

				}

				usuario.nome = $scope.pessoaFisica.nome;

				$rootScope.setUsuarioPortalSeguranca(usuario);

			};

			$scope.salvar = function() {

				if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_FISICA)) {

					// if(!validarCampos())
					// 	return;

					// ajustarCampos();
					tratarUsuario();

					if($scope.editar) {

						pessoaFisicaService.editar($scope.pessoaFisica, $scope.cadastroPublico).then(

							function(response) {

								$scope.atualizarNomeHeaders();

								verificarCaminhoRedirectEdicao(response);

							},
							function(error) {

								$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

							}

						);

					} else {

						pessoaFisicaService.cadastrar($scope.pessoaFisica, $scope.cadastroPublico).then(
							function(response) {

								verificarCaminhoRedirectCadastro(response);

							},
							function(error) {

								$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

								$scope.pessoaFisica.tituloEleitoral = {};

							}

						);

					}

				} else {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

				}

			};



			function verificarCaminhoRedirectCadastro(response) {

				if($scope.cadastroPublico) {

					$rootScope.verifyAndSendMessageToPortal(response);
					$rootScope.goToRoutePortalSeguranca('');

				} else {

					$location.path('/pessoaFisica/gestao');

					$rootScope.verifyAndSendMessage(response);

				}

			}

			function verificarCaminhoRedirectEdicao(response) {

				if($scope.cadastroPublico) {

					$rootScope.verifyAndSendMessageToPortal(response);
					$rootScope.goToRoutePortalSeguranca('');

				} else if($location.path().split('/').pop() == 'edicao') {

					$rootScope.rota.public = false;

					if($rootScope.getRedirect()) {

						var location = $rootScope.getRedirect();

						$rootScope.verifyAndSendMessageToPortal(response);
						$rootScope.removeRedirect();
						$rootScope.goToRoutePortalSeguranca("#" + location);

					} else {

						$rootScope.verifyAndSendMessage(response);

						$rootScope.isAtualizacao = false;

						$rootScope.removeRedirect();

						window.history.back();

					}

				} else if($rootScope.rota.public) {

					if($rootScope.getRedirect()) {

						$rootScope.verifyAndSendMessageToPortal(response);
						$rootScope.removeRedirect();
						$rootScope.goToRoutePortalSeguranca('#/home');

					}

				} else {

					$location.path('/pessoaFisica/gestao');

					$rootScope.verifyAndSendMessage(response);

				}

			}

			function tratarUsuario() {

				if($scope.pessoaFisica.isUsuario) {

					var contatoPrincipal = _.find($scope.pessoaFisica.contatos, function(contato) { return contato.principal; });
					$scope.pessoaFisica.usuario.email = contatoPrincipal.valor;
					$scope.pessoaFisica.usuario.login = $scope.pessoaFisica.cpf;

				} else {

					$scope.pessoaFisica.usuario = null;

				}

			}

			function ajustarCampos() {

				if($scope.enderecoPrincipal.semNumero) {

					$scope.enderecoPrincipal.numero = null;

				}

				if($scope.enderecoCorrespondencia.semNumero) {

					$scope.enderecoCorrespondencia.numero = null;

				}

				if(!$scope.enderecoCorrespondencia.usarOutro) {

					enderecoCorrespondenciaCopyPrincipal();

				}

				$scope.pessoaFisica.enderecos = [
					$scope.enderecoPrincipal,
					$scope.enderecoCorrespondencia
				];

				if($scope.pessoaFisica.estrangeiro) {

					$scope.pessoaFisica.cpf = null;
					$scope.pessoaFisica.naturalidade = null;
					$scope.pessoaFisica.tituloEleitoral = null;
					$scope.pessoaFisica.rg = null;

				} else {

					$scope.pessoaFisica.passaporte = null;

					if(!$scope.pessoaFisica.tituloEleitoral)
						return;

					if((!$scope.pessoaFisica.tituloEleitoral.numero || $scope.pessoaFisica.tituloEleitoral.numero === '') &&
						(!$scope.pessoaFisica.tituloEleitoral.zona || $scope.pessoaFisica.tituloEleitoral.zona === '') &&
						(!$scope.pessoaFisica.tituloEleitoral.secao || $scope.pessoaFisica.tituloEleitoral.secao === '')) {

							$scope.pessoaFisica.tituloEleitoral = null;

						}

				}

			}

			function validarCampos() {

				var message = "";

				if($scope.form.emailAdicional.$error.email || $scope.form.celular.$error.brPhoneNumber || $scope.form.telResidencial.$error.brPhoneNumber || $scope.form.telComercial.$error.brPhoneNumber) {

					message = 'Verifique os campos inválidos.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				} else if($scope.form.emailAdicional.$modelValue || $scope.form.celular.$modelValue || $scope.form.telResidencial.$modelValue || $scope.form.telComercial.$modelValue) {

					message = 'Adicionar o contato preenchido.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				} else if($scope.form.$invalid) {

					message = 'Verifique os campos obrigatórios.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				}

				if(message) {

					return false;

				}

				return true;

			}

			$scope.voltar = function() {

				if($scope.cadastroPublico) {

					$rootScope.goToRoutePortalSeguranca('');

				} else if($location.path().split('/').pop() == 'edicao') {

					$rootScope.rota.public = false;
					$rootScope.isAtualizacao = false;

					window.history.back();

				} else if(($rootScope.isAtualizacao && $rootScope.getRedirect())) {

					$rootScope.rota.public = false;
					$rootScope.isAtualizacao = false;

					window.history.back();

				} else if($rootScope.rota.public) {

					$rootScope.goToRoutePortalSeguranca('#/home');

				} else {

					$location.path('/pessoaFisica/gestao');

				}

			};


			$scope.changeZona = function() {

				var diferente = $scope.enderecoPrincipal.diferenteCorrespondencia;
				var zonaPrincipal = $scope.enderecoPrincipal.zonaLocalizacao.codigo;

				cleanEnderecoPrincipal();

				if(zonaPrincipal == constantes.ZONA_RURAL) {

					if(!diferente) {

						cleanEnderecoCorrespondencia();

						$scope.enderecoPrincipal.diferenteCorrespondencia = false;

					} else {

						$scope.enderecoPrincipal.diferenteCorrespondencia = true;

					}

					$scope.enderecoCorrespondencia.usarOutro = true;
					$scope.enderecoPrincipal.zonaLocalizacao = zonaRural;

				} else {

					if(!diferente) {

						$scope.enderecoCorrespondencia.usarOutro = false;
						$scope.enderecoPrincipal.diferenteCorrespondencia = false;

					} else {

						$scope.enderecoCorrespondencia.usarOutro = true;
						$scope.enderecoPrincipal.diferenteCorrespondencia = true;

					}

				}

				$scope.enderecoPrincipal.uf = null;
				$scope.enderecoPrincipal.municipio = null;

			};

			function verifyEnderecos() {

				idPrincipalCopia = $scope.enderecoPrincipal.id;
				idCorrespondenciaCopia = $scope.enderecoCorrespondencia.id;
				$scope.enderecoPrincipal.id = null;
				$scope.enderecoPrincipal.tipo.id = null;
				$scope.enderecoCorrespondencia.id = null;
				$scope.enderecoCorrespondencia.tipo.id = null;

				if(JSON.stringify($scope.enderecoPrincipal) === JSON.stringify($scope.enderecoCorrespondencia)) {

					enderecoCorrespondenciaCopyPrincipal();

					$scope.enderecoPrincipal.diferenteCorrespondencia = false;

				} else {

					$scope.enderecoPrincipal.diferenteCorrespondencia = true;
					$scope.enderecoCorrespondencia.usarOutro = true;

				}

				$scope.enderecoPrincipal.id = idPrincipalCopia;
				$scope.enderecoPrincipal.tipo.id = constantes.TIPO_ENDERECO_PRINCIPAL;
				$scope.enderecoPrincipal.uf = $scope.enderecoPrincipal.municipio.estado;
				$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
				$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				$scope.enderecoCorrespondencia.uf = $scope.enderecoCorrespondencia.municipio.estado;

			}

			function cleanEnderecoCorrespondencia() {

				$scope.enderecoCorrespondencia = {

					id: idCorrespondenciaCopia,
					tipo: {
						id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
					},
					pais: {
						id: constantes.ID_BRASIL
					},
					usarOutro: true,
					zonaLocalizacao: zonaUrbana,
					semNumero: false,
					uf: null,
					municipio: null

				};

			}

			function cleanEnderecoPrincipal() {

				$scope.enderecoPrincipal = {

					id: idPrincipalCopia,
					tipo: {
						id: constantes.TIPO_ENDERECO_PRINCIPAL
					},
					pais: {
						id: constantes.ID_BRASIL
					},
					zonaLocalizacao: zonaUrbana,
					semNumero: false,
					uf: null,
					municipio: null

				};

			}

			function enderecoCorrespondenciaCopyPrincipal() {

				angular.copy($scope.enderecoPrincipal, $scope.enderecoCorrespondencia);
				$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
				$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				$scope.enderecoCorrespondencia.usarOutro = false;
				$scope.enderecoPrincipal.diferenteCorrespondencia = false;

			}

			$scope.ajustarEnderecoCorrespondencia = function() {

				if($scope.enderecoCorrespondencia.usarOutro) {

					cleanEnderecoCorrespondencia();

				} else {

					enderecoCorrespondenciaCopyPrincipal();

				}

			};


			$scope.carregarPerfis = function (modulo) {

				$scope.perfisUsuario = modulo.perfis;
				trimPerfis($scope.pessoaFisica.usuario.perfis);
			};

			$scope.adicionarPerfil = function(perfil) {

				if(!perfil)
					return;

				var index = _.findIndex($scope.perfisUsuario, {
					id: perfil.id
				});
				$scope.perfisUsuario.splice(index, 1);

				$scope.pessoaFisica.usuario.perfis.push(perfil);

			};

			$scope.removerPerfil = function(perfil) {

				var index = _.findIndex($scope.pessoaFisica.usuario.perfis, {
					id: perfil.id
				});
				$scope.pessoaFisica.usuario.perfis.splice(index, 1);

				$scope.perfisUsuario.push(perfil);

			};

			$scope.adicionarSetor = function(setorEscolhido) {

				if(!setorEscolhido)
					return;

				$scope.pessoaFisica.usuario.setores.push(setorEscolhido);

				trimSetores($scope.pessoaFisica.usuario.setores);
			};

			$scope.removerSetor = function(setorEscolhido) {

				var index = _.findIndex($scope.pessoaFisica.usuario.setores, {
					id: setorEscolhido.id
				});
				$scope.pessoaFisica.usuario.setores.splice(index, 1);

				$scope.setoresUsuario.push(setorEscolhido);

			};

			$scope.uncheckIsUsuario = function() {

				$scope.pessoaFisica.isUsuario = false;
				$scope.pessoaFisica.usuario.email = null;
				$scope.pessoaFisica.usuario.login = null;
				$scope.pessoaFisica.usuario.perfis = [];
				$scope.pessoaFisica.usuario.setores = [];

			};

			function trimPerfis(perfis) {

				perfis.forEach(function(perfil) {

					$scope.perfisUsuario.forEach(function (perfilUsuario, index) {

						if(perfilUsuario.id === perfil.id) {
							$scope.perfisUsuario.splice(index, 1);
						}
					});

				});

			}

			function trimSetores(setoresSelecionados) {

				_.each(setoresSelecionados, function(setorSelecionado) {

					var setoresAux = JSON.parse(JSON.stringify($scope.setoresUsuario));

					_.each(setoresAux, function (setor, index) {

						if (setor.id === setorSelecionado.id) {

							$scope.setoresUsuario.splice(index, 1);
						}
					});
				});
			}

		};

	});

})();
