(function() {

	var modulo = angular.module('appModule');

	modulo.controller('VisualizacaoPessoaFisicaController', function($rootScope, $scope, $routeParams, $location, pessoaFisicaService, constantes, empreendimentoService, mapaService) {

		function init() {

			var idPessoa = $routeParams.id;

			buscarPessoaFisicaPorId(idPessoa);

			buscarEmpreendimentosPorIdPessoa(idPessoa);

			$scope.constantes = constantes;

			angular.element(".modalEmpreendimento").on("hidden.bs.modal", function () {

				$scope.map.remove();

			});

		}

		function buscarPessoaFisicaPorId(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_FISICA)) {

				pessoaFisicaService.buscar(idPessoa).then(

					function(response) {

						$scope.pessoaVisualizar = response.data;

						$scope.classRuleContatos = {
							'col-sm-6': $scope.pessoaVisualizar.contatos.length > 6,
							'col-md-12': $scope.pessoaVisualizar.contatos.length < 7
						};

						$scope.pessoaVisualizar.enderecoPrincipal = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.pessoaVisualizar.enderecoCorrespondencia = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function buscarEmpreendimentosPorIdPessoa(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.LISTAR_EMPREENDIMENTOS)) {

				empreendimentoService.buscarPorIdPessoa(idPessoa).then(

					function(response) {

						$scope.empreendimentos = response.data;

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			}

		}

		$scope.visualizarEmpreendimento = function(idEmpreendimento) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_FISICA)) {

				empreendimentoService.buscar(idEmpreendimento).then(

					function(response) {

						$scope.empreendimento = response.data;

						$scope.enderecoPrincipal = _.find($scope.empreendimento.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.enderecoCorrespondencia = _.find($scope.empreendimento.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

						loadMap();

						tratarDadosRecuperados();

						$rootScope.abrirModal('modalVisualizar');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		function loadMap(){

			setTimeout(function(){

				$scope.map = mapaService.novoMapa('mapa');

				mapaService.setMapaAtual($scope.map)
					.comMapaFundo()
					.comControleZoom()
					.comControleFullscreen();

				var layerGeometry = L.featureGroup();

				$scope.map.addLayer(layerGeometry);

				layerGeometry.addLayer(L.geoJSON(JSON.parse($scope.empreendimento.localizacao.geometria)));

				$scope.map.fitBounds(layerGeometry.getBounds(), {maxZoom: 15});

			}, 400);

		}

		function tratarDadosRecuperados() {

			$scope.classRuleContatos = {
				'col-sm-6': $scope.empreendimento.contatos.length > 6,
				'col-md-12': $scope.empreendimento.contatos.length < 7
			};

		}

		$scope.voltar = function() {

			$location.path('/pessoaFisica/gestao');

		};

		$scope.editar = function() {

			$location.path('/pessoaFisica/cadastro/' + $scope.pessoaVisualizar.id);

		};

		init();

	});

})();