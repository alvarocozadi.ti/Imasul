(function() {

	var modulo = angular.module('appModule');

	modulo.controller('GestaoPessoaFisicaController', function($rootScope, $scope, $location, authService, pessoaFisicaService, constantes, usuarioService, moduloService, agendaDesativacaoService, tipoMotivoService) {

		function init() {

			$scope.constantes = constantes;
			$scope.perfisUsuario = [];
			$scope.pessoas = [];
			$scope.filtro = {};
			$scope.filtro.filtroSimplificado = null;
			$scope.pesquisa = 'simplificada';
			$scope.campo = 'Nome';
			$scope.ordenacaoReversa = false;
			$scope.paginacao = new app.Paginacao($scope.constantes.tamanhoPaginacao, $scope.listar);
			$scope.filtro.filtroUsuario = {
				perfil: null,
				ativo: null
			};

			loadModulos();

			findMotivos();

			$scope.listar($scope.campo, $scope.ordenacaoReversa);

		}

		$scope.irCadastro = function() {
			$location.path('/pessoaFisica/cadastro');
		};

		function ordenacaoPadrao() {

			var ordenacao = 'NOME';

			if($scope.campo === 'Municipio')
				ordenacao = 'MUNICIPIO';

			if($scope.ordenacaoReversa === true)
				ordenacao += '_DESC';
			else
				ordenacao += '_ASC';

			return ordenacao;

		}

		$scope.listar = function(campo, ordenacaoReversa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.FILTRAR_PESSOA_FISICA)) {

				$scope.campo = campo;
				$scope.ordenacaoReversa = ordenacaoReversa;
				$scope.filtro.ordenacao = ordenacaoPadrao();
				$scope.filtro.tamanhoPagina = $scope.paginacao.tamanhoPagina;
				$scope.filtro.numeroPagina = $scope.paginacao.paginaAtual;

				if($scope.filtro.filtroUsuario && !$scope.filtro.filtroUsuario.perfil && $scope.filtro.filtroUsuario.ativo === null) {

					$scope.filtro.filtroUsuario = null;

				}

				pessoaFisicaService.listarPorFiltro($scope.filtro).then(
					function(response) {

						$scope.pessoas = response.data.pageItems;

						$scope.pessoas.forEach(function(value, key) {

							value.enderecoPrincipal = _.find(value.enderecos, function(endereco) {
								return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
							});

							if(!value.rg) {

								value.rg = {};
								value.rg.numero = "-";

							}

						});

						$scope.paginacao.atualizar(response.data.totalItems);

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.limparFiltro = function() {

			angular.element('.btn-blur').blur();

			$scope.filtro.filtroSimplificado = null;
			$scope.filtro.nome = null;
			$scope.filtro.cpf = null;
			$scope.filtro.filtroUsuario = {
				perfil: null,
				ativo: null
			};

			$scope.listar();

		};

		$scope.usuarioStatus = function(pessoa) {

			if(pessoa.isUsuario && pessoa.usuario) {

				return pessoa.usuario.ativo ? "Usuário ativo" : "Usuário inativo";

			} else {

				return "Não usuário";

			}

		};

		$scope.abrirModalExcluir = function(pessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.REMOVER_PESSOA_FISICA)) {

				$scope.pessoaExcluir = pessoa;
				$rootScope.abrirModal('modalExcluir');

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.abrirModalAtivar = function(pessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.ATIVAR_DESATIVAR_USUARIO)) {

				$scope.pessoaAtivar = pessoa;
				$scope.temAgendaDesativacaoDataFim = false;

				if($scope.pessoaAtivar.usuario.agendaDesativacao && $scope.pessoaAtivar.usuario.agendaDesativacao.dataFim) {

					$scope.temAgendaDesativacaoDataFim = true;

				}

				$rootScope.abrirModal('modalAtivar');

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}


		};

		$scope.abrirModalDesativar = function(pessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.ATIVAR_DESATIVAR_USUARIO)) {

				$scope.pessoaDesativar = pessoa;

				initForm();

				if($scope.pessoaDesativar.usuario.agendaDesativacao) {

					$scope.temAgendaDesativacao = true;

				}

				$rootScope.abrirModal('modalDesativar');

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.editar = function(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.EDITAR_PESSOA_FISICA)) {

				$('body').removeClass('modal-open');

				$('.modal-backdrop').remove();

				$location.path("/pessoaFisica/cadastro/"+idPessoa);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.excluir = function(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.REMOVER_PESSOA_FISICA)) {

				$rootScope.fecharModal('modalExcluir');

				pessoaFisicaService.excluir(idPessoa).then(

					function(response) {

						$scope.limparFiltro();

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.ativar = function() {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.ATIVAR_DESATIVAR_USUARIO)) {

				usuarioService.ativar($scope.pessoaAtivar.usuario.id).then(
					function(response) {

						$rootScope.fecharModal('modalAtivar');

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

						$scope.listar();

					},
					function(error) {

						$rootScope.fecharModal('modalAtivar');

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.desativar = function() {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.ATIVAR_DESATIVAR_USUARIO)) {

				if($scope.formModal.$invalid) {

					$scope.formModal.$submitted = true;
					return;

				}

				$scope.pessoaDesativar.desativacao.usuario = $scope.pessoaDesativar.usuario;

				agendaDesativacaoService.desativar($scope.pessoaDesativar.desativacao).then(
					function(response) {

						$rootScope.fecharModal('modalDesativar');

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

						$scope.listar();

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.$watch('pessoaDesativar.desativacao.dataInicio', function(newValue) {

			if(newValue) {

				var dataInicio = angular.copy(newValue);
				var novaData = new Date(dataInicio.setDate(dataInicio.getDate() + 1));
				$scope.datepickerOptionsFim.initDate = novaData;

				if(newValue.getDate() == new Date().getDate() || newValue > $scope.datepickerOptionsInicio.minDate) {

					$scope.datepickerOptionsFim.minDate = novaData;

				}

				if($scope.pessoaDesativar.desativacao.dataFim) {

					if(newValue >= $scope.pessoaDesativar.desativacao.dataFim) {

						$scope.pessoaDesativar.desativacao.dataFim = null;

					}

				}

			}

		});

		$scope.mudarPequisa = function(opcao) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.ATIVAR_DESATIVAR_USUARIO)) {

				$scope.pesquisa = opcao;

				$scope.filtro.filtroSimplificado = null;
				$scope.filtro.nome = null;
				$scope.filtro.cpf = null;
				$scope.filtro.filtroUsuario = {
					perfil: null,
					ativo: null
				};

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		function loadModulos() {

			moduloService.loadModulosComPerfis().then(
				function(response) {

					$scope.modulos = response.data;

					$scope.modulos.forEach(function(moduloPerfil) {

						$scope.perfisUsuario = $scope.perfisUsuario.concat(moduloPerfil.perfis);
					});

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		function findMotivos() {

			tipoMotivoService.find().then(
				function(response) {

					$scope.motivos = response.data;

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		$scope.$watch('pessoaDesativar.desativacao.tempoIndeterminado', function(newValue) {

			if(newValue) {

				$scope.pessoaDesativar.desativacao.dataInicio = null;
				$scope.pessoaDesativar.desativacao.dataFim = null;

				$scope.datepickerOptionsInicio.minDate = new Date();
				$scope.datepickerOptionsFim.minDate = new Date(new Date().setDate(new Date().getDate() + 1));

			}

		});

		function initForm() {

			$scope.formModal.$setPristine();
			$scope.formModal.$setUntouched();
			$scope.pessoaDesativar.desativacao = {};
			$scope.pessoaDesativar.desativacao.motivo = null;
			$scope.pessoaDesativar.desativacao.descricao = null;
			$scope.pessoaDesativar.desativacao.tempoIndeterminado = true;
			$scope.pessoaDesativar.desativacao.dataInicio = null;
			$scope.pessoaDesativar.desativacao.dataFim = null;
			$scope.datepickerOptionsInicio.minDate = new Date();
			$scope.datepickerOptionsFim.minDate = new Date(new Date().setDate(new Date().getDate() + 1));
			$scope.temAgendaDesativacao = false;

		}

		init();

	});

})();
