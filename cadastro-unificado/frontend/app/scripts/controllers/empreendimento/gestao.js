(function() {

	var modulo = angular.module('appModule');

	modulo.controller('GestaoEmpreendimentoController', function($rootScope, $scope, $location, empreendimentoService, constantes) {

		function init() {

			$scope.constantes = constantes;
			$scope.empreendimentos = [];
			$scope.filtro = {};
			$scope.filtro.busca = null;
			$scope.campo = 'Denominação';
			$scope.ordenacaoReversa = false;
			$scope.paginacao = new app.Paginacao($scope.constantes.tamanhoPaginacao, $scope.listar);

			$scope.listar($scope.campo, $scope.ordenacaoReversa);

		}

		function ordenacaoPadrao() {

			var ordenacao = 'DENOMINACAO';

			if($scope.campo === 'Municipio')
				ordenacao = 'MUNICIPIO';

			if($scope.ordenacaoReversa === true)
				ordenacao += '_DESC';
			else
				ordenacao += '_ASC';

			return ordenacao;

		}

		$scope.irCadastro = function() {
			$location.path('/empreendimento/cadastro');
		};

		$scope.listar = function(campo, ordenacaoReversa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.LISTAR_EMPREENDIMENTOS)) {

				$scope.campo = campo;
				$scope.ordenacaoReversa = ordenacaoReversa;
				$scope.filtro.ordenacao = ordenacaoPadrao();
				$scope.filtro.tamanhoPagina = $scope.paginacao.tamanhoPagina;
				$scope.filtro.numeroPagina = $scope.paginacao.paginaAtual;
				$scope.filtro.filtrarCadastro = true;

				empreendimentoService.listarPorFiltro($scope.filtro).then(
					function(response) {

						$scope.empreendimentos = response.data.pageItems;

						$scope.empreendimentos.forEach(function(value, key) {

							value.enderecoPrincipal = _.find(value.enderecos, function(endereco) {
								return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
							});

						});

						$scope.paginacao.atualizar(response.data.totalItems);

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.limparFiltro = function() {

			angular.element('.btn-blur').blur();

			$scope.filtro.busca = null;
			$scope.listar();

		};

		$scope.editar = function(idEmpreendimento) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.EDITAR_EMPREENDIMENTO)) {

				$location.path("/empreendimento/cadastro/"+idEmpreendimento);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.abrirModalExcluir = function(empreendimento) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.REMOVER_EMPREENDIMENTO)) {

				$scope.empreendimentoExcluir = empreendimento;
				$rootScope.abrirModal('modalExcluir');

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.excluir = function(idEmpreendimento) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.REMOVER_EMPREENDIMENTO)) {

				$rootScope.fecharModal('modalExcluir');

				empreendimentoService.excluir(idEmpreendimento).then(

					function(response) {

						$scope.limparFiltro();

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		$scope.visualizar = function(id) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.VISUALIZAR_EMPREENDIMENTO)) {

				$location.path("/empreendimento/visualizacao/"+id);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		};

		init();

	});

})();
