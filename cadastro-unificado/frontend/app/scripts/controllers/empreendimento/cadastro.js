(function() {

	var modulo = angular.module('appModule');

	modulo.controller('CadastroEmpreendimentoController', function(
		$rootScope, $scope, $location, $routeParams, $uibModal, mapaService,
		paisService, estadoService, tipoContatoService, empreendimentoService,
		constantes, pessoaFisicaService, pessoaJuridicaService, municipioService) {

		$scope.datepickerOptions = {
			maxDate: new Date()
		};

		$scope.constantes = constantes;

		var tipos = $rootScope.getConfig().tipos;

		var zonaUrbana = _.find(tipos.ZonaLocalizacao, function(zona) {
			return zona.codigo == constantes.ZONA_URBANA;
		});

		var zonaRural = _.find(tipos.ZonaLocalizacao, function(zona){
			return zona.codigo == constantes.ZONA_RURAL;
		});

		var idCorrespondenciaCopia = null;
		var idPrincipalCopia = null;

		$scope.map = mapaService.novoMapa('mapa');

		mapaService.setMapaAtual($scope.map)
			.comMapaFundo()
			.comControleZoom()
			.comControleFullscreen();

		var layerLocalizacao = L.featureGroup();
		$scope.map.addLayer(layerLocalizacao);

		var drawOptions = {
			position: 'topright',
			draw: {
				polyline: false,
				circle: false,
				rectangle: false,
				marker: true,
				circlemarker: false,
				polygon: {
					allowIntersection: false
				}
			},
			edit: {
				featureGroup: layerLocalizacao,
				remove: true
			}
		};

		var drawControl = new L.Control.Draw(drawOptions);
		$scope.map.addControl(drawControl);

		function init() {

			$scope.defaultPlaceholder = {
				addDefaultPlaceholder: false
			};

			$scope.estadosCivis = tipos.EstadoCivil;
			$scope.sexos = tipos.Sexo;
			$scope.zonasLocalizacao = tipos.ZonaLocalizacao;

			$scope.enderecoPrincipal = {

				tipo: {
					id: constantes.TIPO_ENDERECO_PRINCIPAL
				},
				zonaLocalizacao: zonaUrbana,
				pais: {
					id: constantes.ID_BRASIL
				},
				semNumero: false

			};

			$scope.enderecoCorrespondencia = {

				tipo: {
					id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
				},
				pais: {
					id: constantes.ID_BRASIL
				},
				usarOutro: false,
				zonaLocalizacao: zonaUrbana,
				semNumero: false

			};

			$scope.empreendimento = {

				localizacao: {},
				proprietarios: [],
				responsaveisTecnicos: [],
				representantesLegais: [],
				responsaveisLegais: []

			};

			$scope.cadastro = {};
		
			if($routeParams.id) {

				$scope.isEdicao = true;
				recuperarEmpreendimento($routeParams.id);
				$scope.tituloPagina = 'Edição de empreendimento';

			} else {

				$scope.isEdicao = false;
				$scope.tituloPagina = 'Cadastro de empreendimento';

			}

		}

		$scope.$watch('enderecoPrincipal.municipio', function(newValue, oldValue) {

			if(!newValue || _.values(layerLocalizacao._layers).length >= 1) {
				
				return;

			}

			addGeometriaMunicipioById(newValue.id, true);

		});

		function addGeometriaMunicipioById(id, darZoom) {

			municipioService.findGeometryMunicipioById(id).then(

				function(response) {

					if($scope.geoMunicipio && $scope.map.hasLayer($scope.geoMunicipio)) {

						$scope.map.removeLayer($scope.geoMunicipio);
		
					}

					$scope.geoMunicipio = L.geoJSON(JSON.parse(response.data.geometry));
					$scope.map.addLayer($scope.geoMunicipio);
		
					if(darZoom) {

						$scope.map.fitBounds($scope.geoMunicipio.getBounds(), { maxZoom: 15 });

					}

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		function recuperarEmpreendimento(idEmpreendimento) {

			empreendimentoService.buscar(idEmpreendimento).then(

				function(response) {

					$scope.empreendimento = response.data;

					$scope.enderecoPrincipal = _.find($scope.empreendimento.enderecos, function(endereco) {
						return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
					});

					$scope.enderecoCorrespondencia = _.find($scope.empreendimento.enderecos, function(endereco) {
						return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
					});

					tratarDadosRecuperados();

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		function tratarDadosRecuperados() {

			if($scope.enderecoPrincipal.numero === null && $scope.enderecoPrincipal.zonaLocalizacao.codigo == constantes.ZONA_URBANA) {

				$scope.enderecoPrincipal.semNumero = true;

			}

			if($scope.enderecoCorrespondencia.numero === null) {

				$scope.enderecoCorrespondencia.semNumero = true;

			}

			verifyEnderecos();

			L.geoJSON(JSON.parse($scope.empreendimento.localizacao.geometria), {
				onEachFeature: function(feature, layer) {
					layerLocalizacao.addLayer(layer);
				}
			});

			$scope.classRuleContatos = {
				'col-sm-6' : $scope.empreendimento.contatos.length > 6,
				'col-md-12' : $scope.empreendimento.contatos.length < 7
			};

			$scope.map.fitBounds(layerLocalizacao.getBounds(), { maxZoom: 15 });

		}

		$scope.salvar = function() {

			if($routeParams.id) {

				salvarEdicao();

			} else {

				cadastrar();

			}

		};

		function salvarEdicao() {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.EDITAR_EMPREENDIMENTO)) {

				if(!validarCampos())
					return;

				ajustarCampos();

				empreendimentoService.editar($scope.empreendimento).then(
					function(response) {

						$location.path('/empreendimento/gestao');

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function cadastrar() {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_EMPREENDIMENTO)) {

				if(!validarCampos())
					return;

				ajustarCampos();

				empreendimentoService.cadastrar($scope.empreendimento).then(
					function(response) {

						$location.path('/empreendimento/gestao');

						$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function ajustarCampos() {
		
			if(!$scope.empreendimento) {

				$scope.empreendimento = {};
				$scope.empreendimento.localizacao = {};

			}

			if($scope.enderecoPrincipal.semNumero) {

				$scope.enderecoPrincipal.numero = null;

			}

			if($scope.enderecoCorrespondencia.semNumero) {

				$scope.enderecoCorrespondencia.numero = null;

			}

			if(!$scope.enderecoCorrespondencia.usarOutro) {

				enderecoCorrespondenciaCopyPrincipal();

			}

			$scope.empreendimento.localizacao.geometria = JSON.stringify(layerLocalizacao.toGeoJSON().features[0].geometry);

			$scope.empreendimento.enderecos = [
				$scope.enderecoPrincipal,
				$scope.enderecoCorrespondencia
			];

		}

		function validarMapa() {

			if(_.values(layerLocalizacao._layers).length < 1) {

				var message = 'Informe a localização.';

				$rootScope.$broadcast('showMessageEvent', message, 'danger');

				return false;

			}

			return true;

		}

		function validarCampos() {

			var message = "";

			if($scope.form.emailAdicional.$error.email || $scope.form.celular.$error.brPhoneNumber || $scope.form.telResidencial.$error.brPhoneNumber || $scope.form.telComercial.$error.brPhoneNumber) {

				message = 'Verifique os campos inválidos.';

				$rootScope.$broadcast('showMessageEvent', message , 'danger');

			}else if($scope.form.emailAdicional.$modelValue || $scope.form.celular.$modelValue || $scope.form.telResidencial.$modelValue || $scope.form.telComercial.$modelValue) {

				message = 'Adicionar o contato preenchido.';

				$rootScope.$broadcast('showMessageEvent', message , 'danger');

			}else if($scope.form.$invalid) {

				message = 'Verifique os campos obrigatórios.';

				$rootScope.$broadcast('showMessageEvent', message , 'danger');

			} else if(!$scope.empreendimento.pessoa) {

				message = 'É obrigatório vincular uma pessoa física ou jurídica ao empreendimento.';

				$rootScope.$broadcast('showMessageEvent', message , 'danger');

			}

			if(message){

				return false;

			}

			if(!validarMapa()) {

				return false;

			}

			return true;

		}

		$scope.voltar = function() {
			$location.path('/empreendimento/gestao');
		};

		$scope.changeZona = function() {

			var diferente = $scope.enderecoPrincipal.diferenteCorrespondencia;
			var zonaPrincipal = $scope.enderecoPrincipal.zonaLocalizacao.codigo;

			cleanEnderecoPrincipal();

			if(zonaPrincipal == constantes.ZONA_RURAL) {

				if(!diferente) {

					cleanEnderecoCorrespondencia();

					$scope.enderecoPrincipal.diferenteCorrespondencia = false;

				} else {

					$scope.enderecoPrincipal.diferenteCorrespondencia = true;

				}

				$scope.enderecoCorrespondencia.usarOutro = true;
				$scope.enderecoPrincipal.zonaLocalizacao = zonaRural;

			} else {

				if(!diferente) {

					$scope.enderecoCorrespondencia.usarOutro = false;
					$scope.enderecoPrincipal.diferenteCorrespondencia = false;

				} else {

					$scope.enderecoCorrespondencia.usarOutro = true;
					$scope.enderecoPrincipal.diferenteCorrespondencia = true;

				}

			}

			$scope.enderecoPrincipal.uf = null;
			$scope.enderecoPrincipal.municipio = null;

		};

		function cleanEnderecoCorrespondencia() {

			$scope.enderecoCorrespondencia = {

				id: idCorrespondenciaCopia,
				tipo: {
					id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
				},
				pais: {
					id: constantes.ID_BRASIL
				},
				usarOutro: true,
				zonaLocalizacao: zonaUrbana,
				semNumero: false,
				uf: null,
				municipio: null

			};

		}

		function cleanEnderecoPrincipal() {

			$scope.enderecoPrincipal = {

				id: idPrincipalCopia,
				tipo: {
					id: constantes.TIPO_ENDERECO_PRINCIPAL
				},
				pais: {
					id: constantes.ID_BRASIL
				},
				zonaLocalizacao: zonaUrbana,
				semNumero: false,
				uf: null,
				municipio: null

			};

		}

		function enderecoCorrespondenciaCopyPrincipal() {

			angular.copy($scope.enderecoPrincipal, $scope.enderecoCorrespondencia);
			$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
			$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
			$scope.enderecoCorrespondencia.usarOutro = false;
			$scope.enderecoPrincipal.diferenteCorrespondencia = false;

		}

		$scope.ajustarEnderecoCorrespondencia = function() {

			if($scope.enderecoCorrespondencia.usarOutro) {

				cleanEnderecoCorrespondencia();

			} else {

				enderecoCorrespondenciaCopyPrincipal();

			}

		};

		function verifyEnderecos() {

			idPrincipalCopia = $scope.enderecoPrincipal.id;
			idCorrespondenciaCopia = $scope.enderecoCorrespondencia.id;
			$scope.enderecoPrincipal.id = null;
			$scope.enderecoPrincipal.tipo.id = null;
			$scope.enderecoCorrespondencia.id = null;
			$scope.enderecoCorrespondencia.tipo.id = null;

			if(JSON.stringify($scope.enderecoPrincipal) === JSON.stringify($scope.enderecoCorrespondencia)) {

				enderecoCorrespondenciaCopyPrincipal();

				$scope.enderecoPrincipal.diferenteCorrespondencia = false;

			} else {

				$scope.enderecoPrincipal.diferenteCorrespondencia = true;
				$scope.enderecoCorrespondencia.usarOutro = true;

			}

			$scope.enderecoPrincipal.id = idPrincipalCopia;
			$scope.enderecoPrincipal.tipo.id = constantes.TIPO_ENDERECO_PRINCIPAL;
			$scope.enderecoPrincipal.uf = $scope.enderecoPrincipal.municipio.estado;
			$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
			$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
			$scope.enderecoCorrespondencia.uf = $scope.enderecoCorrespondencia.municipio.estado;

			addGeometriaMunicipioById($scope.enderecoPrincipal.municipio.id, false);

		}

		$scope.vincular = function(relacionamento) {

			if(relacionamento === "pfPj") {

				vincularPFouPJ(relacionamento);

			} else {

				var pessoaVinculada = $scope.empreendimento[relacionamento].some(function(el) { return el.id == $scope.pessoa.id; });

				if(!pessoaVinculada) {
	
					$scope.empreendimento[relacionamento].push($scope.pessoa);
	
				} else {
	
					$rootScope.$broadcast('showMessageEvent', 'Pessoa já vinculada!', 'danger');
	
				}
	
				$scope.cadastro[relacionamento + 'CpfCnpj'] = "";
				$scope[relacionamento + 'NomeRazaoSocial'] = "";

			}

		};

		function vincularPFouPJ(relacionamento) {

			var pessoaVinculada = $scope.empreendimento.pessoa === $scope.pessoa.id ? $scope.empreendimento.pessoa : null;

			if(!pessoaVinculada) {

				$scope.empreendimento.pessoa = $scope.pessoa;

			} else {

				$rootScope.$broadcast('showMessageEvent', 'Pessoa já vinculada!', 'danger');

			}

			$scope.cadastro[relacionamento + 'CpfCnpj'] = "";
			$scope[relacionamento + 'NomeRazaoSocial'] = "";

		}

		// modal de Confirmação de desvinculação de usuário
		var indexDesvincular;
		var listaDesvincular;

		$scope.abrirModalDesvincular = function(index, pessoa, lista) {

			indexDesvincular = index;
			listaDesvincular = lista;
			$scope.pessoaDesvincular = pessoa;

			$rootScope.abrirModal('modalDesvincular');

		};

		$scope.desvincular = function() {

			listaDesvincular.splice(indexDesvincular, 1);

			$rootScope.fecharModal('modalDesvincular');

		};

		$scope.map.on(L.Draw.Event.CREATED, function(e) {

			$scope.$apply(function() {

				if(_.values(layerLocalizacao._layers).length > 0) {

					$rootScope.$broadcast('showMessageEvent', "É permitido definir apenas uma geometria", 'warning');

				} else {

					layerLocalizacao.addLayer(e.layer);

				}

				if(layerLocalizacao.toGeoJSON().features[0].geometry.coordinates[0].length < 4) {

					$rootScope.$broadcast('showMessageEvent', "É necessário definir ao menos 3 pontos.", 'warning');

					layerLocalizacao.removeLayer(e.layer);

				}

			});

		});

		$scope.visualizarPessoa = function(pessoa) {

			if(pessoa.tipo.codigo === constantes.CODIGO_PESSOA_FISICA) {

				visualizarPessoaFisica(pessoa.id);

			} else {

				visualizarPessoaJuridica(pessoa.id);

			}

		};

		function visualizarPessoaFisica(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.VISUALIZAR_PESSOA_FISICA)) {

				pessoaFisicaService.buscar(idPessoa).then(

					function(response) {

						$scope.pessoaVisualizar = response.data;

						tratarDadosRecuperadosPessoa();

						$scope.pessoaVisualizar.enderecoPrincipal = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.pessoaVisualizar.enderecoCorrespondencia = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

						buscarEmpreendimentoPorIdPessoa(idPessoa);

						$rootScope.abrirModal('modalVisualizar');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function visualizarPessoaJuridica(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.VISUALIZAR_PESSOA_JURIDICA)) {

				pessoaJuridicaService.buscar(idPessoa).then(

					function(response) {

						$scope.pessoaVisualizar = response.data;

						tratarDadosRecuperadosPessoa();

						$scope.pessoaVisualizar.enderecoPrincipal = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.pessoaVisualizar.enderecoCorrespondencia = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

						buscarEmpreendimentoPorIdPessoa(idPessoa);

						$rootScope.abrirModal('modalVisualizar');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

					}

				);

			} else {

				$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function tratarDadosRecuperadosPessoa() {

			$scope.classRuleContatosPessoa = {
				'col-sm-6': $scope.pessoaVisualizar.contatos.length > 6,
				'col-md-12': $scope.pessoaVisualizar.contatos.length < 7
			};

		}

		function buscarEmpreendimentoPorIdPessoa(idPessoa) {

			empreendimentoService.buscarPorIdPessoa(idPessoa).then(

				function(response) {

					$scope.empreendimentos = response.data;

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		$scope.limparCampos = function(relacionamento) {

			$scope[relacionamento + 'NomeRazaoSocial'] = "";
			$scope[relacionamento + 'Mensagem'] = false;

		};

		$scope.buscar = function(relacionamento) {

			angular.element('.btn-blur').blur();

			if(!$scope.cadastro[relacionamento + 'CpfCnpj']) {
				return;
			}

			if($scope.cadastro[relacionamento + 'CpfCnpj'].length <= 11) {

				$scope.cpf = $scope.cadastro[relacionamento + 'CpfCnpj'];

				buscarPessoaFisica(relacionamento);

			} else {

				$scope.cnpj = $scope.cadastro[relacionamento + 'CpfCnpj'];

				buscarPessoaJuridica(relacionamento);
			}

		};

		function buscarPessoaFisica(relacionamento) {

			pessoaFisicaService.buscarPorCpf($scope.cpf).then(
				function(response) {

					$scope.pessoa = response.data;
					setEnderecosBuscadosPessoa($scope.pessoa);
					setContatosBuscadosPessoa($scope.pessoa);

					if(!$scope.pessoa) {

						$scope[relacionamento + 'Mensagem'] = true;

					} else {

						$scope[relacionamento + 'Mensagem'] = false;
						$scope[relacionamento + 'NomeRazaoSocial'] = $scope.pessoa.nome;

					}

				}

			);

		}

		function buscarPessoaJuridica(relacionamento) {

			pessoaJuridicaService.buscarPorCnpj($scope.cnpj).then(
				function(response) {

					$scope.pessoa = response.data;
					setEnderecosBuscadosPessoa($scope.pessoa);
					setContatosBuscadosPessoa($scope.pessoa);

					if(!$scope.pessoa) {

						$scope[relacionamento + 'Mensagem'] = true;

					} else {

						$scope[relacionamento + 'Mensagem'] = false;
						$scope[relacionamento + 'NomeRazaoSocial'] = $scope.pessoa.razaoSocial;

					}

				}

			);

		}

		function setEnderecosBuscadosPessoa(pessoaSalva) {

			var endPrincipal = null;
			var endCorrespondencia = null;
			var usarEndCorrespondencia = false;

			pessoaSalva.enderecos.forEach(function(endereco) {

				if (endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL) {
					endPrincipal = endereco;
				}

				if (endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA) {
					endCorrespondencia = endereco;
				}
			});

			$scope.enderecoPrincipal = endPrincipal;
			$scope.enderecoPrincipal.uf = endPrincipal.municipio.estado;
			$scope.enderecoPrincipal.municipio = endPrincipal.municipio;

			if ($scope.enderecoPrincipal.numero === null && $scope.enderecoPrincipal.zonaLocalizacao.codigo == constantes.ZONA_URBANA) {

				$scope.enderecoPrincipal.semNumero = true;
			}

			usarEndCorrespondencia = !isEnderecosIguais(endPrincipal, endCorrespondencia);

			if (usarEndCorrespondencia) {

				$scope.enderecoCorrespondencia = endCorrespondencia;
				$scope.enderecoCorrespondencia.usarOutro = usarEndCorrespondencia;
				$scope.enderecoCorrespondencia.uf = endCorrespondencia.municipio.estado;
				$scope.enderecoCorrespondencia.municipio = endCorrespondencia.municipio;
				$scope.enderecoCorrespondencia.semNumero = $scope.enderecoCorrespondencia.numero ? false : true;
			}
		}

		function isEnderecosIguais(e1, e2) {

			if (!e1 || !e2) {
				return false;
			}

			if (e1.bairro !== e2.bairro || e1.caixaPostal !== e2.caixaPostal || e1.cep !== e2.cep ||
				e1.complemento !== e2.complemento || e1.descricaoAcesso !== e2.descricaoAcesso ||
				e1.logradouro !== e2.logradouro || e1.municipio.estado.id !== e2.municipio.estado.id ||
				e1.municipio.id !== e2.municipio.id || e1.numero !== e2.numero || e1.pais.id !== e2.pais.id ||
				e1.zonaLocalizacao.codigo !== e2.zonaLocalizacao.codigo) {

				return false;
			}

			return true;
		}

		function setContatosBuscadosPessoa(pessoaSalva) {

			$scope.empreendimento.contatos = pessoaSalva.contatos;

		}

		init();

	});

})();
