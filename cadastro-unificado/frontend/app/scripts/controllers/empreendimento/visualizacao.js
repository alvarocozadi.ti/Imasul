(function() {

	var modulo = angular.module('appModule');

	modulo.controller('VisualizacaoEmpreendimentoController', function($rootScope, $scope, $location, $routeParams, empreendimentoService, constantes, mapaService, pessoaFisicaService, pessoaJuridicaService) {

		function init() {

			var idEmpreendimento = $routeParams.id;

			recuperarEmpreendimento(idEmpreendimento);

			$scope.constantes = constantes;

			$scope.map = mapaService.novoMapa('mapa');

			mapaService.setMapaAtual($scope.map)
				.comMapaFundo()
				.comControleZoom()
				.comControleFullscreen();

		}

		function recuperarEmpreendimento(idEmpreendimento) {

			empreendimentoService.buscar(idEmpreendimento).then(

				function(response) {

					$scope.empreendimento = response.data;

					$scope.enderecoPrincipal = _.find($scope.empreendimento.enderecos, function(endereco) {
						return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
					});

					$scope.enderecoCorrespondencia = _.find($scope.empreendimento.enderecos, function(endereco) {
						return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
					});

					var layerGeometry = L.featureGroup();

					$scope.map.addLayer(layerGeometry);

					layerGeometry.addLayer(L.geoJSON(JSON.parse($scope.empreendimento.localizacao.geometria)));

					$scope.map.fitBounds(layerGeometry.getBounds(),  {maxZoom: 15});

					tratarDadosRecuperados();

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		function tratarDadosRecuperados() {

			$scope.classRuleContatos = {
				'col-sm-6': $scope.empreendimento.contatos.length > 6,
				'col-md-12': $scope.empreendimento.contatos.length < 7
			};

		}

		$scope.voltar = function() {

			$location.path('/empreendimento/gestao');

		};

		$scope.editar = function() {

		if($rootScope.verificarPermissao($rootScope.AcaoSistema.EDITAR_EMPREENDIMENTO)) {

			$location.path('/empreendimento/edicao/'+$scope.empreendimento.id);

		} else {

			$rootScope.setUnauthorizedMessageAndRedirectToLogin();

		}

		};

		$scope.visualizarPessoa = function(pessoa) {

			if(pessoa.tipo.codigo === constantes.CODIGO_PESSOA_FISICA) {

				visualizarPessoaFisica(pessoa.id);

			} else {

				visualizarPessoaJuridica(pessoa.id);

			}

		};

		function visualizarPessoaFisica(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.VISUALIZAR_PESSOA_FISICA)) {

				pessoaFisicaService.buscar(idPessoa).then(

					function(response) {

						$scope.pessoaVisualizar = response.data;

						tratarDadosRecuperadosPessoa();

						$scope.pessoaVisualizar.enderecoPrincipal = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.pessoaVisualizar.enderecoCorrespondencia = _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

						buscarEmpreendimentosPorIdPessoa(idPessoa);

						$rootScope.abrirModal('modalVisualizar');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function visualizarPessoaJuridica(idPessoa) {

			if($rootScope.verificarPermissao($rootScope.AcaoSistema.VISUALIZAR_PESSOA_JURIDICA)) {

				pessoaJuridicaService.buscar(idPessoa).then(

					function(response) {

						$scope.pessoaVisualizar = response.data;

						tratarDadosRecuperadosPessoa();

						$scope.pessoaVisualizar.enderecoPrincipal =  _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
						});

						$scope.pessoaVisualizar.enderecoCorrespondencia =  _.find($scope.pessoaVisualizar.enderecos, function(endereco) {
							return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
						});

						buscarEmpreendimentosPorIdPessoa(idPessoa);

						$rootScope.abrirModal('modalVisualizar');

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			} else {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

			}

		}

		function tratarDadosRecuperadosPessoa() {

			$scope.classRuleContatosPessoa = {
				'col-sm-6': $scope.pessoaVisualizar.contatos.length > 6,
				'col-md-12': $scope.pessoaVisualizar.contatos.length < 7
			};

		}

		function buscarEmpreendimentosPorIdPessoa(idPessoa) {

			empreendimentoService.buscarPorIdPessoa(idPessoa).then(

				function(response) {

					$scope.empreendimentos = response.data;

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		init();

	});

})();
