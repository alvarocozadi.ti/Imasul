(function() {

	var modulo = angular.module('appModule');

	modulo.controller('InicialController', function($rootScope, $scope, configService, authService, $location, usuarioService, config) {

		$scope.atualizarCadastro = function() {

			var usuario = $rootScope.usuario;

			if(usuario) {

				if(usuario.cpf) {

					$location.path('/pessoaFisica/cadastro/' + usuario.id);

				} else {

					$location.path('/pessoaJuridica/cadastro/' + usuario.id);

				}

			} else {

				usuario = $rootScope.getUsuario();

				if(usuario.pessoa.cpf) {

					$location.path('/pessoaFisica/cadastro/' + usuario.id);

				} else {

					$location.path('/pessoaJuridica/cadastro/' + usuario.id);

				}

			}

		};

		$scope.senhaAtual = null;
		$scope.senhaConfirmacao = null;
		$scope.senhaNova = null;

		function initForm() {

			$scope.formModalAtualizarSenha.$setPristine();
			$scope.formModalAtualizarSenha.$setUntouched();
			$scope.senhaAtual = null;
			$scope.senhaConfirmacao = null;
			$scope.senhaNova = null;

		}

		$scope.abrirModalAtualizarSenha = function() {

			initForm();

			$rootScope.abrirModal('modalAtualizarSenha');

		};

		$scope.salvarNovaSenha = function() {

			var usuario = $rootScope.usuario || $rootScope.getUsuario();

			if($scope.formModalAtualizarSenha.$error.pwCheck) {

				return;

			}

			var parametros = 'id=' + usuario.id + '&senhaAtual=' + $scope.senhaAtual + '&senhaNova=' + $scope.senhaNova;

			usuarioService.atualizarSenha(parametros).then(
				function(response) {

					$rootScope.fecharModal('modalAtualizarSenha');

					$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

				},

				function(error) {

					$rootScope.fecharModal('modalAtualizarSenha');

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}
			);

		};

		$scope.getUsuarioAutenticado = function() {

			authService.getAuthenticatedUser().then(
				function(response) {

					$rootScope.setUsuario(response.data);

					if(response.data) {

						$scope.getAcoesPermitidas();

					}

				}
			);

		};

		if(!$rootScope.rota.public && !$rootScope.getUsuario()) {

			$scope.getUsuarioAutenticado();

		}

		if(!$rootScope.getConfig()) {

			configService.get().then(
				function(response) {

					var config = response.data;

					if(config.mode === 'DEV')
						console.log(config);

					$rootScope.setConfig(config);

				},
				function(error) {

					$rootScope.fecharModal('modalAtualizarSenha');

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}

			);

		}

		$scope.getAcoesPermitidas = function () {

			var usuario = $rootScope.getUsuario();

			if(usuario.permittedActionsIds) {

				return;

			}

			authService.getAcoesPermitidas().then(
				function(response) {

					usuario.permittedActionsIds = response.data;
					$rootScope.setUsuario(usuario);

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

				}
			);

		};

	});

})();
