(function() {

	var modulo = angular.module('appModule');

	modulo.controller('HeaderController', function($rootScope, $scope, configService, authService, $location, usuarioService, config) {

		$scope.senhaAtual = null;
		$scope.senhaConfirmacao = null;
		$scope.senhaNova = null;

		function initForm() {

			$scope.formModalAtualizarSenha.$setPristine();
			$scope.formModalAtualizarSenha.$setUntouched();
			$scope.senhaAtual = null;
			$scope.senhaConfirmacao = null;
			$scope.senhaNova = null;

		}

		$scope.abrirModalAtualizarSenha = function() {

			initForm();

			$rootScope.abrirModal('modalAtualizarSenha');

		};

		$scope.salvarNovaSenha = function() {

			var parametros = null;

			if($scope.formModalAtualizarSenha.$error.pwCheck) {

				return;

			}

			if($rootScope.getUsuario()) {

				parametros = 'id=' + $rootScope.getUsuario().id + '&senhaAtual=' + $scope.senhaAtual + '&senhaNova=' + $scope.senhaNova;

			} else if($rootScope.usuario) {

				parametros = 'id=' + $rootScope.usuario.idUsuario + '&senhaAtual=' + $scope.senhaAtual + '&senhaNova=' + $scope.senhaNova;

			}

			usuarioService.atualizarSenha(parametros).then(
				function(response) {

					$rootScope.fecharModal('modalAtualizarSenha');

					$rootScope.$broadcast('showMessageEvent', response.data.text, 'success');

				},

				function(error) {

					$scope.senhaAtualDiferente = error;

				}

			);

		};

		if(!$rootScope.getConfig()) {

			configService.get().then(function(response) {

				var config = response.data;

				if(config.mode === 'DEV')
					console.log(config);

				$rootScope.setConfig(config);

			});

		}

		$scope.atualizarCadastro = function() {

			var usuario = $rootScope.getUsuario() || $rootScope.usuario;

			if(usuario.pessoa) {

				$rootScope.isAtualizacao = true;

				if(usuario.pessoa.cpf) {

					$location.path('/pessoaFisica/cadastro/' + usuario.pessoa.id + '/edicao');

				} else {

					$location.path('/pessoaJuridica/cadastro/' + usuario.pessoa.id + '/edicao');

				}

			} else {

				if(usuario.cpf) {

					$location.path('/public/pessoaFisica/cadastro/' + usuario.id);

				} else {

					$location.path('/public/pessoaJuridica/cadastro/' + usuario.id);

				}

			}

		};

		$scope.irHome = function() {

			if($rootScope.getUsuarioPortalSeguranca()) {

				$rootScope.goToRoutePortalSeguranca('#/home');

			}

			$rootScope.goToRoutePortalSeguranca('#');

		};

	});

})();
