(function() {

	var modulo = angular.module('appModule');

	modulo.controller('ValidacaoContaController', function($rootScope, $scope, $routeParams, constantes, pessoaFisicaService, pessoaJuridicaService, $localStorage, $cookies, configService) {

		function init() {

			$scope.dadosValidacao = {};
			$scope.validacao = {};
			$scope.constantes = constantes;
			$scope.datepickerOptions = {
				maxDate: new Date()
			};

			if(loginIsCpf()) {

				verificarExistenciaPessoaFisica();

			} else {

				verificarExistenciaPessoaJuridica();

			}

		}

		function verificarExistenciaPessoaFisica() {

			pessoaFisicaService.existePessoaComCpf($scope.cpf).then(
				function(response) {

					if(response.data) {

						getInformacoesValidacaoPessoaFisica();

						$scope.pagina = constantes.PAGINA_VALIDACAO_PESSOA_FISICA;

					} else {

						$scope.pagina = constantes.PAGINA_CADASTRO_PESSOA_FISICA;

					}

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

				}

			);

		}

		function verificarExistenciaPessoaJuridica() {

			pessoaJuridicaService.existePessoaComCnpj($scope.cnpj).then(
				function(response) {

					if(response.data) {

						getInformacoesValidacaoPessoaJuridica();

						$scope.pagina = constantes.PAGINA_VALIDACAO_PESSOA_JURIDICA;

					} else {

						$scope.pagina = constantes.PAGINA_REDIRECIONAR_REDE_SIMPLES;

					}

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

				}

			);

		}

		function loginIsCpf() {

			var login = $routeParams.login;

			if(login.length <= 11) {

				$scope.cpf = login;

				return true;

			}

			$scope.cnpj = login;

			return false;

		}

		function getInformacoesValidacaoPessoaFisica() {

			$scope.informacoes = $localStorage.getObject($scope.cpf + '-data');

			if($scope.informacoes) {

				return;

			}

			pessoaFisicaService.getInformacoesValidacao($scope.cpf).then(
				function(response) {

					$scope.informacoes = response.data;

					$localStorage.setObject($scope.cpf + '-data', $scope.informacoes);

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

				}

			);

		}

		function getInformacoesValidacaoPessoaJuridica() {

			$scope.informacoes = $localStorage.getObject($scope.cnpj + '-data');

			if($scope.informacoes) {

				return;

			}

			pessoaJuridicaService.getInformacoesValidacao($scope.cnpj).then(
				function(response) {

					$scope.informacoes = response.data;

					$localStorage.setObject($scope.cnpj + '-data', $scope.informacoes);

				},
				function(error) {

					$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

				}

			);

		}

		$scope.validarDadosPessoa = function() {

			if($scope.validacao.form.$invalid) {

				$rootScope.$broadcast('showMessageEvent', 'Verifique os campos obrigatórios.' , 'danger');

				return;

			}

			if(loginIsCpf()) {

				validarDadosPessoaFisica();

			} else {

				validarDadosPessoaJuridica();

			}

		};

		function validarDadosPessoaFisica() {

			$scope.dadosValidacao.cpf = $scope.cpf;

			pessoaFisicaService.validarDados($scope.dadosValidacao).then(
				function(response) {

					$scope.pagina = constantes.PAGINA_CADASTRO_PESSOA_FISICA;

					$localStorage.remove($scope.cpf + '-data');

				},
				function(error) {

					if(error.status === 403) {

						$rootScope.setErrorMessage(error.data);

						$rootScope.goToRoutePortalSeguranca('');

					} else {

						$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

					}

				}

			);

		}

		function validarDadosPessoaJuridica() {

			$scope.dadosValidacao.cnpj = $scope.cnpj;

			pessoaJuridicaService.validarDados($scope.dadosValidacao).then(
				function(response) {

					$scope.pagina = constantes.PAGINA_CADASTRO_PESSOA_JURIDICA;

					$localStorage.remove($scope.cnpj + '-data');

				},
				function(error) {

					if(error.status === 403) {

						$rootScope.setErrorMessage(error.data);

						$rootScope.goToRoutePortalSeguranca('');

					} else {

						$rootScope.$broadcast('showMessageEvent', error.data , 'danger');

					}

				}

			);

		}

		$scope.voltar = function() {

			$rootScope.goToRoutePortalSeguranca('');

		};

		init();

	});

})();
