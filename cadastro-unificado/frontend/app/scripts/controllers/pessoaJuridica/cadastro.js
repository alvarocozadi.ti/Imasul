(function() {

	var modulo = angular.module('appModule');

	modulo.controller('CadastroPessoaJuridicaController', function($rootScope, $scope, $location, $routeParams, paisService, estadoService, tipoContatoService, moduloService, pessoaJuridicaService, constantes, configService, setorService) {

		$scope.datepickerOptions = {
			maxDate: new Date()
		};

		configService.get().then(function(response) {

			$rootScope.tipos = response.data.tipos;

			init();

		});

		var init = function() {

			var cnpjPessoa = $scope.$parent.cnpj;
			var tipos = null;

			if(cnpjPessoa || $rootScope.rota.public) {

				tipos = $rootScope.tipos;

			} else {

				tipos = $rootScope.getConfig().tipos;

			}

			$scope.constantes = constantes;

			$scope.setoresUsuario = [];

			var zonaUrbana = _.find(tipos.ZonaLocalizacao, function(zona) {
				return zona.codigo == constantes.ZONA_URBANA;
			});

			var zonaRural = _.find(tipos.ZonaLocalizacao, function(zona) {
				return zona.codigo == constantes.ZONA_RURAL;
			});

			var idCorrespondenciaCopia = null;
			var idPrincipalCopia = null;

			$scope.setTituloPagina = function(title) {

				if(title != $scope.tituloPagina) {

					$scope.tituloPagina = title;

				}

			};

			$scope.tituloPagina = '';
			$scope.linkRedeSimples = $rootScope.getLinkRedeSimples();
			$scope.cadastroPublico = false;
			$scope.editar = false;
			$scope.isUsuarioEdicao = false;
			$scope.zonasLocalizacao = tipos.ZonaLocalizacao;
			$scope.pessoaJuridica = {};
			$scope.pessoaJuridica.usuario = {};
			$scope.pessoaJuridica.usuario.perfis = [];
			$scope.pessoaJuridica.usuario.setores = [];

			$scope.enderecoPrincipal = {

				tipo: {
					id: constantes.TIPO_ENDERECO_PRINCIPAL
				},
				zonaLocalizacao: zonaUrbana,
				pais: {
					id: constantes.ID_BRASIL
				},
				semNumero: false

			};

			$scope.enderecoCorrespondencia = {

				tipo: {
					id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
				},
				pais: {
					id: constantes.ID_BRASIL
				},
				usarOutro: false,
				zonaLocalizacao: zonaUrbana,
				semNumero: false

			};

			function carregarSetores() {

				setorService.loadSetoresPublic().then(
					function (response) {

						$scope.setoresUsuario = response.data;
						trimSetores($scope.pessoaJuridica.usuario.setores);
					}
				);
			}

			function recuperarPessoaPorCnpj(cnpjPessoa) {

				if($rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_JURIDICA)) {

					pessoaJuridicaService.publicoBuscarPorCnpj(cnpjPessoa).then(

						function(response) {

							if(response.data) {

								preencherDadosRecuperados(response);
								$scope.cadastroPublico = true;
								$scope.pessoaJuridica.isUsuario = true;
								$scope.pessoaJuridica.usuario.perfis = [
									{
										id : constantes.ID_PERFIL_PUBLICO
									}
								];

							} else {

								$scope.cadastroPublico = true;
								$scope.pessoaJuridica.cnpj = cnpjPessoa;
								$scope.pessoaJuridica.isUsuario = true;
								$scope.pessoaJuridica.usuario.perfis = [
									{
										id : constantes.ID_PERFIL_PUBLICO
									}
								];

							}

							carregarSetores();

						},
						function(response) {

							$rootScope.$broadcast('showMessageEvent', response.data, 'danger');

						}

					);

				} else {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

				}

			}

			if(cnpjPessoa) {

				recuperarPessoaPorCnpj(cnpjPessoa);

			} else if($rootScope.rota.public) {

				moduloService.loadModulosComPerfisPublic().then(
					function (response) {

						$scope.modulos = response.data;

						var idPessoa = $routeParams.id;

						if(idPessoa) {

							recuperarPessoaPorId(idPessoa);

						}

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}
				);

			} else {

				moduloService.loadModulosComPerfis().then(
					function (response) {

						$scope.modulos = response.data;

						var idPessoa = $routeParams.id;

						if(idPessoa) {

							recuperarPessoa(idPessoa);

						} else {

							$scope.setTituloPagina('Cadastro de pessoa jurídica');

						}

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}
				);

			}

			function preencherDadosRecuperados(response) {

				$scope.pessoaJuridica = response.data;
				var title = 'Atualização de cadastro de pessoa jurídica';

				if($rootScope.rota.public) {

					$rootScope.usuario = {};
					$rootScope.usuario.pessoaId = $scope.pessoaJuridica.id;
					$rootScope.usuario.razaoSocial = $scope.pessoaJuridica.razaoSocial;
					$rootScope.usuario.cnpj = $scope.pessoaJuridica.cnpj;

					$rootScope.isAtualizacao = false;

				} else if($location.path().split('/').pop() == 'edicao') {

					$rootScope.usuario = {};
					$rootScope.usuario.pessoaId = $scope.pessoaJuridica.id;
					$rootScope.usuario.razaoSocial = $scope.pessoaJuridica.razaoSocial;
					$rootScope.usuario.cnpj = $scope.pessoaJuridica.cnpj;

					$rootScope.isAtualizacao = true;

				} else {

					title = 'Edição de pessoa jurídica';

				}

				$scope.setTituloPagina(title);

				if($rootScope.rota.public && $rootScope.isAtualizacao) {

					$rootScope.usuario = {};
					$rootScope.usuario.id = $scope.pessoaJuridica.id;
					$rootScope.usuario.razaoSocial = $scope.pessoaJuridica.razaoSocial;
					$rootScope.usuario.cnpj = $scope.pessoaJuridica.cnpj;
					$rootScope.usuario.idUsuario = $scope.pessoaJuridica.usuario.id;

				}

				$scope.editar = true;

				if($scope.pessoaJuridica.isUsuario && $scope.pessoaJuridica.usuario) {

					$scope.isUsuarioEdicao = true;

				} else {

					$scope.pessoaJuridica.usuario = {};
					$scope.pessoaJuridica.usuario.perfis = [];

				}

				$scope.enderecoPrincipal =  _.find($scope.pessoaJuridica.enderecos, function(endereco) {
					return endereco.tipo.id == constantes.TIPO_ENDERECO_PRINCIPAL;
				});

				$scope.enderecoCorrespondencia =  _.find($scope.pessoaJuridica.enderecos, function(endereco) {
					return endereco.tipo.id == constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				});

				tratarDadosRecuperados();

			}

			function recuperarPessoa(idPessoa) {

				pessoaJuridicaService.buscar(idPessoa).then(

					function(response) {

						preencherDadosRecuperados(response);
						carregarSetores();

					},
					function(error) {

						$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

					}

				);

			}

			function recuperarPessoaPorId(idPessoa) {

				pessoaJuridicaService.buscarPublic(idPessoa).then(

					function(response) {

						preencherDadosRecuperados(response);
						carregarSetores();
						$scope.pessoaJuridica.isUsuario = true;

					},
					function(response) {

						$rootScope.$broadcast('showMessageEvent', response.data, 'danger');

					}

				);

			}

			function tratarDadosRecuperados() {

				if($scope.enderecoPrincipal.numero === null && $scope.enderecoPrincipal.zonaLocalizacao.codigo == constantes.ZONA_URBANA) {

					$scope.enderecoPrincipal.semNumero = true;

				}

				if($scope.enderecoCorrespondencia.numero === null) {

					$scope.enderecoCorrespondencia.semNumero = true;

				}

				verifyEnderecos();

			}

			$scope.atualizarNomesHeaders = function() {

				var usuario = $rootScope.getUsuarioPortalSeguranca();

				if(!usuario) {

					return;
				}

				if($scope.pessoaJuridica.id !== usuario.pessoaId) {

					return;

				}

				usuario.nome = $scope.pessoaJuridica.razaoSocial;

				$rootScope.setUsuarioPortalSeguranca(usuario);

			};

			$scope.redirecionarRedeSimples = function() {

				$rootScope.goToRouteRedeSimples();
			};

			$scope.salvar = function() {

				if(!$rootScope.verificarPermissao($rootScope.AcaoSistema.CADASTRAR_PESSOA_JURIDICA)) {

					$rootScope.setUnauthorizedMessageAndRedirectToLogin();

				}

				if(!validarCampos())
					return;

				ajustarCampos();
				tratarUsuario();

				if($scope.editar) {

					pessoaJuridicaService.editar($scope.pessoaJuridica, $scope.cadastroPublico).then(
						function(response) {

							$scope.atualizarNomesHeaders();

							verificarCaminhoRedirectEdicao(response);

						},
						function(error) {

							$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

						}
					);

				} else {

					pessoaJuridicaService.cadastrar($scope.pessoaJuridica, $scope.cadastroPublico).then(
						function(response) {

							verificarCaminhoRedirectCadastro(response);

						},
						function(error) {

							$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

						}

					);

				}

			};

			function verificarCaminhoRedirectEdicao(response) {

				if($scope.cadastroPublico) {

					$rootScope.verifyAndSendMessageToPortal(response);
					$rootScope.goToRoutePortalSeguranca('');

				} else if($location.path().split('/').pop() == 'edicao') {

					$rootScope.rota.public = false;

					if($rootScope.getRedirect()) {

						var location = $rootScope.getRedirect();

						$rootScope.verifyAndSendMessageToPortal(response);
						$rootScope.removeRedirect();
						$rootScope.goToRoutePortalSeguranca("#" + location);

					} else {

						$rootScope.verifyAndSendMessage(response);
						$rootScope.isAtualizacao = false;
						$rootScope.removeRedirect();

						window.history.back();

					}

				} else if($rootScope.rota.public) {

					if($rootScope.getRedirect()) {

						$rootScope.removeRedirect();
						$rootScope.verifyAndSendMessageToPortal(response);
						$rootScope.goToRoutePortalSeguranca('#/home');

					}

				} else {

					$location.path('/pessoaJuridica/gestao');

					$rootScope.verifyAndSendMessage(response);

				}

			}

			function verificarCaminhoRedirectCadastro(response) {

				if($scope.cadastroPublico) {

					$rootScope.verifyAndSendMessageToPortal(response);
					$rootScope.goToRoutePortalSeguranca('');

				} else {

					$location.path('/pessoaJuridica/gestao');

					$rootScope.verifyAndSendMessage(response);

				}

			}

			function tratarUsuario() {

				if($scope.pessoaJuridica.isUsuario) {

					var enderecoPrincipal = _.find($scope.pessoaJuridica.contatos, function(contato) { return contato.principal; });
					$scope.pessoaJuridica.usuario.email = enderecoPrincipal.valor;
					$scope.pessoaJuridica.usuario.login = $scope.pessoaJuridica.cnpj;

				} else {

					$scope.pessoaJuridica.usuario = null;

				}

			}

			function ajustarCampos() {

				if($scope.enderecoPrincipal.semNumero) {

					$scope.enderecoPrincipal.numero = null;

				}

				if($scope.enderecoCorrespondencia.semNumero) {

					$scope.enderecoCorrespondencia.numero = null;

				}

				if(!$scope.enderecoCorrespondencia.usarOutro) {

					enderecoCorrespondenciaCopyPrincipal();

				}

				$scope.pessoaJuridica.enderecos = [
					$scope.enderecoPrincipal,
					$scope.enderecoCorrespondencia
				];

			}


			function validarCampos() {

				var message = "";

				if($scope.form.emailAdicional.$error.email || $scope.form.celular.$error.brPhoneNumber || $scope.form.telResidencial.$error.brPhoneNumber || $scope.form.telComercial.$error.brPhoneNumber) {

					message = 'Verifique os campos inválidos.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				} else if($scope.form.emailAdicional.$modelValue || $scope.form.celular.$modelValue || $scope.form.telResidencial.$modelValue || $scope.form.telComercial.$modelValue) {

					message = 'Adicionar o contato preenchido.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				} else if($scope.form.$invalid) {

					message = 'Verifique os campos obrigatórios.';

					$rootScope.$broadcast('showMessageEvent', message , 'danger');

				}

				if(message) {

					return false;

				}

				return true;

			}

			$scope.voltar = function() {

				if($scope.cadastroPublico) {

					$rootScope.goToRoutePortalSeguranca('');

				} else if($rootScope.rota.public) {

					$rootScope.goToRoutePortalSeguranca('#/home');

				} else if($location.path().split('/').pop() == 'edicao') {

					window.history.back();

				} else {

					$location.path('/pessoaJuridica/gestao');

				}

			};

			$scope.changeZona = function() {

				var diferente = $scope.enderecoPrincipal.diferenteCorrespondencia;
				var zonaPrincipal = $scope.enderecoPrincipal.zonaLocalizacao.codigo;

				cleanEnderecoPrincipal();

				if(zonaPrincipal == constantes.ZONA_RURAL) {

					if(!diferente) {

						cleanEnderecoCorrespondencia();

						$scope.enderecoPrincipal.diferenteCorrespondencia = false;

					} else {

						$scope.enderecoPrincipal.diferenteCorrespondencia = true;

					}

					$scope.enderecoCorrespondencia.usarOutro = true;
					$scope.enderecoPrincipal.zonaLocalizacao = zonaRural;

				} else {

					if(!diferente) {

						$scope.enderecoCorrespondencia.usarOutro = false;
						$scope.enderecoPrincipal.diferenteCorrespondencia = false;

					} else {

						$scope.enderecoCorrespondencia.usarOutro = true;
						$scope.enderecoPrincipal.diferenteCorrespondencia = true;

					}

				}

				$scope.enderecoPrincipal.uf = null;
				$scope.enderecoPrincipal.municipio = null;

			};

			function cleanEnderecoCorrespondencia() {

				$scope.enderecoCorrespondencia = {

					id: idCorrespondenciaCopia,
					tipo: {
						id: constantes.TIPO_ENDERECO_CORRESPONDENCIA
					},
					pais: {
						id: constantes.ID_BRASIL
					},
					usarOutro: true,
					zonaLocalizacao: zonaUrbana,
					semNumero: false,
					uf: null,
					municipio: null

				};

			}

			function cleanEnderecoPrincipal() {

				$scope.enderecoPrincipal = {

					id: idPrincipalCopia,
					tipo: {
						id: constantes.TIPO_ENDERECO_PRINCIPAL
					},
					pais: {
						id: constantes.ID_BRASIL
					},
					zonaLocalizacao: zonaUrbana,
					semNumero: false,
					uf: null,
					municipio: null

				};

			}

			function enderecoCorrespondenciaCopyPrincipal() {

				angular.copy($scope.enderecoPrincipal, $scope.enderecoCorrespondencia);
				$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
				$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				$scope.enderecoCorrespondencia.usarOutro = false;
				$scope.enderecoPrincipal.diferenteCorrespondencia = false;

			}

			$scope.ajustarEnderecoCorrespondencia = function() {

				if($scope.enderecoCorrespondencia.usarOutro) {

					cleanEnderecoCorrespondencia();

				} else {

					enderecoCorrespondenciaCopyPrincipal();

				}

			};

			function verifyEnderecos() {

				idPrincipalCopia = $scope.enderecoPrincipal.id;
				idCorrespondenciaCopia = $scope.enderecoCorrespondencia.id;
				$scope.enderecoPrincipal.id = null;
				$scope.enderecoPrincipal.tipo.id = null;
				$scope.enderecoCorrespondencia.id = null;
				$scope.enderecoCorrespondencia.tipo.id = null;

				if(JSON.stringify($scope.enderecoPrincipal) === JSON.stringify($scope.enderecoCorrespondencia)) {

					enderecoCorrespondenciaCopyPrincipal();

					$scope.enderecoPrincipal.diferenteCorrespondencia = false;

				} else {

					$scope.enderecoPrincipal.diferenteCorrespondencia = true;
					$scope.enderecoCorrespondencia.usarOutro = true;

				}

				$scope.enderecoPrincipal.id = idPrincipalCopia;
				$scope.enderecoPrincipal.tipo.id = constantes.TIPO_ENDERECO_PRINCIPAL;
				$scope.enderecoPrincipal.uf = $scope.enderecoPrincipal.municipio.estado;
				$scope.enderecoCorrespondencia.id = idCorrespondenciaCopia;
				$scope.enderecoCorrespondencia.tipo.id = constantes.TIPO_ENDERECO_CORRESPONDENCIA;
				$scope.enderecoCorrespondencia.uf = $scope.enderecoCorrespondencia.municipio.estado;

			}

			$scope.loadModulos = function() {

				if($scope.modulos === undefined) {

					moduloService.loadModulosComPerfis().then(
						function(response) {

							$scope.modulos = response.data;

						},
						function(error) {

							$rootScope.$broadcast('showMessageEvent', error.data, 'danger');

						}

					);

				}

			};

			$scope.carregarPerfis = function (modulo) {

				$scope.perfisUsuario = modulo.perfis;
				trimPerfis($scope.pessoaJuridica.usuario.perfis);
			};

			$scope.adicionarPerfil = function(perfil) {

				if(!perfil)
					return;

				var index = $scope.perfisUsuario.indexOf(perfil);
				$scope.perfisUsuario.splice(index, 1);

				if(!$scope.pessoaJuridica.usuario) {
					$scope.pessoaJuridica.usuario = {perfis:[]};
				}

				$scope.pessoaJuridica.usuario.perfis.push(perfil);

			};

			$scope.adicionarSetor = function(setorEscolhido) {

				if(!setorEscolhido)
					return;

				$scope.pessoaJuridica.usuario.setores.push(setorEscolhido);

				trimSetores($scope.pessoaJuridica.usuario.setores);
			};

			$scope.removerSetor = function(setorEscolhido) {

				var index = _.findIndex($scope.pessoaJuridica.usuario.setores, {
					id: setorEscolhido.id
				});
				$scope.pessoaJuridica.usuario.setores.splice(index, 1);

				$scope.setoresUsuario.push(setorEscolhido);
			};

			$scope.removerPerfil = function(perfil) {

				var index = $scope.pessoaJuridica.usuario.perfis.indexOf(perfil);
				$scope.pessoaJuridica.usuario.perfis.splice(index, 1);

				$scope.perfisUsuario.push(perfil);

			};

			$scope.uncheckIsUsuario = function() {

				$scope.pessoaJuridica.isUsuario = false;
				$scope.pessoaJuridica.usuario.email = null;
				$scope.pessoaJuridica.usuario.login = null;
				$scope.pessoaJuridica.usuario.perfis = [];

			};

			function trimPerfis(perfis) {

				perfis.forEach(function(perfil) {

					$scope.perfisUsuario.forEach(function (perfilUsuario, index) {

						if(perfilUsuario.id === perfil.id) {
							$scope.perfisUsuario.splice(index, 1);
						}
					});

				});

			}

			function trimSetores(setoresSelecionados) {

				_.each(setoresSelecionados, function(setorSelecionado) {

					var setoresAux = JSON.parse(JSON.stringify($scope.setoresUsuario));

					_.each(setoresAux, function (setor, index) {

						if (setor.id === setorSelecionado.id) {

							$scope.setoresUsuario.splice(index, 1);
						}
					});
				});
			}

		};

	});

})();
