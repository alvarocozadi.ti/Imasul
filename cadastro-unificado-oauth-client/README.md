# Autenticação OAuth Client - Entrada Unica  v2.1.7(java 1.8)

Implementação da camada Client de autenticação para a aplicação Entrada Unica - Lemaf

### Gerar nova versão da biblioteca

- Defina a nova versão a ser gerada e altere o arquivo *build.gradle*
 ```
    # Atributo que define a versão do do arquivo .jar
    version = '2.1.7'
 ```

#### Instalar o Gradle

*https://gradle.org/install/*

#### Build
```
    # Estando no diretório raiz da aplicação, digite o comando abaixo:
    gradle build

    # Após o término, o arquivo jar da biblioteca será gravado no diretório build/libs/
```

#### Publicação

- Solicite a criação de um usuário no repositório Nexus do Lemaf (http://repo.ti.lemaf.ufla.br/repository/releases/)

- Crie um novo arquivo *gradle.properties* no diretório _$HOME/.gradle/_ :

```
    # Exemplo de configuração contendo dados do usuário criado no Nexus
    lemafUser=USUARIO
    lemafPassword=SENHA

```

- Comando de publicação da nova versão:
```
    gradle publish
```


### Utilização da biblioteca ###

#### Importação no Play Framework (versão 1.x.x) ####
###### Altere o arquivo *conf/dependencies.yml* incluindo o bloco de código abaixo:
```
# se o bloco "require" existir, adicione somente a referência da biblioteca "- br.ufla.lemaf..."
require:
    - br.ufla.lemaf -> cadastro-unificado-oauth-client 2.1.7

# se o bloco "repositories" existir, adicione somente as linhas abaixo de referência ao repositório da biblioteca "- BitBucket..."
repositories:
    - BitBucket:
            type: iBiblio
            root: "http://repo.ti.lemaf.ufla.br/repository/public"
            contains:
                - br.ufla.lemaf -> *
```

###### Após atualizar o arquivo, execute o comando abaixo dentro do diretório *backend* da aplicação:
```
play deps --sync
```

#### Codificando a autenticação ####
###### Criando a instancia do objeto responsável pela autenticação com o sistema Entrada Unica:
```
    // Imports
    import main.java.br.ufla.lemaf.OAuthClientEntradaUnica;

    String clientId = "f0b693ad7b1c70b723aa7ca0c468a4ae6b6acd27bed01f1c854e83e52fa6cc74690bd4b6b9fc3bf4a594d6734798f7f0f3e2046eb76438a57b724c342428558b";
    String clientSecret = "7d6ba7cc61490dc6a7ca7f3871ff201441bd4c71aa867599c9660dbd4fcbcdf345f3a14c7d5b8fb8ebd7bfbd1a6ea81af998cdd5b4d9656282ef9163a84fc377";
    String urlPortal = "http://gt4.ti.lemaf.ufla.br/portalSeguranca";

    OAuthClientCadastroUnificado oAuthClient = new OAuthClientCadastroUnificado(clientId, clientSecret, urlPortal);

```
###### Exemplo de requisição GET aos serviços do Entrada Unica:
```
    ... // código anterior onde a instancia do objeto de autenticação foi criada

    String urlServico = "teste/nomeservico"
    ClasseResposta clResposta = oAuthClient.executeRequestGet(urlServico, ClasseResposta.class);
```
###### Exemplo de requisição POST aos serviços do Entrada Unica:
```
    ... // código anterior onde a instancia do objeto de autenticação foi criada

    String urlServico = "teste/nomeservico"

    Map<String, String> parametros = new HashMap<String, String>();
    parametros.put("parametro1", "teste-comunicacao");

    ClasseResposta clResposta = oAuthClient.executeRequestPost(urlPortal, parametros, ClasseResposta.class);
```
