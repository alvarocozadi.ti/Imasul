package br.ufla.lemaf.httpClient;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.ListenableFuture;
import com.ning.http.client.Response;
import com.ning.http.client.multipart.FilePart;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class MGAHttpClient extends AsyncHttpClient {

    private String url;
    private BoundRequestBuilder requestBuilder;

    public enum TPRequest {
        GET,
        POST,
        PUT
    }

    public MGAHttpClient(TPRequest type, String url, int timeout) {

        super(new AsyncHttpClientConfig.Builder()
                    .setConnectTimeout(timeout)
                    .setReadTimeout(timeout)
                    .setRequestTimeout(timeout)
                    .build());
    	

        if(type.equals(TPRequest.GET)) {

            this.requestBuilder = this.prepareGet(url);
        }
        else if(type.equals(TPRequest.POST)) {

            this.requestBuilder = this.preparePost(url);
        }
        else if(type.equals(TPRequest.PUT)) {

            this.requestBuilder = this.preparePut(url);
        }
        
        this.requestBuilder.setBodyEncoding("UTF-8");
    }

    public void addHeader(String name, String value) {

        this.requestBuilder.addHeader(name, value);
    }

    public void addParam(String name, String value) {

        this.requestBuilder.addFormParam(name, value);
    }

    public void addFile(String name, File file) {

        this.requestBuilder.addBodyPart(new FilePart(name, file));
    }

    public void addJsonStr(String json) {

        this.requestBuilder.setBody(json);
    }

    public WSHttpResponse execute() throws ExecutionException, InterruptedException, IOException {

        ListenableFuture<Response> resp = this.executeRequest(this.requestBuilder.build());

        Response response = resp.get();

        return new WSHttpResponse(response.getStatusCode(), response.getResponseBody("UTF-8"));
    }

    protected void finalize() throws Throwable {

        System.out.print("MGAHttpClient::finalize()");

        this.requestBuilder = null;
        super.finalize();
    }
}
