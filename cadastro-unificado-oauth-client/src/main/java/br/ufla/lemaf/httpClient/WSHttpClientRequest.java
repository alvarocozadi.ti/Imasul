package br.ufla.lemaf.httpClient;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class WSHttpClientRequest {

    private MGAHttpClient httpClient;

    public WSHttpClientRequest(MGAHttpClient.TPRequest tipo, String url, int timeout) {

        this.httpClient = new MGAHttpClient(tipo, url, timeout);
    }

    public void addHeader(String name, String value) {

        this.httpClient.addHeader(name, value);
    }

    public void addParam(String name, String value) {

        this.httpClient.addParam(name, value);
    }

    public void addFile(String name, File file) {

        this.httpClient.addFile(name, file);
    }

    public void addJsonStr(String json) {

        this.httpClient.addJsonStr(json);
    }

    public WSHttpResponse execute() throws InterruptedException, ExecutionException, IOException {

        return this.httpClient.execute();
    }

    protected void finalize() throws Throwable {

        System.out.print("WSHttpClientRequest::finalize()");

        this.httpClient.close();

        this.httpClient.finalize();

        this.httpClient = null;

        super.finalize();
    }
}
