package br.ufla.lemaf.httpClient;

public class WSHttpResponse {

    private int status;
    private String body;

    public WSHttpResponse(int status, String body) {

        this.status = status;
        this.body = body;
    }

    public void setStatus(int value) {
        this.status = value;
    }

    public void setBody(String value) {
        this.body = value;
    }

    public int getStatus() {
        return this.status;
    }

    public String getBody() {
        return this.body;
    }
}
