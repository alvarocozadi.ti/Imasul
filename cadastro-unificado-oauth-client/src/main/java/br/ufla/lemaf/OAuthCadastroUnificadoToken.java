package br.ufla.lemaf;

import java.io.Serializable;

/**
 * Created by marcio on 26/01/18.
 */
public class OAuthCadastroUnificadoToken implements Serializable {

    public String access_token;
    public String expires_in;
    public String token_type;
    public long timeExpireInMs;
    public long tick;

}

