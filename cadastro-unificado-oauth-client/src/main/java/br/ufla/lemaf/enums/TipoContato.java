package br.ufla.lemaf.enums;

public class TipoContato {

    public static final Integer ID_EMAIL = 1;
    public static final Integer ID_TELEFONE_RESIDENCIAL = 2;
    public static final Integer ID_TELEFONE_COMERCIAL = 3;
    public static final Integer ID_TELEFONE_CELULAR = 4;

}
