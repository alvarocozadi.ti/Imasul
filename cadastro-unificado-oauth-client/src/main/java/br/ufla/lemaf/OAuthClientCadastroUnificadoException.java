package br.ufla.lemaf;

/**
 * Created by marcio on 26/01/18.
 */
public class OAuthClientCadastroUnificadoException extends RuntimeException {

    public enum OAuthClientExceptionCode {
        INTERNAL_ERROR,
        ERROR_GET_TOKEN,
        ERROR_REQUEST_TOKEN_NOT_EXIST,
        ERROR_REQUEST_GET,
        ERROR_REQUEST_POST,
        ERROR_REQUEST_PUT
    }

    public int status;
    public OAuthClientExceptionCode code;

    public OAuthClientCadastroUnificadoException(OAuthClientExceptionCode code, int status, String message) {
        super(message);

        this.code = code;
        this.status = status;
    }
}
