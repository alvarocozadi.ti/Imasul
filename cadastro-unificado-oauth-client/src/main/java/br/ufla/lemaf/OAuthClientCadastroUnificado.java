package br.ufla.lemaf;

import br.ufla.lemaf.beans.pessoa.Setor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ning.http.util.Base64;
import br.ufla.lemaf.beans.pessoa.Usuario;
import br.ufla.lemaf.deserializers.DateDeserializer;
import br.ufla.lemaf.httpClient.MGAHttpClient;
import br.ufla.lemaf.httpClient.WSHttpClientRequest;
import br.ufla.lemaf.httpClient.WSHttpResponse;
import br.ufla.lemaf.serializers.DateSerializer;
import br.ufla.lemaf.utils.Time;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class OAuthClientCadastroUnificado {

    private String clientId;
    private String clientSecret;
    private String authorizationRequest;
    private String urlPortal;
    private OAuthCadastroUnificadoToken token;

    private static final String URL_SERVICE_GET_TOKEN = "/external/token";
    private static final String URL_SERVICE_GET_LOGIN = "/external/login";
    private static final String URL_SERVICE_GET_IS_LOGGED = "/external/session/estaLogado";
    private static final String URL_USUARIO_POSSUI_LOGIN = "/public/temUsuarioComLogin/";
    private static final String URL_SEARCH_USER_BY_SESSION_KEY = "/external/usuario/buscaPorSessionKey";
    private static final String URL_ALLOWED_ACCESS_SERVICE = "/external/modulo/allowed/access";
    private static final String URL_BUSCAR_USUARIO_BY_PERFIL = "/external/usuario/perfil/";
    private static final String URL_BUSCAR_SETOR_BY_NIVEL = "/external/setor/nivel/";
    private static final String URL_BUSCAR_SETOR_BY_SIGLA = "/external/setor/sigla/";
    private static final String URL_VERIFICAR_USUARIO_BY_LOGIN = "/external/usuario/verificar/";
    private static final String URL_CRIAR_USUARIO = "/external/usuario";
    private static final String URL_EMAIL_RECUPERAR_SENHA = "/usuario/emailRedefinirSenha/";
	private static final String URL_BUSCAR_USUARIO_LOGIN = "/external/usuario/buscarPorLogin/";


    private static final String CONTENT_TYPE_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String GRANT_TYPE = "client_credentials";

    public static final Gson gson;
    static {

        gson = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .registerTypeAdapter(Date.class, new DateSerializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

    }

    /**
     * Construtor
     * @param clientId
     * @param clientSecret
     * @param urlPortal
     */
    public OAuthClientCadastroUnificado(String clientId, String clientSecret, String urlPortal) {

        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.urlPortal = urlPortal;

        this.generateToken();
    }

    /**
     * Gera o token de comunicação
     */
    private void generateToken() {

        long nowInMs = new Date().getTime();

        if(this.token == null || (nowInMs > this.token.timeExpireInMs)) {

            this.token = this.requestToken();
            this.token.tick = new Date().getTime();
            this.token.timeExpireInMs = this.token.tick + Time.parseDuration(this.token.expires_in) * 1000;
            this.authorizationRequest = this.token.token_type + " " + this.token.access_token;
        }
    }

    /**
     * Requisição do tipo GET para qualquer serviço utilizando segurança por token
     * @param urlService
     * @param classResult
     * @param <T>
     * @return
     */
    public <T> T executeRequestGet(String urlService, Class<T> classResult) {

        if(this.token == null) {

            throw new OAuthClientCadastroUnificadoException(OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_TOKEN_NOT_EXIST, -1, "token inexistente");
        }

        this.generateToken();

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.GET, urlService, 60000);

        request.addHeader("authorization", this.authorizationRequest);
        request.addHeader("Content-Type", CONTENT_TYPE_X_WWW_FORM_URLENCODED);

        return getResponseFromRequest(request, classResult, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_GET, "");
    }

    /**
     * Requisição do tipo POST para qualquer serviço utilizando segurança por token
     * @param urlService
     * @param parameters
     * @param classResult
     * @param <T>
     * @return
     */
    public <T> T executeRequestPost(String urlService, Map<String, String> parameters, Class<T> classResult) {

        if(this.token == null) {

            throw new OAuthClientCadastroUnificadoException(OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_TOKEN_NOT_EXIST, -1, "token inexistente");
        }

        this.generateToken();

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.POST, urlService, 60000);

        request.addHeader("authorization", this.authorizationRequest);
        request.addHeader("Content-Type", CONTENT_TYPE_X_WWW_FORM_URLENCODED);

        if(parameters != null) {

            for(String key : parameters.keySet()) {

                request.addParam(key, parameters.get(key));
            }
        }

        return getResponseFromRequest(request, classResult, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_POST, "");
    }

    /**
     * Requisição do tipo POST para qualquer serviço utilizando segurança por token
     * @param urlService
     * @param json Json do objeto
     * @param classResult
     * @param <T>
     * @return
     */
    public <T> T executeRequestPostJson(String urlService, String json, Class<T> classResult) {

        if(this.token == null) {

            throw new OAuthClientCadastroUnificadoException(OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_TOKEN_NOT_EXIST, -1, "token inexistente");
        }

        this.generateToken();

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.POST, urlService, 60000);

        request.addHeader("authorization", this.authorizationRequest);
        request.addHeader("Content-Type", CONTENT_TYPE_JSON);

        request.addJsonStr(json);

        return getResponseFromRequest(request, classResult, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_POST, "");
    }

    /**
     * Requisição do tipo PUT para qualquer serviço utilizando segurança por token
     * @param urlService
     * @param json Json do objeto
     * @param classResult
     * @param <T>
     * @return
     */
    public <T> T executeRequestPutJson(String urlService, String json, Class<T> classResult) {

        if(this.token == null) {

            throw new OAuthClientCadastroUnificadoException(OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_TOKEN_NOT_EXIST, -1, "token inexistente");
        }

        this.generateToken();

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.PUT, urlService, 60000);

        request.addHeader("authorization", this.authorizationRequest);
        request.addHeader("Content-Type", CONTENT_TYPE_JSON);

        request.addJsonStr(json);

        return getResponseFromRequest(request, classResult, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_REQUEST_PUT, "");
    }

    /**
     * Token da comunicação OAuth
     * @return
     */
    public OAuthCadastroUnificadoToken getToken() {
        return this.token;
    }

    /**
     * Obtem o token de autorização
     * @return
     */
    private OAuthCadastroUnificadoToken requestToken() {

        String url = this.urlPortal + URL_SERVICE_GET_TOKEN;

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.POST, url, 60000);

        String strEncode = this.clientId + ":" + this.clientSecret;

        String authorizationGetToken = "Basic " + new String(Base64.encode(strEncode.getBytes()));

        request.addHeader("authorization", authorizationGetToken);
        request.addHeader("Content-Type", CONTENT_TYPE_X_WWW_FORM_URLENCODED);
        request.addParam("grant_type", GRANT_TYPE);

        return getResponseFromRequest(request, OAuthCadastroUnificadoToken.class, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_GET_TOKEN, "Erro ao tentar obter o token");
    }

    /**
     * Obtem o token de autorização para um módulo a partir da chave
     * @return
     */
    private OAuthCadastroUnificadoToken requestTokenByKey(String key) {

        String url = this.urlPortal + URL_SERVICE_GET_TOKEN;

        WSHttpClientRequest request = new WSHttpClientRequest(MGAHttpClient.TPRequest.POST, url, 60000);

        request.addHeader("authorization", key);
        request.addHeader("Content-Type", CONTENT_TYPE_X_WWW_FORM_URLENCODED);
        request.addParam("grant_type", GRANT_TYPE);

        return getResponseFromRequest(request, OAuthCadastroUnificadoToken.class, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode.ERROR_GET_TOKEN, "Erro ao tentar obter o token");
    }


    /**
     * Retorna a resposta da requisição genérica
     * @param request
     * @param classResult
     * @param errorCode
     * @param <T>
     * @return
     */
    private <T> T getResponseFromRequest(WSHttpClientRequest request, Class<T> classResult, OAuthClientCadastroUnificadoException.OAuthClientExceptionCode errorCode, String messageError) {

        WSHttpResponse response = null;

        try {

            response = request.execute();

            if (response.getStatus() == 200) {

                return gson.fromJson(response.getBody(), classResult);

            } else {

                System.err.println("Error -> Status: " + response.getStatus() + " Code: " + errorCode + " Message: " + response.getBody() + " " + messageError);

                throw new OAuthClientCadastroUnificadoException(errorCode, response.getStatus(), response.getBody() + " " + messageError);
            }

        } catch (InterruptedException e) {

            e.printStackTrace();

        } catch (ExecutionException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }

        return null;
    }

    /**
     * Retorna se o usuário está logado no Cadastro Unificado
     * @return
     */
    public boolean isLogged(String userSessionKey) {

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("sessionKey", userSessionKey);

        return this.executeRequestPost(this.urlPortal + URL_SERVICE_GET_IS_LOGGED, parametros, Boolean.class);
    }

    /**
     * Login no Cadastro Unificado
     * @param usuario
     * @param senha
     * @return
     */
    public Usuario login(String usuario, String senha) {

        Map<String, String> parametros = new HashMap<String, String>();
        parametros.put("username", usuario);
        parametros.put("password", senha);

        return executeRequestPost(this.urlPortal + URL_SERVICE_GET_LOGIN, parametros, Usuario.class);
    }

    /**
     * Verifica se pessoa possui usuario ativo no E.U
     * @param cpfCnpj
     * @return
     */
    public Boolean isUser(String cpfCnpj) {

        return this.executeRequestGet(this.urlPortal + URL_USUARIO_POSSUI_LOGIN + cpfCnpj, Boolean.class);
    }

    /**
     * Busca  usuario pela session key, para realizar login diretamente pelo E.U.
     * */
    public Usuario searchBySessionKey(String sessionKey) {

        Map<String, String> parametros = new HashMap<String, String>();

        parametros.put("sessionKeyEntradaUnica", sessionKey);

        return executeRequestPost(this.urlPortal + URL_SEARCH_USER_BY_SESSION_KEY, parametros, Usuario.class);
    }

    protected void finalize() throws Throwable {

        System.out.print("OAuthClientCadastroUnificado::finalize()");

        super.finalize();
    }

    /**
     * Retorna se o acesso está liberado para o servico do módulo informado
     * @param userSession - chave da sessão do usuário logado no Gestão de Acesso (cliente)
     * @param service - serviço a ser consultado
     * @param address - endereço IP do solicitante
     * @return
     */
    public boolean isAllowedAccess(String userSession, String service, String address) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("userSession", userSession);
        params.put("service", service);
        params.put("address", address);

        return this.executeRequestPost(this.urlPortal + URL_ALLOWED_ACCESS_SERVICE, params, Boolean.class);
    }

    /**
     * Retorna se o acesso está liberado para o serviço do módulo informado
     * @param moduleKey - chave fixa (Basic client_id + client_secret)
     * @param service - serviço a ser consultado
     * @param address - endereço IP do solicitante
     * @return
     */
    public boolean isAllowedAccessByModule(String moduleKey, String service, String address) {

        OAuthCadastroUnificadoToken tokenModule = this.requestTokenByKey(moduleKey);

        String authorizationModule = tokenModule.token_type + " " + tokenModule.access_token;

        return isAllowedAccess(authorizationModule, service, address);
    }

    /**
     * Busca os usuários por perfil
     * @param  codigoPerfil
     * @return Usuario[]
     */
    public Usuario[] findUsuariosByPerfil(String codigoPerfil) {

        return (Usuario[])this.executeRequestGet(this.urlPortal + URL_BUSCAR_USUARIO_BY_PERFIL + codigoPerfil, Usuario[].class);
    }

    /**
     * Busca os usuários por perfil e setor
     * @param  codigoPerfil
     * @param  siglaSetor
     * @return Usuario[]
     */
    public Usuario[] findUsuariosByPerfilAndSetores(String codigoPerfil, String siglaSetor) {

        return (Usuario[])this.executeRequestGet(this.urlPortal + URL_BUSCAR_USUARIO_BY_PERFIL + codigoPerfil + "/" + siglaSetor, Usuario[].class);
    }

    /**
     * Busca a sigla dos setores por nível
     * @param  siglaSetor
     * @param  nivel
     * @return String[]
     */
    public String[] getSiglaSetoresByNivel(String siglaSetor, int nivel) {

        return (String[])this.executeRequestGet(this.urlPortal + URL_BUSCAR_SETOR_BY_NIVEL + siglaSetor + "/" + nivel, String[].class);
    }

    /**
     * Busca setor por sigla
     * @param  siglaSetor
     * @return Setor
     */
    public Setor getSetorBySigla(String siglaSetor) {

        return this.executeRequestGet(this.urlPortal + URL_BUSCAR_SETOR_BY_SIGLA  + siglaSetor, Setor.class);
    }

    /**
     * Verifica se existe um usuário cadastrado com o login informado
     * @param  login
     * @return Message
     */
    public Boolean verificarUsuarioByLogin(String login) {

        return this.executeRequestGet(this.urlPortal + URL_VERIFICAR_USUARIO_BY_LOGIN  + login, Boolean.class);
    }

    /**
     * Cadastra um novo usuário
     * @param  usuario
     * @return Message
     */
    public br.ufla.lemaf.beans.Message cadastrarUsuario(Usuario usuario) {

        String json = gson.toJson(usuario);

        return (br.ufla.lemaf.beans.Message)this.executeRequestPostJson(this.urlPortal + URL_CRIAR_USUARIO, json, br.ufla.lemaf.beans.Message.class);
    }

    /**
     * Envia email para redefinir a senha do usuário
     * @param  login
     * @return Message
     */
    public br.ufla.lemaf.beans.Message emailResetSenha(String login) {

        return this.executeRequestGet(this.urlPortal + URL_EMAIL_RECUPERAR_SENHA + login, br.ufla.lemaf.beans.Message.class);
    }

	/**
	 * Retorna usuario a partir do login
	 * @param  login
	 * @return Usuario
	 */
	public Usuario buscarUsuarioPorLogin(String login) {

		return this.executeRequestGet(this.urlPortal + URL_BUSCAR_USUARIO_LOGIN + login, Usuario.class);
	}

    public Usuario[] findUsuariosBySiglaModulo(String siglaModulo) {

        return (Usuario[])this.executeRequestGet(this.urlPortal + "/external/usuario/" + siglaModulo, Usuario[].class);

    }


}
