package br.ufla.lemaf.services;

import br.ufla.lemaf.OAuthClientCadastroUnificado;
import br.ufla.lemaf.OAuthClientCadastroUnificadoException;
import br.ufla.lemaf.beans.*;
import br.ufla.lemaf.beans.historico.EmpreendimentoHistorico;
import br.ufla.lemaf.beans.historico.EmpreendimentoSobreposicao;
import br.ufla.lemaf.beans.historico.HistoricoEmpreendimento;
import br.ufla.lemaf.beans.pessoa.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by marcio on 29/01/18.
 */
public class CadastroUnificadoPessoaService extends OAuthClientCadastroUnificado {

    //URLS EXTERNALS
    private static String URL_BUSCAR_PESSOA_FISICA = "/external/pessoaFisica/buscarPorCpf/";
    private static String URL_BUSCAR_PESSOA_JURIDICA = "/external/pessoaJuridica/buscarPorCnpj/";
    private static String URL_CADASTRAR_EMPREENDIMENTO_PESSOA = "/external/empreendimento";
    private static String URL_BUSCAR_EMPREENDIMENTOS_PESSOA = "/external/empreendimentos/";
    private static String URL_BUSCAR_EMPREENDIMENTOS_COM_FILTRO = "/external/empreendimentos";
    private static String URL_BUSCAR_EMPREENDEDORES_COM_FILTRO = "/external/empreendedores";
    private static String URL_BUSCAR_PESSOA_COM_FILTRO = "/external/pessoas";
    private static String URL_EDITAR_PESSOA_FISICA = "/external/pessoaFisica";
    private static String URL_EDITAR_PESSOA_JURIDICA = "/external/pessoaJuridica";
    private static String URL_CRIAR_PESSOA_JURIDICA = "/external/pessoaJuridica";
    private static String URL_CRIAR_PESSOA_FISICA = "/external/pessoaFisica";
    private static String URL_BURCAR_PESSOA_COM_FILTRO_TODOS_MODULOS = "/external/pessoas/todosModulos";
    private static String URL_BURCAR_PESSOA_COM_FILTRO_TODOS_MODULOS_NOVO = "/external/pessoas/todosModulosNovo";
    private static String URL_ATUALIZAR_PERFIL= "/external/pessoas/atualizaPerfil";
    private static String URL_ADICIONAR_PERFIL= "/external/pessoas/adicionaPerfil";
    private static String URL_REMOVER_PERFIL= "/external/pessoas/removePerfil";
    private static String URL_BUSCA_USUARIO_SESSION_KEY = "/external/usuario/buscaPorSessionKey";
    private static String URL_VINCULAR_GESTAO_EMPREENDIMENTOS = "/external/usuario/vincularGestaoEmpreendimentos";
    private static String URL_ENVIAR_DOCUMENTO_REDE_SIMPLES  = "/external/empreendimento/confirmaRespostaOrgao";
    private static String URL_BUSCAR_HISTORICO_EMPREENDIMENTO_POR_CPF_CNPJ  = "/external/empreendimento/historico/";
    private static String URL_BUSCAR_EMPREENDIMENTO_HISTORICO_POR_ID  = "/external/empreendimento/historico/id/";
    private static String URL_BUSCAR_EMPREENDIMENTO_SOBREPOSICAO_POR_CPFCNPJ  = "/external/empreendimento/intersects/";
    private static String URL_BUSCAR_EMPREENDIMENTO_SOBREPOSICAO_POR_ID = "/external/empreendimento/intersects/id/";
    private static String URL_BUSCAR_EMPREENDEDOR = "/external/empreendedor/";
    private static String URL_INATIVAR_EMPREENDEDOR = "/external/empreendedor/inativar/";
    private static String URL_BUSCAR_EMPREENDIMENTO_ID = "/external/buscarEmpreendimento";
    private static String URL_BUSCAR_EMPREENDIMENTO_EMPREEENDEDOR_ID = "/external/empreendimentos/empreendedor";
    private static String URL_BUSCAR_EMPREENEDORES_CADASTRANTE = "/external/getEmpreendedores/cadastrante";
    private static String URL_ATUALIZAR_PESSOA_JURIDICA_REDESIM = "/external/pessoaJuridica/atualizado";

    //URLS PUBLICAS
    private static String URL_TEM_PESSOA_CNPJ = "/public/pessoaJuridica/temPessoaComCnpj/";
    private static String URL_TEM_PESSOA_CPF = "/public/pessoaFisica/temPessoaComCpf/";
    private static String URL_PUBLIC_PAISES = "/public/paises";
    private static String URL_PUBLIC_PAIS_ESTADOS = "/public/pais/{id}/estados";
    private static String URL_PUBLIC_ESTADO_MUNICIPIOS = "/public/estado/{id}/municipios";
    private static String URL_PUBLIC_TIPOS_CONTATO= "/public/tiposContato";
    private static String URL_PUBLIC_CONFIG = "/public/config";
    private static String URL_USUARIO_POSSUI_LOGIN = "/public/temUsuarioComLogin/";
    private static String URL_PUBLIC_PERFIS = "/public/perfis ";
    private static String URL_PUBLIC_MOTIVOS = "/public/motivos ";
    private static String URL_PUBLIC_PERFIL_CODIGO = "/public/perfil/{codigo} ";
    private static String URL_PUBLIC_VERIFICA_CADASTRO_SIRIEMA = "/public/pessoasSiriema/verificaCadastro/{cpfCnpj}";

    private String urlCadastro;

    /**
     * Construtor
     *
     * @param clientId
     * @param clientSecret
     * @param urlPortal
     * @param urlCadastro
     */
    public CadastroUnificadoPessoaService(String clientId, String clientSecret, String urlPortal, String urlCadastro) {
        super(clientId, clientSecret, urlPortal);

        this.urlCadastro = urlCadastro;
    }

    /**
     * Busca os dados da pessoa física dado o CPF como parâmetro
     * @param cpf
     * @return
     */
    public Pessoa buscarPessoaFisicaPeloCpf(String cpf) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_PESSOA_FISICA + cpf, Pessoa.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Verifica se a pessoa esta cadastrada na base do Siriema (retornando true ou false)
     * @param cpfCnpj
     * @return
    */
    public Boolean verificaPessoaCadastradaSiriema(String cpfCnpj) {

        try{
            return this.executeRequestGet(this.urlCadastro + URL_PUBLIC_VERIFICA_CADASTRO_SIRIEMA + cpfCnpj, Boolean.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os dados da pessoa Jurídica dado o Cnpj como parâmetro
     * @param cnpj
     * @return
     */
    public Pessoa buscarPessoaJuridicaPeloCnpj(String cnpj) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_PESSOA_JURIDICA + cnpj, Pessoa.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }


    /**
     * Cadastra um novo empreendimento (Verificar regras e atributos obrigatórios no documento de integração)
     * @param empreendimento
     * @return
     */
    public Empreendimento cadastrarEmpreendimentoPessoa(Empreendimento empreendimento) {

        try {

            String json = gson.toJson(empreendimento);

            return this.executeRequestPostJson(this.urlCadastro + URL_CADASTRAR_EMPREENDIMENTO_PESSOA, json, Empreendimento.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

//            return new Message("Error -> Status: " + e.status + " Code: " + e.code + " Message: " + e.getMessage());
            return null;
        }
    }
    /**
     * Edita um novo empreendimento (Verificar regras e atributos obrigatórios no documento de integração)
     * @param empreendimento
     * @return Message
     */
    public Message editarEmpreendimentoPessoa(Empreendimento empreendimento) {

        try {

            String json = gson.toJson(empreendimento);

            return this.executeRequestPutJson(this.urlCadastro + URL_CADASTRAR_EMPREENDIMENTO_PESSOA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return new Message("Error -> Status: " + e.status + " Code: " + e.code + " Message: " + e.getMessage());
        }
    }


    /**
     * Edita um novo empreendimento (Verificar regras e atributos obrigatórios no documento de integração)
     * @param empreendimento
     * @return Message
     */
    public Empreendimento editarEmpreendimentoPessoa(Empreendimento empreendimento, String login) {

        try {

            String json = gson.toJson(empreendimento);

            return this.executeRequestPutJson(this.urlCadastro + URL_CADASTRAR_EMPREENDIMENTO_PESSOA + "/" + login, json, Empreendimento.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Retorna os empreendimentos vinculados à Pessoa informada através do Cpf/Cnpj
     * passado por parâmetro.
     * @param cpfCnpj
     * @return
     */
    public EmpreendimentosPessoa buscarEmpreendimentosPessoa(String cpfCnpj) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTOS_PESSOA + cpfCnpj, EmpreendimentosPessoa.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Retorna os Empreendimentos cadastrados com o filtro informado
     * @param filtro
     * @return
     */
    public EmpreendimentoFiltroResult buscarEmpreendimentosComFiltro(FiltroEmpreendimento filtro) {

        try {

            String json = gson.toJson(filtro);

            return this.executeRequestPostJson(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTOS_COM_FILTRO, json, EmpreendimentoFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Retorna os Empreendedores cadastrados com o filtro informado
     * @param filtro
     * @return
     */
    public EmpreendedorFiltroResult buscarEmpreendedoresComFiltro(FiltroEmpreendedor filtro) {

        try {

            String json = gson.toJson(filtro);

            return this.executeRequestPostJson(this.urlCadastro + URL_BUSCAR_EMPREENDEDORES_COM_FILTRO, json, EmpreendedorFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Retorna as Pessoas cadastradas com o filtro informado
     * @param filtro
     * @return
     */
    public PessoaFiltroResult buscarPessoasComFiltro(FiltroPessoa filtro) {

        try {

            String json = gson.toJson(filtro);

            return this.executeRequestPostJson(this.urlCadastro + URL_BUSCAR_PESSOA_COM_FILTRO, json, PessoaFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }
    
    /**
     * Retorna as Pessoas sendo ou não usuários cadastradas com o filtro informado de todos os modulos
     * @param filtro
     * @return
     */
    public PessoaFiltroResult buscarPessoasComFiltroAll(FiltroPessoa filtro) {

        try {

            String json = gson.toJson(filtro);

            return this.executeRequestPostJson(this.urlCadastro + URL_BURCAR_PESSOA_COM_FILTRO_TODOS_MODULOS, json, PessoaFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Metodo novo
     * Retorna as Pessoas sendo ou não usuários cadastradas com o filtro informado de todos os modulos - SEM os dados de usuário
     * @param filtro
     * @return
     */
    public PessoaFiltroResult buscarPessoasComFiltroAllNovo(FiltroPessoa filtro) {

        try {

            String json = gson.toJson(filtro);

            return this.executeRequestPostJson(this.urlCadastro + URL_BURCAR_PESSOA_COM_FILTRO_TODOS_MODULOS_NOVO, json, PessoaFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Insere/Remove perfil informado correspondente ao codigo do modulo requisitante, caso o cpf/cnpj seja de um usuário.
     * @param cpfCnpj
     * @param codigoPerfil
     * @return
     */
    public Message atualizarPerfilPeloCpfCnpj(String cpfCnpj, String codigoPerfil) {

        try {


            Map<String, String> params = new HashMap<String, String>();
            params.put("cpfCnpj", cpfCnpj);
            params.put("codigoPerfil", codigoPerfil);

            Message message = this.executeRequestPost(this.urlCadastro + URL_ATUALIZAR_PERFIL, params, Message.class);

            return message;
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Insere perfil informado correspondente ao codigo do modulo requisitante, caso o cpf/cnpj seja de um usuário e ainda não tenha tal perfil.
     * @param cpfCnpj
     * @param codigoPerfil
     * @return
     */
    public Message adicionarPerfilPeloCpfCnpj(String cpfCnpj, String codigoPerfil) {

        try {


            Map<String, String> params = new HashMap<String, String>();
            params.put("cpfCnpj", cpfCnpj);
            params.put("codigoPerfil", codigoPerfil);

            Message message = this.executeRequestPost(this.urlCadastro + URL_ADICIONAR_PERFIL, params, Message.class);

            return message;
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Remove perfil informado correspondente ao codigo do modulo requisitante, caso o cpf/cnpj seja de um usuário e tenha tal perfil.
     * @param cpfCnpj
     * @param codigoPerfil
     * @return
     */
    public Message removerPerfilPeloCpfCnpj(String cpfCnpj, String codigoPerfil) {

        try {


            Map<String, String> params = new HashMap<String, String>();
            params.put("cpfCnpj", cpfCnpj);
            params.put("codigoPerfil", codigoPerfil);

            Message message = this.executeRequestPost(this.urlCadastro + URL_REMOVER_PERFIL, params, Message.class);

            return message;
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Altera os dados da Pessoa Fisica
     * @param pessoa
     * @return
     */
    public Message alterarDadosPessoaFisica(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPutJson(this.urlCadastro + URL_EDITAR_PESSOA_FISICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Altera os dados da Pessoa Juridica
     * @param pessoa
     * @return
     */
    public Message alterarDadosPessoaJuridica(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPutJson(this.urlCadastro + URL_EDITAR_PESSOA_JURIDICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }
    
    /**
     * Cadastra Pessoa Juridica
     * @param pessoa
     * @return
     */
    public Message cadastrarPessoaJuridica(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPostJson(this.urlCadastro + URL_CRIAR_PESSOA_JURIDICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Cadastra Pessoa JuridicaSC
     * @param pessoa
     * @return
     */
    public Message cadastrarPessoaJuridicaSC(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPostJson(this.urlCadastro + URL_CRIAR_PESSOA_JURIDICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return new Message(Integer.toString(e.status));
        }
    }
    
    /**
     * Cadastra Pessoa Juridica
     * @param pessoa
     * @return
     */
    public Message cadastrarPessoaFisica(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPostJson(this.urlCadastro + URL_CRIAR_PESSOA_FISICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Cadastra Pessoa Física SC
     * @param pessoa
     * @return
     */
    public Message cadastrarPessoaFisicaSC(Pessoa pessoa) {

        try {

            String json = gson.toJson(pessoa);

            return this.executeRequestPostJson(this.urlCadastro + URL_CRIAR_PESSOA_FISICA, json, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return new Message(Integer.toString(e.status));
        }
    }
    
    /**
     * Existe Pessoa Fisica/Juridica pelo CPF/CNPJ
     * @param cpfCnpj
     * @return Boolean
     */
    public Boolean temPessoaComCpfCnpj(String cpfCnpj) {

        try {

        	if(cpfCnpj.length() > 11) {
        		
        		return this.executeRequestGet(this.urlCadastro + URL_TEM_PESSOA_CNPJ + cpfCnpj, Boolean.class);
        	} else {
        		
        		return this.executeRequestGet(this.urlCadastro + URL_TEM_PESSOA_CPF + cpfCnpj, Boolean.class);
        	}
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os paises no Cadastro Unificado
     * @return Pais[]
     */
    public Pais[] buscarPaises() {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_PUBLIC_PAISES, Pais[].class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os estados no Cadastro Unificado
     * @param idPais
     * @return Estado[]
     */
    public Estado[] buscarEstados(Integer idPais) {

        try {
            String paisEstado = URL_PUBLIC_PAIS_ESTADOS.replace("{id}", idPais.toString());
            return this.executeRequestGet(this.urlCadastro + paisEstado, Estado[].class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os municípios no Cadastro Unificado
     * @param idEstado
     * @return Municipio[]
     */
    public Municipio[] buscarMunicipio(Integer idEstado) {

        try {
            String paisEstado = URL_PUBLIC_ESTADO_MUNICIPIOS.replace("{id}", idEstado.toString());
            return this.executeRequestGet(this.urlCadastro + paisEstado, Municipio[].class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os tipos de contato no Cadastro Unificado
     * @return TipoContato[]
     */
    public TipoContato[] buscarTipoContato() {

        try {
            return this.executeRequestGet(this.urlCadastro + URL_PUBLIC_TIPOS_CONTATO, TipoContato[].class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os tipos de contato no Cadastro Unificado
     * @return Configuracao
     */
    public Configuracao buscarConfiguracao() {

        try {
            return this.executeRequestGet(this.urlCadastro + URL_PUBLIC_CONFIG, Configuracao.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Busca os estados no Cadastro Unificado
     * @param  codigo
     * @return Perfil
     */
    public Perfil buscaPerfilPorCodigo(String codigo) {

        try {
            String perfilPorCodigo = URL_PUBLIC_PERFIL_CODIGO.replace("{codigo}", codigo);
            return this.executeRequestGet(this.urlCadastro + perfilPorCodigo, Perfil.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Vincula usuario ao gestão de empreendimentos
     * @param  cpfCnpj
     * @return Message
     */
    public br.ufla.lemaf.beans.Message vincularGestaoEmpreendimentos(String cpfCnpj) {

        return this.executeRequestGet(this.urlCadastro + URL_VINCULAR_GESTAO_EMPREENDIMENTOS + "/" + cpfCnpj, br.ufla.lemaf.beans.Message.class);
    }

    /**
     * Envia documento da licença para Rede Simples
     * @param  confirmaRespostaOrgaoRedeSimples
     * @return Message
     */
    public br.ufla.lemaf.beans.Message enviarDocumentoRedeSimples(ConfirmaRespostaOrgaoRedeSimples confirmaRespostaOrgaoRedeSimples) {

        String json = gson.toJson(confirmaRespostaOrgaoRedeSimples);
        return this.executeRequestPostJson(this.urlCadastro + URL_ENVIAR_DOCUMENTO_REDE_SIMPLES, json, Message.class);
    }

    /**
     * Busca um histórico de empreendimentos
     * @param  cpfCnpj
     * @return Message
     */
    public HistoricoEmpreendimento buscarHistoricoEmpreendimentoPorCpfCnpj(String cpfCnpj) {

        return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_HISTORICO_EMPREENDIMENTO_POR_CPF_CNPJ + cpfCnpj, HistoricoEmpreendimento.class);
    }

    /**
     * Busca um empreendimento historico especifico
     * @param  idEmpreendimentoHistorico
     * @return Message
     */
    public EmpreendimentoHistorico buscarEmpreendimentoHistoricoPorId(Integer idEmpreendimentoHistorico) {

        return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTO_HISTORICO_POR_ID + idEmpreendimentoHistorico, EmpreendimentoHistorico.class);
    }

    public EmpreendimentoSobreposicao intersects(String cpfCnpj) {

        return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTO_SOBREPOSICAO_POR_CPFCNPJ + cpfCnpj, EmpreendimentoSobreposicao.class);
    }

    public EmpreendimentoSobreposicao intersects(Integer idEmpreendimento) {

        return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTO_SOBREPOSICAO_POR_ID + idEmpreendimento, EmpreendimentoSobreposicao.class);
    }

    /**
     * Busca um empreendedor pelo cpfCnpj
     * @param  cpfCnpj
     * @return Empreendedor
     */

    public Empreendedor buscarEmpreendedorPorCpfCnpj(String cpfCnpj) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDEDOR + cpfCnpj, Empreendedor.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Inativa um empreendedor pelo idEmpreendedor
     * @param  idEmpreendedor
     * @return Message
     */

    public Message inativarEmpreendedor(Long idEmpreendedor) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_INATIVAR_EMPREENDEDOR + idEmpreendedor, Message.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * EBuscar um empreendimento pelo id
     * @param id
     * @return Message
     */
    public Empreendimento buscarEmpreendimentoComId(Long id) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTO_ID + "/" + id, Empreendimento.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Buscar empreendimentos pelo id do Empreendedor
     * @param id
     * @return Message
     */
    public Empreendimento findEmpreendimentosByEmpreendedor(Long id) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENDIMENTO_EMPREEENDEDOR_ID + "/" + id, Empreendimento.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Buscar todos empreendedores cadastrados pelo cpfCnpj informado
     * @param cpfCnpj
     * @return EmpreendedorCadastrante
     */
    public EmpreendedorFiltroResult getEmpreendedoresCadastrante(String cpfCnpj) {

        try {
            return this.executeRequestGet(this.urlCadastro + URL_BUSCAR_EMPREENEDORES_CADASTRANTE+ "/" + cpfCnpj, EmpreendedorFiltroResult.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

    /**
     * Força a atualização dos dados cadastrais de Pessoa Jurídica e Empreendimento pela Redesim do CNPJ informado
     * @param cnpj
     * @return Boolean indicando sucesso ou não
     */
    public Boolean forcarAtualizacaoDadosRedesim(String cnpj) {

        try {

            return this.executeRequestGet(this.urlCadastro + URL_ATUALIZAR_PESSOA_JURIDICA_REDESIM + "/" + cnpj, Boolean.class);
        }
        catch (OAuthClientCadastroUnificadoException e) {

            return null;
        }
    }

}
