package br.ufla.lemaf.beans.pessoa;

import br.ufla.lemaf.beans.pessoa.Setor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marcio on 29/01/18.
 */
public class Perfil implements Serializable {

    public Integer id;
    public String nome;
    public String codigo;
    public List<Permissao> permissoes;
    public List<Setor> setores;
}
