
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class ZonaLocalizacao implements Serializable
{

    public String nome;
    public Integer codigo;
    public String descricao;
}
