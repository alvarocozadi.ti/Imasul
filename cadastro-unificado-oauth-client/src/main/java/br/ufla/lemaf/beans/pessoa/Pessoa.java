
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Pessoa implements Serializable
{

    public Integer id;

    public List<Contato> contatos = null;
    public String cpf;
    public Date dataAtualizacao;
    public Date dataCadastro;
    public Date dataNascimento;
    public List<Endereco> enderecos = null;
    public EstadoCivil estadoCivil;
    public Boolean estrangeiro;
    public Boolean isUsuario;
    public String naturalidade;
    public String nome;
    public String nomeMae;
    public Object passaporte;
    public Rg rg;
    public Sexo sexo;
    public TipoPessoa tipo;
    public Object tituloEleitoral;

    public String cnpj;
    public Date dataConstituicao;
    public String inscricaoEstadual;
    public String nomeFantasia;
    public String razaoSocial;
    public Boolean isJunta;

    public Usuario usuario;

}
