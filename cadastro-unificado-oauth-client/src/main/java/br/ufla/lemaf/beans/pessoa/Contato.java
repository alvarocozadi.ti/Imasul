
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class Contato implements Serializable
{

    public Integer id;
    public Boolean principal;
    public TipoContato tipo;
    public String valor;
}
