package br.ufla.lemaf.beans;

import java.util.List;

public class EmpreendimentoFiltroResult {

	public Long totalItems;
	public List<Empreendimento> pageItems;
}