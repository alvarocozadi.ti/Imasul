
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Estado implements Serializable
{

    public Integer id;
    public String sigla;
    public String nome;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
