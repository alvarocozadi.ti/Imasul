package br.ufla.lemaf.beans.historico;

import br.ufla.lemaf.beans.pessoa.Pessoa;

import java.util.Collection;

public class EmpreendimentoSobreposicaoVO {

    public String cpfCnpj;
    public String denominacao;
    public Collection<Pessoa> proprietarios;
    public String geometria;

}
