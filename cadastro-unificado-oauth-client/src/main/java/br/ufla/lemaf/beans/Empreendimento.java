package br.ufla.lemaf.beans;

import br.ufla.lemaf.beans.pessoa.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marcio on 02/02/18.
 */
public class Empreendimento implements Serializable {

    public Integer id;
    public String denominacao;
    public Pessoa pessoa;
    public List<Contato> contatos;
    public List<Endereco> enderecos;
    public Localizacao localizacao;
    public List<Pessoa> proprietarios;
    public List<Pessoa> representantesLegais;
    public List<Pessoa> responsaveisLegais;
    public List<Pessoa> responsaveisTecnicos;
    public List<Cnae> cnaes;
    public String porte;
    public ClassificacaoEmpreendimento classificacaoEmpreendimento;
    public Empreendedor empreendedor;
    public String cpfCnpjCadastrante;
    public Boolean removido;
}
