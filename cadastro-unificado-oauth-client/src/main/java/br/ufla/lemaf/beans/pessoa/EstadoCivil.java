
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class EstadoCivil implements Serializable
{

    public String nome;
    public Integer codigo;
    public String descricao;
}
