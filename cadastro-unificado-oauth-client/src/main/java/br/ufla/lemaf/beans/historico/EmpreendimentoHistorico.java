package br.ufla.lemaf.beans.historico;

import br.ufla.lemaf.beans.Empreendimento;
import br.ufla.lemaf.beans.pessoa.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by marcio on 02/02/18.
 */
public class EmpreendimentoHistorico extends Empreendimento implements Serializable {

    public Pessoa pessoaModificadora;
    public Date dataHistorico;

}
