package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by marcio on 02/02/18.
 */
public class FiltroPessoa implements Serializable {

    public String login;
    public String nome;
    public String nomePerfil;
    public String codigoPerfil;
    public String codigoPermissao;
    public Integer numeroPagina;
    public Integer tamanhoPagina;
    public Date dataAtualizacaoInicio;
    public Date dataAtualizacaoFim;
    public String passaporte;
    public boolean isUsuario;
    public boolean todosUsuarios;
    public Boolean somentePessoaJuridica;
	public Boolean somentePessoaFisica;
	public Boolean somenteCidadaosBrasileiro;

    public FiltroPessoa() {
        this.isUsuario = true;
        this.todosUsuarios = false;
    }
}
