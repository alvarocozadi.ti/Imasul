
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class ClassificacaoEmpreendimento implements Serializable
{

    public Integer id;
    public String descricao;
    public String codigo;
    public Boolean isento;
}
