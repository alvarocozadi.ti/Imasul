package br.ufla.lemaf.beans;

import br.ufla.lemaf.beans.pessoa.Pessoa;

import java.io.Serializable;
import java.util.Date;

public class Empreendedor implements Serializable {

	public Long id;
	public Pessoa pessoa;
	public Date dataCadastro;
	public boolean ativo;
	public boolean podeSerExcluido;

}
