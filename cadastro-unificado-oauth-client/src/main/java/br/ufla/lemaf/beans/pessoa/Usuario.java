package br.ufla.lemaf.beans.pessoa;

import br.ufla.lemaf.beans.pessoa.Setor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marcio on 29/01/18.
 */
@SuppressWarnings("serial")
public class Usuario implements Serializable {
	public Integer id;
    public String login;
    public String nome;
    public String email;
    public List<Perfil> perfis;
    public Perfil perfilSelecionado;
    public List<Setor> setores;
    public Setor setorSelecionado;
    public String sessionKeyEntradaUnica;
}
