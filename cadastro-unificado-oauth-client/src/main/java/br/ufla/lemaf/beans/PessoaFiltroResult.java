package br.ufla.lemaf.beans;

import br.ufla.lemaf.beans.pessoa.Pessoa;

import java.util.List;

public class PessoaFiltroResult {

	public Long totalItems;
	public List<Pessoa> pageItems;
}