package br.ufla.lemaf.beans;

import java.io.Serializable;
import java.util.List;

public class FiltroEmpreendedor implements Serializable {

    public String busca;

    public int numeroPagina;

    public int tamanhoPagina;

    public String ordenacao; /* DENOMINACAO_ASC, DENOMINACAO_DESC, MUNICIPIO_ASC, MUNICIPIO_DESC */

    public String tipoPessoaVinculada; /* PROPRIETARIO, REPRESENTANTE_LEGAL, RESPONSAVEL_LEGAL, RESPONSAVEL_TECNICO */

    public String nomePessoaVinculada;

    public String cpfCnpjPessoaVinculada;

    public String cpfCnpj;

    public Integer codigoIbge;

    public List<String> cpfsCnpjs;
}
