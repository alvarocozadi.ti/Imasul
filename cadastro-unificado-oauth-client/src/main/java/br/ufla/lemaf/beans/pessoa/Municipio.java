
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Municipio implements Serializable
{

    public Integer id;
    public String nome;
    public Estado estado;
    public Integer codigoIbge;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}
