
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class TipoPessoa implements Serializable
{

    public String nome;
    public Integer codigo;
    public String descricao;
}
