package br.ufla.lemaf.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marcio on 02/02/18.
 */
public class EmpreendimentosPessoa implements Serializable {

    public List<Empreendimento> proprietarioEmpreendimentos;
    public List<Empreendimento> representanteLegalEmpreendimentos;
    public List<Empreendimento> responsavelLegalEmpreendimentos;
    public List<Empreendimento> responsavelTecnicoEmpreendimentos;
}
