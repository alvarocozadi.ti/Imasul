
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class ConfirmaRespostaOrgaoRedeSimples implements Serializable {

    public String cnpj;
    public String codSituacao;
    public String numeroLicenca;
    public String dataExpedicao;
    public byte[] arquivoPDF;

    public ConfirmaRespostaOrgaoRedeSimples(String cnpj, String codSituacao, String numeroLicenca, String dataExpedicao, byte[] arquivoPDF) {
        super();
        this.cnpj = cnpj;
        this.codSituacao = codSituacao;
        this.numeroLicenca = numeroLicenca;
        this.dataExpedicao = dataExpedicao;
        this.arquivoPDF = arquivoPDF;
    }
}
