
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class TipoContato implements Serializable
{

    public static final Integer ID_EMAIL = 1;
    public static final Integer ID_TELEFONE_RESIDENCIAL = 2;
    public static final Integer ID_TELEFONE_COMERCIAL = 3;
    public static final Integer ID_TELEFONE_CELULAR = 4;

    public String descricao;
    public Integer id;
}
