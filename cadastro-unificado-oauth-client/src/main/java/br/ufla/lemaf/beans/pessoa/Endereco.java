
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

public class Endereco implements Serializable
{

    public String bairro;
    public String caixaPostal;
    public String cep;
    public String complemento;
    public Object descricaoAcesso;
    public Integer id;
    public String logradouro;
    public Municipio municipio;
    public Integer numero;
    public Pais pais;
    public Boolean semNumero;
    public TipoEndereco tipo;
    public ZonaLocalizacao zonaLocalizacao;
}
