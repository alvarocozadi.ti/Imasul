
package br.ufla.lemaf.beans.pessoa;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Pais implements Serializable {

    public Integer id;
	public String nome;
	public String sigla;
}
