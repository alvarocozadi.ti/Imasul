package br.ufla.lemaf.beans;

import java.util.List;

public class EmpreendedorFiltroResult {

	public Long totalItems;
	public List<Empreendedor> pageItems;
}