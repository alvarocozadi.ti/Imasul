package test.java;

import com.ning.http.util.Base64;
import br.ufla.lemaf.beans.pessoa.Usuario;
import br.ufla.lemaf.services.CadastroUnificadoPessoaService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class UsuarioTest extends DefaultTest {

	CadastroUnificadoPessoaService client;

	@Before
	public void initialize() {

		client = new CadastroUnificadoPessoaService(clientId, clientSecret, urlPortal, urlCadastro);
		assertNotNull(client);
	}

	@Test
	public void login() {

		Usuario usuario = client.login(cpfUsuario, senhaUsuario);
		assertNotNull(usuario);
		assertNotNull(usuario.sessionKeyEntradaUnica);
	}

	@Test
	public void searchBySessionKey() {

		Usuario usuario = client.login(cpfUsuario, senhaUsuario);
		assertNotNull(usuario);
		assertNotNull(usuario.sessionKeyEntradaUnica);

        Usuario usuarioByPortal = client.searchBySessionKey(usuario.sessionKeyEntradaUnica);
        assertNotNull(usuarioByPortal);
	}

	@Test
	public void isAllowedAccess() {

		Usuario usuario = client.login(cpfUsuario, senhaUsuario);
		assertNotNull(usuario);
		assertNotNull(usuario.sessionKeyEntradaUnica);

		boolean result = client.isAllowedAccess(usuario.sessionKeyEntradaUnica, "", "");
		assertNotNull(result);
	}

	@Test
	public void isAllowedAccessByModule() {

		Usuario usuario = client.login(cpfUsuario, senhaUsuario);
		assertNotNull(usuario);
		assertNotNull(usuario.sessionKeyEntradaUnica);

		String strEncode = "96b484a5307e3c40f404a7af53a8c6645664e31ff25ae3cfe7cea3bdcf675e72466dfa9a1f2d2bcbe2732888a97b7bb62bb070745c680aa548e5dd6e46354e00" +
			":" +
			"cf5ccfcf69a18b04721fa6bb34e899ab56258f5212e095e7c04df9ba93b398695d00d3059e49008746203667b47c3b8a98bb864aa9cc3b09a2ce08f02e888b14";

		String authorizationGetToken = "Basic " + new String(Base64.encode(strEncode.getBytes()));

		boolean result = client.isAllowedAccessByModule(authorizationGetToken, "", "");
		assertNotNull(result);
	}

}
