package test.java;

import br.ufla.lemaf.beans.Configuracao;
import br.ufla.lemaf.beans.PessoaFiltroResult;
import br.ufla.lemaf.beans.pessoa.FiltroPessoa;
import br.ufla.lemaf.beans.pessoa.Pessoa;
import br.ufla.lemaf.services.CadastroUnificadoPessoaService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class PessoaTest extends DefaultTest {

    CadastroUnificadoPessoaService client;

    @Before
    public void initialize() {

        client = new CadastroUnificadoPessoaService(clientId, clientSecret, urlPortal, urlCadastro);

        assertNotNull(client);
    }

    @Test
    public void pessoaByFiltro() {

        FiltroPessoa filtro = new FiltroPessoa();
        filtro.login = cpfUsuario;
        PessoaFiltroResult pessoas = client.buscarPessoasComFiltroAll(filtro);
        assertNotNull(pessoas);
    }

    // TODO - Implementar outros testes para cada atributo do filtro FiltroPessoa

    @Test
    public void pessoaByCpf() {

        Pessoa pessoaFisica = client.buscarPessoaFisicaPeloCpf(cpfUsuario);
        assertNotNull(pessoaFisica);
        assertNotNull(pessoaFisica.cpf);
    }

    @Test
    public void pessoaByCnpj() {

        Pessoa pessoaJuridica = client.buscarPessoaJuridicaPeloCnpj("34696784000156");

        // TODO - Definir cnpj de testes
    }

    @Test
    public void buscarConfiguracoes() {

        Configuracao config = client.buscarConfiguracao();
        assertNotNull(config);
    }

}
