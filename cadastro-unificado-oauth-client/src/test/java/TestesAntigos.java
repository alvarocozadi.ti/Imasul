package test.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import br.ufla.lemaf.OAuthClientCadastroUnificadoException;
import br.ufla.lemaf.beans.pessoa.*;
import br.ufla.lemaf.deserializers.DateDeserializer;
import br.ufla.lemaf.serializers.DateSerializer;
import br.ufla.lemaf.services.CadastroUnificadoPessoaService;

import java.util.ArrayList;
import java.util.Date;

public class TestesAntigos {

    public static final Gson gson;
    static {

        gson = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .registerTypeAdapter(Date.class, new DateSerializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
    }
    
    public static void main(String[] args) {

    	// Informações para configurar o Cadastro unificado
        String clientId = "439a725c402d486774957b23bd15cf6bcc9e546494a8ca483fe27fb354c9f9389157ed315890bc01c27382ac6e4e5cd91c60d4c6f63b6feecc5c6c4d52cb9f3d";
        String clientSecret = "683a83da075eeda5bab2d10fb575af8d49432b61bdc97258ca795e4d1c1d3cdfdf53f312063c92a4c9e4568df5d3ea91f7ed6049e65428499b82817b7bfb6ac6";
        String urlPortal = "http://localhost:9524";
        String urlCadastro = "http://localhost:9523";

//        String jsonEmpreendimento = "{\"denominacao\":\"Prefeitura municipal de Chaves 2\",\"pessoa\":{\"tituloEleitoral\":null,\"estrangeiro\":false,\"cpf\":\"18634848507\",\"nome\":\"Márcio Greison de Azevedo\",\"dataNascimento\":\"05/08/1981\",\"sexo\":{\"nome\":\"MASCULINO\",\"codigo\":0,\"descricao\":\"Masculino\"},\"nomeMae\":\"Mae do marcio\",\"estadoCivil\":{\"nome\":\"CASADO\",\"codigo\":1,\"descricao\":\"Casado\"},\"naturalidade\":\"Lavrense\",\"rg\":{\"numero\":\"111111111\",\"orgaoExpedidor\":\"XXX\",\"dataExpedicao\":\"04/04/1994\"},\"enderecos\":[{\"tipo\":{\"id\":1},\"zonaLocalizacao\":{\"nome\":\"URBANA\",\"codigo\":0,\"descricao\":\"Urbana\"},\"pais\":{\"id\":29},\"semNumero\":false,\"logradouro\":\"Logradouro\",\"numero\":\"19\",\"bairro\":\"Bairro\",\"cep\":\"37200000\",\"municipio\":{\"codigoIbge\":1502509,\"estado\":{\"id\":14},\"id\":1068,\"nome\":\"Chaves\"},\"usarPrincipal\":true},{\"tipo\":{\"id\":2},\"zonaLocalizacao\":{\"nome\":\"URBANA\",\"codigo\":0,\"descricao\":\"Urbana\"},\"pais\":{\"id\":29},\"semNumero\":false,\"logradouro\":\"Logradouro2\",\"numero\":\"19\",\"bairro\":\"Bairro\",\"cep\":\"37200000\",\"municipio\":{\"codigoIbge\":1502509,\"estado\":{\"id\":14},\"id\":1068,\"nome\":\"Chaves\"}}],\"passaporte\":null,\"tipo\":{\"nome\":\"FISICA\",\"codigo\":0,\"descricao\":\"Física\"}},\"enderecos\":[{\"tipo\":{\"id\":1},\"zonaLocalizacao\":{\"nome\":\"URBANA\",\"codigo\":0,\"descricao\":\"Urbana\"},\"pais\":{\"id\":29},\"semNumero\":false,\"logradouro\":\"Logradouro\",\"numero\":\"19\",\"bairro\":\"Bairro\",\"uf\":{\"id\":14},\"cep\":\"37200000\",\"municipio\":{\"codigoIbge\":1502509,\"estado\":{\"id\":14},\"id\":1068,\"nome\":\"Chaves\"},\"usarPrincipal\":true},{\"tipo\":{\"id\":2},\"zonaLocalizacao\":{\"nome\":\"URBANA\",\"codigo\":0,\"descricao\":\"Urbana\"},\"pais\":{\"id\":29},\"semNumero\":false,\"logradouro\":\"Logradouro2\",\"numero\":\"19\",\"bairro\":\"Bairro\",\"uf\":{\"id\":14},\"cep\":\"37200000\",\"municipio\":{\"codigoIbge\":1502509,\"estado\":{\"id\":14},\"id\":1068,\"nome\":\"Chaves\"}}],\"localizacao\":{\"geometria\":{\"geometry\":{\"type\":\"Point\",\"coordinates\":[-46.40625,-15.623036831528252]}}},\"proprietarios\":[],\"representantesLegais\":[{\"id\":163,\"tipo\":{\"nome\":\"FISICA\",\"codigo\":0,\"descricao\":\"Física\"}}],\"responsaveisLegais\":[],\"responsaveisTecnicos\":[]}";

        try {

            CadastroUnificadoPessoaService client = new CadastroUnificadoPessoaService(clientId, clientSecret, urlPortal, urlCadastro);
	        Perfil perfil= client.buscaPerfilPorCodigo("Proprietario Possuidor");
//
            // Usuario
//            Usuario usuario = client.login("92964946051", "230503");
//            Boolean isUser = client.isUser(usuario.login);
//            Usuario usuarioLogado = client.searchBySessionKey(usuario.sessionKeyEntradaUnica);

//            FiltroPessoa filtro = new FiltroPessoa();
//            filtro.login = "33987490896";
//            filtro.nomePerfil = "Gestor Municipal";
//            PessoaFiltroResult pessoas = client.buscarPessoasComFiltroAll(filtro);
//            System.out.println("Filtro: " + pessoas.pageItems.get(0).cpf);
//            // Empreendimento
//            Empreendimento empreendimentoCadastrar = gson.fromJson(jsonEmpreendimento, Empreendimento.class);
//            Message cadastroEmp = client.cadastrarEmpreendimentoPessoa(empreendimentoCadastrar);
//            EmpreendimentosPessoa empreendimentos = client.buscarEmpreendimentosPessoa("05344064675");
//
//            // Pessoa
//            Pessoa pessoaFisica = client.buscarPessoaFisicaPeloCpf("33987490896");
//            Pessoa pessoaJuridica = client.buscarPessoaJuridicaPeloCnpj("34696784000156");
            
//            Municipio[] municipio = client.buscarMunicipio(20);
//
//            System.out.print(municipio[0].nome);
//            System.out.println(client.temPessoaComCpfCnpj("33987490896"));
//            Configuracao config = client.buscarConfiguracao();
//            System.out.println(config.tipos.TipoPessoa[0].nome);
            
//            System.out.println("Edita Pessoa Fisica: " + listPaises.paises.size());
            
            // Gera dados Fake para teste
//            Pessoa pessoaFisicaCE = geraPessoaFisica();
//            Pessoa pessoaJuridicaCE = geraPessoaJuridica();
//            
//            Message cadastPF = client.cadastrarPessoaFisica(pessoaFisicaCE);
//            Message cadastPJ = client.cadastrarPessoaJuridica(pessoaJuridicaCE);
//            System.out.println("Cadastro Pessoa Fisica: " + cadastPF);
//            System.out.println("Cadastro Pessoa Jurídica: " + cadastPJ);
//            
//            pessoaFisica.nome = "Alterado PF";
//            pessoaJuridica.nomeFantasia = "Alterado PJ";
////                    
//            Message editPF = client.alterarDadosPessoaFisica(pessoaFisica);
//            Message editPJ = client.alterarDadosPessoaJuridica(pessoaJuridica);
////
//            System.out.println("Edita Pessoa Fisica: " + editPF);
//            System.out.println("Edita Pessoa Jurídica: " + editPJ);
            
//            System.out.println("Usuario Logado: " + client.isLogged(usuario.sessionKeyEntradaUnica));

        }
        catch (OAuthClientCadastroUnificadoException e) {
            e.printStackTrace();
        }
    }
    
    public static Pessoa geraPessoaFisica() {
    	Pessoa pessoa = new Pessoa();
    	
    	Contato contato = new Contato();
    	contato.principal = true;
    	contato.tipo = new TipoContato();
    	contato.tipo.descricao = "Opa";
    	contato.tipo.id = 2;
    	contato.valor = "123";
    	pessoa.contatos = new ArrayList<Contato>();
    	
    	pessoa.contatos.add(contato);
    	
    	pessoa.cpf = "92964946051";
    	pessoa.dataAtualizacao = new Date();
    	pessoa.dataCadastro = new Date();
    	pessoa.dataNascimento = new Date();
    	
    	Endereco endereco = new Endereco();
    	
    	endereco.bairro = "Bairro PF";
    	endereco.caixaPostal = "Caix postal PF";
    	endereco.numero = 2;
    	endereco.cep = "37200000";
    	endereco.complemento = "complemento";
    	endereco.descricaoAcesso = "Descricao";
    	endereco.logradouro = "Rua";
    	
    	endereco.zonaLocalizacao = new ZonaLocalizacao();
    	endereco.zonaLocalizacao.codigo = 1;
    	endereco.zonaLocalizacao.descricao = "Zona 2";
    	endereco.zonaLocalizacao.nome = "Urbana";
    	
    	endereco.municipio = new Municipio();
    	endereco.municipio.estado = new Estado();
    	endereco.municipio.estado.id = 27;
    	endereco.municipio.id = 2579;
    	endereco.municipio.codigoIbge = 3;
    	endereco.semNumero = false;
    	endereco.municipio.nome = "nome";
    	endereco.tipo = new TipoEndereco();
    	endereco.tipo.id = 1;
    	endereco.pais = new Pais();
    	endereco.pais.id = 29;

    	
    	pessoa.enderecos = new ArrayList<Endereco>();
    	
    	pessoa.enderecos.add(endereco);
    	
    	Endereco enderecoCorrespondencia = new Endereco();
    	
    	enderecoCorrespondencia.bairro = "Bairroe PF";
    	enderecoCorrespondencia.caixaPostal = "Caix postal PF";
    	enderecoCorrespondencia.numero = 1;
    	enderecoCorrespondencia.cep = "37300000";
    	enderecoCorrespondencia.complemento = "complemefnto";
    	enderecoCorrespondencia.descricaoAcesso = "Descrficao";
    	enderecoCorrespondencia.logradouro = "Ruea";

    	enderecoCorrespondencia.zonaLocalizacao = new ZonaLocalizacao();
    	enderecoCorrespondencia.zonaLocalizacao.codigo = 0;
    	enderecoCorrespondencia.zonaLocalizacao.descricao = "Zona 2";
    	enderecoCorrespondencia.zonaLocalizacao.nome = "Urbana";

    	enderecoCorrespondencia.municipio = new Municipio();
    	enderecoCorrespondencia.municipio.estado = new Estado();
    	enderecoCorrespondencia.municipio.estado.id = 27;
    	enderecoCorrespondencia.municipio.codigoIbge = 3;
    	enderecoCorrespondencia.municipio.id = 2579;
    	enderecoCorrespondencia.municipio.nome = "nome";
    	enderecoCorrespondencia.semNumero = false;
    	enderecoCorrespondencia.tipo = new TipoEndereco();
    	enderecoCorrespondencia.pais = new Pais();
    	enderecoCorrespondencia.pais.id = 29;
    	
    	enderecoCorrespondencia.tipo.id = 2;
    	
    	pessoa.enderecos.add(enderecoCorrespondencia);
    	
    	pessoa.estadoCivil = new EstadoCivil();
    	
    	pessoa.estadoCivil.descricao = "descrito";
    	pessoa.estadoCivil.nome = "nome";
    	
    	pessoa.estrangeiro = false; 	
    	
    	pessoa.nome = "Meu nome";
    	pessoa.nomeMae = "Mãe";
    	
    	pessoa.rg = new Rg();
    	pessoa.rg.numero = "4654987";
    	pessoa.rg.orgaoExpedidor = "MGsp";

    	pessoa.sexo = new Sexo();
    	pessoa.sexo.codigo = 1;
    	pessoa.sexo.descricao = "M";
    	pessoa.sexo.nome = "Um nome";
    	
    	pessoa.naturalidade = "Brasileiro";
    	
    	pessoa.estadoCivil = new EstadoCivil();
    	pessoa.estadoCivil.codigo = 1;
    	pessoa.estadoCivil.descricao = "Solteiro";
    	pessoa.estadoCivil.nome = "Nme Civil";
    	
    	
    	return pessoa;
    }
    
    public static Pessoa geraPessoaJuridica() {
    	Pessoa pessoa = new Pessoa();
    	
    	Endereco endereco = new Endereco();
    	
    	endereco.bairro = "Bairro PF";
    	endereco.caixaPostal = "Caix postal PF";
    	endereco.numero = 2;
    	endereco.cep = "37200000";
    	endereco.complemento = "complemento";
    	endereco.descricaoAcesso = "Descricao";
    	endereco.logradouro = "Rua";
    	
    	endereco.zonaLocalizacao = new ZonaLocalizacao();
    	endereco.zonaLocalizacao.codigo = 1;
    	endereco.zonaLocalizacao.descricao = "Zona 2";
    	endereco.zonaLocalizacao.nome = "Urbana";
    	
    	endereco.municipio = new Municipio();
    	endereco.municipio.estado = new Estado();
    	endereco.municipio.estado.id = 27;
    	endereco.municipio.id = 2579;
    	endereco.municipio.codigoIbge = 3;
    	endereco.semNumero = false;
    	endereco.municipio.nome = "nome";
    	endereco.tipo = new TipoEndereco();
    	endereco.tipo.id = 1;
    	endereco.pais = new Pais();
    	endereco.pais.id = 29;

    	
    	pessoa.enderecos = new ArrayList<Endereco>();
    	
    	pessoa.enderecos.add(endereco);
    	
    	Endereco enderecoCorrespondencia = new Endereco();
    	
    	enderecoCorrespondencia.bairro = "Bairroe PF";
    	enderecoCorrespondencia.caixaPostal = "Caix postal PF";
    	enderecoCorrespondencia.numero = 1;
    	enderecoCorrespondencia.cep = "37300000";
    	enderecoCorrespondencia.complemento = "complemefnto";
    	enderecoCorrespondencia.descricaoAcesso = "Descrficao";
    	enderecoCorrespondencia.logradouro = "Ruea";

    	enderecoCorrespondencia.zonaLocalizacao = new ZonaLocalizacao();
    	enderecoCorrespondencia.zonaLocalizacao.codigo = 0;
    	enderecoCorrespondencia.zonaLocalizacao.descricao = "Zona 2";
    	enderecoCorrespondencia.zonaLocalizacao.nome = "Urbana";

    	enderecoCorrespondencia.municipio = new Municipio();
    	enderecoCorrespondencia.municipio.estado = new Estado();
    	enderecoCorrespondencia.municipio.estado.id = 27;
    	enderecoCorrespondencia.municipio.codigoIbge = 3;
    	enderecoCorrespondencia.municipio.id = 2579;
    	enderecoCorrespondencia.municipio.nome = "nome";
    	enderecoCorrespondencia.semNumero = false;
    	enderecoCorrespondencia.tipo = new TipoEndereco();
    	enderecoCorrespondencia.pais = new Pais();
    	enderecoCorrespondencia.pais.id = 29;
    	
    	enderecoCorrespondencia.tipo.id = 2;
    	
    	pessoa.enderecos.add(enderecoCorrespondencia);
    	
    	pessoa.cnpj = "34696784000156";
    	pessoa.dataConstituicao = new Date();
    	pessoa.inscricaoEstadual = "123456";
    	pessoa.nomeFantasia = "Nome pf";
    	pessoa.razaoSocial = "Razao";
    	return pessoa;
    }
    
    
}
