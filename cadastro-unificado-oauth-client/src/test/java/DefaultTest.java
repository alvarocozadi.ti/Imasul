package test.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import br.ufla.lemaf.deserializers.DateDeserializer;
import br.ufla.lemaf.serializers.DateSerializer;

import java.util.Date;

public class DefaultTest {

    // Informações para configurar o Cadastro unificado - Módulo Analise - Márcio - DEV1
    protected String clientId = "9a7fdbfaf547ca98958c8cf1d4777b67c08f4ad893f110f364a0eaa05bb249de7b66b242bb64df898b74bb7b49b9777c2d79e5c8e4fa7cd721e004e30c07f9a5";
    protected String clientSecret = "36f30a526b96d71b48e8cbc863dc6ca8481e1bd3f14193e00621902a8aa58eedffe9374c4c9fba76b1fb4d7bf46388cfcd37e5a479e2326fdf7af46395440f29";

    // Informações para configurar o Cadastro unificado - Módulo ????
//    protected String clientId = "439a725c402d486774957b23bd15cf6bcc9e546494a8ca483fe27fb354c9f9389157ed315890bc01c27382ac6e4e5cd91c60d4c6f63b6feecc5c6c4d52cb9f3d";
//    protected String clientSecret = "683a83da075eeda5bab2d10fb575af8d49432b61bdc97258ca795e4d1c1d3cdfdf53f312063c92a4c9e4568df5d3ea91f7ed6049e65428499b82817b7bfb6ac6";

    // Url's de acesso ao portal e cadastro unificado - Servidor Runners
    protected String urlPortal = "http://car.to.java3-5.ti.lemaf.ufla.br/portal-seguranca";
    protected String urlCadastro = "http://car.to.java3-5.ti.lemaf.ufla.br/cadastro-unificado";

    // Url's de acesso ao portal e cadastro unificado - Local
//    protected String urlPortal = "http://localhost:9524";
//    protected String urlCadastro = "http://localhost:9523";


    // Dados de autenticação do usuário de testes
    protected String cpfUsuario = "05344064675";
    protected String senhaUsuario = "230503";

    protected static final Gson gson;
    static {

        gson = new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .registerTypeAdapter(Date.class, new DateSerializer())
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
    }
}
